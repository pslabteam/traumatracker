'use strict';

angular.module(
		'gwApp',
		[ 'ui.router', 'ui.bootstrap', 'ngAnimate', 'ngTouch', 'ngSanitize', 'dialogs.main' ])

.constant("config", {
	"SERVER_URL" : 'http://127.0.0.1:8082',
	"SIMULATION" : false
})

.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('');

	$stateProvider
		.state('monitors_list', {
			url : '',
			templateUrl : 'views/monitors_list_view.html',
			controller : 'monitors_list_controller'
		})
		.state('monitor', {
			url : '/monitorView',
			cache : false,
			templateUrl : 'views/monitor_view.html',
			params : {
				monitorID : 'unknown',
				ip_gateway : 'unknown',
				port_gateway : 'unknown'
			},
			controller : 'monitor_controller'
		});
	});
