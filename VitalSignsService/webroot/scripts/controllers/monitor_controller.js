'use strict';

angular
		.module('gwApp')
		.controller('monitor_controller', function($scope, $rootScope, $location, $state, $http, config) {
					$scope.check_autoupdate = false;
					var autoreload = $scope.check_autoupdate;
					var autoreload_timer;
					$scope.check_audio = false;
					var autoreload_time = 3000;
					var audio = $scope.check_audio;
					var audio_tic = new Audio('audio/tic_ecg.wav');
					$scope.showProblemsAlert = true;
					$scope.message = "Caricamento..";

					// info monitor
					var monitorID = $state.params.monitorID;
					var bed_unit = monitorID.split("_")[1];
					var care_unit = monitorID.split("_")[0];
					if (bed_unit == null && care_unit == null) {
						$state.go('monitors_list');
					}
					$scope.monitorID = monitorID;
					$scope.bed_unit = bed_unit;
					$scope.care_unit = care_unit;
					var ip_gateway = $state.params.ip_gateway;
					$scope.ip_gateway = ip_gateway;
					var port_gateway = $state.params.port_gateway;
					$scope.port_gateway = port_gateway;

					// opzioni monitor
					$scope.check_autoupdate = false;

					// prendo i valori la prima volta, poi dipende se l'utente
					// mette l'auto update
					if (config.SIMULATION == true) {
						simulateValues();
					} else {
						getValues();
					}

					// simulazione valori
					function simulateValues() {
						$scope.showProblemsAlert = false;
						if (autoreload == true) {
							autoreload_timer = setInterval(function() {
								playAudio();
								$scope.DIA = Math
										.floor((Math.random() * 167) + 1);
								$scope.SYS = Math
										.floor((Math.random() * 69) + 1);
								$scope.Temp = Math
										.floor((Math.random() * 40) + 1);
								$scope.HR = Math
										.floor((Math.random() * 83) + 1);
								$scope.SpO2 = Math
										.floor((Math.random() * 97) + 1);
								$scope.EtCO2 = Math
										.floor((Math.random() * 50) + 1);
								$scope.$apply();
							}, 3000);

						} else if (autoreload == false) {
							playAudio();
							window.clearInterval(autoreload_timer);
							autoreload_timer = null;
							$scope.DIA = 167;
							$scope.SYS = 69;
							$scope.Temp = 40;
							$scope.HR = 83;
							$scope.SpO2 = 97;
							$scope.EtCO2 = 50;
						}
					}

					// get valori da gatewayservice
					function getValues() {
						if (autoreload == true) {
							autoreload_timer = setInterval(function() {
								playAudio();
								getValuesServer();
							}, 5000);
						} else if (autoreload == false) {
							playAudio();
							window.clearInterval(autoreload_timer);
							autoreload_timer = null;
							getValuesServer();
						}
					}

					function getValuesServer() {
						$http
								.get(
										"/gt2/vsservice/api/monitors/"
												+ monitorID + "/vitalsigns")
								.then(
										function(response) {
											$scope.showProblemsAlert = false;
											
											var json = response.data;
											
											$scope.pid = json.pid;
											$scope.pname = json.pname;
											$scope.timestamp = json.timestamp;
											
											var vitalsigns = json.vitalsigns;
											
											
											for (var i = 0; i < vitalsigns.length; i++) {
												if (vitalsigns[i].type == "DIA") {
													$scope.DIA = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												} else if (vitalsigns[i].type == "SYS") {
													$scope.SYS = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												} else if (vitalsigns[i].type == "Temp") {
													$scope.Temp = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												} else if (vitalsigns[i].type == "HR") {
													$scope.HR = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												} else if (vitalsigns[i].type == "SpO2") {
													$scope.SpO2 = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												} else if (vitalsigns[i].type == "EtCO2") {
													$scope.EtCO2 = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												} else if (vitalsigns[i].type == "NBP-S") {
													$scope.NBPS = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												} else if (vitalsigns[i].type == "NBP-D") {
													$scope.NBPD = vitalsigns[i].value
															+ " "
															+ vitalsigns[i].uom;
												}
											}
										},
										function error(response) {
											$scope.message = "Impossibile recuperare i dati. Controllare che il server e il gateway siano attivi!";
											$scope.showProblemsAlert = true;
											$scope.HR = "--";
											$scope.SpO2 = "--";
											$scope.EtCO2 = "--";
											$scope.NBPS = "--";
											$scope.NBPD = "--";
											$scope.SYS = "--";
											$scope.DIA = "--";
											$scope.Temp = "--";
										});
					}

					// tick quando cambiano i valori
					function playAudio() {
						if (audio == true) {
							audio_tic.play();
						}
					}

					// checkbox
					$scope.autoreload_change = function(value) {
						autoreload = value;
						if (config.SIMULATION == true) {
							simulateValues();
						} else {
							getValues();
						}
					}

					$scope.audio_change = function(value) {
						audio = value;
					}

					// resettare la interval alla chiusura
					$scope.$on("$destroy", function() {
						window.clearInterval(autoreload_timer);
					});

				});
