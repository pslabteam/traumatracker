'use strict';

angular.module('gwApp')
	.controller('monitors_list_controller', function ($scope, $rootScope, $location, $state, $http, dialogs, config) {

		$scope.showProblemsAlert = true;
		$scope.message = "Caricamento..";
		
		var monitors;

		setup();

		function setup() {
			if (config.SIMULATION == true) {
				$scope.showProblemsAlert = false;
				$scope.monitors = [
					{ id: "prova_prova", ip_gateway: "localhost_simulated", port_gateway: 8080 },
					{ id: "prova2_prova2", ip_gateway: "localhost_simulated", port_gateway: 8080 }
				];
			} else {
				$http.get("/gt2/vsservice/api/monitors").then(function success(response) {
					$scope.showProblemsAlert = false;
					$scope.monitors = [].concat(response.data);
					console.lo
					updateValues();
				}, function error(response) {
					$scope.message = "Impossibile recuperare i dati. Controllare che il server e il gateway siano attivi!";
					$scope.showProblemsAlert = true;
				});
			}
		}
		
		function updateValues(){
			for (var i = 0; i < $scope.monitors.length; i++) {
				
				$http.get("/gt2/vsservice/api/monitors/" + $scope.monitors[i].id + "/vitalsigns")
				.then(function(response) {
					var json = response.data;
					
					var monitorID = response.config.url.split("/",6)[5];

					$scope.monitors.find(x=>x.id == monitorID).pid = json.pid;
					$scope.monitors.find(x=>x.id == monitorID).pname = json.pname;
					$scope.monitors.find(x=>x.id == monitorID).timestamp = json.timestamp;
					
					var vitalsigns = json.vitalsigns;
					
					for (var j = 0; j < vitalsigns.length; j++) {
						if (vitalsigns[j].type == "DIA") {
							$scope.monitors.find(x => x.id == monitorID).DIA = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						} else if (vitalsigns[j].type == "SYS") {
							$scope.monitors.find(x=>x.id == monitorID).SYS = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						} else if (vitalsigns[j].type == "Temp") {
							$scope.monitors.find(x=>x.id == monitorID).Temp = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						} else if (vitalsigns[j].type == "HR") {
							$scope.monitors.find(x=>x.id == monitorID).HR = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						} else if (vitalsigns[j].type == "SpO2") {
							$scope.monitors.find(x=>x.id == monitorID).SpO2 = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						} else if (vitalsigns[j].type == "EtCO2") {
							$scope.monitors.find(x=>x.id == monitorID).EtCO2 = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						} else if (vitalsigns[j].type == "NBP-S") {
							$scope.monitors.find(x=>x.id == monitorID).NBPS = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						} else if (vitalsigns[j].type == "NBP-D") {
							$scope.monitors.find(x=>x.id == monitorID).NBPD = vitalsigns[j].value
									+ " "
									+ vitalsigns[j].uom;
						}
					}
				}, function error(response){
					var monitorID = response.config.url.split("/",6)[5];
					
					$scope.monitors.find(x=>x.id == monitorID).pid = "N/A";
					$scope.monitors.find(x=>x.id == monitorID).pname = "N/A";
					$scope.monitors.find(x=>x.id == monitorID).timestamp = "N/A";
					
					$scope.monitors.find(x=>x.id == monitorID).HR = "--";
					$scope.monitors.find(x=>x.id == monitorID).SYS = "--";
					$scope.monitors.find(x=>x.id == monitorID).DIA = "--";
					$scope.monitors.find(x=>x.id == monitorID).NBPS = "--";
					$scope.monitors.find(x=>x.id == monitorID).NBPD = "--";
					$scope.monitors.find(x=>x.id == monitorID).SpO2 = "--";
					$scope.monitors.find(x=>x.id == monitorID).EtCO2 = "--";
					$scope.monitors.find(x=>x.id == monitorID).Temp = "--";
				});
				
				//console.log("HTTP request done for " + $scope.monitors[i].id);
			}
		}

		//aprire pagina monitor
		$scope.openMonitor = function (id, ip, port) {
			$state.go('monitor', { monitorID: id, ip_gateway: ip, port_gateway: port });
		}

		//dialog che chiede la conferma
		$scope.showConfirm = function (monitorID) {
			var dlg = dialogs.confirm('Continuare?', 'Le modifiche sono irreversibili!');
			dlg.result.then(function (btn) {
				deleteMonitor(monitorID);
			}, function (btn) {

			});
		}

		//modifica/aggiunta monitor
		$scope.showAddEdit = function (id, ip_gateway, port_gateway, edit) {
			var data;
			if (edit == true) {
				var care_unit_label = id.split("_")[0];
				var bed_unit_label = id.split("_")[1];
				data = { care_unit_label: care_unit_label, bed_unit_label: bed_unit_label, ip_gateway: ip_gateway, port_gateway: port_gateway, edit: true };
			} else {
				data = { care_unit_label: "", bed_unit_label: "", ip_gateway: "", port_gateway: "", edit: false };
			}
			var dlg = dialogs.create('/views/dialog_insert.html', 'dialogInsertController', data, { key: false, back: 'static' });
			dlg.result.then(function (ret) {
				if (edit == true) {
					editMonitor(ret.oldID,ret.id, ret.ip_gateway, ret.port_gateway);
				} else {
					addMonitor(ret.id, ret.ip_gateway, ret.port_gateway);
				}
			}, function () {

			});
		}

		//modifica monitor
		function editMonitor(oldMonitorID,newMonitorID, ip_gateway, port_gateway) {
			$scope.showProblemsAlert = true;
			$http.put("/gt2/vsservice/api/monitors/" + oldMonitorID, { id: newMonitorID, ip_gateway: ip_gateway, port_gateway: port_gateway }).then(function (response) {
				$scope.showProblemsAlert = false;
				$scope.messageInfo = "Monitor modificato con successo!";
				$scope.showInfoAlert = true;
				setup();
			}, function error(response) {
				$scope.message = "Impossibile recuperare i dati. Controllare che il server e il gateway siano attivi!";
				$scope.showProblemsAlert = true;
			});
		}

		//aggiunta monitor
		function addMonitor(monitorID, ip_gateway, port_gateway) {
			$scope.showProblemsAlert = true;
			$http.post("/gt2/vsservice/api/monitors", { id: monitorID, ip_gateway: ip_gateway, port_gateway: port_gateway }).then(function (response) {
				$scope.showProblemsAlert = false;
				$scope.messageInfo = "Monitor aggiunto con successo!";
				$scope.showInfoAlert = true;
				setup();
			}, function error(response) {
				if(response.data == null){
					$scope.message = "Impossibile recuperare i dati. Controllare che il server e il gateway siano attivi!";
				} else{
					$scope.message="Errore: "+response.data.message;
				}
				$scope.showProblemsAlert = true;
			});
		}

		//eliminazione monitor
		function deleteMonitor(monitorID) {
			$scope.showProblemsAlert = true;
			$http.delete("/gt2/vsservice/api/monitors/" + monitorID).then(function (response) {
				$scope.showProblemsAlert = false;
				$scope.messageInfo = "Monitor eliminato con successo!";
				$scope.showInfoAlert = true;
				setup();

			}, function error(response) {
				$scope.message = "Impossibile recuperare i dati. Controllare che il server e il gateway siano attivi!";
				$scope.showProblemsAlert = true;
			});
		}
	})

	//controller per il dialog inserimento dati
	.controller('dialogInsertController', function ($scope, $uibModalInstance, data) {

		var oldID=data.care_unit_label+"_"+data.bed_unit_label;
		if (data.edit == true) {
			$scope.care_unit_label = data.care_unit_label;
			$scope.bed_unit_label = data.bed_unit_label;
			$scope.port_gateway = data.port_gateway;
			$scope.ip_gateway = data.ip_gateway;
		}

		$scope.insertCancel = function () {
			$uibModalInstance.dismiss('Canceled');
		};

		$scope.insertSave = function (care_unit_label, bed_unit_label, ip_gateway, port_gateway) {
			var id=care_unit_label+"_"+bed_unit_label;
			var ret={care_unit_label: care_unit_label, bed_unit_label: bed_unit_label, ip_gateway: ip_gateway, port_gateway: port_gateway,id: id, oldID: oldID};
			$uibModalInstance.close(ret);
		};

	});
