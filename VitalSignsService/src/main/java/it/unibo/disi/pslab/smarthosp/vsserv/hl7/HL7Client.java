package it.unibo.disi.pslab.smarthosp.vsserv.hl7;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;

import it.unibo.disi.pslab.smarthosp.vsserv.utils.Log;

public class HL7Client {
	private static final int HL7CLIENT_CONNECTION_TIMEOUT = 60000;
	
	private static final List<HL7Client> instances = new ArrayList<>();
	
	public static synchronized HL7Client instance(final String ip, final int port) throws HL7Exception {
		for(final HL7Client instance : instances) {
			if(instance.remoteIpAddress.equals(ip) && instance.remoteTcpPort == port) {
				return instance;
			}
		}
		
		final HL7Client instance = new HL7Client(ip, port);
		instances.add(instance);
		return instance;
	}
	
	private HapiContext context;
	private Connection connection;
	private Initiator initiator;
	
	private final int remoteTcpPort;
	private final String remoteIpAddress;
	
	private HL7Client(final String ip, final int port) throws HL7Exception {
		context = new DefaultHapiContext();
		
		connection = context.newLazyClient(ip, port, false);
		
		initiator = connection.getInitiator();
		initiator.setTimeout(HL7CLIENT_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
		
		remoteIpAddress = ip;
		remoteTcpPort = port;
	}

	public void terminate() {
		if (connection != null) {
			connection.close();
		}
	}

	public Message sendMessage(Message msg) {
		try {
			final Parser p = context.getPipeParser();
			
			Log.printDebug("QRY Message that will be sent:\n" + p.encode(msg));
			
			final Message response = initiator.sendAndReceive(msg);
			
			Log.printDebug("Received response:\n" + p.encode(response));
			
			return response;
		} catch (HL7Exception | LLPException | IOException  e) {
			Log.print("Unble to reach the gateway at " + this.remoteIpAddress + ":" + this.remoteTcpPort);
			return null;
		}
	}
}
