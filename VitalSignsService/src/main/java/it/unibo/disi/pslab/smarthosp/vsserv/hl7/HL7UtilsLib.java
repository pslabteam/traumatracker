package it.unibo.disi.pslab.smarthosp.vsserv.hl7;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v23.datatype.ST;
import ca.uhn.hl7v2.model.v23.group.ORF_R04_OBSERVATION;
import ca.uhn.hl7v2.model.v23.group.ORF_R04_QUERY_RESPONSE;
import ca.uhn.hl7v2.model.v23.message.ACK;
import ca.uhn.hl7v2.model.v23.message.ORF_R04;
import ca.uhn.hl7v2.model.v23.message.QRY_R02;
import ca.uhn.hl7v2.model.v23.segment.MSA;
import ca.uhn.hl7v2.model.v23.segment.MSH;
import ca.uhn.hl7v2.model.v23.segment.OBX;
import ca.uhn.hl7v2.model.v23.segment.QRD;
import ca.uhn.hl7v2.model.v23.segment.QRF;
import ca.uhn.hl7v2.util.idgenerator.NanoTimeGenerator;

public class HL7UtilsLib {

	/**
	 * Creates a QRY Message Request to be sent from the HL7Client to the HL7Server (gateway)
	 * to request vital signs for the specified monitor (identified by CareUnit and BedLabel).
	 * 
	 * @param careUnit The CareUnit of the monitor.
	 * @param bedLabel The BedLabel of the monitor.
	 * 
	 * @return The QRY Message (instance of QRY_R02 class).
	 */
	public static Message createQRYRequest(final String careUnit, final String bedLabel) {
		QRY_R02 qry = null;
		
		try {
			qry = new QRY_R02();
			qry.initQuickstart("QRY", "R02", "P");
			
			final MSH mshSegment = qry.getMSH();
			mshSegment.getVersionID().setValue("2.3");
			mshSegment.getReceivingApplication().getHd1_NamespaceID().setValue("INFINITY");
			mshSegment.getSendingApplication().getHd1_NamespaceID().setValue("Applicazione");
			
			final QRD qrd = qry.getQRD();
			qrd.getQueryDateTime().getTimeOfAnEvent().setValue(new GregorianCalendar());
			qrd.getQueryFormatCode().setValue("R");
			qrd.getQueryPriority().setValue("I");
			qrd.getQueryID().setValue(new NanoTimeGenerator().getID());
			qrd.insertWhatSubjectFilter(0);
			qrd.getWhatSubjectFilter()[0].getIdentifier().setValue("RES");
			
			final QRF qrf = qry.getQRF();
			qrf.insertWhereSubjectFilter(0);
			qrf.getWhereSubjectFilter()[0].setValue("MONITOR");
			qrf.insertOtherQRYSubjectFilter(0);
			
			final ST data = qrf.getOtherQRYSubjectFilter(0);
			data.setValue(careUnit);
			
			final ST subData = new ST(qrf.getMessage());
			subData.setValue(bedLabel);
			
			data.getExtraComponents().getComponent(1).setData(subData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return qry;
	}
	
	/**
	 * 
	 * 
	 * @param sign
	 * @param m
	 * 
	 * @return
	 */
	public static RequestResult processORFResponse(final Message m) {
		try {
			return processORF_R04((ORF_R04) m);
		} catch (Exception e) {
			e.printStackTrace();
			return processACK((ACK) m);
		}
	}
	
	private static RequestResult processORF_R04(final ORF_R04 orf) {

		final MSH msh = orf.getMSH();
		final MSA msa = orf.getMSA();
		
		RequestResult res = checkError(msa.getAcknowledgementCode().getValue(), msa.getTextMessage().getValue());
		
		res.setTimestamp(msh.getMsh7_DateTimeOfMessage().getTs1_TimeOfAnEvent().getValue());
		
		if (res.getStatus().equals(Status.SUCCESS_REQUEST)) {
			final ORF_R04_QUERY_RESPONSE qrSegment = orf.getQUERY_RESPONSE(0);
			
			res.setPid(qrSegment.getPATIENT().getPID().getPid3_PatientIDInternalID()[0].getCx1_ID().getValue());
			res.setPname(qrSegment.getPATIENT().getPID().getPid5_PatientName()[0].getXpn1_FamilyName().getValue());
			
			final List<OBX> obxSegments = new ArrayList<>();
			
			List<ORF_R04_OBSERVATION> observations = null;
			
			try {
				observations = qrSegment.getORDER().getOBSERVATIONAll();
			} catch (HL7Exception e) {
				e.printStackTrace();
			}
			
			for (int i = 0; i < observations.size(); i++) {
				obxSegments.add(observations.get(i).getOBX());
			}
			
			if (obxSegments.size() == 0) {
				// bed not found
				res.setStatus(Status.SUCCESS_BED_NOT_FOUND);
			} else {
				// bed found
				res = processVitalSigns(obxSegments, orf, res);
			}
		} 
		
		return res;
	}
	
	private static RequestResult processACK(final ACK ackMess) {
		final MSA msaSegment = ackMess.getMSA();
		return checkError(msaSegment.getAcknowledgementCode().getValue(), msaSegment.getTextMessage().getValue());
	}

	private static RequestResult checkError(final String type, final String mess) {
		final RequestResult result = new RequestResult();
		
		switch(type) {
			case "AA":
				result.setStatus(Status.SUCCESS_REQUEST);
				break;
				
			case "AE":
				result.setErrorMessage(Status.ERROR_APPLICATION_ERROR.description() + "Messaggio: " + mess + "\n");
				result.setStatus(Status.ERROR_APPLICATION_ERROR);
				break;
				
			case "AR":
				result.setErrorMessage(Status.ERROR_APPLICATION_REJECT.description() + "Messaggio: " + mess + "\n");
				result.setStatus(Status.ERROR_APPLICATION_REJECT);
				break;
		}
		
		return result;
	}
	
	/*
	private static RequestResult processParameter(final VitalSign sign, final List<OBX> observations, final ORF_R04 mess, final RequestResult result) {
		String measure = "0";
		
		String unitMeasure = sign.unitMeasure();
		
		for (int i = 0; i < observations.size(); i++) {
			final OBX obxSegment = observations.get(i);
			
			final String cod = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getValue();
			final String inst = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getExtraComponents().getComponent(0).getData().toString();
			
			if (cod != null && inst != null) {
				if (cod.equals(sign.code()) && inst.equals(sign.instance())) {
					measure = obxSegment.getObservationValue(0).getData().toString();
					// tengo solo i numeri
					measure = measure.replaceAll("[^\\.0123456789]", "");
					unitMeasure = obxSegment.getUnits().getCe1_Identifier().getValue();
					if (measure.equals("")) {
						//se il valore è vuoto viene 0 come se non fosse trovato
						measure = "0";
					}
				}
			}
		}
		
		result.setVitalSign(sign);
		result.setMeasure(measure);
		result.setUnitMeasure(unitMeasure);
		
		return result;
	}
	*/
	
	private static RequestResult processVitalSigns(final List<OBX> observations, final ORF_R04 mess, final RequestResult result) {		
		String measure = "";
		String unitMeasure = "";
		
		final VitalSign[] signs = VitalSign.values();
		
		List<String> codes = new ArrayList<>();
		List<String> instances = new ArrayList<>();
		List<String> um = new ArrayList<>();
		
		for (int i = 0; i < signs.length; i++) {
			codes.add(signs[i].code());
			instances.add(signs[i].instance());
			um.add(signs[i].uom());
		}
		
		int findCount = 0;

		List<Integer> findList = new ArrayList<>();
		
		for (int i = 0; i < instances.size(); i++) {
			findList.add(0);
		}
				
		List<String> toCheck = new ArrayList<String>();
		
		for (int i = 0; i < signs.length; i++) {
			toCheck.add(codes.get(i) + instances.get(i));
		}
		
		for (int i = 0; i < observations.size(); i++) {
			OBX obxSegment = observations.get(i);
			String code = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getValue();
			String instance = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getExtraComponents().getComponent(0).getData().toString();

			String check = code + instance;
			if (toCheck.contains(check)) {
				int index = toCheck.indexOf(check);
				findList.set(index, 1);
				findCount++;
				measure = obxSegment.getObservationValue(0).getData().toString();
				measure = measure.replaceAll("[^\\.0123456789]", "");
				unitMeasure = obxSegment.getUnits().getCe1_Identifier().getValue();

				if (measure.equals("")) {
					measure = "0";
				}

				result.setVitalSign(signs[toCheck.indexOf(check)], result.new VitalSignValue(measure, unitMeasure));
			}
		}

		if (findCount != instances.size()) {
			for (int i = 0; i < findList.size(); i++) {
				if (findList.get(i) == 0) {					
					result.setVitalSign(signs[i], result.new VitalSignValue("" + 0, um.get(i)));
				}
			}
		}

		return result;
	}
}
