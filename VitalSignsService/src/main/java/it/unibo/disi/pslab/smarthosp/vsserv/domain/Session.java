package it.unibo.disi.pslab.smarthosp.vsserv.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Session {
	
	private String sid;
	private String pid;
	private String pname;
	private int interval;
	private String status;
	private Measurement[] measurements;
	
	public Session(
			@JsonProperty(value = "sid", required = true) final String sid,
			@JsonProperty(value = "pid", required = true) final String pid,
			@JsonProperty(value = "pname", required = true) final String pname,
			@JsonProperty(value = "interval", required = true) final int interval) {
		this.sid = sid;
		this.pid = pid;
		this.pname = pname;
		this.interval = interval;
		this.status = Status.CREATED.toString();
		this.measurements = new Measurement[0];
	}
	
	public String getSid() {
		return sid;
	}
	
	public void setSid(final String sid) {
		this.sid = sid;
	}
	
	public String getPid() {
		return pid;
	}
	
	public void setPid(final String pid) {
		this.pid = pid;
	}
	
	public String getPname() {
		return pname;
	}
	
	public void setPname(final String pname) {
		this.pname = pname;
	}
	
	public int getInterval() {
		return interval;
	}

	public void setInterval(final int interval) {
		this.interval = interval;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(final String status) {
		this.status = status;
	}

	public Measurement[] getMeasurements() {		
		return measurements;		
	}

	public void setMeasurements(final Measurement[] measurements) {
		this.measurements = measurements;
	}
	
	public enum Status {
		CREATED("created"), STARTED("started"), STOPPED("stopped");
		
		private String label;
		
		Status(final String label) {
			this.label = label;
		}
		
		public String toString() {
			return this.label;
		}
	}
}