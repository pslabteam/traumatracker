package it.unibo.disi.pslab.smarthosp.vsserv.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO
 */
public class VitalSign {
	private String type;
	private String uom;
	private String value;

	public VitalSign(
			@JsonProperty(value = "type", required = true) final String type,
			@JsonProperty(value = "value", required = true) final String value,
			@JsonProperty(value = "uom", required = true) final String uom) {
		this.type = type;
		this.value = value;
		this.uom = uom;
	}

	public VitalSign() {
		this.type = "";
		this.value = "";
		this.uom = "";
	}

	public String getType() {
		return this.type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(final String uom) {
		this.uom = uom;
	}
}