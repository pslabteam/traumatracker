package it.unibo.disi.pslab.smarthosp.vsserv.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log {
	
	public static final boolean DEBUG_ACTIVE = false;
	public static final boolean LOG_ACTIVE = true;
	
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public static void print(final String msg) {
		if(LOG_ACTIVE) {
			log(msg, true);
		}
	}
	
	public static void printDebug(final String msg) {
		if(DEBUG_ACTIVE) {
			log(msg, true);
		}
	}
	
	private static void log(final String message, boolean datetime) {
		if(datetime) {
			System.out.println("[" + LocalDateTime.now().format(formatter) + "] " + message);
		} else {
			System.out.println(message);
		}
	}
}
