package it.unibo.disi.pslab.smarthosp.vsserv.domain;

public class Error {

	public int code;
	public String message;
	public String otherInfos;

	public Error(final int code, final String message, final String otherInfos) {
		this.code = code;
		this.message = message;
		this.otherInfos = otherInfos;
	}

	public int getCode() {
		return this.code;
	}

	public void setCode(final int code) {
		this.code = code;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getOtherInfos() {
		return this.otherInfos;
	}

	public void setOtherInfos(final String otherInfos) {
		this.otherInfos = otherInfos;
	}
}
