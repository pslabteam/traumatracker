package it.unibo.disi.pslab.smarthosp.vsserv.db.exceptions;

public class DbConnectionFailedException extends Exception {

	private static final long serialVersionUID = 7040584613246883088L;

	public DbConnectionFailedException() {
		super();
	}
}
