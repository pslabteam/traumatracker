package it.unibo.disi.pslab.smarthosp.vsserv.net;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VitalSignsRequestBody {
	
	private String report_id;
	private String vitalsign;
	
	public VitalSignsRequestBody(
			@JsonProperty(value = "report_id", required = true) String report_id,
			@JsonProperty(value = "vitalsign", required = true) String vitalsign) {
		this.report_id=report_id;
		this.vitalsign=vitalsign;
	}
	
	public String getReport_id() {
		return report_id;
	}
	
	public void setReport_id(String report_id) {
		this.report_id = report_id;
	}
	
	public String getVitalsign() {
		return vitalsign;
	}
	
	public void setVitalsign(String vitalsign) {
		this.vitalsign = vitalsign;
	}

}
