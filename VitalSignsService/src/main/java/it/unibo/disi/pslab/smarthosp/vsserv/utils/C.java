package it.unibo.disi.pslab.smarthosp.vsserv.utils;

public class C {
	
	public static final String SERVICE_VERSION = "1.0.0-20190709";
	
	public class service {
		public static final String BASIC_URL= "/gt2/vsservice";
		public static final String API_PREFIX = BASIC_URL + "/api";
		public static final String WEBROOT = "webroot";
		public static final String CONFIG_FILE_PATH = "./config.properties";
	}

	public class mongodb {
		public class collection {
			public static final String MONITORS = "monitors";
			public static final String SESSIONS = "sessions";
		}
		
		public class keys {
			public static final String PUSH = "$push";
			public static final String SET = "$set";
		}
	}
	
	public class http {
		public class header {
			public static final String CONTENT_TYPE = "content-type";
			
			public class values {
				public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json; charset=utf-8";
			}
		}
	}
	
	public class message {
		public class error {
			public static final String gatewayError = "A genering error has occurred while processing the request.";
			
			public static final String queryError = "An error has occured while querying the database.";
			
			public static final String monitorNotFound = "The requested monitor has not been found in the list of registered ones.";
			public static final String monitorAlreadyExists = "The requested monitor is already present into the list of registered ones.";
			
			public static final String sessionAlreadyExists = "Unable to create the session. Another with the same SID already exists.";
			public static final String sessionNotExists = "The requested session does not exists on the service.";
			public static final String sessionWrongState = "The current state of the involved session does not allow to performe requested operation.";
			
			public static final String vitalSignRequestNotValid = "The performed requeste has been rejected from the service.";
		}
	}
}
