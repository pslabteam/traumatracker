package it.unibo.disi.pslab.smarthosp.vsserv;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.WorkerExecutor;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;

import it.unibo.disi.pslab.smarthosp.vsserv.db.DB;
import it.unibo.disi.pslab.smarthosp.vsserv.db.Monitor;
import it.unibo.disi.pslab.smarthosp.vsserv.domain.Error;
import it.unibo.disi.pslab.smarthosp.vsserv.domain.Session;
import it.unibo.disi.pslab.smarthosp.vsserv.hl7.HL7Client;
import it.unibo.disi.pslab.smarthosp.vsserv.hl7.HL7UtilsLib;
import it.unibo.disi.pslab.smarthosp.vsserv.hl7.RequestResult;
import it.unibo.disi.pslab.smarthosp.vsserv.hl7.Status;
import it.unibo.disi.pslab.smarthosp.vsserv.net.MonitorRequestBody;
import it.unibo.disi.pslab.smarthosp.vsserv.net.SessionRequestBody;
import it.unibo.disi.pslab.smarthosp.vsserv.utils.C;
import it.unibo.disi.pslab.smarthosp.vsserv.utils.Log;
import it.unibo.disi.pslab.smarthosp.vsserv.utils.Pair;

public class VitalSignsServiceHandlers {
	private Vertx vertx;
	private MongoClient mongoClient;
	
	private Map<String, Pair<Long, WorkerExecutor>> activeSessions;

	private static final String EXECUTORS_POOL_NAME = "vsservice-pool";

	public VitalSignsServiceHandlers(final Vertx vertx, final MongoClient mongoClient) {
		this.vertx = vertx;
		this.mongoClient = mongoClient;
		
		activeSessions = new ConcurrentHashMap<>();
	}
	
	/**
	 * Handler for the "<ip:port>/gt2/vsservice/info" Service API
	 * @param rc
	 */
	public void serviceInfoHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		sendOK(rc, new JsonObject().put("version", C.SERVICE_VERSION));
	}

	/**
	 * Handler for the "GET VitalSigns from specific Monitor" Service API
	 * @param rc
	 */
	public void retrieveVitalSignsHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String monitorId = rc.request().getParam("monitor_id");
		
		try {
			final Monitor monitor = DB.monitors().get(monitorId);
			
			final WorkerExecutor executor = vertx.createSharedWorkerExecutor(EXECUTORS_POOL_NAME, DB.monitors().count());
						
			contactMonitorThroughGateway(executor, monitor, result -> {
				if (result.succeeded()) {
					Log.printDebug("VitalSigns received correctly from " + monitor.id());
					sendOK(rc, result.result());
				} else {
					Log.printDebug("VitalSigns not received from " + monitor.id() + ". Maybe the monitor is turned off or the gateway is unrechable!");
					sendError(rc, 500, result.cause().getMessage(), "");
				}
			});
		} catch(NoSuchElementException e) {
			sendError(rc, 400, C.message.error.monitorNotFound, "");
		}
	}
	
	/**
	 * 
	 * @param rc
	 */
	public void addMonitorRequesthandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final MonitorRequestBody requestBody = Json.decodeValue(rc.getBodyAsString(), MonitorRequestBody.class);

		final JsonObject query = new JsonObject().put("id", requestBody.getId());

		mongoClient.find(C.mongodb.collection.MONITORS, query, res -> {			
			if (res.succeeded() && res.result() != null) {
				if (res.result().size() > 0) {
					sendError(rc, 500, C.message.error.monitorAlreadyExists, "");
				} else {
					final JsonObject document = new JsonObject()
							.put("id", requestBody.getId())
							.put("ip_gateway", requestBody.getIp_gateway())
							.put("port_gateway", requestBody.getPort_gateway());

					mongoClient.insert(C.mongodb.collection.MONITORS, document, res2 -> {
						if (res2.succeeded()) {
							DB.monitors().add(requestBody.getId(), requestBody.getIp_gateway(), requestBody.getPort_gateway());
							sendOK(rc);
						} else {
							sendError(rc, 500, C.message.error.queryError, res2.cause().getMessage());
						}
					});
				}
			} else {
				sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
			}
		});
	}

	/**
	 * 
	 * @param rc
	 */
	public void getMonitorsRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		mongoClient.find(C.mongodb.collection.MONITORS,
				new JsonObject(), 
				res -> {
					if (res.succeeded() && res.result() != null) {
						List<JsonObject> result = res.result();
						result.forEach(obj -> obj.remove("_id"));
						
						sendOK(rc, result);
					} else {
						sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
					}
				}
		);
	}
	
	/**
	 * 
	 * @param rc
	 */
	public void getMonitorRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String id = rc.request().getParam("monitor_id");
		
		mongoClient.findOne(C.mongodb.collection.MONITORS, 
				new JsonObject().put("id", id), 
				null, 
				res -> {
					if (res.succeeded() && res.result() != null) {
						final JsonObject result = res.result();
						result.remove("_id");
						sendOK(rc, result);
					} else {
						sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
					}
				}
		);
	}

	/**
	 * 
	 * @param rc
	 */
	public void updateMonitorRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String id = rc.request().getParam("monitor_id");
		final MonitorRequestBody requestBody = Json.decodeValue(rc.getBodyAsString(), MonitorRequestBody.class);
		
		final JsonObject document = new JsonObject()
				.put("id", requestBody.getId())
				.put("ip_gateway", requestBody.getIp_gateway())
				.put("port_gateway", requestBody.getPort_gateway());
		
		mongoClient.updateCollection(C.mongodb.collection.MONITORS,
				new JsonObject().put("id", id), 
				new JsonObject().put(C.mongodb.keys.SET, document), 
				res -> {
					if (res.succeeded()) {
						DB.monitors().update(id, requestBody.getId(), requestBody.getIp_gateway(), requestBody.getPort_gateway());
						sendOK(rc);
					} else {
						sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
					}
				}
		);
	}

	/**
	 * 
	 * @param rc
	 */
	public void deleteMonitorRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String id = rc.request().getParam("monitor_id");
		final JsonObject query = new JsonObject().put("id", id);

		mongoClient.removeDocument(C.mongodb.collection.MONITORS, query, res -> {
			if (res.succeeded()) {
				DB.monitors().remove(id);
				sendOK(rc);
			} else {
				sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
			}
		});
	}

	/**
	 * 
	 * @param rc
	 */
	public void createSessionRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final SessionRequestBody requestBody = Json.decodeValue(rc.getBodyAsString(), SessionRequestBody.class);
		
		mongoClient.findOne(C.mongodb.collection.SESSIONS, new JsonObject().put("sid", requestBody.sid()), null, res -> {
			if (res.succeeded()) {
				if (res.result() != null) {
					sendError(rc, 404, C.message.error.sessionAlreadyExists, "");
				} else {
					final Session session = new Session(requestBody.sid(), requestBody.pid(), requestBody.pname(), requestBody.interval());
					
					mongoClient.insert(C.mongodb.collection.SESSIONS, new JsonObject(Json.encodePrettily(session)), res2 -> {
						if (res2.succeeded()) {
							Log.print("Session '" + session.getSid() + "' registered for patient " + session.getPid() + " | " + session.getPname());
							sendOK(rc);
						} else {
							sendError(rc, 500, C.message.error.queryError, res2.cause().getMessage());
						}
					});
				}
			} else {
				sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
			}
		});
	}
	
	/**
	 * 
	 * @param rc
	 */
	public void startSessionRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String sessionId = rc.request().getParam("session_id");
		
		mongoClient.findOne(C.mongodb.collection.SESSIONS, new JsonObject().put("sid", sessionId), null, res -> {
			if (res.succeeded()) {
				if (res.result() != null) {
					final Session session = Json.decodeValue(res.result().toString(), Session.class);
					
					if(session.getStatus().equals(Session.Status.CREATED.toString())) {
						mongoClient.updateCollection(C.mongodb.collection.SESSIONS,
								new JsonObject().put("sid", session.getSid()),
								new JsonObject().put(C.mongodb.keys.SET, new JsonObject().put("status", Session.Status.STARTED.toString())), 
								res3 -> {
									if (res3.succeeded()) {
										startSession(session);
										sendOK(rc);
									} else {
										sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
									}
								});
					} else {
						sendError(rc, 404, C.message.error.sessionWrongState, "");
					}
				} else {
					sendError(rc, 404, C.message.error.sessionNotExists, "");
				}
			} else {
				sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
			}
		});
	}

	/**
	 * 
	 * @param rc
	 */
	public void stopSessionRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String sessionId = rc.request().getParam("session_id");

		mongoClient.findOne(C.mongodb.collection.SESSIONS, new JsonObject().put("sid", sessionId), null, res -> {
			if (res.succeeded()) {
				if (res.result() != null) {
					final Session session = Json.decodeValue(res.result().toString(), Session.class);
					
					if(session.getStatus().equals(Session.Status.STARTED.toString())) {
						mongoClient.updateCollection(C.mongodb.collection.SESSIONS,
								new JsonObject().put("sid", session.getSid()),
								new JsonObject().put(C.mongodb.keys.SET, new JsonObject().put("status", Session.Status.STOPPED.toString())), 
								res3 -> {
									if (res3.succeeded()) {
										stopSession(session);
										sendOK(rc);
									} else {
										sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
									}
								});
					} else {
						sendError(rc, 404, C.message.error.sessionWrongState, "");
					}
				} else {
					sendError(rc, 404, C.message.error.sessionNotExists, "");
				}
			} else {
				sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
			}
		});
	}
	
	/**
	 * 
	 * @param rc
	 */
	public void deleteSessionRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String sessionId = rc.request().getParam("session_id");
		
		
		mongoClient.removeDocument(C.mongodb.collection.SESSIONS, 
				new JsonObject().put("sid", sessionId), 
				res -> {
					if (res.succeeded()) {
						sendOK(rc);
					} else {
						sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
					}
				});
	}
	
	/**
	 * 
	 * @param rc
	 */
	public void retrieveSessionRequestHandler(final RoutingContext rc) {
		logHandlerRequest(rc.request());
		
		final String sessionId = rc.request().getParam("session_id");
		
		mongoClient.findOne(C.mongodb.collection.SESSIONS, 
				new JsonObject().put("sid", sessionId), 
				null, res -> {
					if (res.succeeded() && res.result() != null) {
						final JsonObject result = res.result();
						result.remove("_id");
						sendOK(rc, result);
					} else {
						sendError(rc, 500, C.message.error.queryError, res.cause().getMessage());
					}
				});
	}
	
	//----- [SUPPORT FUNCTIONS] -----
	
	 private void contactMonitorThroughGateway(final WorkerExecutor executor, final Monitor monitor, final Handler<AsyncResult<Object>> result) {
		executor.executeBlocking(blockingCodeHandler -> {
			Log.printDebug(String.format("Attempting to reach monitor %s through gateway @ %s:%d", monitor.id(), monitor.gwAddress(), monitor.gwPort()));
			
			final Message requestMsg = HL7UtilsLib.createQRYRequest(
						Monitor.Utils.getCareUnitFromId(monitor.id()),
						Monitor.Utils.getBedLabelFromId(monitor.id()));
				
			Message responseMsg = null;
				
			try {
				responseMsg = HL7Client.instance(monitor.gwAddress(), monitor.gwPort()).sendMessage(requestMsg);
			} catch (HL7Exception e1) {
				blockingCodeHandler.fail(C.message.error.vitalSignRequestNotValid);
				return;
			}

			RequestResult monitorResult = null;
				
			if (responseMsg != null) {
				@SuppressWarnings("resource")
				final Parser p = new DefaultHapiContext().getPipeParser();
					
				try {
					monitorResult = HL7UtilsLib.processORFResponse(responseMsg);
					
					monitorResult.setSentMessage(p.encode(requestMsg));
					monitorResult.setReceivedMessage(p.encode(responseMsg));
				} catch (HL7Exception e) {
					e.printStackTrace();
				}
			}

			if (monitorResult.getStatus().equals(Status.SUCCESS_REQUEST)) {					
				blockingCodeHandler.complete(new JsonObject()
						.put("pid", monitorResult.getPid())
						.put("pname", monitorResult.getPname())
						.put("timestamp", monitorResult.getTimestamp())
						.put("vitalsigns", monitorResult.getVitalSigns()));
			} else {
				blockingCodeHandler.fail(C.message.error.gatewayError + "\n" + monitorResult.getStatus().description());
			}
		}, resultHandler -> {
			result.handle(resultHandler);
		});
	}
	
	private void startSession(final Session session) {
		long sessionPeriodicId = vertx.setPeriodic(session.getInterval(), timerId -> {
			if(activeSessions.get(session.getSid()) == null){
				vertx.cancelTimer(timerId);
				Log.print("timer ID = "+ timerId + " canceled!");
				return;
			}
			
			mongoClient.findOne(C.mongodb.collection.SESSIONS, new JsonObject().put("sid", session.getSid()), null, res -> {
				if (res.succeeded() && res.result() != null) {
					final Session s = Json.decodeValue(res.result().toString(), Session.class);
					doSessionStep(s);
				} else {
					vertx.cancelTimer(timerId);
				}
			});
		});
		
		activeSessions.put(session.getSid(), new Pair<Long, WorkerExecutor>(sessionPeriodicId, vertx.createSharedWorkerExecutor(EXECUTORS_POOL_NAME, DB.monitors().count())));
		
		Log.print("Session \"" + session.getSid() + "\" started (internal id: " + sessionPeriodicId + "), period = " + session.getInterval());
	}

	private void stopSession(final Session session) {
		activeSessions.remove(session.getSid());
		
		Log.print("Session \"" + session.getSid() + "\" stopped.");
	}

	private void doSessionStep(final Session session) {
		final WorkerExecutor executor = activeSessions.get(session.getSid()).second();
		
		DB.monitors().getAll().stream().forEach(monitor -> {
			Log.printDebug("session step for " + monitor.id());
			
			contactMonitorThroughGateway(executor, monitor, result -> {
				if(result.result() != null) {
					final JsonObject resultObj = new JsonObject(result.result().toString());
					
					final String pid = resultObj.getString("pid");
					final String pname = resultObj.getString("pname");
					
					if(pid.equalsIgnoreCase(session.getPid()) || pname.equalsIgnoreCase(session.getPname())) {
						//is a valid result!
						final JsonObject measure = new JsonObject().put("measurements", new JsonObject()
									.put("timestamp", resultObj.getString("timestamp"))
									.put("source", monitor.id())
									.put("vitalsigns", resultObj.getJsonArray("vitalsigns")));
						
						mongoClient.updateCollection(C.mongodb.collection.SESSIONS,
								new JsonObject().put("sid", session.getSid()),
								new JsonObject().put(C.mongodb.keys.PUSH, measure),
								res2 -> {
									if (!res2.succeeded()) {
										System.out.println(res2.cause().getMessage());
									} 
								}
						); 
					}
				}
			});
		});
	}	
	
	private void sendOK(final RoutingContext rc, final Object response) {
		rc.response()
			.putHeader(C.http.header.CONTENT_TYPE, C.http.header.values.CONTENT_TYPE_APPLICATION_JSON)
			.end(Json.encodePrettily(response));
	}

	private void sendOK(final RoutingContext rc) {
		rc.response()
			.putHeader(C.http.header.CONTENT_TYPE, C.http.header.values.CONTENT_TYPE_APPLICATION_JSON)
			.end();
	}

	private void sendError(final RoutingContext rc, final int code, final String message, final String otherInfos) {
		rc.response()
			.setStatusCode(code)
			.putHeader(C.http.header.CONTENT_TYPE, C.http.header.values.CONTENT_TYPE_APPLICATION_JSON)
			.end(Json.encodePrettily(new Error(code, message, otherInfos)));
	}
	
	private void logHandlerRequest(HttpServerRequest request) {
		Log.print("Request for \"" + request.uri() + "\""
			+ " from " + (request.remoteAddress().host().equals("0:0:0:0:0:0:0:1") ? "localhost" : request.remoteAddress().host()));
	}
}
