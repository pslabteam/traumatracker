package it.unibo.disi.pslab.smarthosp.vsserv.db;

public class Monitor {
	private String id;
	private String gw_address;
	private int gw_port;
	
	public Monitor(final String id, final String gwAddress, final int gwPort) {
		this.id = id;
		this.gw_address = gwAddress;
		this.gw_port = gwPort;
	}
	
	public String id() {
		return this.id;
	}
		
	public String gwAddress() {
		return this.gw_address;
	}
	
	public int gwPort() {
		return this.gw_port;
	}
	
	/**
	 * 
	 */
	public static class Utils {
		private static final String SEPARATOR = "_";
		
		public static String getCareUnitFromId(final String id) {
			return splitId(id, 0);
		}
		
		public static String getBedLabelFromId(final String id) {
			return splitId(id, 1);
		}
		
		private static String splitId(final String id, final int element) {
			if(id != null && element < 2) {
				final String[] data = id.split(SEPARATOR);
				
				if(data.length == 2) {
					return data[element];
				}
			}
			
			return "";
		}
	}
}
