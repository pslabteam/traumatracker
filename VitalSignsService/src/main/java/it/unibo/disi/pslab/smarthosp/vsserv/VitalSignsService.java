package it.unibo.disi.pslab.smarthosp.vsserv;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

import it.unibo.disi.pslab.smarthosp.vsserv.db.DB;
import it.unibo.disi.pslab.smarthosp.vsserv.utils.C;
import it.unibo.disi.pslab.smarthosp.vsserv.utils.ConfigFile;
import it.unibo.disi.pslab.smarthosp.vsserv.utils.Log;

public class VitalSignsService extends AbstractVerticle {
	
	public static void main(final String[] args) {
		Vertx.vertx().deployVerticle(new VitalSignsService());
	}
	
	
	
	

	private Router router;
	
	private VitalSignsServiceHandlers handlers;

	@Override
	public void start(final Future<Void> startingStatus) {
		handlers = new VitalSignsServiceHandlers(vertx, initializeDB());

		router = Router.router(vertx);

		final Set<String> allowHeaders = new HashSet<>();
		allowHeaders.add("x-requested-with");
		allowHeaders.add("Access-Control-Allow-Origin");
		allowHeaders.add("origin");
		allowHeaders.add("Content-Type");
		allowHeaders.add("accept");

		final Set<HttpMethod> allowMethods = new HashSet<>();
		allowMethods.add(HttpMethod.GET);
		allowMethods.add(HttpMethod.POST);
		allowMethods.add(HttpMethod.DELETE);
		allowMethods.add(HttpMethod.PUT);

		router.route().handler(CorsHandler.create("*").allowedHeaders(allowHeaders).allowedMethods(allowMethods));

		initializeAPIs();

		router.route().handler(StaticHandler.create().setWebRoot(C.service.WEBROOT).setCachingEnabled(false));
				
		vertx.executeBlocking(bch -> {
			bch.complete(vertx.createHttpServer());
		}, rh -> {
			final HttpServer server = (HttpServer) rh.result();
			final int port = config().getInteger("http.port", ConfigFile.getInt("port_gatewayservice"));
			
			startListening(server, port, res -> {
				if (res.succeeded()) {
					Log.print("The Web Service is active @ localhost:" + res.result().actualPort());
					Log.print("Service APIs:\n" + getServiceAPIs());
					startingStatus.complete();
				} else {
					Log.print("Warning: unable to start the Web Service!");
					startingStatus.fail(res.cause());
				}
			});
		});
	}
	
	
	private void startListening(final HttpServer server, final int port, final Handler<AsyncResult<HttpServer>> result) {
		vertx.executeBlocking(bch -> {
			bch.complete(server.requestHandler(router::accept));
		}, rh -> {
			server.listen(port, res -> {
				result.handle(res);
			});
		});
	}

 	private void initializeAPIs() {
 		router.get(C.service.BASIC_URL + "/info").handler(handlers::serviceInfoHandler);
 		
		router.get(C.service.API_PREFIX + "/monitors/:monitor_id/vitalsigns").handler(handlers::retrieveVitalSignsHandler);
		
		router.route(C.service.API_PREFIX + "/monitors*").handler(BodyHandler.create());
		router.get(C.service.API_PREFIX + "/monitors").handler(handlers::getMonitorsRequestHandler);
		router.get(C.service.API_PREFIX + "/monitors/:monitor_id").handler(handlers::getMonitorRequestHandler);
		router.post(C.service.API_PREFIX + "/monitors").handler(handlers::addMonitorRequesthandler);
		router.put(C.service.API_PREFIX + "/monitors/:monitor_id").handler(handlers::updateMonitorRequestHandler);
		router.delete(C.service.API_PREFIX + "/monitors/:monitor_id").handler(handlers::deleteMonitorRequestHandler);

		router.route(C.service.API_PREFIX + "/sessions*").handler(BodyHandler.create());
		router.post(C.service.API_PREFIX + "/sessions").handler(handlers::createSessionRequestHandler);
		router.get(C.service.API_PREFIX + "/sessions/:session_id/start").handler(handlers::startSessionRequestHandler);
		router.get(C.service.API_PREFIX + "/sessions/:session_id/stop").handler(handlers::stopSessionRequestHandler);
		router.get(C.service.API_PREFIX + "/sessions/:session_id").handler(handlers::retrieveSessionRequestHandler);
		router.delete(C.service.API_PREFIX + "/sessions/:session_id").handler(handlers::deleteSessionRequestHandler);
	}
	
 	private MongoClient initializeDB() {
 		final MongoClient mongoClient = MongoClient.createShared(vertx,
				new JsonObject().put("host", ConfigFile.getString("ipDB_mongo"))
						.put("port", ConfigFile.getInt("portDB_mongo"))
						.put("db_name", ConfigFile.getString("databaseDB_mongo")));
		
		vertx.executeBlocking(h -> {
			DB.initialize(mongoClient);
		}, null);
		
		return mongoClient;
	}
 	
	private String getServiceAPIs() {
		final StringBuilder apis = new StringBuilder();
		
		router.getRoutes().forEach(r -> { 
			try {
				final Field f = r.getClass().getDeclaredField("methods");
			    f.setAccessible(true);
			    
			    @SuppressWarnings("unchecked")
				final Set<HttpMethod> methods = (Set<HttpMethod>) f.get(r);
			    
			    apis.append(r.getPath() == null || methods.toString().contentEquals("[]") 
			    		? "" 
			    		: " > " + methods.toString() + "\t" + r.getPath() + "\n");
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} 
		});
		
		return apis.toString();
	}
}
