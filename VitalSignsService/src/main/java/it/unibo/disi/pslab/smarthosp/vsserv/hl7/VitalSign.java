package it.unibo.disi.pslab.smarthosp.vsserv.hl7;

public enum VitalSign {
	PRESSURE_DIASTOLIC("8462-4", "LOINC", "SN", "1", "ART D", "mm(hg)", "DIA"),
	PRESSURE_SISTOLIC("8480-6", "LOINC", "SN", "1", "ART S", "mm(hg)", "SYS"),
	HEARTH_RATE("8867-4", "LOINC", "SN", "1", "HR", "/min", "HR"),
	SPO2("2710-2", "LOINC", "SN", "1", "SpO2", "%", "SpO2"),
	ETCO2("20824", "MIB/CEN", "SN", "2", "etCO2*", "mm(hg)", "EtCO2"),
	TEMPERATURE("8332-9", "LOINC", "SN", "4", "Ta", "cel", "Temp"),
	NBP_D("8496-2", "LOINC", "SN", "1", "NBP D", "mm(hg)", "NBP-D"),
	NBP_S("8508-4", "LOINC", "SN", "1", "NBP S", "mm(hg)", "NBP-S");

	private final String code, codeType, dataType, instance, label, uom, description;

	// code: campo "HL7/LOINC Code" o "MIB/CEN Code" nella tabella
	// codeType: indica se viene usato loinc o mib/cen
	// dataType: se SN (numeric) o ST (stringa)
	// instance: campo instance nella tabella
	// label: campo label nella tabella
	// uom: unit� di misura
	// description: stringa testuale che si vuole associare al parametro vitale (a scelta)

	private VitalSign(final String code, final String codeType, final String dataType, final String instance,
			final String label, final String uom, final String description) {
		this.code = code;
		this.codeType = codeType;
		this.dataType = dataType;
		this.instance = instance;
		this.label = label;
		this.uom = uom;
		this.description = description;
	}

	public String code() {
		return this.code;
	}

	public String codeType() {
		return this.codeType;
	}

	public String dataType() {
		return this.dataType;
	}

	public String instance() {
		return this.instance;
	}

	public String label() {
		return this.label;
	}

	public String uom() {
		return this.uom;
	}

	public String description() {
		return this.description;
	}

	public static boolean checkVitalSignByValue(final String description) {
		for (final VitalSign vs : values()) {
			if (vs.description().equals(description)) {
				return true;
			}
		}
		
		return false;
	}
}
