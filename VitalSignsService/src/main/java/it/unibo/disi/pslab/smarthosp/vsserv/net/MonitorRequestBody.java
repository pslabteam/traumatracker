package it.unibo.disi.pslab.smarthosp.vsserv.net;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MonitorRequestBody {

	private String id;
	private String ip_gateway;
	private int port_gateway;

	public MonitorRequestBody(@JsonProperty(value = "id", required = true) String id,
			@JsonProperty(value = "ip_gateway", required = true) String ip_gateway,
			@JsonProperty(value = "port_gateway", required = true) int port_gateway) {
		this.id=id;
		this.ip_gateway = ip_gateway;
		this.port_gateway = port_gateway;
	}	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	public String getIp_gateway() {
		return ip_gateway;
	}

	public void setIp_gateway(String ip_gateway) {
		this.ip_gateway = ip_gateway;
	}

	public int getPort_gateway() {
		return port_gateway;
	}

	public void setPort_gateway(int port_gateway) {
		this.port_gateway = port_gateway;
	}

}
