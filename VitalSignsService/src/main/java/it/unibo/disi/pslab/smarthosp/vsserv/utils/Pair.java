package it.unibo.disi.pslab.smarthosp.vsserv.utils;

public class Pair<T1 extends Object, T2 extends Object> {
	
	private T1 first;
	private T2 second;
	
	public Pair(final T1 first, final T2 second) {
		this.first = first;
		this.second = second;
	}
	
	public T1 first() {
		return this.first;
	}
	
	public T2 second() {
		return this.second;
	}
}
