package it.unibo.disi.pslab.smarthosp.vsserv.hl7;

import java.util.HashMap;
import java.util.Map;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class RequestResult {
	private Status status;
	private String errorMessage;
	private String sentMessage, receivedMessage;
	private String pid, pname, timestamp;
	private Map<VitalSign, VitalSignValue> vitalsigns;
		
	public RequestResult() {
		this.errorMessage = "";
		
		this.sentMessage = "";
		this.receivedMessage = "";

		this.pid = "";
		this.pname = "";
		this.timestamp = "";

		vitalsigns = new HashMap<>();
	}
	
	public Status getStatus() {
		return this.status;
	}

	public void setStatus(final Status status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getSentMessage() {
		return this.sentMessage;
	}
	
	public void setSentMessage(final String sentMessage) {
		this.sentMessage = sentMessage;
	}

	public String getReceivedMessage() {
		return this.receivedMessage;
	}

	public void setReceivedMessage(final String receivedMessage) {
		this.receivedMessage = receivedMessage;
	}

	public String getPid() {
		return this.pid;
	}
	
	public void setPid(final String pid) {
		this.pid = pid;
	}
	
	public String getPname() {
		return this.pname;
	}
	
	public void setPname(final String pname) {
		this.pname = pname;
	}
	
	public String getTimestamp() {
		return this.timestamp;
	}
	
	public void setTimestamp(final String timestamp) {
		this.timestamp = timestamp;
	}
	
	public VitalSignValue getVitalSign(final VitalSign vs) {
		return vitalsigns.get(vs);
	}
	
	public void setVitalSign(final VitalSign vs, final VitalSignValue value) {
		vitalsigns.put(vs, value);
	}
	
	public JsonArray getVitalSigns(){
		final JsonArray vitalsignsArray = new JsonArray();
		
		vitalsigns.entrySet().forEach(e -> {
			vitalsignsArray.add(new JsonObject()
					.put("type", e.getKey().description())
					.put("value", e.getValue().value())
					.put("uom", e.getValue().uom()));
		});
		
		return vitalsignsArray;
	}
	
	public String toString() {
		final StringBuilder res = new StringBuilder();
		
		res.append("Result: ");
		
		vitalsigns.entrySet().forEach(e -> {
			res.append(e.getKey().description() + ": " + e.getValue().value() + " " + e.getValue().uom() + "\n");
		});
		
//		for (int i = 0; i < signs.size(); i++) {
//			res = res + this.signs.get(i).value() + ": " + this.measures.get(i) + " " + this.unitMeasures.get(i)
//					+ "\n";
//		}
		
		return res.toString();
	}

	/**
	 * 
	 * 
	 */
	public class VitalSignValue {
		private String value, uom;
		
		public VitalSignValue(final String value, final String uom) {
			this.value = value;
			this.uom = uom;
		}
		
		public String value() {
			return this.value;
		}
		
		public String uom() {
			return this.uom;
		}
	}
}
