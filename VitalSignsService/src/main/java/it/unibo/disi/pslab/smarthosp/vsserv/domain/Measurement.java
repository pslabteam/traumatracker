package it.unibo.disi.pslab.smarthosp.vsserv.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = Measurement.class)
public class Measurement {
	private String timestamp;
	private VitalSign[] vitalsigns;
	
	public Measurement(
			@JsonProperty(value = "timestamp", required = true) String timestamp,
			@JsonProperty(value = "vitalsigns", required = true) VitalSign[] vitalsigns) {
		this.timestamp = timestamp;
		this.vitalsigns = vitalsigns;
	}
	
	@JsonDeserialize(as = VitalSign[].class)
	public VitalSign[] getVitalsigns() {
		return vitalsigns;
	}
	
	@JsonDeserialize(as = VitalSign[].class)
	public void getVitalsigns(final VitalSign[] vitalsigns) {
		this.vitalsigns = vitalsigns;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(final String timestamp) {
		this.timestamp = timestamp;
	}

}
