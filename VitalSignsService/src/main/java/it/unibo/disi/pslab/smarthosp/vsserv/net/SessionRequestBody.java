package it.unibo.disi.pslab.smarthosp.vsserv.net;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SessionRequestBody {
	private String sid;
	private String pid;
	private String pname;
	private int interval;

	public SessionRequestBody(
			@JsonProperty(value = "sid", required = true) String sid,
			@JsonProperty(value = "pid", required = true) String pid,
			@JsonProperty(value = "pname", required = true) String pname,
			@JsonProperty(value = "interval", required = true) int interval) {
		this.sid = sid;
		this.pid = pid;
		this.pname = pname;
		this.interval = interval;
	}

	public String sid() {
		return sid;
	}

	public String pid() {
		return pid;
	}
	
	public String pname() {
		return pname;
	}

	public int interval() {
		return interval;
	}
}
