package it.unibo.disi.pslab.smarthosp.vsserv.utils;

import java.io.FileInputStream;
import java.util.Properties;

public class ConfigFile {
	/**
	 * Retrieve an Integer Property value from Config file
	 * @param param
	 * @return the value of the property
	 */
	public static int getInt(String param){
		return getProperty(Integer.class, param);
	}
	
	/**
	 * Retrieve a String Property value from Config file
	 * @param param
	 * @return the value of the property
	 */
	public static String getString(String param){
		return getProperty(String.class, param); 
	}
	
	/**
	 * Retrieve a Boolean Property value from Config file
	 * @param param
	 * @return the value of the property
	 */
	public static boolean getBoolean(String param){
		return getProperty(Boolean.class, param);
	}
	
	@SuppressWarnings("unchecked")
	private static <T> T getProperty(Class<T> type, String param) {
		final Properties properties = new Properties();
		
		T ret = null;
		
		try {
			final FileInputStream file = new FileInputStream(C.service.CONFIG_FILE_PATH);
			properties.load(file);
			file.close();
		} catch (Exception e) {
			return null;
		}
		
		if(type.equals(Boolean.class)) {
			ret = (T) new Boolean(Boolean.parseBoolean(properties.getProperty(param)));
		} else if(type.equals(Integer.class)) {
			ret = (T) new Integer(Integer.parseInt(properties.getProperty(param)));
		} else if(type.equals(String.class)) {
			ret = (T) new String(properties.getProperty(param));
		}
		
		return ret;
	}
}
