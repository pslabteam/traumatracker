package it.unibo.disi.pslab.smarthosp.vsserv.db;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import it.unibo.disi.pslab.smarthosp.vsserv.utils.C;

public class DB {

	private static MongoClient mongoClient;
	
	public static void initialize(final MongoClient client) {
		mongoClient = client;
		
		mongoClient.find(C.mongodb.collection.MONITORS, new JsonObject(), res -> {
			if (res.succeeded()) {
				for(JsonObject obj : res.result()) {
					Monitors.instance.monitors.add(new Monitor(obj.getString("id"), obj.getString("ip_gateway"), obj.getInteger("port_gateway")));
				}
			}
		});
	}
	
	public static Monitors monitors() {
		return Monitors.instance;
	}
	
	public static class Monitors {		
		private static Monitors instance = new Monitors();
		
		private Set<Monitor> monitors;
		
		private Monitors() {
			monitors = new HashSet<Monitor>();
		}
		
		public synchronized int count() {
			return monitors.size();
		}
		
		public synchronized Monitor get(final String id) {
			return monitors.stream().filter(m -> m.id().contentEquals(id)).findFirst().get();
		}
		
		public synchronized List<Monitor> getAll(){
			return new ArrayList<Monitor>(monitors);
		}
		
		public synchronized void add(final String id, final String gwAddress, final int gwPort) {
			monitors.add(new Monitor(id, gwAddress, gwPort));
		}
		
		public synchronized void update(final String prevId, final String id, final String gwAddress, final int gwPort) {
			monitors.removeIf(m -> m.id().contentEquals(prevId));
			monitors.add(new Monitor(id, gwAddress, gwPort));
		}
		
		public synchronized void remove(final String id) {
			monitors.removeIf(m -> m.id().contentEquals(id));
		}
	}
}
