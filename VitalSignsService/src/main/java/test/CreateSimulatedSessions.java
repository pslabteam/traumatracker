package test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class CreateSimulatedSessions {
	
	private final static SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private final static String FILE_NAME = "simulated-sessions.json";
	private final static int N_SESSIONS = 10;
	private final static int SESSION_MEASUREMENTS = 300;
	private final static int SESSION_INTERVAL = 10000;
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		try {
			final PrintWriter printWriter = new PrintWriter(new FileWriter(FILE_NAME));
			
			final Date d = new Date();
			
			for(int i = 0; i < N_SESSIONS; i++) {
				printWriter.append(generateSession(i+1, d, SESSION_MEASUREMENTS).encode()+"\n");
				d.setDate(d.getDate() + 1);
			}
			
			printWriter.close();
		} catch (IOException e) {
			System.out.println("Errors in file generation...");
			return;
		}
		
		System.out.println("File 'sessions.json' generated in project root!");
	}

	private static JsonObject generateSession(final int progressive, final Date start, final int numMeasurements) {
		final JsonObject session = new JsonObject();
		
		session.put("_id", "" + (System.currentTimeMillis() + progressive));
		session.put("sid", "sim_session_" + progressive);
		session.put("pid","pz1");
		session.put("pname","pz1");
		session.put("interval", SESSION_INTERVAL);
		session.put("status","stopped");
		session.put("measurements", genMeasurements(start, numMeasurements));
		
		return session;
	}
	
	private static JsonArray genMeasurements(final Date start, final int numMeasurements) {
		final JsonArray measurements = new JsonArray();
		
		Date timestamp = start;
		int duration = SESSION_INTERVAL;
		
		for(int i = 0; i < numMeasurements; i++) {
			final JsonObject measurement = new JsonObject();
						
			measurement.put("timestamp", df.format(timestamp));
			measurement.put("source", getSource(duration));
			measurement.put("vitalsigns", genVitalSigns());
					
			measurements.add(measurement);
			
			timestamp = new Date(timestamp.getTime() + duration);
			duration += SESSION_INTERVAL;
		}
		
		return measurements;
	}
	
	private static String getSource(final int duration) {
		
		if(duration < (1000*60*10)) {
			return "TRAUMAT_SHOCKR";
		} else if (duration < (1000*60*11)) {
			return "TRAUMAT_TTMW1";
		} else if (duration < (1000*60*19)) {
			return "TRAUMAT_TACPS";
		} else if (duration < (1000*60*24)) {
			return "TRAUMAT_TTMW1";
		} else if (duration < (1000*60*34)) {
			return "TRAUMAT_ANGIO1";
		} else if (duration < (1000*60*42)) {
			return "TRAUMAT_TTMW1";
		} else if (duration < (1000*60*52)) {
			return "TRAUMAT_SO6";
		}
		
		return "";
	}
	
	private static JsonArray genVitalSigns() {
		final JsonArray vs = new JsonArray();
		
		vs.add(new JsonObject()
				.put("type", "EtCO2")
				.put("value","" + random(20,50))
				.put("uom","mm(hg)"));
		
		vs.add(new JsonObject()
				.put("type", "NBP-S")
				.put("value","" + random(80,140))
				.put("uom","mm(hg)"));
		
		vs.add(new JsonObject()
				.put("type", "HR")
				.put("value","" + random(50,150))
				.put("uom","/min"));
		
		vs.add(new JsonObject()
				.put("type", "SYS")
				.put("value","" + random(80,140))
				.put("uom","mm(hg)"));
		
		vs.add(new JsonObject()
				.put("type", "Temp")
				.put("value","" + random(29,40))
				.put("uom","cel"));
		
		vs.add(new JsonObject()
				.put("type", "NBP-D")
				.put("value","" + random(60,90))
				.put("uom","mm(hg)"));
		
		vs.add(new JsonObject()
				.put("type", "SpO2")
				.put("value","" + random(60,100))
				.put("uom","%"));
		
		vs.add(new JsonObject()
				.put("type", "DIA")
				.put("value","" + random(60,90))
				.put("uom","mm(hg)"));
		
		return vs;
	}
	
	private static double random(int min, int max) {
		Random r = new Random();
		return Math.floor((min + (max - min) * r.nextDouble()) * 10) / 10;
	}
}
