package server_main;

import java.awt.EventQueue;

import org.apache.log4j.Logger;

import server.ServerWrapper;
import server_gui.GUI;
import server_gui.GUIListener;
import server_interfaces.IGUIListener;

public class ServerMain_GUI {

	public static void main(String[] args) {
		// disabilitare logging della libreria che � eccessivo
		Logger.getRootLogger().removeAllAppenders();

		// creo server wrapper
		ServerWrapper init=new ServerWrapper(2550);
		init.initialize();

		// listener GUI
		final IGUIListener listener = new GUIListener(init);

		// start GUI
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI(listener);
					listener.setGUI(frame);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}); 
		
	

	}

}
