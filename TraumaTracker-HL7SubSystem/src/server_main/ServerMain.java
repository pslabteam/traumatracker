package server_main;

import server.ServerWrapper;
import server_data.SimulationData;
import server_interfaces.ISimulationData;

public class ServerMain {

	public static void main(String[] args) {
		

		//oneServer();
		moreServers(3);

	}
	
	public static void oneServer(){
		ServerWrapper init=new ServerWrapper(2550);
		init.initialize();
		init.startServer(new SimulationData());
	}
	
	public static void moreServers(int n){
		int basePort=2550;
		for(int i=0;i<n;i++){
			ServerWrapper init=new ServerWrapper(basePort+i);
			init.initialize();
			ISimulationData simData=new SimulationData();
			if(i==1){
				simData.setCareUnitLabel("RE");	
			} else if(i==2){
				simData.setCareUnitLabel("RA");
			}
			init.startServer(simData);
		}
	}

}
