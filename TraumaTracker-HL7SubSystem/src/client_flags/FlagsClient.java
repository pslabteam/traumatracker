package client_flags;

public class FlagsClient {
	
	//indica se sto simulando o no
	public static boolean isSimulation=false;
	
	//numero pazienti
	public static int numPatients=10;

	
	public static String cuFake="CU1";
	public static String blFake="Bed1";
	public static String cu="TI";
	public static String bl="SALA1";
	
	public static String CU(){
		if(isSimulation==true){
			return cuFake;
		} else{
			return cu;
		}
	}
	
	public static String BL(){
		if(isSimulation==true){
			return blFake;
		} else{
			return bl;
		}
	}
	
	

}
