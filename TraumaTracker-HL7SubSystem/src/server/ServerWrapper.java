package server;


import org.apache.log4j.Logger;

import server_data.PatientsSimulator;
import server_gui.GUI;
import server_interfaces.IPatientsSimulator;
import server_interfaces.IServer;
import server_interfaces.IServerWrapper;
import server_interfaces.ISimulationData;

public class ServerWrapper implements IServerWrapper {
	private IServer server;
	private IPatientsSimulator sim;
	private int port;

	public ServerWrapper(int port) {
		this.port=port;
		// disabilitare logging della libreria che � eccessivo
		Logger.getRootLogger().removeAllAppenders();
	}

	public void initialize() {
		// creo server
		this.server = new Server(this.port);
	}

	public void startServer(ISimulationData simData) {
		// start server
		this.server.startServer(simData);
		// inizio simulazione pazienti
		this.sim = new PatientsSimulator(simData);
		sim.initPatients();
		sim.startSimulation();
		server.setSimulation(sim, null);
	}
	
	public void startServer(GUI gui,ISimulationData simData){
		// start server
		this.server.startServer(simData);
		// inizio simulazione pazienti
		this.sim = new PatientsSimulator(simData);
		sim.initPatients();
		sim.startSimulation();
		server.setSimulation(sim, gui);
	}
	
	//getter vari
	public IServer getServer(){
		return this.server;
	}
	
	public IPatientsSimulator getPatientsSimulator(){
		return this.sim;
	}

}
