package server;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.HL7Service;
//import ca.uhn.hl7v2.examples.ExampleReceiverApplication;
import server_gui.GUI;
import server_handlers.QRYHandler;
import server_interfaces.IPatientsSimulator;
import server_interfaces.IServer;
import server_interfaces.ISimulationData;
import server_listeners.MyConnectionListener;
import server_listeners.MyExceptionHandler;

public class Server implements IServer {
	private HL7Service server;
	private HapiContext context;
	private int port;
	private boolean useTls;

	public Server(int port) {
		this.context = new DefaultHapiContext();
		this.port = port;
		this.useTls = false;
	}

	// setup server
	public void startServer(ISimulationData simData){
		server = context.newServer(port, useTls);
		//handler generico che manda sempre ack: lo uso per gli ADT (exampleusage)
	/*	ReceivingApplication handler = new ExampleReceiverApplication();
		server.registerApplication("ADT", "A01", handler);
		server.registerApplication("ADT", "A01", handler); */
		//per essere notificati di nuove connessioni o connessioni chiuse
		server.registerConnectionListener(new MyConnectionListener());
		//notifiche di eccezioni
		server.setExceptionHandler(new MyExceptionHandler());
		//start del server e attesa messaggi
		try {
			server.startAndWait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// per stoppare il server
	public void stopServer() {
		server.stopAndWait();
	}

	// imposto la simulazione
	public void setSimulation(IPatientsSimulator sim, GUI gui) {
		QRYHandler handler2 = new QRYHandler(sim);
		handler2.setGUI(gui);
		server.registerApplication("QRY", "R02", handler2);
	}

}
