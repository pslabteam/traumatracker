package server_gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import server_data.SimulationData;
import server_interfaces.IGUIListener;
import server_interfaces.ISimulationData;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton startServerButton;
	private JTextPane textSent, textReceived;
	private JScrollPane scrollSent, scrollReceived;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JPanel panel;
	private JLabel lblNewLabel_2;
	private JTextField textField;
	private JLabel lblNewLabel_3;
	private JTextField textField_1;
	private JLabel lblNewLabel_4;
	private JTextField textField_2;

	/**
	 * Create the frame.
	 */
	public GUI(final IGUIListener listener) {
		setTitle("HL7 Server");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		// start del server
		this.startServerButton = new JButton("Start server");
		this.startServerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ISimulationData simData=new SimulationData();
				simData.setNumPatients(Integer.parseInt(textField_2.getText()));
				simData.setCareUnitLabel(textField.getText());
				simData.setBedUnitLabel_prefix(textField_1.getText());
				listener.startServer(simData);
				startServerButton.setEnabled(false);
			}
		});
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		lblNewLabel_4 = new JLabel("Numero pazienti");
		panel.add(lblNewLabel_4);
		
		textField_2 = new JTextField();
		panel.add(textField_2);
		textField_2.setColumns(10);
		textField_2.setText(""+10);
		
		lblNewLabel_2 = new JLabel("Care Unit Label");
		panel.add(lblNewLabel_2);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
		textField.setText("TI");
		
		lblNewLabel_3 = new JLabel("Care Unit Label (prefix)");
		panel.add(lblNewLabel_3);
		
		textField_1 = new JTextField();
		panel.add(textField_1);
		textField_1.setColumns(10);
		textField_1.setText("SALA");
		contentPane.add(this.startServerButton);

		lblNewLabel = new JLabel("Ultimo messaggio inviato");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel);

		// dove si scrive il messaggio ricevuto
		this.textSent = new JTextPane();
		this.scrollSent = new JScrollPane(this.textSent);
		contentPane.add(this.scrollSent);

		lblNewLabel_1 = new JLabel("Ultimo messaggio ricevuto");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel_1);

		// dove si scrive il messaggio inviato
		this.textReceived = new JTextPane();
		this.scrollReceived = new JScrollPane(this.textReceived);
		contentPane.add(this.scrollReceived);

	}
	
	public void setStartServerButtonEnabled(boolean enable) {
		this.startServerButton.setEnabled(enable);
	}

	public void setMessageSent(String text) {
		this.textSent.setText(text);
	}

	public void setMessageReceived(String text) {
		this.textReceived.setText(text);
	}
	

}
