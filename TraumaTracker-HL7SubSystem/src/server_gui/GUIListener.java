package server_gui;

import server_interfaces.IGUIListener;
import server_interfaces.IServerWrapper;
import server_interfaces.ISimulationData;

public class GUIListener implements IGUIListener {
	private IServerWrapper init;
	private GUI gui;

	public GUIListener(IServerWrapper init) {
		this.init = init;
	}

	// start server
	public void startServer(ISimulationData simData) {
		//start server
		init.startServer(gui,simData);
	}

	public void setGUI(GUI gui) {
		this.gui = gui;
	}

	
}
