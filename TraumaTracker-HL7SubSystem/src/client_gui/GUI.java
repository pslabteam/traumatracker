package client_gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import client_data.VitalSign;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.JTextPane;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerModel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.JTextField;

public class GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton connectButton, sendMessageButton;
	private JTextPane textSent, textReceived,textRequestedValue;
	private JScrollPane scrollSent, scrollReceived;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JSpinner spinner;
	private VitalSign[] signs;
	private String[] values;
	private JPanel panel;
	private JLabel lblNewLabel_3;
	private JTextField textField;
	private JLabel lblNewLabel_4;
	private JTextField textField_1;

	/**
	 * Create the frame.
	 */
	public GUI(final GUIListener listener) {

		setTitle("HL7 Client");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		// connessione al server
		this.connectButton = new JButton("Connetti al server");
		this.connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.connectListener();
			}
		});
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		lblNewLabel_3 = new JLabel("Care Unit Label");
		panel.add(lblNewLabel_3);
		
		textField = new JTextField();
		textField.setText("TI");
		panel.add(textField);
		textField.setColumns(10);
		
		lblNewLabel_4 = new JLabel("Bed Unit Label");
		panel.add(lblNewLabel_4);
		
		textField_1 = new JTextField();
		textField_1.setText("SALA1");
		panel.add(textField_1);
		textField_1.setColumns(10);
		contentPane.add(this.connectButton);

		// invio messaggio al server
		this.sendMessageButton = new JButton("Invia messaggio");
		this.sendMessageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.sendMsgListener(signs[getSelectedIndex(spinner.getValue().toString())],textField.getText(),textField_1.getText());
			}
		});
		this.sendMessageButton.setEnabled(false);
		contentPane.add(this.sendMessageButton);

		
		this.values= new String[VitalSign.values().length];
		this.signs=VitalSign.values();
		for(int i=0;i<this.signs.length;i++){
			this.values[i]=this.signs[i].Value();
		}
		SpinnerModel model = new SpinnerListModel(values);
		spinner = new JSpinner(model);
		contentPane.add(spinner);

		lblNewLabel = new JLabel("Ultimo messaggio inviato");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel);

		// dove si scrive il messaggio ricevuto
		this.textSent = new JTextPane();
		this.scrollSent=new JScrollPane(this.textSent);
		contentPane.add(this.scrollSent);

		lblNewLabel_1 = new JLabel("Ultimo messaggio ricevuto");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel_1);

		// dove si scrive il messaggio inviato
		this.textReceived = new JTextPane();
		this.scrollReceived=new JScrollPane(this.textReceived);
		contentPane.add(this.scrollReceived);
		
		lblNewLabel_2 = new JLabel("Valore richiesto paziente");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel_2);
		
		//dove si scrive il valore richiesto
		this.textRequestedValue = new JTextPane();
		contentPane.add(this.textRequestedValue);
	}

	public void setConnectButtonEnabled(boolean enable) {
		this.connectButton.setEnabled(enable);
	}

	public void setMessageButtonEnabled(boolean enable) {
		this.sendMessageButton.setEnabled(enable);
	}

	public void setMessageSent(String text) {
		this.textSent.setText(text);
	}

	public void setMessageReceived(String text) {
		this.textReceived.setText(text);
	}
	
	public void setRequestedValue(String text){
		this.textRequestedValue.setText(text);
	}
	
	
	private int getSelectedIndex(String value) {
		for(int i=0;i<values.length;i++){
			if(value.equals(values[i])){
				return i;
			}
		}
		return -1;
	}

}
