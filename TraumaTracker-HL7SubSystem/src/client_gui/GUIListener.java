package client_gui;

import client.ClientWrapper;
import client_data.ConnectionStatus;
import client_data.RequestResult;
import client_data.Status;
import client_data.VitalSign;

public class GUIListener {
	private ClientWrapper init;
	private GUI gui;
	private boolean isConnected;

	public GUIListener(ClientWrapper init) {
		this.isConnected = false;
		this.init = init;
	}

	// connessione al server
	public void connectListener() {
		ConnectionStatus connRes = init.connect();
		if (connRes.getStatus().equals(Status.SUCCESS_CONNECTION)) {
			// connessione avvenuta
			this.isConnected = true;
			gui.setMessageButtonEnabled(true);
			gui.setConnectButtonEnabled(false);
		} else {
			// errore nella connessione al server
			this.isConnected = false;
			System.out.println(connRes.getMessage());
			this.gui.setRequestedValue(connRes.getMessage());
			connectionHandler(connRes);
		}
	}

	// invio messaggio al server
	public void sendMsgListener(VitalSign sign,String bedUnitLabel,String careUnitLabel) {
		if (this.isConnected == true) {
			RequestResult result = new RequestResult();
			result = init.getVitalSigns(sign,bedUnitLabel,careUnitLabel);
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString() + "\n");
				this.gui.setRequestedValue(result.toString());
				this.gui.setMessageSent(result.getSentMessage());
				this.gui.setMessageReceived(result.getReceivedMessage());
			} else {
				// errore nella richiesta
				this.gui.setRequestedValue(result.getErrorMessage());
				this.gui.setMessageSent(result.getSentMessage());
				this.gui.setMessageReceived(result.getReceivedMessage());
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}
		} else {
			this.gui.setRequestedValue("Prima � necessario connettersi al server");
		}
	}

	public void setGUI(GUI gui) {
		this.gui = gui;
	}

	// RICHIESTE: per controllare gli eventuali errori e gestirli
	private void requestHandler(RequestResult result) {
		System.out.println("prova qui");
		if (result.getStatus().equals(Status.ERROR_APPLICATION_ERROR)) {
			System.out.println("TODO: application error");
		} else if (result.getStatus().equals(Status.ERROR_APPLICATION_REJECT)) {
			System.out.println("TODO: application reject");
		} else if (result.getStatus().equals(Status.ERROR_REQUEST)) {
			System.out.println("TODO: error request");
		} else if (result.getStatus().equals(Status.SUCCESS_BED_NOT_FOUND)) {
			System.out.println("TODO: bed not found");
		} else {
			System.out.println("TODO: else");
		}
	}

	// CONNESSIONE: per controllare gli eventuali errori e gestirli
	private void connectionHandler(ConnectionStatus result) {
		if (result.getStatus().equals(Status.ERROR_CONNECTION)) {
			System.out.println("TODO: error connection");
		} else {
			System.out.println("TODO: else");
		}
	}

}
