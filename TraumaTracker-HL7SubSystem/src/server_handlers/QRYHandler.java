package server_handlers;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Random;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Varies;
import ca.uhn.hl7v2.model.v23.datatype.CE;
import ca.uhn.hl7v2.model.v23.datatype.ST;
import ca.uhn.hl7v2.model.v23.group.ORF_R04_QUERY_RESPONSE;
import ca.uhn.hl7v2.model.v23.message.ORF_R04;
import ca.uhn.hl7v2.model.v23.message.QRY_R02;
import ca.uhn.hl7v2.model.v23.segment.MSA;
import ca.uhn.hl7v2.model.v23.segment.MSH;
import ca.uhn.hl7v2.model.v23.segment.OBR;
import ca.uhn.hl7v2.model.v23.segment.OBX;
import ca.uhn.hl7v2.model.v23.segment.PID;
import ca.uhn.hl7v2.model.v23.segment.QRD;
import ca.uhn.hl7v2.model.v23.segment.QRF;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import client_data.VitalSign;
import server_data.Patient;
import server_gui.GUI;
import server_interfaces.IPatient;
import server_interfaces.IPatientsSimulator;

public class QRYHandler implements ReceivingApplication {
	private DefaultHapiContext defaultHapiContext;
	private IPatientsSimulator sim;
	private GUI gui;
	private boolean firstMessage;

	public QRYHandler(IPatientsSimulator sim) {
		this.sim = sim;
		this.firstMessage=true;
	}

	@Override
	public boolean canProcess(Message arg0) {
		return true;
	}

	@Override
	public Message processMessage(Message theMessage, Map<String, Object> theMetadata) throws HL7Exception {
		
		
		
		
		/* TEST ERRORE SERVER 
		Patient p1=null;
		p1.getBedLabel();
		 TEST ERRORE SERVER */
		
		
		if (sim.getSimData().isUseLatency()== true) {
			
			int toWait=0;
			
			//latenza connessione
			if(firstMessage==true){
				
					Random r=new Random();
					int lat=r.nextInt(sim.getSimData().getConnectionLatencyMax()-sim.getSimData().getConnectionLatencyMin())+sim.getSimData().getConnectionLatencyMin();
					toWait=toWait+lat;
					System.out.println("lat "+lat);
					firstMessage=false;
				
			} else{
				
			}
			
			//latenza messaggio
			try {
				toWait=toWait+sim.getSimData().getMessageLatency();
				Thread.sleep(toWait);
			} catch (InterruptedException e) {
			}
		}
		

		defaultHapiContext = new DefaultHapiContext();
		String encodedMessage = defaultHapiContext.getPipeParser().encode(theMessage);
		System.out.println("Received message:\n" + encodedMessage + "\n\n");
		if (gui != null) {
			this.gui.setMessageReceived(encodedMessage);
		}
		// prendo il messaggio ricevuto
		QRY_R02 qry = (QRY_R02) theMessage;
		// recupero gli id del paziente dal messaggio
		QRF qrf = qry.getQRF();
		ST data = qrf.getOtherQRYSubjectFilter(0);
		String unitLabel = data.getValue();
		String bedLabel = data.getExtraComponents().getComponent(1).getData().toString();
		// prendo il paziente
		IPatient p = sim.getPatient(unitLabel, bedLabel);
		// c'� il paziente?

		 ORF_R04 orf=null;

		if (p == null) {
			p = new Patient(unitLabel, bedLabel);
			orf = getResponseMessage(qry, p, false);
		} else {
			orf = getResponseMessage(qry, p, true);
		} 
		


		return orf;
	}

	// creazione messaggio da inviare come risposta
	private ORF_R04 getResponseMessage(QRY_R02 qry, IPatient p, boolean foundPatient) {
		ORF_R04 orf = new ORF_R04();
		try {
			orf.initQuickstart("ORF", "R04", "P");

			composeMSH(orf, qry);
			composeMSA(orf, qry, p, foundPatient);
			composeQRD(orf, qry);
			composeQRF(orf, qry, p);

			if (foundPatient == true) {
				// ***QUERY RESPONSE SEGMENT***
				// nel manuale non � scritto, ma questi segmenti da qui in poi
				// vanno innestati dentro al query response segment
				ORF_R04_QUERY_RESPONSE qrSegment = orf.insertQUERY_RESPONSE(0);

				composePID(qrSegment, p);

				// ***PV1 SEGMENT***
				// TODO: non c'� nello standard ma il drager lo mette!
				composeOBR(orf, qrSegment);
				// inserimento dati vitali
				for (int i = 0; i < VitalSign.values().length - 1; i++) {
					composeVitalSign(VitalSign.values()[i], orf, qrSegment, p, i);
				}
			} else {
			}
			String encodedMessage = defaultHapiContext.getPipeParser().encode(orf);
			if (gui != null) {
				this.gui.setMessageSent(encodedMessage);
			}
			return orf;
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return null;
	}

	// ***MSH SEGMENT***
	private void composeMSH(ORF_R04 orf, QRY_R02 qry) throws DataTypeException {
		MSH mshSegment = orf.getMSH();
		mshSegment.getSendingApplication().getHd1_NamespaceID().setValue("INFINITY");
		// devi inserire il valore del "sendingapplication" di chi ti ha inviato
		// il messaggio
		mshSegment.getReceivingApplication().getHd1_NamespaceID()
				.setValue(qry.getMSH().getSendingApplication().getHd1_NamespaceID().getValue());
	}

	// ***MSA SEGMENT***
	private void composeMSA(ORF_R04 orf, QRY_R02 qry, IPatient p, boolean foundPatient) throws DataTypeException {

		MSA msaSegment = orf.getMSA();
		// se non setti lo stesso message control ID della richiesta nella
		// risposta non viene ricevuto
		msaSegment.getMessageControlID().setValue(qry.getMSH().getMessageControlID().getValue());
		msaSegment.getAcknowledgementCode().setValue("AA"); // AA: application
															// accept; AE:
															// application
															// error; AR:
															// application
															// reject
		if (foundPatient == true) {
			msaSegment.getTextMessage().setValue("[" + p.getUnitLabel() + "," + p.getBedLabel() + "]" + "Bed found"); // stringa
																														// predefinit
																														// da
																														// restituire
		} else {
			msaSegment.getTextMessage()
					.setValue("[" + p.getUnitLabel() + "," + p.getBedLabel() + "]" + "Bed not found"); // stringa
																										// predefinit
																										// da
																										// restituire
		}
	}

	// ***QRD SEGMENT***
	private void composeQRD(ORF_R04 orf, QRY_R02 qry) throws HL7Exception {
		// il query definition segment deve essere uguale a quello del messaggio
		// ricevuto
		QRD qrdSegment = orf.getQRD();
		qrdSegment.getQueryDateTime().getTs1_TimeOfAnEvent()
				.setValue(qry.getQRD().getQueryDateTime().getTs1_TimeOfAnEvent().getValue());
		qrdSegment.getQueryFormatCode().setValue(qry.getQRD().getQueryFormatCode().getValue());
		qrdSegment.getQueryPriority().setValue(qry.getQRD().getQueryPriority().getValue());
		qrdSegment.getQueryID().setValue(qry.getQRD().getQueryID().getValue());
		qrdSegment.insertWhatSubjectFilter(0);
		qrdSegment.getWhatSubjectFilter()[0].getIdentifier()
				.setValue(qry.getQRD().getWhatSubjectFilter()[0].getIdentifier().getValue());

	}

	// ***QRF SEGMENT***
	private void composeQRF(ORF_R04 orf, QRY_R02 qry, IPatient p) throws HL7Exception {
		// il query filter deve essere uguale a quello del meggaggio ricevuto
		QRF qrfSegment = orf.getQRF();
		qrfSegment.insertWhereSubjectFilter(0);
		qrfSegment.getWhereSubjectFilter()[0].setValue(qry.getQRF().getWhereSubjectFilter()[0].getValue());
		qrfSegment.insertOtherQRYSubjectFilter(0);
		ST data = qrfSegment.getOtherQRYSubjectFilter(0);
		data.setValue(p.getUnitLabel());
		ST subData = new ST(qrfSegment.getMessage());
		subData.setValue(p.getBedLabel());
		data.getExtraComponents().getComponent(1).setData(subData);

	}

	// ***PID SEGMENT***
	private void composePID(ORF_R04_QUERY_RESPONSE qrSegment, IPatient p) throws HL7Exception {
		PID pidSegment = qrSegment.getPATIENT().getPID();
		// TODO: id dice di metterlo nel campo 3 e lasciare vuoto il campo 18,
		// ma nel messaggio di esempio fa il contrario
		pidSegment.insertPatientIDInternalID(0);
		pidSegment.getPatientIDInternalID()[0].getCx1_ID().setValue("" + p.getID());
		pidSegment.getPatientIDInternalID()[0].getCx2_CheckDigit().setValue("AN");
		pidSegment.getPatientAccountNumber().getCx1_ID().setValue("" + p.getID());
		// lastname^firstname^middlename
		pidSegment.insertPatientName(0);
		pidSegment.getPatientName()[0].getXpn2_GivenName().setValue(p.getFirstName());
		pidSegment.getPatientName()[0].getFamilyName().setValue(p.getLastName());
		pidSegment.getPatientName()[0].getMiddleInitialOrName().setValue(p.getMiddleName());

	}

	// ***OBR SEGMENT***
	private void composeOBR(ORF_R04 orf, ORF_R04_QUERY_RESPONSE qrSegment) throws DataTypeException {
		OBR obrSegment = qrSegment.getORDER().getOBR();
		// stessa data del messaggio che invierai (data di quando l'informazione
		// viene presa)
		obrSegment.getObservationDateTime().getTimeOfAnEvent()
				.setValue(orf.getMSH().getDateTimeOfMessage().getTimeOfAnEvent().getValue());
	}

	private void composeVitalSign(VitalSign sign, ORF_R04 orf, ORF_R04_QUERY_RESPONSE qrSegment, IPatient p, int pos)
			throws HL7Exception {
		// ***OBX SEGMENT***
		// inseriscine quanti ne vuoi
		qrSegment.getORDER().insertOBSERVATION(pos);
		OBX obxSegment = qrSegment.getORDER().getOBSERVATION(pos).getOBX();
		// SN (structured numeric):numeric; ST: string (a seconda del valore
		// della misurazione)
		obxSegment.getValueType().setValue(sign.DataType());
		// identificatore osservazione
		obxSegment.getObservationIdentifier().getCe1_Identifier().setValue(sign.Label()); // Dr�ger�s
																							// label
																							// for
																							// the
																							// parameter
		obxSegment.getObservationIdentifier().getCe2_Text();
		obxSegment.getObservationIdentifier().getCe3_NameOfCodingSystem().setValue("local"); // sempre
																								// cos�
		obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().setValue(sign.Code()); // codice
																									// della
																									// pressione
																									// arteriosa
																									// (pulse
																									// rate)
																									// nella
																									// tabella
																									// LOINC
		ST extraSubcomponent = new ST(orf); // per la instance
		extraSubcomponent.setValue(sign.Instance());
		obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getExtraComponents().getComponent(0)
				.setData(extraSubcomponent);
		obxSegment.getObservationIdentifier().getCe5_AlternateText();
		obxSegment.getObservationIdentifier().getCe6_NameOfAlternateCodingSystem().setValue(sign.CodeType()); // o
																												// LOINC
																												// o
																												// MIB/CEN
																												// a
																												// seconda
																												// di
																												// che
																												// tabella
																												// usi
		// observation value
		CE ce = new CE(orf);
		// il prefisso va messo cosi se � numerico il valore, altrimenti non c'�
		String prefix = "";
		if (sign.DataType().equals("SN")) {
			prefix = "=";
		}
		ce.getCe1_Identifier().setValue(prefix);
		ce.getCe2_Text().setValue("" + p.getVitalSign(sign));
		Varies value = obxSegment.getObservationValue(0);
		value.setData(ce);
		// unit� di misura
		obxSegment.getUnits().getCe1_Identifier().setValue(sign.UnitMeasure()); // unit�
																				// di
																				// misura
		obxSegment.getUnits().getCe3_NameOfCodingSystem().setValue("ISO+"); // sempre
																			// cos�
		obxSegment.getObservResultStatus().setValue("R");
		// tempo osservazione
		GregorianCalendar now = new GregorianCalendar();
		obxSegment.getDateTimeOfTheObservation().getTimeOfAnEvent().setValue(now);
	}

	public void setGUI(GUI gui) {
		this.gui = gui;
	}

}
