package server_listeners;



import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.ConnectionListener;

public class MyConnectionListener implements ConnectionListener {
	
	public MyConnectionListener(){
	}

	@Override
	public void connectionReceived(Connection theC) {
		System.out.println("New connection received: " + theC.getRemoteAddress().getHostAddress());
		
	}

	@Override
	public void connectionDiscarded(Connection theC) {
		System.out.println("Lost connection from: " + theC.getRemoteAddress().getHostAddress());
	}

}
