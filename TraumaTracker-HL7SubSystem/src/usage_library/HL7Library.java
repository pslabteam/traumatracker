package usage_library;

import java.util.concurrent.Executors;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.parser.Parser;

public class HL7Library {
	private HapiContext context;
	private Parser parser;

	private HL7Parser messageParser;
	private HL7Printer messagePrinter;

	// costruttore: inizializzo i componenti della libreria
	public HL7Library() {
		context = new DefaultHapiContext();
		context.setExecutorService(Executors.newCachedThreadPool());
		parser = context.getGenericParser();
		init_MessageParser();
		init_MessagePrinter();
	}

	private void init_MessageParser() {
		this.messageParser = new HL7Parser(parser);
	}

	private void init_MessagePrinter() {
		this.messagePrinter = new HL7Printer(parser);
	}

	public HL7Parser getMessageParser() {
		return this.messageParser;
	}

	public HL7Printer getMessagePrinter() {
		return this.messagePrinter;
	}

}
