package usage_library;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;

public class HL7Printer {
	private Parser parser;

	public HL7Printer(Parser parser) {
		this.parser = parser;
	}

	public void printADTMessage(Message msg) {
		String encodedMessage = null;
		try {
			encodedMessage = parser.encode(msg);
		} catch (HL7Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Printing Message:");
		System.out.println(encodedMessage);
	}

}
