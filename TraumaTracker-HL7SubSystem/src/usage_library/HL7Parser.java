package usage_library;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v23.message.ADT_A01;
import ca.uhn.hl7v2.model.v23.segment.MSH;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.Parser;

/**
 * Example code for parsing messages.
 * 
 * @author <a href="mailto:jamesagnew@sourceforge.net">James Agnew</a>
 * @version $Revision: 1.1 $ updated on $Date: 2007-02-19 02:24:46 $ by $Author:
 *          jamesagnew $
 */
public class HL7Parser {
	// cambiano quando chiami parsemessage
	private String msg;
	private Parser parser;
	private MSH msh;
	private ADT_A01 adtMsg;

	public HL7Parser(Parser parser) {
			this.parser=parser;
	}

	// parsing di un messaggio
	public void parseMessage(String type, String msg) {
		this.msg = msg;
		// cambio in base al tipo di messaggio
		if (type.equals("ADT")) {
			ADTMessage();
		} // TODO: aggiungi gli altri tipi di messaggi
	}

	private void ADTMessage() {
		Message hapiMsg = null;
		try {
			// The parse method performs the actual parsing
			hapiMsg = parser.parse(msg);
		} catch (EncodingNotSupportedException e) {
			e.printStackTrace();
		} catch (HL7Exception e) {
			e.printStackTrace();
		}
		adtMsg = (ADT_A01) hapiMsg;
		msh = adtMsg.getMSH();
	}
	
	//recupero ultimo msh
	public MSH getMSHMsg(){
		return this.msh;
	}
	
	//recupero ultimo adt
	public ADT_A01 getADTMsg(){
		return this.adtMsg;
	}
	
	

	

}
