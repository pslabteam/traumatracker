package client_main;

import java.awt.EventQueue;

import org.apache.log4j.Logger;

import client.ClientWrapper;
import client_gui.GUI;
import client_gui.GUIListener;

public class ClientMain_GUI {

	public static void main(String[] args) {
		// disabilitare logging della libreria che � eccessivo
		Logger.getRootLogger().removeAllAppenders();
		
		
		//String ip="137.204.107.184";
		//String ip="191.1.1.76";
		String ip="localhost";
		int port=2550;

		// creo client wrapper		
		ClientWrapper init = new ClientWrapper(ip, port,10000);
		init.initialize();
		// listener GUI
		final GUIListener listener = new GUIListener(init);

		// start GUI
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI(listener);
					listener.setGUI(frame);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
