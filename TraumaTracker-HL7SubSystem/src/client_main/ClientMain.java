package client_main;


import client.ClientWrapper;
import client_data.ConnectionStatus;
import client_data.RequestResult;
import client_data.Status;
import client_flags.FlagsClient;

public class ClientMain {

	public static void main(String[] args) {
		
		//String ip="137.204.107.184";
		String ip="191.1.1.76";
		//String ip="localhost";
		int port=2550;
		
		//IClientWrapper init = new ClientWrapper("localhost", 2550,5000);
		ClientWrapper init = new ClientWrapper(ip, port,5000);
		init.initialize();
		ConnectionStatus connRes = init.connect();
		if (connRes.getStatus().equals(Status.SUCCESS_CONNECTION) == true) {
			// connessione avvenuta: richiesta dati al server
			RequestResult result = new RequestResult();

			/*
			 * NOTA: in alternativa puoi richiedere i dati in questo modo:
			 * result=init.getVitalSigns(VitalSign.PRESSURE_DIASTOLIC,culabel,blabel);
			 * senza dover aggiungere metodi a clientwrapper
			 */

			result = init.getHearthRate(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString());
				//puoi anche stampare i singoli parametri
				//result.getMeasure();
				//result.getUnitMeasure();
			} else {
				// errore nella richiesta
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}

			result = init.getPressureSistolic(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString());
				//puoi anche stampare i singoli parametri
				//result.getMeasure();
				//result.getUnitMeasure();
			} else {
				// errore nella richiesta
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}

			result = init.getHearthRate(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString());
				//puoi anche stampare i singoli parametri
				//result.getMeasure();
				//result.getUnitMeasure();
			} else {
				// errore nella richiesta
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}

			result = init.getSaturation(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString());
				//puoi anche stampare i singoli parametri
				//result.getMeasure();
				//result.getUnitMeasure();
			} else {
				// errore nella richiesta
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}

			result = init.getETC02(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString());
				//puoi anche stampare i singoli parametri
				//result.getMeasure();
				//result.getUnitMeasure();
			} else {
				// errore nella richiesta
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}

			result = init.getTemperature(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString());
				//puoi anche stampare i singoli parametri
				//result.getMeasure();
				//result.getUnitMeasure();
			} else {
				// errore nella richiesta
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}

			result = init.getVitalSigns(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// richiesta parametri vitali ha successo
				System.out.println(result.toString());
				//puoi anche accedere alle liste di valori
				//result.getMeasures();
				//result.getUnitMeasures();
			} else {
				// errore nella richiesta
				System.out.println(result.getErrorMessage() + "\n");
				requestHandler(result);
			}		

		} else {
			// errore nella connessione al server
			System.out.println(connRes.getMessage());
			connectionHandler(connRes);
		}

	}
	
	//RICHIESTE: per controllare gli eventuali errori e gestirli
	public static void requestHandler(RequestResult result){
		 if(result.getStatus().equals(Status.ERROR_APPLICATION_ERROR)){
			 System.out.println("TODO: application error");
		 } else if(result.getStatus().equals(Status.ERROR_APPLICATION_REJECT)){
			 System.out.println("TODO: application reject");
		 }else if(result.getStatus().equals(Status.ERROR_REQUEST)){
			 System.out.println("TODO: error request");
		 }else if(result.getStatus().equals(Status.SUCCESS_BED_NOT_FOUND)){
			 System.out.println("TODO: bed not found"); 
		 }else{
			 System.out.println("TODO: else");  
		 }
	}
	
	//CONNESSIONE: per controllare gli eventuali errori e gestirli
	public static void connectionHandler(ConnectionStatus result){
		 if(result.getStatus().equals(Status.ERROR_CONNECTION)){
			 System.out.println("TODO: error connection");
		 } else{
			 System.out.println("TODO: else");  
		 }
	}
}