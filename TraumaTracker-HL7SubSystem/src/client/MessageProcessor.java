package client;

import java.util.ArrayList;
import java.util.List;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v23.group.ORF_R04_OBSERVATION;
import ca.uhn.hl7v2.model.v23.group.ORF_R04_QUERY_RESPONSE;
import ca.uhn.hl7v2.model.v23.message.ACK;
import ca.uhn.hl7v2.model.v23.message.ORF_R04;
import ca.uhn.hl7v2.model.v23.segment.MSA;
import ca.uhn.hl7v2.model.v23.segment.OBX;
import client_data.RequestResult;
import client_data.Status;
import client_data.VitalSign;

public class MessageProcessor {
	

	// processing dei vari tipi di messaggi
	public RequestResult processMessage(VitalSign sign, Message m) {
		ORF_R04 mess=null;
		ACK ackMess=null;
		boolean isAck=false;
		try {
			mess = (ORF_R04) m;
			isAck=false;
		} catch (Exception e) {
			isAck=true;
			ackMess=(ACK) m;
			// non � ORF, probabilmente � ACK
		}
		if(isAck==false){
			// per prima cosa controllo l'MSA per vedere se non ci sono problemi
			MSA msaSegment = mess.getMSA();
			String type = msaSegment.getAcknowledgementCode().getValue();
			String message = msaSegment.getTextMessage().getValue();
			RequestResult res = checkError(type, message);
			// tutto ok
			if (res.getStatus().equals(Status.SUCCESS_REQUEST)) {
				// prendo le osservazioni nel messaggio (segmenti OBX)
				ORF_R04_QUERY_RESPONSE qrSegment = mess.getQUERY_RESPONSE(0);
				List<OBX> obxSegments = new ArrayList<>();
				List<ORF_R04_OBSERVATION> observations = null;
				try {
					observations = qrSegment.getORDER().getOBSERVATIONAll();
				} catch (HL7Exception e) {
					e.printStackTrace();
				}
				for (int i = 0; i < observations.size(); i++) {
					obxSegments.add(observations.get(i).getOBX());
				}
				//bed not found
				if(obxSegments.size()==0){
					res.setStatus(Status.SUCCESS_BED_NOT_FOUND);
				} else{
					//bed found
					// scelgo in base a quello che voglio
					if (sign.equals(VitalSign.VITAL_SIGNS)) {
						res = processVitalSigns(obxSegments, mess,res);
					} else {
						res = processParameter(sign, obxSegments, mess,res);
					}
				}
				return res;
			} else {
				// errore
				return res;
			}
		} else{
			MSA msaSegment = ackMess.getMSA();
			String type = msaSegment.getAcknowledgementCode().getValue();
			String message = msaSegment.getTextMessage().getValue();
			RequestResult res = checkError(type, message);
			return res;
		}

	}

	private RequestResult checkError(String type, String mess) {
		RequestResult result = new RequestResult();
		String ret = "";
		if (type.equals("AA")) {
			// tutto ok
			result.setStatus(Status.SUCCESS_REQUEST);
			return result;
		} else if (type.equals("AE")) {
			ret = ret + Status.ERROR_APPLICATION_ERROR.Description();
			ret = ret + "Messaggio: " + mess + "\n";
			result.setErrorMessage(ret);
			result.setStatus(Status.ERROR_APPLICATION_ERROR);
			return result;
		} else if (type.equals("AR")) {
			ret = ret + Status.ERROR_APPLICATION_REJECT.Description();
			ret = ret + "Messaggio: " + mess + "\n";
			result.setErrorMessage(ret);
			result.setStatus(Status.ERROR_APPLICATION_REJECT);
			return result;
		}
		return result;
	}

	private RequestResult processParameter(VitalSign sign, List<OBX> observations, ORF_R04 mess,RequestResult result) {
		String measure = "0";
		String unitMeasure = sign.UnitMeasure();
		for (int i = 0; i < observations.size(); i++) {
			OBX obxSegment = observations.get(i);
			String cod = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getValue();
			String inst = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getExtraComponents()
					.getComponent(0).getData().toString();
			if (cod != null && inst != null) {
				if (cod.equals(sign.Code()) && inst.equals(sign.Instance())) {
					measure = obxSegment.getObservationValue(0).getData().toString();
					// tengo solo i numeri
					measure = measure.replaceAll("[^\\.0123456789]", "");
					unitMeasure = obxSegment.getUnits().getCe1_Identifier().getValue();
					if(measure.equals("")){
						//cos� se il valore � vuoto viene 0 come se non fosse trovato
						measure="0";
					}
				}
			}
		}
		//setto risultati
		result.setVitalSign(sign);
		result.setMeasure(measure);
		result.setUnitMeasure(unitMeasure);
		return result;
	}

	// TODO: torna un array di requestresult caso in cui li restituisci tutti
	private RequestResult processVitalSigns(List<OBX> observations, ORF_R04 mess,RequestResult result) {
		List<VitalSign> vitalSigns=new ArrayList<>();
		List<String> measures=new ArrayList<>();
		List<String> unitMeasures=new ArrayList<>();
		String measure = "";
		String unitMeasure = "";
		VitalSign[] signs = VitalSign.values();
		List<String> codes = new ArrayList<>();
		for (int i = 0; i < signs.length; i++) {
			codes.add(signs[i].Code());
		}
		List<String> instances = new ArrayList<>();
		for (int i = 0; i < signs.length; i++) {
			instances.add(signs[i].Instance());
		}
		//per gestire gli elementi mancanti
		List<String> um= new ArrayList<>();
		for (int i = 0; i < signs.length; i++) {
			um.add(signs[i].UnitMeasure());
		}
		int findCount=0;
		
		List<Integer> findList=new ArrayList<>();
		for(int i=0;i<instances.size();i++){
			findList.add(0);
		}
		// concateno codice e instance per avere unicit�
		List<String> toCheck = new ArrayList<String>();
		for (int i = 0; i < signs.length; i++) {
			toCheck.add(codes.get(i) + instances.get(i));
		}
		for (int i = 0; i < observations.size(); i++) {
			OBX obxSegment = observations.get(i);
			String code = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getValue();
			String instance = obxSegment.getObservationIdentifier().getCe4_AlternateIdentifier().getExtraComponents()
					.getComponent(0).getData().toString();
			// controllo se code e instance corrispondono a uno
			String check = code + instance;
			if (toCheck.contains(check)) {
				int index=toCheck.indexOf(check);
				findList.set(index, 1);
				findCount++;
				measure = obxSegment.getObservationValue(0).getData().toString();
				measure = measure.replaceAll("[^\\.0123456789]", "");
				unitMeasure = obxSegment.getUnits().getCe1_Identifier().getValue();		
				
				if(measure.equals("")){
					//cos� se il valore � vuoto viene 0 come se non fosse trovato
					measure="0";
				}
				
				vitalSigns.add(signs[toCheck.indexOf(check)]);
				measures.add(measure);
				unitMeasures.add(unitMeasure);
			}
		}
		//se qualche valore non � stato trovato (-1 per escludere "tutti")
		if(findCount!=instances.size()){
			for(int i=0;i<findList.size()-1;i++){
				if(findList.get(i)==0){
					vitalSigns.add(signs[i]);
					measures.add(""+0);
					unitMeasures.add(um.get(i));
				}
			}
		}
		result.setVitalSigns(vitalSigns);
		result.setMeasures(measures);
		result.setUnitMeasures(unitMeasures);
		return result;
	}

}
