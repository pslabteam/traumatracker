package client;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import client_data.ConnectionStatus;
import client_data.Status;

public class Client {
	private HapiContext context;
	private int port;
	private String ip;
	private boolean useTls;
	private Connection connection;
	private Initiator initiator;
	private int timeout;
	private boolean log;

	public Client(int port, String ip, int timeout) {
		this.context = new DefaultHapiContext();
		this.port = port;
		this.useTls = false;
		this.timeout=timeout;
		this.ip = ip;
		this.log = true;
	}

	public ConnectionStatus startClient() {
		ConnectionStatus result=new ConnectionStatus();
		try {
			connection = context.newClient(ip, port, useTls);
			initiator=connection.getInitiator();
			initiator.setTimeout(timeout, TimeUnit.MILLISECONDS);
			result.setMessage(Status.SUCCESS_CONNECTION.Description());
			result.setStatus(Status.SUCCESS_CONNECTION);
		} catch (HL7Exception e) {
			result.setMessage(Status.ERROR_CONNECTION.Description());
			result.setStatus(Status.ERROR_CONNECTION);
			//e.printStackTrace(System.out);
			//e.printStackTrace();
		}
		return result;
	}

	// mandare un messaggio custom
	public Message sendMsg(String msg) {
		try {
			Parser p = context.getPipeParser();
			Message mess = p.parse(msg);
			// invio messaggio e risposta ricevuta
			Message response = initiator.sendAndReceive(mess);
			String responseString = p.encode(response);
			if(this.log==true){
				System.out.println("QRY Message to send:\n" + msg); 
				System.out.println("Received response:\n" + responseString);	
			}
			return response;
		} catch (HL7Exception e) {
			//e.printStackTrace();
		} catch (LLPException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return null;
	}
	
	
	public Message sendMsg(Message msg) {
		try {
			Parser p = context.getPipeParser();
			// invio messaggio e risposta ricevuta
			long t0=System.currentTimeMillis();
			Message response = initiator.sendAndReceive(msg);
			long t1=System.currentTimeMillis();
			//System.out.println("SENDANDRECEIVE "+(t1-t0));
			if(this.log==true){
				String responseString = p.encode(response);
				// print messaggi inviati e ricevuti
				this.context.getPipeParser();
				String sentString = null;
				try {
					sentString = p.encode(msg);
				} catch (HL7Exception e) {
					e.printStackTrace();
				}
				System.out.println("QRY Message to send:\n" + sentString); 
				System.out.println("Received response:\n" + responseString);	
			}
			return response;
		} catch (HL7Exception e) {
			//e.printStackTrace();
		} catch (LLPException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return null;
	}
	
	//chiusura connessione
	public void closeConnection(){
		if(connection!=null){
			connection.close();	
		}
	}
	
	//se hai chiuso e vuoi riaprire
	public void reinitializeClient(){
		try {
			connection = context.newClient(ip, port, useTls);
		} catch (HL7Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setLog(boolean log){
		this.log=log;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}



}
