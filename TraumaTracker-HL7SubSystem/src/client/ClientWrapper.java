package client;

import org.apache.log4j.Logger;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import client_data.ConnectionStatus;
import client_data.RequestResult;
import client_data.Status;
import client_data.VitalSign;

public class ClientWrapper {
	private Client client;
	private MessageProcessor processor;
	private QRYCreator qryCr;
	private DefaultHapiContext defaultHapiContext;
	private String ip;
	private int port;
	private int timeout;

	public ClientWrapper(String ip, int port,int timeout) {
		this.ip = ip;
		this.port = port;
		this.timeout=timeout;
		// disabilitare logging della libreria che � eccessivo
		Logger.getRootLogger().removeAllAppenders();
	}

	public void initialize() {

		this.defaultHapiContext = new DefaultHapiContext();
		// creo client e inizializzo i suoi componenti
		this.client = new Client(this.port, this.ip, timeout);
		this.processor = new MessageProcessor();
		this.qryCr = new QRYCreator();

	}

	public ConnectionStatus connect() {
		return this.client.startClient();
	}

	public RequestResult getPressureDiastolic(String careUnitLabel, String bedUnitLabel) {
		return getVitalSigns(VitalSign.PRESSURE_DIASTOLIC,careUnitLabel,bedUnitLabel);
	}

	public RequestResult getPressureSistolic(String careUnitLabel, String bedUnitLabel) {
		return getVitalSigns(VitalSign.PRESSURE_SISTOLIC,careUnitLabel,bedUnitLabel);
	}

	public RequestResult getHearthRate(String careUnitLabel, String bedUnitLabel) {
		return getVitalSigns(VitalSign.HEARTH_RATE,careUnitLabel,bedUnitLabel);
	}

	public RequestResult getETC02(String careUnitLabel, String bedUnitLabel) {
		return getVitalSigns(VitalSign.ETCO2,careUnitLabel,bedUnitLabel);

	}

	public RequestResult getSaturation(String careUnitLabel, String bedUnitLabel) {
		return getVitalSigns(VitalSign.SATURATION,careUnitLabel,bedUnitLabel);

	}

	public RequestResult getTemperature(String careUnitLabel, String bedUnitLabel) {
		return getVitalSigns(VitalSign.TEMPERATURE,careUnitLabel,bedUnitLabel);

	}

	public RequestResult getVitalSigns(String careUnitLabel, String bedUnitLabel) {
		return getVitalSigns(VitalSign.VITAL_SIGNS,careUnitLabel,bedUnitLabel);
	}
	
	public RequestResult getVitalSignsForPatient(VitalSign sign, String pid) {
		Message m = qryCr.createRequest(pid);
		Parser p = defaultHapiContext.getPipeParser();
		String toPrint = null;
		try {
			toPrint = p.encode(m);
		} catch (HL7Exception e) {
			e.printStackTrace();
		}
		// invio il messaggio al server
		Message response = client.sendMsg(m);
		if(response==null){
			//problemi
			RequestResult result = new RequestResult();
			result.setErrorMessage(Status.ERROR_REQUEST.Description());
			result.setStatus(Status.ERROR_REQUEST);
			result.setSentMessage(toPrint);
			return result;
		} else{
			//tutto ok: processing del messaggio
			RequestResult ret = this.processor.processMessage(sign, response);
			ret.setSentMessage(toPrint);
			try {
				ret.setReceivedMessage(p.encode(response));
			} catch (HL7Exception e) {
				e.printStackTrace();
			}
			return ret;
		}
	}

	public RequestResult getVitalSigns(VitalSign sign,String careUnitLabel, String bedUnitLabel) {
		// creo la richiesta da inviare al gateway
		Message m = qryCr.createRequest(careUnitLabel, bedUnitLabel);
		Parser p = defaultHapiContext.getPipeParser();
		
		String toPrint = null;
		
		try {
			toPrint = p.encode(m);
		} catch (HL7Exception e) {
			e.printStackTrace();
		}
		
		// invio il messaggio al server
		Message response = client.sendMsg(m);
		if(response==null){
			//problemi
			RequestResult result = new RequestResult();
			result.setErrorMessage(Status.ERROR_REQUEST.Description());
			result.setStatus(Status.ERROR_REQUEST);
			result.setSentMessage(toPrint);
			return result;
		} else{
			//tutto ok: processing del messaggio
			RequestResult ret = this.processor.processMessage(sign, response);
			ret.setSentMessage(toPrint);
			try {
				ret.setReceivedMessage(p.encode(response));
			} catch (HL7Exception e) {
				e.printStackTrace();
			}
			return ret;
		}
	}

	// getter vari
	public MessageProcessor getMessageProcessor() {
		return this.processor;
	}

	public QRYCreator getQRYCreator() {
		return this.qryCr;
	}

	public Client getClient() {
		return this.client;
	}
}
