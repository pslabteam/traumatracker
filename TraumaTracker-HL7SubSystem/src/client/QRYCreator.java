package client;

import java.util.GregorianCalendar;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v23.datatype.ST;
import ca.uhn.hl7v2.model.v23.message.QRY_R02;
import ca.uhn.hl7v2.model.v23.segment.MSH;
import ca.uhn.hl7v2.model.v23.segment.QRD;
import ca.uhn.hl7v2.model.v23.segment.QRF;
import ca.uhn.hl7v2.util.idgenerator.NanoTimeGenerator;

public class QRYCreator {
	//confrontati con questo per vedere se viene bene (richiesta)

	public QRYCreator() {
	}

	public Message createRequest(String careUnitLabel, String bedUnitLabel) {
		// creo messaggio e lo ritorno
		QRY_R02 qry = null;
		try {
			
			
			//seguendo il manuale
			GregorianCalendar now = new GregorianCalendar();
			qry = new QRY_R02();
			qry.initQuickstart("QRY", "R02", "P"); //tipo;triggering event;production/test
			MSH mshSegment = qry.getMSH();
			mshSegment.getVersionID().setValue("2.3");
			mshSegment.getReceivingApplication().getHd1_NamespaceID().setValue("INFINITY");
			mshSegment.getSendingApplication().getHd1_NamespaceID().setValue("Applicazione");
			QRD qrd = qry.getQRD();					
			qrd.getQueryDateTime().getTimeOfAnEvent().setValue(now);
			qrd.getQueryFormatCode().setValue("R");
			qrd.getQueryPriority().setValue("I");
			qrd.getQueryID().setValue(new NanoTimeGenerator().getID());
			qrd.insertWhatSubjectFilter(0);
			qrd.getWhatSubjectFilter()[0].getIdentifier().setValue("RES");
			QRF qrf=qry.getQRF();
			qrf.insertWhereSubjectFilter(0);
			qrf.getWhereSubjectFilter()[0].setValue("MONITOR");
			qrf.insertOtherQRYSubjectFilter(0);
			ST data = qrf.getOtherQRYSubjectFilter(0);
			data.setValue(careUnitLabel);
			ST subData = new ST(qrf.getMessage());
			subData.setValue(bedUnitLabel);
			data.getExtraComponents().getComponent(1).setData(subData);  
			
		/*	//MESSAGGIO CARMELO
			HapiContext context = new DefaultHapiContext();
			Parser p1 = context.getGenericParser();
			Message hapiMsg = null;
			try {
				// The parse method performs the actual parsing
				hapiMsg = p1.parse(ExampleMessages.test);
			} catch (Exception e) {
				e.printStackTrace();
			}
			qry = (QRY_R02) hapiMsg; */
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return qry;
	}
	
	public Message createRequest(String pid) {
		QRY_R02 qry = null;
		
		try {
			qry = new QRY_R02();
			qry.initQuickstart("QRY", "R02", "P"); //tipo;triggering event;production/test
			
			MSH mshSegment = qry.getMSH();
			mshSegment.getVersionID().setValue("2.3");
			mshSegment.getReceivingApplication().getHd1_NamespaceID().setValue("INFINITY");
			mshSegment.getSendingApplication().getHd1_NamespaceID().setValue("Applicazione");
			
			QRD qrd = qry.getQRD();					
			qrd.getQueryDateTime().getTimeOfAnEvent().setValue(new GregorianCalendar());
			qrd.getQueryFormatCode().setValue("R");
			qrd.getQueryPriority().setValue("I");
			qrd.getQueryID().setValue(new NanoTimeGenerator().getID());
			qrd.insertWhatSubjectFilter(0);
			qrd.getWhatSubjectFilter()[0].getIdentifier().setValue("RES");
			
			QRF qrf=qry.getQRF();
			qrf.insertWhereSubjectFilter(0);
			qrf.getWhereSubjectFilter()[0].setValue("MONITOR");
			qrf.insertOtherQRYSubjectFilter(0);
			ST data = qrf.getOtherQRYSubjectFilter(0);
			//data.setValue(careUnitLabel);
			ST subData = new ST(qrf.getMessage());
			//subData.setValue(bedUnitLabel);
			data.getExtraComponents().getComponent(1).setData(subData);  
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return qry;
	}

}
