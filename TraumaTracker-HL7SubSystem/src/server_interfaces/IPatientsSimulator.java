package server_interfaces;

import java.util.ArrayList;


public interface IPatientsSimulator {
	
	public void initPatients();

	public void startSimulation();

	public void stopSimulation();
	
	public IPatient getPatient(String unitLabel,String bedLabel);
	
	public ArrayList<IPatient> getPatients();

	public void setPatients(ArrayList<IPatient> patients);
	
	public ISimulationData getSimData();

	public void setSimData(ISimulationData simData);
	
}
