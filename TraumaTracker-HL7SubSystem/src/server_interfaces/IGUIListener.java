package server_interfaces;

import server_gui.GUI;

public interface IGUIListener {

	// connessione al server
	public void startServer(ISimulationData simData);
	
	public void setGUI(GUI gui);

}
