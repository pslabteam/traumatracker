package server_interfaces;

import server_gui.GUI;

public interface IServer {

	public void startServer(ISimulationData simData);

	public void stopServer();
	
	public void setSimulation(IPatientsSimulator sim,GUI gui);


}
