package server_interfaces;

import client_data.VitalSign;

public interface IPatient {
	
	//dati paziente
	public String getUnitLabel();
	
	public void setUnitLabel(String unitLabel);
	
	public String getBedLabel();
	
	public void setBedLabel(String bedLabel);
	
	public String getFirstName();
	
	public String getLastName();
	
	public String getMiddleName();
	
	public void setFirstName(String firstName);
	
	public void setLastName(String lastName);
	
	public void setMiddleName(String middleName);
	
	public void setID(int id);
	
	public int getID();
	
	//segnali vitali
	public int getPressureDiastolic();
	
	public int getPressureSistolic();
	
	public void setPressureDiastolic(int pressureDiastolic);
	
	public void setPressureSistolic(int pressureSistolic);
	
	public int getHearthRate();
	
	public void setHearthRate(int heartRate);
	
	public int getETCO2();
	
	public void setETCO2(int etco2);
	
	public int getSaturation();
	
	public void setSaturation(int saturation);
	
	public int getTemperature();
	
	public void setTemperature(int temperature);
	
	public int getVitalSign(VitalSign sign);
	

}
