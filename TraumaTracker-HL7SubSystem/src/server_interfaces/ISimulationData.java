package server_interfaces;


public interface ISimulationData {
	
	public int getMinPressureDiastolic();

	public void setMinPressureDiastolic(int minPressureDiastolic);

	public int getMaxPressureDiastolic();

	public void setMaxPressureDiastolic(int maxPressureDiastolic);

	public int getMinPressureSistolic();

	public void setMinPressureSistolic(int minPressureSistolic);

	public int getMaxPressureSistolic();

	public void setMaxPressureSistolic(int maxPressureSistolic);

	public int getMinHearthRate();

	public void setMinHearthRate(int minHearthRate);

	public int getMaxHearthRate();

	public void setMaxHearthRate(int maxHearthRate);

	public int getMinSaturation();

	public void setMinSaturation(int minSaturation);

	public int getMaxSaturation();

	public void setMaxSaturation(int maxSaturation);

	public int getMinETCO2();

	public void setMinETCO2(int minETCO2);

	public int getMaxETCO2();

	public void setMaxETCO2(int maxETCO2);

	public int getMinTemperature();

	public void setMinTemperature(int minTemperature);

	public int getMaxTemperature();

	public void setMaxTemperature(int maxTemperature);

	public int getUpdateSeconds();

	public void setUpdateSeconds(int updateSeconds);

	public String[] getFirstNames();

	public void setFirstNames(String[] firstNames);

	public String[] getMiddleNames();

	public void setMiddleNames(String[] middleNames);

	public String[] getLastNames();

	public void setLastNames(String[] lastNames);

	public boolean isUseLatency();

	public void setUseLatency(boolean useLatency);

	public int getConnectionLatencyMax();

	public void setConnectionLatencyMax(int connectionLatencyMax);

	public int getConnectionLatencyMin();

	public void setConnectionLatencyMin(int connectionLatencyMin);

	public int getMessageLatency();

	public void setMessageLatency(int messageLatency);
	
	public int getNumPatients();

	public void setNumPatients(int numPatients);

	public String getCareUnitLabel();

	public void setCareUnitLabel(String careUnitLabel);

	public String getBedUnitLabel_prefix();

	public void setBedUnitLabel_prefix(String bedUnitLabel_prefix);

}
