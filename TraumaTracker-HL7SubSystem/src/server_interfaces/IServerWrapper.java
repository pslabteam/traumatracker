package server_interfaces;

import server_gui.GUI;

public interface IServerWrapper {
	
	public void initialize();

	public void startServer(ISimulationData simData);
	
	public void startServer(GUI gui,ISimulationData simData);
	
	//getter vari
	public IServer getServer();
	
	public IPatientsSimulator getPatientsSimulator();

}
