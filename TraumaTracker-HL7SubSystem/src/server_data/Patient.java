package server_data;

import java.util.concurrent.ThreadLocalRandom;

import client_data.VitalSign;
import server_interfaces.IPatient;

public class Patient implements IPatient {
	private String unitLabel;
	private String bedLabel;
	private String firstName;
	private String lastName;
	private String middleName;
	private int pressureDiastolic, pressureSistolic, temperature, saturation, etco2, heartRate;
	private int id;

	public Patient(String unitLabel, String bedLabel) {
		this.unitLabel = unitLabel;
		this.bedLabel = bedLabel;
	}

	@Override
	public String getUnitLabel() {
		return this.unitLabel;
	}

	@Override
	public String getBedLabel() {
		return this.bedLabel;
	}

	@Override
	public void setUnitLabel(String unitLabel) {
		this.unitLabel = unitLabel;
	}

	@Override
	public void setBedLabel(String bedLabel) {
		this.bedLabel = bedLabel;
	}

	@Override
	public String getFirstName() {
		return this.firstName;
	}

	@Override
	public String getLastName() {
		return this.lastName;
	}

	@Override
	public String getMiddleName() {
		return this.middleName;
	}

	@Override
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Override
	public void setID(int id) {
		this.id = id;
	}

	@Override
	public int getID() {
		return this.id;
	}

	@Override
	public int getPressureDiastolic() {
		return this.pressureDiastolic;
	}

	@Override
	public int getPressureSistolic() {
		return this.pressureSistolic;
	}

	@Override
	public void setPressureDiastolic(int pressureDiastolic) {
		this.pressureDiastolic = pressureDiastolic;
	}

	@Override
	public void setPressureSistolic(int pressureSistolic) {
		this.pressureSistolic = pressureSistolic;
	}

	@Override
	public int getHearthRate() {
		return this.heartRate;
	}

	@Override
	public void setHearthRate(int heartRate) {
		this.heartRate = heartRate;
	}

	@Override
	public int getETCO2() {
		return this.etco2;
	}

	@Override
	public void setETCO2(int etco2) {
		this.etco2 = etco2;
	}

	@Override
	public int getSaturation() {
		return this.saturation;
	}

	@Override
	public void setSaturation(int saturation) {
		this.saturation = saturation;
	}

	@Override
	public int getTemperature() {
		return this.temperature;
	}

	@Override
	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	@Override
	public int getVitalSign(VitalSign sign) {
		if(sign.equals(VitalSign.PRESSURE_DIASTOLIC)){
			return getPressureDiastolic();
		} else if(sign.equals(VitalSign.PRESSURE_SISTOLIC)){
			return getPressureSistolic();
		} else if (sign.equals(VitalSign.ETCO2)){
			return getETCO2();
		}else if(sign.equals(VitalSign.HEARTH_RATE)){
			return getHearthRate();
		}else if(sign.equals(VitalSign.TEMPERATURE)){
			return getTemperature();
		}else if(sign.equals(VitalSign.SATURATION)){
			return getSaturation();
		}
		return ThreadLocalRandom.current().nextInt(50, 100);		
	}

}
