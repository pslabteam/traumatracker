package server_data;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import server_interfaces.IPatient;
import server_interfaces.IPatientsSimulator;
import server_interfaces.ISimulationData;

public class PatientsSimulator implements IPatientsSimulator {
	private ArrayList<IPatient> patients;
	private ScheduledExecutorService exec;
	private ScheduledFuture<?> scheduledFuture;
	private ISimulationData simData;

	public PatientsSimulator(ISimulationData simData) {
		this.patients = new ArrayList<>();
		this.exec=Executors.newSingleThreadScheduledExecutor();
		this.simData=simData;
	}
	
	public PatientsSimulator(){
		this.patients = new ArrayList<>();
		this.exec=Executors.newSingleThreadScheduledExecutor();
		this.simData=new SimulationData();
	}
	
	// creo i pazienti
	public void initPatients(){
		for (int i = 0; i < simData.getNumPatients(); i++) {
			IPatient p = new Patient(simData.getCareUnitLabel(), ""+simData.getBedUnitLabel_prefix() + (i + 1));
			String firstName=simData.getFirstNames()[ThreadLocalRandom.current().nextInt(0, simData.getFirstNames().length)];
			String lastName=simData.getLastNames()[ThreadLocalRandom.current().nextInt(0, simData.getLastNames().length)];
			String middleName=simData.getMiddleNames()[ThreadLocalRandom.current().nextInt(0, simData.getMiddleNames().length)];
			p.setFirstName(firstName);
			p.setLastName(lastName);
			p.setMiddleName(middleName);
			p.setPressureDiastolic(50);
			p.setPressureSistolic(50);
			p.setSaturation(50);
			p.setETCO2(50);
			p.setTemperature(50);
			p.setHearthRate(50);
			p.setID((i+1));
			patients.add(p);
		}
	}

	// start simulazione: ogni 6 secondi aggiorno i dati della pressione con dati a caso
	public void startSimulation() {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				System.out.println("Nuovi dati pazienti raccolti");
				for (int i = 0; i < patients.size(); i++) {
					//cambio dati paziente
					int pressureDiastolic=ThreadLocalRandom.current().nextInt(simData.getMinPressureDiastolic(), simData.getMaxPressureDiastolic()+1);
					patients.get(i).setPressureDiastolic(pressureDiastolic);
					int pressureSistolic=ThreadLocalRandom.current().nextInt(simData.getMinPressureSistolic(), simData.getMaxPressureSistolic()+1);
					patients.get(i).setPressureSistolic(pressureSistolic);
					int saturation=ThreadLocalRandom.current().nextInt(simData.getMinSaturation(), simData.getMaxSaturation()+1);
					patients.get(i).setSaturation(saturation);
					int co2=ThreadLocalRandom.current().nextInt(simData.getMinETCO2(), simData.getMaxETCO2()+1);
					patients.get(i).setETCO2(co2);
					int temperature=ThreadLocalRandom.current().nextInt(simData.getMinTemperature(), simData.getMaxTemperature()+1);
					patients.get(i).setTemperature(temperature);
					int hearthRate=ThreadLocalRandom.current().nextInt(simData.getMinHearthRate(), simData.getMaxHearthRate()+1);
					patients.get(i).setHearthRate(hearthRate);
				}
			}
		};
		scheduledFuture = exec.scheduleAtFixedRate(r, 0, simData.getUpdateSeconds(), TimeUnit.SECONDS);
	}

	//ferma la simulazione
	public void stopSimulation() {
		scheduledFuture.cancel(false);
	}
	
	public IPatient getPatient(String unitLabel,String bedLabel){
		for (int i = 0; i < patients.size(); i++) {
			IPatient p=patients.get(i);
			if(p.getBedLabel().equals(bedLabel) && p.getUnitLabel().equals(unitLabel)){
				return p;
			}
		}
		return null;
	}

	public ArrayList<IPatient> getPatients() {
		return patients;
	}

	public void setPatients(ArrayList<IPatient> patients) {
		this.patients = patients;
	}

	public ISimulationData getSimData() {
		return simData;
	}

	public void setSimData(ISimulationData simData) {
		this.simData = simData;
	}
	

}
