package server_data;

import server_interfaces.ISimulationData;

public class SimulationData implements ISimulationData {
	
	//dati principali
	private int numPatients;
	private String careUnitLabel;
	private String bedUnitLabel_prefix;

	// valori intervallo pressione
	private int minPressureDiastolic;
	private int maxPressureDiastolic;
	private int minPressureSistolic;
	private int maxPressureSistolic;
	private int minHearthRate;
	private int maxHearthRate;
	private int minSaturation;
	private int maxSaturation;
	private int minETCO2;
	private int maxETCO2;
	private int minTemperature;
	private int maxTemperature;

	// aggiornamento dati pazienti
	private int updateSeconds;
	// nomi, cognomi, e middle name random
	private String[] firstNames;
	private String[] middleNames;
	private String[] lastNames;
	
	//latenze
	private boolean useLatency;
	private int connectionLatencyMax;
	private int connectionLatencyMin;
	private int messageLatency;

	public SimulationData() {
		numPatients=10;
		careUnitLabel="TI";
		bedUnitLabel_prefix="SALA";
		minPressureDiastolic = 50;
		maxPressureDiastolic = 100;
		minPressureSistolic = 50;
		maxPressureSistolic = 100;
		minHearthRate = 50;
		maxHearthRate = 100;
		minSaturation = 50;
		maxSaturation = 100;
		minETCO2 = 50;
		maxETCO2 = 100;
		minTemperature = 50;
		maxTemperature = 100;
		updateSeconds = 6;
		firstNames = new String[]{ "Jack", "John", "Fred", "Alfred", "Jan", "Piero", "Mauro", "Junior", "Laura",
				"Lauren", "Alice", "Alicia" };
		middleNames = new String[]{ "J", "", "M", "K", "", "", "", "" };
		lastNames = new String[]{ "Red", "Green", "Purple", "Yellow", "White", "Orange", "Pink", "Rossi",
				"Verdi", "Violi", "Giallo", "Bianchi", "Arancioni", "Rosi" };
		useLatency=true;
		connectionLatencyMax=1500;
		connectionLatencyMin=1000;
		messageLatency=210;
	}

	public int getMinPressureDiastolic() {
		return minPressureDiastolic;
	}

	public void setMinPressureDiastolic(int minPressureDiastolic) {
		this.minPressureDiastolic = minPressureDiastolic;
	}

	public int getMaxPressureDiastolic() {
		return maxPressureDiastolic;
	}

	public void setMaxPressureDiastolic(int maxPressureDiastolic) {
		this.maxPressureDiastolic = maxPressureDiastolic;
	}

	public int getMinPressureSistolic() {
		return minPressureSistolic;
	}

	public void setMinPressureSistolic(int minPressureSistolic) {
		this.minPressureSistolic = minPressureSistolic;
	}

	public int getMaxPressureSistolic() {
		return maxPressureSistolic;
	}

	public void setMaxPressureSistolic(int maxPressureSistolic) {
		this.maxPressureSistolic = maxPressureSistolic;
	}

	public int getMinHearthRate() {
		return minHearthRate;
	}

	public void setMinHearthRate(int minHearthRate) {
		this.minHearthRate = minHearthRate;
	}

	public int getMaxHearthRate() {
		return maxHearthRate;
	}

	public void setMaxHearthRate(int maxHearthRate) {
		this.maxHearthRate = maxHearthRate;
	}

	public int getMinSaturation() {
		return minSaturation;
	}

	public void setMinSaturation(int minSaturation) {
		this.minSaturation = minSaturation;
	}

	public int getMaxSaturation() {
		return maxSaturation;
	}

	public void setMaxSaturation(int maxSaturation) {
		this.maxSaturation = maxSaturation;
	}

	public int getMinETCO2() {
		return minETCO2;
	}

	public void setMinETCO2(int minETCO2) {
		this.minETCO2 = minETCO2;
	}

	public int getMaxETCO2() {
		return maxETCO2;
	}

	public void setMaxETCO2(int maxETCO2) {
		this.maxETCO2 = maxETCO2;
	}

	public int getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(int minTemperature) {
		this.minTemperature = minTemperature;
	}

	public int getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(int maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	public int getUpdateSeconds() {
		return updateSeconds;
	}

	public void setUpdateSeconds(int updateSeconds) {
		this.updateSeconds = updateSeconds;
	}

	public String[] getFirstNames() {
		return firstNames;
	}

	public void setFirstNames(String[] firstNames) {
		this.firstNames = firstNames;
	}

	public String[] getMiddleNames() {
		return middleNames;
	}

	public void setMiddleNames(String[] middleNames) {
		this.middleNames = middleNames;
	}

	public String[] getLastNames() {
		return lastNames;
	}

	public void setLastNames(String[] lastNames) {
		this.lastNames = lastNames;
	}

	public boolean isUseLatency() {
		return useLatency;
	}

	public void setUseLatency(boolean useLatency) {
		this.useLatency = useLatency;
	}

	public int getConnectionLatencyMax() {
		return connectionLatencyMax;
	}

	public void setConnectionLatencyMax(int connectionLatencyMax) {
		this.connectionLatencyMax = connectionLatencyMax;
	}

	public int getConnectionLatencyMin() {
		return connectionLatencyMin;
	}

	public void setConnectionLatencyMin(int connectionLatencyMin) {
		this.connectionLatencyMin = connectionLatencyMin;
	}

	public int getMessageLatency() {
		return messageLatency;
	}

	public void setMessageLatency(int messageLatency) {
		this.messageLatency = messageLatency;
	}

	public int getNumPatients() {
		return numPatients;
	}

	public void setNumPatients(int numPatients) {
		this.numPatients = numPatients;
	}

	public String getCareUnitLabel() {
		return careUnitLabel;
	}

	public void setCareUnitLabel(String careUnitLabel) {
		this.careUnitLabel = careUnitLabel;
	}

	public String getBedUnitLabel_prefix() {
		return bedUnitLabel_prefix;
	}

	public void setBedUnitLabel_prefix(String bedUnitLabel_prefix) {
		this.bedUnitLabel_prefix = bedUnitLabel_prefix;
	}

}
