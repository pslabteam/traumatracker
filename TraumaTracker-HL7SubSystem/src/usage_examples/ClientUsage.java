package usage_examples;

import org.apache.log4j.Logger;

import client.Client;
import usage_utilities.ExampleMessages;

public class ClientUsage {

	public static void main(String[] args) {
		//disabilitare logging della libreria che � eccessivo
		Logger.getRootLogger().removeAllAppenders();
		//creo clients
		Client client1 = new Client(1011, "localhost", 5000);
		client1.startClient();
		Client client2 = new Client(1011, "localhost", 5000);
		client2.startClient();

		//dopo 5 secondo invio due messaggi
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		client1.sendMsg(ExampleMessages.msgADT2);
		client2.sendMsg(ExampleMessages.msgQRY);
		//aspetto altri 5 secondi e lo rifaccio
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//invio messaggio
		client1.sendMsg(ExampleMessages.msgADT2);
		client2.sendMsg(ExampleMessages.msgQRY);
		//chiudo connessioni
		client1.closeConnection();
		client2.closeConnection();
		
	}

}
