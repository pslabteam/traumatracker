package usage_examples;

import org.apache.log4j.Logger;

import server.Server;
import server_data.SimulationData;

public class ServerUsage {

	public static void main(String[] args) {
		//disabilitare logging della libreria che � eccessivo
		Logger.getRootLogger().removeAllAppenders();
		// creazione server
		Server server = new Server(1011);
		server.startServer(new SimulationData());
	}

}
