package usage_examples;

import ca.uhn.hl7v2.model.v23.datatype.XPN;
import ca.uhn.hl7v2.model.v23.message.ADT_A01;
import ca.uhn.hl7v2.model.v23.segment.MSH;
import ca.uhn.hl7v2.model.v23.segment.PID;
import usage_library.HL7Library;
import usage_library.HL7Parser;
import usage_library.HL7Printer;
import usage_utilities.ExampleMessages;

public class Examples {

	public static void main(String[] args) {

		// inizializzazione libreria
		HL7Library library = new HL7Library();

		parsingExample(library);
		creationExample(library);

		// creazione messaggio

	}

	// parsing messaggio
	public static void parsingExample(HL7Library library) {
		System.out.println("*****INIT PARSING EXAMPLE*****");
		// parsing messaggio
		HL7Parser parser = library.getMessageParser();
		parser.parseMessage("ADT", ExampleMessages.msgADT);
		MSH msh = parser.getMSHMsg();
		ADT_A01 adt = parser.getADTMsg();
		String msgType = msh.getMessageType().getMessageType().getValue();
		String msgTrigger = msh.getMessageType().getTriggerEvent().getValue();
		System.out.println("type: " + msgType + " trigger: " + msgTrigger);
		XPN patientName = adt.getPID().getPatientName(0);
		String familyName = patientName.getXpn1_FamilyName().getValue();
		System.out.println("familyname: " + familyName);
		System.out.println("*****END PARSING EXAMPLE*****");
	}

	// creazione messaggio
	public static void creationExample(HL7Library library) {
		//creazione messaggio
		System.out.println("*****INIT CREATION EXAMPLE*****");
		ADT_A01 adt=null;
		try {
			adt = new ADT_A01();
			adt.initQuickstart("ADT", "A01", "P");
			MSH mshSegment = adt.getMSH();
			mshSegment.getSendingApplication().getNamespaceID().setValue("TestSendingSystem");
			mshSegment.getSequenceNumber().setValue("123");
			PID pid = adt.getPID();
			pid.getPatientName(0).getFamilyName().setValue("Doe");
			pid.getPatientName(0).getGivenName().setValue("John");
	        pid.getPid1_SetIDPatientID().setValue("123456");
		} catch (Exception e) {
			e.printStackTrace();
		}
		//stampa
		HL7Printer printer = library.getMessagePrinter();
		printer.printADTMessage(adt);

		System.out.println("*****END CREATION EXAMPLE*****");
	}

}
