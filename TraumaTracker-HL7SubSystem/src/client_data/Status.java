package client_data;


public enum Status {

	
	//codici errori
	SUCCESS_CONNECTION("Connessione stabilita con successo"),
	SUCCESS_REQUEST("Richiesta effettuata con successo"),
	SUCCESS_BED_NOT_FOUND("Bed not found"),
	ERROR_CONNECTION("Connessione non stabilita. Hai specificato l'indirizzo ip e il numero di porta corretti? Il server � disponibile?"),
	ERROR_REQUEST("Errore nella richiesta dei parametri vitali: server non disponibile o timeout scaduto"),
	ERROR_APPLICATION_REJECT("Tipo di errore: Application reject \n"),
	ERROR_APPLICATION_ERROR("Tipo di errore: Application error \n");
	

	private final String description;
	
	
    private Status(String description){
        this.description=description;
    }
    
    
    public String Description(){
    	return this.description;
    }
    

}
