package client_data;

import java.util.ArrayList;
import java.util.List;

public class RequestResult {
	private String errorMessage;
	private Status status;
	private VitalSign sign;
	private String measure;
	private String unitMeasure;
	private String sentMessage;
	private String receivedMessage;
	private List<VitalSign> signs;
	private List<String> measures;
	private List<String> unitMeasures;
	
	public RequestResult(Status status, String result, String errorMessage){
		this.status=status;
		this.errorMessage=errorMessage;
		this.measure="";
		this.unitMeasure="";
		this.receivedMessage="";
		this.sentMessage="";
		signs=new ArrayList<>();
		measures=new ArrayList<>();
		unitMeasures=new ArrayList<>();
	}
	
	public RequestResult(){
		this.errorMessage="";
		this.measure="";
		this.unitMeasure="";
		this.receivedMessage="";
		this.sentMessage="";
		signs=new ArrayList<>();
		measures=new ArrayList<>();
		unitMeasures=new ArrayList<>();
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage=errorMessage;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status=status;
	}

	public VitalSign getVitalSign() {
		return this.sign;
	}

	public void setVitalSign(VitalSign sign) {
		this.sign=sign;
	}

	public String getMeasure() {
		return this.measure;
	}

	public void setMeasure(String measure) {
		this.measure=measure;
	}

	public String getUnitMeasure() {
		return this.unitMeasure;
	}

	public void setUnitMeasure(String unitMeasure) {
		this.unitMeasure=unitMeasure;
	}
	
	public String toString(){
		if(this.signs.size()>0){
			//pi� parametri
			String res="Result: ";
			for(int i=0;i<signs.size();i++){
				res=res+this.signs.get(i).Value()+": "+this.measures.get(i)+" "+this.unitMeasures.get(i) + "\n";
			}
			return res;
		} else{
			//un solo parametro
			if(this.sign!=null && this.measure!=null && this.unitMeasure!=null){
				return "Result: " + this.sign.Value()+": "+this.measure+" "+this.unitMeasure + "\n";
			} else{
				return "Result: ";
			}
		}
	}

	public List<VitalSign> getVitalSigns() {
		return this.signs;
	}

	public void setVitalSigns(List<VitalSign> signs) {
		this.signs=signs;
	}

	public List<String> getMeasures() {
		return this.measures;
	}

	public void setMeasures(List<String> measures) {
		this.measures=measures;
	}

	public List<String> getUnitMeasures() {
		return this.unitMeasures;
	}

	public void setUnitMeasures(List<String> unitMeasures) {
		this.unitMeasures=unitMeasures;
	}

	public String getSentMessage() {
		return this.sentMessage;
	}

	public String getReceivedMessage() {
		return this.receivedMessage;
	}

	public void setSentMessage(String sentMessage) {
		this.sentMessage=sentMessage;
	}

	public void setReceivedMessage(String receivedMessage) {
		this.receivedMessage=receivedMessage;
	}
}
