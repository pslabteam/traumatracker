package client_data;

public enum VitalSign {

	//pressione diastolica
	PRESSURE_DIASTOLIC("8462-4","LOINC","SN","1","ART D","mm(hg)","DIA"),
	//pressione sistolica
	PRESSURE_SISTOLIC("8480-6","LOINC","SN","1","ART S","mm(hg)","SYS"),
	//frequenza cardiaca
	HEARTH_RATE("8867-4","LOINC","SN","1","HR","/min","HR"),
	//saturazione
	SATURATION("2710-2","LOINC","SN","1","SpO2","%","SpO2"),
	//etCO2
	ETCO2("20824","MIB/CEN","SN","2","etCO2*","mm(hg)","EtCO2"),
	//temperatura
	TEMPERATURE("8332-9","LOINC","SN","4","Ta","cel","Temp"),
	
	NBP_D("8496-2", "LOINC", "SN", "1","NBP D","mm(hg)","NBP-D"),
	NBP_S("8508-4", "LOINC", "SN", "1","NBP S","mm(hg)","NBP-S"),
	
	/*INSERIRE QUI (PRIMA DI VITAL SIGN) EVENTUALI NUOVI PARAMETRI */
	
	//segnali vitali
	VITAL_SIGNS("vitalsigns","vitalsigns","vitalsigns","vitalsigns","vitalsigns","vitalsigns","vitalsigns");

	private final String code,codeType ,dataType,instance, label,unitMeasure,value;

	//code: campo "HL7/LOINC Code" o "MIB/CEN Code" nella tabella
	//codeType: indica se viene usato loinc o mib/cen
	//dataType: se SN (numeric) o ST (stringa)
	//instance: campo instance nella tabella
	//label: campo label nella tabella
	//unitMeasure: unit� di misura
	//value: stringa testuale che si vuole associare al parametro vitale (a scelta)
	
	
    private VitalSign(String code,String codeType, String dataType, String instance, String label, String unitMeasure, String value){
        this.code=code;
        this.codeType=codeType;
        this.dataType=dataType;
        this.instance=instance;
        this.label=label;
        this.unitMeasure=unitMeasure;
        this.value=value;
    }
    
    
    public String Code() {
       return this.code;
    }
    
    public String CodeType(){
    	return this.codeType;
    }
    
    public String DataType(){
    	return this.dataType;
    }
    
    public String Instance(){
    	return this.instance;
    }
    
    public String Label(){
    	return this.label;
    }
    
    public String UnitMeasure(){
    	return this.unitMeasure;
    }
    
    public String Value(){
    	return this.value;
    }
    
  
    
    


}
