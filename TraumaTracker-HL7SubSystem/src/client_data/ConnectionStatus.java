package client_data;

public class ConnectionStatus {
	private String message;
	private Status status;
	
	public ConnectionStatus(String message,Status status){
		this.message=message;
		this.status=status;
	}
	
	public ConnectionStatus(){
		this.message="";
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public Status getStatus(){
		return this.status;
	}
	
	public void setMessage(String message){
		this.message=message;
	}
	
	public void setStatus(Status status){
		this.status=status;
	}

	

}
