package test;

import client.ClientWrapper;
import client_data.ConnectionStatus;
import client_data.Status;

public class TestConnectionLatency {

	public static void main(String[] args) {
		 String ip = "137.204.107.184";
		//String ip = "191.1.1.76";
		int port = 2550;

		ClientWrapper init = new ClientWrapper(ip, port, 5000);
		init.initialize();
		long t0 = System.currentTimeMillis();
		ConnectionStatus connRes = init.connect();
		long t1 = System.currentTimeMillis();
		if (connRes.getStatus().equals(Status.SUCCESS_CONNECTION) == true) {
			System.out.println("Tempo impiegato: " + (t1 - t0));
			init.getClient().closeConnection();
		} else {
			System.out.println(connRes.getMessage());
		}

	}

	

}
