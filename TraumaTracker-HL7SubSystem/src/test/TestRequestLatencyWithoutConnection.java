package test;

import client.ClientWrapper;
import client_data.ConnectionStatus;
import client_data.RequestResult;
import client_data.Status;
import client_flags.FlagsClient;

public class TestRequestLatencyWithoutConnection {

	public static void main(String[] args) {
		String ip="137.204.107.184";
		//String ip="191.1.1.76";
		int port=2550;
		
		ClientWrapper init = new ClientWrapper(ip, port,5000);
		init.initialize();
		init.getClient().setLog(false);
		ConnectionStatus connRes = init.connect();
		long totalTime=0;
		int nMessages=100;
		waitTime(3000);
		if (connRes.getStatus().equals(Status.SUCCESS_CONNECTION) == true) {
			for(int i=0;i<nMessages;i++){
				RequestResult result = new RequestResult();
				long t0=System.currentTimeMillis();
				result = init.getPressureDiastolic(FlagsClient.CU(),FlagsClient.BL());
				long t1=System.currentTimeMillis();
				System.out.println("Tempo impiegato: "+(t1-t0));
				totalTime=totalTime+(t1-t0);
				if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
					System.out.println(result.toString());
				} else {
					System.out.println(result.getErrorMessage() + "\n");
				}
				waitTime(1000);
			}
			System.out.println("Tempo medio: "+(totalTime/nMessages));
			
		} else {
			System.out.println(connRes.getMessage());
		}

	}
	
	//per non provocare l'errore del troncamento
	public static void waitTime(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		

}
