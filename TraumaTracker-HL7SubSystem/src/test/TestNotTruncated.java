package test;

import client.ClientWrapper;
import client_data.ConnectionStatus;
import client_data.RequestResult;
import client_data.Status;
import client_data.VitalSign;

public class TestNotTruncated {
	
	public static void main(String[] args) {
		boolean truncated=false;
		String ip = "137.204.107.184";
		int port = 2550;
		ClientWrapper init = new ClientWrapper(ip, port, 5000);
		init.initialize();
		ConnectionStatus connRes = init.connect();
		if (connRes.getStatus().equals(Status.SUCCESS_CONNECTION) == true) {
			while(true){
				RequestResult result = new RequestResult();
				result = init.getVitalSigns(VitalSign.ETCO2,"TI","SALA1");
				if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
					System.out.println(result.toString());
					if(result.getMeasure()=="0"){
						System.out.println("MESSAGGIO TRONCATO");
						truncated=true;
						break;
					}
				} else {
					System.out.println(result.getErrorMessage() + "\n");
				}
				if(truncated==true){
					break;
				}
				//se inferiore a 800 succede il problema
				waitTime(800);
			}
		} else {
			System.out.println(connRes.getMessage());
		}

	}
	
	public static void waitTime(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
