package test;


import client.ClientWrapper;
import client_data.ConnectionStatus;
import client_data.RequestResult;
import client_data.Status;
import client_flags.FlagsClient;

public class TestVitalParameters {

	public static void main(String[] args) {
		
		String ip="137.204.107.184";
		int port=2550;
		
		ClientWrapper init = new ClientWrapper(ip, port,5000);
		init.initialize();
		ConnectionStatus connRes = init.connect();
		
		if (connRes.getStatus().equals(Status.SUCCESS_CONNECTION) == true) {
			
			RequestResult result = new RequestResult();
			result = init.getPressureDiastolic(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				System.out.println(result.toString());
			} else {
				System.out.println(result.getErrorMessage() + "\n");
			}
			waitTime(800);

			result = init.getPressureSistolic(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				System.out.println(result.toString());
			} else {
				System.out.println(result.getErrorMessage() + "\n");
			}
			waitTime(800);

			result = init.getHearthRate(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				System.out.println(result.toString());
			} else {
				System.out.println(result.getErrorMessage() + "\n");
			}
			waitTime(800);

			result = init.getSaturation(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				System.out.println(result.toString());
			} else {
				System.out.println(result.getErrorMessage() + "\n");
			}
			waitTime(800);

			result = init.getETC02(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				System.out.println(result.toString());
			} else {
				System.out.println(result.getErrorMessage() + "\n");
			}
			waitTime(800);

			result = init.getTemperature(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				System.out.println(result.toString());
			} else {
				System.out.println(result.getErrorMessage() + "\n");
			}
			waitTime(800);

			result = init.getVitalSigns(FlagsClient.CU(),FlagsClient.BL());
			if (result.getStatus().equals(Status.SUCCESS_REQUEST)) {
				System.out.println(result.toString());
			} else {
				System.out.println(result.getErrorMessage() + "\n");
			}
			waitTime(800);

		} else {
			System.out.println(connRes.getMessage());
		}

	}
	
	public static void waitTime(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}