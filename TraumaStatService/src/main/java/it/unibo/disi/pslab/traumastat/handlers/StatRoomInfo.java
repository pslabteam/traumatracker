package it.unibo.disi.pslab.traumastat.handlers;

public class StatRoomInfo {
	
	private String roomName;
	private long firstTime;
	private long duration;
	private long lastEntrance;
	private boolean exitOK;
	
	public StatRoomInfo(String param, long firstTime) {
		this.roomName = param;
		this.firstTime = firstTime;
		this.lastEntrance = firstTime;
		duration = 0;
		exitOK = false;
	}
	
	public String getRoomName() {
		return roomName;
	}
	
	public long getFirstTime() {
		return firstTime;
	}

	public void setNewEntrance(long en) {
		lastEntrance = en;
		exitOK = false;
	}
	
	public void setExit(long end) {
		duration += (end - lastEntrance);
		exitOK = true;
	}
	
	public boolean isExitOK() {
		return exitOK;
	}
	
	public long getDuration() {
		return duration;
	}
}