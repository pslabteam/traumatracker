package it.unibo.disi.pslab.traumastat.handlers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.traumastat.AbstractServiceHandler;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public abstract class GeneralStatHandler extends AbstractServiceHandler {

	public GeneralStatHandler(TraumaStatService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling stat request from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		JsonObject query = new JsonObject();
		/* fetching all reports */
		log("fetching all reports...");
		findInReports(query, res -> {
			if (res.succeeded()) {				
				/* executed in a separate thread */
				log("Reports fetched => Start processing...");
				this.getService().getVertx().executeBlocking(f -> {
					/* calling the specific stat */
					JsonObject stat = computeStat(new Params() {
						public String getParam(String paramName) {
							return routingContext.request().getParam(paramName);
						};
					}, res.result());
					/* back to the event loop though the future */
					log("Processing DONE.");

					f.complete(stat);
				}, result -> {
					if (result.succeeded()) {
						/* we got the result */
						log("OK - Replying data");
						JsonObject data = ((JsonObject)result.result());
						String format = routingContext.request().getParam("format");
						if (format == null || format.equals("json")) {
							String reply = data.encodePrettily();
							// log("Snippet sent: \n" + reply.substring(0, 80) + "...");
							response.putHeader("content-type", "application/json").end(reply);
						} else if (format.equals("csv")) {
							// String reply = data.encodePrettily();
							String csv = this.toCSV(data);
							// log("Snippet sent: \n" + csv.substring(0, 80) + "...");
							response.putHeader("content-type", "text/csv").end(csv);
							
						} else {
							log("ERROR - invalid format .");
							JsonObject error = new JsonObject().put("result", "error: invalid format");
							response.putHeader("content-type", "application/json").end(error.encodePrettily());
						}
					} else {
						log("ERROR - Generic error.");
						JsonObject error = new JsonObject().put("result", "error");
						response.putHeader("content-type", "application/json").end(error.encodePrettily());
					}
				});
			} else {
				sendError(404, response);
			}
		});
	}
	
	abstract protected JsonObject computeStat(Params params, List<JsonObject> elements);

	protected String toCSV(JsonObject source) {
		return null;
	}
	
	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected boolean hasTraumaLeader(JsonObject rec, String operatorId) {
		if (rec.getString("startOperatorId").equals(operatorId)) {
			return true;
		} else {
			return false;
		}
	}
	protected List<JsonObject> elementsFromToBy(List<JsonObject> recs, String dateFrom, String dateTo, String operatorId){
		return recs.stream().filter(rec -> {
			if (hasTraumaLeader(rec, operatorId)) {
				String startDate = rec.getString("startDate"); // yyyy-mm-dd
				if (startDate != null && !startDate.equals("")) {
					return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
				} else {
					return false;
				}
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	protected Optional<String> getNestedString(JsonObject obj, String... labels) {
		JsonObject ob = obj;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		String v = ob.getString(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);

	}

	protected Optional<Boolean> getNestedBoolean(JsonObject obj, String... labels) {
		JsonObject ob = obj;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		Boolean v = ob.getBoolean(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);
	}

	protected Optional<Integer> getNestedInt(JsonObject obj, String... labels) {
		JsonObject ob = obj;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		Integer v = ob.getInteger(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);
	}
	
	protected Optional<JsonObject> getNestedObject(JsonObject obj, String... labels) {
		JsonObject ob = obj;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		JsonObject v = ob.getJsonObject(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);
	}

	private long getAbsTimeInSec(String startDate, String startTime) {
		if (startDate != null && !startDate.equals("") && 
			startTime != null && !startTime.equals("")) {

			// date: // yyyy-mm-dd
			// time: // hh:mm:ss
			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
		    try {
		    		Date date = sdf.parse(startDate);
		    
				long startValue = 
						Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
	
			    return date.getTime()/1000 + startValue;
		    } catch (Exception ex) {
				throw new IllegalArgumentException("invalid date");
		    }
		} else {
			throw new IllegalArgumentException("invalid date");
		}
		
	}
	
}
