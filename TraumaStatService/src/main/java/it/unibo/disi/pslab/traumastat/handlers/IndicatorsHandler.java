package it.unibo.disi.pslab.traumastat.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumastat.TraumaStatService;
import it.unibo.disi.pslab.traumatracker.ontology.TraumaReportDomainModel.FinalDest;
import it.unibo.disi.pslab.traumatracker.ontology.TraumaReportDomainModel;
import it.unibo.disi.pslab.traumatracker.ontology.TraumaReportDomainModel.Gender;
import it.unibo.disi.pslab.traumatracker.ontology.TraumaReportDomainModel.Vehicle;

import static it.unibo.disi.pslab.traumatracker.util.StatUtils.*;

/*
      { id: 'num',                  desc: 'Numerosità Totale', cb: false }, 
      { id: 'primary-admin',        desc: 'Ammissioni Primarie', cb: false }, 
      { id: 'transf-before24',      desc: 'Trasferiti entro 24h Trauma', cb: false },
      { id: 'transf-after24',       desc: 'Trasferiti dopo 24h Trauma', cb: false },
      { id: 'transf-from-spoke',    desc: 'Trasferiti da spoke', cb: false },   
      { id: 'age',                  desc: 'Età pazienti', cb: false },   
      { id: 'age-older-70',         desc: 'Anziani (età >= 70)', cb: false },
      { id: 'men',                  desc: 'Uomini', cb: false },   
      { id: 'trauma-head',          desc: 'Trauma cranico', cb: false },   
      { id: 'trauma-hemo',          desc: 'Trauma emorragico', cb: false },   
      { id: 'trauma-poli',          desc: 'Politraumi', cb: false },   
      { id: 'iss-16+',              desc: 'ISS 16+', cb: false },   
      { id: 'iss',                  desc: 'ISS', cb: false },   
      { id: 'cause-traffic',        desc: 'Causa Traffico', cb: false },   
      { id: 'cause-other',          desc: 'Altra Causa', cb: false },   
      { id: 'arrival-heli',         desc: 'Arrivo in Elicottero', cb: false },   
      { id: 'time-shr-tc',          desc: 'Tempo Shock Room e TC', cb: false },   
      { id: 'time-shr-dam',         desc: 'Tempo Shock Room e damage control', cb: false },   
      { id: 'time-shr-head',        desc: 'Tempo Shock Room e craniotomia (sop)', cb: false },   
      { id: 'final-dest-PS',        desc: 'Destinazione finale: PS', cb: false },   
      { id: 'final-dest-TI',        desc: 'Destinazione finale: TI', cb: false },   
      { id: 'final-dest-CM',        desc: 'Destinazione finale: CM', cb: false },   
      { id: 'final-dest-other',     desc: 'Destinazione finale: Altro', cb: false },   

 */
public class IndicatorsHandler extends GeneralStatHandler {

	HashMap<String, Function<List<JsonObject>,JsonObject>> indics;
	
	public IndicatorsHandler(TraumaStatService service){
		super(service);
		indics = new HashMap<String, Function<List<JsonObject>,JsonObject>>();
		indics.put("num", this::indicNum);
		indics.put("primary-admin", this::indicPrimaryAdmin);
		indics.put("transf-before24", this::indicTransfBefore24);
		indics.put("transf-after24", this::indicTransfAfter24);
		indics.put("transf-from-spoke", this::indicTransfFromSpoke);
		indics.put("age", this::indicAge);
		indics.put("age-older-70", this::indicAgeOlderThan70);
		indics.put("men", this::indicMen);
		indics.put("trauma-head", this::indicTraumaHead);
		indics.put("trauma-hemo", this::indicTraumaHemo);
		indics.put("trauma-poli", this::indicTraumaPoli);
		indics.put("trauma-major", this::indicTraumaMajor);
		indics.put("iss-greater-than-16", this::indicHighIss);
		indics.put("iss", this::indicIss);
	    indics.put("cause-traffic", this::indicCauseTraffic);
	    indics.put("cause-other", this::indicCauseOther);
	    indics.put("time-shr-tc", this::indicTimeFromSHRtoTC);
	    indics.put("time-in-shr", this::indicTimeInSHR);
	    indics.put("time-shr-dam", this::indicTimeFromSHRtoDAM);
	    indics.put("time-shr-head", this::indicTimeFromSHRtoHEAD);
	    indics.put("arrival-heli", this::indicArrivalHeli);
	    indics.put("final-dest-PS", this::indicFinalDestPS);
	    indics.put("final-dest-TI", this::indicFinalDestTI);
	    indics.put("final-dest-MU", this::indicFinalDestMU);
	    indics.put("final-dest-other", this::indicFinalDestOther);
	}

	@Override
	protected JsonObject computeStat(Params params, List<JsonObject> elements) {
		
		try {
			JsonObject reply = new JsonObject();		
			String dateFrom = params.getParam("dateFrom");
			String dateTo = params.getParam("dateTo");
			List<JsonObject> reps = this.elementsFromTo(elements, dateFrom, dateTo);
			List<JsonObject> repsPrevYear = null;
			
			String[] indicatorsval = params.getParam("indicators").split(",");
			String period = params.getParam("period");	
			boolean comp = false;
			try {
				String comparison = params.getParam("comparisonWithPrevYear");
				if (comparison != null) {
					comp = Boolean.parseBoolean(comparison);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (comp) {
				try {
					int y1 = Integer.parseInt(dateFrom.substring(0, 4));
					int y2 = Integer.parseInt(dateTo.substring(0, 4));
					String dateFrom2 = "" + (y1 - 1) + dateFrom.substring(4);
					String dateTo2 = "" + (y2 - 1) + dateTo.substring(4);
					repsPrevYear = this.elementsFromTo(elements, dateFrom2, dateTo2);
				} catch (Exception ex) {
					comp = false;
				}
			}
			HashMap<String,List<JsonObject>> aggregates = this.aggregate(reps, period);
			HashMap<String,List<JsonObject>> aggregatesPrevYear  = null;
			if (repsPrevYear != null) {
				aggregatesPrevYear = this.aggregate(repsPrevYear, period);
			}
			JsonArray indicData = new JsonArray();
			for (Object obj: indicatorsval) {			
				JsonObject ind = new JsonObject();				
				String indic = (String) obj;
				ind.put("id", indic);
				
				JsonArray data = new JsonArray();
				List<JsonObject> list = new ArrayList<JsonObject>();
				for (Entry<String, List<JsonObject>> reportsPerPeriod: aggregates.entrySet()) {
					JsonObject res = indics.get(indic).apply(reportsPerPeriod.getValue());
					res.put("period",reportsPerPeriod.getKey());
					list.add(res);
				}
				list.sort((JsonObject el1, JsonObject el2) -> { 
					return el1.getString("period").compareTo(el2.getString("period")); });
				for (JsonObject el: list) {
					data.add(el);
				}
				
				ind.put("data", data);

				if (aggregatesPrevYear != null) {
					JsonArray dataPrevYear = new JsonArray();
					List<JsonObject> listPrevYear = new ArrayList<JsonObject>();
					for (Entry<String, List<JsonObject>> reportsPerPeriod: aggregatesPrevYear.entrySet()) {
						JsonObject res = indics.get(indic).apply(reportsPerPeriod.getValue());
						res.put("period",reportsPerPeriod.getKey());
						listPrevYear.add(res);
					}
					listPrevYear.sort((JsonObject el1, JsonObject el2) -> { 
						return el1.getString("period").compareTo(el2.getString("period")); });
					for (JsonObject el: listPrevYear) {
						dataPrevYear.add(el);
					}
					ind.put("dataPrevYear", dataPrevYear);
				}
				
				indicData.add(ind);
			}

			reply.put("content", "indicators");
			reply.put("dateFrom", dateFrom);
			reply.put("dateTo", dateFrom);
			reply.put("period", period);
			reply.put("data", indicData);
			if (comp) {
				reply.put("comparisonPrevYear", comp);
			}
			return reply;
		} catch (Exception ex) {
			ex.printStackTrace();
			JsonObject reply = new JsonObject();
			reply.put("error", "internal-error: " + ex.getMessage());
			return reply;
			
		}
	}
	
	protected HashMap<String, List<JsonObject>> aggregate(List<JsonObject> reps, String period){
		/* aggregate per period */
		HashMap<String, List<JsonObject>> aggregates = new HashMap<String, List<JsonObject>>();
		
		String tag = "-m-";
		int periodLength = 1;

		if (period.equals("mon")) {
			tag = "-m-";
			periodLength = 1;
		} else if (period.equals("tri")) {
			tag = "-t-";
			periodLength = 3;
		} else if (period.equals("sem")) {
			tag = "-s-";
			periodLength = 6;
		} else if (period.equals("year")) {
			tag = "-y-";
			periodLength = 12;
		} else {
			return null;
		}
		
		for (JsonObject rep: reps) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			String startDate = rep.getString("startDate");
			int mm = dom.getMonth(startDate); // 1..12
			int index = ((mm - 1) / periodLength) + 1;
			String sindex = index < 10 ? "0" + index: "" + index;
			String key = "" + dom.getYear(startDate) + tag + sindex;
			List<JsonObject> slot = aggregates.get(key);
			if (slot == null) {
				slot = new ArrayList<JsonObject>();
				aggregates.put(key, slot);
			}
			slot.add(rep);
		}
		
		return aggregates;
	}
	
	/* indicators */
	
	protected JsonObject indicNum(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		indic.put("nTrauma", elems.size());
		return indic;
	}
	
	protected JsonObject indicPrimaryAdmin(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		long num = elems.stream().filter( (JsonObject rep) -> {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			return !dom.isFromOtherPS();
		}).count();
		indic.put("nTotalTrauma", elems.size());
		indic.put("nPrimaryAdmis", num);
		if (elems.size() > 0) {
			indic.put("perc", (int) (num * 100 / elems.size()));
		}
		return indic;
	}

	protected JsonObject indicTransfBefore24(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		long num = elems.stream().filter( (JsonObject rep) -> {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			boolean fromOtherPS = dom.isFromOtherPS();
			Optional<Long> accTime = dom.getAccidentAbsTimeInSec();
			if (fromOtherPS && accTime.isPresent()) {
				long startTime = dom.getAbsStartTimeInSec();
				return (startTime - accTime.get()) < 24*3600;
			} else {
				return false;
			}
		}).count();
		indic.put("nTotalTrauma", elems.size());
		indic.put("nTransfBefore24h", num);
		if (elems.size() > 0) {
			indic.put("perc", (int) (num * 100 / elems.size()));
		}
		return indic;
	}

	protected JsonObject indicTransfAfter24(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		long num = elems.stream().filter( (JsonObject rep) -> {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			boolean fromOtherPS = dom.isFromOtherPS();
			Optional<Long> accTime = dom.getAccidentAbsTimeInSec();
			if (fromOtherPS && accTime.isPresent()) {
				long startTime = dom.getAbsStartTimeInSec();
				return (startTime - accTime.get()) >= 24*3600;
			} else {
				return false;
			}
		}).count();
		indic.put("nTotalTrauma", elems.size());
		indic.put("nTransfAfter24h", num);
		if (elems.size() > 0) {
			indic.put("perc", (int) (num * 100 / elems.size()));
		}
		return indic;
	}

	protected JsonObject indicTransfFromSpoke(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		HashMap<String, Integer> provs = new HashMap<String, Integer>();
		
		int others = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			if (dom.isFromOtherPS()) {
				others++;
				Optional<String> prov = dom.getProvenienceSpoke();
				if (prov.isPresent()) {
					Integer val = provs.get(prov.get());
					if (val == null) {
						provs.put(prov.get(), 1);
					} else {
						provs.put(prov.get(), val + 1);
					}
				}
			}
		}
		indic.put("nTotalTrauma", elems.size());
		indic.put("nTotalFromSpokes", others);
		indic.put("perc", (int) (others * 100 / elems.size()));
		JsonArray spokes = new JsonArray();
		for (Entry<String, Integer> e: provs.entrySet()) {
			JsonObject sp = new JsonObject();
			sp.put("spoke", e.getKey());
			sp.put("nTrauma", e.getValue());
			spokes.add(sp);
		}
		indic.put("spokes", spokes);
		// System.out.println(spokes);
		return indic;
	}
	
	protected JsonObject indicAge(List<JsonObject> elems) {
		List<Double> ages = new ArrayList<Double>();
		int missing = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Integer> age = dom.getAge();
			if (age.isPresent()) {
				ages.add((double) age.get());
			} else {
				missing++;
			}
		}		
		JsonObject indic = computeBasicStat(ages);
		indic.put("nTotalTrauma", elems.size());
		indic.put("missing", missing);
		return indic;
	}
	
	protected JsonObject indicAgeOlderThan70(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Integer> age = dom.getAge();
			if (age.isPresent()) {
				nTot++;
				if (age.get() >= 70) {
					count++;
				} 
			}
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nAgeOlderEqThan70", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}

	protected JsonObject indicMen(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Gender> gen = dom.getGender();
			if (gen.isPresent()) {
				nTot++;
				if (gen.get().equals(Gender.M)) {
					count++;
				}
			}
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nMen", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}
	
	protected JsonObject indicTraumaHead(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			if (dom.isTraumaHead()) {
				count++;
			}
		}		
		indic.put("nTotalTrauma", elems.size());
		indic.put("nTraumaHead", count);
		if (elems.size() > 0) {
			indic.put("perc", (int) (count * 100 / elems.size()));
		}
		return indic;
	}

	protected JsonObject indicTraumaHemo(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			if (dom.isShockEmo()) {
				count++;
			}
		}		
		indic.put("nTotalTrauma", elems.size());
		indic.put("nTraumeHemo", count);
		if (elems.size() > 0) {
			indic.put("perc", (int) (count * 100 / elems.size()));
		}
		return indic;
	}
	
	protected JsonObject indicTraumaPoli(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			if (dom.isTraumaPoli()) {
				count++;
			}
		}		
		indic.put("nTotalTrauma", elems.size());
		indic.put("nTraumaPoli", count);
		if (elems.size() > 0) {
			indic.put("perc", (int) (count * 100 / elems.size()));
		}
		return indic;
	}
	
	protected JsonObject indicTraumaMajor(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			if (dom.isMajorTrauma()) {
				count++;
			}
		}		
		indic.put("nTotalTrauma", elems.size());
		indic.put("nTraumaMajor", count);
		if (elems.size() > 0) {
			indic.put("perc", (int) (count * 100 / elems.size()));
		}
		return indic;
	}
	
	protected JsonObject indicHighIss(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int missing = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Integer> iss = dom.getIss();
			if (iss.isPresent()) {
				int issv = iss.get();
				if (issv >= 16) {
					count++;
				}
			} else {
				missing++;
			}
		}		
		indic.put("nTotalTrauma", elems.size());
		indic.put("nISSGreaterEqualThan16", count);
		indic.put("nMissingIss", missing);
		if (elems.size() > 0) {
			indic.put("perc", (int) (count * 100 / elems.size()));
		}
		return indic;
	}
	
	protected JsonObject indicIss(List<JsonObject> elems) {
		List<Double> values = new ArrayList<Double>();
		int missing = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Integer> iss = dom.getIss();
			if (iss.isPresent()) {
				values.add((double) iss.get() );
			} else {
				missing++;
			}
		}		
		JsonObject indic = computeBasicStat(values);
		indic.put("nMissingIss", missing);
		indic.put("nTotalTrauma", values.size());
		return indic;
	}

	protected JsonObject indicCauseTraffic(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Boolean> isacc = dom.isCarAccident();
			if (isacc.isPresent()) {
				nTot++;
				if (isacc.get()) {
					count++;
				}
			} 
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nCarAccidents", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}
	
	protected JsonObject indicCauseOther(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Boolean> isacc = dom.isCarAccident();
			if (isacc.isPresent()) {
				nTot++;
				if (!isacc.get()) {
					count++;
				}
			} 
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nNotCarAccidents", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}
	
	
	protected JsonObject indicArrivalHeli(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Vehicle> veh = dom.getArrivalVehicle();
			if (veh.isPresent()) {
				nTot++;
				if (veh.get().equals(Vehicle.HELI)) {
					count++;
				}
			} 
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nArrivalsHeli", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}

	protected JsonObject indicFinalDestPS(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<FinalDest> fd = dom.getFinalDest();
			if (fd.isPresent()) {
				nTot++;
				if (fd.get().equals(FinalDest.PS)) {
					count++;
				}
			} 
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nFinalDestPS", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}

	protected JsonObject indicFinalDestTI(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<FinalDest> fd = dom.getFinalDest();
			if (fd.isPresent()) {
				nTot++;
				if (fd.get().equals(FinalDest.TI)) {
					count++;
				}
			} 
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nFinalDestTI", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}

	protected JsonObject indicFinalDestMU(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<FinalDest> fd = dom.getFinalDest();
			if (fd.isPresent()) {
				nTot++;
				if (fd.get().equals(FinalDest.MU)) {
					count++;
				}
			} 
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nFinalDestMU", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}

	protected JsonObject indicFinalDestOther(List<JsonObject> elems) {
		JsonObject indic = new JsonObject();
		int count = 0;
		int nTot = 0;
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<FinalDest> fd = dom.getFinalDest();
			if (fd.isPresent()) {
				nTot++;
				if (fd.get().equals(FinalDest.OTHER)) {
					count++;
				}
			} 
		}		
		indic.put("nTotalTrauma", nTot);
		indic.put("nFinalDestOther", count);
		if (nTot > 0) {
			indic.put("perc", (int) (count * 100 / nTot));
		}
		return indic;
	}

	protected JsonObject indicTimeFromSHRtoTC(List<JsonObject> elems) {
		List<Double> values = new ArrayList<Double>();
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Long> firstTimeInSHR = dom.getFirstTimeInRoomInSec(dom.SHOCK_ROOM);
			Optional<Long> firstTimeInTC = dom.getFirstTimeInRoomInSec(dom.TAC_PS);
			if (firstTimeInTC.isPresent() && firstTimeInSHR.isPresent()) {
				values.add((double)((firstTimeInTC.get() - firstTimeInSHR.get())/60));
			}
		}	
		JsonObject indic = computeBasicStat(values);
		indic.put("nTotalTrauma", elems.size());
		return indic;

	}
	
	protected JsonObject indicTimeInSHR(List<JsonObject> elems) {
		List<Double> values = new ArrayList<Double>();
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			values.add((double) (dom.getTotalTimeInRoomInSec(dom.SHOCK_ROOM) / 60));
		}		
		JsonObject indic = computeBasicStat(values);
		indic.put("nTotalTrauma", elems.size());
		return indic;
	}

	protected JsonObject indicTimeFromSHRtoDAM(List<JsonObject> elems) {
		List<Double> values = new ArrayList<Double>();
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			Optional<Long> firstTimeInSHR = dom.getFirstTimeInRoomInSec(dom.SHOCK_ROOM);
			Optional<Long> firstTimeDam = dom.getFirstTimeInDamageControlInSec();
			if (firstTimeDam.isPresent() && firstTimeInSHR.isPresent()) {
				values.add((double)((firstTimeDam.get() - firstTimeInSHR.get())/60));
			}
		}		
		JsonObject indic = computeBasicStat(values);
		indic.put("nTotalTrauma", elems.size());
		return indic;
	}

	protected JsonObject indicTimeFromSHRtoHEAD(List<JsonObject> elems) {
		List<Double> values = new ArrayList<Double>();
		for (JsonObject rep: elems) {
			TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
			// values.add((double) dom.getIss());
			/* Indica il tempo medio trascorso tra ingresso in shock room e sala operatoria NCH per pazienti anisocorici e midiriatici */
			Optional<Boolean> ani = dom.isAnisocoric();
			Optional<Boolean> mid = dom.isMidriatic();
			if (ani.isPresent() && ani.get() && mid.isPresent() && mid.isPresent()) {
				Optional<Long> firstTimeInSHR = dom.getFirstTimeInRoomInSec(dom.OP_ROOM_NCH);
				Optional<Long> firstTimeInNCH = dom.getFirstTimeInRoomInSec(dom.SHOCK_ROOM);
				if (firstTimeInSHR.isPresent() && firstTimeInNCH.isPresent()) {
					values.add((double)((firstTimeInNCH.get() - firstTimeInSHR.get())/60));
				}
			}
		}		
		JsonObject indic = computeBasicStat(values);
		indic.put("nTotalTrauma", elems.size());
		return indic;
	}

}
