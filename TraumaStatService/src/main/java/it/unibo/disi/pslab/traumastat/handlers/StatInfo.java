package it.unibo.disi.pslab.traumastat.handlers;

public class StatInfo {
	
	private String param;
	private long firstTime;
	private int nReps;
	
	public StatInfo(String param, long firstTime) {
		this.param = param;
		this.firstTime = firstTime;
		this.nReps = 1;
	}
	
	public String getParam() {
		return param;
	}
	
	public long getFirstTime() {
		return firstTime;
	}
	
	public void incOcc() {
		nReps++;
	}
	public int getNReps() {
		return nReps;
	}
}
