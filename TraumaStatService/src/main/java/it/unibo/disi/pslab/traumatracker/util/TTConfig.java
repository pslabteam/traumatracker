package it.unibo.disi.pslab.traumatracker.util;

import io.vertx.core.json.JsonArray;
import it.unibo.disi.pslab.util.Config;

public class TTConfig extends Config {

	private static final String portKey = "port";
	private static final String dbNameKey = "dbName";
	private static final String reportsCollectionNameKey = "reportsCollectionName";
	private static final String usersCollectionNameKey = "usersCollectionName";
	private static final String ttVersionKey = "version";
	private static final String numRecentReportsKey = "numRecentReports";
	private static final String dbPathKey = "dbPath";
	private static final String mongoPathKey = "mongoPath";
	private static final String logGUIEnabled = "logGUIEnabled";
	private static final String defaultUsers = "defaultUsers";
	
	public TTConfig(String fileName) throws InvalidConfigFileException {
		super(fileName);
	}
	
	/* default */
	public TTConfig(){
		config.put(dbNameKey, "ttservicedb");
		config.put(portKey, "8082");
		config.put(reportsCollectionNameKey, "reports");
		config.put(usersCollectionNameKey, "users");
		config.put(ttVersionKey,"1.0");
		config.put(numRecentReportsKey,20);
		config.put(dbPathKey,"./data/db");
		config.put(mongoPathKey,"/usr/local/bin/mongod");
		config.put(logGUIEnabled,false);
	}
	
	public String getMongoPath() {
		return config.getString(mongoPathKey);
	}
	
	public String getDBPath() {
		return config.getString(dbPathKey);
	}
	
	
	public int getNumRecentReports(){
		return config.getInteger(numRecentReportsKey);
	}
	public String getDBName(){
		return config.getString(dbNameKey);
	}

	public String getVersion(){
		return config.getString(ttVersionKey);
	}
	
	public int getPort(){
		return config.getInteger(portKey);		
	}
	
	public String getUsersCollectionName(){
		return config.getString(usersCollectionNameKey);
	}

	public String getReportsCollectionName(){
		return config.getString(reportsCollectionNameKey);
	}
	
	public boolean isLogGUIEnabled() {
		return config.getBoolean(logGUIEnabled);
	}
	
	public JsonArray getDefUsers() {
		return config.getJsonArray(defaultUsers);
	}

}
