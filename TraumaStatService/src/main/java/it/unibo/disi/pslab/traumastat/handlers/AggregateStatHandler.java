package it.unibo.disi.pslab.traumastat.handlers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public class AggregateStatHandler extends GeneralStatHandler {

	public AggregateStatHandler(TraumaStatService service){
		super(service);
	}

	@Override
	protected JsonObject computeStat(Params params, List<JsonObject> elements) {
		String dateFrom = params.getParam("dateFrom");
		String dateTo = params.getParam("dateTo");
		List<JsonObject> elems = this.elementsFromTo(elements, dateFrom, dateTo);
	
		/* "numerosità totale" */ 
		int nTotRecs = elems.size();
		      
		/* "ammissioni primarie */
		int primaryAdmission = 0;
		
		/* "trasferiti in 24h */
		int secActTransfWithin24h = 0;
		int secActTransfAfter24h = 0;

		HashMap<String,Integer> fromOtherPSMap = new HashMap<String,Integer>();		
		fromOtherPSMap.put("ravenna", 0);
		fromOtherPSMap.put("lugo", 0);
		fromOtherPSMap.put("rimini", 0);
		fromOtherPSMap.put("faenza", 0);
		fromOtherPSMap.put("riccione", 0);
		fromOtherPSMap.put("forli", 0);
				
		
		double totAge = 0; 
		double totChildren = 0;
		int totOld = 0;
		int numMen = 0;		
		List<Double> ages = new ArrayList<Double>();
		
		
		/* trauma severity */
		
		int mais1 = 0;
		int mais2 = 0;
		int mais3 = 0;

		int totIss = 0;
		List<Double> isses = new ArrayList<Double>();
		int totIssGreater16 = 0;
		
		int totTraumaEmor = 0;
		int totTrumaCran = 0;
		int totPolit = 0;

		int nHelic = 0;

		int totGCS = 0;
		int totGCStracheo = 0;
		int totGCSnoTracheo = 0;
		List<Double> gcsList = new ArrayList<Double>();
		List<Double> gcsTracheoList = new ArrayList<Double>();
		List<Double> gcsNoTracheoList = new ArrayList<Double>();

		int nEcho = 0;
		int nRxTor = 0;
		int nTcCel = 0;
		int nTctorAbd = 0;
		
		long totTimeSRdestOR = 0;
		List<Double> timesSRdestORList = new ArrayList<Double>();

		long totTimeSRdestICU = 0;
		List<Double> timesSRdestICUList = new ArrayList<Double>();
		
		for (JsonObject el: elems) {
			
			String finalDestination = el.getString("finalDestination");

			JsonObject traumaInfo = el.getJsonObject("traumaInfo");
			String age = traumaInfo.getString("age");
			if (age != null) {
				int ageVal = Integer.parseInt(age);
				totAge += ageVal;
				ages.add((double) ageVal);
				
				if (ageVal <= 16) {
					totChildren++;
				} else if (ageVal >= 70) {
					totOld++;
				}				
			}

			String startTime = el.getString("startTime");
			String startDate = el.getString("startDate");
			
			
			String otherPS = traumaInfo.getString("fromOtherEmergency");
			boolean primaryAdmis = true;
			
			if (otherPS != null) {
				if (otherPS.toLowerCase().equals("no")) {
					primaryAdmission++;
				} else {
					primaryAdmis = false;
					String accTime = traumaInfo.getString("accidentTime");
					String accDate = traumaInfo.getString("accidentDate");
					try {
						long accAbsTimeInSec = this.getAbsTimeInSec(accDate, accTime);
						long traumaAbsStartTimeInSec = this.getAbsTimeInSec(startDate, startTime);
						long diff = traumaAbsStartTimeInSec - accAbsTimeInSec;
						if (diff <= 24*3600) {
							secActTransfWithin24h++;
						} else {
							secActTransfAfter24h++;
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					
					String ps =  traumaInfo.getString("otherEmergency");
					if (ps != null) {
						Integer value = fromOtherPSMap.get(ps.toLowerCase());
						if (value != null) {
							fromOtherPSMap.put(ps.toLowerCase(), value + 1);
						}
					}
				}
			}
			
			String gender = traumaInfo.getString("gender");
			if (gender != null) {
				if (gender.equals("maschio")) {
					numMen++;	
				}
			}
			
			JsonObject iss = traumaInfo.getJsonObject("iss");
			
			int maxIss = -1;
			JsonObject issHG = iss.getJsonObject("head-group");
			if (issHG != null) {
				maxIss = checkMax(issHG, "head-neck-ais",  maxIss);
				maxIss = checkMax(issHG, "brain-ais", maxIss);
				maxIss = checkMax(issHG, "cervical-spine-ais", maxIss);
			}
			JsonObject issFG = iss.getJsonObject("face-group");
			if (issFG != null) {
				maxIss = checkMax(issFG, "face-ais",  maxIss);
			}			
			JsonObject issTG = iss.getJsonObject("torax-group");
			if (issTG != null) {
				maxIss = checkMax(issTG, "torax-ais",  maxIss);
				maxIss = checkMax(issTG, "spine-ais", maxIss);
			}
			JsonObject issAG = iss.getJsonObject("abdomen-group");
			if (issAG != null) {
				maxIss = checkMax(issAG, "abdomen-ais",  maxIss);
				maxIss = checkMax(issAG, "lumbar-spine-ais", maxIss);
			}
			JsonObject issEG = iss.getJsonObject("extremities-group");
			if (issEG != null) {
				maxIss = checkMax(issEG, "upper-extremities-ais",  maxIss);
				maxIss = checkMax(issEG, "lower-extremities-ais",  maxIss);
				maxIss = checkMax(issEG, "pelvic-girdle-ais",  maxIss);
			}
			JsonObject issXG = iss.getJsonObject("externa-group");
			if (issXG != null) {
				maxIss = checkMax(issXG, "externa-ais",  maxIss);
			}
			
			if (maxIss == 1) {
				mais1++;
			} else if (maxIss == 2) {
				mais2++;
			} else if (maxIss >= 3) {
				mais3++;
			}
			
			String totalIss = iss.getString("total-iss");
			if (totalIss != null) {
				int issv = Integer.parseInt(totalIss);
				totIss += issv;
				isses.add((double) issv);
				
				if (issv >= 16) {
					totIssGreater16 ++;
				}
			}
			
			/* TODO: trauma emorragico */
			
			/* trauma cranico */
			
			if (issHG != null) {
				String issc = issHG.getString("brain-ais");
				if (issc != null) {
					int isscv = Integer.parseInt(issc);
					if (isscv >= 3) {
						totTrumaCran++;
					}
				}
			}
			
			/* politraumi */
			
			int ntr = 0;
			ntr = ntr + (checkMaxAIS(issHG, "head-neck-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "brain-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "cervical-spine-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "face-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "torax-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "spine-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "abdomen-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "lumbar-spine-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "upper-extremities-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "lower-extremities-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "pelvic-girdle-ais") ? 1 : 0);
			ntr = ntr + (checkMaxAIS(issHG, "externa-ais") ? 1 : 0);
			
			if (ntr >= 2) {
				totPolit++;
			}

			if (primaryAdmis) {
				/* arrivo in ospedale in elicottero */
	
				String vehicle =  traumaInfo.getString("vehicle");
				if (vehicle.equals("elicottero")) {
					nHelic++;
				}
				
				/* gcs */ 
				
				int gcsVal = -1;
				
				JsonObject preh = el.getJsonObject("preh");
				if (preh != null) {
					String gcs = preh.getString("dGcsTotal");
					if (gcs != null) {
						gcsVal = Integer.parseInt(gcs);
						totGCS += gcsVal;
						gcsList.add((double) gcsVal);
					}
				}
				
				JsonObject pat = el.getJsonObject("patientInitialCondition");
				if (pat != null) {
					JsonObject vs = pat.getJsonObject("clinicalPicture");
					Boolean trach = vs.getBoolean("positiveInhalation");
					if (trach != null) {
						if (trach.booleanValue()) {
							if (gcsVal > -1) {
								totGCStracheo += gcsVal;
								gcsTracheoList.add((double) gcsVal);
							}
						} else {
							if (gcsVal > -1) {
								totGCSnoTracheo += gcsVal;
								gcsNoTracheoList.add((double) gcsVal);
							}
						}				
					}
				}
				
				/* diag eco */
				
				JsonArray events = el.getJsonArray("events");
				Iterator<Object> it = events.iterator();
				
				boolean echo = false;
				boolean rxtor = false;
				boolean tcceleb = false;
				boolean tcabdom = false;
				
				while (it.hasNext()) {
					JsonObject ev = (JsonObject) it.next();
					String type = ev.getString("type");
					if (type.equals("diagnostic")) {
						JsonObject content = ev.getJsonObject("content");
						if (content != null) {
							String did = content.getString("diagnosticId");
							if (did.equals("echofast")) {
								echo = true;
							} else if (did.equals("chest-rx")) {
								rxtor = true;	
							} else if (did.equals("cerebral-cervical-tc")) {
								tcceleb = true;	
							} else if (did.equals("thoracoabdominal-tc")) {
								tcabdom = true;	
							}
						}
					}
				}
				
				if (echo) nEcho++;
				if (rxtor) nRxTor++;
				if (tcceleb) nTcCel++;
				if (tcabdom) nTctorAbd++;
				
				long totTimeInShockRoom = getTotalTimeInRoom(events, "Shock-Room");
				
				if (finalDestination.equals("Terapia Intensiva")) {
					totTimeSRdestICU += totTimeInShockRoom;
					timesSRdestICUList.add((double) totTimeInShockRoom);					
				} else if (finalDestination.startsWith("Altro")) {
					totTimeSRdestOR += totTimeInShockRoom;
					timesSRdestORList.add((double) totTimeInShockRoom);					
				}
			}			
			
		}
		
		/* compiling the report */
		
		JsonObject report = new JsonObject();
		
		/* STATISTICHE PAZIENTI */

		/* - ammissioni primarie/trasferimenti */
		
		report.put("numTot", nTotRecs);
		report.put("primAdmin", primaryAdmission);
		report.put("trasfWithin24h", secActTransfWithin24h);
		report.put("trasfAfter24h", secActTransfAfter24h);
		
		JsonArray spokes = new JsonArray();
		for (Entry<String, Integer> en: fromOtherPSMap.entrySet()) {
			spokes.add(new JsonObject().put(en.getKey(), en.getValue()));
		}
		report.put("trasfSpokes", spokes);

		/* - caratteristiche pazienti */
		
		report.put("agesStat", computeBasicStat(ages, totAge));
		report.put("children", totChildren);
		report.put("elderly", totOld);
		report.put("men", numMen);

		/* - severità trauma */
		
		report.put("mais1", mais1);
		report.put("mais2", mais2);
		report.put("mais3", mais3);
		
		report.put("iss", computeBasicStat(isses, totIss));
		report.put("iss_16plus", totIssGreater16);

		// report.put("traumaEmor", XXX); // TODO
		
		report.put("traumaCran", totTrumaCran);
		report.put("polit", totPolit);
		
		/*
		 * - Traumi Maggiori: TODO
		 * - Pazienti deceduti entro x ore: TODO
		 */
		
		/* PRE-OSPEDALIZZAZIONE: TODO */
		/* - meccanismo del trauma: TODO */
		/* - segni vitali: TODO */
		/* - condizioni al ritrovamento: TODO */
		/* - terapia: TODO */
		/* - somministrazioni */
		
		/* SHOCK ROOM */
		
		report.put("helicopter", nHelic);
		
		/* - gcs */
		report.put("gcsTracheo", computeBasicStat(gcsTracheoList, totGCStracheo));
		report.put("gcsNoTracheo", computeBasicStat(gcsNoTracheoList, totGCSnoTracheo));
		report.put("gcsTot", computeBasicStat(gcsList, totGCS));
		
		/* - diagnostica iniziale */
		
		report.put("echo", nEcho);
		report.put("rxTor", nRxTor);
		report.put("tcCelCer", nTcCel);
		report.put("tcTorAbd", nTctorAbd);

		// angio: TODO

		/* -TEMPI */
		
		/* tempo tra shock room e TC: TODO */
		
		/* tempo IN shock Room (DF= sala operatoria) */
		report.put("timeInSR_OR", computeBasicStat(timesSRdestORList, totTimeSRdestOR));
		
		/* tempo IN shock Room (DF= ICU) */
		report.put("timeInSR_ICU", computeBasicStat(timesSRdestICUList, totTimeSRdestICU));
		
		/* tempo tra shock room e damage control: TODO */
		
		/* tempo tra shock room e craniotomia (sop): TODO */
		
		/* TODO: */
		/* - Sanguinamento e Trasfusioni	 */
		// Pressione Arteriosa Sistolica (sBP) <= 90 mmHg
		// terapia Emostatica
		// somministrazione acido tranexamico
		// ROTEM
	
		/* Procedure	 */
		// Intubazione Orotracheale
		// Drenaggio Toracico
		// Reboa
		// inserire altri

		/* Destinazione Finale*/	
		
		// Pronto Soccorso
		// Terapia Intensiva
		// Camera Mortuaria
		// Altro
		
		/*
		
		 */
		return report;
	}
	
	private JsonObject computeBasicStat(List<Double> values, double sum) {
		JsonObject obj = new JsonObject();
		double avg = sum / values.size();
		
		obj.put("average", avg);
		
		obj.put("standDev", computeStandDev(values, avg));
		
		/* calcolo mediana */
		
		values.sort((Double a, Double b) -> a > b ? 1 : (a == b ? 0 : -1)); 
		
		double median = computeMedian(values, 0, values.size() - 1);
		obj.put("median", median);

		/* calcolo quartili */
		
		double q1 = 0;
		double q3 = 0;
		if (values.size() % 2 == 0) {
			q1 = computeMedian(values, 0, values.size() / 2 - 1);
			q3 = computeMedian(values, values.size() / 2, values.size() - 1);
		} else {
			q1 = computeMedian(values, 0, (values.size() - 1) / 2 - 1);
			q3 = computeMedian(values, (values.size() + 1) / 2, values.size() - 1);
		}
		
		double iqr = q3 - q1;		
		obj.put("iqr", iqr);
				
		return obj;
	}
	
	private double computeMedian(List<Double> values, int indexFrom, int indexTo) {
		double median = 0;
		int n = (indexTo - indexFrom) + 1;
		if (n % 2 == 0) {
			median = (values.get(indexFrom + n/2) + values.get(indexFrom + (n + 1)/2)) / 2.0;
		} else {
			median = values.get(indexFrom + (n + 1)/2);
		}
		return median;
	}
	
	private double computeStandDev(List<Double> elems, double avg) {
		double sum = 0;
		for (Double e: elems) {
			double diff = (e - avg); 
			sum += diff*diff;
		}
		sum = sum / elems.size();
		return Math.sqrt(sum);
	}

		
	private long getTotalTimeInRoom(JsonArray events, String room) {
		Iterator<Object> it = events.iterator();
		
		long totTime = 0;
		long absTimeFrom = -1;
		
		while (it.hasNext()) {
			JsonObject ev = (JsonObject) it.next();
			String date = ev.getString("date");
			String time = ev.getString("time");
			String type = ev.getString("type");
			if (type.equals("room-in") || (type.equals("patient-accepted") && room.equals("Shock-Room"))) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room) && absTimeFrom == -1) {
						absTimeFrom = getAbsTimeInSec(date, time);
					} else if (!place.equals(room) && absTimeFrom != -1) {
						/* changing the room without a room out event */
						long absTimeTo = getAbsTimeInSec(date, time);						
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			} else if (type.equals("room-out") && absTimeFrom != -1) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room)) {
						long absTimeTo = getAbsTimeInSec(date, time);
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			}
		}
		
		return totTime;
	}
	
	private long getAbsTimeInSec(String startDate, String startTime) {
		if (startDate != null && !startDate.equals("") && 
			startTime != null && !startTime.equals("")) {

			// date: // yyyy-mm-dd
			// time: // hh:mm:ss
			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
		    try {
		    		Date date = sdf.parse(startDate);
		    
				long startValue = 
						Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
	
			    return date.getTime()/1000 + startValue;
		    } catch (Exception ex) {
				throw new IllegalArgumentException("invalid date");
		    }
		} else {
			throw new IllegalArgumentException("invalid date");
		}
		
	}

	private int checkMax(JsonObject obj, String aisn, int maxIss) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv > maxIss) {
				return hnv;
			} else {
				return maxIss;
			}
		} else {
			return maxIss;
		}
	}

	private boolean checkMaxAIS(JsonObject obj, String aisn) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv >= 3) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


}
