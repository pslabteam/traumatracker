package it.unibo.disi.pslab.traumastat;

import java.util.Date;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.mongo.*;
import it.unibo.disi.pslab.traumastat.handlers.*;
import it.unibo.disi.pslab.traumastat.management.*;
import it.unibo.disi.pslab.traumatracker.util.*;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class TraumaStatService extends AbstractVerticle {

	private TTConfig config;
	private TSLogger ttLogger;
	private MongoClient store;
	private Router router;
	private Logger logger = LoggerFactory.getLogger(TraumaStatService.class);
	private Date startTime;
	
	private static final String API_BASE_PATH = "/gt2/traumastat/api";

	public TraumaStatService(TSLogger view, TTConfig config){
		this.config = config;
		this.ttLogger = view;
	}
	
	@Override
	public void start() {
		initAPI();		
		initDB();
		initWS();
	}
	
	
	private void initAPI() {
		router = Router.router(vertx);
		
		router.route().handler(CorsHandler.create("*")
				.allowedMethod(io.vertx.core.http.HttpMethod.GET)
				.allowedMethod(io.vertx.core.http.HttpMethod.POST)
				.allowedMethod(io.vertx.core.http.HttpMethod.PUT)
				.allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
				.allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
				.allowedHeader("Access-Control-Request-Method")
				.allowedHeader("Access-Control-Allow-Credentials")
				.allowedHeader("Access-Control-Allow-Origin")
				.allowedHeader("Access-Control-Allow-Headers")
				.allowedHeader("Content-Type"));

		router.route().handler(BodyHandler.create());
		
		// management 
		router.get(API_BASE_PATH + "/version").handler(new GetVersionHandler(this));
		router.get(API_BASE_PATH + "/state").handler(new GetVersionHandler(this));
				
		// statistics
		router.get(API_BASE_PATH + "/traumafreq").handler(new GetTraumaFreqHandler(this));
		router.get(API_BASE_PATH + "/traumalen").handler(new GetAverageTraumaLenHandler(this));		
		router.get(API_BASE_PATH + "/count").handler(new CountStatHandler(this));
		router.get(API_BASE_PATH + "/export").handler(new ExportStatHandler(this));
		router.get(API_BASE_PATH + "/export-aggr").handler(new ExportAggrStatHandler(this));
		router.get(API_BASE_PATH + "/export-room-data").handler(new ExportRoomStatHandler(this));
		router.get(API_BASE_PATH + "/def-indicators").handler(new DefIndicatorsHandler(this));
		router.get(API_BASE_PATH + "/indicators/").handler(new IndicatorsHandler(this));

		router.route("/gt2/traumatracker/static/*")
			.handler(StaticHandler.create()); 
		 
	}

	private void initDB() {		
		JsonObject mongoConfig = new JsonObject().put("db_name", config.getDBName()); //put("host", ipDB).put("port", portDB).;			
		store = MongoClient.createNonShared(vertx, mongoConfig);
		log("DB connection created.");
	}

	private void initWS() {
		vertx.createHttpServer()
			.requestHandler(router::accept)
			.listen(config.getPort(), result -> {
					if (result.succeeded()) {
						startTime = new Date();
						log("Ready.");
					} else {
						log("Failed: "+result.cause());
					}
				});		
	}

	
	

	public void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}
	
	public MongoClient getStore(){
		return store;		
	}
	
	public void log(String msg){
		logger.info("[TraumaStat] "+msg);
		// ttLogger.log("[TTService] "+msg);
	}

	public void logError(String msg){
		logger.error("[TraumaStat] "+msg);
		ttLogger.log("[TraumaStat] "+msg);
	}
	
	public TTConfig getConfig(){
		return config;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	public FileSystem getFS() {
		return vertx.fileSystem();
	}
	
}
