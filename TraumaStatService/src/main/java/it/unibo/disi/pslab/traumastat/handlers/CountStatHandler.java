package it.unibo.disi.pslab.traumastat.handlers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public class CountStatHandler extends GeneralStatHandler {

	public CountStatHandler(TraumaStatService service){
		super(service);
	}

	@Override
	protected JsonObject computeStat(Params params, List<JsonObject> elements) {
		String dateFrom = params.getParam("dateFrom");
		String dateTo = params.getParam("dateTo");
		List<JsonObject> elems = this.elementsFromTo(elements, dateFrom, dateTo);
			
		/* compiling the report */
		
		JsonObject report = new JsonObject();
		
		report.put("numTot", elems.size());

		return report;
	}
	
	private JsonObject computeBasicStat(List<Double> values, double sum) {
		JsonObject obj = new JsonObject();
		double avg = sum / values.size();
		
		obj.put("average", avg);
		
		obj.put("standDev", computeStandDev(values, avg));
		
		/* calcolo mediana */
		
		values.sort((Double a, Double b) -> a > b ? 1 : (a == b ? 0 : -1)); 
		
		double median = computeMedian(values, 0, values.size() - 1);
		obj.put("median", median);

		/* calcolo quartili */
		
		double q1 = 0;
		double q3 = 0;
		if (values.size() % 2 == 0) {
			q1 = computeMedian(values, 0, values.size() / 2 - 1);
			q3 = computeMedian(values, values.size() / 2, values.size() - 1);
		} else {
			q1 = computeMedian(values, 0, (values.size() - 1) / 2 - 1);
			q3 = computeMedian(values, (values.size() + 1) / 2, values.size() - 1);
		}
		
		double iqr = q3 - q1;		
		obj.put("iqr", iqr);
				
		return obj;
	}
	
	private double computeMedian(List<Double> values, int indexFrom, int indexTo) {
		double median = 0;
		int n = (indexTo - indexFrom) + 1;
		if (n % 2 == 0) {
			median = (values.get(indexFrom + n/2) + values.get(indexFrom + (n + 1)/2)) / 2.0;
		} else {
			median = values.get(indexFrom + (n + 1)/2);
		}
		return median;
	}
	
	private double computeStandDev(List<Double> elems, double avg) {
		double sum = 0;
		for (Double e: elems) {
			double diff = (e - avg); 
			sum += diff*diff;
		}
		sum = sum / elems.size();
		return Math.sqrt(sum);
	}

		
	private long getTotalTimeInRoom(JsonArray events, String room) {
		Iterator<Object> it = events.iterator();
		
		long totTime = 0;
		long absTimeFrom = -1;
		
		while (it.hasNext()) {
			JsonObject ev = (JsonObject) it.next();
			String date = ev.getString("date");
			String time = ev.getString("time");
			String type = ev.getString("type");
			if (type.equals("room-in") || (type.equals("patient-accepted") && room.equals("Shock-Room"))) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room) && absTimeFrom == -1) {
						absTimeFrom = getAbsTimeInSec(date, time);
					} else if (!place.equals(room) && absTimeFrom != -1) {
						/* changing the room without a room out event */
						long absTimeTo = getAbsTimeInSec(date, time);						
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			} else if (type.equals("room-out") && absTimeFrom != -1) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room)) {
						long absTimeTo = getAbsTimeInSec(date, time);
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			}
		}
		
		return totTime;
	}
	
	private long getAbsTimeInSec(String startDate, String startTime) {
		if (startDate != null && !startDate.equals("") && 
			startTime != null && !startTime.equals("")) {

			// date: // yyyy-mm-dd
			// time: // hh:mm:ss
			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
		    try {
		    		Date date = sdf.parse(startDate);
		    
				long startValue = 
						Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
	
			    return date.getTime()/1000 + startValue;
		    } catch (Exception ex) {
				throw new IllegalArgumentException("invalid date");
		    }
		} else {
			throw new IllegalArgumentException("invalid date");
		}
		
	}

	private int checkMax(JsonObject obj, String aisn, int maxIss) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv > maxIss) {
				return hnv;
			} else {
				return maxIss;
			}
		} else {
			return maxIss;
		}
	}

	private boolean checkMaxAIS(JsonObject obj, String aisn) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv >= 3) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


}
