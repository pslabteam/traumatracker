package it.unibo.disi.pslab.traumastat;

import java.util.Iterator;
import io.vertx.core.json.JsonObject;

public class EventGen {

	private JsonObject report;
	
	public EventGen(JsonObject report) {
		this.report = report;
	}
	
	public void analyseEvents() {
		int index = 0;
		Iterator<Object> it = report.getJsonArray("events").iterator();
		while (it.hasNext()) {
			JsonObject ev = (JsonObject) it.next();
			System.out.println("ev #" + index + " " + ev.getString("time") + " " + ev.getString("type"));
			index++;
		}
	}
}
