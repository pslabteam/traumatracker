package it.unibo.disi.pslab.traumastat.management;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.traumastat.AbstractServiceHandler;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public class GetStateHandler extends AbstractServiceHandler {

	public GetStateHandler(TraumaStatService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling GetState from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		JsonObject reply = new JsonObject()
							.put("version", getConfig().getVersion())	
							.put("startTime", getStartTime());
		response.putHeader("content-type", "application/json").end(reply.encodePrettily());		
	}	
}
