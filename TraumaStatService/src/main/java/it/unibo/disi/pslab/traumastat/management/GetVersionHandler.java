package it.unibo.disi.pslab.traumastat.management;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.traumastat.AbstractServiceHandler;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public class GetVersionHandler extends AbstractServiceHandler {

	public GetVersionHandler(TraumaStatService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Get Version from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		response.putHeader("content-type", "application/text").end(this.getConfig().getVersion());
	}	
}
