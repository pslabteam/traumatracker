package it.unibo.disi.pslab.traumastat.handlers;

public interface Params {	
	String getParam(String paramName);
}
