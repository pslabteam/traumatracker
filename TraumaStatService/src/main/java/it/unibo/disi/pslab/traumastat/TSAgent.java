package it.unibo.disi.pslab.traumastat;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumatracker.util.TTConfig;

public class TSAgent extends AbstractVerticle {

	private String tsServiceAddress = "localhost";
	private TSLogger view;
	private TTConfig config;
	private Vertx vertx;
	
	public TSAgent(TSLogger view, TTConfig config) {
		this.view = view;
		this.config = config;
		vertx = Vertx.vertx();
	}
	
	public void start(){
		log("Setting up TraumaStat Service ...");
		setupService();
		/*
		getVertx().setPeriodic(10000, id -> {
			log("Check service.");
			HttpClient client = getVertx()
				.createHttpClient(new HttpClientOptions().setDefaultHost(tsServiceAddress).setDefaultPort(config.getPort()));				
			client.get("http://"+tsServiceAddress + "/gt2/traumastat/api/state")
				.handler(this::handleGetReportInfo)
				.exceptionHandler(h -> {
					log("exception raised " + h);
					this.getVertx().setTimer(5000, id2 -> {
						setupService();
					});					
				})
			.end();
		});*/
	};

	protected void handleGetReportInfo(HttpClientResponse res) {
		log("Check service result: " + res.statusCode());
		res.bodyHandler(data -> {
			JsonObject ob = data.toJsonObject();			
			log("Check service state - version: " + ob.getString("version")+ " | startTime: "+ob.getString("startTime"));
		});
	}

	
	private void setupService(){
		try {
			TraumaStatService ttService = new TraumaStatService(view,config);
			this.getVertx().deployVerticle(ttService);
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

	private void log(String msg){
		System.out.println("[TSServiceAgent] "+msg);
		view.log("[TSServiceAgent] "+msg);
	}	
	
}
