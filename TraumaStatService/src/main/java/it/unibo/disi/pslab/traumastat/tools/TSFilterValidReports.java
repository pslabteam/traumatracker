package it.unibo.disi.pslab.traumastat.tools;

import java.io.FileWriter;
import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class TSFilterValidReports extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(TSFilterValidReports.class);
	private Vertx vertx;
	private String targetFile;
	
	public TSFilterValidReports( String targetFile){		
		this.targetFile = targetFile;
	}
	
	@Override
	public void start() {
		vertx = this.getVertx();
		JsonObject mongoConfig = new JsonObject().put("db_name", "ttservicedb"); //put("host", ipDB).put("port", portDB).;			
		store = MongoClient.createNonShared(vertx, mongoConfig);
		log("DB connection created.");
		
		JsonObject query = new JsonObject();
		System.out.println("Fetching reports...");
		store.find("reports", query, res -> {
			// System.out.println("Got something "+(res.succeeded() ? "ok" : res.cause()));
			if (res.succeeded()) {				
				List<JsonObject> reps = res.result();				
				// System.out.println("N. tot reps " + reps.size());

				List<JsonObject> goodReports = reps.stream().filter(rep -> {
					JsonObject iss = rep.getJsonObject("iss");
					if (iss != null) {
						String totalIss = iss.getString("totalIss");
						if (totalIss != null && !totalIss.trim().equals("")) {
							// System.out.println(totalIss);
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}).collect(Collectors.toList());

				System.out.println("Writing file: "+targetFile);
				try {
					FileWriter writer = new FileWriter(targetFile);
					writer.write("[ ");
					for (int i = 0; i < goodReports.size() - 1; i++) {
						writer.write(goodReports.get(i) + ",\n");
					}
					writer.write(goodReports.get(goodReports.size() - 1) + "]");
					System.out.println("Done.");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				// System.out.println("N. reps " + goodReports.size());
				
			} else {
			}
		});
		
	}
	
	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static void main(String[] args) throws Exception {
		String destFileName = "data/valid-reports.json";
		if (args.length == 1) {
			destFileName = args[0];
		}
		Vertx vertx = Vertx.vertx();
		TSFilterValidReports app = new TSFilterValidReports(destFileName);
		vertx.deployVerticle(app);				
	}
		
}
