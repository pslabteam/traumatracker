package it.unibo.disi.pslab.traumastat;

import io.vertx.core.Vertx;
import it.unibo.disi.pslab.traumatracker.util.TTConfig;

public class TSLauncher {

	private static final String TT_CONFIG_FILE = "./config.json";
	
	public static void main(String[] args) throws Exception {
				
		System.out.println("Booting TraumaStat Service.");

		TTConfig config;
		try {
			String configFile = TT_CONFIG_FILE;
			if (args.length == 1) {
				configFile = args[0];
			}
			config = new TTConfig(configFile);
		} catch (Exception ex){
			System.err.println("Config file not found or invalid");
			config = new TTConfig();
		}
		
		TSLogger logger = new TSLogger(config.isLogGUIEnabled());
		logger.log("Booting TraumaStat service.");

		// new VPNManagerAgentOSX(logger,config).start();
		// Thread.sleep(2000);

		Vertx vertx = Vertx.vertx();
		
		// MongoDBAgent mongodb = new MongoDBAgent(logger,config);
		// vertx.deployVerticle(mongodb);
		// Thread.sleep(5000);
		
		TSAgent srvag = new TSAgent(logger,config);
		vertx.deployVerticle(srvag);

		logger.log("TraumaStat Booting Completed.");
	}
}
