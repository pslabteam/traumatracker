package it.unibo.disi.pslab.traumastat.handlers;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.traumastat.AbstractServiceHandler;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public class GetTraumaFreqHandler extends AbstractServiceHandler {

	public GetTraumaFreqHandler(TraumaStatService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		// log("Handling Get Reports from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		// log("handle get reports");
		JsonObject query = new JsonObject();
		
		JsonObject options = new JsonObject();
		options.put("sort", new JsonObject().put("startDate",-1).put("startTime",-1));
		
		// log("doing a query: \n" + query.encodePrettily());
		findInReportsWithOptions(query, options, res -> {
			if (res.succeeded()) {
				HashMap<String,Integer> freq = new HashMap<String,Integer>();
				for (JsonObject rep : res.result()) {
					String date = rep.getString("startDate").substring(0, 7);
					Integer value = freq.get(date);
					if (value != null){
						freq.put(date, value + 1);
					} else {
						freq.put(date, 1);
					}
				}
				
				List<Entry<String,Integer>> elems = freq.entrySet().stream().sorted(
						(Entry<String,Integer> a, Entry<String,Integer> b) -> a.getKey().compareTo(b.getKey())).collect(Collectors.toList());
				
				
				JsonObject stat = new JsonObject(); 
				JsonArray dataX = new JsonArray();
				JsonArray dataY = new JsonArray();
				for (Entry<String, Integer> elem: elems){
					dataX.add(elem.getKey());
					dataY.add(elem.getValue());
				}
				stat.put("x", dataX);
				stat.put("y", dataY);
				response.putHeader("content-type", "application/json").end(stat.encodePrettily());
			} else {
				sendError(404, response);
			}
		});
	}
	
}
