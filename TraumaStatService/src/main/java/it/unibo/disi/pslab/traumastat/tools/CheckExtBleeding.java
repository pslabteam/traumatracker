package it.unibo.disi.pslab.traumastat.tools;

import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class CheckExtBleeding extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(CheckExtBleeding.class);
	private Vertx vertx;
	private String targetFile;
	
	public CheckExtBleeding(){		
	}
	
	@Override
	public void start() {
		vertx = this.getVertx();
		JsonObject mongoConfig = new JsonObject().put("db_name", "ttservicedb"); //put("host", ipDB).put("port", portDB).;			
		store = MongoClient.createNonShared(vertx, mongoConfig);
		log("DB connection created.");
		
		JsonObject query = new JsonObject();
		System.out.println("Fetching reports...");
		store.find("reports", query, res -> {
			// System.out.println("Got something "+(res.succeeded() ? "ok" : res.cause()));
			if (res.succeeded()) {				
				List<JsonObject> reps = res.result();				
				List<JsonObject> extBleeding = reps.stream().filter(rep -> {
					JsonObject pat = rep.getJsonObject("patientInitialCondition");
					if (pat != null) {
						JsonObject cp = rep.getJsonObject("clinicalPicture");
						if (cp != null) {
							Boolean isBleeding = cp.getBoolean("hemorrhage");
							if (isBleeding != null && isBleeding.booleanValue()) {
								return true;
							} else {
								return false;
							}
						} else {
							return false;
						}
					} else {
						return false;
					}
				}).collect(Collectors.toList());

				
				System.out.println("Num trauma with ext bleeding: " + extBleeding.size());
				extBleeding.forEach(rep -> {
					System.out.println(rep.getString("_id"));
				});
			} else {
			}
		});
		
	}
	
	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		CheckExtBleeding app = new CheckExtBleeding();
		vertx.deployVerticle(app);				
	}
		
}
