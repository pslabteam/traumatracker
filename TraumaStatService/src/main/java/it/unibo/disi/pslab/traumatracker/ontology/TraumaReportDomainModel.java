package it.unibo.disi.pslab.traumatracker.ontology;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class TraumaReportDomainModel {

	public final String VERSION 	= "1.3";
	public enum Gender { M, F };
	public enum Vehicle { HELI, AUTOMED, AMBU};
	public enum FinalDest { PS, TI, MU, OTHER};
	public final String SHOCK_ROOM 	= "Shock-Room";
	public final String TAC_PS 		= "TAC PS";
	public final String OP_ROOM 	= "Sala Operatoria: Blocco";
	public final String OP_ROOM_NCH 	= "Sala Operatoria: Neurochirurgia";
	public final String PUPILS_ANI = "Aniso";
	public final List<String> DAMAGE_CONTROL_PROCS = Arrays.asList("thoracotomy", "pelvic-packing", "fixator", "reboa");
	public final List<String> PROC_ER = Arrays.asList("intubation", "supraglottic-presidium", "drainage", 
										"chest-tube",  "fixator", "reboa", "thoracotomy", "als", "pelvic-binder", "pelvic-packing");
		
	private JsonObject report;
	private JsonArray events, vs;
	
	public TraumaReportDomainModel(JsonObject report){
		this.report = report;
		this.events = report.getJsonArray("events");
		this.vs = report.getJsonArray("vitalSignsObservations");
	}
 	
	public long getAbsStartTimeInSec() {
		String st = report.getString("startTime");
		String sd = report.getString("startDate");
		return this.getAbsTimeInSec(sd, st);
	}
	
	public Optional<Gender> getGender() {
		Optional<String> gen = this.getNestedString("traumaInfo","gender");
		if (gen.isPresent()) {
			return Optional.of(gen.get().equals("M") ? Gender.M : Gender.F);
		} else {
			return Optional.empty();
		}
	}
	
	public long getAbsEndTimeInSec() {
		try {
			String et = report.getString("endTime");
			String ed = report.getString("endDate");
			return this.getAbsTimeInSec(ed, et);
		} catch (Exception ex) {
			System.out.println("Invalid end time in: " + report.getString("_id"));
			throw ex;
		}
	}
	
	public Optional<Integer> getAge() {
		return this.getNestedInt("traumaInfo","age");
	}
	
	public Optional<Long> getAccidentAbsTimeInSec() {
		Optional<String> _date = this.getNestedString("traumaInfo","accidentDate");
		Optional<String> _time = this.getNestedString("traumaInfo","accidentTime");
		if (_date.isPresent() && _time.isPresent()) {
			return Optional.of(this.getAbsTimeInSec(_date.get(), _time.get()));
		} else {
			return Optional.empty();
		}
	}
	
	public Optional<String> getProvenienceSpoke() {
		Optional<Boolean> other = this.getNestedBoolean("traumaInfo","fromOtherEmergency");
		if (other.isPresent() && other.get()) {
			return this.getNestedString("traumaInfo","otherEmergency");
		} else {
			return Optional.empty();
		}
	}
	
	private Optional<String> getNestedString(String... labels) {
		JsonObject ob = report;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		String v = ob.getString(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);

	}

	private Optional<Boolean> getNestedBoolean(String... labels) {
		JsonObject ob = report;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		Boolean v = ob.getBoolean(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);
	}

	private Optional<Integer> getNestedInt(String... labels) {
		JsonObject ob = report;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		Integer v = ob.getInteger(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);
	}
	
	private Optional<JsonObject> getNestedObject(String... labels) {
		JsonObject ob = report;
		for (int i = 0; i < labels.length - 1; i++) {
			ob = ob.getJsonObject(labels[i]);
			if (ob == null) {
				return Optional.empty();
			}
		}
		JsonObject v = ob.getJsonObject(labels[labels.length - 1]);
		return v == null ? 	Optional.empty() : Optional.of(v);
	}

		
	public Optional<Long> getFirstTimeInRoomInSec(String roomName) {
		Optional<?> roomIn = null;
		if (roomName.equals(SHOCK_ROOM)) {
			roomIn = events.stream().filter(obj -> {
				JsonObject ev = (JsonObject) obj;
				return (ev.getString("type").equals("room-in") && ev.getString("place").equals(SHOCK_ROOM)) || ev.getString("type").equals("patient-accepted");
			}).findFirst();
		} else {
			roomIn = events.stream().filter(obj -> {
				JsonObject ev = (JsonObject) obj;
				return (ev.getString("type").equals("room-in") && ev.getString("place").equals(roomName));
			}).findFirst();
		}
		if (roomIn.isPresent()) {
			JsonObject in = (JsonObject) roomIn.get();
			String inDate = in.getString("date");
			String inTime = in.getString("time");
			return Optional.of(getAbsTimeInSec(inDate, inTime));
		} else {
			return Optional.empty();
		}
 
	}
		
	public boolean inRoom(String roomName) {
		if (roomName.equals(SHOCK_ROOM)) {
			return events.stream().anyMatch(obj -> {
				JsonObject ev = (JsonObject) obj;
				return (ev.getString("type").equals("room-in")  && ev.getString("place").equals(SHOCK_ROOM)) || ev.getString("type").equals("patient-accepted");
			});
		} else {
			return events.stream().anyMatch(obj -> {
				JsonObject ev = (JsonObject) obj;
				return ev.getString("type").equals("room-in") && ev.getString("place").equals(roomName);
			});
		}
	}

	public Optional<Boolean> isCarAccident() {
		return getNestedBoolean("preh", "isCarAccident");
	}

	public Optional<FinalDest> getFinalDest() {
		Optional<String> fd = this.getNestedString("finalDestination");
		if (fd.isPresent()) {
			String d = fd.get();
			if (d.equals("Pronto Soccorso")) {
				return Optional.of(FinalDest.PS);
			} else if (d.equals("Terapia Intensiva")) {
				return Optional.of(FinalDest.TI);
			} else if (d.equals("Medicina d'Urgenza")) {
				return Optional.of(FinalDest.MU);
			} else {
				return Optional.of(FinalDest.OTHER);
			}
		} else {
			return Optional.empty();
		}
	}
	public Optional<Vehicle> getArrivalVehicle(){
		Optional<String> veh = this.getNestedString("traumaInfo","vehicle");
		if (veh.isPresent()) {
			String ve = veh.get();
			if (ve.equals("Elicottero")) {
				return Optional.of(Vehicle.HELI);
			} else if (ve.equals("Automedica")) {
				return Optional.of(Vehicle.AUTOMED);
			} else if (ve.equals("Ambulanza")) {
				return Optional.of(Vehicle.AMBU);
			} else {
				return Optional.empty();
			}
		} else {
			return Optional.empty();
		}
		
	}
	
	public Optional<Boolean> isAnisocoric() {
		Optional<String> pupils = getNestedString("clinicalPicture", "pupils");
		if (pupils.isPresent()) {
			return Optional.of(pupils.get().equals("Aniso"));
		} else {
			return Optional.empty();
		}
	}

	public Optional<Boolean> isMidriatic() {
		Optional<String> pupils = getNestedString("clinicalPicture", "pupils");
		if (pupils.isPresent()) {
			return Optional.of(pupils.get().equals("Midriasi"));
		} else {
			return Optional.empty();
		}
	}
	
	public boolean isLowGCS() {
		Optional<Integer> gcs = getNestedInt("patientInitialCondition", "clinicalPicture", "gcsTotal");
		return (gcs.isPresent() && gcs.get() < 9);
	}
	
	public boolean isArrivedIntubated() {
		Optional<String> intu = getNestedString("preh", "aValue");
		return (intu.isPresent() && (intu.get().equals("TOT") || intu.get().equals("SGA")));
	}
	
	public boolean isPupilsAniso() {
		Optional<String> pupils = getNestedString("patientInitialCondition", "clinicalPicture", "pupils");
		return pupils.isPresent() && pupils.get().equals(PUPILS_ANI);

	}

	public Optional<Integer> getIss() {
		return this.getNestedInt("iss","totalIss");
	}
	
	public boolean isMajorTrauma() {
		boolean isMaj = false;
		Optional<String> a = getNestedString("preh", "aValue");
		Optional<Boolean> b = getNestedBoolean("preh", "bPleuralDecompression");
		Optional<Boolean> c1 = getNestedBoolean("preh", "cBloodProtocol");
		Optional<Boolean> c2 = getNestedBoolean("preh", "cTpod");
		Optional<Boolean> d1 = getNestedBoolean("preh", "dAnisocoria");
		Optional<Boolean> d2 = getNestedBoolean("preh", "dMidriasi");
		Optional<Boolean> e = getNestedBoolean("preh", "eMotility");
		Optional<Integer> gcs = getNestedInt("preh", "dGcsTotal"); 

		if (a.isPresent()) {
			isMaj = isMaj || (a.equals("TOT") || a.equals("SGA") || b.equals("CT"));
		}
	
		isMaj = isMaj || (b.isPresent() && b.get());
		isMaj = isMaj || (c1.isPresent() && c1.get());
		isMaj = isMaj || (c2.isPresent() && c2.get());
		isMaj = isMaj || (d1.isPresent() && d1.get());
		isMaj = isMaj || (d2.isPresent() && d2.get());
		isMaj = isMaj || (e.isPresent() && e.get());
		isMaj = isMaj || (gcs.isPresent() && gcs.get() < 14);

		return isMaj;
	}
	
	public boolean isTraumaHead() {
		Optional<Integer> ais = getNestedInt("iss", "headGroup", "brainAis");
		return ais.isPresent() && ais.get() >= 3;
	}

	public boolean isTraumaPoli() {
		int count = 0;
		Optional<Integer> a = getNestedInt("iss", "headGroup", "headNeckAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "headGroup", "brainAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "headGroup", "cervicalSpineAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "faceGroup", "faceAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "toraxGroup", "toraxAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "toraxGroup", "spineAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "abdomenGroup", "abdomenAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "abdomenGroup", "lumbarSpineAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "extremitiesGroup", "upperExtremitiesAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "extremitiesGroup", "lowerExtremitiesAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "extremitiesGroup", "pelvicGirdleAis");
		if (a.isPresent() && a.get() >= 3) count++;
		a = getNestedInt("iss", "externaGroup", "externaAis");
		if (a.isPresent() && a.get() >= 3) count++;
		return count >= 2;
	}

	public boolean isShockEmo() {
		Optional<String> bp = getNestedString("patientInitialCondition", "vitalSigns", "bp");
		Optional<Boolean> hem = getNestedBoolean("patientInitialCondition", "clinicalPicture", "hemorrhage");
					
		boolean bloodPressCond = false;
		boolean hemorrage = false;
		boolean pas = false;
		boolean beLow = false;
		boolean latHigh = false;
		
		bloodPressCond = bp.isPresent() && bp.get().equals("Ipoteso");
		
		hemorrage = hem.isPresent() && hem.get();				

		pas = vs.stream().anyMatch(obj -> {
			JsonObject ob = (JsonObject) obj;
			return ob.getJsonArray("vitalsigns").stream().anyMatch(sig -> {
				try {
					JsonObject s = (JsonObject) sig;
					String sname = s.getString("type");
					double val = (double) (Double.parseDouble(s.getString("value")));
					return (sname.equals("SYS") || sname.equals("NBP-S")) && (val < 90);
				} catch (Exception ex) {
					return false;
				}
			});
		});
		
		beLow = events.stream().anyMatch(obj -> {
			JsonObject ev = (JsonObject) obj;
			if (ev.getString("type").equals("diagnostic")) {
				JsonObject content = ev.getJsonObject("content");
				if (content.getString("diagnosticId").equals("abg")) {
					Integer be = content.getInteger("be");
					return (be != null && be <= -6);
				} 
			}
			return false;		
		});
		
		latHigh = events.stream().anyMatch(obj -> {
			JsonObject ev = (JsonObject) obj;
			if (ev.getString("type").equals("diagnostic")) {
				JsonObject content = ev.getJsonObject("content");
				if (content.getString("diagnosticId").equals("abg")) {
					Integer lat = content.getInteger("lactates");
					return (lat != null && lat >= 5);
				} 
			}
			return false;		
		});
		
		int nConds = 0;
		if (bloodPressCond) nConds++;
		if (hemorrage) nConds++;
		if (pas) nConds++;
		if (beLow) nConds++;
		if (latHigh) nConds++;
		
		return nConds >= 2;		
	}
		
	public long getTotalTimeInRoomInSec(String room) {
		Iterator<Object> it = events.iterator();
		
		long totTime = 0;
		long absTimeFrom = -1;
		
		while (it.hasNext()) {
			JsonObject ev = (JsonObject) it.next();
			String date = ev.getString("date");
			String time = ev.getString("time");
			String type = ev.getString("type");
			if (type.equals("room-in") || (type.equals("patient-accepted") && room.equals("Shock-Room"))) {
				if (type.equals("room-in")) {
					JsonObject content = ev.getJsonObject("content");
					if (content != null) {
						String place = content.getString("place");
						if (place.equals(room) && absTimeFrom == -1) {
							absTimeFrom = getAbsTimeInSec(date, time);
						} else if (!place.equals(room) && absTimeFrom != -1) {
							/* changing the room without a room out event */
							long absTimeTo = getAbsTimeInSec(date, time);						
							totTime = absTimeTo - absTimeFrom;
							absTimeFrom = -1;
						}
					}
				} else {
					absTimeFrom = getAbsTimeInSec(date, time);
				}
			} else if (type.equals("room-out") && absTimeFrom != -1) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room)) {
						long absTimeTo = getAbsTimeInSec(date, time);
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			}
		}
		
		return totTime;
	}
	
	public Optional<Long> getFirstTimeInDamageControlInSec() {
		
		/* @TODO aggiungere anche final destination? */
		/* @TODO aggiungere embolizzazione */
	
		Optional<Long> time = getFirstTimeInRoomInSec(OP_ROOM); 
		
		Optional<?> evs = events.stream()
			.filter(ev -> ((JsonObject) ev).getString("type").equals("procedure"))
			.filter(ev -> DAMAGE_CONTROL_PROCS.stream()
				.anyMatch(el -> el.equals((((JsonObject) ev)
												.getJsonObject("content")
							 					.getString("procedureId")))))
			.findFirst();
		
		if (evs.isPresent()) {
			long timeproc = getAbsTimeInSec((JsonObject) evs.get());
			if (time.isPresent()) {
				return Optional.of(Math.min(time.get(), timeproc));
			} else {
				return Optional.of(timeproc);
			}
		} else {
			return time.isPresent() ? Optional.of(time.get()) : Optional.empty();
		}
	}

	public Optional<Long> getTimeFirstERProcInSec() {
		
		Optional<?> evs = events.stream()
			.filter(ev -> ((JsonObject) ev).getString("type").equals("procedure"))
			.filter(ev -> PROC_ER.stream()
				.anyMatch(el -> el.equals((((JsonObject) ev)
												.getJsonObject("content")
							 					.getString("procedureId")))))
			.findFirst();

		return evs.isPresent() ? Optional.of(getAbsTimeInSec((JsonObject) evs.get())) : Optional.empty();
	}
	
	
	public boolean appliedProc(String procName) {
		
		return  events.stream()
				.filter(ev -> ((JsonObject) ev).getString("type").equals("procedure"))
				.anyMatch(ev -> ((JsonObject) ev)
									.getJsonObject("content")
								 	.getString("procedureId").equals(procName));
	}
	
	public JsonObject getReport() {
		return report;
	}
	
	public boolean isFromOtherPS() {
		Optional<Boolean> val = this.getNestedBoolean("traumaInfo","fromOtherEmergency");
		if (!val.isPresent()) {
			// System.out.println("HERE");
			return false;
		} else {
			return val.get();
		}
	}
	
	
	/* update */
	
	public void validateReport(String operatorId, String validationDate, String validationTime) throws ReportAlreadyValidatedException {
		JsonObject val = report.getJsonObject("_validation");
		boolean isValidated = val.getBoolean("isValidated");
		if (!isValidated) {
			val.put("isValidated", true);
			val.put("operatorId", operatorId);
			val.put("validationDate", validationDate);
			val.put("validationTime", validationTime);
			report.put("_validation", val);
			
			JsonObject iss = report.getJsonObject("iss");
			iss.put("isPresumed", false);
			report.put("iss", iss);
		} else {
			throw new ReportAlreadyValidatedException();
		}
	}
	
	public void updateVitalSigns(JsonObject vs) {
		JsonObject sez = report.getJsonObject("patientInitialCondition");
		sez.put("vitalSigns", vs);
		report.put("patientInitialCondition", sez);
	}

	public void updateClinicalPicture(JsonObject cp) {
		JsonObject sez = report.getJsonObject("patientInitialCondition");
		sez.put("clinicalPicture", cp);
		report.put("patientInitialCondition", sez);
	}

	public void updateISS(JsonObject iss) {
		report.put("iss", iss);
	}
	
	/* util */

	private long getAbsTimeInSec(JsonObject ev) {
		String time = ev.getString("time");
		String date = ev.getString("date");
		return getAbsTimeInSec(date, time);
	}	

	
	public long getAbsTimeInSec(String startDate, String startTime) {
		if (startDate != null && !startDate.equals("") && 
			startTime != null && !startTime.equals("")) {

			// date: // yyyy-mm-dd
			// time: // hh:mm:ss
			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
		    try {
		    		Date date = sdf.parse(startDate);
		    
				long startValue = 
						Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
	
			    return date.getTime()/1000 + startValue;
		    } catch (Exception ex) {
				throw new IllegalArgumentException("invalid date or time: " + startDate + " " + startTime);
		    }
		} else {
			throw new IllegalArgumentException("invalid date: " + startDate);
		}
		
	}
	
	public int getMonth(String sdate) {
		return Integer.parseInt(sdate.substring(5,7));				
	}

	public int getYear(String sdate) {
		return Integer.parseInt(sdate.substring(0,4));				
	}

	
}
