package it.unibo.disi.pslab.traumastat;

import java.io.FileWriter;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TSLogger  {

	private TSLoggerView view;
	private FileWriter logFile;
	private boolean hasGUI;
	private Executor exec;
	
	public TSLogger(boolean hasGUI) {
		this.hasGUI = hasGUI;
		exec = Executors.newSingleThreadExecutor();
		if (hasGUI){
			view = new TSLoggerView();
			view.setVisible(true);
		}
		try {
			logFile = new FileWriter("logTT-"+(new Date().toString().replace(" ", "-").replace(":", ""))+".txt");
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void log(String msg){
		String logmsg = "["+new Date()+"] "+msg;
		if (hasGUI){
			view.log(logmsg);	
		} else {
			synchronized (System.out) {
				System.out.println(logmsg);
			}
		}
		exec.execute(() -> {
			try {
				synchronized(logFile){
					logFile.write(logmsg+"\n");
					logFile.flush();
				}
			} catch (Exception ex){
				ex.printStackTrace();
			}
		});
	}

}
