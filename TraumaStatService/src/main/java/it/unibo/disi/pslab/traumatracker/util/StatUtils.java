package it.unibo.disi.pslab.traumatracker.util;

import java.util.List;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class StatUtils {

	static public JsonObject computeBasicStat(List<Double> values) {
		JsonObject obj = new JsonObject();
		
		obj.put("nValues", values.size());
		if (values.size() > 0) {

			try {
				double sum = 0;
				JsonArray vals = new JsonArray();
				for (double v: values) {
					vals.add(v);
					sum += v;
				}
				obj.put("values", vals);
				double avg = sum / values.size();
				
				obj.put("mean", Math.round(avg));
				
				obj.put("standDev", Math.round(computeStandDev(values, avg)));
				
				/* calcolo mediana */
				
				try {
					values.sort((Double a, Double b) -> a.compareTo(b)); 
				
					double median = computeMedian(values, 0, values.size() - 1);
					obj.put("median", Math.round(median));
			
					/* calcolo quartili */
					
					if (values.size() >= 2) {
						double q1 = 0;
						double q3 = 0;
						if (values.size() % 2 == 0) {
							q1 = computeMedian(values, 0, values.size() / 2 - 1);
							obj.put("q1", Math.round(q1));
							q3 = computeMedian(values, values.size() / 2, values.size() - 1);
							obj.put("q1", Math.round(q3));
						} else {
							q1 = computeMedian(values, 0, (values.size() - 1) / 2 - 1);
							obj.put("q1", Math.round(q1));
							q3 = computeMedian(values, (values.size() + 1) / 2, values.size() - 1);
							obj.put("q3", Math.round(q3));
						}
						
						double iqr = q3 - q1;		
						obj.put("iqr", Math.round(iqr));
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				System.out.println(values);
			}
		}
		
		return obj;
	}
	
	static public double computeMedian(List<Double> values, int indexFrom, int indexTo) {
		double median = 0;
		int n = (indexTo - indexFrom) + 1;
		if (n % 2 == 0) {
			median = (values.get(indexFrom + n/2) + values.get(indexFrom + (n + 1)/2)) / 2.0;
		} else {
			median = values.get(indexFrom + (n - 1)/2);
		}
		return median;
	}
	
	static public double computeStandDev(List<Double> elems, double avg) {
		double sum = 0;
		for (Double e: elems) {
			double diff = (e - avg); 
			sum += diff*diff;
		}
		sum = sum / elems.size();
		return Math.sqrt(sum);
	}
}
