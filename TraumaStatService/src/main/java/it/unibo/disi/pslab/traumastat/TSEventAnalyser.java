package it.unibo.disi.pslab.traumastat;

import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class TSEventAnalyser extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(TSEventAnalyser.class);
	private Vertx vertx;
	
	public TSEventAnalyser(){		
	}
	
	@Override
	public void start() {
		vertx = this.getVertx();
		JsonObject mongoConfig = new JsonObject().put("db_name", "ttservicedb2"); 			
		store = MongoClient.createNonShared(vertx, mongoConfig);
		log("DB connection created.");
		
		JsonObject query = new JsonObject();
		store.find("reports", query, res -> {
			if (res.succeeded()) {				
				List<JsonObject> reps = res.result();				
				System.out.println("N. tot reps " + reps.size());

				/* select valid reports */
				List<JsonObject>  validReps = filterValidReports(reps);				

				validReps.stream().forEach(rep -> {
					EventGen gen = new EventGen(rep);
					gen.analyseEvents();
				});
				
			} else {
			}
		});
		
	}
	
	
	private List<JsonObject>  filterValidReports(List<JsonObject> reps) {
		return reps.stream().filter(rep -> {
			JsonObject iss = rep.getJsonObject("iss");
			if (iss != null) {
				String totalIss = iss.getString("totalIss");
				if (totalIss != null && !totalIss.trim().equals("")) {
					
					JsonObject patInfo = rep.getJsonObject("traumaInfo");
					if (patInfo != null) {
						String sdo = patInfo.getString("sdo");
						String code = patInfo.getString("code");
						return (sdo != null && !sdo.trim().equals("") && !sdo.startsWith("12345")) || 
								(code != null && !code.trim().equals("") && !code.startsWith("12345"));
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		TSEventAnalyser app = new TSEventAnalyser();
		vertx.deployVerticle(app);				
	}
		
}
