package it.unibo.disi.pslab.traumastat.tools;

import java.io.FileWriter;
import java.util.Iterator;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumatracker.util.TTConfig;

public class TSExportAggregateRoom {

	private static final String TT_CONFIG_FILE = "./config.json";
	private static final String API_BASE_PATH = "/gt2/traumastat/api";
	private 	static FileWriter writer;

	public static void main(String[] args) throws Exception {

		TTConfig config;
		try {
			String configFile = TT_CONFIG_FILE;
			if (args.length == 1) {
				configFile = args[0];
			}
			config = new TTConfig(configFile);
		} catch (Exception ex){
			System.err.println("Config file not found or invalid");
			config = new TTConfig();
		}

		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx
				.createHttpClient(
						new HttpClientOptions()
							.setDefaultHost("localhost"/*args[0]*/)
							.setDefaultPort(config.getPort()));

		requestExportRoom(client);
	}

	
	private static void requestExport(HttpClient client) {
		Buffer buffer = Buffer.buffer("{ }");

		client
			.get(API_BASE_PATH + "/export?dateFrom=2017-01-01&dateTo=2018-11-01")
			.handler(res -> {
				System.out.println("response status " + res.statusCode());
				res.bodyHandler(res2 -> {
					JsonObject data = res2.toJsonObject();
					JsonArray list = data.getJsonArray("patients");
					
					dumpCSV(list);
					/*
					Iterator it = list.iterator();
					while (it.hasNext()) {
						JsonObject rep = (JsonObject) it.next();
						dump(rep);
					}
					*/
					System.exit(0);
				});
			})
			.setTimeout(100000)
			.putHeader("content-type", "application/json")
			.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer).end();
		
	}
	
	private static void requestExportRoom(HttpClient client) {
		Buffer buffer = Buffer.buffer("{ }");

		client
			.get(API_BASE_PATH + "/export-room-data?dateFrom=2017-01-01&dateTo=2018-11-01")
			.handler(res -> {
				System.out.println("response status " + res.statusCode());
				res.bodyHandler(res2 -> {
					JsonObject data = res2.toJsonObject();
					JsonArray list = data.getJsonArray("patients");
					
					dumpRoomCSV(list);
					
					/*
					Iterator it = list.iterator();
					while (it.hasNext()) {
						JsonObject rep = (JsonObject) it.next();
						dumpRoomInfo(rep);
					}*/
					
					System.exit(0);
				});
			})
			.setTimeout(100000)
			.putHeader("content-type", "application/json")
			.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer).end();
		
	}	
	
	
	private static void dumpCSV(JsonArray list) {
		
		try {
			writer = new FileWriter("stat.csv");
		
			/* header */
			
			JsonObject first = list.getJsonObject(0);
			flog("SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TL");
			
			Iterator it = first.getJsonArray("proc-actions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("drug-actions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("diag-actions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			flogn("");
			
			/* content */
	
			Iterator it2 = list.iterator();
			while (it2.hasNext()) {
				JsonObject rep = (JsonObject) it2.next();
	
				flog(rep.getString("sdo") + " ; " + 
						rep.getString("code") + " ; " +
						rep.getString("start-date") + " ; " +
						rep.getString("start-time") + " ; " +
						rep.getString("end-date") + " ; " +
						rep.getString("end-time") + " ; " +
						rep.getString("start-leader"));
				
				// flog("SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TL ; ");
				
				JsonArray actions = rep.getJsonArray("proc-actions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
	
				actions = rep.getJsonArray("drug-actions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
				
				actions = rep.getJsonArray("diag-actions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog ("; ; ");
					} else {
						flog(" ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
				flogn("");
			}

			log("DONE.");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				writer.flush();
				writer.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}

	private static void dumpRoomCSV(JsonArray list) {
		
		try {
			writer = new FileWriter("stat-room.csv");
		
			/* header */
			
			JsonObject first = list.getJsonObject(0);
			flog("SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TL");
			
			Iterator it = first.getJsonArray("room-data").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("room")+"-entrance" + " ; " + act.getString("room")+"-duration");
			}
			flogn("");
			
			/* content */
	
			Iterator it2 = list.iterator();
			while (it2.hasNext()) {
				JsonObject rep = (JsonObject) it2.next();
	
				flog(rep.getString("sdo") + " ; " + 
						rep.getString("code") + " ; " +
						rep.getString("start-date") + " ; " +
						rep.getString("start-time") + " ; " +
						rep.getString("end-date") + " ; " +
						rep.getString("end-time") + " ; " +
						rep.getString("start-leader"));
				
				// flog("SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TL ; ");
				
				JsonArray actions = rep.getJsonArray("room-data");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + info.getInteger("entrance") + " ; " + info.getInteger("duration"));
					}
				}
				flogn("");
			}

			log("DONE.");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				writer.flush();
				writer.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}
	
	private static void dumpRoomInfo(JsonObject rep) {

		logn("SDO: " + rep.getString("sdo"));
		JsonArray actions = rep.getJsonArray("room-data");
		Iterator it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("room") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");
	}
	
	
	
	private static void dump(JsonObject rep) {

		logn("SDO: " + rep.getString("sdo"));
		JsonArray actions = rep.getJsonArray("proc-actions");
		Iterator it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("param") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");

		actions = rep.getJsonArray("drug-actions");
		it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("param") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");
		
		actions = rep.getJsonArray("diag-actions");
		it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("param") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");
	}
	
	private static void log(String msg) {
		System.out.print(msg);
	}
	
	private static void logn(String msg) {
		System.out.println(msg);
	}

	private static void flog(String msg) {
		try {
			writer.write(msg);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void flogn(String msg) {
		try {
			writer.write(msg + "\n");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
