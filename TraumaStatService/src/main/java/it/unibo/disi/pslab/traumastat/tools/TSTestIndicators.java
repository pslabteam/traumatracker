package it.unibo.disi.pslab.traumastat.tools;

import java.io.FileWriter;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumatracker.util.TTConfig;

public class TSTestIndicators {

	private static final String TT_CONFIG_FILE = "./config.json";
	private static final String API_BASE_PATH = "/gt2/traumastat/api";
	private 	static FileWriter writer;

	public static void main(String[] args) throws Exception {

		TTConfig config;
		try {
			String configFile = TT_CONFIG_FILE;
			if (args.length == 1) {
				configFile = args[0];
			}
			config = new TTConfig(configFile);
		} catch (Exception ex){
			System.err.println("Config file not found or invalid");
			config = new TTConfig();
		}

		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx
				.createHttpClient(
						new HttpClientOptions()
							.setDefaultHost("localhost"/*args[0]*/)
							.setDefaultPort(config.getPort()));

		requestDefIndicators(client, "jan-jun", 2020);
	}

	private static void requestDefIndicators(HttpClient client, String period, int year) {
		Buffer buffer = Buffer.buffer("{ }");

		client
			.get(API_BASE_PATH + "/def-indicators?period=" + period + "&year=" + year)
			.handler(res -> {
				System.out.println("response status " + res.statusCode());
				res.bodyHandler(res2 -> {
					JsonObject data = res2.toJsonObject();
					System.out.println(data);
					System.exit(0);
				});
			})
			.setTimeout(100000)
			.putHeader("content-type", "application/json")
			.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer).end();
		
	}
	
	
}
