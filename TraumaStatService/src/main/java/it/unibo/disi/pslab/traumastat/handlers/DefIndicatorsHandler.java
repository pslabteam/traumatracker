package it.unibo.disi.pslab.traumastat.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

import it.unibo.disi.pslab.traumatracker.ontology.TraumaReportDomainModel;
import static it.unibo.disi.pslab.traumatracker.util.StatUtils.*;

public class DefIndicatorsHandler extends GeneralStatHandler {

	public DefIndicatorsHandler(TraumaStatService service){
		super(service);
	}

	@Override
	protected JsonObject computeStat(Params params, List<JsonObject> elements) {
		
		try {
			JsonObject reply = new JsonObject();
			
			String period = params.getParam("period");
			String year = params.getParam("year");
			
			reply.put("content", "default-indicators-comparison");

			int yearVal = Integer.parseInt(year);
			
			JsonObject rep1 = this.computeDefIndic(yearVal, period, elements);
			rep1.put("year", yearVal);
			reply.put("refYear", rep1);
			
			JsonObject rep2 = this.computeDefIndic(yearVal - 1, period, elements);
			rep2.put("year", yearVal - 1);
			reply.put("prevYear", rep2);

			
			return reply;
		} catch (Exception ex) {
			ex.printStackTrace();
			JsonObject reply = new JsonObject();
			reply.put("error", "internal-error: " + ex.getMessage());
			return reply;
			
		}
	}
	
	protected JsonObject computeDefIndic(int year, String period, List<JsonObject> elements) {
		
		try {
			JsonObject reply = new JsonObject();
			
			String dateFrom = null;
			String dateTo = null;
			
			if (period.equals("jan-jun")) {
				dateFrom = year + "-" + "01-01";
				dateTo = year + "-" + "06-30";
			} else if (period.equals("jul-dec")) {
				dateFrom = year + "-" + "07-01";
				dateTo = year + "-" + "12-31";
			} else if (period.equals("jan-dec")) {
				dateFrom = year + "-" + "01-01";
				dateTo = year + "-" + "12-31";
			} else {
				reply.put("error", "invalid period");
				return reply;
			}

			reply.put("year", year);

			List<JsonObject> elems = this.elementsFromTo(elements, dateFrom, dateTo);
	
			reply.put("nReports", elems.size());
			
			int nMissingShockRoomTAC = 0;  		

			int totTrauma = elems.size();		// indic #1
			int issGreaterThan14 = 0;
			int repsWithMissingISS = 0;

			int nShockEmoLowDamContrTime = 0; 	// indic #2 

			List<Double> timingFirstERProc 
				= new ArrayList<Double>();		// indic #3

			List<Double> timingShockRoomTac 
				= new ArrayList<Double>();		// indic #4

			List<Double> timingShockRoomOR 
				= new ArrayList<Double>();		// indic #5

			int nGCSLowAndIntubated = 0;			// indic #6
			
			int nDelayedActivations = 0;			// indic #7
			int repsWithMissingDelayed = 0;
						
			List<Double> timingShockRoomTacGCSPupils 
				= new ArrayList<Double>();		// indic #8
			
			int nPelvicFractures = 0;			// indic #9

			List<Double> timingInShockRoom 
				= new ArrayList<Double>();		// indic #10
			
			int nShockEmoWithTACBeforeOR = 0;	// indic #11
			
			List<Double> timingInShockRoomIfOpRoom 
				= new ArrayList<Double>();		// indic #12

			for (JsonObject rep: elems) {

				TraumaReportDomainModel dom = new TraumaReportDomainModel(rep);
								
				/* common */			

				long startTimeInSec = dom.getAbsStartTimeInSec();
				// long endTimeInSec = dom.getAbsEndTimeInSec();
						
				Optional<Long> firstTimeInSHR = dom.getFirstTimeInRoomInSec(dom.SHOCK_ROOM);
				Optional<Long> firstTimeInTAC = dom.getFirstTimeInRoomInSec(dom.TAC_PS);
				Optional<Long> firstTimeInOR = dom.getFirstTimeInRoomInSec(dom.OP_ROOM);
				boolean isShockEmo = dom.isShockEmo();
				boolean isLowGCS = dom.isLowGCS();
				
				/* TODO check iss presumed - mod 1.3 */
				
				/* indicator #1 - trauma & iss */
		 		
				JsonObject iss = rep.getJsonObject("iss");
				if (iss != null) {
					Integer totalIss = iss.getInteger("totalIss");
					if (totalIss != null && totalIss > 14) {
						issGreaterThan14++;
					} else if (totalIss == null) {
						repsWithMissingISS++;
					}
				} else {
					repsWithMissingISS++;
				}

				/* indicator #2 */

				if (isShockEmo && firstTimeInSHR.isPresent()) {
					// timingShockRoomOR.add((double) timeShockRoomOR);
					Optional<Long> timeDamContr = dom.getFirstTimeInDamageControlInSec();
					if (timeDamContr.isPresent()) {
						long timeShr = firstTimeInSHR.get();
						long timeDamCont = timeDamContr.get();
						if (timeDamCont - timeShr < 3600) {
							nShockEmoLowDamContrTime++;
						}
					}
				}
				
				/* indicator #3 - Tempo alla prima manovra terapeutica in emergenza*/
				
				Optional<Long> time = dom.getTimeFirstERProcInSec();
				if (time.isPresent()) {
					long dt = (time.get() - startTimeInSec)/60;
					timingFirstERProc.add((double) dt);
				}
				
				/* indicator #4 - shockroom - tac timing*/

				if (firstTimeInSHR.isPresent() && firstTimeInTAC.isPresent()) {
					long timeShockRoomTAC = (firstTimeInTAC.get() - firstTimeInSHR.get())/60;
					if (timeShockRoomTAC > 0) {
						timingShockRoomTac.add((double) timeShockRoomTAC);
					}
				}
				
				/* indicator #5 */
				
				if (isShockEmo && firstTimeInSHR.isPresent() && firstTimeInOR.isPresent()) {
					long timeShockRoomOR = (firstTimeInOR.get() - firstTimeInSHR.get())/60;
					if (timeShockRoomOR > 0) {
						timingShockRoomOR.add((double) timeShockRoomOR);
					}
				}
				
				/* indicator #6 - Numero di pazienti con GCS <9 arrivati intubati */
				
				if (isLowGCS && dom.isArrivedIntubated()) {
					nGCSLowAndIntubated++;
				}
				
				/* indicator #7 - delayed activation */
				
				JsonObject del = rep.getJsonObject("delayedActivation");
				if (del != null) {
					Boolean is = del.getBoolean("isDelayedActivation");
					if (is != null && is) {
						nDelayedActivations++;
					} else {
						repsWithMissingDelayed++;
					}
				} else {
					repsWithMissingDelayed++;
				}

				/* indicator #8 -  */

				if (isLowGCS && dom.isPupilsAniso()) {
					if (firstTimeInSHR.isPresent() && firstTimeInTAC.isPresent()) {
						long timeShockRoomTAC = (firstTimeInTAC.get() - firstTimeInSHR.get())/60;
						if (timeShockRoomTAC > 0) {
							timingShockRoomTacGCSPupils.add((double)timeShockRoomTAC);
						} else {
							// @TODO - error
						}
					} else {
						nMissingShockRoomTAC++;
					}
				}
				
				/* indicator #9 */
				
				Optional<Integer> abdISS = getNestedInt(rep, "iss", "abdomenGroup", "abdomenAis");
				if (abdISS.isPresent()) {
					int abdIss = abdISS.get();					
					if (abdIss >= 3 && abdIss <= 5 && dom.appliedProc("pelvic-binder")) {
						nPelvicFractures++;
					}
				}
				
				/* indicator #10 */
				
				double tsr = (double) dom.getTotalTimeInRoomInSec(dom.SHOCK_ROOM) / 60;
				timingInShockRoom.add(tsr);

				/* indicator #11 */
				
				if (isShockEmo) {
					if (firstTimeInTAC.isPresent() && firstTimeInOR.isPresent()) {
						long timeTAC = firstTimeInTAC.get();	
						long timeOR = firstTimeInOR.get();	
						if (timeTAC < timeOR) {
							nShockEmoWithTACBeforeOR++;
						}
					}
				}
				
				/* indicator #12 */
				
				if (dom.inRoom(dom.OP_ROOM)){
					timingInShockRoomIfOpRoom.add(tsr);
				}
				
			}
			
			/* indicator #1 */
			
			JsonObject traumaISSIndicator = new JsonObject();
			traumaISSIndicator.put("nTotTrauma", (totTrauma - repsWithMissingISS));
			traumaISSIndicator.put("issGreaterThan14Perc", (int)(issGreaterThan14*100/(totTrauma - repsWithMissingISS)));
			reply.put("traumaISSIndic", traumaISSIndicator);

			/* indicator #2 */
			
			JsonObject numShockEmoWithShortDamContrTime = new JsonObject();
			numShockEmoWithShortDamContrTime.put("nTrauma", nShockEmoLowDamContrTime);
			reply.put("numShockEmoWithShortDamContrTime", numShockEmoWithShortDamContrTime);

			/* indicator #3 */

			JsonObject timingFirstERProcIndic = computeBasicStat(timingFirstERProc);
			reply.put("timingFirstERProcIndic", timingFirstERProcIndic);

			/* indicator #4 */

			JsonObject timingShockRoomTACIndic = computeBasicStat(timingShockRoomTac);
			reply.put("timingShockRoomTACIndic", timingShockRoomTACIndic);

			/* indicator #5 */

			JsonObject timingShockRoomORforShockEmoIndic = computeBasicStat(timingShockRoomOR);
			reply.put("timingShockRoomORforShockEmoIndic", timingShockRoomORforShockEmoIndic);
			
			/* indicator #6 */
			
			JsonObject numTraumaGCSLowAndIntu = new JsonObject();
			numTraumaGCSLowAndIntu.put("nTrauma", nGCSLowAndIntubated);
			reply.put("nTraumaGCSLowAndIntubated", numTraumaGCSLowAndIntu);

			
			/* indicator #7 */
			
			JsonObject numDelayedActivations = new JsonObject();
			numDelayedActivations.put("nTrauma", nDelayedActivations);
			numDelayedActivations.put("nMissingDelayed", repsWithMissingDelayed);
			reply.put("nDelayedActivations", numDelayedActivations);

			/* indicator #8 */
			
			JsonObject timingShockRoomTacGCSPupilsIndic = computeBasicStat(timingShockRoomTacGCSPupils);
			timingShockRoomTacGCSPupilsIndic.put("missingShockRoomTAC", nMissingShockRoomTAC);
			reply.put("timingShockRoomTacGCSPupilsIndic", timingShockRoomTacGCSPupilsIndic);

			/* indicator #6 */
			
			JsonObject numPelvicFractures = new JsonObject();
			numPelvicFractures.put("nTrauma", nPelvicFractures);
			reply.put("nPelvicFractures", numPelvicFractures);
			
			/* indicator #10 */

			JsonObject timingInShockRoomIndic = computeBasicStat(timingInShockRoom);
			reply.put("timingInShockRoomIndic", timingInShockRoomIndic);

			/* indicator #11 */

			JsonObject numShockEmoTacBeforeOR = new JsonObject();
			numShockEmoTacBeforeOR.put("nTrauma", nShockEmoWithTACBeforeOR);
			reply.put("nShockEmoWithTACBeforeOR", numShockEmoTacBeforeOR);
			
			/* indicator #12 */

			JsonObject timingInShockRoomIfOpRoomIndic = computeBasicStat(timingInShockRoomIfOpRoom);
			reply.put("timingInShockRoomIfOpRoomIndic", timingInShockRoomIfOpRoomIndic);

			return reply;
		} catch (Exception ex) {
			ex.printStackTrace();
			JsonObject reply = new JsonObject();
			reply.put("error", "internal-error: " + ex.getMessage());
			return reply;
			
		}
	}
}
