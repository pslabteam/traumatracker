package it.unibo.disi.pslab.traumastat;

import java.util.Date;

import com.mongodb.event.*;

import io.vertx.core.AbstractVerticle;
import it.unibo.disi.pslab.traumatracker.util.TTConfig;

public class MongoDBAgent extends AbstractVerticle {

	private int port;
	private String host;
	// private MongoClient client;
	private Date lastPingTime;
	private TSLogger view;
	private TTConfig config;

	public MongoDBAgent(TSLogger view, TTConfig config){
		host = "localhost";
		port = 27017;
		this.view = view;
		this.config = config;

	}

	public void start() {
		log("Setting up the server.");
		setupServer();
	}

	class ServerListener implements ServerMonitorListener {
		@Override
		public void serverHearbeatStarted(ServerHeartbeatStartedEvent serverHeartbeatStartedEvent) {
			// Ping Started
			// log("Checking is mongodb is alive..");
		}

		@Override
		public void serverHeartbeatSucceeded(ServerHeartbeatSucceededEvent serverHeartbeatSucceededEvent) {
			// Ping Succeed, Connected to server
			lastPingTime = new Date();
			// log("Server is alive.");
		}

		@Override
		public void serverHeartbeatFailed(ServerHeartbeatFailedEvent serverHeartbeatFailedEvent) {
			// Ping failed, server down or connection lost
			log("Server down or connection lost => Setting up the server");
			setupServer();	    	
		}
	}

	private void log(String msg){
		System.out.println("[TTMongoDBAgent] "+msg);
		view.log("[TTMongoDBAgent] "+msg);
	}

	private void setupServer(){
		this.getVertx().executeBlocking(f -> {
			try {
				Process start = Runtime.getRuntime().exec(new String[]{ config.getMongoPath(), "--dbpath", config.getDBPath()});
				/*
				BufferedReader reader = new BufferedReader(new InputStreamReader(start.getInputStream()));
				reader.lines().forEach( s -> {
					System.out.println(s);
				});*/
			} catch (Exception ex){
				ex.printStackTrace();
				log("MongoDB already running.");
			}
			f.complete();
		}, res -> {
			log("MongoDB setup succeeded.");
			/*
			try {
				log("Connecting to the DB...");
				MongoClientOptions clientOptions = new MongoClientOptions.Builder()	
						.addServerMonitorListener(new ServerListener())	                
						.build();

				client = new MongoClient(new ServerAddress(host, port), clientOptions);
				log(""+client.getAddress());

			} catch (Exception ex) {
				ex.printStackTrace();
			}*/
			
		});
	}


}
