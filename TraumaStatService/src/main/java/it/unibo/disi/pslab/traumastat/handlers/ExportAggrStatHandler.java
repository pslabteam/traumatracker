package it.unibo.disi.pslab.traumastat.handlers;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumastat.TraumaStatService;


public class ExportAggrStatHandler extends GeneralStatHandler {

	private final static Integer NI = null;
	private final static String NS = null;
	
	public ExportAggrStatHandler(TraumaStatService service){
		super(service);
	}

	@Override
	protected JsonObject computeStat(Params params, List<JsonObject> elements) {
		try {
			String dateFrom = params.getParam("dateFrom");
			String dateTo = params.getParam("dateTo");
			String who = params.getParam("operatorId");
			
			List<JsonObject> elems = null;
			if (who != null && !who.equals("")) {
				elems = this.elementsFromToBy(elements, dateFrom, dateTo, who);
			} else {
				elems = this.elementsFromTo(elements, dateFrom, dateTo);
			}
			/* compiling the report */
			
			LinkedList<JsonObject> list = new LinkedList<JsonObject>();
			HashMap<String,String> keyMapDiag = new HashMap<>();
			HashMap<String,String> keyMapDrug = new HashMap<>();
			HashMap<String,String> keyMapProc = new HashMap<>();
			HashMap<String,String> keyMapBloodProduct = new HashMap<>();
			
			HashMap<String,String> places = new HashMap<>();
			
			for (JsonObject rep: elems){
				HashMap<String,StatInfo> stats = new HashMap<>();
				HashMap<String,StatRoomInfo> roomsStats = new HashMap<>();
	
				String startDate = rep.getString("startDate");
				String startTime = rep.getString("startTime");
						
				JsonArray events = rep.getJsonArray("events");
				Iterator<Object> it = events.iterator();
				while (it.hasNext()) {
					JsonObject ev = (JsonObject) it.next();
					
					String date = ev.getString("date");
					String time = ev.getString("time");
					
					long when = getDeltaTime(startDate,startTime,date,time);
					String et = ev.getString("type");
					String key = null;
					JsonObject co = ev.getJsonObject("content");
					if (et.equals("procedure")) {	
						key = co.getString("procedureId");
						String val = keyMapProc.get(key);
						if (val == null) {
							keyMapProc.put(key, key);
						}
	
					} else if (et.equals("diagnostic")) {
						key = co.getString("diagnosticId");
						String val = keyMapDiag.get(key);
						if (val == null) {
							keyMapDiag.put(key, key);
						}
					} else if (et.equals("blood-product")) {
						key = co.getString("bloodProductId");
						String val = keyMapBloodProduct.get(key);
						if (val == null) {
							keyMapBloodProduct.put(key, key);
						}						
					} else if (et.equals("drug")) {
						key = co.getString("drugId");
						String val = keyMapDrug.get(key);
						if (val == null) {
							keyMapDrug.put(key, key);
						}
					} else if (et.equals("room-in") || et.equals("room-out")) {
						JsonObject content = ev.getJsonObject("content");
						String place = content.getString("place");
	
						if (!place.equals("PRE-H")) {
							if (et.equals("room-in")) {
								StatRoomInfo info = roomsStats.get(place);
								if (info == null) {
									info = new StatRoomInfo(place, when);
									roomsStats.put(place,  info);
								} else {
									info.setNewEntrance(when);
								}
								
								if (places.get(place) == null) {
									places.put(place, place);
								}
							} else {
								StatRoomInfo info = roomsStats.get(place);
								if (info != null) {
									info.setExit(when);
								} else {
									System.err.println("FOUND ROOM OUT without IN");
								}
							}
						}
					}
					
					if (key != null) {
						StatInfo statInfo = stats.get(key);
						if (statInfo == null) {
							statInfo = new StatInfo(key,when);
							stats.put(key, statInfo);
						} else {
							statInfo.incOcc();
						}
					}
				}
				
				JsonObject newRep = new JsonObject();
				newRep.put("_id", rep.getString("_id"));
				
				System.out.println("Processing " + rep.getString("_id"));
				
				newRep.put("sdo", rep.getJsonObject("traumaInfo").getString("sdo"));
				newRep.put("code", rep.getJsonObject("traumaInfo").getString("code"));
				newRep.put("startDate", rep.getString("startDate"));
				newRep.put("startTime", rep.getString("startTime"));
				newRep.put("endDate", rep.getString("endDate"));
				newRep.put("endTime", rep.getString("endTime"));
				
				newRep.put("finalDestination", rep.getString("finalDestination"));
				
				newRep.put("startLeader", rep.getString("startOperatorId"));			
				JsonArray tteam = rep.getJsonArray("traumaTeamMembers");
				newRep.put("startTeam", tteam.toString());
	
				JsonObject da = rep.getJsonObject("delayedActivation");
				if (da != null && da.containsKey("isDelayedActivation") && da.getBoolean("isDelayedActivation")) {
					newRep.put("delayedActivation","sì");
					newRep.put("delayedActivationDate",checkNullString(da.getString("originalAccessDate")));
					newRep.put("delayedActivationTime",checkNullString(da.getString("originalAccessTime")));
				} else {
					newRep.put("delayedActivation","no");
					newRep.put("delayedActivationDate","");
					newRep.put("delayedActivationTime","");
				}
	
				JsonObject iss = rep.getJsonObject("iss");
				if (iss != null) {
					newRep.put("issTotal",iss.getInteger("totalIss"));
					
					JsonObject issh = iss.getJsonObject("headGroup");
					if (issh != null) {
						newRep.put("issHeadGroupTotalIss",issh.getInteger("groupTotalIss"));
						newRep.put("issHeadGroupNeckAis",issh.getInteger("headNeckAis"));
						newRep.put("issHeadGroupBrainAis",issh.getInteger("brainAis"));
						newRep.put("issHeadGroupCervicalSpineAis",issh.getInteger("cervicalSpineAis"));
					} else {
						newRep.put("issHeadGroupTotalIss",NI);
						newRep.put("issHeadGroupNeckAis",NI);
						newRep.put("issHeadGroupBrainAis",NI);
						newRep.put("issHeadGroupCervicalSpineAis",NI);					
					}
					
					JsonObject issf = iss.getJsonObject("faceGroup");
					if (issf != null) {
						newRep.put("issFaceGroupTotalIss",issf.getInteger("groupTotalIss"));
						newRep.put("issFaceGroupFaceAis",issf.getInteger("faceAis"));
					} else {
						newRep.put("issFaceGroupTotalIss",NI);
						newRep.put("issFaceGroupFaceAis",NI);
					}
					
					JsonObject isst = iss.getJsonObject("toraxGroup");
					if (isst != null) {
						newRep.put("issToraxGroupTotalIss",isst.getInteger("groupTotalIss"));
						newRep.put("issToraxGroupToraxAis",isst.getInteger("toraxAis"));
						newRep.put("issToraxGroupSpineAis",isst.getInteger("spineAis"));
					} else {
						newRep.put("issToraxGroupTotalIss",NI);
						newRep.put("issToraxGroupToraxAis",NI);
						newRep.put("issToraxGroupSpineAis",NI);
					}
	
					JsonObject issa = iss.getJsonObject("abdomenGroup");
					if (issa != null) {
						newRep.put("issAbdomenGroupTotalIss",issa.getInteger("groupTotalIss"));
						newRep.put("issAbdomenGroupAbdomenAis",issa.getInteger("abdomenAis"));
						newRep.put("issAbdomenGroupLumbarSpineAis",issa.getInteger("lumbarSpineAis"));
					} else {
						newRep.put("issAbdomenGroupTotalIss",NI);
						newRep.put("issAbdomenGroupAbdomenAis",NI);
						newRep.put("issAbdomenGroupLumbarSpineAis",NI);
					}
					
					JsonObject isse = iss.getJsonObject("extremitiesGroup");
					if (isse != null) {
						newRep.put("issExtremitiesGroupTotalIss",isse.getInteger("groupTotalIss"));
						newRep.put("issExtremitiesGroupUpperExtremitiesAis",isse.getInteger("upperExtremitiesAis"));
						newRep.put("issExtremitiesGroupLowerExtremitiesAis",isse.getInteger("lowerExtremitiesAis"));
						newRep.put("issExtremitiesGroupPelvicGirdleAis",isse.getInteger("pelvicGirdleAis"));
					} else {
						newRep.put("issExtremitiesGroupTotalIss",NI);
						newRep.put("issExtremitiesGroupUpperExtremitiesAis",NI);
						newRep.put("issExtremitiesGroupLowerExtremitiesAis",NI);
						newRep.put("issExtremitiesGroupPelvicGirdleAis",NI);
					}
	
					JsonObject issx = iss.getJsonObject("externaGroup");
					if (isse != null) {
						newRep.put("issExternalGroupTotalIss",issx.getInteger("groupTotalIss"));
						newRep.put("issExternalGroupExternalAis",issx.getInteger("externaAis"));
					} else {
						newRep.put("issExternalGroupTotalIss",NI);
						newRep.put("issExternalGroupExternalAis",NI);
					}
	
				} else {
					newRep.put("issTotal",NI);
					
					newRep.put("issHeadGroupTotalIss",NI);
					newRep.put("issHeadGroupNeckAis",NI);
					newRep.put("issHeadGroupBrainAis",NI);
					newRep.put("issHeadGroupCervicalSpineAis",NI);					
	
					newRep.put("issFaceGroupTotalIss",NI);
					newRep.put("issFaceGroupFaceAis",NI);
	
					newRep.put("issToraxGroupTotalIss",NI);
					newRep.put("issToraxGroupToraxAis",NI);
					newRep.put("issToraxGroupSpineAis",NI);
	
					newRep.put("issAbdomenGroupTotalIss",NI);
					newRep.put("issAbdomenGroupAbdomenAis",NI);
					newRep.put("issAbdomenGroupLumbarSpineAis",NI);
					
					newRep.put("issExtremitiesGroupTotalIss",NI);
					newRep.put("issExtremitiesGroupUpperExtremitiesAis",NI);
					newRep.put("issExtremitiesGroupLowerExtremitiesAis",NI);
					newRep.put("issExtremitiesGroupPelvicGirdleAis",NI);
	
					newRep.put("issExternalGroupTotalIss",NI);
					newRep.put("issExternalGroupExternalAis",NI);
					
				}
				
				JsonObject ti = rep.getJsonObject("traumaInfo");
				if (ti != null) {
					newRep.put("tiErDeceased",boolToS(ti.getBoolean("erDeceased")));
					newRep.put("tiName",ti.getString("name"));
					newRep.put("tiSurname",ti.getString("surname"));
					newRep.put("tiDOB",ti.getString("dob"));
					newRep.put("tiGender",ti.getString("gender"));
					newRep.put("tiAge",ti.getInteger("age"));
					newRep.put("tiAccidentDate",ti.getString("accidentDate"));
					newRep.put("tiAccidentTime",ti.getString("accidentTime"));
					newRep.put("tiAccidentType",ti.getString("accidentType"));
					newRep.put("tiVehicle",ti.getString("vehicle"));
					newRep.put("tiFromOtherEmergency",boolToS(ti.getBoolean("fromOtherEmergency")));
					newRep.put("tiOtherEmergency",ti.getString("otherEmergency"));
				} else {
					newRep.put("tiName",NS);
					newRep.put("tiSurname",NS);
					newRep.put("tiDOB",NS);
					newRep.put("tiErDeceased",NS);
					newRep.put("tiGender",NS);
					newRep.put("tiAge",NS);
					newRep.put("tiAccidentDate",NS);
					newRep.put("tiAccidentTime",NS);
					newRep.put("tiAccidentType",NS);
					newRep.put("tiVehicle",NS);
					newRep.put("tiFromOtherEmergency",NS);
					newRep.put("tiOtherEmergency",NS);
				}
				
				JsonObject mt = rep.getJsonObject("majorTraumaCriteria");
				if (mt != null) {
					newRep.put("mtcDynamic", boolToS(mt.getBoolean("dynamic")));
					newRep.put("mtcPhysiological", boolToS(mt.getBoolean("physiological")));
					newRep.put("mtcAnatomical", boolToS(mt.getBoolean("anatomical")));
				} else {
					newRep.put("mtcDynamic", NS);
					newRep.put("mtcPhysiological", NS);
					newRep.put("mtcAnatomical", NS);
				}
	
				JsonObject an = rep.getJsonObject("anamnesi");
				if (an != null) {
					newRep.put("anAntiplatelets", boolToS(an.getBoolean("antiplatelets")));
					newRep.put("anAnticoagulants", boolToS(an.getBoolean("anticoagulants")));
					newRep.put("anNao", boolToS(an.getBoolean("nao")));
				} else {
					newRep.put("anAntiplatelets", NS);
					newRep.put("anAnticoagulants", NS);
					newRep.put("anNao", NS);
				}
				
				JsonObject preh = rep.getJsonObject("preh");
				if (preh != null) {
					newRep.put("preHTerritorialArea", preh.getString("territorialArea"));
					newRep.put("preHIsCarAccident", boolToS(preh.getBoolean("isCarAccident")));
					newRep.put("preHaValue", preh.getString("aValue"));
					newRep.put("preHbPleuralDecompression", boolToS(preh.getBoolean("bPleuralDecompression")));
					newRep.put("preHcBloodProtocol", boolToS(preh.getBoolean("cBloodProtocol")));
					newRep.put("preHcTpod", boolToS(preh.getBoolean("cTpod")));
					newRep.put("preHdGcsTotal", preh.getInteger("dGcsTotal"));
					newRep.put("preHdAnisocoria", boolToS(preh.getBoolean("dAnisocoria")));
					newRep.put("preHdMidriasi", boolToS(preh.getBoolean("dMidriasi")));
					newRep.put("preHeMotility", boolToS(preh.getBoolean("eMotility")));
					newRep.put("preHWorstBloodPressure", preh.getInteger("worstBloodPressure"));
					newRep.put("preHWorstRespiratoryRate", preh.getInteger("worstRespiratoryRate"));
				} else {
					newRep.put("preHTerritorialArea", NS);
					newRep.put("preHIsCarAccident", NS);
					newRep.put("preHaValue", NS);
					newRep.put("preHbPleuralDecompression", NS);
					newRep.put("preHcBloodProtocol", NS);
					newRep.put("preHcTpod", NS);
					newRep.put("preHdGcsTotal", NI);
					newRep.put("preHdAnisocoria", NS);
					newRep.put("preHdMidriasi", NS);
					newRep.put("preHeMotility", NS);
					newRep.put("preHWorstBloodPressure", NI);
					newRep.put("preHWorstRespiratoryRate", NI);
					
				}
				
				JsonObject pat = rep.getJsonObject("patientInitialCondition");
				if (pat != null) {
					JsonObject vs = pat.getJsonObject("vitalSigns");
					if (vs != null) {
						newRep.put("svsTemp", vs.getString("temp"));
						newRep.put("svsHR", vs.getString("hr"));
						newRep.put("svsBP", vs.getString("bp"));
						newRep.put("svsSpO2", vs.getString("spo2"));
						newRep.put("svsEtCO2", vs.getString("etco2"));
					} else {
						newRep.put("svsTemp", NS);
						newRep.put("svsHR", NS);
						newRep.put("svsBP", NS);
						newRep.put("svsSpO2", NS);
						newRep.put("svsEtCO2", NS);
					}
					
					JsonObject cp = pat.getJsonObject("clinicalPicture");
					if (cp != null) {
						newRep.put("svsGCSTotal", cp.getInteger("gcsTotal"));
						newRep.put("svsGCSMotor", cp.getString("gcsMotor"));
						newRep.put("svsGCSVerbal", cp.getString("gcsVerbal"));
						newRep.put("svsGCSEyes", cp.getString("gcsEyes"));
						newRep.put("svsSedated", boolToS(cp.getBoolean("sedated")));
						newRep.put("svsPupils", cp.getString("pupils"));
						newRep.put("svsAirway", cp.getString("airway"));
						newRep.put("svsPositiveInhal", boolToS(cp.getBoolean("positiveInhalation")));
						newRep.put("svsIntubationFailed", boolToS(cp.getBoolean("intubationFailed")));
						newRep.put("svsChestTube", cp.getString("chestTube"));
						newRep.put("svsOxygenPercentage", cp.getInteger("oxygenPercentage"));
						newRep.put("svsHemorrhage", boolToS(cp.getBoolean("hemorrhage")));
						newRep.put("svsLimbsFracture", boolToS(cp.getBoolean("limbsFracture")));
						newRep.put("svsFractureExposition", boolToS(cp.getBoolean("fractureExposition")));
						newRep.put("svsBurn", cp.getString("burn"));
					} else {
						newRep.put("svsGCSTotal", NI);
						newRep.put("svsGCSMotor", NS);
						newRep.put("svsGCSVerbal", NS);
						newRep.put("svsGCSEyes", NS);
						newRep.put("svsSedated", NS);
						newRep.put("svsPupils", NS);
						newRep.put("svsAirway", NS);
						newRep.put("svsPositiveInhal", NS);
						newRep.put("svsIntubationFailed", NS);
						newRep.put("svsChestTube", NS);
						newRep.put("svsOxygenPercentage", NI);
						newRep.put("svsHemorrhage", NS);
						newRep.put("svsLimbsFracture", NS);
						newRep.put("svsFractureExposition", NS);
						newRep.put("svsBurn", NS);
						
					}	
				}
				
				for (StatInfo info: stats.values()) {
					JsonObject elem = new JsonObject();
					long firstTime = info.getFirstTime();
					double mins = minutes(firstTime);
					elem.put("first", mins);  // in minutes
					elem.put("rep", info.getNReps());
					newRep.put(info.getParam(), elem);
				}
	
				for (StatRoomInfo info: roomsStats.values()) {
					JsonObject elem = new JsonObject();
					long firstTime = info.getFirstTime();
					double mins = minutes(firstTime);
					long duration = info.getDuration();
					double dur = minutes(duration);
					elem.put("entrance", mins); // in minutes
					elem.put("duration", dur); // in minutes
					// elem.put("exitRegistered", info.isExitOK());
					newRep.put(info.getRoomName(), elem);
				}
	
				list.add(newRep);
				
				
			}
	
			String[] valuesProc = new String[keyMapProc.values().size()];
			keyMapProc.values().toArray(valuesProc);		
			Arrays.sort(valuesProc);
	
			String[] valuesDrug = new String[keyMapDrug.values().size()];
			keyMapDrug.values().toArray(valuesDrug);		
			Arrays.sort(valuesDrug);
	
			String[] valuesBloodProduct = new String[keyMapBloodProduct.values().size()];
			keyMapBloodProduct.values().toArray(valuesBloodProduct);		
			Arrays.sort(valuesBloodProduct);

			String[] valuesDiag = new String[keyMapDiag.values().size()];
			keyMapDiag.values().toArray(valuesDiag);		
			Arrays.sort(valuesDiag);
			
			String[] valuesPlaces = new String[places.values().size()];
			places.values().toArray(valuesPlaces);		
			Arrays.sort(valuesPlaces);
	
			JsonObject data = new JsonObject();		
			JsonArray plist = new JsonArray();
			
			for (JsonObject obj: list) {
				JsonObject el = new JsonObject();
				
				for (Entry<String,Object> entry: obj.getMap().entrySet()) {
					el.put(entry.getKey(), entry.getValue());
				}
				
				/*
				el.put("sdo", obj.getString("sdo"));			
				el.put("code", obj.getString("code"));
				el.put("startDate", obj.getString("startDate"));
				el.put("startTime", obj.getString("startTime"));
				el.put("endDate", obj.getString("endDate"));
				el.put("endTime", obj.getString("endTime"));
				el.put("startLeader", obj.getString("startLeader"));
				*/
				
				JsonArray actList = new JsonArray();
				for (String key: valuesPlaces) {
						JsonObject elem = new JsonObject();
						JsonObject info = obj.getJsonObject(key);
						elem.put("room", key);
						elem.put("info", info);
						actList.add(elem);
				}			
				el.put("roomData", actList);		
				
				fill(obj, el, valuesProc, "procActions");
				fill(obj, el, valuesDrug, "drugActions");
				fill(obj, el, valuesBloodProduct, "bloodProductActions");
				fill(obj, el, valuesDiag, "diagActions");
				plist.add(el);
			}
			
			data.put("patients", plist);
			return data;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	protected String toCSV(JsonObject source) {
		StringBuffer buffer = dumpCSV(source.getJsonArray("patients"));
		return buffer.toString();
	}

	private static double minutes(long seconds) {
		return (Math.round((seconds / 60.0) * 100))*0.01;
	}

	private static void fill(JsonObject source, JsonObject target, String[] values,  String fieldName) {
		try {
			JsonArray actList = new JsonArray();
			for (String key: values) {
					JsonObject elem = new JsonObject();
					JsonObject info = source.getJsonObject(key);
					elem.put("param", key);
					elem.put("info", info);
					actList.add(elem);
			}			
			target.put(fieldName, actList);		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private long getDeltaTime(String startDate, String startTime, String evDate, String evTime) {

			long startValue = 
					Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
			long evValue = 
					Integer.parseInt(evTime.substring(0, 2))*3600 + Integer.parseInt(evTime.substring(3, 5))*60 + Integer.parseInt(evTime.substring(6));

			int startDay = Integer.parseInt(startDate.substring(8));
			int endDay = Integer.parseInt(evDate.substring(8));
			if (startDay != endDay){
				evValue = evValue + 3600*24;
			}
			return evValue - startValue;
		
	}
	private JsonObject computeBasicStat(List<Double> values, double sum) {
		JsonObject obj = new JsonObject();
		double avg = sum / values.size();
		
		obj.put("average", avg);
		
		obj.put("standDev", computeStandDev(values, avg));
		
		/* calcolo mediana */
		
		values.sort((Double a, Double b) -> a > b ? 1 : (a == b ? 0 : -1)); 
		
		double median = computeMedian(values, 0, values.size() - 1);
		obj.put("median", median);

		/* calcolo quartili */
		
		double q1 = 0;
		double q3 = 0;
		if (values.size() % 2 == 0) {
			q1 = computeMedian(values, 0, values.size() / 2 - 1);
			q3 = computeMedian(values, values.size() / 2, values.size() - 1);
		} else {
			q1 = computeMedian(values, 0, (values.size() - 1) / 2 - 1);
			q3 = computeMedian(values, (values.size() + 1) / 2, values.size() - 1);
		}
		
		double iqr = q3 - q1;		
		obj.put("iqr", iqr);
				
		return obj;
	}
	
	private double computeMedian(List<Double> values, int indexFrom, int indexTo) {
		double median = 0;
		int n = (indexTo - indexFrom) + 1;
		if (n % 2 == 0) {
			median = (values.get(indexFrom + n/2) + values.get(indexFrom + (n + 1)/2)) / 2.0;
		} else {
			median = values.get(indexFrom + (n + 1)/2);
		}
		return median;
	}
	
	private double computeStandDev(List<Double> elems, double avg) {
		double sum = 0;
		for (Double e: elems) {
			double diff = (e - avg); 
			sum += diff*diff;
		}
		sum = sum / elems.size();
		return Math.sqrt(sum);
	}

		
	private long getTotalTimeInRoom(JsonArray events, String room) {
		Iterator<Object> it = events.iterator();
		
		long totTime = 0;
		long absTimeFrom = -1;
		
		while (it.hasNext()) {
			JsonObject ev = (JsonObject) it.next();
			String date = ev.getString("date");
			String time = ev.getString("time");
			String type = ev.getString("type");
			if (type.equals("room-in") || (type.equals("patient-accepted") && room.equals("Shock-Room"))) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room) && absTimeFrom == -1) {
						absTimeFrom = getAbsTimeInSec(date, time);
					} else if (!place.equals(room) && absTimeFrom != -1) {
						/* changing the room without a room out event */
						long absTimeTo = getAbsTimeInSec(date, time);						
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			} else if (type.equals("room-out") && absTimeFrom != -1) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room)) {
						long absTimeTo = getAbsTimeInSec(date, time);
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			}
		}
		
		return totTime;
	}
	
	private long getAbsTimeInSec(String startDate, String startTime) {
		if (startDate != null && !startDate.equals("") && 
			startTime != null && !startTime.equals("")) {

			// date: // yyyy-mm-dd
			// time: // hh:mm:ss
			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
		    try {
		    		Date date = sdf.parse(startDate);
		    
				long startValue = 
						Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
	
			    return date.getTime()/1000 + startValue;
		    } catch (Exception ex) {
				throw new IllegalArgumentException("invalid date");
		    }
		} else {
			throw new IllegalArgumentException("invalid date");
		}
		
	}

	private int checkMax(JsonObject obj, String aisn, int maxIss) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv > maxIss) {
				return hnv;
			} else {
				return maxIss;
			}
		} else {
			return maxIss;
		}
	}

	private boolean checkMaxAIS(JsonObject obj, String aisn) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv >= 3) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private String checkNullString(String st) {
		if (st == null) {
			return "";
		} else {
			return st;
		}
	}

	private String boolToS(Boolean value) {
		if (value == null) {
			return null;
		} else {
			return value.booleanValue() ? "si" : "no";
		}
	}
	
	private static void flog(StringBuffer buf, String msg) {
		try {
			buf.append(msg);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void flogl(StringBuffer buf, Object... msgs) {
		try {
			for (Object m: msgs) {
				if (m != null) {
					buf.append(m + " ; ");
				} else {
					buf.append(" ; ");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void flogn(StringBuffer buf, String msg) {
		try {
			buf.append(msg + "\n");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static StringBuffer dumpCSV(JsonArray list) {
		
		StringBuffer buffer = new StringBuffer();
		try {
			/* header */
			
			JsonObject first = list.getJsonObject(0);
			flog(buffer, "_ID ; SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TLEADER ; TTEAM ; ");
			
			flog(buffer, "DEL-ACT ; DEL-ACT-date ; DEL-ACT-time ; ");
			
			flog(buffer, "ISS-TOT ; ");
			flog(buffer, "ISS-HG-TOT ; ISS-HG-NECK-AIS ; ISS-HG-BRAIN-AIS ; ISS-HG-SPINE-AIS ; ");
			flog(buffer, "ISS-FG-TOT ; ISS-FG-FACE-AIS ; ");
			flog(buffer, "ISS-TG-TOT ; ISS-TG-TORAX-AIS ; ISS-TG-SPINE-AIS ; ");
			flog(buffer, "ISS-AG-TOT ; ISS-AG-ABDOMEN-AIS ; ISS-AG-SPINE-AIS ; ");
			flog(buffer, "ISS-EG-TOT ; ISS-EG-UPPER-AIS ; ISS-EG-LOWER-AIS ; ISS-EG-PELVIC-AIS ; ");
			flog(buffer, "ISS-EXG-TOT ; ISS-EXG-EXT-AIS ; ");
			
			
			flog(buffer, "TI-PAT-NAME ; TI-PAT-SURNAME ; TI-PAT-GENDER ; TI-PAT-DOB ; TI-PAT-AGE ; TI-PAT-ACC-DATE ; TI-PAT-ACC-TIME ; TI-PAT-ACC-TYPE ; TI-DECEASED ;");
			flog(buffer, "TI-VEHICLE ; TI-FROM-OTHER-ER ; TI-OTHER-ER ; ");
			
			flog(buffer, "MTC-DYNAMIC ; MTC-PHYSIO ; MTC-ANATOM ; ");
			flog(buffer, "AN-ANTIPLATELES ; AN-ANTICOAG ; AN-NAO ; ");
			
			flog(buffer, "PREH-TERRIT ; PREH-IS-CAR-ACC ; PREH-AVALUE	 ; PREH-BPLEDEC ; PREH-CBLOODPROT ; PREH-CTPOD ;  PREH-DGCS-TOT ; PREH-DANISOC ; PREH-DMID ; PREH-EMOT ; PREH-WORSTBP ; PREH-WORSTRESPRATE ; ");
			flog(buffer, "SVS-TEMP ;  SVS-HR ; SVS-BP ; SVS-SPO2 ; SVS-ETCO2 ; ");
			flog(buffer, "SVS-GCSTOT ; SVS-GCS-MOT ; SVS-GCS-VERB ; SVS-GCS-EYES ; SVS-SEDATED ; SVS-PUPILS ; ");
			flog(buffer, "SVS-AIRWAY ; SVS-POSITIVE-INHAL ; SVS-INTUBFAILED ; SVS-CHESTTUBE ; SVS-OXYGPERC ; ");
			flog(buffer, "SVS-HEMORR ; SVS-LIMBSFRACT ; SVS-FRACTEXP ; SVS-BURN ");
			
			// actions
			
			Iterator it = first.getJsonArray("procActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(buffer, " ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("drugActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(buffer, " ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("bloodProductActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(buffer, " ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("diagActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(buffer, " ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			
			// rooms
			
			it = first.getJsonArray("roomData").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(buffer, " ; " + act.getString("room")+"-entrance" + " ; " + act.getString("room")+"-duration");
			}
			
			flogn(buffer, "");
			

			
			/* content */
	
			Iterator it2 = list.iterator();
			while (it2.hasNext()) {
				JsonObject rep = (JsonObject) it2.next();
	
				// flog(buffer, "_ID ; SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TLEADER ; TTEAM ; ");
				
				flogl(buffer, 	rep.getString("_id"),
						rep.getString("sdo"), 
						rep.getString("code"),
						rep.getString("startDate"),
						rep.getString("startTime"), 
						rep.getString("endDate"),
						rep.getString("endTime"),
						rep.getString("startLeader"),
						rep.getString("startTeam"));
				
				// flog(buffer, "DEL-ACT ; DEL-ACT-date ; DEL-ACT-time ; ");
				
				flogl(buffer, rep.getString("delayedActivation"), 
						rep.getString("delayedActivationDate"), 
						rep.getString("delayedActivationTime"));
				
				/*
				
			flog(buffer, "ISS-TOT ; ");
			flog(buffer, "ISS-HG-TOT ; ISS-HG-NECK-AIS ; ISS-HG-BRAIN-AIS ; ISS-HG-SPINE-AIS ; ");
			flog(buffer, "ISS-FG-TOT ; ISS-FG-FACE-AIS ; ");
			flog(buffer, "ISS-TG-TOT ; ISS-TG-TORAX-AIS ; ISS-TG-SPINE-AIS ; ");
			flog(buffer, "ISS-AG-TOT ; ISS-AG-ABDOMEN-AIS ; ISS-AG-SPINE-AIS ; ");
			flog(buffer, "ISS-EG-TOT ; ISS-EG-UPPER-AIS ; ISS-EG-LOWER-AIS ; ISS-EG-PELVIC-AIS ; ");
			flog(buffer, "ISS-EXG-TOT ; ISS-EXG-EXT-AIS ; ");
							 
				 */
				
				flogl(buffer, 
						rep.getInteger("issTotal"),
						rep.getInteger("issHeadGroupTotalIss"),
						rep.getInteger("issHeadGroupNeckAis"),
						rep.getInteger("issHeadGroupBrainAis"),
						rep.getInteger("issHeadGroupCervicalSpineAis"),
						rep.getInteger("issFaceGroupTotalIss"),
						rep.getInteger("issFaceGroupFaceAis"),
						rep.getInteger("issToraxGroupTotalIss"),
						rep.getInteger("issToraxGroupToraxAis"),
						rep.getInteger("issToraxGroupSpineAis"),
						rep.getInteger("issAbdomenGroupTotalIss"),
						rep.getInteger("issAbdomenGroupAbdomenAis"),
						rep.getInteger("issAbdomenGroupLumbarSpineAis"),
						rep.getInteger("issExtremitiesGroupTotalIss"),
						rep.getInteger("issExtremitiesGroupUpperExtremitiesAis"),
						rep.getInteger("issExtremitiesGroupLowerExtremitiesAis"),
						rep.getInteger("issExtremitiesGroupPelvicGirdleAis"),
						rep.getInteger("issExternalGroupTotalIss"),
						rep.getInteger("issExternalGroupExternalAis")
						);
				
				/*
			flog(buffer, "TI-PAT-NAME ; TI-PAT-SURNAME ; TI-PAT-GENDER ; TI-PAT-DOB ; TI-PAT-AGE ; TI-PAT-ACC-DATE ; TI-PAT-ACC-TIME ; TI-PAT-ACC-TYPE ; TI-DECEASED ;");
			flog(buffer, "TI-VEHICLE ; TI-FROM-OTHER-ER ; TI-OTHER-ER ; ");
				 */

				flogl(buffer, 
						rep.getString("tiName"),
						rep.getString("tiSurname"),
						rep.getString("tiGender"),
						rep.getString("tiDOB"),
						rep.getInteger("tiAge"),
						rep.getString("tiAccidentDate"),
						rep.getString("tiAccidentTime"),
						rep.getString("tiAccidentType"),
						rep.getString("tiErDeceased"),
						rep.getString("tiVehicle"),
						rep.getString("tiFromOtherEmergency"),
						rep.getString("tiOtherEmergency")						
						);
				
				/*
				flog(buffer, "MTC-DYNAMIC ; MTC-PHYSIO ; MTC-ANATOM ; ");
				*/
				
				flogl(buffer, 
						rep.getString("mtcDynamic"),
						rep.getString("mtcPhysiological"),
						rep.getString("mtcAnatomical")
						);

				/*
				flog(buffer, "AN-ANTIPLATELES ; AN-ANTICOAG ; AN-NAO ; ");
				*/

				flogl(buffer, 
						rep.getString("anAntiplatelets"),
						rep.getString("anAnticoagulants"),
						rep.getString("anNao")
						);

				/*
				flog(buffer, "PREH-TERRIT ; PREH-IS-CAR-ACC ; PREH-AVALUE	 ; PREH-BPLEDEC ; PREH-CBLOODPROT ; PREH-CTPOD ; 
				PREH-DGCS-TOT ; PREH-DANISOC ; PREH-DMID	; PREH-EMOT ; PREH-WORSTBP ; PREH-WORSTRESPRATE ; ");
				 */
				
				flogl(buffer, 
						rep.getString("preHTerritorialArea"),
						rep.getString("preHIsCarAccident"),
						rep.getString("preHaValue"),
						rep.getString("preHbPleuralDecompression"),
						rep.getString("preHcBloodProtocol"),
						rep.getString("preHcTpod"),
						rep.getInteger("preHdGcsTotal"),
						rep.getString("preHdAnisocoria"),
						rep.getString("preHdMidriasi"),
						rep.getString("preHeMotility"),
						rep.getInteger("preHWorstBloodPressure"),
						rep.getInteger("preHWorstRespiratoryRate")
						);

				/*
			flog(buffer, "SVS-TEMP ;  SVS-HR ; SVS-BP ; SVS-SPO2 ; SVS-ETCO2 ; ");
				 */
	
				flogl(buffer, 
						rep.getString("svsTemp"),
						rep.getString("svsHR"),
						rep.getString("svsBP"),
						rep.getString("svsSpO2"),
						rep.getString("svsEtCO2")
						);
				
				/*
			flog(buffer, "SVS-GCSTOT ; SVS-GCS-MOT ; SVS-GCS-VERB ; SVS-GCS-EYES ; SVS-SEDATED ; SVS-PUPILS ; ");
			flog(buffer, "SVS-AIRWAY ; SVS-POSITIVE-INHAL ; SVS-INTUBFAILED ; SVS-CHESTTUBE ; SVS-OXYGPERC ; ");
			flog(buffer, "SVS-EXTBLEED ; SVS-LIMBSFRACT ; SVS-FRACTEXP ; SVS-BURN ;");
				 */
				
				flogl(buffer, 
						rep.getInteger("svsGCSTotal"),
						rep.getString("svsGCSMotor"),
						rep.getString("svsGCSVerbal"),
						rep.getString("svsGCSEyes"),
						rep.getString("svsSedated"),
						rep.getString("svsPupils"),
						rep.getString("svsAirway"),
						rep.getString("svsPositiveInhal"),
						rep.getString("svsIntubationFailed"),
						rep.getString("svsChestTube"),
						rep.getInteger("svsOxygenPercentage"),
						rep.getString("svsHemorrhage"),
						rep.getString("svsLimbsFracture"),
						rep.getString("svsFractureExposition")
						);

				flog(buffer, rep.getString("svsBurn"));

				JsonArray actions = rep.getJsonArray("procActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog(buffer, " ; ; ");
					} else {
						flog(buffer, " ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
	
				actions = rep.getJsonArray("bloodProductActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog(buffer, " ; ; ");
					} else {
						flog(buffer, " ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
				
				actions = rep.getJsonArray("drugActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog(buffer, " ; ; ");
					} else {
						flog(buffer, " ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}

				actions = rep.getJsonArray("diagActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog(buffer, "; ; ");
					} else {
						flog(buffer, " ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
				
				actions = rep.getJsonArray("roomData");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog(buffer, " ; ; ");
					} else {
						flog(buffer, " ; " + info.getInteger("entrance") + " ; " + info.getInteger("duration"));
					}
				}
				flogn(buffer, "");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			return buffer;
		}		
	}

	
}
