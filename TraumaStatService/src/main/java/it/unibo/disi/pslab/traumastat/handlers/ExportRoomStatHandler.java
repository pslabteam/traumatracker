package it.unibo.disi.pslab.traumastat.handlers;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public class ExportRoomStatHandler extends GeneralStatHandler {

	public ExportRoomStatHandler(TraumaStatService service){
		super(service);
	}

	
	@Override
	protected JsonObject computeStat(Params params, List<JsonObject> elements) {
		String dateFrom = params.getParam("dateFrom");
		String dateTo = params.getParam("dateTo");
		List<JsonObject> elems = this.elementsFromTo(elements, dateFrom, dateTo);
			
		/* compiling the report */
				
		HashMap<String,String> places = new HashMap<>();
		LinkedList<JsonObject> list = new LinkedList<JsonObject>();

		for (JsonObject rep: elems){
			
			HashMap<String,StatRoomInfo> stats = new HashMap<>();

			String startDate = rep.getString("startDate");
			String startTime = rep.getString("startTime");
					
			JsonArray events = rep.getJsonArray("events");
			Iterator<Object> it = events.iterator();
			while (it.hasNext()) {
				JsonObject ev = (JsonObject) it.next();
				
				String date = ev.getString("date");
				String time = ev.getString("time");
				
				long when = getDeltaTime(startDate,startTime,date,time);
				String et = ev.getString("type");
				
				if (et.equals("room-in") || et.equals("room-out")) {
					JsonObject content = ev.getJsonObject("content");
					String place = content.getString("place");

					if (!place.equals("PRE-H")) {
						if (et.equals("room-in")) {
							StatRoomInfo info = stats.get(place);
							if (info == null) {
								info = new StatRoomInfo(place, when);
								stats.put(place,  info);
							} else {
								info.setNewEntrance(when);
							}
							
							if (places.get(place) == null) {
								places.put(place, place);
							}
						} else {
							StatRoomInfo info = stats.get(place);
							if (info != null) {
								info.setExit(when);
							} else {
								System.err.println("FOUND ROOM OUT without IN");
							}
						}
					}
				}
			}
			
			JsonObject newRep = new JsonObject();
			
			newRep.put("sdo", rep.getJsonObject("traumaInfo").getString("sdo"));
			newRep.put("code", rep.getJsonObject("traumaInfo").getString("code"));
			newRep.put("start-date", rep.getString("startDate"));
			newRep.put("start-time", rep.getString("startTime"));
			newRep.put("end-date", rep.getString("endDate"));
			newRep.put("end-time", rep.getString("endTime"));
			newRep.put("start-leader", rep.getString("startOperatorId"));

			for (StatRoomInfo info: stats.values()) {
				JsonObject elem = new JsonObject();
				elem.put("entrance", info.getFirstTime());
				elem.put("duration", info.getDuration());
				// elem.put("exitRegistered", info.isExitOK());
				newRep.put(info.getRoomName(), elem);
			}
			// newRep.put("ncol", stats.entrySet().size());
			list.add(newRep);
		}

		String[] valuesPlaces = new String[places.values().size()];
		places.values().toArray(valuesPlaces);		
		Arrays.sort(valuesPlaces);
	
		JsonObject data = new JsonObject();		
		JsonArray plist = new JsonArray();
		for (JsonObject obj: list) {
			JsonObject el = new JsonObject();
			el.put("sdo", obj.getString("sdo"));			
			el.put("code", obj.getString("code"));
			el.put("start-date", obj.getString("start-date"));
			el.put("start-time", obj.getString("start-time"));
			el.put("end-date", obj.getString("end-date"));
			el.put("end-time", obj.getString("end-time"));
			el.put("start-leader", obj.getString("start-leader"));

			JsonArray actList = new JsonArray();
			for (String key: valuesPlaces) {
					JsonObject elem = new JsonObject();
					JsonObject info = obj.getJsonObject(key);
					elem.put("room", key);
					elem.put("info", info);
					actList.add(elem);
			}			
			el.put("room-data", actList);		
			
			plist.add(el);
		}

		
		data.put("patients", plist);

		return data;
	}
	
	private static void fill(JsonObject source, JsonObject target, HashMap<String, String> map,  String fieldName) {
		String[] values = new String[map.values().size()];
		map.values().toArray(values);		
		Arrays.sort(values);
		JsonArray actList = new JsonArray();
		for (String key: values) {
				JsonObject elem = new JsonObject();
				JsonObject info = source.getJsonObject(key);
				elem.put("param", key);
				elem.put("info", info);
				actList.add(elem);
		}			
		target.put(fieldName, actList);		
	}

	private long getDeltaTime(String startDate, String startTime, String evDate, String evTime) {

			long startValue = 
					Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
			long evValue = 
					Integer.parseInt(evTime.substring(0, 2))*3600 + Integer.parseInt(evTime.substring(3, 5))*60 + Integer.parseInt(evTime.substring(6));

			int startDay = Integer.parseInt(startDate.substring(8));
			int endDay = Integer.parseInt(evDate.substring(8));
			if (startDay != endDay){
				evValue = evValue + 3600*24;
			}
			return evValue - startValue;
		
	}
	private JsonObject computeBasicStat(List<Double> values, double sum) {
		JsonObject obj = new JsonObject();
		double avg = sum / values.size();
		
		obj.put("average", avg);
		
		obj.put("standDev", computeStandDev(values, avg));
		
		/* calcolo mediana */
		
		values.sort((Double a, Double b) -> a > b ? 1 : (a == b ? 0 : -1)); 
		
		double median = computeMedian(values, 0, values.size() - 1);
		obj.put("median", median);

		/* calcolo quartili */
		
		double q1 = 0;
		double q3 = 0;
		if (values.size() % 2 == 0) {
			q1 = computeMedian(values, 0, values.size() / 2 - 1);
			q3 = computeMedian(values, values.size() / 2, values.size() - 1);
		} else {
			q1 = computeMedian(values, 0, (values.size() - 1) / 2 - 1);
			q3 = computeMedian(values, (values.size() + 1) / 2, values.size() - 1);
		}
		
		double iqr = q3 - q1;		
		obj.put("iqr", iqr);
				
		return obj;
	}
	
	private double computeMedian(List<Double> values, int indexFrom, int indexTo) {
		double median = 0;
		int n = (indexTo - indexFrom) + 1;
		if (n % 2 == 0) {
			median = (values.get(indexFrom + n/2) + values.get(indexFrom + (n + 1)/2)) / 2.0;
		} else {
			median = values.get(indexFrom + (n + 1)/2);
		}
		return median;
	}
	
	private double computeStandDev(List<Double> elems, double avg) {
		double sum = 0;
		for (Double e: elems) {
			double diff = (e - avg); 
			sum += diff*diff;
		}
		sum = sum / elems.size();
		return Math.sqrt(sum);
	}

		
	private long getTotalTimeInRoom(JsonArray events, String room) {
		Iterator<Object> it = events.iterator();
		
		long totTime = 0;
		long absTimeFrom = -1;
		
		while (it.hasNext()) {
			JsonObject ev = (JsonObject) it.next();
			String date = ev.getString("date");
			String time = ev.getString("time");
			String type = ev.getString("type");
			if (type.equals("room-in") || (type.equals("patient-accepted") && room.equals("Shock-Room"))) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room) && absTimeFrom == -1) {
						absTimeFrom = getAbsTimeInSec(date, time);
					} else if (!place.equals(room) && absTimeFrom != -1) {
						/* changing the room without a room out event */
						long absTimeTo = getAbsTimeInSec(date, time);						
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			} else if (type.equals("room-out") && absTimeFrom != -1) {
				JsonObject content = ev.getJsonObject("content");
				if (content != null) {
					String place = content.getString("place");
					if (place.equals(room)) {
						long absTimeTo = getAbsTimeInSec(date, time);
						totTime = absTimeTo - absTimeFrom;
						absTimeFrom = -1;
					}
				}
			}
		}
		
		return totTime;
	}
	
	private long getAbsTimeInSec(String startDate, String startTime) {
		if (startDate != null && !startDate.equals("") && 
			startTime != null && !startTime.equals("")) {

			// date: // yyyy-mm-dd
			// time: // hh:mm:ss
			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
		    try {
		    		Date date = sdf.parse(startDate);
		    
				long startValue = 
						Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
	
			    return date.getTime()/1000 + startValue;
		    } catch (Exception ex) {
				throw new IllegalArgumentException("invalid date");
		    }
		} else {
			throw new IllegalArgumentException("invalid date");
		}
		
	}

	private int checkMax(JsonObject obj, String aisn, int maxIss) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv > maxIss) {
				return hnv;
			} else {
				return maxIss;
			}
		} else {
			return maxIss;
		}
	}

	private boolean checkMaxAIS(JsonObject obj, String aisn) {
		String ais = obj.getString(aisn);
		if (ais != null) {
			int hnv =  Integer.parseInt(ais);
			if (hnv >= 3) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


}
