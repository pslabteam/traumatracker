package it.unibo.disi.pslab.traumastat.tools;

import java.io.FileWriter;
import java.util.Iterator;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.traumatracker.util.TTConfig;

public class TSExportAggregate {

	private static final String TT_CONFIG_FILE = "./config.json";
	private static final String API_BASE_PATH = "/gt2/traumastat/api";
	private 	static FileWriter writer;

	public static void main(String[] args) throws Exception {

		TTConfig config;
		try {
			String configFile = TT_CONFIG_FILE;
			if (args.length == 1) {
				configFile = args[0];
			}
			config = new TTConfig(configFile);
		} catch (Exception ex){
			System.err.println("Config file not found or invalid");
			config = new TTConfig();
		}

		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx
				.createHttpClient(
						new HttpClientOptions()
							.setDefaultHost("localhost"/*args[0]*/)
							.setDefaultPort(config.getPort()));

		requestAggrExport(client, "2017-01-01", "2019-06-15");
	}

	private static void requestAggrExport(HttpClient client, String from, String to) {
		Buffer buffer = Buffer.buffer("{ }");

		client
			.get(API_BASE_PATH + "/export-aggr?dateFrom=" + from + "&dateTo=" + to)
			.handler(res -> {
				System.out.println("response status " + res.statusCode());
				res.bodyHandler(res2 -> {
					JsonObject data = res2.toJsonObject();
					JsonArray list = data.getJsonArray("patients");					
					dumpCSV(list);
					System.exit(0);
				});
			})
			.setTimeout(100000)
			.putHeader("content-type", "application/json")
			.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer).end();
		
	}
	
	private static void requestExport(HttpClient client) {
		Buffer buffer = Buffer.buffer("{ }");

		client
			.get(API_BASE_PATH + "/export?dateFrom=2017-01-01&dateTo=2018-11-01")
			.handler(res -> {
				System.out.println("response status " + res.statusCode());
				res.bodyHandler(res2 -> {
					JsonObject data = res2.toJsonObject();
					JsonArray list = data.getJsonArray("patients");
					
					dumpCSV(list);
					System.exit(0);
				});
			})
			.setTimeout(100000)
			.putHeader("content-type", "application/json")
			.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer).end();
		
	}
	
	private static void requestExportRoom(HttpClient client) {
		Buffer buffer = Buffer.buffer("{ }");

		client
			.get(API_BASE_PATH + "/export-room-data?dateFrom=2017-01-01&dateTo=2018-11-01")
			.handler(res -> {
				System.out.println("response status " + res.statusCode());
				res.bodyHandler(res2 -> {
					JsonObject data = res2.toJsonObject();
					JsonArray list = data.getJsonArray("patients");
					
					dumpRoomCSV(list);
					
					/*
					Iterator it = list.iterator();
					while (it.hasNext()) {
						JsonObject rep = (JsonObject) it.next();
						dumpRoomInfo(rep);
					}*/
					
					System.exit(0);
				});
			})
			.setTimeout(100000)
			.putHeader("content-type", "application/json")
			.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer).end();
		
	}	
	
	
	private static void dumpCSV(JsonArray list) {
		
		try {
			writer = new FileWriter("stat.csv");
		
			/* header */
			
			JsonObject first = list.getJsonObject(0);
			flog("_ID ; SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TLEADER ; TTEAM ; ");
			
			flog("DEL-ACT ; DEL-ACT-date ; DEL-ACT-time ; ");
			
			flog("ISS-TOT ; ");
			flog("ISS-HG-TOT ; ISS-HG-NECK-AIS ; ISS-HG-BRAIN-AIS ; ISS-HG-SPINE-AIS ; ");
			flog("ISS-FG-TOT ; ISS-FG-FACE-AIS ; ");
			flog("ISS-TG-TOT ; ISS-TG-TORAX-AIS ; ISS-TG-SPINE-AIS ; ");
			flog("ISS-AG-TOT ; ISS-AG-ABDOMEN-AIS ; ISS-AG-SPINE-AIS ; ");
			flog("ISS-EG-TOT ; ISS-EG-UPPER-AIS ; ISS-EG-LOWER-AIS ; ISS-EG-PELVIC-AIS ; ");
			flog("ISS-EXG-TOT ; ISS-EXG-EXT-AIS ; ");
			
			
			flog("TI-PAT-NAME ; TI-PAT-SURNAME ; TI-PAT-GENDER ; TI-PAT-DOB ; TI-PAT-AGE ; TI-PAT-ACC-DATE ; TI-PAT-ACC-TIME ; TI-PAT-ACC-TYPE ; TI-DECEASED ;");
			flog("TI-VEHICLE ; TI-FROM-OTHER-ER ; TI-OTHER-ER ; ");
			
			flog("MTC-DYNAMIC ; MTC-PHYSIO ; MTC-ANATOM ; ");
			flog("AN-ANTIPLATELES ; AN-ANTICOAG ; AN-NAO ; ");
			
			flog("PREH-TERRIT ; PREH-IS-CAR-ACC ; PREH-AVALUE	 ; PREH-BPLEDEC ; PREH-CBLOODPROT ; PREH-CTPOD ;  PREH-DGCS-TOT ; PREH-DANISOC ; PREH-DMID ; PREH-EMOT ; PREH-WORSTBP ; PREH-WORSTRESPRATE ; ");
			flog("SVS-TEMP ;  SVS-HR ; SVS-BP ; SVS-SPO2 ; SVS-ETCO2 ; ");
			flog("SVS-GCSTOT ; SVS-GCS-MOT ; SVS-GCS-VERB ; SVS-GCS-EYES ; SVS-SEDATED ; SVS-PUPILS ; ");
			flog("SVS-AIRWAY ; SVS-POSITIVE-INHAL ; SVS-INTUBFAILED ; SVS-CHESTTUBE ; SVS-OXYGPERC ; ");
			flog("SVS-HEMORR ; SVS-LIMBSFRACT ; SVS-FRACTEXP ; SVS-BURN ");
			
			// actions
			
			Iterator it = first.getJsonArray("procActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("drugActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("bloodProductActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			it = first.getJsonArray("diagActions").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("param") + "-first ; " + act.getString("param") + "-rep " );
			}
			
			// rooms
			
			it = first.getJsonArray("roomData").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("room")+"-entrance" + " ; " + act.getString("room")+"-duration");
			}
			
			flogn("");
			

			
			/* content */
	
			Iterator it2 = list.iterator();
			while (it2.hasNext()) {
				JsonObject rep = (JsonObject) it2.next();
	
				// flog("_ID ; SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TLEADER ; TTEAM ; ");
				
				flogl(	rep.getString("_id"),
						rep.getString("sdo"), 
						rep.getString("code"),
						rep.getString("startDate"),
						rep.getString("startTime"), 
						rep.getString("endDate"),
						rep.getString("endTime"),
						rep.getString("startLeader"),
						rep.getString("startTeam"));
				
				// flog("DEL-ACT ; DEL-ACT-date ; DEL-ACT-time ; ");
				
				flogl(rep.getString("delayedActivation"), 
						rep.getString("delayedActivationDate"), 
						rep.getString("delayedActivationTime"));
				
				/*
				
			flog("ISS-TOT ; ");
			flog("ISS-HG-TOT ; ISS-HG-NECK-AIS ; ISS-HG-BRAIN-AIS ; ISS-HG-SPINE-AIS ; ");
			flog("ISS-FG-TOT ; ISS-FG-FACE-AIS ; ");
			flog("ISS-TG-TOT ; ISS-TG-TORAX-AIS ; ISS-TG-SPINE-AIS ; ");
			flog("ISS-AG-TOT ; ISS-AG-ABDOMEN-AIS ; ISS-AG-SPINE-AIS ; ");
			flog("ISS-EG-TOT ; ISS-EG-UPPER-AIS ; ISS-EG-LOWER-AIS ; ISS-EG-PELVIC-AIS ; ");
			flog("ISS-EXG-TOT ; ISS-EXG-EXT-AIS ; ");
							 
				 */
				
				flogl(
						rep.getInteger("issTotal"),
						rep.getInteger("issHeadGroupTotalIss"),
						rep.getInteger("issHeadGroupNeckAis"),
						rep.getInteger("issHeadGroupBrainAis"),
						rep.getInteger("issHeadGroupCervicalSpineAis"),
						rep.getInteger("issFaceGroupTotalIss"),
						rep.getInteger("issFaceGroupFaceAis"),
						rep.getInteger("issToraxGroupTotalIss"),
						rep.getInteger("issToraxGroupToraxAis"),
						rep.getInteger("issToraxGroupSpineAis"),
						rep.getInteger("issAbdomenGroupTotalIss"),
						rep.getInteger("issAbdomenGroupAbdomenAis"),
						rep.getInteger("issAbdomenGroupLumbarSpineAis"),
						rep.getInteger("issExtremitiesGroupTotalIss"),
						rep.getInteger("issExtremitiesGroupUpperExtremitiesAis"),
						rep.getInteger("issExtremitiesGroupLowerExtremitiesAis"),
						rep.getInteger("issExtremitiesGroupPelvicGirdleAis"),
						rep.getInteger("issExternalGroupTotalIss"),
						rep.getInteger("issExternalGroupExternalAis")
						);
				
				/*
			flog("TI-PAT-NAME ; TI-PAT-SURNAME ; TI-PAT-GENDER ; TI-PAT-DOB ; TI-PAT-AGE ; TI-PAT-ACC-DATE ; TI-PAT-ACC-TIME ; TI-PAT-ACC-TYPE ; TI-DECEASED ;");
			flog("TI-VEHICLE ; TI-FROM-OTHER-ER ; TI-OTHER-ER ; ");
				 */

				flogl(
						rep.getString("tiName"),
						rep.getString("tiSurname"),
						rep.getString("tiGender"),
						rep.getString("tiDOB"),
						rep.getInteger("tiAge"),
						rep.getString("tiAccidentDate"),
						rep.getString("tiAccidentTime"),
						rep.getString("tiAccidentType"),
						rep.getString("tiErDeceased"),
						rep.getString("tiVehicle"),
						rep.getString("tiFromOtherEmergency"),
						rep.getString("tiOtherEmergency")						
						);
				
				/*
				flog("MTC-DYNAMIC ; MTC-PHYSIO ; MTC-ANATOM ; ");
				*/
				
				flogl(
						rep.getString("mtcDynamic"),
						rep.getString("mtcPhysiological"),
						rep.getString("mtcAnatomical")
						);

				/*
				flog("AN-ANTIPLATELES ; AN-ANTICOAG ; AN-NAO ; ");
				*/

				flogl(
						rep.getString("anAntiplatelets"),
						rep.getString("anAnticoagulants"),
						rep.getString("anNao")
						);

				/*
				flog("PREH-TERRIT ; PREH-IS-CAR-ACC ; PREH-AVALUE	 ; PREH-BPLEDEC ; PREH-CBLOODPROT ; PREH-CTPOD ; 
				PREH-DGCS-TOT ; PREH-DANISOC ; PREH-DMID	; PREH-EMOT ; PREH-WORSTBP ; PREH-WORSTRESPRATE ; ");
				 */
				
				flogl(
						rep.getString("preHTerritorialArea"),
						rep.getString("preHIsCarAccident"),
						rep.getString("preHaValue"),
						rep.getString("preHbPleuralDecompression"),
						rep.getString("preHcBloodProtocol"),
						rep.getString("preHcTpod"),
						rep.getInteger("preHdGcsTotal"),
						rep.getString("preHdAnisocoria"),
						rep.getString("preHdMidriasi"),
						rep.getString("preHeMotility"),
						rep.getInteger("preHWorstBloodPressure"),
						rep.getInteger("preHWorstRespiratoryRate")
						);

				/*
			flog("SVS-TEMP ;  SVS-HR ; SVS-BP ; SVS-SPO2 ; SVS-ETCO2 ; ");
				 */
	
				flogl(
						rep.getString("svsTemp"),
						rep.getString("svsHR"),
						rep.getString("svsBP"),
						rep.getString("svsSpO2"),
						rep.getString("svsEtCO2")
						);
				
				/*
			flog("SVS-GCSTOT ; SVS-GCS-MOT ; SVS-GCS-VERB ; SVS-GCS-EYES ; SVS-SEDATED ; SVS-PUPILS ; ");
			flog("SVS-AIRWAY ; SVS-POSITIVE-INHAL ; SVS-INTUBFAILED ; SVS-CHESTTUBE ; SVS-OXYGPERC ; ");
			flog("SVS-EXTBLEED ; SVS-LIMBSFRACT ; SVS-FRACTEXP ; SVS-BURN ;");
				 */
				
				flogl(
						rep.getInteger("svsGCSTotal"),
						rep.getString("svsGCSMotor"),
						rep.getString("svsGCSVerbal"),
						rep.getString("svsGCSEyes"),
						rep.getString("svsSedated"),
						rep.getString("svsPupils"),
						rep.getString("svsAirway"),
						rep.getString("svsPositiveInhal"),
						rep.getString("svsIntubationFailed"),
						rep.getString("svsChestTube"),
						rep.getInteger("svsOxygenPercentage"),
						rep.getString("svsHemorrhage"),
						rep.getString("svsLimbsFracture"),
						rep.getString("svsFractureExposition")
						);

				flog(rep.getString("svsBurn"));

				JsonArray actions = rep.getJsonArray("procActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
	
				actions = rep.getJsonArray("bloodProductActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
				
				actions = rep.getJsonArray("drugActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}

				actions = rep.getJsonArray("diagActions");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog ("; ; ");
					} else {
						flog(" ; " + act.getJsonObject("info").getInteger("first") + " ; " + act.getJsonObject("info").getInteger("rep"));
					}
				}
				
				actions = rep.getJsonArray("roomData");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + info.getInteger("entrance") + " ; " + info.getInteger("duration"));
					}
				}
				flogn("");
			}

			log("DONE.");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				writer.flush();
				writer.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}

	private static void dumpRoomCSV(JsonArray list) {
		
		try {
			writer = new FileWriter("stat-room.csv");
		
			/* header */
			
			JsonObject first = list.getJsonObject(0);
			flog("SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TL");
			
			Iterator it = first.getJsonArray("room-data").iterator();
			while (it.hasNext()) {
				JsonObject act = (JsonObject) it.next();
				flog(" ; " + act.getString("room")+"-entrance" + " ; " + act.getString("room")+"-duration");
			}
			flogn("");
			
			/* content */
	
			Iterator it2 = list.iterator();
			while (it2.hasNext()) {
				JsonObject rep = (JsonObject) it2.next();
	
				flog(rep.getString("sdo") + " ; " + 
						rep.getString("code") + " ; " +
						rep.getString("start-date") + " ; " +
						rep.getString("start-time") + " ; " +
						rep.getString("end-date") + " ; " +
						rep.getString("end-time") + " ; " +
						rep.getString("start-leader"));
				
				// flog("SDO ; CODE ; DATE-start ; TIME-start ; DATE-end ; TIME-end ; TL ; ");
				
				JsonArray actions = rep.getJsonArray("room-data");
				it = actions.iterator();
				while (it.hasNext()) {
					JsonObject act = (JsonObject) it.next();
					JsonObject info = act.getJsonObject("info");
					if (info == null) {
						flog (" ; ; ");
					} else {
						flog(" ; " + info.getInteger("entrance") + " ; " + info.getInteger("duration"));
					}
				}
				flogn("");
			}

			log("DONE.");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				writer.flush();
				writer.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}
	
	private static void dumpRoomInfo(JsonObject rep) {

		logn("SDO: " + rep.getString("sdo"));
		JsonArray actions = rep.getJsonArray("room-data");
		Iterator it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("room") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");
	}
	
	
	
	private static void dump(JsonObject rep) {

		logn("SDO: " + rep.getString("sdo"));
		JsonArray actions = rep.getJsonArray("proc-actions");
		Iterator it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("param") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");

		actions = rep.getJsonArray("drug-actions");
		it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("param") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");
		
		actions = rep.getJsonArray("diag-actions");
		it = actions.iterator();
		while (it.hasNext()) {
			JsonObject act = (JsonObject) it.next();
			log(act.getString("param") + ":" + act.getJsonObject("info") + ", ");
		}
		logn("");
	}
	
	private static void log(String msg) {
		System.out.print(msg);
	}
	
	private static void logn(String msg) {
		System.out.println(msg);
	}

	private static void flog(String msg) {
		try {
			writer.write(msg);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void flogl(Object... msgs) {
		try {
			for (Object m: msgs) {
				if (m != null) {
					writer.write(m + " ; ");
				} else {
					writer.write(" ; ");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void flogn(String msg) {
		try {
			writer.write(msg + "\n");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
