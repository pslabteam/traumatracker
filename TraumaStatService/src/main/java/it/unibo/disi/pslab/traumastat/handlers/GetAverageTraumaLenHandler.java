package it.unibo.disi.pslab.traumastat.handlers;

import java.util.HashMap;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.traumastat.AbstractServiceHandler;
import it.unibo.disi.pslab.traumastat.TraumaStatService;

public class GetAverageTraumaLenHandler extends AbstractServiceHandler {

	public GetAverageTraumaLenHandler(TraumaStatService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		// log("Handling Get Reports from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		// log("handle get reports");
		JsonObject query = new JsonObject();
		
		// log("doing a query: \n" + query.encodePrettily());
		findInReports(query, res -> {
			if (res.succeeded()) {
				HashMap<String,Integer> freq = new HashMap<String,Integer>();
				double sum = 0;
				int nReps = 0;
				for (JsonObject rep : res.result()) {
					
					String startDate = rep.getString("startDate"); // yyyy-mm-dd
					String startTime = rep.getString("startTime"); // hh:mm:ss
					
					String endDate = rep.getString("endDate"); // yyyy-mm-dd
					String endTime = rep.getString("endTime"); // hh:mm:ss
					
					// System.out.println("startTime - "+startTime+" - date "+startDate);
					// System.out.println("endTime - "+endTime+" - date "+endDate);

					if (startDate != null && !startDate.equals("") && 
						startTime != null && !startTime.equals("") && 
						endDate != null && !endDate.equals("") && 
						endTime != null && !endTime.equals("")) {
						
						long startValue = 
								Integer.parseInt(startTime.substring(0, 2))*3600 + Integer.parseInt(startTime.substring(3, 5))*60 + Integer.parseInt(startTime.substring(6));
						long endValue = 
								Integer.parseInt(endTime.substring(0, 2))*3600 + Integer.parseInt(endTime.substring(3, 5))*60 + Integer.parseInt(endTime.substring(6));
	
						int startDay = Integer.parseInt(startDate.substring(8));
						int endDay = Integer.parseInt(endDate.substring(8));
						// System.out.println("-> "+ startValue + " - "+ endValue);
						// System.out.println("-> "+startDay + " - " +endDay);
						if (startDay < endDay){
							endValue = endValue + 3600*24;
						}
						sum += (endValue-startValue);
						nReps++;
					}
				}
				
				int average = (int)(nReps > 0 ? sum/nReps : 0);
				
				int hh = average / 3600;
				int mm = (average % 3600) / 60;
				int ss = (average % 3600) % 60;
				
				JsonObject stat = new JsonObject(); 
				stat.put("nReports", nReps);
				stat.put("averageTime", 
						new JsonObject()
						.put("hh", hh)
						.put("mm", mm)
						.put("ss", ss));
				response.putHeader("content-type", "application/json").end(stat.encodePrettily());
			} else {
				sendError(404, response);
			}
		});
	}
	
}
