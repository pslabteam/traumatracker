package it.unibo.disi.pslab.traumastat;

import io.vertx.ext.mongo.*;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.traumatracker.util.TTConfig;
import io.vertx.core.Handler;
import io.vertx.core.file.FileSystem;
import io.vertx.core.AsyncResult;

import java.util.Date;
import java.util.List;

public abstract class AbstractServiceHandler implements Handler<RoutingContext> {

	TraumaStatService service;
	
	public AbstractServiceHandler(TraumaStatService service){
		this.service = service;
	}

	abstract public void handle(RoutingContext routingContext);
	
	protected TraumaStatService getService() {
		return service;
	}
	
	protected void sendError(int statusCode, HttpServerResponse response){
		response.setStatusCode(statusCode).end();
	}
	
	protected void log(String msg){
		service.log(msg);
	}

	protected void logError(String msg){
		service.logError(msg);
	}
	
	protected Date getStartTime() {
		return service.getStartTime();
	}
	
	protected MongoClient getStore(){
		return service.getStore();
	}
	
	protected TTConfig getConfig(){
		return service.getConfig();
	}
	
	protected void findInUsers(JsonObject query, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().find(getConfig().getUsersCollectionName(), query, handler);		
	}

	protected void storeInUsers(JsonObject user, Handler<AsyncResult<String>> handler){
		service.getStore().insert(getConfig().getUsersCollectionName(), user, handler);		
	}

	protected void findInReports(JsonObject query, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().find(getConfig().getReportsCollectionName(), query, handler);		
	}

	protected void saveInReports(JsonObject doc, Handler<AsyncResult<String>> handler){
		service.getStore().save(getConfig().getReportsCollectionName(), doc, handler);		
	}
	
	protected void findInReportsWithOptions(JsonObject query, JsonObject opt, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().findWithOptions(getConfig().getReportsCollectionName(), query, new FindOptions(opt), handler);		
	}
	
	protected FileSystem getFS() {
		return service.getFS();
	}
}
