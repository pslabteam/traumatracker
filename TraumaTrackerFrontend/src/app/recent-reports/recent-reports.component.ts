import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ReportService } from '../shared/report.service';
import { AppStateService } from '../shared/app-state.service';
import { Subscription } from "rxjs/Subscription";


@Component( {
    selector: 'recent-reports',
    templateUrl: './recent-reports.component.html',
    providers: [
        ReportService
    ],
} )
export class RecentReportsComponent implements OnInit, OnDestroy {

    public reports;
    private repUpdateSub: Subscription;
    private timer;
    private timerSub: Subscription;

    constructor(
        private reportService: ReportService,
        private appStateService: AppStateService
    ) { }

    ngOnInit(): void {
      this.reportService.startFetchingRecentReports();
      this.repUpdateSub = this.reportService.recentReportsUpdatedChannel$.subscribe((reports) => {
        this.reports = reports;
      });
    }
    
    ngOnDestroy() {
        if ( this.repUpdateSub ) {
            this.repUpdateSub.unsubscribe(); 
       }
    }  
}