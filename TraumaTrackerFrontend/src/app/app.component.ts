import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppStateService } from './shared/app-state.service';
import { Subscription } from "rxjs/Subscription";

@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css',"../../node_modules/ng2-toastr/bundles/ng2-toastr.min.css"],    
} )
export class AppComponent implements OnInit, OnDestroy {

    
    private reportSub: Subscription;
    private selectedReport;
    private eventVisualisedPhoto;
    
    constructor(
        private appStateService: AppStateService
    ) { };

    ngOnInit(): void {
      this.reportSub = this.appStateService.appEventChannel$.subscribe((event) => {
          if (event.eventType == 'report-selected'){
              this.selectedReport = event.report;
          } else if (event.eventType == 'report-unselected'){
              this.selectedReport = {};
          } else if (event.eventType == 'photo-selected'){
              this.selectedReport = event.report;
              this.eventVisualisedPhoto = event.event;
        }  
        });
    }

    ngOnDestroy(): void {      
        if ( this.reportSub ) {
            this.reportSub.unsubscribe();
        }      
    }

}
