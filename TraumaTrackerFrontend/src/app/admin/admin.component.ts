import { Component } from '@angular/core';
import { ReportService } from '../shared/report.service';
import { UserService } from '../shared/user.service';

@Component( {
    selector: 'admin',
    templateUrl: './admin.component.html',
    // styleUrls: ['./admin.component.css']
} )
export class AdminComponent {

    constructor(
        private reportService: ReportService,
        private userService: UserService,
    ) { }

    resetReportsDB(): void {
        this.reportService.deleteAllReports();
    }

    resetUsersDB(): void {
        this.userService.deleteAllUsers();
        // if actions are needed after the operation
        // in the response a json object is returned 
        // with obj.req and obj.res fields 
    }

}
