import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from '../shared/login.service';
import { Router } from '@angular/router';
import { AppStateService } from '../shared/app-state.service';
import { User } from '../data-model/user';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

@Component( {
    selector: 'login',
    templateUrl: './login.component.html',
    styles: ['./login.component.css']
} )
export class LoginComponent implements OnInit {

    userId: string;
    pwd: string;
    loginMsg: string;

    constructor(
      private loginService: LoginService,
      private appStateService: AppStateService,
      private router: Router
    ) {
    }

    ngOnInit(): void {
      this.loginMsg = 'Inserire nome utente e password.';
    }

    login() {
      if (this.userId === undefined || this.userId === ""){
          this.loginMsg = 'Nome utente non specificato.';
      } else if (this.pwd === undefined || this.pwd === ""){
          this.loginMsg = 'Password non specificata.';
      } else {
        this.loginService.login( this.userId, this.pwd)
              .then( response => {
                // console.log('REPLY ' + response);
                if (response.result === 'ok') {
                   const user = new User();
                   user.userId = this.userId;
                   user.name = response.name;
                   user.surname = response.surname;
                   user.email = response.email;
                   user.pwd = this.pwd;
                   user.role = response.role;
                   user.token = response.token;
                   this.appStateService.setLoggedIn(user);
                   this.router.navigate(['/dashboard']);
                } else {
                   this.loginMsg = 'Nome utente o password non corretti.';
                }
              })
              .catch( error => {
                console.log('FAILED ' + error);
                this.loginMsg = 'Errore nella connessione con il sistema.';
              });
      }
     }

     recoverPwd() {
      if (this.userId === undefined || this.userId === ""){
          this.loginMsg = 'Nome utente non specificato.';
      } else {
          const timer = TimerObservable.create( 0, 500 );
          let n = 0;
          this.loginMsg = 'Invio in corso .';
          
          let done_ok = false;
          let done_error = false;
          let timeout = false;
        
          const timerSub = timer.subscribe(() => {
            if (!done_ok && !done_error){
               this.loginMsg = this.loginMsg + ' .'
               n++;
               if (n % 5 == 0){
                  this.loginMsg = 'Invio in corso .';
               }
               if (n > 40) {
                   timerSub.unsubscribe();
                   timeout = true;
                   this.loginMsg = 'Tempo scaduto. Riprovare.';             
               }
            } else {
               timerSub.unsubscribe();
            }
          });
           this.loginService.recoverPwd( this.userId )
              .then( response => {
                // console.log('REPLY ' + response);
                if (!timeout){
                  if (response.result === 'ok') {
                     done_ok = true;
                     this.loginMsg = 'Password inviata via email.';
                  } else {
                     done_error = true;
                     this.loginMsg = 'Nome utente non valido.';
                  }
                }
              })
              .catch( error => {
                console.log('FAILED ' + error);
                done_error = true;
                this.loginMsg = 'Errore nella connessione con il sistema.';
              });
 
       }
     }

}
