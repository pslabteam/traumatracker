import { jsPDF } from "jspdf";
import { Report, Event, ISS, ProcEventContent, DiagEventContent, DrugEventContent, 
          TraumaLeaderEventContent, TextNoteEventContent, BloodProductEventContent, ClinicalVariationEventContent, 
          RoomEventContent, VitalSignsMonEventContent } from '../data-model/report';
import { TraumaTrackerAPIService } from '../shared/trauma-tracker-api.service';

export class PrintReportLib {

  y = 25;
  nPages = 1;
  date = "xx/xx/xxxx";
  doc;
  img = new Image();
  nPass: number;
  totalPages: number;
  secondPass: boolean;
  selectedReport: Report;
    
  public photoBaseURL: string;

  
  constructor(
          private reportServiceAPI: TraumaTrackerAPIService
  ) {
      this.img.src = "../../assets/img/header.jpg"; // event.content.ref;
      this.photoBaseURL = reportServiceAPI.getPhotoDownloadURI();
  }

  printReport(report: Report, withMultimedia: boolean) {

    this.doc = new jsPDF()
    this.selectedReport = report;
    const canvas = document.createElement('canvas');
    const scale = 675 / this.img.width;
    canvas.width = this.img.width * scale;
    canvas.height = this.img.height * scale;

    const context = canvas.getContext('2d');
    context.drawImage(this.img, 0, 0, this.img.width, this.img.height, 0, 0, canvas.width, canvas.height);

    const dataURL = canvas.toDataURL('image/jpeg');
    this.doc.addImage(dataURL, "JPEG", 20, 20);

    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1;
    const yyyy = today.getFullYear();

    const ho = today.getHours();
    const mi = today.getMinutes();

    const ddToString = dd < 10 ? '0' + dd : '' + dd; 
    const mmToString = mm < 10 ? '0' + mm : '' + mm;
    const hoToString = ho < 10 ? '0' + ho : '' + ho;
    const miToString = mi < 10 ? '0' + mi : '' + mi;
    
    this.date = ddToString + '/' + mmToString + '/' + yyyy + " " + hoToString + ":" + miToString;
    this.nPages = 1;
    this.y = 80;
    this.totalPages = 0;
    this.secondPass = false;
    this.printReportContinuation(report,withMultimedia);
       
    this.totalPages = this.nPages;   
    this.doc = new jsPDF()
    this.doc.addImage(dataURL, "JPEG", 20, 20);
    
    this.doc.roundedRect(120, 60, 50, 20, 2, 2, 'S');
    this.doc.setFont("helvetica","normal");
    this.doc.setFontSize(10);
    this.doc.text(135, 71, "Etichetta PS");
   
    this.nPages = 1;
    this.y = 70;
    this.secondPass = true;
    this.printReportContinuation(report,withMultimedia);

    if (withMultimedia) {
      this.addPhotosToReport(this, this.doc, report, 0, 1);
    } else {
      this.doc.save(report._id + '.pdf');
      // Notification.success( { message: 'PDF del report ' + report._id + ' creato.', delay: 2000 } )
    }
  }
 
  private yesNo(v: boolean): string {
    if (v !== undefined) {
      return v ? 'sì' : 'no';
    } else {
      return undefined;
    }
  }
  
  private printReportContinuation(report: Report, multimediaToBeIncluded: boolean) {

    const isok = function(value) {
      return value !== undefined && value !== "";
    }
    
    this.addMainTitle("Trauma Report ");
    
    this.addTitle("DATI GENERALI");
    this.addItem(20, 110, "Operatore", report.startOperatorDescription);
    if (report.delayedActivation !== undefined && report.delayedActivation.isDelayedActivation) {
      if (report.delayedActivation.originalAccessDate === undefined){
        this.addItem(20, 110,'Attivazione ritardata', 'sì');
      } else {
        this.addItem(20, 110,'Attivazione ritardata', 'sì: data '+report.delayedActivation.originalAccessDate+" ora: "+report.delayedActivation.originalAccessTime);
      }
    }
    let res = this.filterTeamMembers(report.traumaTeamMembers);
    this.addItem(20, 110, "Trauma team", res.text);
    this.y += res.dy;
    this.addItem(20, 110, "Data inizio", report.startDate);
    this.addItem(20, 110, "Ora", report.startTime);
    this.addItem(20, 110, "Data fine", report.endDate);
    this.addItem(20, 110, "Ora", report.endTime);
    this.addItem(20, 110, "Destinazione Finale", report.finalDestination);
    if (report.iss !== undefined){
      const iss: ISS = report.iss;
      this.addItem(20, 110, "Injurity Severity Score (ISS)", 'Totale ISS: '+report.iss.totalIss);
      if (iss.aisMap.headGroup !== undefined){
          this.addSingleItem(110, 
            'Testa-Collo: '+(iss.aisMap.headGroup.headNeckAis !== undefined ? iss.aisMap.headGroup.headNeckAis : '-') +
            ', Cervello: ' + (iss.aisMap.headGroup.brainAis !== undefined ? iss.aisMap.headGroup.brainAis : '-') +
            ', Colonna Cerv.: ' + (iss.aisMap.headGroup.cervicalSpineAis !== undefined ? iss.aisMap.headGroup.cervicalSpineAis : '-') +
            ' - ISS: ' + (iss.aisMap.headGroup.groupTotalIss !== undefined ? iss.aisMap.headGroup.groupTotalIss : '-'));
      }
      if (iss.aisMap.faceGroup !== undefined){
          this.addSingleItem(110, 
            'Viso: ' + (iss.aisMap.faceGroup.faceAis !== undefined ? iss.aisMap.faceGroup.faceAis : '-') +
            ' - ISS: '+(iss.aisMap.faceGroup.groupTotalIss !== undefined ? iss.aisMap.faceGroup.groupTotalIss : '-'));
      }
      if (iss.aisMap.toraxGroup !== undefined){
          this.addSingleItem(110, 
            'Torace: '+(iss.aisMap.toraxGroup.toraxAis !== undefined ? iss.aisMap.toraxGroup.toraxAis : '-') +
            ', Colonna Vertebrale: '+ (iss.aisMap.toraxGroup.spineAis !== undefined ? iss.aisMap.toraxGroup.spineAis : '-') +
            ' - ISS: '+ (iss.aisMap.toraxGroup.groupTotalIss !== undefined ? iss.aisMap.toraxGroup.groupTotalIss : '-'));
      }
      if (iss.aisMap.abdomenGroup !== undefined){
          this.addSingleItem(110, 
            'Addome: '+(iss.aisMap.abdomenGroup.abdomenAis !== undefined ? iss.aisMap.abdomenGroup.abdomenAis : '-') + 
            ', Colonna Lombare: '+(iss.aisMap.abdomenGroup.lumbarSpineAis !== undefined ? iss.aisMap.abdomenGroup.lumbarSpineAis : '-') +
            ' - ISS: '+(iss.aisMap.abdomenGroup.groupTotalIss !== undefined ? iss.aisMap.abdomenGroup.groupTotalIss : '-'));
      }
      if (iss.aisMap.extremitiesGroup !== undefined){
          this.addSingleItem(110, 
            'Estrem. sup.: '+(iss.aisMap.extremitiesGroup.upperExtremitiesAis !== undefined ? iss.aisMap.extremitiesGroup.upperExtremitiesAis : '-') +
            ', Estrem. inf.: '+(iss.aisMap.extremitiesGroup.lowerExtremitiesAis !== undefined ? iss.aisMap.extremitiesGroup.lowerExtremitiesAis : '-') +
            ', Cintura lomb.: '+(iss.aisMap.extremitiesGroup.pelvicGirdleAis !== undefined ? iss.aisMap.extremitiesGroup.pelvicGirdleAis : '-')+
            ' - ISS: '+(iss.aisMap.extremitiesGroup.groupTotalIss !== undefined ? iss.aisMap.extremitiesGroup.groupTotalIss : '-'));
      }
      if (iss.aisMap.externaGroup !== undefined){
           this.addSingleItem(110,
             'Lesioni esterne: '+(iss.aisMap.externaGroup.externaAis !== undefined ? iss.aisMap.externaGroup.externaAis : '-')+
             ' - ISS: '+(iss.aisMap.externaGroup.groupTotalIss !== undefined ? iss.aisMap.externaGroup.groupTotalIss : '-'));
      }
    }
    const patInfo = report.traumaInfo;

    this.addSubTitleIfAnyAvailable("INFORMAZIONI TRAUMA", [patInfo.code, patInfo.gender, patInfo.name, patInfo.surname, patInfo.dob, patInfo.admissionCode, patInfo.age, patInfo.accidentDate, patInfo.accidentTime, patInfo.accidentType, patInfo.vehicle, patInfo.fromOtherEmergency]);
    this.addItem(20, 110, "Codice PS", patInfo.code);
    this.addItem(20, 110, "SDO", patInfo.sdo);
    this.addItem(20, 110, "Codice Ammissione", patInfo.admissionCode);
    this.addItem(20, 110, "Nome", patInfo.name);
    this.addItem(20, 110, "Cognome", patInfo.surname);
    this.addItem(20, 110, "Data di Nascita", patInfo.dob);
    this.addItem(20, 110, "Genere", patInfo.gender);
    this.addItem(20, 110, "Età", patInfo.age);
    this.addItem(20, 110, "Data dell'incidente", patInfo.accidentDate);
    this.addItem(20, 110, "Ora dell'incidente", patInfo.accidentTime);
    this.addItem(20, 110, "Veicolo di arrivo in PS", patInfo.vehicle);
    this.addItem(20, 110, "Provenienza da altro PS", this.yesNo(patInfo.fromOtherEmergency));
    this.addItem(20, 110, "PS di provenienza", patInfo.otherEmergency);
    this.addItem(20, 110, "Paziente deceduto in PS", this.yesNo(patInfo.erDeceased));

    // this.newPage();
    
    if (report.majorTraumaCriteria !== undefined) {
      this.addSubTitleIfAnyAvailable("CRITERI TRAUMA MAGGIORE", [report.majorTraumaCriteria.dynamic, report.majorTraumaCriteria.physiological, report.majorTraumaCriteria.anatomical]);
      this.addItem(20, 110, "Dinamico", this.yesNo(report.majorTraumaCriteria.dynamic));
      this.addItem(20, 110, "Fisiologico", this.yesNo(report.majorTraumaCriteria.physiological));
      this.addItem(20, 110, "Anatomico", this.yesNo(report.majorTraumaCriteria.anatomical));
    }
    
    if (report.anamnesi !== undefined) {
      this.addSubTitleIfAnyAvailable("ANAMNESI", [report.anamnesi.antiplatelets, report.anamnesi.anticoagulants, report.anamnesi.nao]);
      this.addItem(20, 110, "Antiaggreganti", this.yesNo(report.anamnesi.antiplatelets));
      this.addItem(20, 110, "Anticoagulanti", this.yesNo(report.anamnesi.anticoagulants));
      this.addItem(20, 110, "Nao", this.yesNo(report.anamnesi.nao));
    }
    
    if (report.preH !== undefined) {
      this.addSubTitleIfAnyAvailable("Scheda PRE-H",
        [report.preH.territorialArea, report.preH.isCarAccident, report.preH.worstBloodPressure, report.preH.worstRespiratoryRate,
         report.preH.aAirways, report.preH.bPleuralDecompression, report.preH.cBloodProtocol, report.preH.cTpod,
         report.preH.dGcsTotal, report.preH.dAnisocoria, report.preH.dMidriasi, report.preH.eMotility]);
      this.addItem(20, 110, "Ambito", report.preH.territorialArea);
      this.addItem(20, 110, "Incidente stradale", this.yesNo(report.preH.isCarAccident));
      this.addItem(20, 110, "Peggiore PAS misurata", report.preH.worstBloodPressure);
      this.addItem(20, 110, "Peggiore Frequenza Respiratoria Misurata", report.preH.worstRespiratoryRate);
      this.addItem(20, 110, "Vie Aeree", report.preH.aAirways);
      this.addItem(20, 110, "Decompressione pleurica", this.yesNo(report.preH.bPleuralDecompression));    this.addItem(20, 110, "Decompressione pleurica", this.yesNo(report.preH.bPleuralDecompression));
      this.addItem(20, 110, "Protocollo Sangue", this.yesNo(report.preH.cBloodProtocol));    
      this.addItem(20, 110, "Decompressione pleurica", this.yesNo(report.preH.bPleuralDecompression));    this.addItem(20, 110, "Decompressione pleurica", this.yesNo(report.preH.bPleuralDecompression));
      this.addItem(20, 110, "T-POD", this.yesNo(report.preH.cTpod));   
      this.addItem(20, 110, "GCS totale", report.preH.dGcsTotal);   
      this.addItem(20, 110, "Anisocoria", this.yesNo(report.preH.dAnisocoria));
      this.addItem(20, 110, "Midriasi", this.yesNo(report.preH.dMidriasi));   
      this.addItem(20, 110, "Paraparesi/Tetraparesi", this.yesNo(report.preH.eMotility));
    }
      
      
    const pat = report.patientInitialCondition;
          
    if (pat !== undefined){ 
      const startVS = pat.vitalSigns;
      const cp = pat.clinicalPicture;
      
      if (startVS !== undefined){
        this.addSubTitleIfAnyAvailable("STATO INIZIALE PAZIENTE", [startVS.temp, startVS.bp, startVS.hr, startVS.etco2, startVS.spo2,
          cp.airway, cp.burn, cp.chestTube, cp.fractureExposition, cp.gcsEyes, cp.gcsMotor, cp.gcsTotal,
          cp.gcsVerbal, cp.hemorrhage, cp.intubationFailed, cp.limbsFracture]);
        
      const exfilter = function(exact, other) {
        return exact !== undefined && exact !== "" ? exact : other;
      }
  
      const posx = 110;
  
      this.y = this.y - 5;
      
      this.addSubTitleIfAnyAvailable("Parametri vitali", [startVS.temp, startVS.bp, startVS.etco2, startVS.hr, startVS.spo2]);
      this.addItem(20, posx, "Temperatura", startVS.temp);
      this.addItem(20, posx, "Frequenza cardiaca", startVS.hr);
      this.addItem(20, posx, "Pressione arteriosa sistolica", startVS.bp);
      this.addItem(20, posx, "Saturazione (SpO2)", startVS.spo2);
      this.addItem(20, posx, "EtCO2", startVS.etco2);
     
      
      this.addSubTitleIfAnyAvailable("Esame neurologico", [cp.gcsEyes, cp.gcsMotor, cp.gcsTotal, cp.sedated, cp.gcsVerbal, cp.pupils]);
        
      this.addItem(20, posx, "GCS totale", cp.gcsTotal);
      this.addItem(20, posx, "GCS motorio", cp.gcsMotor);
      this.addItem(20, posx, "GCS verbale", cp.gcsVerbal);
      this.addItem(20, posx, "GCS oculare", cp.gcsEyes);
      this.addItem(20, posx, "Paziente sedato", this.yesNo(cp.sedated));
      this.addItem(20, posx, "Pupille", cp.pupils);
  
      this.addSubTitleIfAnyAvailable("Vie aeree e respirazione", [cp.airway, cp.positiveInhalation, cp.intubationFailed, cp.chestTube, cp.oxygenPercentage]);
      this.addItem(20, posx, "Vie aeree", cp.airway);
      this.addItem(20, posx, "Tracheo", this.yesNo(cp.positiveInhalation));
      this.addItem(20, posx, "IOT fallita", this.yesNo(cp.intubationFailed));
      this.addItem(20, posx, "Decompressione pleurica", cp.chestTube);
      this.addItem(20, posx, "Ossigeno (%)", cp.oxygenPercentage);
  
      
      this.addSubTitleIfAnyAvailable("Emorragie", [cp.hemorrhage]);
      this.addItem(20, posx, "Emorragie esterne", this.yesNo(cp.hemorrhage));
       
      this.addSubTitleIfAnyAvailable("Lesioni ortopediche", [cp.limbsFracture, cp.fractureExposition]);
      this.addItem(20, posx, "Frattura arti", this.yesNo(cp.limbsFracture));
      this.addItem(20, posx, "Frattura esposta", this.yesNo(cp.fractureExposition));
  
      this.addSubTitleIfAnyAvailable("Ustioni", [cp.burn]);
      this.addItem(20, posx, "Ustione", cp.burn);
    }
    }
    
    this.addEventTrackPage();

    let nPhoto = 1;
    let nVideo = 1;
    let nAudio = 1;

    for (let i = 0; i < report.events.length; i++) {
      const event: Event = report.events[i];
      let what = "";
      let deltay = 0;
      switch (event.evType) {
        case "procedure": {
          const evc: ProcEventContent = <ProcEventContent> event.content;
          if (evc.procedureType === "one-shot") {
            what = "Procedura " + evc.procedureDescription;
          } else {
            if (evc.event === "start") {
              what = "Inizio Procedura " + evc.procedureDescription;
            } else {
              what = "Fine Procedura " + evc.procedureDescription;
            }
          }
          if (evc.procedureId === "intubation") {
            deltay = 0;
            if (isok(evc.difficultAirway)) {
              what = what + "\r\n - vie aeree difficili: " + this.yesNo(evc.difficultAirway);
              deltay += 4;
            }
            if (isok(evc.inhalation)) {
              what = what + "\r\n - inalazione: " + this.yesNo(evc.inhalation);
              deltay += 4;
            }
            if (isok(evc.videolaringo)) {
              what = what + "\r\n - videolaringo: " + this.yesNo(evc.videolaringo);
              deltay += 4;
            }

            if (isok(evc.frova)) {
              what = what + "\r\n - frova: " + this.yesNo(evc.frova);
              deltay += 4;
            }
          } else if (evc.procedureId === "drainage") {
            deltay = 0;
            if (isok(evc.right)) {
              what = what + "\r\n - destro: " + this.yesNo(evc.right);
              deltay += 4;
            }
            if (isok(evc.left)) {
              what = what + "\r\n - sinistro: " + this.yesNo(evc.left);
              deltay += 4;
            }
          } else if (evc.procedureId === "chest-tube") {
            deltay = 0;
            if (isok(evc.right)) {
              what = what + "\r\n - destro: " + this.yesNo(evc.right);
              deltay += 4;
            }
            if (isok(evc.left)) {
              what = what + "\r\n - sinistro: " + this.yesNo(evc.left);
              deltay += 4;
            }
          }
          break;
        }
        case "diagnostic": {
          const evc: DiagEventContent = <DiagEventContent> event.content;
          what = "Esame clinico " + evc.diagnosticDescription;
          if (evc.diagnosticId === "abg") {
            deltay = 0;
            if (isok(evc.lactates)) {
              what = what + "\r\n - lattati: " + evc.lactates;
              deltay += 4;
            }
            if (isok(evc.be)) {
              what = what + "\r\n - be: " + evc.be;
              deltay += 4;
            }
            if (isok(evc.ph)) {
              what = what + "\r\n - ph: " + evc.ph;
              deltay += 4;
            }
            if (isok(evc.hb)) {
              what = what + "\r\n - hb: " + evc.hb;
              deltay += 4;
            }
          } else if (evc.diagnosticId === "rotem") {
            deltay = 0;
            if (isok(evc.fibtem)) {
              what = what + "\r\n - fibtem: " + evc.fibtem;
              deltay += 4;
            }
            if (isok(evc.extem)) {
              what = what + "\r\n - extem: " + evc.extem;
              deltay += 4;
            }
            if (isok(evc.hyperfibrinolysis)) {
              what = what + "\r\n - hyperfibrinolysis: " + this.yesNo(evc.hyperfibrinolysis);
              deltay += 4;
            }
          }
          break;
        }
        case "drug": {
          const evc: DrugEventContent = <DrugEventContent> event.content;
          if (evc.administrationType === "one-shot") {
            what = "Farmaco " + evc.drugDescription + " " + evc.qty + evc.unit;
          } else if (evc.administrationType === "continuous-infusion") {
            if (evc.event === "start") {
              what = "Inizio infusione farmaco " + evc.drugDescription + " " + evc.qty + evc.unit;
            } else if (evc.event === "variation") {
              what = "Variazione infusione farmaco " + evc.drugDescription + " " + evc.qty + evc.unit;
            } else {
              what = "Fine infusione farmaco " + evc.drugDescription + "\r\n - durata: " + evc.duration + "s";
            }
          } else {
              what = "Protocollo farmaco " + evc.drugDescription;
          }
          break;
        }
        case "blood-product": {
          const evc: BloodProductEventContent = <BloodProductEventContent> event.content;
          if  ( evc.administrationType == undefined || evc.administrationType === "one-shot" )  {
            let code = '';
                   if (evc.bloodProductId === 'emazies' || evc.bloodProductId === 'platelets' ||  evc.bloodProductId === 'fresh-frozen-plasma'){
                      code = '\r\n (Codice sacca: ' + evc.bagCode + ')';
                      deltay += 4;
                   }
            what =  "Emoderivato: " + evc.bloodProductDescription + " " + evc.qty + evc.unit + code;
          } else if (evc.administrationType === "blood-protocol") {
              what = "Emoderivato: " + evc.bloodProductDescription;
          }
          break;
        }
        case "vital-signs-mon": {
          const evc: VitalSignsMonEventContent = <VitalSignsMonEventContent> event.content;          
          what = "Parametri vitali monitor: \r\n" +
            "TEMP " + evc.Temp +
            "; HR " + evc.HR +
            "; DIA " + evc.DIA +
            "; SYS " + evc.SYS +
            "; SpO2 " + evc.SpO2 +
            "; etCO2 " + evc.EtCO2;
          deltay = 4;
          break;
        }
        case "clinical-variation": {
          const evc: ClinicalVariationEventContent = <ClinicalVariationEventContent> event.content;
          if (evc.variationId !== "sedated"){
            what = "Variazione parametro vitale: \r\n" + evc.variationDescription + " - nuovo valore: " + evc.value;
          } else {
            what = "Variazione parametro vitale: \r\n" + evc.variationDescription + " - nuovo valore: " + (evc.value == "true" ? "sì" : "no");
          }
          deltay = 4;
          break;
        }
        case "trauma-leader": {
          const evc: TraumaLeaderEventContent = <TraumaLeaderEventContent> event.content;          
          what = "Cambio trauma leader: " + evc.name + " "+evc.surname;
          break;
        }
        case "photo": {
          if (multimediaToBeIncluded) {
            what = "Foto #" + nPhoto + ": " + event.eventId;
            nPhoto++;
          };
          break;
        }
        case "video": {
          if (multimediaToBeIncluded) {
            what = "Video #" + nVideo + ": " + event.eventId;
            nVideo++;
          };
          break;
          }
        case "vocal-note": {
          if (multimediaToBeIncluded) {
            what = "Registrazione vocale #" + nAudio + ": " + event.eventId;
            nAudio++;
          };
          break;
        }
        case "text-note": {
          const text: string = (<TextNoteEventContent>event.content).text;          
          let txt = '';
          const len = text.length;
          //const words = evc.text.split(" ");
          let rlen = 0;
          let index = 0;
          deltay = 4;
          while (index < len) {
            const ch = text[index];
            index++;
            if (ch == '\n' || ch == '\r'){
              txt = txt + "\r\n";
              rlen = 0;
              deltay += 4;
            } else {
              txt = txt + ch;
              rlen++;
              if (rlen > 50) {
                txt = txt + "\r\n";
                rlen = 0;
                deltay += 4;
              }
            }
          }
          what = "Nota testuale: \r\n" + txt;
          break
        }
        
        case "room-in": {
          const evc: RoomEventContent = <RoomEventContent> event.content; 
          if (evc.place === 'PRE-H') {  
            what =  "Attivazione del Trauma Team";
          } else if (evc.place === 'Trasporto') {
            what =  "Inizio trasporto paziente";
          } else {
            what = "Ingresso luogo: " + evc.place;
          }
          break
        }
        case "room-out":{
          const evc: RoomEventContent = <RoomEventContent> event.content;          
          if (evc.place === 'Trasporto') {
            what = 'Fine trasporto paziente';
          } else {
            what = "Uscita luogo: " + evc.place;
          }
          break
          }
        case "patient-accepted":{
          what = "Ingresso del paziente in PS";
          break;
        }
        default: {
          what = "unknown: " + event.evType;
          break;
          }
      }

      let place = event.place;
      if (event.place === "Sala Operatoria: Chirurgia Generale") {
        place = "Chirurgia Generale (SO)";
      } else if (event.place === "Sala Operatoria: Neurochirurgia") {
        place = "Neurochirurgia (SO)";
      } else if (event.place === "Sala Operatoria: Ortopedia") {
        place = "Ortopedia (SO)";
      }

      if ((event.evType !== "photo" && event.evType !== "video" && event.evType !== "vocal-note") || multimediaToBeIncluded) {
        this.addEventItem(event.date, this.filterTime(event.time), place, what, deltay);
      }
    }

    this.printFooter();
  }
 
  
  public addPhotosToReport(service, doc, report: Report, i, photoCount) {
    while (i < report.events.length && report.events[i].evType !== "photo") {
      i++;
    }
    if (i < report.events.length) {
      const event = report.events[i];
      const img = new Image();
      img.crossOrigin = "Anonymous";
      img.src = this.photoBaseURL + "/"+report._id + "-" + event.eventId + ".jpg"; // event.content.ref;
      img.onload = function() {
        const canvas = document.createElement('canvas');
        
        const scale = 600 / img.width;
        canvas.width = img.width * scale;
        canvas.height = img.height * scale;
        
        const context = canvas.getContext('2d');        
        context.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);

        const dataURL = canvas.toDataURL('image/jpeg');
        
        doc.addPage();

        doc.setFont("helvetica","bold")
        doc.text(20, 20, "Foto #" + photoCount + ": ")
        doc.setFont("helvetica","normal")
        doc.text(40, 20, report._id + "-" + event.eventId + ".jpg")
        doc.addImage(dataURL, "JPEG", 20, 30);

        service.addPhotosToReport(service, doc, report, i + 1, photoCount + 1)
      }
    } else {
      doc.save(report._id + '.pdf');
      //Notification.success( { message: 'PDF del report ' + report._id + ' creato.', delay: 2000 } )
    }
  }

  private addMainTitle(title) {
    this.doc.setFontSize(20);
    this.doc.text(20, this.y, title);
    this.doc.setFontSize(10);
    this.y += 10;
  }

  private addTitle(title) {
    this.y += 5;
    // this.doc.setFont("helvetica","bold");
    this.doc.setFontSize(12);
    this.doc.text(20, this.y, title);
    this.doc.line(20, this.y + 4, 190, this.y + 4);
    this.y += 10;
    this.doc.setFontSize(10);
    this.checkNewPage();
  }

  private addSubTitle(title) {
    this.y += 5;
    // this.doc.setFont("helvetica","bold");
    this.doc.setFontSize(12);
    this.doc.text(20, this.y, title);
    //this.doc.line(20, this.y+4, 190, this.y+4);
    this.y += 5;
    this.doc.setFontSize(10);
    this.checkNewPage();
  }

  private addSubTitleIfAnyAvailable(title, list) {
    let found = false;
    list.forEach(function(el) {
      if (el !== undefined && el !== "") {
        found = true;
      }
    })
    if (found) {
      this.y += 5;
      // this.doc.setFont("helvetica","bold");
      this.doc.setFontSize(12);
      this.doc.text(20, this.y, title);
      //this.doc.line(20, this.y+4, 190, this.y+4);
      this.y += 10;
      this.doc.setFontSize(10);
      this.checkNewPage();
    }
  }

  private addItem(x1, x2, what, content) {
    if (what !== undefined && what !== "" && content !== undefined && content != "") {
      this.doc.setFont("helvetica","bold");
      this.doc.text(x1, this.y, what)
      this.doc.setFont("helvetica","normal");
      this.doc.text(x2, this.y, "" + content)
      this.y += 8;
      this.checkNewPage();
    }
  }

  private addSingleItem(x, content) {
    if (content !== undefined && content != "") {
      this.doc.setFont("helvetica","normal");
      this.doc.text(x, this.y, "" + content)
      this.y += 8;
      this.checkNewPage();
    }
  }
  
  private addEventTrackPage() {
    this.newPage();
    this.addTitle("TRACCIA DEGLI EVENTI");
    this.doc.setFontSize(10)
    this.doc.setFont("helvetica","bold");
    this.doc.text(20, this.y, "Data")
    this.doc.text(45, this.y, "Ora")
    this.doc.text(60, this.y, "Luogo")
    this.doc.text(110, this.y, "Evento")
    this.doc.setFont("helvetica","normal");
    this.y += 10;
  }

  private addEventItem(date, time, place, what, deltay) {
    this.doc.text(20, this.y, date);
    this.doc.text(45, this.y, time);
    this.doc.text(60, this.y, place);
    this.doc.text(110, this.y, what)
    this.y += 8 + deltay
    this.checkNewPage();

  }

  private checkNewPage() {
    if (this.y > 260) {
      this.printFooter();
      this.doc.addPage();
      this.y = 20;
      this.nPages++;
    }
  }

  private newPage() {
    if (this.y > 20) {
      this.printFooter();
      this.doc.addPage();
      this.y = 20;
      this.nPages++;
    }
  }

  private printFooter() {
    this.doc.setFont("helvetica","normal");
    this.doc.text(20, 280, "Report: " + this.selectedReport._id);
    this.doc.text(20, 284, "Codice PS: " + this.selectedReport.traumaInfo.code);

    if (this.selectedReport.traumaInfo.sdo != undefined && this.selectedReport.traumaInfo.sdo !==''){
      this.doc.text(60, 284, "SDO: " + this.selectedReport.traumaInfo.sdo);
    }
    
    this.doc.text(100, 280, "Stampato il: " + this.date);
    if (this.secondPass) {
      this.doc.text(170, 280, "Pagina: " + this.nPages+"/"+ this.totalPages);      
    } else {
      this.doc.text(170, 280, "Pagina: " + this.nPages)      
    }
  }
  
  filterTime(timestamp: string): string {
    if (timestamp !== undefined) {
      return timestamp.substring(0, 5);
    } else {
      return 'undefined';
    }
  }
  
  
  filterTeamMembers( teamMembers: string[]): any {
        let deltay = 0;
        if ( teamMembers !== undefined ) {
            if ( teamMembers.length > 0 ) {
                let list = '';
                let line = '';
                for ( let i = 0; i < teamMembers.length - 1; i++ ) {
                    if (line.length + teamMembers[i].length > 50){
                      list = list + '\r\n';
                      line = '';
                      deltay += 4;
                    }
                    line += teamMembers[i] + ', ';
                    list += teamMembers[i] + ', '; 
                }
                list += teamMembers[teamMembers.length - 1];
                return { text: list, dy: deltay};
            } else {
                return { text: '', dy: 0};
            }
        } else {
            return { text: '', dy: 0};
        }
  }
  
}


