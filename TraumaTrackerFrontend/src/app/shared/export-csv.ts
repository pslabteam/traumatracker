
export class ExportReportLib {

    exportReport( report ) {
        // written javascript like, TODO refactoring
        let csvStr = 'EV_ID, DATE, TIME, PLACE, EVENT \n';
      
        for ( let i = 0; i < report.events.length; i++ ) {
            const event = report.events[i];
            let t = event.evType;
            if (t !== undefined && t != "photo" && t != "video" && t != "vocal-note" && t != "text-note") {
              csvStr += event.eventId + ',' +
                event.date + ',' +
                event.time + ',' +
                event.place + ',' +
                this.filterEventContent(event) + '\n';
          }
        }

        const blob = new Blob( [csvStr], { type: 'text/csv;charset=utf-8;' } );
        const filename = report._id + ".csv";

        if ( navigator.msSaveBlob ) { // IE 10+
            navigator.msSaveBlob( blob, filename );
        } else {
            const link = document.createElement( "a" );
            if ( link.download !== undefined ) { // feature detection
                // Browsers that support HTML5 download attribute
                const url = URL.createObjectURL( blob );
                link.setAttribute( "href", url );
                link.setAttribute( "download", filename );
                link.style.visibility = 'hidden';
                document.body.appendChild( link );
                link.click();
                document.body.removeChild( link );
            }
        }
    }
  
    checked(field): string {
      return field !== undefined ? field : '';
    }
    
    checkedb(field: boolean): string {
      return field !== undefined ? (field ? 'sì' : 'no') : '';
    }
  
    flatten(list): string {
      let s = '';
      for (let el in list) {
        s += list[el] + " ";
      }
      return s;
    }


    exportReports(dateFrom, dateTo, reportList ) {
      let csvStr = 
         'ID, OPID, OPDESC, DELACT,TEAM,START_DATE,START_TIME,END_DATE,END_TIME,DEST,'
      csvStr += 
          'ISS_TOT,' +
          'ISS_HEAD_TOT, ISS_H_NECK_AIS, ISS_H_BRAIN_AIS, ISS_H_CERV_SPINE_AIS,' + 
          'ISS_FACE_TOT, ISS_F_AIS,' +
          'ISS_TOR_TOT, ISS_T_AIS, ISS_T_SPINE_AIS,' +
          'ISS_ABD_TOT,ISS_A_AIS,ISS_A_LUMB_AIS,' + 
          'ISS_EXTR_TOT,ISS_E_UP_AIS,ISS_E_LO_AIS,ISS_E_PELV_AIS,'+
          'ISS_EXT_TOT,ISS_E_AIS,',
      csvStr += 
          'PAT_CODE, SDO, ER_DEC, ADM_CODE, NAME, SURNAME, GENDER, DOB, AGE, ACC_DATE, ADD_TIME, ADD_TYPE, VEHICLE,  FROM_OTHER_ER, OTHER_ER,';
      csvStr += 
          'MTC_DYN,MTC_PHYS,MTC_ANATOM,';
      csvStr += 
          'AN_ANTI_PLAT,AN_ANTI_CO,AN_NAO,';
      csvStr += 
          'TER_AREA,IS_CAR_ACC,A_VALUE,B_PLEU_DECO,C_BLOOD_DECO,C_TPOD,D_GCS_TOT,D_ANISOC,DMID,E_MOTI,WORST_BP,WORST_RR,';
      csvStr += 
          'TEMP,HR,BP,SPO2,SPO2,ETCO2,' + 
          'GCS_TOT,GCS_MOT,GCS_VERB,GCS_EYES,SEDATED,PUPILS,AIRWAY,POS_INHAL,INTUB_FAILED,CHEST_TUBE,OXYG_PERC,'+
          'HEMORR,LIMBS_FRACT,FRACT_EXPO,BURN\n';


        for ( let i = 0; i < reportList.length; i++ ) {
            const rep = reportList[i];            
            csvStr += 
              this.checked(rep._id) + ',' +
              this.checked(rep.startOperatorId) + ',' +
              this.checked(rep.startOperatorDescription) + ',';
            
            if (rep.delayedActivation !== undefined && rep.delayedActivation.isDelayedActivation == true){
              csvStr +=  'sì ';
              if (rep.delayedActivation.originalAccessDate !== undefined) {
                csvStr += 'data: '+rep.delayedActivation.originalAccessDate;
              }
              if (rep.delayedActivation.originalAccessTime !== undefined) {
                csvStr += 'ora: '+rep.delayedActivation.originalAccessTime;
              }
              csvStr +=  ', ';
            } else {
              csvStr += 'no, ';
            }
            
            csvStr +=
              this.checked(this.flatten(rep.traumaTeamMembers))  + ',' +
              this.checked(rep.startDate) + ',' +
              this.checked(rep.startTime) + ',' +
              this.checked(rep.endDate)  + ',' +
              this.checked(rep.endTime)  + ',' +
              this.checked(rep.finalDestination)+',';
          
          if (rep.iss !== undefined) {
              csvStr += this.checked(rep.iss.totalIss) + ',';

              const iss = rep.iss;

              if (iss.headGroup !== undefined) {
                csvStr += this.checked(iss.headGroup.groupTotalIss) +
                  ',' + this.checked(iss.headGroup.headNeckAis) +
                  ',' + this.checked(iss.headGroup.brainAis) +
                  ',' + this.checked(iss.headGroup.cervicalSpineAis) + ',';
              } else {
                csvStr += ',,,,';
              }

              if (iss.faceGroup !== undefined) {
                csvStr += this.checked(iss.faceGroup.groupTotalIss) +
                  ',' + this.checked(iss.faceGroup.faceAis) + ',';
              } else {
                csvStr += ',,';
              }

              if (iss.toraxGroup !== undefined) {
                csvStr += this.checked(iss.toraxGroup.groupTotalIss) +
                  ',' + this.checked(iss.toraxGroup.toraxAis) +
                  ',' + this.checked(iss.toraxGroup.spineAis) + ',';
              } else {
                csvStr += ',,,';
              }

              if (iss.abdomenGroup !== undefined) {
                csvStr += this.checked(iss.abdomenGroup.groupTotalIss) +
                  ',' + this.checked(iss.abdomenGroup.abdomenAis) +
                  ',' + this.checked(iss.abdomenGroup.lumbarSpineAis) + ',';
              } else {
                csvStr += ',,,';
              }

             if (iss.extremitiesGroup !== undefined) {
                csvStr += this.checked(iss.extremitiesGroup.groupTotalIss) +
                  ',' + this.checked(iss.extremitiesGroup.upperExtremitiesAis) +
                  ',' + this.checked(iss.extremitiesGroup.lowerExtremitiesAis) +
                  ',' + this.checked(iss.extremitiesGroup.pelvicGirdleAis) + ',';
              } else {
                csvStr += ',,,,';
              }

              if (iss.externaGroup !== undefined) {
                csvStr += this.checked(iss.externaGroup.groupTotalIss) +
                  ',' + this.checked(iss.externaGroup.externaAis) + ',';
              } else {
                csvStr += ',,';
              }

          } else {
              csvStr += ',,,,' + ',,' + ',,,' + ',,,' + ',,,,' + ',,';
          }

          if (rep.traumaInfo !== undefined){
               csvStr +=  
                 this.checked(rep.traumaInfo.code) +
                 ',' +this.checked(rep.traumaInfo.sdo) +
                 ',' +this.checkedb(rep.traumaInfo.erDeceased) +
                 ',' +this.checked(rep.traumaInfo.admissionCode) +
                 ',' +this.checked(rep.traumaInfo.name) +
                 ',' +this.checked(rep.traumaInfo.surname) +
                 ',' +this.checked(rep.traumaInfo.gender) +
                 ',' +this.checked(rep.traumaInfo.dob) +
                 ',' +this.checked(rep.traumaInfo.age) +
                 ',' +this.checked(rep.traumaInfo.accidentDate) +
                 ',' +this.checked(rep.traumaInfo.accidentTime) +
                 ',' +this.checked(rep.traumaInfo.accidentType) +
                 ',' +this.checked(rep.traumaInfo.vehicle) +
                 ',' +this.checkedb(rep.traumaInfo.fromOtherEmergency) +
                 ',' +this.checked(rep.traumaInfo.otherEmergency)+',';
          } else {
              csvStr += ',,,,,,,,,,,,,,,';
          }   
          
          if (rep.majorTraumaCriteria  !== undefined){
              csvStr +=  
                this.checkedb(rep.majorTraumaCriteria.dynamic) +
                ',' + this.checkedb(rep.majorTraumaCriteria.physiological) +
                ',' + this.checkedb(rep.majorTraumaCriteria.anatomical)+',';               
          } else {
              csvStr += ',,,';
          }
          
      
          if (rep.anamnesi  !== undefined){
              csvStr +=  
                this.checkedb(rep.anamnesi.antiplatelets) +
                ',' + this.checkedb(rep.anamnesi.anticoagulants) +
                ',' + this.checkedb(rep.anamnesi.nao)+',';               
          } else {
              csvStr += ',,,';
          }


          if (rep.preH !== undefined){
              csvStr +=  
                this.checked(rep.preH.territorialArea) + 
                ',' +  this.checkedb(rep.preH.isCarAccident) +
                ',' +  this.checked(rep.preH.aValue) + 
                ',' +  this.checkedb(rep.preH.bPleuralDecompression) + 
                ',' +  this.checkedb(rep.preH.cBloodProtocol) + 
                ',' +  this.checkedb(rep.preH.cTpod) + 
                ',' +  this.checked(rep.preH.dGcsTotal) + 
                ',' +  this.checkedb(rep.preH.dAnisocoria) + 
                ',' +  this.checkedb(rep.preH.dMidriasi) + 
                ',' +  this.checkedb(rep.preH.eMotility) +           
                ',' +  this.checked(rep.preH.worstBloodOPressure) +            
                ',' +  this.checked(rep.preH.worstRespiratoryRate) +',';            
          } else {
              csvStr += ',,,,,,,,,,,,';
          }

          if (rep.patientInitialCondition !== undefined && rep.patientInitialCondition.vitalSigns !== undefined) {
              csvStr += this.checked(rep.patientInitialCondition.vitalSigns.temp) +
                ',' +   this.checked(rep.patientInitialCondition.vitalSigns.hr) +
                ',' +   this.checked(rep.patientInitialCondition.vitalSigns.bp) +
                ',' +   this.checked(rep.patientInitialCondition.vitalSigns.spO2) +
                ',' +   this.checked(rep.patientInitialCondition.vitalSigns.etCO2);
          } else {
              csvStr +=
               ',,,,';
          }

          if (rep.patientInitialCondition !== undefined && rep.patientInitialCondition.clinicalPicture !== undefined) {
              csvStr +=  this.checked(rep.patientInitialCondition.clinicalPicture.gcsTotal) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.gcsMotor) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.gcsVerbal) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.gcsEyes) +
                ',' +  this.checkedb(rep.patientInitialCondition.clinicalPicture.sedated) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.pupils) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.airway) +
                ',' +  this.checkedb(rep.patientInitialCondition.clinicalPicture.positiveInhalation) +
                ',' +  this.checkedb(rep.patientInitialCondition.clinicalPicture.intubationFailed) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.chestTube) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.oxygenPercentage) +
                ',' +  this.checkedb(rep.patientInitialCondition.clinicalPicture.hemorrhage) +
                ',' +  this.checkedb(rep.patientInitialCondition.clinicalPicture.limbsFracture) +
                ',' +  this.checkedb(rep.patientInitialCondition.clinicalPicture.fractureExposition) +
                ',' +  this.checked(rep.patientInitialCondition.clinicalPicture.burn);
          } else {
              csvStr +=
               ',,,,,,,,,,,,,,';
          }

          csvStr += '\n';
        }

        const blob = new Blob( [csvStr], { type: 'text/csv;charset=utf-8;' } );
        const filename = "reports_"+dateFrom+"_"+dateTo + ".csv";

        if ( navigator.msSaveBlob ) { // IE 10+
            navigator.msSaveBlob( blob, filename );
        } else {
            const link = document.createElement( "a" );
            if ( link.download !== undefined ) { // feature detection
                // Browsers that support HTML5 download attribute
                const url = URL.createObjectURL( blob );
                link.setAttribute( "href", url );
                link.setAttribute( "download", filename );
                link.style.visibility = 'hidden';
                document.body.appendChild( link );
                link.click();
                document.body.removeChild( link );
            }
        }
    }
  

    private filterEventContent( event ) {
        // written javascript like, TODO refactor
        
        let eventType = event.evType;
        let eventContent = event.content;
      
        let msg = "";
        const isok = function( value ) {
            return value !== undefined;
        }

         switch ( eventType ) {
            case "drug":
                if ( eventContent.administrationType === "one-shot" ) {
                    return "Farmaco: " + eventContent.drugDescription + " " + eventContent.qty + eventContent.unit;
                } else {
                    if ( eventContent.event === "continuous-infusion" ) {
                        return "Inizio Farmaco ad infusione: " + eventContent.drugDescription + " " + eventContent.qty + eventContent.unit;
                    } else if ( eventContent.event === "variation" ) {
                        return "Variazione Farmaco ad infusione: " + eventContent.drugDescription + " " + eventContent.qty + eventContent.unit;
                    } else if ( eventContent.event === "stop" ) {
                        return "Fine Farmaco ad infusione: " + eventContent.drugDescription + " - durata: " + eventContent.duration + " s";
                    } else {
                        return  "Protocollo farmaco " + eventContent.drugDescription;
                    }
                }
           case "blood-product":
                if (eventContent.administrationType === "one-shot" ) {
                    let code = '';
                    if (eventContent.bloodProductId === 'emazies' || eventContent.bloodProductId === 'platelets' ||  eventContent.bloodProductId === 'fresh-frozen-plasma'){
                      code = ' (Codice sacca: ' + eventContent.bagCode + ')';
                   }
                    
                   return "Emoderivato: " + eventContent.bloodProductDescription + " " + eventContent.qty + eventContent.unit + code;
                } else {
                    return "Emoderivato: " + eventContent.bloodProductDescription;
                }
            
            case "procedure":
                if ( eventContent.procedureType === "one-shot" ) {
                    msg = "Manovra " + eventContent.procedureDescription;
                } else if ( eventContent.procedureType === "time-dependent" ) {
                    if ( eventContent.event === "start" ) {
                        msg = "Inizio Manovra " + eventContent.procedureDescription;
                    } else if ( eventContent.event === "end" ) {
                        msg = "Fine Manovra " + eventContent.procedureDescription;
                    } else {
                        msg = "Manovra " + eventContent.procedureDescription;
                    }
                }
                if ( eventContent.procedureId === "intubation" ) {
                    if ( isok( eventContent.difficultAirway ) ) {
                        msg = msg + " - vie aeree difficili: " + this.yesOrNo(eventContent.difficultAirway);
                    }
                    if ( isok( eventContent.inhalation ) ) {
                        msg = msg + " - inalazione: " + this.yesOrNo(eventContent.inhalation);
                    }
                    if ( isok( eventContent.videolaringo ) ) {
                        msg = msg + " - videolaringo: " + this.yesOrNo(eventContent.videolaringo);
                    }

                    if ( isok( eventContent.frova ) ) {
                        msg = msg + " - frova: " + this.yesOrNo(eventContent.frova);
                    }

                    return msg;

                } else if ( eventContent.procedureId === "drainage" ) {
                    if ( isok( eventContent.right ) ) {
                        msg = msg + " - destro: " + this.yesOrNo(eventContent.right);
                    }
                    if ( isok( eventContent.left ) ) {
                        msg = msg + " - sinistro: " + this.yesOrNo(eventContent.left);
                    }
                    return msg;
                } else if ( eventContent.procedureId === "chest-tube" ) {
                    if ( eventContent.right !== "" ) {
                        msg = msg + " - destro: " + this.yesOrNo(eventContent.right);
                    }
                    if ( isok( eventContent.left ) ) {
                        msg = msg + " - sinistro: " + this.yesOrNo(eventContent.left);
                    }
                    return msg;
                } else {
                    return msg;
                }
            case "diagnostic":
                msg = "Esame clinico " + eventContent.diagnosticDescription;
                if ( eventContent.diagnosticId === 'abg') {
                    if ( isok( eventContent.lactates ) ) {
                        msg = msg + " - lattati: " + eventContent.lactates;                    
                    } else  if ( isok( eventContent.be ) ) {
                        msg = msg + " - be: " + eventContent.be;                    
                    } else if ( isok( eventContent.ph ) ){
                        msg = msg + " - ph: " + eventContent.ph;                    
                    } else if ( isok( eventContent.hb ) ){
                        msg = msg + " - hb: " + eventContent.hb;                    
                  }
                } else if ( eventContent.diagnosticId === "rotem" ) {
                    if ( isok( eventContent.fibtem ) ) {
                        msg = msg + " - fibtem: " + eventContent.fibtem;
                    }
                    if ( isok( eventContent.extem ) ) {
                        msg = msg + " - extem: " + eventContent.extem;
                    }
                    if ( isok( eventContent.hyperfibrinolysis ) ) {
                        msg = msg + " - hyperfibrinolysis: " + this.yesOrNo(eventContent.hyperfibrinolysis);
                    }
                    return msg;
                } else {
                    return msg;
                }
            case "clinical-variation": {
              return  "Variazione parametro vitale: " + eventContent.vsDescription + " - nuovo valore: " + eventContent.value;
            }
            case "vital-signs-mon":
                return "Parametri vitali monitor: " +
                    "TEMP " + eventContent.Temp +
                    " HR " + eventContent.HR +
                    " DIA " + eventContent.DIA +
                    " SYS " + eventContent.SYS +
                    " SpO2 " + eventContent.SpO2 +
                    " etCO2 " + eventContent.EtCO2;
            case "trauma-leader":
                return "Cambio trauma-leader: " + eventContent.name + " " + eventContent.surname;
            /*
            case "photo":
                return "Foto";
            case "video":
                return "Video";
            case "vocal-note":
                return "Registrazione vocale";
            case "text-note":
                return "Nota testuale: " + eventContent.text;
             */
            case "als-start":
                return "Inizio ALS";
            case "als-stop":
                return "Fine ALS - durata: " + eventContent.duration + " s";
            case "room-in":
             if (event.place === 'PRE-H'){
                  return "Attivazione del Trauma Team";
             } else  if (event.place === 'Trasporto'){ 
                  return "Inizio trasporto paziente";
             } else {
                  return "Ingresso luogo: " + eventContent.place;
             }
            case "room-out":
             if (event.place === 'Trasporto'){ 
                  return "Fine trasporto paziente";
             } else {
                return "Uscita luogo: " + eventContent.place;
             }
            case "patient-accepted":
                return "Ingresso del paziente in PS.";
            case "report-reactivation":
                return "Ripristino report.";
            default:
                return eventContent;
                }
    }
  
    private yesOrNo(v: boolean): string {
      return (v ? 'sì' : 'no');
    }
  
     exportDefIndic( data ) {
        // written javascript like, TODO refactoring
        let csvStr =  'INDICATORE, ' +
                      'ANNO ' + data.refYear.year + ' (' + data.refYear.nReports + '), ' +
                      'ANNO ' + data.prevYear.year + ' (' + data.prevYear.nReports + ') \n';
       
        csvStr += 
           'Num totale traumi (report validi), ' +
           data.refYear.traumaISSIndic.nTotTrauma + ', ' +
           data.prevYear.traumaISSIndic.nTotTrauma + '\n';
        
        csvStr += 
           'Percentuale traumi con ISS > 14, ' +
           data.refYear.traumaISSIndic.issGreaterThan14Perc + ', ' +
           data.prevYear.traumaISSIndic.issGreaterThan14Perc + '\n';
        
        csvStr += 
           'Numero pazienti in shock emorragico (presunto) per cui tempo tra ingresso in shock room e damage control < 1h, ' +
           data.refYear.numShockEmoWithShortDamContrTime.nTrauma + ', ' +
           data.prevYear.numShockEmoWithShortDamContrTime.nTrauma + '\n';
        
        // =
       
        csvStr += 
           'Tempo (in minuti) alla prima manovra terapeutica in emergenza,,\n';
       
        csvStr += 
           ' - numero casi, ' +
           data.refYear.timingFirstERProcIndic.nValues + ', ' +
           data.prevYear.timingFirstERProcIndic.nValues + '\n';
       
        csvStr += 
           ' - media (in minuti): , ' +
           data.refYear.timingFirstERProcIndic.mean + ', ' +
           data.prevYear.timingFirstERProcIndic.mean + '\n';
        csvStr += 
           ' - σ , ' +
           data.refYear.timingFirstERProcIndic.standDev + ', ' +
           data.prevYear.timingFirstERProcIndic.standDev + '\n';        
        csvStr += 
           ' - mediana (in minuti): , ' +
           data.refYear.timingFirstERProcIndic.median + ', ' +
           data.prevYear.timingFirstERProcIndic.median + '\n';
        csvStr += 
           ' - iqr (in minuti): , ' +
           data.refYear.timingFirstERProcIndic.iqr + ', ' +
           data.prevYear.timingFirstERProcIndic.iqr + '\n';
      
      
        csvStr += 
           'Tempo (in minuti) tra ingresso shock room e tc,,\n';
       
        csvStr += 
           ' - numero casi, ' +
           data.refYear.timingShockRoomTACIndic.nValues + ', ' +
           data.prevYear.timingShockRoomTACIndic.nValues + '\n';
       
        csvStr += 
           ' - media (in minuti): , ' +
           data.refYear.timingShockRoomTACIndic.mean + ', ' +
           data.prevYear.timingShockRoomTACIndic.mean + '\n';
        csvStr += 
           ' - σ , ' +
           data.refYear.timingShockRoomTACIndic.standDev + ', ' +
           data.prevYear.timingShockRoomTACIndic.standDev + '\n';        
        csvStr += 
           ' - mediana (in minuti): , ' +
           data.refYear.timingShockRoomTACIndic.median + ', ' +
           data.prevYear.timingShockRoomTACIndic.median + '\n';
        csvStr += 
           ' - iqr (in minuti): , ' +
           data.refYear.timingShockRoomTACIndic.iqr + ', ' +
           data.prevYear.timingShockRoomTACIndic.iqr + '\n';
       
       // 
       
        csvStr += 
           'Tempo (in minuti) tra ingresso in shock room e damage control per traumi in shock emorragico,,\n';
       
        csvStr += 
           ' - numero casi, ' +
           data.refYear.timingShockRoomORforShockEmoIndic.nValues + ', ' +
           data.prevYear.timingShockRoomORforShockEmoIndic.nValues + '\n';
       
        csvStr += 
           ' - media (in minuti): , ' +
           data.refYear.timingShockRoomORforShockEmoIndic.mean + ', ' +
           data.prevYear.timingShockRoomORforShockEmoIndic.mean + '\n';
        csvStr += 
           ' - σ , ' +
           data.refYear.timingShockRoomORforShockEmoIndic.standDev + ', ' +
           data.prevYear.timingShockRoomORforShockEmoIndic.standDev + '\n';        
        csvStr += 
           ' - mediana (in minuti): , ' +
           data.refYear.timingShockRoomORforShockEmoIndic.median + ', ' +
           data.prevYear.timingShockRoomORforShockEmoIndic.median + '\n';
        csvStr += 
           ' - iqr (in minuti): , ' +
           data.refYear.timingShockRoomORforShockEmoIndic.iqr + ', ' +
           data.prevYear.timingShockRoomORforShockEmoIndic.iqr + '\n';
      
        //
        csvStr += 
           'Numero di pazienti con GCS < 9 e arrivati intubati, ' +
           data.refYear.nTraumaGCSLowAndIntubated.nTrauma + ', ' +
           data.prevYear.nTraumaGCSLowAndIntubated.nTrauma + '\n';
        
        csvStr += 
           'Numero attivazioni ritardate, ' +
           data.refYear.nDelayedActivations.nTrauma + ', ' +
           data.prevYear.nDelayedActivations.nTrauma + '\n';

       // 
       
        csvStr += 
           'Tempo (in minuti) tra ingresso in shock Room e tc per traumi con pupille anisocoriche e GCS,,\n';
       
        csvStr += 
           ' - numero casi, ' +
           data.refYear.timingShockRoomTacGCSPupilsIndic.nValues + ', ' +
           data.prevYear.timingShockRoomTacGCSPupilsIndic.nValues + '\n';
       
        csvStr += 
           ' - media (in minuti): , ' +
           data.refYear.timingShockRoomTacGCSPupilsIndic.mean + ', ' +
           data.prevYear.timingShockRoomTacGCSPupilsIndic.mean + '\n';
        csvStr += 
           ' - σ , ' +
           data.refYear.timingShockRoomTacGCSPupilsIndic.standDev + ', ' +
           data.prevYear.timingShockRoomTacGCSPupilsIndic.standDev + '\n';        
        csvStr += 
           ' - mediana (in minuti): , ' +
           data.refYear.timingShockRoomTacGCSPupilsIndic.median + ', ' +
           data.prevYear.timingShockRoomTacGCSPupilsIndic.median + '\n';
        csvStr += 
           ' - iqr (in minuti): , ' +
           data.refYear.timingShockRoomTacGCSPupilsIndic.iqr + ', ' +
           data.prevYear.timingShockRoomTacGCSPupilsIndic.iqr + '\n';

       //
       
       csvStr += 
           'Numero di pazienti con frattura pelvica con Pelvic binder, ' +
           data.refYear.nPelvicFractures.nTrauma + ', ' +
           data.prevYear.nPelvicFractures.nTrauma + '\n';

       // 
       
        csvStr += 
           'Tempo (in minuti) di permanenza in shock room,,\n';
              
        csvStr += 
           ' - media (in minuti): , ' +
           data.refYear.timingInShockRoomIndic.mean + ', ' +
           data.prevYear.timingInShockRoomIndic.mean + '\n';
        csvStr += 
           ' - σ , ' +
           data.refYear.timingInShockRoomIndic.standDev + ', ' +
           data.prevYear.timingInShockRoomIndic.standDev + '\n';        
        csvStr += 
           ' - mediana (in minuti): , ' +
           data.refYear.timingInShockRoomIndic.median + ', ' +
           data.prevYear.timingInShockRoomIndic.median + '\n';
        csvStr += 
           ' - iqr (in minuti): , ' +
           data.refYear.timingInShockRoomIndic.iqr + ', ' +
           data.prevYear.timingInShockRoomIndic.iqr + '\n';

       //
       
      csvStr += 
           'Numero di traumi in shock emorragico (presunto) che hanno effettuato tc prima di entrare in sala operatoria, ' +
           data.refYear.nShockEmoWithTACBeforeOR.nTrauma + ', ' +
           data.prevYear.nShockEmoWithTACBeforeOR.nTrauma + '\n';

       //
       
       
        csvStr += 
           'Tempo (in minuti) di permanenza in shock room per pazienti che entrano in sala operatoria,,\n';
       
        csvStr += 
           ' - numero casi, ' +
           data.refYear.timingInShockRoomIfOpRoomIndic.nValues + ', ' +
           data.prevYear.timingInShockRoomIfOpRoomIndic.nValues + '\n';
       
        csvStr += 
           ' - media (in minuti): , ' +
           data.refYear.timingInShockRoomIfOpRoomIndic.mean + ', ' +
           data.prevYear.timingInShockRoomIfOpRoomIndic.mean + '\n';
        csvStr += 
           ' - σ , ' +
           data.refYear.timingInShockRoomIfOpRoomIndic.standDev + ', ' +
           data.prevYear.timingInShockRoomIfOpRoomIndic.standDev + '\n';        
        csvStr += 
           ' - mediana (in minuti): , ' +
           data.refYear.timingInShockRoomIfOpRoomIndic.median + ', ' +
           data.prevYear.timingInShockRoomIfOpRoomIndic.median + '\n';
        csvStr += 
           ' - iqr (in minuti): , ' +
           data.refYear.timingInShockRoomIfOpRoomIndic.iqr + ', ' +
           data.prevYear.timingInShockRoomIfOpRoomIndic.iqr + '\n';
       
       //===
       
        const blob = new Blob( [csvStr], { type: 'text/csv;charset=utf-8;' } );
        const filename = "def-indicators.csv";

        if ( navigator.msSaveBlob ) { // IE 10+
            navigator.msSaveBlob( blob, filename );
        } else {
            const link = document.createElement( "a" );
            if ( link.download !== undefined ) { // feature detection
                // Browsers that support HTML5 download attribute
                const url = URL.createObjectURL( blob );
                link.setAttribute( "href", url );
                link.setAttribute( "download", filename );
                link.style.visibility = 'hidden';
                document.body.appendChild( link );
                link.click();
                document.body.removeChild( link );
            }
        }
    }

  
}

