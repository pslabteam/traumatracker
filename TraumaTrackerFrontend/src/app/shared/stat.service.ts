import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TraumaStatApiService } from '../shared/trauma-stat-api.service';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class StatService {

  constructor(
     private http: HttpClient,
     private traumaStatApi: TraumaStatApiService
) { }

  countRecsStat(dateFrom, dateTo) {
    const url = this.traumaStatApi.getCountRecsStatUrl();
    return this.http.get( url, 
          { params: { 
            'dateFrom': this.makeFlatDataFormat(dateFrom),
            'dateTo': this.makeFlatDataFormat(dateTo),
            }})
            .toPromise()
            .catch( err => {
              console.log('Exception ' + err);
              return this.handleError(err);
              });
  }

  getAggregateExport(dateFrom, dateTo, operatorId) {
    const url = this.traumaStatApi.getAggrExportUrl();
    // console.log( 'user data ' + userToAdd.name + '  ' + userToAdd.surname + '  ' + userToAdd.userId );
    return this.http.get( url, { 
            responseType: 'text',
            params: { 
            'dateFrom': this.makeFlatDataFormat(dateFrom),
            'dateTo': this.makeFlatDataFormat(dateTo),
            'operatorId': operatorId,
            'format': 'csv'
            }} )
            .toPromise()
            .catch( this.handleError );

  }
  
  getStatisticsTraumaFreq(dateFrom, dateTo) {
    const url = this.traumaStatApi.getTraumaFreqUrl();
    // console.log( 'user data ' + userToAdd.name + '  ' + userToAdd.surname + '  ' + userToAdd.userId );
    return this.http.get( url, {} )
            .toPromise()
            .catch( this.handleError );

  }
  
  getStatisticsAverageTraumaLen(): void {
    alert('not implemented');
    return;
  }
  
    private makeFlatDataFormat(date): string {
       if (date !== null && date !== undefined) {
          return date.year + '-' + (date.month > 9 ? date.month : '0' + date.month) + '-' + (date.day > 9 ? date.day : '0' + date.day);
      } else {
         return '';
       }
    }
  
    computeDefIndic(year, period) {
      const url = this.traumaStatApi.getDefIndicUri();
      return this.http.get( url,
          { params: { 
            'period': period,
            'year': year,
            }})
            .toPromise()
            .catch( err => {
              console.log('Exception ' + err);
              return this.handleError(err);
              });
    }
  
    computeIndic( dateFrom, dateTo, comparisonWithPrevYear, period, indicators, issConstraint) {
      const url = this.traumaStatApi.getIndicUri();
      return this.http.get( url,
          { params: { 
            'dateFrom': this.makeFlatDataFormat(dateFrom),
            'dateTo': this.makeFlatDataFormat(dateTo),
            'period': period,
            'comparisonWithPrevYear': comparisonWithPrevYear,
            'indicators': indicators,
            'issConstraint': issConstraint
            }})
            .toPromise()
            .catch( err => {
              console.log('Exception ' + err);
              return this.handleError(err);
              });
  }
  
  
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    alert('An error occurred');
    return Promise.reject(error.message || error);
  }
}
