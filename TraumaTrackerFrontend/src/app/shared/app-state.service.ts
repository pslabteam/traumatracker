import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { CanActivate, Router } from '@angular/router';


@Injectable()
export class AppStateService implements CanActivate {

     private loggedIn: boolean;
     private userInfo;
     private currentSelectedReport;

    private appEventChannel = new Subject<any>();
    public appEventChannel$ = this.appEventChannel.asObservable();

    constructor(
      private router: Router,
    ) {
      this.loggedIn = false;
    }

    notifyReportSelected( report ) {
      this.currentSelectedReport = report;
      this.appEventChannel.next({ eventType: 'report-selected', report: report});
    }

    notifyReportUnselected() {
      this.currentSelectedReport = {};
      this.appEventChannel.next({ eventType: 'report-unselected' });
    }

    notifyChangedTab() {
      this.appEventChannel.next({ eventType: 'changed-tab'});
    }

    notifyRepToDelete(rep) {
      this.appEventChannel.next({ eventType: 'report-to-delete', report: rep});
    }

    notifyRepToChart(rep) {
      this.appEventChannel.next({ eventType: 'report-to-chart', report: rep});
    }

    setLoggedIn(userInfo) {
      this.loggedIn = true;
      this.userInfo = userInfo;
      this.appEventChannel.next({ eventType: 'user-logged-in', user: userInfo });
    }

    getLoggedUserInfo() {
      return this.userInfo;
    }

    logout() {
       this.setLoggedOut();
       this.router.navigate(['/login']);
    }

    setLoggedOut() {
      this.loggedIn = false;
      this.appEventChannel.next({ eventType: 'user-logged-out', user: this.userInfo });
      this.userInfo = null;
    }

    canActivate(): boolean {
      if (!this.loggedIn) {
        this.router.navigate(['login']);
        return false;
      }
      return true;
    }

    getUserInfo() {
       return this.userInfo;
    }

    canUserAdmin(): boolean {
      return this.userInfo !== undefined && this.userInfo !== null && this.userInfo.isAdminRole();
    }

    canUserManageUsers(): boolean {
      return this.userInfo !== undefined && this.userInfo !== null &&  this.canUserAdmin() || this.userInfo.isDirectorRole();
    }
    
    canUserManageData(): boolean {
      return this.userInfo !== undefined && this.userInfo !== null &&  this.userInfo.canManageData();
    }
}
