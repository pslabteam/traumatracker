import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { TraumaTrackerAPIService } from '../shared/trauma-tracker-api.service';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

    constructor(
        private http: HttpClient,
        private traumaTrackerApi: TraumaTrackerAPIService
    ) { }

    getAllUsers() : Promise<any>{
        const url = this.traumaTrackerApi.getUsersURI();
        return this.http.get<JSON>( url )
            .toPromise()
            .then( this.extractData )
            .catch( this.handleError );
    }

    addUser( userToAdd ) {
        const url = this.traumaTrackerApi.getUsersURI();
        return this.http.post( url, userToAdd )
            .toPromise()
            .then( this.extractData )
            .catch( this.handleError );
    }
  
    updateUser( userToUpdate ) {
        const url = this.traumaTrackerApi.getUsersURI();
        return this.http.put( url + '/' + userToUpdate.userId, userToUpdate )
            .toPromise()
            .catch( this.handleError );
    }

    deleteUser(userId) {
        const url = this.traumaTrackerApi.getUsersURI();
        return this.http.delete( url + '/' + userId)
            .toPromise()
            .catch( this.handleError );
    }
    
    deleteAllUsers() {
        const url = this.traumaTrackerApi.getUsersURI();
        return this.http.delete( url )
            .toPromise()
            .then( this.extractData )
            .catch( this.handleError );
    }

    private extractData( res ): string {
        return res;
    }

    private handleError( error: any ): Promise<any> {
        console.error( 'An error occurred', error );
        return Promise.reject( error.message || error );
    }
}