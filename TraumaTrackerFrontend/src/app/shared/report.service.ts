import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TraumaTrackerAPIService } from '../shared/trauma-tracker-api.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

import { PrintReportLib } from './print-pdf';
import { ExportReportLib } from './export-csv';

import { Report, ISS, VitalSigns, ClinicalPicture } from '../data-model/report';
import 'rxjs/add/operator/toPromise';
import {JsonConvert, OperationMode, ValueCheckingMode} from 'json2typescript';
import { AppStateService } from '../shared/app-state.service';


@Injectable()
export class ReportService {

    private recentReports: Object[] = undefined;

    private timerDelay = 0; // sets the timer initial delay (setting it to 0 makes the refresh happen immediatly)
    private reportRefreshInterval = 1000; // 10 seconds refresh interval
    private timer: Observable<number>;
    private timerSub: Subscription;
    private reportRefreshSubscriber: Subscription;

    private recentReportsUpdatedChannel = new Subject<any>();
    public recentReportsUpdatedChannel$ = this.recentReportsUpdatedChannel.asObservable();

    private exportLib: ExportReportLib = new ExportReportLib();
    private printLib: PrintReportLib;
    private jsonConvert: JsonConvert = new JsonConvert();

    constructor(
        private http: HttpClient,
        private traumaTrackerAPI: TraumaTrackerAPIService,
        private appStateService: AppStateService,

    ) {
        this.printLib = new PrintReportLib(traumaTrackerAPI);
        this.jsonConvert.operationMode = OperationMode.ENABLE;        
        this.jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
        // jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
        // jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
      
        this.timer = TimerObservable.create( this.timerDelay, this.reportRefreshInterval );      
      /*
        this.timer = TimerObservable.create( this.timerDelay, this.reportRefreshInterval );
        this.timerSub = this.timer.subscribe(() => {
            this.getReports();
        }); */
    }

    startFetchingRecentReports() {
        this.timerSub = this.timer.subscribe(() => {
            this.getReports();
        });
    }
  
    stopFetchingRecentReports() {
      this.timerSub.unsubscribe();
    }
  
    makeReportObjectFromJson(report): any {
      if (Object.keys(report).indexOf('_version') !== -1) {
        // console.log("Versioned report - v. "+reportm.get("_version"));
        return this.jsonConvert.deserializeObject(report, Report);
      } else {
        // console.log("Unversioned report");
        return this.processUnversionedReport(report);
      }
    }

    getReports(): void {
        const numReports = 10;
        this.getMostRecentReports( numReports ).then( reports => {
          this.recentReports = new Array<Object>();
          for (const report of reports) {
            // console.log(report);
            try {
              this.recentReports.push(this.makeReportObjectFromJson(report));
            } catch (e) {
              console.log('Exception ' + e);
            }
          }
          this.recentReportsUpdatedChannel.next(this.recentReports) ;
        });
    }

    getMostRecentReports( numReports: number ): Promise<any> {
        let userInfo = this.appStateService.getUserInfo();
        const url = this.traumaTrackerAPI.getReportsURI();
        if (userInfo.role === 'member') {
           return this.http.get( url, 
            { 
              params: { 'mostRecent': numReports.toString(), 'userId': userInfo.userId, 'opId': userInfo.userId } ,
              responseType: 'json'
            }).toPromise()
              .catch( this.handleError );

        } else {
           return this.http.get( url, 
            { 
              params: { 'mostRecent': numReports.toString(), 'userId': userInfo.userId } ,
              responseType: 'json'
            }).toPromise()
              .catch( this.handleError );
        }
    }

    viewReportDetails( reportId ): Promise<JSON> {
        const url = this.traumaTrackerAPI.getReportsURI();
        return this.http.get( url + '/' + reportId, { responseType: 'json' } )
          .toPromise()
          .catch( this.handleError );
    }

    private makeFlatDataFormat(date): string {
       if (date !== null && date !== undefined) {
          return date.year + '-' + (date.month > 9 ? date.month : '0' + date.month) + '-' + (date.day > 9 ? date.day : '0' + date.day);
      } else {
         return '';
       }
    }
  
    findReports( dateFrom, dateTo, operatorId, iss ): Promise<JSON[]> {
        let userInfo = this.appStateService.getUserInfo();
        const url = this.traumaTrackerAPI.getReportsURI();
        let args;
        if (userInfo.role === 'member') {
           args = {
            'dateFrom': this.makeFlatDataFormat(dateFrom),
            'dateTo': this.makeFlatDataFormat(dateTo),
            'opId': userInfo.userId,
            'iss': iss !== undefined && iss !== null ? iss : '',
            'userId': userInfo.userId
            };
        } else {
           args = {
            'dateFrom': this.makeFlatDataFormat(dateFrom),
            'dateTo': this.makeFlatDataFormat(dateTo),
            'opId': operatorId !== undefined && operatorId !== null ? operatorId : '',
            'iss': iss !== undefined && iss !== null ? iss : '',
            'userId': userInfo.userId
            };
        }
        return this.http.get( url,
          { params: args, responseType: 'json' })
            .toPromise()
            .then(reps => {
              const repObjs = new Array<Object>();
              for (const report of (reps as Object[])) {
                try {
                  repObjs.push(this.makeReportObjectFromJson(report));
                } catch (e) {
                  console.log('Exception ' + e);
                }
              }
              return repObjs;
            })
            .catch( err => {
              console.log('Exception ' + err);
              return this.handleError(err); 
            });
    }

    getReport(reportId: string): Promise<Report> {
        const url = this.traumaTrackerAPI.getReportsURI() + '/' + reportId;
        return this.http.get( url , 
          { 
            responseType: 'json' 
          }).toPromise()
            .then( rep => {
              return this.makeReportObjectFromJson(rep);
          })
            .catch( this.handleError );
      
    }

    validateReport( reportId: string, opId: string ): Promise<Report>  {
        const url = this.traumaTrackerAPI.getReportsURI() + '/' + reportId + '/validation';
        const info = {
          'operatorId': opId,
        };
        return this.http.put<JSON>( url, info)
          .toPromise()
          .then(onResolve => {
            return this.getReport(reportId);
          }, onReject => {
            return Promise.reject('validation failed');
          });
    }

      updateISS( reportId: string, iss: ISS): Promise<Report>  {
        const url = this.traumaTrackerAPI.getReportsURI() + '/' + reportId + '/iss';
        return this.http.put<JSON>( url, iss)
          .toPromise()
          .then(onResolve => {
            return this.getReport(reportId);
          }, onReject => {
            return Promise.reject('iss update failed');
          });
    }

      updateVitalSigns( reportId: string, vs: VitalSigns): Promise<Report>  {
        const url = this.traumaTrackerAPI.getReportsURI() + '/' + reportId + '/vitalsigns';
        return this.http.put( url, vs)
          .toPromise()
          .then( onResolve => {
            return this.getReport(reportId);
          }, onReject => {
            return Promise.reject('vital signs update failed');
          });
    }

      updateClinicalPicture( reportId: string, cp: ClinicalPicture): Promise<Report>  {
        const url = this.traumaTrackerAPI.getReportsURI() + '/' + reportId + '/clinicalpicture';
        return this.http.put( url, cp)
          .toPromise()
          .then( onResolve => {
            return this.getReport(reportId);
          }, onReject => {
            return Promise.reject('clinical picture update failed');
          });
    }

    deleteReport( reportId: string ) {
        const url = this.traumaTrackerAPI.getReportsURI();
        return this.http.delete( url + '/' + reportId )
            .toPromise()
            .then( res => JSON.parse(res.toString()))
            .catch( this.handleError );
    }    
    
    deleteAllReports() {
        const url = this.traumaTrackerAPI.getReportsURI();
        return this.http.delete( url )
            .toPromise()
            .then( res => JSON.parse(res.toString()))
            .catch( this.handleError );
    }
    
    /*
    private extractData( res: Response ): any {
        return res.json(); //res.text() res.json().data as string
    }

    private extractText( res: Response ): string {
        return res.text();
    }
    */
    private handleError( error: any ): Promise<any> {
        console.error( 'An error occurred', error );
        return Promise.reject( error.message || error );
    }
  
    printReport(rep, withMultimedia: boolean){
      this.printLib.printReport(rep, withMultimedia);
    }
    
    exportReport(rep){
      this.exportLib.exportReport(rep);
    }
    
    processUnversionedReport(report) {
      for (let event of report.events){
        event.evType = event.type;
      } 
      return report;
    }
    
}