import { Injectable } from '@angular/core';

@Injectable()
export class TraumaStatApiService {

    private host = 'http://' + location.hostname + ':8084';
    private versionUrl = '/gt2/traumastat/api/version';
    private traumaFreqUrl = '/gt2/traumastat/api/freq';
    private traumaLenUrl = '/gt2/traumastat/api/len';
    private countRecsUrl = '/gt2/traumastat/api/count';
    private traumaAggrExportUrl = '/gt2/traumastat/api/export-aggr';
    private defIndicUri = '/gt2/traumastat/api/def-indicators';
    private indicUri = '/gt2/traumastat/api/indicators';

    getVersionUrl(): string {
        return this.host + this.versionUrl;
    }

    getCountRecsStatUrl(): string {
        return this.host + this.countRecsUrl;
    }

    getTraumaFreqUrl(): string {
        return this.host + this.traumaFreqUrl;
    }
    getTraumaLenUrl(): string {
        return this.host + this.traumaLenUrl;
    }

    getAggrExportUrl(): string {
        return this.host + this.traumaAggrExportUrl;
    }
  
    getDefIndicUri(): string {
        return this.host + this.defIndicUri;
    }
    
    getIndicUri(): string {
        return this.host + this.indicUri;
    }

}
