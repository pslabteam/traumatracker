import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TraumaTrackerAPIService } from '../shared/trauma-tracker-api.service';
import { AppStateService } from '../shared/app-state.service';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService {

  
    constructor(
        private http: HttpClient,
        private traumaTrackerApi: TraumaTrackerAPIService,
        private appStateService: AppStateService
    ) {
    }

    login(user, pwd) {
        const url = this.traumaTrackerApi.getLoginURI();
        return this.http.post( url, {
            userId: user,
            pwd: pwd
        }).toPromise()
            .catch( this.handleError );
    }
    
    recoverPwd(user) {
        const url = this.traumaTrackerApi.getPwdRecoveryURI();
        return this.http.post( url, {
            userId: user  
        }).toPromise()
          .catch( this.handleError );
    }

    private handleError( error: any ): Promise<any> {
        console.error( 'An error occurred', error );
        return Promise.reject( error.message || error );
    }


}