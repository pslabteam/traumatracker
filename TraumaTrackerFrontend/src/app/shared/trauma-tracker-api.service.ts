import { Injectable } from '@angular/core';

@Injectable()
export class TraumaTrackerAPIService {

    private host = 'http://' + location.hostname + ':8080';
    private versionURI = '/gt2/traumatracker/api/version';
    private reportsURI = '/gt2/traumatracker/api/reports';
    private usersURI = '/gt2/traumatracker/api/users';
    private loginURI = '/gt2/traumatracker/api/login';
    private pwdRecoveryURI = '/gt2/traumatracker/api/login/pwd-recovery';
    private traumafreqURI = '/gt2/traumatracker/api/stats/traumafreq';
    private traumalenURI = '/gt2/traumatracker/api/stats/traumalen';
    private audioURI = '/gt2/traumatracker/audio';
    private videoURI = '/gt2/traumatracker/video';
    private photoURI = '/gt2/traumatracker/photo';

    getVersionURI(): string {
        return this.host + this.versionURI;
    }

    getLoginURI(): string {
        return this.host + this.loginURI;
    }

    getPwdRecoveryURI(): string {
        return this.host + this.pwdRecoveryURI;
    }

    getReportsURI(): string {
        return this.host + this.reportsURI;
    }

    getUsersURI(): string {
        return this.host + this.usersURI;
    }
    getAudioURI(): string {
        return this.host + this.audioURI;
    }

    getVideoURI(): string {
        return this.host + this.videoURI;
    }

    getPhotoURI(): string {
        return this.host + this.photoURI;
    }

    getPhotoDownloadURI(): string {
        return this.host + '/gt2/traumatracker/static/photo';
    }

    getVideoDownloadURI(): string {
        return this.host + '/gt2/traumatracker/static/video';
    }

    getAudioDownloadURI(): string {
        return this.host + '/gt2/traumatracker/static/audio';
    }

}
