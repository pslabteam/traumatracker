import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { Report, Anamnesi } from '../../data-model/report';

@Component( {
    selector: 'anamnesi',
    templateUrl: './anamnesi.component.html',
} )
export class AnamnesiComponent {
    @Input() selectedReport: Report;
}

@Pipe( { name: 'isAnamnesyNotEmpty' } )
export class IsAnamnesyNotEmptyPipe implements PipeTransform {
    transform( an ): boolean {
      return (an !== undefined) && (an.antiplatelets !== undefined ||
              an.anticoagulants !== undefined || an.nao !== undefined); 
    }
}
