import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { Report, PreH } from '../../data-model/report';

@Component( {
    selector: 'pre-h',
    templateUrl: './pre-h.component.html',
} )
export class PreHComponent {
    @Input() selectedReport;
}

@Pipe( { name: 'isPreHNotEmpty' } )
export class IsPreHNotEmptyPipe implements PipeTransform {
    transform( preh: PreH ): boolean {
      return preh !== undefined && (
            (preh.territorialArea !== undefined)  ||
            (preh.isCarAccident !== undefined)  ||
            (preh.aAirways !== undefined && preh.aAirways !== '') ||
            (preh.bPleuralDecompression !== undefined ) ||
            (preh.cBloodProtocol !== undefined ) ||
            (preh.cTpod !== undefined ) ||
            (preh.dGcsTotal !== undefined ) ||
            (preh.dAnisocoria !== undefined ) ||
            (preh.dMidriasi !== undefined ) ||
            (preh.eMotility !== undefined ) ||
            (preh.worstBloodPressure !== undefined ) ||
            (preh.worstRespiratoryRate !== undefined));
    }
}
