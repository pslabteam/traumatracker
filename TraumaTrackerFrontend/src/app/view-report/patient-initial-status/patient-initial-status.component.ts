import { Component, Input, ViewChild } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Report, PatientInitialCondition, VitalSigns, ClinicalPicture } from '../../data-model/report';
import { AppStateService } from '../../shared/app-state.service';
import { ReportService } from '../../shared/report.service';

@Component( {
    selector: 'patient-initial-status',
    templateUrl: './patient-initial-status.component.html',
} )
export class PatientInitialStatusComponent {
    @Input() selectedReport;
    private dialog: NgbModalRef;
    @ViewChild('updateClinicalPictureDialog') private updateClinicalPictureDialog;
    @ViewChild('updateVitalSignsDialog') private updateVitalSignsDialog;

    box_vs_temp_selected: string;
    box_vs_temp_options = ["Normotermico", "Ipotermico", "Ipertermico"];
    box_vs_hr_selected: string;
    box_vs_hr_options = ["Normale", "Bradicardico", "Tachicardico"];
    box_vs_bp_selected: string;
    box_vs_bp_options = ["Normale", "Ipoteso", "Iperteso"];
    box_vs_spo2_selected: string;
    box_vs_spo2_options = ["Normale", "Ipossico"];
    box_vs_etco2_selected: string;
    box_vs_etco2_options = ["Normale", "Ipocapnico", "Ipercapnico"];
    
    box_cp_gcsMotor_selected: string;
    box_cp_gcsMotor_options = ['(6) Esegue', '(5) Localizza', '(4) Retrae', '(3) Flessione', '(2) Estende', '(1) Nulla'];
    box_cp_gcsVerbal_selected: string;
    box_cp_gcsVerbal_options = ['(5) Orienta', '(4) Confusa', '(3) Inappropriate', '(2) Incomprensibile', '(1) Nulla'];
    box_cp_gcsEyes_selected: string;
    box_cp_gcsEyes_options = ['(4) Spontanea', '(3) Chiamata', '(2) Dolore', '(1) Nulla'];
    box_cp_sedated_selected: string;
    box_cp_sedated_options = ['Sì', 'No'];
    box_cp_pupils_selected: string;
    box_cp_pupils_options = ['Normale', 'Aniso', 'Midriasi'];
  
    box_cp_airways_selected: string;
    box_cp_airways_options = ["Respiro Spontaneo", "Presidio Sovraglottico", "Tubo Tracheale"];
  
    box_cp_oxygenPercentage: number;
  
    box_cp_positiveInhalation_selected: string;
    box_cp_positiveInhalation_options = ['Sì', 'No'];
    
    box_cp_intubationFailed_selected: string;
    box_cp_intubationFailed_options = ['Sì', 'No'];
    
    box_cp_chestTube_selected: string;
    box_cp_chestTube_options = ["Non Presente", "Destra", "Sinistra", "Bilaterale"];
     
    box_cp_hemorrhage_selected: string;
    box_cp_hemorrhage_options = ['Sì', 'No'];
    
    box_cp_burn_selected: string;
    box_cp_burn_options = ["Non Presente", "Termica", "Chimica"];
    
    box_cp_limbsFracture_selected: string;
    box_cp_limbsFracture_options = ['Sì', 'No'];

    box_cp_fractureExposition_selected: string;
    box_cp_fractureExposition_options = ['Sì', 'No'];

    
    constructor(
        private modalService: NgbModal,
        private reportService: ReportService,
        private appStateService: AppStateService
    ) {}
    
     updateVitalSigns() {
      let rep: Report = this.selectedReport;
      this.box_vs_temp_selected = rep.patientInitialCondition.vitalSigns.temp;
      this.box_vs_hr_selected = rep.patientInitialCondition.vitalSigns.hr;
      this.box_vs_bp_selected = rep.patientInitialCondition.vitalSigns.bp; 
      this.box_vs_etco2_selected = rep.patientInitialCondition.vitalSigns.etco2;
      this.box_vs_spo2_selected = rep.patientInitialCondition.vitalSigns.spo2;      
      this.dialog = this.modalService.open(this.updateVitalSignsDialog, {
        backdrop: 'static',
        keyboard: false
    });
    }

    updateClinicalPicture() {
      let rep: Report = this.selectedReport;
      this.box_cp_gcsMotor_selected = rep.patientInitialCondition.clinicalPicture.gcsMotor || '';
      this.box_cp_gcsVerbal_selected = rep.patientInitialCondition.clinicalPicture.gcsVerbal || '';
      this.box_cp_gcsEyes_selected = rep.patientInitialCondition.clinicalPicture.gcsEyes || '';
      this.box_cp_sedated_selected = rep.patientInitialCondition.clinicalPicture.sedated ? 'Sì' : 'No';
      this.box_cp_pupils_selected = rep.patientInitialCondition.clinicalPicture.pupils || '';
      this.box_cp_airways_selected = rep.patientInitialCondition.clinicalPicture.airway || '';
      this.box_cp_positiveInhalation_selected = rep.patientInitialCondition.clinicalPicture.positiveInhalation ? 'Sì' : 'No';
      this.box_cp_intubationFailed_selected = rep.patientInitialCondition.clinicalPicture.intubationFailed ? 'Sì' : 'No';     
      this.box_cp_chestTube_selected = rep.patientInitialCondition.clinicalPicture.chestTube || '';
      this.box_cp_oxygenPercentage = rep.patientInitialCondition.clinicalPicture.oxygenPercentage;

      this.box_cp_hemorrhage_selected = rep.patientInitialCondition.clinicalPicture.hemorrhage ? 'Sì' : 'No';
      this.box_cp_limbsFracture_selected = rep.patientInitialCondition.clinicalPicture.limbsFracture ? 'Sì' : 'No';
      this.box_cp_fractureExposition_selected = rep.patientInitialCondition.clinicalPicture.fractureExposition ? 'Sì' : 'No';
      this.box_cp_burn_selected = rep.patientInitialCondition.clinicalPicture.burn || '';
      
      this.dialog = this.modalService.open(this.updateClinicalPictureDialog, {
        backdrop: 'static',
        keyboard: false
    });
    }

    dismissDialog() {
      this.dialog.close();
    }

    confirmUpdateClinicalPicture() {
      const cp: ClinicalPicture = this.selectedReport.patientInitialCondition.clinicalPicture;
      cp.airway = this.box_cp_airways_selected;
      cp.burn = this.box_cp_burn_selected;
      cp.chestTube = this.box_cp_chestTube_selected;
      cp.fractureExposition = this.box_cp_fractureExposition_selected === 'Sì';
      cp.gcsEyes = this.box_cp_gcsEyes_selected;
      cp.gcsMotor = this.box_cp_gcsMotor_selected;
      cp.gcsVerbal = this.box_cp_gcsVerbal_selected;
      cp.computeGCSTotal();
      cp.hemorrhage = this.box_cp_hemorrhage_selected === 'Sì';
      cp.intubationFailed = this.box_cp_intubationFailed_selected === 'Sì';
      cp.limbsFracture = this.box_cp_limbsFracture_selected === 'Sì';
      cp.oxygenPercentage = this.box_cp_oxygenPercentage;
      this.reportService.updateClinicalPicture( this.selectedReport._id, cp)
        .then(rep => {
          this.appStateService.notifyReportSelected( rep );
          this.dialog.close();
        })
        .catch(error => {
          console.log('Error in validating the report (clinical picture)');
        });
    }

    confirmUpdateVitalSigns() {
      const vs: VitalSigns = this.selectedReport.patientInitialCondition.vitalSigns;
      vs.temp = this.box_vs_temp_selected;
      vs.hr = this.box_vs_hr_selected;
      vs.bp = this.box_vs_bp_selected;
      vs.spo2 = this.box_vs_spo2_selected;
      vs.etco2 = this.box_vs_etco2_selected;
      this.reportService.updateVitalSigns( this.selectedReport._id, vs)
        .then(rep => {
          this.appStateService.notifyReportSelected( rep );
          this.dialog.close();
        })
        .catch(error => {
          console.log('Error in validating the report (vital signs)');
        });
    }
}

@Pipe( { name: 'getFirstAvailable' } )
export class GetFirsAvailablePipe implements PipeTransform {
    transform( first, second ): string {
        return first === "" || first === undefined ? second : first;
    }
}

function isValid(value: string): boolean {
   return value !== undefined && value !== '';
}

@Pipe( { name: 'isPatientInitialConditionNotEmpty' } )
export class IsPatientInitialConditionNotEmpty implements PipeTransform {
    transform( obj: Report): boolean {
        return obj !== undefined && obj.patientInitialCondition !== undefined;
   }
}

@Pipe( { name: 'isVitalParamsNotEmpty' } )
export class IsVitalParamsNotEmptyPipe implements PipeTransform {
    transform( obj: PatientInitialCondition): boolean {
        return obj.vitalSigns !== undefined &&
        (isValid(obj.vitalSigns.temp) || isValid(obj.vitalSigns.bp) || 
          isValid(obj.vitalSigns.etco2) || isValid(obj.vitalSigns.hr) || isValid(obj.vitalSigns.spo2));
  }
}

@Pipe( { name: 'isClinicalPictureNotEmpty' } )
export class IsClinicalPictureNotEmpty implements PipeTransform {
    transform( obj: PatientInitialCondition): boolean {
        return obj.clinicalPicture !== undefined;
    }
}

@Pipe( { name: 'isNeuroNotEmpty' } )
export class IsNeuroNotEmptyPipe implements PipeTransform {
    transform( obj: PatientInitialCondition): boolean {
        return obj !== undefined && obj.clinicalPicture !== undefined && 
              (obj.clinicalPicture.gcsTotal !== undefined) ||
              isValid(obj.clinicalPicture.gcsMotor) || isValid(obj.clinicalPicture.gcsVerbal) || isValid(obj.clinicalPicture.gcsEyes) ||
              obj.clinicalPicture.sedated !== undefined || isValid(obj.clinicalPicture.pupils);
  }
}

@Pipe( { name: 'isAirwayNotEmpty' } )
export class IsAirwayNotEmptyPipe implements PipeTransform {
    transform( obj: PatientInitialCondition ): boolean {
        return obj !== undefined && obj.clinicalPicture !== undefined && 
              (isValid(obj.clinicalPicture.airway) || (obj.clinicalPicture.positiveInhalation !== undefined) || 
               (obj.clinicalPicture.intubationFailed !== undefined) ||
               isValid(obj.clinicalPicture.chestTube) || (obj.clinicalPicture.oxygenPercentage !== undefined));
    }
}

@Pipe( { name: 'isHemorrhageNotEmpty' } )
export class IsHemorrhageNotEmpty implements PipeTransform {
    transform( obj: PatientInitialCondition): boolean {
        return (obj !== undefined && obj.clinicalPicture.hemorrhage !== undefined);
    }
}

@Pipe( { name: 'isOrtopNotEmpty' } )
export class IsOrtopNotEmptyPipe implements PipeTransform {
    transform( obj: PatientInitialCondition): boolean {
        return (obj !== undefined) && (obj.clinicalPicture !== undefined) &&  
          (obj.clinicalPicture.limbsFracture !== undefined) && (obj.clinicalPicture.fractureExposition !== undefined);
  }
}

@Pipe( { name: 'isBurnNotEmpty' } )
export class IsBurnNotEmptyPipe implements PipeTransform {
  transform( obj: PatientInitialCondition): boolean {
    return (obj !== undefined) && (obj.clinicalPicture.burn !== undefined);
  }
}



