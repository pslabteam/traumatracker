import { Component, Input, ViewChild } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { ISS } from '../../data-model/report'
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AppStateService } from '../../shared/app-state.service';
import { ReportService } from '../../shared/report.service';
import { Report } from '../../data-model/report'

@Component( {
    selector: 'general-info',
    templateUrl: './general-info.component.html',
    styles: [`
    .view-modal .modal-content {
      position: fixed;
      width: 600;
      height: 450;
      margin-top: 10%;
      }
    iss-tab {
      background-color:#bbbbbb;
    }
    iss-tr {
      height: 20px;
    }
      `
    ]
})
export class GeneralInfoComponent {
    @Input() selectedReport;
    private dialog: NgbModalRef;
    @ViewChild('issDetailsDialog') private issDetailsDialog;
    @ViewChild('issUpdateDialog') private issUpdateDialog;

    /* input form about ISS */
    private box_headGroup_headNeckAis;
    private box_headGroup_brainAis;
    private box_headGroup_cervicalSpineAis;
    private box_headGroup_faceAis;
    private box_faceGroup_faceAis;
    private box_toraxGroup_toraxAis;
    private box_toraxGroup_spineAis;
    private box_abdomenGroup_abdomenAis;
    private box_abdomenGroup_lumbarSpineAis;
    private box_extremitiesGroup_upperExtremitiesAis;
    private box_extremitiesGroup_lowerExtremitiesAis;
    private box_extremitiesGroup_pelvicGirdleAis;
    private box_pelvicGirdleGroup_pelvicGirdleAis;
    private box_externaGroup_externaAis;

    constructor(
        private modalService: NgbModal,
        private reportService: ReportService,
        private appStateService: AppStateService
    ) {}

    showISSDetails() {
      this.dialog = this.modalService.open(this.issDetailsDialog, {
        backdrop: 'static',
        keyboard: false
      });
    }

    updateISS() {
      if(this.selectedReport.iss.aisMapType === "Default"){
        this.box_headGroup_brainAis = this.selectedReport.iss.aisMap.headGroup.brainAis;
        this.box_headGroup_headNeckAis = this.selectedReport.iss.aisMap.headGroup.headNeckAis;
        this.box_headGroup_cervicalSpineAis = this.selectedReport.iss.aisMap.headGroup.cervicalSpineAis;
        this.box_faceGroup_faceAis = this.selectedReport.iss.aisMap.faceGroup.faceAis;
        this.box_toraxGroup_toraxAis = this.selectedReport.iss.aisMap.toraxGroup.toraxAis;
        this.box_toraxGroup_spineAis = this.selectedReport.iss.aisMap.toraxGroup.spineAis;
        this.box_abdomenGroup_abdomenAis = this.selectedReport.iss.aisMap.abdomenGroup.abdomenAis;
        this.box_abdomenGroup_lumbarSpineAis = this.selectedReport.iss.aisMap.abdomenGroup.lumbarSpineAis;
        this.box_extremitiesGroup_upperExtremitiesAis = this.selectedReport.iss.aisMap.extremitiesGroup.upperExtremitiesAis;
        this.box_extremitiesGroup_lowerExtremitiesAis = this.selectedReport.iss.aisMap.extremitiesGroup.lowerExtremitiesAis;
        this.box_extremitiesGroup_pelvicGirdleAis = this.selectedReport.iss.aisMap.extremitiesGroup.pelvicGirdleAis;
        this.box_externaGroup_externaAis = this.selectedReport.iss.aisMap.externaGroup.externaAis;
      }

      if(this.selectedReport.iss.aisMapType === "CenterTBI"){
        this.box_externaGroup_externaAis = this.selectedReport.iss.aisMap.externaGroup.externaAis;
        this.box_headGroup_brainAis = this.selectedReport.iss.aisMap.headGroup.brainAis;
        this.box_headGroup_headNeckAis = this.selectedReport.iss.aisMap.headGroup.headNeckAis;
        this.box_headGroup_cervicalSpineAis = this.selectedReport.iss.aisMap.headGroup.cervicalSpineAis;
        this.box_headGroup_faceAis = this.selectedReport.iss.aisMap.headGroup.faceAis;
        this.box_toraxGroup_toraxAis = this.selectedReport.iss.aisMap.toraxGroup.toraxAis;
        this.box_toraxGroup_spineAis = this.selectedReport.iss.aisMap.toraxGroup.spineAis;
        this.box_abdomenGroup_abdomenAis = this.selectedReport.iss.aisMap.abdomenGroup.abdomenAis;
        this.box_abdomenGroup_lumbarSpineAis = this.selectedReport.iss.aisMap.abdomenGroup.lumbarSpineAis;
        this.box_extremitiesGroup_upperExtremitiesAis = this.selectedReport.iss.aisMap.extremitiesGroup.upperExtremitiesAis;
        this.box_extremitiesGroup_lowerExtremitiesAis = this.selectedReport.iss.aisMap.extremitiesGroup.lowerExtremitiesAis;
        this.box_pelvicGirdleGroup_pelvicGirdleAis = this.selectedReport.iss.aisMap.pelvicGirdleGroup.pelvicGirdleAis;
      }

      this.dialog = this.modalService.open(this.issUpdateDialog, {
        backdrop: 'static',
        keyboard: false
      });
    }

    dismissDialog() {
      this.dialog.close();
    }

    confirmUpdateISS() {

      if(this.selectedReport.iss.aisMapType === "Default"){
        this.selectedReport.iss.isPresumed = false;
        this.selectedReport.iss.aisMap.headGroup.brainAis = this.box_headGroup_brainAis || 0;
        this.selectedReport.iss.aisMap.headGroup.headNeckAis = this.box_headGroup_headNeckAis || 0;
        this.selectedReport.iss.aisMap.headGroup.cervicalSpineAis = this.box_headGroup_cervicalSpineAis || 0;
        this.selectedReport.iss.aisMap.faceGroup.faceAis = this.box_faceGroup_faceAis || 0;
        this.selectedReport.iss.aisMap.toraxGroup.toraxAis = this.box_toraxGroup_toraxAis || 0;
        this.selectedReport.iss.aisMap.toraxGroup.spineAis = this.box_toraxGroup_spineAis || 0;
        this.selectedReport.iss.aisMap.abdomenGroup.abdomenAis = this.box_abdomenGroup_abdomenAis || 0;
        this.selectedReport.iss.aisMap.abdomenGroup.lumbarSpineAis = this.box_abdomenGroup_lumbarSpineAis || 0;
        this.selectedReport.iss.aisMap.extremitiesGroup.upperExtremitiesAis = this.box_extremitiesGroup_upperExtremitiesAis || 0;
        this.selectedReport.iss.aisMap.extremitiesGroup.lowerExtremitiesAis = this.box_extremitiesGroup_lowerExtremitiesAis || 0;
        this.selectedReport.iss.aisMap.extremitiesGroup.pelvicGirdleAis = this.box_extremitiesGroup_pelvicGirdleAis || 0;
        this.selectedReport.iss.aisMap.externaGroup.externaAis = this.box_externaGroup_externaAis || 0;
        this.selectedReport.iss.computeTotal();
      }

      if(this.selectedReport.iss.aisMapType === "CenterTBI"){
        this.selectedReport.iss.isPresumed = false;
        this.selectedReport.iss.aisMap.externaGroup.externaAis = this.box_externaGroup_externaAis || 0;
        this.selectedReport.iss.aisMap.headGroup.brainAis = this.box_headGroup_brainAis || 0;
        this.selectedReport.iss.aisMap.headGroup.headNeckAis = this.box_headGroup_headNeckAis || 0;
        this.selectedReport.iss.aisMap.headGroup.cervicalSpineAis = this.box_headGroup_cervicalSpineAis || 0;
        this.selectedReport.iss.aisMap.headGroup.faceAis = this.box_headGroup_faceAis || 0;
        this.selectedReport.iss.aisMap.toraxGroup.toraxAis = this.box_toraxGroup_toraxAis || 0;
        this.selectedReport.iss.aisMap.toraxGroup.spineAis = this.box_toraxGroup_spineAis || 0;
        this.selectedReport.iss.aisMap.abdomenGroup.abdomenAis = this.box_abdomenGroup_abdomenAis || 0;
        this.selectedReport.iss.aisMap.abdomenGroup.lumbarSpineAis = this.box_abdomenGroup_lumbarSpineAis || 0;
        this.selectedReport.iss.aisMap.extremitiesGroup.upperExtremitiesAis = this.box_extremitiesGroup_upperExtremitiesAis || 0;
        this.selectedReport.iss.aisMap.extremitiesGroup.lowerExtremitiesAis = this.box_extremitiesGroup_lowerExtremitiesAis || 0;
        this.selectedReport.iss.aisMap.pelvicGirdleGroup.pelvicGirdleAis = this.box_pelvicGirdleGroup_pelvicGirdleAis || 0;
        this.selectedReport.iss.computeTotal();
      }

      this.reportService.updateISS( this.selectedReport._id, this.selectedReport.iss)
      .then(rep => {
        this.appStateService.notifyReportSelected( rep );
        this.dialog.close();
      })
      .catch(error => {
        console.log('Error in updating the ISS' + error);
      });
    }
}

@Pipe( { name: 'filterOperatorName' } )
export class FilterOperatorNamePipe implements PipeTransform {
    transform( selectedReport ): string {
        if ( selectedReport !== undefined ) {
            if ( selectedReport.startOperatorDescription !== undefined && selectedReport.startOperatorDescription !== '' ) {
                return selectedReport.startOperatorDescription;
            } else {
                return selectedReport.startOperatorId;
            }
        } else {
            return '';
        }
    }
}

@Pipe( { name: 'filterTeamMembers' } )
export class FilterTeamMembersPipe implements PipeTransform {
    transform( teamMembers: string ): string {
        if ( teamMembers !== undefined ) {
            if ( teamMembers.length > 0 ) {
                let list = '';
                for ( let i = 0; i < teamMembers.length - 1; i++ ) {
                    list += teamMembers[i] + ', ';
                }
                list += teamMembers[teamMembers.length - 1];
                return list;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
}



