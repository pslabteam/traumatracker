import {Component, OnInit, Input, AfterViewInit, ViewChild} from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import {Chart} from 'chart.js';
import {Report} from '../../data-model/report';
import { ReportService } from '../../shared/report.service';
import { FilterEventContent } from '../event-trace/event-trace.component';

const  TEMP_VALUE_INDEX = 4;
const  HR_VALUE_INDEX = 2;
const  DIA_VALUE_INDEX = 7;
const  SIS_VALUE_INDEX = 3;
const  SPO2_VALUE_INDEX = 6;
const  ETCO2_VALUE_INDEX = 0;
const  NBPS_VALUE_INDEX = 1;
const  NBPD_VALUE_INDEX = 5;

const EVENT_DATASET_INDEX = 0;
const TEMP_DATASET_INDEX = 1;
const HR_DATASET_INDEX = 2;
const DIA_DATASET_INDEX = 3;
const SIS_DATASET_INDEX = 4;
const SPO2_DATASET_INDEX = 5;
const ETCO2_DATASET_INDEX = 6;
const NBPS_DATASET_INDEX = 7;
const NBPD_DATASET_INDEX = 8;


@Component({
  selector: 'events-chart',
  templateUrl: './events-chart.component.html',
  styleUrls: ['./events-chart.component.css']
})
export class EventsChartComponent implements OnInit {
  @Input() selectedReport: Report = undefined;

  vitalSignsViewOn: boolean;
  
  viewButtonText  = 'Visualizza parametri vitali';
  eventDistanceInPixels = 75; 
  minAxisYInCommon  = 0;
  maxAxisYInCommpn = 1;
  
  numValuesToConsiderAtStart = 20;
  numValuesToConsider = this.numValuesToConsiderAtStart;
  minValuesToConsider = 20;
  deltaValuesToConsider = 20; 
   
  eventsDataSet: any;
  dataSets: Array<any>;
  dataSetsList: Array<any>;
  maxTime: number;
      
  currentReport: Report;
      
  hrChart;
  tempChart;
  sis_diaChart;
  nbpsChart;
  nbpdChart;
  spO2Chart;
  etco2Chart;
  
  eventsConsidered: Array<any>;
  
  constructor(
    private reportService: ReportService,
  ) { 
      this.vitalSignsViewOn = false;
      this.currentReport = undefined;
  }

  ngOnInit() {}
  
  swapVitalSignsView(){
    const me: EventsChartComponent = this;
    if (!this.vitalSignsViewOn){
      this.vitalSignsViewOn = true;
      this.viewButtonText = 'Nascondi parametri vitali';
        this.reportService.getReport(this.selectedReport._id)
        .then(res => { 
          this.currentReport = res;
          me.buildCharts();
        });
    } else {
      this.vitalSignsViewOn = false;
      this.viewButtonText = 'Visualizza parametri vitali';
      this.numValuesToConsider = this.numValuesToConsiderAtStart;
    }
   }
  
  resetElement(elemName, wrapperName){
    let oldc = document.getElementById(elemName);
    if (oldc != null && oldc != undefined){
            oldc.remove();
            let canvas = document.createElement('canvas');
            canvas.setAttribute('id',elemName);
            let wrapper = document.getElementById(wrapperName);
            wrapper.appendChild(canvas);
    }
  }

  buildCharts(){
      this.prepareDataSets(this.currentReport);
      if (this.currentReport.vitalSignsObservations.length > 0) {
          
          this.resetElement('hrChart','hrChartAreaWrapper');
          this.resetElement('sis_dia_Chart','sis_dia_ChartAreaWrapper');
          this.resetElement('nbps_nbpd_Chart','nbps_nbpd_ChartAreaWrapper');
          this.resetElement('spO2Chart','spO2ChartAreaWrapper');
          this.resetElement('etcO2Chart','etcO2ChartAreaWrapper');
          this.resetElement('tempChart','tempChartAreaWrapper');
        
          if (this.hrChart != null) { this.hrChart.destroy(); }
          if (this.sis_diaChart != null) { this.sis_diaChart.destroy(); }
          if (this.nbpsChart != null) { this.nbpsChart.destroy(); }
          if (this.spO2Chart != null) { this.spO2Chart.destroy(); }
          if (this.etco2Chart != null) { this.etco2Chart.destroy(); }
          if (this.tempChart != null) { this.tempChart.destroy(); }
               
          this.hrChart = this.makeChart([this.dataSetsList[HR_DATASET_INDEX]], 'hrChart', 'hrChartAreaWrapper' );   
          this.sis_diaChart = this.makeChart([this.dataSetsList[SIS_DATASET_INDEX],this.dataSetsList[DIA_DATASET_INDEX]], 'sis_dia_Chart', 'sis_dia_ChartAreaWrapper');   
          this.nbpsChart = this.makeChart([this.dataSetsList[NBPS_DATASET_INDEX],this.dataSetsList[NBPD_DATASET_INDEX]], 'nbps_nbpd_Chart', 'nbps_nbpd_ChartAreaWrapper');   
          this.spO2Chart = this.makeChart([this.dataSetsList[SPO2_DATASET_INDEX]], 'spO2Chart', 'spO2ChartAreaWrapper');   
          this.etco2Chart = this.makeChart([this.dataSetsList[ETCO2_DATASET_INDEX]], 'etcO2Chart', 'etcO2ChartAreaWrapper');                 
          this.tempChart = this.makeChart([this.dataSetsList[TEMP_DATASET_INDEX]], 'tempChart', 'tempChartAreaWrapper');  
      }
    
  }
  
  addValues(){
    
      this.numValuesToConsider += this.deltaValuesToConsider;  
      this.buildCharts();
  }
  
  delValues(){
      this.numValuesToConsider -= this.deltaValuesToConsider; 
      if (this.numValuesToConsider < this.minValuesToConsider){
         this.numValuesToConsider = this.minValuesToConsider;  
      } 
      this.buildCharts();
  }
  
  prepareDataSets(rep: Report){    
            
    this.dataSets = new Array<any>();    
    this.eventsConsidered = new Array<any>();
        
    let eventsDataSet = new Array<any>();        
    let t = 0;
    let t0Ev;
     if (rep.events.length > 0){
       t0Ev = this.getTimeInSec(rep.events[0].time);   
     }
    let tprev = t0Ev;
    for (let ev of rep.events){
      let tcurr = this.getTimeInSec(ev.time);
      t += (tcurr - tprev);
      if (tcurr - tprev < 0) {
        t += 3600*24; /* new day */
      }
      this.maxTime = t;
      tprev = tcurr; 
      // console.log(' event '+ev.time+' '+time);
      if (ev.evType == 'procedure' || ev.evType == 'diagnostic' || ev.evType == 'drug' || ev.evType == 'blood-protocol' || 
        ev.evType == 'patient-accepted'){
        eventsDataSet.push({ x: t/60, y: this.minAxisYInCommon } );      
        this.eventsConsidered.push(ev);
      }
    }    
    
    let tempDataSet = new Array<any>();     
    let hrDataSet = new Array<any>();
    let diaDataSet = new Array<any>();
    let sysDataSet = new Array<any>();
    let nbpsDataSet = new Array<any>();
    let nbpdDataSet = new Array<any>();
    let spO2DataSet = new Array<any>();
    let etcO2DataSet = new Array<any>();
      
    let t0;    
    if (rep.vitalSignsObservations.length > 0){
      t0 = this.toSec(rep.vitalSignsObservations[0].timestamp);   
       
      t = 0;
      tprev = t0;
            
      const delta = rep.vitalSignsObservations.length > this.numValuesToConsider ? rep.vitalSignsObservations.length / this.numValuesToConsider : 1;
      let ind = 0;
      for (let i = 0; i < rep.vitalSignsObservations.length; i++){
          const obs = rep.vitalSignsObservations[i];
          if (i == Math.trunc(ind)){
            ind += delta;
            let tmin = t/60;
            tempDataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[TEMP_VALUE_INDEX].value.toString())});
            hrDataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[HR_VALUE_INDEX].value.toString()) });
            diaDataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[DIA_VALUE_INDEX].value.toString())});
            sysDataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[SIS_VALUE_INDEX].value.toString())});
            nbpsDataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[NBPS_VALUE_INDEX].value.toString())});
            nbpdDataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[NBPD_VALUE_INDEX].value.toString())}); 
            spO2DataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[SPO2_VALUE_INDEX].value.toString())});
            etcO2DataSet.push({ x: tmin, y: parseFloat(obs.vitalsigns[ETCO2_VALUE_INDEX].value.toString())});
          }
          let tcurr = this.toSec(obs.timestamp);
          t += (tcurr - tprev);
          if (tcurr - tprev < 0) {
            t += 3600*24; // new day 
          }
          tprev = tcurr; 
          if (t >= this.maxTime){
            break;
          }
      }  
    }
    
   // inserting data sets in proper order
     
   this.dataSets.push({ 
      pointRadius: 3,
      showLine: false,
      color: '#000000',
      data: eventsDataSet,
      label: 'Eventi'
    });

    this.dataSets.push({ 
          pointRadius: 2,
          showLine: true,
          cubicInterpolationMode: 'monotone',
      color: '#2ED3FC',
      data: tempDataSet,
      label: 'Temp ',
    });
    
    this.dataSets.push({ 
          pointRadius: 2,
          showLine: true,
          cubicInterpolationMode: 'monotone',
      color: '#008D07',
      data: hrDataSet,
      label: 'HR   '
    });

     
    this.dataSets.push({ 
          pointRadius: 2,
          showLine: true,
          cubicInterpolationMode: 'monotone',
      color: '#FF8400',
      data: diaDataSet,
      label: 'DIA  '
    });
    
   this.dataSets.push({ 
          pointRadius: 2,
          showLine: true,
          cubicInterpolationMode: 'monotone',
      color: '#CC000A',
      data: sysDataSet,
      label: 'SIS  '      
    });
      
    this.dataSets.push({ 
          pointRadius: 2,
          showLine: true,
          cubicInterpolationMode: 'monotone',
      color: '#AAAAAA',
      data: spO2DataSet,
      label: 'SpO2 '      
    });
    
    this.dataSets.push({ 
          pointRadius: 2,
          showLine: true,
          cubicInterpolationMode: 'monotone',
      color: '#ECE000',
      data: etcO2DataSet,
      label: 'EtCO2 '      
    });
  
    this.dataSets.push({ 
          pointRadius: 2,
          showLine: false,
          cubicInterpolationMode: 'monotone',
      color: '#CC000A',
          pointStyle: 'triangle',
      data: nbpsDataSet,
      label: 'NBP-S'      
    });
    
    
    this.dataSets.push({ 
          pointRadius: 2,
          showLine: false,
          cubicInterpolationMode: 'monotone',
          pointStyle: 'rect',
      color: '#BB000A',
      data: nbpdDataSet,
      label: 'NBP-D'      
    });

    
    this.dataSetsList = new Array<any>();
    for (let dataSet of this.dataSets){
      this.dataSetsList.push({
          label: dataSet.label,
          data: dataSet.data,
          borderColor: dataSet.color,
          pointRadius: dataSet.pointRadius,
          showLine: dataSet.showLine,
          cubicInterpolationMode: dataSet.cubicInterpolationMode,
          pointStyle: dataSet.pointStyle,
          fill: false,
      });
    } 
    
    this.eventsDataSet = this.dataSetsList[0];         
  }
  
  

  
makeChart(datasets, divChartName, wrapName): Chart{
    
    let tempWrapperArea = document.getElementById(wrapName);
    
    /* the width of the chart depends on the number of events */
    // tempWrapperArea.style.width = ''+this.timeDataPoints.length*this.eventDistanceInPixels+'px';
    tempWrapperArea.style.width = '1000px'; // +this.maxTime+'px';
    
    let canvas: any = document.getElementById(divChartName);
    let ctx: any = canvas.getContext('2d');        
    
    let evs = this.eventsConsidered; // this.selectedReport.events;     
    let dataSetsRef = this.dataSets;
    let evsDataSet = this.eventsDataSet;
        
    return new Chart(ctx, {
      type: 'scatter',      
      data: {
        datasets: datasets.concat(evsDataSet)
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        display: true,
        animation: {
            easing: "easeOutQuint",
        },                                   
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom',
                ticks: {
                    callback: function(value, index, values) {
                        return '' + Math.trunc(value / 60) +"m" + (value % 60) + 's' ;
                    }
                }                
            }]
        },
        legend: {
            position: 'right',
            display: true,
            labels: {
                fontFamily: 'Courier',
                generateLabels: function(){
                  let lab = new Array<any>();
                  for (const ds of datasets){
                    lab.push({
                      text: ds.label,
                      fillStyle: ds.borderColor,
                  });  
                  }
                  return lab;
                }
            }
        },
        
        tooltips: {
            callbacks: {
                title: function(tooltipItems, data){
                    let ev = evs[tooltipItems[0].index];
                    return ev.time+" "+'in '+ev.place+":";
                },
                label: function(tooltipItem, chart){
                  if (tooltipItem.datasetIndex == datasets.length){
                    return new FilterEventContent().apply(evs[tooltipItem.index].content,evs[tooltipItem.index].evType);
                  } else { 
                    return datasets[tooltipItem.datasetIndex].label + ": " + datasets[tooltipItem.datasetIndex].data[tooltipItem.index].y;
                  } 
                },
                labelColor: function(tooltipItem, chart) {
                  let obj = undefined;
                  if (tooltipItem.datasetIndex == datasets.length){
                    obj = evs;
                  } else {
                    obj = datasets[tooltipItem.datasetIndex];
                  }
                  return {
                        borderColor: '#FFFFFF',
                        backgroundColor: obj.borderColor
                  }
                },
                labelTextColor:function(tooltipItem, chart){
                    return '#FFFFFF';
                }
            }
          }      
        }
    });    
  }
  
  getTimeInSec(time: string): number {
    return parseInt(time.substring(0,2))*3600+
           parseInt(time.substring(3,5))*60+
           parseInt(time.substring(6)); 
    
  }
  
  // 20190610183315
  toSec(time: String): number {
    return parseInt(time.substring(8,10))*3600+
           parseInt(time.substring(10,12))*60+
           parseInt(time.substring(12));     
  }

}

@Pipe( { name: 'isVitalSignsDataNotEmpty' } )
 export class IsVitalSignsDataNotEmpty implements PipeTransform {
    transform( rep: Report): boolean {
        return rep !== undefined && rep.vitalSignsObservations !== undefined && rep.vitalSignsObservations.length > 0;
   }
}


