import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { Report, MajorTraumaCriteria } from '../../data-model/report';

@Component( {
    selector: 'major-trauma-criteria',
    templateUrl: './major-trauma-criteria.component.html',
} )
export class MajorTraumaCriteriaComponent {
    @Input() selectedReport: Report;
}

@Pipe( { name: 'isMajorTraumaCriteriaNotEmpty' } )
export class IsMajorTraumaCriteriaNotEmptyPipe implements PipeTransform {
    transform( an ): boolean {
      return an !== undefined && ((an.dynamic !== undefined && an.dynamic !=='') ||
              (an.physiological !== undefined && an.physiological !== '') ||
                (an.anatomical !== undefined && an.anatomical !== '')); 
    }
}
