import { Component, Input, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Pipe, PipeTransform } from '@angular/core';
import { TraumaTrackerAPIService } from '../../shared/trauma-tracker-api.service';
import { Report } from '../../data-model/report'
import { Event } from '../../data-model/report'
  
@Component( {
    selector: 'event-trace',
    templateUrl: './event-trace.component.html',
    /* encapsulation: ViewEncapsulation.None,
    styles: [`
    .view-modal.modal-content {
      width: 600;
      height: 450;
      }`
      // ,'table.test { border: 1px solid red; }' 
      ] */
    })
export class EventTraceComponent {

    @Input() selectedReport: Report = undefined;
    private eventToView;
    private dialog: NgbModalRef;
    
    public photoBaseURL: string;
    public videoBaseURL: string;
    public audioBaseURL: string;
  
    constructor(
        private modalService: NgbModal,
        private reportServiceAPI: TraumaTrackerAPIService
    ) { 
      this.photoBaseURL = reportServiceAPI.getPhotoDownloadURI();
      this.videoBaseURL = reportServiceAPI.getVideoDownloadURI();
      this.audioBaseURL = reportServiceAPI.getAudioDownloadURI();
    }
    
    viewMultimedia(content,event) {
      this.eventToView = event;
      this.dialog = this.modalService.open(content /*, { windowClass: 'view-modal'} */);      
    }
    
    getRoomColor(room) { 
      if (room=='TAC')
        return 'red';
      else 
        return 'blue';
    }
  
    dismissDialog(){
      this.dialog.close();
    }
    
    getEventsGroupedByDate(events: Event[]): Array<Event[]> {
      let g = new Array<Event[]>();
      let evl: Event[] = new Array<Event>();
      let currdate = "";
      for (let ev of events){
        if (ev.date !== currdate){
          if (evl.length !== 0){
            g.push(evl);
          }
          evl= new Array<Event>();
          currdate = ev.date;
        }
        evl.push(ev);
      }
      if (evl.length !== 0){
        g.push(evl);
      }
      return g;     
    }

   getEventsGroupedByPlace(events: Event[]): Array<Event[]> {
      let g = new Array<Event[]>();
      let evl: Event[] = new Array<Event>();
      let currplace = "";
      for (let ev of events){
        if (ev.place !== currplace){
          if (evl.length !== 0){
            g.push(evl);
          }
          evl= new Array<Event>();
          currplace = ev.place;
        }
        evl.push(ev);
      }
      if (evl.length !== 0){
        g.push(evl);
      }
      return g;
    }
  
   getGroupedEvents(report): Array<Event[]> {
      let g = new Array<Event[]>();
      let evl: Event[] = new Array<Event>();
      let currplace = "";
      for (let ev of report.events){
        if (ev.place !== currplace){
          if (evl.length !== 0){
            g.push(evl);
          }
          evl= new Array<Event>();
          currplace = ev.place;
        }
        evl.push(ev);
      }
      if (evl.length !== 0){
        g.push(evl);
      }
      return g;
    }
  
    removeEvent( selectedReport, event ) {
        //this.showModalService.publishEventToDelete( selectedReport, event );
    }  
}

@Pipe( { name: 'isEventVideo' } )
export class IsEventVideoPipe implements PipeTransform {
    transform( eventType ): boolean {
        // return eventType == "video" ? "show" : "hidden";
        return eventType === "video" ? true : false;
    }
}

@Pipe( { name: 'isEventAudio' } )
export class IsEventAudioPipe implements PipeTransform {
    transform( eventType ): boolean {
        // return eventType == "vocal-note" ? "show" : "hidden";
        return eventType === "vocal-note" ? true : false;
    }
}

@Pipe( { name: 'isEventPhoto' } )
export class IsEventPhotoPipe implements PipeTransform {
    transform( eventType ): boolean {
        // return eventType == "photo" ? "show" : "hidden";
        return eventType === "photo" ? true : false;
    }
}

export class FilterEventContent {
    apply( eventContent, eventType ): string {
        let msg = "";
        const isok = function( value ) {
            return value !== undefined; // && value !== "";
        }

        switch ( eventType ) {
            case "drug":
                if ( eventContent.administrationType == undefined || eventContent.administrationType === "one-shot" ) {
                    return "Farmaco: " + eventContent.drugDescription + " " + eventContent.qty + eventContent.unit;
                } else if ( eventContent.administrationType == "continuous-infusion"){
                    if ( eventContent.event === "start" ) {
                        return "Inizio Farmaco ad infusione: " + eventContent.drugDescription + " " + eventContent.qty + eventContent.unit;
                    } else if ( eventContent.event === "variation" ) {
                        return "Variazione Farmaco ad infusione: " + eventContent.drugDescription + " " + eventContent.qty + eventContent.unit;
                    } else if ( eventContent.event === "stop" ) {
                        return "Fine Farmaco ad infusione: " + eventContent.drugDescription + " - durata: " + eventContent.duration + "s";
                    }
                } else if ( eventContent.administrationType == undefined) { // drug-protocol
                    return "Protocollo Farmaco: " + eventContent.drugDescription;
                }
              break;
            case "blood-product":
                if ( eventContent.administrationType == undefined || eventContent.administrationType === "one-shot" ) {
                   let code = '';
                   if (eventContent.bloodProductId === 'emazies' || eventContent.bloodProductId === 'platelets' ||  eventContent.bloodProductId === 'fresh-frozen-plasma'){
                      code = ' (Codice sacca: ' + eventContent.bagCode + ')';
                   }
                   return "Emoderivato: " + eventContent.bloodProductDescription + " " + eventContent.qty + eventContent.unit + code;
                } else if ( eventContent.administrationType == "blood-protocol"){
                    return "Emoderivato: " + eventContent.bloodProductDescription;
                }
              break;
            case "procedure":
                if ( eventContent.procedureType === "one-shot" ) {
                    msg = "Manovra " + eventContent.procedureDescription;
                } else if ( eventContent.procedureType === "time-dependent" ) {
                    if ( eventContent.event === "start" ) {
                        msg = "Inizio Manovra " + eventContent.procedureDescription;
                    } else if ( eventContent.event === "end" ) {
                        msg = "Fine Manovra " + eventContent.procedureDescription;
                    } else {
                        msg = "Manovra " + eventContent.procedureDescription;
                    }
                }
                if ( eventContent.procedureId === "intubation" ) {
                    if ( isok( eventContent.difficultAirway ) ) {
                        msg = msg + " - vie aeree difficili: " + this.yesOrNo(eventContent.difficultAirway);
                    }
                    if ( isok( eventContent.inhalation ) ) {
                        msg = msg + " - inalazione: " + this.yesOrNo(eventContent.inhalation);
                    }
                    if ( isok( eventContent.videolaringo ) ) {
                        msg = msg + " - videolaringo: " + this.yesOrNo(eventContent.videolaringo);
                    }

                    if ( isok( eventContent.frova ) ) {
                        msg = msg + " - frova: " + this.yesOrNo(eventContent.frova);
                    }

                    return msg;

                } else if ( eventContent.procedureId === "drainage" ) {
                    if ( isok( eventContent.right ) ) {
                        msg = msg + " - destro: " + this.yesOrNo(eventContent.right);
                    }
                    if ( isok( eventContent.left ) ) {
                        msg = msg + " - sinistro: " + this.yesOrNo(eventContent.left);
                    }
                    return msg;
                } else if ( eventContent.procedureId === "chest-tube" ) {
                    if ( eventContent.right !== "" ) {
                        msg = msg + " - destro: " + this.yesOrNo(eventContent.right);
                    }
                    if ( isok( eventContent.left ) ) {
                        msg = msg + " - sinistro: " + this.yesOrNo(eventContent.left);
                    }
                    return msg;
                } else {
                    return msg;
                }
            case "diagnostic":
                msg = "Esame clinico " + eventContent.diagnosticDescription;
                  if ( eventContent.diagnosticId === "abg" ) {
                    if ( isok( eventContent.lactates ) ) {
                        msg = msg + " - lattati: " + eventContent.lactates + " mmoli/l";
                    }
                    if ( isok( eventContent.be ) ) {
                        msg = msg + " - be: " + eventContent.be;
                    }
                    if ( isok( eventContent.ph ) ) {
                        msg = msg + " - ph: " + eventContent.ph;
                    }
                    if ( isok( eventContent.hb ) ) {
                        msg = msg + " - hb: " + eventContent.hb;
                    }
                    return msg;
                } else if ( eventContent.diagnosticId === "rotem" ) {
                    if ( isok( eventContent.fibtem ) ) {
                        msg = msg + " - fibtem: " + eventContent.fibtem;
                    }
                    if ( isok( eventContent.extem ) ) {
                        msg = msg + " - extem: " + eventContent.extem;
                    }
                    if ( isok( eventContent.hyperfibrinolysis ) ) {
                        msg = msg + " - iperfibrinolisi: " + this.yesOrNo(eventContent.hyperfibrinolysis);
                    }
                    return msg;
                } else {
                    return msg;
                }

            case "vital-signs-mon":
                return "Parametri vitali monitor: \r\n" +
                    "TEMP " + eventContent.Temp +
                    "; HR " + eventContent.HR +
                    "; DIA " + eventContent.DIA +
                    "; SYS " + eventContent.SYS +
                    "; SpO2 " + eventContent.SpO2 +
                    "; etCO2 " + eventContent.EtCO2;
            case "clinical-variation":
                if  (eventContent.variationId !== "sedated"){
                  return "Variazione parametro vitale: " + eventContent.variationDescription +
                    " - nuovo valore: " + eventContent.value;
              } else {
                  /* "sedated" case: it is a boolean */
                  return "Variazione parametro vitale: " + eventContent.variationDescription +
                    " - nuovo valore: " + (eventContent.value == "true" ? "si" : "no");
              }
            case "trauma-leader":
                return "Cambio trauma-leader: " + eventContent.name + " " + eventContent.surname;
            case "photo":
                return "Foto";
            case "video":
                return "Video";
            case "vocal-note":
                return "Registrazione vocale";
            case "text-note":
                return "Nota testuale: " + eventContent.text;
            case "als-start":
                return "Inizio ALS";
            case "als-stop":
                return "Fine ALS - durata: " + eventContent.duration + " s";
            case "room-in":
                if (eventContent.place == "PRE-H"){
                    return  "Attivazione del Trauma Team";
                } else if (eventContent.place == "Trasporto"){
                    return "Inizio trasporto paziente";
                } else {
                  return "Ingresso luogo: " + eventContent.place ;
                }
            case "room-out":
                return eventContent.place == "Trasporto" ? "Fine trasporto paziente" : "Uscita luogo: " + eventContent.place
            case "report-reactivation":
                return "Ripristino report";
            case "patient-accepted":
                return "Ingresso del paziente in PS";
            default:
                return eventContent;
        }
    }

    yesOrNo(val: boolean): string {
      return val !== undefined ? (val ? 'sì' : 'no') : '';    
    }
}

@Pipe( { name: 'filterEventContent' } )
export class FilterEventContentPipe implements PipeTransform {
    transform( eventContent, eventType ): string {
      return new FilterEventContent().apply(eventContent,eventType);
    }
}


