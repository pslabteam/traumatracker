import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { Report } from '../data-model/report';
import { Subscription } from 'rxjs/Subscription';
import { AppStateService } from '../shared/app-state.service';
import { ReportService } from '../shared/report.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component( {
    selector: 'view-report',
    templateUrl: './view-report.component.html',
    providers: [
      /*PrintReportService */],
} )
export class ViewReportComponent implements OnInit, OnDestroy {

    private reportSub: Subscription;
    public selectedReport: Report;
    isReportToShow: boolean;
    private deleteRepModal: NgbModalRef;
    private viewVS: NgbModalRef;

    private dialog: NgbModalRef;
    @ViewChild('validateDialog') private validateDialog;

    private currentTab: number = 1;

    constructor(
        private appStateService: AppStateService,
        private reportService: ReportService,
        private modalService: NgbModal,
    ) { }

    ngOnInit(): void {
        this.currentTab = 1;
        this.isReportToShow = false;
        this.reportSub = this.appStateService.appEventChannel$.subscribe((event) => {
          if (event.eventType == 'report-selected'){
              this.selectedReport = event.report;
              this.isReportToShow = true;
              this.reportService.stopFetchingRecentReports();
            
          } else if (event.eventType == 'report-unselected' || event.eventType == 'changed-tab'){
              this.reportService.startFetchingRecentReports();
              this.isReportToShow = false;
          } 
        });
    }
  
    ngOnDestroy(){
        if ( this.reportSub !== undefined ) {
            this.reportSub.unsubscribe();
        }
    } 

    printReport(withMultimedia) {
        this.reportService.printReport(this.selectedReport, withMultimedia); 
    }

    deleteReport(){
        this.appStateService.notifyRepToDelete(this.selectedReport);
    }  
    
    viewChart(){
        this.appStateService.notifyRepToChart(this.selectedReport);
    }  
    
    viewVitalSigns(content): void {
      this.viewVS = this.modalService.open(content /*, { windowClass: 'view-modal'} */);      
    }
    
    dismissDialog(){
      this.viewVS.close();
    }
    
    
    exportReport( selectedReport ): void {
        this.reportService.exportReport(this.selectedReport); 
    }

    backToMain(): void {
        this.currentTab = 1;
        this.appStateService.notifyReportUnselected();
    }

    validate() {
        this.dialog = this.modalService.open(this.validateDialog, {
          backdrop: 'static',
          keyboard: false
        });
      }

      dismissValidationDialog() {
        this.dialog.close();
      }
  
      confirmValidation() {
          const operatorId = this.appStateService.getUserInfo().userId;
          this.reportService.validateReport( this.selectedReport._id, operatorId)
          .then(rep => {
            this.appStateService.notifyReportSelected( rep );
            this.dialog.close();
          })
          .catch(error => {
            console.log('Error in validating the report');
          });
      }

      changeTab(tabId){
          this.currentTab = tabId;
      }

      isTabActive(tabId): boolean {
          return this.currentTab===tabId;
      }
  
}
