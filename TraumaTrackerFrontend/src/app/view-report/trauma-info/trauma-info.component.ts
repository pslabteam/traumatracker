import { Component, Input } from '@angular/core';

@Component( {
    selector: 'trauma-info',
    templateUrl: './trauma-info.component.html',
} )
export class TraumaInfoComponent {
    @Input() selectedReport;
}
