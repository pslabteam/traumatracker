import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import {Chart} from 'chart.js';
import { ExportReportLib } from '../shared/export-csv';
import { ReportService } from '../shared/report.service';
import { StatService } from '../shared/stat.service';
import { AppStateService } from '../shared/app-state.service';
import { Subscription } from "rxjs/Subscription";
// import { StoreSearchParametersService } from '../store-search-parameters.service';
import "chartjs-chart-box-and-violin-plot/build/Chart.BoxPlot.js";




@Component( {
    selector: 'data-analysis',
    templateUrl: './data-analysis.component.html',
    providers: [ReportService],
    // styleUrls: ['./find-report.component.css']
} )
export class DataAnalysisComponent implements OnInit, OnDestroy {

    startDate;
    endDate;
    operatorId;

    /* def indicators */
  
    year: string = "2020";
    period: string = "jan-jun";
    defIndicatorsData = undefined;

    /* indicators */

    box_indic_temporalunit = 'mensile';
    box_indic_temporalunit_options = ['mensile', 'trimestrale', 'semestrale', 'annuale'];
    box_issConstraint = 0;
    comparisonWithPrevYear = false;
  
    indic_periods = ['mon','tri','sem','year'];
  
    indicators: Indicator[] = [ 
      new TraumaIndic(), 
      new PrimaryAdmisIndic(),
      new TransfBefore24Indic(),
      new TransfAfter24Indic(),
      new TransfFromSpokeIndic(),
      new AgeIndic(),
      new AgesOlderThan70Indic(),
      new MenIndic(),
      new TraumaHeadIndic(),
      new TraumaHemoIndic(),
      new TraumaPoliIndic(),
      new TraumaMajorIndic(),
      new IssGreaterThan16Indic(),
      new IssIndic(),
      new CauseTrafficIndic(),
      new CauseOtherIndic(),
      new TimeSHRtoTCIndic(),
      new TimeSHRtoDamIndic(),
      new TimeInSHRIndic(),
      new TimeSHRtoHead(),
      new ArrivalHeliIndic(),
      new FinalDestPSIndic(),
      new FinalDestTIIndic(),
      new FinalDestMUIndic(),
      new FinalDestOtherIndic()
      ];
    
    
    indicatorsData = undefined;
    showIndicCharts = false;
  
    constructor(
        private reportService: ReportService,
        private statService: StatService,
        private exportCSV: ExportReportLib,
    ) { }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    onExport() {
      const dateFrom: String = this.startDate !== undefined  && this.startDate !== null ?
              (this.startDate.year + '-' +
              (this.startDate.month > 9 ? this.startDate.month : '0' + this.startDate.month) + '-' +
              (this.startDate.day > 9 ? this.startDate.day : '0' + this.startDate.day)) : '';

      const dateTo: String = this.endDate !== undefined  && this.endDate !== null ?
              this.endDate.year + '-' +
              (this.endDate.month > 9 ? this.endDate.month : '0' + this.endDate.month) + '-' +
              (this.endDate.day > 9 ? this.endDate.day : '0' + this.endDate.day) : '';

      this.reportService.findReports( this.startDate, this.endDate, this.operatorId, null )
            .then( response => {
              console.log('EXPORT DONE.');
              this.exportCSV.exportReports(dateFrom, dateTo, response);
            } );

    }
  
   onAggrExport() {
      const dateFrom: String = this.startDate !== undefined  && this.startDate !== null ?
              (this.startDate.year + '-' +
              (this.startDate.month > 9 ? this.startDate.month : '0' + this.startDate.month) + '-' +
              (this.startDate.day > 9 ? this.startDate.day : '0' + this.startDate.day)) : '';

      const dateTo: String = this.endDate !== undefined  && this.endDate !== null ?
              this.endDate.year + '-' +
              (this.endDate.month > 9 ? this.endDate.month : '0' + this.endDate.month) + '-' +
              (this.endDate.day > 9 ? this.endDate.day : '0' + this.endDate.day) : '';

      this.statService.getAggregateExport( this.startDate, this.endDate, this.operatorId )
            .then( content => {
              console.log('AGGR EXPORT received, going to export.');
              this.saveData(content);
            } );

    }
   
     private saveData( content) {
       const blob = new Blob( [content], { type: 'text/csv;charset=utf-8;' } );
        const filename = "aggr-stat.csv";

        if ( navigator.msSaveBlob ) { // IE 10+
            navigator.msSaveBlob( blob, filename );
        } else {
            const link = document.createElement( "a" );
            if ( link.download !== undefined ) { // feature detection
                // Browsers that support HTML5 download attribute
                const url = URL.createObjectURL( blob );
                link.setAttribute( "href", url );
                link.setAttribute( "download", filename );
                link.style.visibility = 'hidden';
                document.body.appendChild( link );
                link.click();
                document.body.removeChild( link );
            }
        }
    }

    onCountReports(){
        const dateFrom = this.getDateFormatted(this.startDate);
        const dateTo = this.getDateFormatted(this.endDate);
        this.statService.countRecsStat( this.startDate, this.endDate)
            .then( response => {
              console.log(response);
            } );

    }

    getDateFormatted(date): string {
      return date !== undefined  && date !== null ?
              (date.year + '-' +
              (date.month > 9 ? date.month : '0' + date.month) + '-' +
              (date.day > 9 ? date.day : '0' + date.day)) : '';
    }
  
    yearSelected(yy: string) {
      this.year = yy;
    }

    periodSelected(p: string) {
      this.period = p;
    }

    resetDefIndic() {
      this.defIndicatorsData = undefined;
    }

    computeDefIndic() {
        this.statService.computeDefIndic( this.year, this.period)
            .then( response => {
              this.defIndicatorsData = response;
              console.log(response);
            } );
   }
  
    exportDefIndicPDF(){
    }
    
    exportDefIndicCSV() {
      this.exportCSV.exportDefIndic(this.defIndicatorsData);
    }
  
    computeIndic() {
        /* for testing */
        
        // this.indicators.forEach(el => el.selected = true );
        /*
        this.startDate = { day: 1, month: 6, year: 2019 };
        this.endDate = { day: 1, month: 6, year: 2020 };
        this.comparisonWithPrevYear = true;
        */
        const indics = this.indicators
        .filter(function(e) {
          return e.selected;
        }).map(function(e) {
          return e.id;
        });

        const index = this.box_indic_temporalunit_options.indexOf(this.box_indic_temporalunit);
        this.showIndicCharts = true;
        this.statService.computeIndic( this.startDate, this.endDate, this.comparisonWithPrevYear,
                                      this.indic_periods[index], indics.toString(), this.box_issConstraint)
            .then( response => {
              this.indicatorsData = response;

              for (let indicData of this.indicatorsData.data) {
                  this.indicators
                    .find(indicat => indicat.id === indicData.id)
                    .buildChart(document, {
                      selectedPeriodData: indicData.data,
                      prevYearPeriodData: indicData.dataPrevYear  
                    });
              }
              // console.log(response);
            })
            .catch( error => {
              this.showIndicCharts = false;
            });
  }
}


abstract class Indicator {
   selected: boolean;
   comparisonWithPrevYear: boolean;
  
   /* main chart */
   chart;
   wrapperId;
   canvasId;
  
   /* aux chart -- @TODO trick to remove */
   companionChart;
   wrapperCompanionId;
   canvasCompanionId;
  
   /* perc */
    chartPerc;
    wrapperPercId;
    canvasPercId;
   
  constructor(public id: string, public desc: string) {
    this.selected = false;
    this.comparisonWithPrevYear = false;
    this.wrapperId = id + "Wrapper";
    this.canvasId = id + "Canvas";
  }

  hasPerc(): boolean { return false; }
  
  hasCompanion(): boolean { return false; }
  
  buildChart(doc, data) {
    if (this.chart !== undefined) {
      this.chart.destroy();
    }
  }
  
  protected buildBarChart(doc, data, func) {
      let labels = data.selectedPeriodData.map(function (el) { return el.period; });  
      let chartData = [{
        label: this.desc,
        data: data.selectedPeriodData.map(func),
        backgroundColor: 'rgba(0,0,255,1)',
        borderWidth: 1,
        type: 'bar'
      }
      ];
      if (data.prevYearPeriodData !== undefined && data.prevYearPeriodData.length > 0) {
        chartData.push({
          label: 'Anno precedente',
        backgroundColor: 'rgba(0,0,255,0.5)',
          data: data.prevYearPeriodData.map(func),
          borderWidth: 1,
          type: 'bar'
        });
      }
      this.chart = makeChart(doc, this.wrapperId, this.canvasId, labels, chartData);   
  }
  
  protected buildRawBarChart(doc, labels, chartData) {
      this.chart = makeChart(doc, this.wrapperId, this.canvasId, labels, chartData);   
  }
  
  protected buildGenChart(chartType, doc, labels, chartData, opts) {
      this.chart = makeGenChart(chartType, doc, this.wrapperId, this.canvasId, labels, chartData, opts);   
  }
  
  protected buildCompanionGenChart(chartType, doc, labels, chartData, opts) {
      this.companionChart = makeGenChart(chartType, doc, this.wrapperCompanionId, this.canvasCompanionId, labels, chartData, opts);   
  }
}

abstract class ValueIndicator extends Indicator {

    hasCompanion(): boolean { return true; }

    buildChart(doc, data) {
      super.buildChart(doc, data);

      const indicData = data.selectedPeriodData;
      const boxPlotMedianValues = indicData.map(function (el) { return el.values; });
      const labels = indicData.map(function (el) { return el.period; });
      const chartData = [
        {
          label: this.desc + ': mediana e IQR',
          data: boxPlotMedianValues,
          borderWidth: 1,
          backgroundColor: 'rgba(0,0,255,0.5)',
          borderColor: 'rgba(0,0,255,1)',
          outlierColor: '#999999',
          padding: 10,
          itemRadius: 0,
        }
      ];
      if (data.prevYearPeriodData !== undefined && data.prevYearPeriodData.length > 0) {
        const boxPlotMedianValuesPrevYear = data.prevYearPeriodData.map(function (el) { return el.values; });

        chartData.push({
          label: this.desc + ': mediana e IQR anno precedente',
          data: boxPlotMedianValuesPrevYear,
          borderWidth: 1,
          backgroundColor: 'rgba(128,128,255,0.5)',
          borderColor: 'rgba(128,128,255,1)',
          outlierColor: '#999999',
          padding: 10,
          itemRadius: 0,
        });
      }

      this.buildGenChart('boxplot', doc, labels, chartData, {} );

      const chartAverageData = indicData.map(function (el) { return el.mean; });
      const chartStandDevData = indicData.map(function (el) { return el.standDev; });
      const chartMeanData = [
        {
          label: this.desc + ': Media',
          data: chartAverageData,
          stack: '0',
          backgroundColor: 'rgba(0,0,255,0.8)',
        },
        {
          label: this.desc + ': Deviazione Standard',
          data: chartStandDevData,
          stack: '0',
          backgroundColor: 'rgba(0,0,255,0.2)',
        },
      ];
      if (data.prevYearPeriodData !== undefined && data.prevYearPeriodData.length > 0) {
        const chartAverageDataPrevYear = data.prevYearPeriodData.map(function (el) { return el.mean; });
        const chartStandDevDataPrevYear = data.prevYearPeriodData.map(function (el) { return el.standDev; });

        chartMeanData.push(
        {
          label: this.desc + ': media anno precedente',
          data: chartAverageDataPrevYear,
          stack: '1',
          backgroundColor: 'rgba(128,128,255,0.8)',
        });

        chartMeanData.push(
        {
          label: this.desc + ': deviazione standard anno precedente',
          data: chartStandDevDataPrevYear,
          stack: '1',
          backgroundColor: 'rgba(128,128,255,0.2)',
        });
      }

      const options = {
        scales: {
          xAxes: [{
            stacked: true,
             gridLines: {
               display: false,
            }
          }],
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
              precision: 0
            },
        }]
        },
        responsive: true,
      }
      this.wrapperCompanionId = this.wrapperId + 'comp';
      this.canvasCompanionId = this.canvasId + 'comp';
      this.buildCompanionGenChart('bar', doc, labels, chartMeanData, options);
    }
}

abstract class NumberOfReportIndicator extends Indicator {

    hasPerc(): boolean { return true; }

    buildChart(doc, data) {
      super.buildChart(doc, data);
      this.wrapperPercId = this.wrapperId + 'perc';
      this.canvasPercId = this.canvasId + 'perc';
      this.chartPerc = makeChartPerc(doc,data,this.wrapperPercId, this.canvasPercId);
    }
}


class TraumaIndic extends Indicator {
    constructor(){
      super('num', 'Numerosità Totale')
    }
    buildChart(doc, data) {
      super.buildChart(doc, data);
      this.buildBarChart(doc, data, el => el.nTrauma );  
    }
}

class PrimaryAdmisIndic extends NumberOfReportIndicator {
    constructor(){
      super('primary-admin', 'Ammissioni Primarie');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nPrimaryAdmis );  
    }
}

class TransfBefore24Indic extends NumberOfReportIndicator {
    constructor(){
      super('transf-before24', 'Trasferiti entro 24h Trauma');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nTransfBefore24h );  
    }
}

class TransfAfter24Indic extends NumberOfReportIndicator {
    constructor(){
      super('transf-after24', 'Trasferiti dopo 24h Trauma');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nTransfAfter24h );  
    }
}

class TransfFromSpokeIndic extends NumberOfReportIndicator {
    constructor(){
      super('transf-from-spoke', 'Trasferiti da spoke');
    }
    
    buildChart(doc, data) {
      super.buildChart(doc,data);
      
      let indicData = data.selectedPeriodData;
      let chartDataTotal = indicData.map(function (el) { return el.nTotalFromSpokes; });
      let labels = indicData.map(function (el) { return el.period; });               
    
      let fromRA = this.fromCity(indicData, 'Ravenna');
      let fromLU = this.fromCity(indicData, 'Lugo');
      let fromFA = this.fromCity(indicData, 'Faenza');
      let fromFO = this.fromCity(indicData, 'Forlì');
      let fromRN = this.fromCity(indicData, 'Rimini');
      let fromRC = this.fromCity(indicData, 'Riccione');
      let fromOthers = this.fromOtherCities(indicData, ['Ravenna', 'Forlì', 'Rimini', 'Faenza', 'Lugo','Riccione']);
      let cities = [];
      for (let el of fromOthers) {
          for ( let elem of el.cities) {
            if (cities.indexOf(elem) === -1) {
            cities.push(elem);
          }
        }
      }

      let othersLabel = 'Trasferiti da Altri (' + cities + ')';
      let othersData = fromOthers.map(el => el.total);
      let chartData = [
      
      {
        label: 'Trasferiti da Ravenna',
        data: fromRA,
        backgroundColor: "#caf270",
      },
      {
        label: 'Trasferiti da Lugo',
        backgroundColor: "#45c490",
        data: fromLU
      },
      {
        label: 'Trasferiti da Faenza',
        backgroundColor: "#008d93",
        data: fromFA
      },
      {
        label: 'Trasferiti da Forlì',
        backgroundColor: "#2e5468",
        data: fromFO
      },
      {
        label: 'Trasferiti da Rimini',
        backgroundColor: "#408d50",
        data: fromRN,
      },
      {
        label: 'Trasferiti da Riccione',
        backgroundColor: "#a0f250",
        data: fromRC
      },
      {
        label: othersLabel,
        backgroundColor: 'rgba(0,0,0,0.2)',
        data: othersData
      }
      
      ];
      
      let options = {
        scales: {
          xAxes: [{
            stacked: true,
             gridLines: {
               display: false,
            }
          }],
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
              precision: 0
            },
        }]
        },
        responsive: true,
      } 
      this.buildGenChart('bar', doc, labels, chartData, options);
  }

  protected fromCity(indicData, city){
    return indicData.map(function (el) { 
        let l = el.spokes.filter(function (sp) {
          return sp.spoke === city;
        });
        if (l.length > 0) {
          return l[0].nTrauma;
        } else {
          return 0;
        }
      });
  }
  
  protected fromOtherCities(indicData, exeptCities){
    return indicData.map(el => { 
        let l = el.spokes.filter(sp => exeptCities.indexOf(sp.spoke) == -1);
        
        return l.reduce((acc, sp) => {
          let nc =  acc.cities;
          if (sp.spoke !== '' && acc.cities.indexOf(sp.spoke) === -1) {
            nc.push(sp.spoke);
          }
          let nt = acc.total + sp.nTrauma;
          return {
            cities: nc,
            total: nt
          }
        }, {
            cities: [],
            total: 0
        });
      });
  }
}

class AgeIndic extends ValueIndicator {
    constructor(){
      super('age', 'Età pazienti');
    }
}

class AgesOlderThan70Indic extends NumberOfReportIndicator {
    constructor(){
      super('age-older-70', 'Età >= 70');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nAgeOlderEqThan70 );  
    }
}

class MenIndic extends NumberOfReportIndicator {
    constructor(){
      super('men', 'Uomini');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nMen );  
    }
}

class TraumaHeadIndic extends NumberOfReportIndicator {
    constructor(){
      super('trauma-head', 'Traumi cranici');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nTraumaHead );  
    }
}

class TraumaHemoIndic extends NumberOfReportIndicator {
    constructor(){
      super('trauma-hemo', 'Traumi emorragici');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nTraumeHemo );  
    }
}

class TraumaPoliIndic extends NumberOfReportIndicator {
    constructor(){
      super('trauma-poli', 'Politraumi');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nTraumaPoli );  
    }
}

class TraumaMajorIndic extends NumberOfReportIndicator {
    constructor(){
      super('trauma-major', 'Traumi maggiori');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nTraumaMajor );  
    }
}

class IssGreaterThan16Indic extends NumberOfReportIndicator {
    constructor() {
      super('iss-greater-than-16', 'ISS >= 16');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nISSGreaterEqualThan16 );  
    }
}

class IssIndic extends ValueIndicator {
    constructor() {
      super('iss', 'ISS');
    }
}

class CauseTrafficIndic extends NumberOfReportIndicator {
    constructor() {
      super('cause-traffic', 'Causa incidente stradale');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nCarAccidents );  
    }
}

class CauseOtherIndic extends NumberOfReportIndicator {
    constructor() {
      super('cause-other', 'Non causati da incidente stradale');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nNotCarAccidents );  
    }
}

class TimeSHRtoTCIndic extends ValueIndicator {
    constructor() {
      super('time-shr-tc', 'Tempo tra shock oom e tc');
    }
}

class TimeSHRtoDamIndic extends ValueIndicator {
    constructor() {
      super('time-shr-dam', 'Tempo tra shock room e damage control');
    }
}

class TimeInSHRIndic extends ValueIndicator {
    constructor() {
      super('time-in-shr', 'Tempo in shock room');
    }
}

class TimeSHRtoHead extends ValueIndicator {
    constructor() {
      super('time-shr-head', 'Tempo fra shock room e craniotomia (sop)');
    }
}

class ArrivalHeliIndic extends NumberOfReportIndicator {
    constructor() {
      super('arrival-heli', 'Mezzo soccorso: Elicottero');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nArrivalsHeli );  
    }
}

class FinalDestPSIndic extends NumberOfReportIndicator {
    constructor() {
      super('final-dest-PS', 'Destinalzione finale: Pronto Soccorso');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nFinalDestPS );  
    }
}

class FinalDestTIIndic extends NumberOfReportIndicator {
    constructor() {
      super('final-dest-TI', 'Destinalzione finale: Terapia Intensiva');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nFinalDestTI );  
    }
}

class FinalDestMUIndic extends NumberOfReportIndicator {
    constructor() {
      super('final-dest-MU', "Destinalzione finale: Medicina d'Urgenza");
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nFinalDestMU );  
    }
}
 
class FinalDestOtherIndic extends NumberOfReportIndicator {
    constructor() {
      super('final-dest-other', 'Destinalzione finale: Altro');
    }
    buildChart(doc, data) {
      super.buildChart(doc,data);
      this.buildBarChart(doc, data, el => el.nFinalDestOther );  
    }
}

function randomValues(count, min, max) {
  const delta = max - min;
  return Array.from({length: count}).map(() => Math.random() * delta + min);
}

 function makeGenChart(chartType, document, chartAreaWrapper, divChartId, labels, chartData, opts): Chart {

    let tempWrapperArea = document.getElementById(chartAreaWrapper);
    tempWrapperArea.style.width = '1000px';  
    let canvas: any = document.getElementById(divChartId);
    let ctx: any = canvas.getContext('2d');

    return new Chart(ctx, {
      type: chartType,
      data: {
        labels: labels,
        datasets: chartData
      },
      options: opts
    })
  }

 function makeChart(document, chartAreaWrapper, divChartId, labels, chartData): Chart {

    let tempWrapperArea = document.getElementById(chartAreaWrapper);
    tempWrapperArea.style.width = '1000px'; // +this.maxTime+'px';
    
    let canvas: any = document.getElementById(divChartId);
    let ctx: any = canvas.getContext('2d');

    return new Chart(ctx, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: chartData
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    precision: 0
                }
            }]
        }
      }
    })
  }
 
 function makeChartPerc(doc,data,chartAreaWrapper, divChartId): Chart{

  let indicData = data.selectedPeriodData;
  let chartPercData = indicData.map(function (el) { return el.perc; });
  let labels = indicData.map(function (el) { return el.period; });  
  let chartData = [{
        label: 'In percentuale',
        data: chartPercData,
        borderColor: 'rgba(0,0,255,1)',
        borderWidth: 1,
        type: 'line',
        fill: false,
      }
  ];
  
  if (data.prevYearPeriodData !== undefined && data.prevYearPeriodData.length > 0) {
        let chartPercDataPrev = data.prevYearPeriodData.map(function (el) { return el.perc; });
        chartData.push({
          label: 'Anno precedente',
          data: chartPercDataPrev,
          borderColor: 'rgba(0,0,255,0.5)',
          borderWidth: 1,
          fill: false,
          type: 'line'
        });
      }
  
   let tempWrapperArea = document.getElementById(chartAreaWrapper);
    tempWrapperArea.style.width = '1000px'; // +this.maxTime+'px';
    
    let canvas: any = document.getElementById(divChartId);
    let ctx: any = canvas.getContext('2d');

    return new Chart(ctx, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: chartData
      },
      options: {
        scales: {
            yAxes: [
            {
                ticks: {
                    min: 0,
                    max: 100,
                    precision: 0
                }
              }]
        }
      }
    });  
 }
 