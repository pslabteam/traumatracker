import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';

import { AppStateService } from '../shared/app-state.service';
import { ReportService } from '../shared/report.service';
import { Pipe, PipeTransform } from '@angular/core';
import { Report } from "../data-model/report";
import { Subscription } from "rxjs/Subscription";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component( {
    selector: 'report-compact-details',
    templateUrl: './report-compact-details.component.html',
} )
export class ReportCompactDetailsComponent {
    
    @Input() private reports;
    
    private reportSub: Subscription;
    public selectedReport: Report;
    isReportToShow: boolean;
    private deleteRepModal: NgbModalRef;
    
    constructor(
        private appStateService: AppStateService,
        private reportService: ReportService
    ) { }
    
  ngOnInit(): void {
        this.isReportToShow = false;
        this.reportSub = this.appStateService.appEventChannel$.subscribe((event) => {
          if (event.eventType == 'report-selected'){
              this.selectedReport = event.report;
              if (this.selectedReport._id === 'rep-20190528-111945'){
                   let ms = this.selectedReport.vitalSignsObservations;                
                   console.log("size " + ms.length);
              }
              this.isReportToShow = true;
          } else if (event.eventType == 'report-unselected'){
              this.isReportToShow = false;
          } 
        });
    }
  
    ngOnDestroy(){
        if ( this.reportSub !== undefined ) {
            this.reportSub.unsubscribe();
        }
    } 
   
    showReport( report ): void {
        this.appStateService.notifyReportSelected( report );
    }

    printReport(report) {
        this.reportService.printReport(report, false); 
    }
  
    exportReport( report ): void {
        this.reportService.exportReport( report );
    }

    deleteReport( report ): void {
        this.appStateService.notifyRepToDelete(report);
    }
}





