import { Component, OnInit } from '@angular/core';
import { AppStateService } from '../shared/app-state.service';
import { GetAppVersionService } from './get-version.service';
import { Subscription } from "rxjs/Subscription";
import { ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../shared/user.service';
import { User } from '../data-model/user';

import 'rxjs/add/operator/toPromise';

@Component( {
    selector: 'app-state',
    templateUrl: './app-state.component.html',
    // styleUrls: ['./app-version.component.css'],
    providers: [GetAppVersionService]
} )
export class AppStateComponent implements OnInit {

    version = '<unknown version>';
    private user;
    isUserLogged: boolean;
    private loginSub: Subscription;
    private updateUserDialog: NgbModalRef;

    updatedEmail: string;
    updatedRole: string;
    updatedPassword: string;
  
    @ViewChild('updateUserModal') private updateUserModal;

    constructor(
        private appVersionService: GetAppVersionService,
        private appStateService: AppStateService,
        private userService: UserService,        
        private modalService: NgbModal
    ) { 
      this.isUserLogged = false;
    }

    ngOnInit(): void {
        this.getAppVersion();
        this.loginSub = this.appStateService.appEventChannel$.subscribe((event) => {
          if (event.eventType === 'user-logged-in') {
              this.isUserLogged = true;
              this.user = event.user ;
          } else if (event.eventType === 'user-logged-out') {
              this.isUserLogged = false;
              this.user = undefined;
          }
        });
      
    }

    private getAppVersion() {
        this.appVersionService.getAppVersionFromServer().then(response => {
            this.version = response.version;
        })
    }
    
    logout(): void {
        this.appStateService.logout();
    }
  
    openUserProfileDialog(): void {
        this.updatedEmail = this.user.email;
        this.updatedPassword = this.user.pwd;
        this.updatedRole = this.user.role;
                
        this.updateUserDialog = this.modalService.open(this.updateUserModal, {
            backdrop: 'static',
            keyboard: false
        });

    }
  
    updateUser(): void {
        const updatedUser = new User();
        updatedUser.userId = this.user.userId;
        updatedUser.name = this.user.name;
        updatedUser.surname = this.user.surname;
        updatedUser.email = this.updatedEmail;
        updatedUser.pwd = this.updatedPassword;
        updatedUser.role = this.updatedRole;

        this.userService.updateUser(updatedUser)
          .then(res => {
            console.log('user update ok');
            this.updateUserDialog.close();
          })
          .catch(err => {
            console.log('user update failed');
            this.updateUserDialog.close();
          });
    }

    dismissUpdateUserDialog(): void {
        this.updateUserDialog.close();
    }
}

