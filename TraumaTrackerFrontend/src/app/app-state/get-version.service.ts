import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { TraumaTrackerAPIService } from '../shared/trauma-tracker-api.service';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class GetAppVersionService {

    constructor(
        private http: HttpClient,
        private traumaTrackerApi: TraumaTrackerAPIService
    ) { }

    getAppVersionFromServer(): Promise<any> {
        const uri =  this.traumaTrackerApi.getVersionURI();
        return this.http.get(uri, {responseType: 'json'})
                .toPromise()
                .catch( this.handleError );
    }

    private handleError( error: any ): Promise<any> {
        console.error( 'An error occurred', error );
        return Promise.reject( error.message || error );
    }
}
