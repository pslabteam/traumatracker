import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecentReportsComponent } from './recent-reports/recent-reports.component';
import { FindReportComponent } from './find-report/find-report.component';

const routes: Routes = [
    { path: '', redirectTo: 'gt2/traumatracker/web2', pathMatch: 'full' },
    { path: 'gt2/traumatracker/web2/recent-reports', component: RecentReportsComponent },
    { path: 'gt2/traumatracker/web2/find-reports', component: FindReportComponent }
];

@NgModule( {
    imports: [RouterModule.forRoot( routes )],
    exports: [RouterModule]
} )
export class AppRoutingModule { }
