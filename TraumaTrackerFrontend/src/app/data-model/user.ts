import {JsonObject, JsonProperty} from "json2typescript";

@JsonObject("User")
export class User {
  
    userId: string;
    name: string;
    surname: string;
    pwd: string;
    email: string;
    role: string;
    token: string;
        
    isAdminRole(): boolean {
      return this.role === 'admin';
    }

    isDirectorRole(): boolean {
      return this.role === 'director';
    }
    
    canManageData(): boolean {
      return this.role === 'director' || this.role == 'data-analyst' || this.role == 'admin';
    }
}