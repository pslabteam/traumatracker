import { unsupported } from '@angular/compiler/src/render3/view/util';
import {JsonObject, JsonProperty, JsonCustomConvert, JsonConvert, JsonConverter} from "json2typescript";

@JsonObject("Validation")
export class Validation {
    
    @JsonProperty("isValidated", Boolean, true)
    isValidated: boolean = undefined;
  
    @JsonProperty("operatorId",String,true)
    operatorId: string = undefined;

    @JsonProperty("validationTime",String,true)
    validationTime: string = undefined;
  
    @JsonProperty("validationDate",String,true)
    validationDate: string = undefined;
}

@JsonObject("TraumaInfo")
export class TraumaInfo {
    
    @JsonProperty("name",String,true)
    name: string = undefined;    
    
    @JsonProperty("surname",String,true)
    surname: string = undefined;    
    
    @JsonProperty("dob",String,true)
    dob: string = undefined;    
    
    @JsonProperty("code",String,true)
    code: string = undefined;    
    
    @JsonProperty("sdo",String,true)
    sdo: string = undefined;
  
    @JsonProperty("erDeceased",Boolean,true)
    erDeceased: boolean = undefined;
    
    @JsonProperty("admissionCode",String,true)
    admissionCode: string = undefined;
  
    @JsonProperty("gender",String,true)
    gender: string = undefined;
    
    @JsonProperty("age",Number,true)
    age: number = undefined;
    
    @JsonProperty("accidentDate",String,true)
    accidentDate: string = undefined;
    
    @JsonProperty("accidentTime",String,true)
    accidentTime: string = undefined;
    
    @JsonProperty("accidentType",String,true)
    accidentType: string = undefined;
    
    @JsonProperty("vehicle",String,true)
    vehicle: string = undefined;
    
    @JsonProperty("fromOtherEmergency",Boolean,true)
    fromOtherEmergency: boolean = undefined;
  
    @JsonProperty("otherEmergency",String,true)
    otherEmergency: string = undefined;
}

@JsonObject("Anamnesi")
export class  Anamnesi {
    @JsonProperty("antiplatelets",Boolean,true)
    antiplatelets: boolean = undefined;
    
    @JsonProperty("anticoagulants",Boolean,true)
    anticoagulants: boolean = undefined;
    
    @JsonProperty("nao",Boolean,true)
    nao: boolean = undefined;
}

@JsonObject("MajorTraumaCriteria")
export class  MajorTraumaCriteria {
    @JsonProperty("dynamic",Boolean,true)
    dynamic: boolean = undefined;
    
    @JsonProperty("physiological",Boolean,true)
    physiological: boolean = undefined;
    
    @JsonProperty("anatomical",Boolean,true)
    anatomical: boolean = undefined;
    
}

@JsonObject("VitalSigns")
export class VitalSigns {

  @JsonProperty("temp",String,true)
  temp: string = undefined;

  @JsonProperty("hr",String,true)
  hr: string = undefined;

  @JsonProperty("bp",String,true)
  bp: string = undefined;

  @JsonProperty("spo2",String,true)
  spo2: string = undefined;

  @JsonProperty("etco2",String,true)
  etco2: string = undefined;
}

@JsonObject("ClinicalPicture")
export class ClinicalPicture {
    
      gcsMotorOptions = ['(6) Esegue', '(5) Localizza', '(4) Retrae', '(3) Flessione', '(2) Estende', '(1) Nulla'];
      gcsVerbalOptions = ['(5) Orienta', '(4) Confusa', '(3) Inappropriate', '(2) Incomprensibile', '(1) Nulla'];
      gcsEyesOptions = ['(4) Spontanea', '(3) Chiamata', '(2) Dolore', '(1) Nulla'];
  
        @JsonProperty("gcsTotal",Number,true)
        gcsTotal: number = undefined;
        
        @JsonProperty("gcsMotor",String,true)
        gcsMotor: string = undefined;
        
        @JsonProperty("gcsVerbal",String,true)
        gcsVerbal: string = undefined;
        
        @JsonProperty("gcsEyes",String,true)
        gcsEyes: string = undefined;
        
        @JsonProperty("sedated",Boolean,true)
        sedated: boolean = undefined;
         
        @JsonProperty("pupils",String,true)
        pupils: string = undefined;
        
        @JsonProperty("airway",String,true)
        airway: string = undefined;

        @JsonProperty("positiveInhalation",Boolean,true)
        positiveInhalation: boolean = undefined;

        @JsonProperty("intubationFailed",Boolean,true)
        intubationFailed: boolean = undefined;

        @JsonProperty("chestTube",String,true)
        chestTube: string = undefined;

        @JsonProperty("oxygenPercentage",Number,true)
        oxygenPercentage: number = undefined;

        @JsonProperty("hemorrhage",Boolean,true)
        hemorrhage: boolean = undefined;

        @JsonProperty("limbsFracture",Boolean,true)        
        limbsFracture: boolean = undefined;

        @JsonProperty("fractureExposition",Boolean,true)        
        fractureExposition: boolean = undefined;

        @JsonProperty("burn",String,true)
        burn: string = undefined;

    computeGCSTotal() {
      if (this.gcsMotor !== undefined && this.gcsEyes !== undefined && this.gcsVerbal !== undefined) {
        const v1 = this.gcsMotorOptions.indexOf(this.gcsMotor);
        const v2 = this.gcsVerbalOptions.indexOf(this.gcsVerbal);
        const v3 = this.gcsEyesOptions.indexOf(this.gcsEyes);
        this.gcsTotal = (6 - v1) + (5 - v2) + (4 - v3);
      }
    }

}

@JsonObject("PatientInitialCondition")
export class PatientInitialCondition {

  @JsonProperty("vitalSigns",VitalSigns,true)
  vitalSigns: VitalSigns = undefined

  @JsonProperty("clinicalPicture", ClinicalPicture, true)
  clinicalPicture: ClinicalPicture = undefined;

}

@JsonObject("PreH")
export class PreH {
    @JsonProperty("territorialArea",String,true)        
    territorialArea: string = undefined;
    
    @JsonProperty("isCarAccident",Boolean,true)        
    isCarAccident: boolean = undefined;
    
    @JsonProperty("aAirways",String,true)        
    aAirways: string = undefined;
    
    @JsonProperty("bPleuralDecompression",Boolean,true)        
    bPleuralDecompression: boolean = undefined;
    
    @JsonProperty("cBloodProtocol",Boolean,true)        
    cBloodProtocol: boolean = undefined;    
    
    @JsonProperty("cTpod",Boolean,true)        
    cTpod: boolean = undefined;
    
    @JsonProperty("dGcsTotal",Number,true)        
    dGcsTotal: number = undefined;
    
    @JsonProperty("dAnisocoria",Boolean,true)        
    dAnisocoria: boolean = undefined;
    
    @JsonProperty("dMidriasi",Boolean,true)        
    dMidriasi: boolean = undefined;
    
    @JsonProperty("eMotility",Boolean,true)        
    eMotility: boolean = undefined;
    
    @JsonProperty("worstBloodPressure",Number,true)        
    worstBloodPressure: number = undefined;
    
    @JsonProperty("worstRespiratoryRate",Number,true)        
    worstRespiratoryRate: number = undefined;
}

@JsonObject("ISSHeadGroup")
export class ISSHeadGroup {
    @JsonProperty("groupTotalIss",Number,true)        
    groupTotalIss: number = undefined;
    
    @JsonProperty("headNeckAis",Number,true)        
    headNeckAis: number = undefined;
    
    @JsonProperty("brainAis",Number,true)        
    brainAis: number = undefined;
    
    @JsonProperty("cervicalSpineAis",Number,true)            
    cervicalSpineAis: number = undefined; 

    /* 'CenterTBI' ISS type */

    @JsonProperty("faceAis",Number,true)            
    faceAis: number = undefined; 

    hasMaxAIS(): boolean {
      if(this.faceAis != undefined){
        return this.headNeckAis === 6 || this.brainAis === 6 || this.cervicalSpineAis === 6 || this.faceAis === 6;
      }

      return this.headNeckAis === 6 || this.brainAis === 6 || this.cervicalSpineAis === 6;
    }
  
    computeTotal() {
      if(this.faceAis != undefined){
        const max = Math.max(this.headNeckAis, this.brainAis, this.cervicalSpineAis, this.faceAis);
        this.groupTotalIss = max*max;
      } else {
        const max = Math.max(this.headNeckAis, this.brainAis, this.cervicalSpineAis);
        this.groupTotalIss = max*max;
      }
      
    }

}

@JsonObject("ISSToraxGroup")
export class ISSToraxGroup {
    @JsonProperty("groupTotalIss",Number,true)        
    groupTotalIss: number = undefined;
    
    @JsonProperty("toraxAis",Number,true)        
    toraxAis: number = undefined;
    
    @JsonProperty("spineAis",Number,true)        
    spineAis: number = undefined;

    hasMaxAIS(): boolean {
      return this.toraxAis === 6 || this.spineAis === 6;
    }

    computeTotal() {
      const max = Math.max(this.toraxAis, this.spineAis);
       this.groupTotalIss = max*max;
    }

}

@JsonObject("ISSAbdomenGroup")
export class ISSAbdomenGroup {
    @JsonProperty("groupTotalIss",Number,true)        
    groupTotalIss: number = undefined;
    
    @JsonProperty("abdomenAis",Number,true)        
    abdomenAis: number = undefined;
    
    @JsonProperty("lumbarSpineAis",Number,true)        
    lumbarSpineAis: number = undefined;
  
    hasMaxAIS(): boolean {
      return this.abdomenAis === 6 || this.lumbarSpineAis === 6;
    }

    computeTotal() {
      const max = Math.max(this.abdomenAis, this.lumbarSpineAis);
      this.groupTotalIss = max*max;
    }
  
}

@JsonObject("ISSExtremitiesGroup")
export class ISSExtremitiesGroup {
    @JsonProperty("groupTotalIss",Number,true)
    groupTotalIss: number = undefined;
    
    @JsonProperty("upperExtremitiesAis",Number,true)
    upperExtremitiesAis: number = undefined;
    
    @JsonProperty("lowerExtremitiesAis",Number,true)
    lowerExtremitiesAis: number = undefined;
    
    /* 'Default' ISS type */

    @JsonProperty("pelvicGirdleAis",Number,true)
    pelvicGirdleAis: number = undefined;

    hasMaxAIS(): boolean {
      if(this.pelvicGirdleAis != undefined){
        return this.lowerExtremitiesAis === 6  || this.upperExtremitiesAis === 6 || this.pelvicGirdleAis === 6;
      }

      return this.lowerExtremitiesAis === 6 || this.upperExtremitiesAis === 6;
    }

    computeTotal() {
      if(this.pelvicGirdleAis != undefined){
        const max = Math.max(this.upperExtremitiesAis, this.lowerExtremitiesAis, this.pelvicGirdleAis);
        this.groupTotalIss = max*max;
      } else {
        const max = Math.max(this.upperExtremitiesAis, this.lowerExtremitiesAis);
        this.groupTotalIss = max*max;
      }
    }

}

@JsonObject("ISSExternaGroup")
export class ISSExternaGroup {
    @JsonProperty("groupTotalIss",Number,true)        
    groupTotalIss: number = undefined;
    
    @JsonProperty("externaAis",Number,true)        
    externaAis: number = undefined;

    hasMaxAIS(): boolean {
      return this.externaAis === 6;
    }
    
    computeTotal() {
      this.groupTotalIss = this.externaAis;
    }

}

@JsonObject("ISSFaceGroup")
export class ISSFaceGroup {
    @JsonProperty("groupTotalIss",Number,true)        
    groupTotalIss: number = undefined;
    
    @JsonProperty("faceAis",Number,true)        
    faceAis: number = undefined; 
  
    hasMaxAIS(): boolean {
      return this.faceAis === 6;
    }
  
    computeTotal() {
      this.groupTotalIss = this.faceAis;
    }
}

@JsonObject("ISSPelvicGirdleGroup")
export class ISSPelvicGirdleGroup {
    @JsonProperty("groupTotalIss",Number,true)        
    groupTotalIss: number = undefined;
    
    @JsonProperty("pelvicGirdleAis",Number,true)        
    pelvicGirdleAis: number = undefined; 
  
    hasMaxAIS(): boolean {
      return this.pelvicGirdleAis === 6;
    }
  
    computeTotal() {
      this.groupTotalIss = this.pelvicGirdleAis;
    }
}



@JsonObject("ISSAISMap")
export class ISSAISMap{

  /* all ISS types*/
  @JsonProperty("headGroup",ISSHeadGroup,true)        
  headGroup: ISSHeadGroup = undefined;

  @JsonProperty("toraxGroup",ISSToraxGroup,true)        
  toraxGroup: ISSToraxGroup = undefined;

  @JsonProperty("abdomenGroup",ISSAbdomenGroup,true)        
  abdomenGroup: ISSAbdomenGroup = undefined;
  
  @JsonProperty("extremitiesGroup",ISSExtremitiesGroup,true)        
  extremitiesGroup: ISSExtremitiesGroup = undefined;

  @JsonProperty("externaGroup",ISSExternaGroup,true)        
  externaGroup: ISSExternaGroup = undefined;

  /* 'Default' ISS type */
    
  @JsonProperty("faceGroup",ISSFaceGroup,true)        
  faceGroup: ISSFaceGroup = undefined;
  
  /* 'CenterTBI' ISS type */
    
  @JsonProperty("pelvicGirdleGroup",ISSPelvicGirdleGroup,true)        
  pelvicGirdleGroup: ISSPelvicGirdleGroup = undefined;
}

@JsonObject("ISS")
export class ISS {
    @JsonProperty("isPresumed",Boolean,true)        
    isPresumed: boolean = undefined;
    
    @JsonProperty("totalIss",Number,true)        
    totalIss: number = undefined;

    @JsonProperty("aisMapType",String,true)        
    aisMapType: string = undefined;
    
    @JsonProperty("aisMap",ISSAISMap,true)        
    aisMap: ISSAISMap = undefined;
  
    computeTotal() {
      if(this.aisMapType === 'Default'){
        this.aisMap.headGroup.computeTotal();
        this.aisMap.faceGroup.computeTotal();
        this.aisMap.toraxGroup.computeTotal();
        this.aisMap.abdomenGroup.computeTotal();
        this.aisMap.extremitiesGroup.computeTotal();
        this.aisMap.externaGroup.computeTotal();
        
        const hasMaxAIS = this.aisMap.headGroup.hasMaxAIS() || this.aisMap.faceGroup.hasMaxAIS() || this.aisMap.toraxGroup.hasMaxAIS() ||
                      this.aisMap.abdomenGroup.hasMaxAIS() || this.aisMap.externaGroup.hasMaxAIS() || this.aisMap.extremitiesGroup.hasMaxAIS();
            
        if (hasMaxAIS) {
          this.totalIss = 75;
        } else {
          const values = [this.aisMap.headGroup.groupTotalIss, this.aisMap.faceGroup.groupTotalIss,
                          this.aisMap.toraxGroup.groupTotalIss, this.aisMap.abdomenGroup.groupTotalIss,
                          this.aisMap.extremitiesGroup.groupTotalIss, this.aisMap.externaGroup.groupTotalIss];
          values.sort();
          this.totalIss = values[5] + values[4] + values[3];
        }
      }

      if(this.aisMapType === 'CenterTBI'){
        this.aisMap.externaGroup.computeTotal();
        this.aisMap.headGroup.computeTotal();
        this.aisMap.toraxGroup.computeTotal();
        this.aisMap.abdomenGroup.computeTotal();
        this.aisMap.pelvicGirdleGroup.computeTotal();
        this.aisMap.extremitiesGroup.computeTotal();
        
        const hasMaxAIS = this.aisMap.externaGroup.hasMaxAIS() || this.aisMap.headGroup.hasMaxAIS() || this.aisMap.toraxGroup.hasMaxAIS() ||
                      this.aisMap.abdomenGroup.hasMaxAIS() || this.aisMap.pelvicGirdleGroup.hasMaxAIS() || this.aisMap.extremitiesGroup.hasMaxAIS();
            
        if (hasMaxAIS) {
          this.totalIss = 75;
        } else {
          const values = [this.aisMap.externaGroup.groupTotalIss, this.aisMap.headGroup.groupTotalIss,
                          this.aisMap.toraxGroup.groupTotalIss, this.aisMap.abdomenGroup.groupTotalIss,
                          this.aisMap.pelvicGirdleGroup.groupTotalIss, this.aisMap.extremitiesGroup.groupTotalIss];
          values.sort();
          this.totalIss = values[5] + values[4] + values[3];
        }
      }
    }
}

@JsonObject("EventContent")
export class EventContent {
}

@JsonObject("ProcEventContent")
export class ProcEventContent extends EventContent {
    @JsonProperty("procedureId",String,true)        
    procedureId: string = undefined;
    
    @JsonProperty("procedureDescription",String,true)        
    procedureDescription: string = undefined;
    
    @JsonProperty("procedureType",String,true)        
    procedureType: string = undefined;
    
    @JsonProperty("event",String,true)        
    event: string = undefined;

    /* if procedureId is Intubation */
  
    @JsonProperty("difficultAirway", Boolean,true)        
    difficultAirway: boolean = undefined;
    
    @JsonProperty("inhalation",Boolean,true)        
    inhalation: boolean = undefined;
    
    @JsonProperty("videolaringo",Boolean,true)        
    videolaringo: boolean = undefined;
    
    @JsonProperty("frova",Boolean,true)        
    frova: boolean = undefined;

    /* procedureId is drenage or chest tube */
  
    @JsonProperty("right",Boolean,true)        
    right: boolean = undefined;
    
    @JsonProperty("left",Boolean,true)        
    left: boolean = undefined;
  
    /* if proc id is hemo */
    /*
    @JsonProperty("epistat",String,true)        
    epistat: string = undefined;
    
    @JsonProperty("suture",String,true)        
    suture: string = undefined;
    
    @JsonProperty("compression",String,true)        
    compression: string = undefined;
    /*
    /* if proc id is ALR */
    /*
    @JsonProperty("upper",String,true)        
    upper: string = undefined;
    
    @JsonProperty("lower",String,true)        
    lower: string = undefined;
    */
}

@JsonObject("DiagEventContent")
export class DiagEventContent extends EventContent {
    @JsonProperty("diagnosticId",String,true)        
    diagnosticId: string = undefined;
    
    @JsonProperty("diagnosticDescription",String,true)        
    diagnosticDescription: string = undefined;

    /* if diag id is skel rx */
    /*
    @JsonProperty("topLeft",String,true)        
    topLeft: string = undefined;
    
    @JsonProperty("topRight",String,true)            
    topRight: string = undefined;
  
    @JsonProperty("bottomLeft",String,true)        
    bottomLeft: string = undefined;
  
    @JsonProperty("bottomRight",String,true)        
    bottomRight: string = undefined;
    */
  
    /* if diag id is abg */
    
    @JsonProperty("lactates",Number,true)        
    lactates: number = undefined;  
    
    @JsonProperty("be",Number,true)        
    be: number = undefined;
    
    @JsonProperty("ph",Number,true)        
    ph: number = undefined;
    
    @JsonProperty("hb",Number,true)        
    hb: number | null = undefined;
    
    /*
    @JsonProperty("glycemia",String,true)        
    glycemia: string = undefined;
    
    @JsonProperty("pco2",String,true)        
    pco2: string = undefined;
    
    @JsonProperty("po2",String,true)        
    po2: string = undefined;
    */
    /* if diag id is rotem */
    
    @JsonProperty("fibtem",String,true)        
    fibtem: string = undefined;
    
    @JsonProperty("extem",String,true)        
    extem: string = undefined;
    
    @JsonProperty("hyperfibrinolysis",Boolean,true)        
    hyperfibrinolysis: boolean = undefined;
}

@JsonObject("DrugEventContent")
export class DrugEventContent extends EventContent {
    
    @JsonProperty("drugId",String,true)        
    drugId: string = undefined;
    
    @JsonProperty("drugDescription",String,true)        
    drugDescription: string = undefined;
  
    @JsonProperty("administrationType",String,true)        
    administrationType: string = undefined;

    @JsonProperty("event",String,true)        
    event: string = undefined;

    @JsonProperty("qty", Number,true)        
    qty:  number = undefined;

    @JsonProperty("unit",String,true)        
    unit: string = undefined;
  
    @JsonProperty("duration",Number,true)        
    duration: number = undefined;
}

@JsonObject("BloodProductEventContent")
export class BloodProductEventContent extends EventContent {
    
    @JsonProperty("bloodProductId",String,true)        
    bloodProductId: string = undefined;
    
    @JsonProperty("bloodProductDescription",String,true)        
    bloodProductDescription: string = undefined;
  
    @JsonProperty("administrationType",String,true)        
    administrationType: string = undefined;

    @JsonProperty("qty", Number,true)        
    qty:  number = undefined;

    @JsonProperty("unit",String,true)        
    unit: string = undefined;
  
    @JsonProperty("bagCode",String,true)        
    bagCode: string = undefined;
}


@JsonObject("ClinicalVariationEventContent")
export class ClinicalVariationEventContent extends EventContent {
    @JsonProperty("variationId",String,true)        
    variationId: string = undefined;
    
    @JsonProperty("variationDescription",String,true)        
    variationDescription: string = undefined;
    
    @JsonProperty("value",String,true)        
    value: string = undefined;  
}

@JsonObject("VitalSignsMonEventContent")
export class VitalSignsMonEventContent extends EventContent {
    @JsonProperty("Temp",Number,true)        
    Temp: number = undefined;
    
    @JsonProperty("HR",Number,true)        
    HR: number = undefined;
    
    @JsonProperty("DIA",Number,true)        
    DIA: number = undefined;
    
    @JsonProperty("SYS",Number,true)        
    SYS: number = undefined;
    
    @JsonProperty("SpO2",Number,true)        
    SpO2: number = undefined;
    
    @JsonProperty("EtCO2",Number,true)        
    EtCO2: number = undefined;
}

@JsonObject("TextNoteEventContent")
export class TextNoteEventContent extends EventContent {
    @JsonProperty("text",String,true)        
    text: string = undefined;
}

@JsonObject("TraumaLeaderEventContent")
export class TraumaLeaderEventContent extends EventContent {
    @JsonProperty("userId",String,true)        
    userId: string = undefined;
    
    @JsonProperty("name",String,true)        
    name: string = undefined;
  
    @JsonProperty("surname",String,true)        
    surname: string = undefined;
}

@JsonObject("RoomEventContent")
export class RoomEventContent extends EventContent {
    @JsonProperty("place",String,true)        
    place: string = undefined;
}

@JsonConverter
export class EventContentConverter implements JsonCustomConvert<EventContent> {
    serialize(ev: EventContent): any {
        return undefined;
    }
  
    deserialize(evContentObj: any): EventContent {
       const jsonConvert: JsonConvert = new JsonConvert();
       const props = Object.keys(evContentObj);
          try {
            if (props.indexOf('procedureId') != -1){
              return jsonConvert.deserialize(evContentObj,ProcEventContent)
            } else if (props.indexOf('diagnosticId') != -1){
              return jsonConvert.deserialize(evContentObj,DiagEventContent)         
            } else if (props.indexOf('drugId') != -1){
              return jsonConvert.deserialize(evContentObj,DrugEventContent)         
            } else if (props.indexOf('bloodProductId') != -1){
              return jsonConvert.deserialize(evContentObj,BloodProductEventContent)         
            } else if (props.indexOf('variationId') != -1){
              return jsonConvert.deserialize(evContentObj,ClinicalVariationEventContent)         
            } else if (props.indexOf('Temp') != -1){
              return jsonConvert.deserialize(evContentObj,VitalSignsMonEventContent)         
            } else if (props.indexOf('text') != -1){
              return jsonConvert.deserialize(evContentObj,TextNoteEventContent)         
            } else if (props.indexOf('userId') != -1){
              return jsonConvert.deserialize(evContentObj,TraumaLeaderEventContent)         
            } else if (props.indexOf('place') != -1){
              return jsonConvert.deserialize(evContentObj,RoomEventContent)         
            } else {
              return <any>{}; 
            }
         } catch (e){
            console.log(e);
         }
         return <any>{}; 
    }
}

@JsonObject("Event")
export class Event {
    @JsonProperty("eventId",Number,true)        
    eventId: number = undefined;
    
    @JsonProperty("date",String,true)        
    date: string = undefined;
    
    @JsonProperty("time",String,true)        
    time: string = undefined;
    
    @JsonProperty("place",String,true)        
    place: string = undefined;      
    
    @JsonProperty("type",String,true)        
    evType: string = undefined;
    
    @JsonProperty("content",EventContentConverter,true)        
    content: EventContent = undefined;
}

@JsonObject("DelayedActivationInfo")
export class DelayedActivationInfo {

    @JsonProperty("isDelayedActivation", Boolean, true)
    isDelayedActivation: boolean = undefined;

    @JsonProperty("originalAccessDate", String, true)
    originalAccessDate: string = undefined;

    @JsonProperty("originalAccessTime", String, true)
    originalAccessTime: string = undefined;
}

@JsonObject("VitalSignMeasurement")
export class VitalSignMeasurement {

  @JsonProperty("type", String, true)
  vsType: String = undefined;

  @JsonProperty("value", String, true)
  value: String = undefined;

  @JsonProperty("uom", String, true)
  uom: String = undefined;    
}

@JsonObject("VitalSignsObservation")
export class VitalSignsObservation {
    
    @JsonProperty("timestamp", String, true)
    timestamp: String = undefined;
  
    @JsonProperty("source", String, true)
    source: String = undefined;

    @JsonProperty("vitalsigns", [VitalSignMeasurement], true)
    vitalsigns: VitalSignMeasurement[] = undefined;
    
}


@JsonObject("Report")
export class Report {
    
    @JsonProperty("_id",String,true)
    _id: string = undefined;
    
    @JsonProperty("_version",String,true)
    _version: string = undefined;
    
    @JsonProperty("_validation",Validation,true)
    _validation: Validation = undefined;
    
    @JsonProperty("startOperatorId",String,true)
    startOperatorId: string = undefined;
    
    @JsonProperty("startOperatorDescription",String,true)
    startOperatorDescription: string = undefined;
    
    @JsonProperty("delayedActivation",DelayedActivationInfo, true)
    delayedActivation: DelayedActivationInfo = undefined;
    
    @JsonProperty("traumaTeamMembers",[String],true)
    traumaTeamMembers: string[] = undefined;
    
    @JsonProperty("startDate",String,true)
    startDate: string = undefined;
    
    @JsonProperty("startTime",String,true)
    startTime: string = undefined;
    
    @JsonProperty("endDate",String,true)
    endDate: string = undefined;
    
    @JsonProperty("endTime",String,true)
    endTime: string = undefined;
        
    @JsonProperty("iss",ISS,true)
    iss: ISS = undefined;
    
    @JsonProperty("finalDestination",String,true)
    finalDestination: string = undefined;
    
    @JsonProperty("traumaInfo", TraumaInfo, true)
    traumaInfo: TraumaInfo = undefined;
    
    @JsonProperty("majorTraumaCriteria", MajorTraumaCriteria, true)
    majorTraumaCriteria: MajorTraumaCriteria = undefined;
    
    @JsonProperty("anamnesi", Anamnesi, true)
    anamnesi: Anamnesi = undefined;
    
    @JsonProperty("preh",PreH,true)
    preH: PreH = undefined;
    
    @JsonProperty("patientInitialCondition", PatientInitialCondition, true)
    patientInitialCondition: PatientInitialCondition = undefined;
    
    @JsonProperty("events", [Event], true)
    events: Event[] = undefined;

    @JsonProperty("vitalSignsObservations", [VitalSignsObservation], true)
    vitalSignsObservations: VitalSignsObservation[] = undefined;
  
}






