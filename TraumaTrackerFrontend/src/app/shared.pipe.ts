import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'filterTime'})
export class FilterTimePipe implements PipeTransform {
  /* HH:MM:SS" */
  transform(timestamp: string): string {
    if (timestamp !== undefined) {
      return timestamp.substring(0, 5);
    } else {
      return 'undefined';
    }
  }
}

@Pipe( { name: 'isValidValue' } )
export class IsValidValuePipe implements PipeTransform {
    transform( value ): boolean {
        return value !== undefined; // && value != null && value !== '';
    }
}

@Pipe( { name: 'getYesNoValue' } )
export class GetYesNoValuePipe implements PipeTransform {
    transform(value: boolean): string {
        if (value != null) {
          return value ? 'sì' : 'no';
        } else {
          return '';
        }
    }
}

@Pipe( { name: 'getValueOrDash' } )
export class GetValueOrDashPipe implements PipeTransform {
    transform( value ): boolean {
        return value !== undefined && value !== '' ? value : '-';
    }
}

