import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { AppStateService } from '../shared/app-state.service';

@Component( {
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
} )
export class Dashboard implements OnInit, OnDestroy {

    private isRecentRepActive: boolean = false;
    private isReportToShow: boolean = false;
  
    private isFindRepActive: boolean = false;
    private isUsersActive: boolean = false;
    private isStatActive: boolean = false;
    private isAdminActive: boolean = false;
    private wasFindRepActive: boolean = false;
    
    private subscriber: Subscription;

    constructor(
        private appStateService: AppStateService      
    ) { }
  
    ngOnInit(): void {
        this.subscriber = this.appStateService.appEventChannel$.subscribe((event) => {
          if (event.eventType == 'report-selected'){
            if (this.isFindRepActive){
              this.wasFindRepActive = true;
            } else {
              this.wasFindRepActive = false;
            }
            this.isRecentRepActive = false;
            this.isFindRepActive = false;
            this.isReportToShow = true;
          } else if (event.eventType == 'report-unselected'){
             this.isReportToShow = false;
           if (this.wasFindRepActive){
              this.setUnactive();
              this.isFindRepActive = true;
            } else {
              this.setUnactive();
              this.isRecentRepActive = true;
            }  
          }
        });
      
        this.setUnactive();
        this.isRecentRepActive = true;
    }

    ngOnDestroy() {
        if ( this.subscriber ) {
            this.subscriber.unsubscribe();
        }
    }

    onRecentRepSelect() {
        this.setUnactive();
        this.isRecentRepActive = true;
    }

    onFindRepSelect() {
        this.setUnactive();
        this.isFindRepActive = true;
    }

    onUsersSelect() {
        this.setUnactive();
        this.isUsersActive = true;
    }

    onStatSelect() {
        this.setUnactive();
        this.isStatActive = true;
    }

    onAdminSelect() {
        this.setUnactive();
        this.isAdminActive = true;
    }

    canUserAdmin(): boolean {
      return this.appStateService.canUserAdmin();
    }

    canUserManageUsers(): boolean {
      return this.appStateService.canUserManageUsers();
    }
    
    canUserManageData(): boolean {
      return this.appStateService.canUserManageData();
    }

    private setUnactive() {
        this.isRecentRepActive = false;
        this.isFindRepActive = false;
        this.isUsersActive = false;
        this.isStatActive = false;
        this.isAdminActive = false;
        this.appStateService.notifyChangedTab();
    }

}
