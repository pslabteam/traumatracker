import 'rxjs/add/operator/switchMap';

import { Component, OnInit, OnDestroy } from '@angular/core';

import { ReportService } from '../shared/report.service';
import { AppStateService } from '../shared/app-state.service';
import { Subscription } from "rxjs/Subscription";
// import { StoreSearchParametersService } from '../store-search-parameters.service';


@Component( {
    selector: 'find-report',
    templateUrl: './find-report.component.html',
    providers: [ReportService],
    // styleUrls: ['./find-report.component.css']
} )
export class FindReportComponent  {
    
    reportsFound;
    startDate;
    endDate;
    operator: string;
    iss: string;

  
    constructor(
        private reportService: ReportService,
        private appStateService: AppStateService,
    ) { }
  
    findReports() {
        // console.log( 'searching reports with date: ' + this.reportToSearchDate + ' and operatorId: ' + this.reportToSearchOperator );
        this.reportService.findReports( this.startDate, this.endDate, this.operator, this.iss )
            .then( response => this.reportsFound = response );
    }
}
