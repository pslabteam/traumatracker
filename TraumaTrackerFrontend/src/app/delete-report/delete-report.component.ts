import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from "rxjs/Subscription";
import { AppStateService } from '../shared/app-state.service';
import { ReportService } from '../shared/report.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';

@Component( {
    selector: 'delete-report',
    templateUrl: './delete-report.component.html',
    providers: [
      /*PrintReportService */],
} )
export class DeleteReportComponent implements OnInit, OnDestroy {

    private reportSub: Subscription;
    private selectedReport;
    private deleteRepModal: NgbModalRef;
    @ViewChild('deleteReportModal') private content;

    constructor(
        private appStateService: AppStateService,
        private reportService: ReportService, 
        private modalService: NgbModal, 
    ) { }

    ngOnInit(): void {
        this.reportSub = this.appStateService.appEventChannel$.subscribe((event) => {
          if (event.eventType == 'report-to-delete'){
              this.selectedReport = event.report;
              this.deleteRepModal = this.modalService.open(this.content, {
                backdrop: 'static',
                keyboard: false
            });      
          } 
        });
    }
  
    ngOnDestroy(){
        if ( this.reportSub !== undefined ) {
            this.reportSub.unsubscribe();
        }
    } 
    
    deleteReport() {
        this.reportService.deleteReport( this.selectedReport._id )
        .then(response => {
            if(response === 'Report deleted'){
                // this.showModalService.notifyToRefreshReport();
                console.log( 'Deleted report with id ' + this.selectedReport._id );
                // Notification.success({message: 'Report '+ report._id + ' cancellato con successo.', delay: 2000});
            }
        });
        this.deleteRepModal.close();
        this.appStateService.notifyReportUnselected();
    }
    
    dismissDeleteDialog(){
      this.deleteRepModal.close();
      this.appStateService.notifyReportUnselected();
   }
    
    backToMain(): void {
        this.appStateService.notifyReportUnselected();
    }
}
