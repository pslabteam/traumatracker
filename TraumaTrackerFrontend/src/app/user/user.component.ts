import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AppStateService } from '../shared/app-state.service';
import { NgbModal, NgbModalRef, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { UserService } from '../shared/user.service';
import { User } from '../data-model/user';


@Component( {
    selector: 'user',
    templateUrl: './user.component.html',
    providers: [
    ]
} )
export class UserComponent implements OnInit, OnDestroy {

    name: string;
    surname: string;
    password: string;
    email: string;
    role: string;
    userId: string;
  
    addUserDialog: NgbModalRef;
    updateUserDialog: NgbModalRef;
    deleteUserDialog: NgbModalRef;
    newUserReviewDialog: NgbModalRef;
    selectedUser: User;

    private users;
    areUsersLoaded = false;
    private userRefreshSub: Subscription;

    @ViewChild('addUserModal') private addUserModal;
    @ViewChild('updateUserModal') private updateUserModal;
    @ViewChild('deleteUserModal') private deleteUserModal;
    @ViewChild('newUserReviewModal') private newUserReviewModal;

    @Input()
    set onUserPillClick(val: boolean){
        this.getUsers();
    }

    constructor(
        private appStateService: AppStateService,
        private userService: UserService,
        private modalService: NgbModal
    ) {
    }

    ngOnInit(): void {
        this.getUsers();
        /*
        this.userRefreshSub = this.appStateService.isUserRefreshToBeDone$.subscribe(() => {
            this.getUsers();
        } );
         */
    }

    ngOnDestroy(): void {
        /*
        if ( this.userRefreshSub ) {
            this.userRefreshSub.unsubscribe();
        }*/
    }

    addUser() {
        if (this.name === undefined || this.name === "" 
                || this.surname === undefined || this.surname === "" 
                || this.email === undefined || this.email === ""
                || this.role === undefined || this.role === ""
                || this.password === undefined || this.password === ""){
            return;
        } else {
            const user = new User();
            user.email = this.email;
            user.role = this.role;
            user.name = this.name;
            user.surname = this.surname;
            user.pwd = this.password;
    
            this.userService.addUser( user )
              .then(response => {
                  this.getUsers();
                  this.addUserDialog.close();
                  this.userId = response.user.userId;
                  this.newUserReviewDialog = this.modalService.open(this.newUserReviewModal, {
                    backdrop: 'static',
                    keyboard: false
                });
    
              })
              .catch(error => {
                  console.log( error );
                  this.addUserDialog.close();
               });
        }
    }

    updateUser(){
        /* NOTA: se non si specifica una nuova password, il relativo campo non sarà inviato al backend, 
                 ma sarà premura di quest'ultimo mantenere la precedente password
                 (in quanto la password non è mai retropropagata all'applicazione).
        */

        this.selectedUser.pwd = this.password;
        this.selectedUser.role = this.role;

        this.userService.updateUser(this.selectedUser)
            .then(response => {
                this.getUsers();
                this.updateUserDialog.close();
            }).catch(error => {
                console.log( error );
                this.updateUserDialog.close();
            });
    }

    deleteUser() {
       this.userService.deleteUser(this.selectedUser.userId)
          .then(response => {
              this.getUsers();
              this.deleteUserDialog.close();
          })
          .catch(error => {
              console.log( error );
              this.deleteUserDialog.close();
           });
    }

    getUsers() {
        this.userService.getAllUsers().then( response => {
            this.users = response;
            this.areUsersLoaded = true;
        } );
    }

    openAddUserDialog() {
        this.name = '';
        this.surname = '';
        this.password = '';
        this.email = '';
        this.role = '';
        this.userId = '';
        this.addUserDialog = this.modalService.open(this.addUserModal, {
            backdrop: 'static',
            keyboard: false
        });
    }

    openUpdateUserDialog(user) {
        this.selectedUser = user;
        
        this.name = user.name;
        this.surname = user.surname;
        this.email = user.email;
        this.userId = user.userId;
        this.role = user.role;
        this.password = user.pwd;

        this.updateUserDialog = this.modalService.open(this.updateUserModal, {
            backdrop: 'static',
            keyboard: false
        });

    }

    openDeleteUserDialog(user) {
        this.selectedUser = user;
        this.deleteUserDialog = this.modalService.open(this.deleteUserModal, {
            backdrop: 'static',
            keyboard: false
        });
    }

    dismissAddUserDialog() {
        this.addUserDialog.close();
    }

    dismissUpdateUserDialog() {
        this.updateUserDialog.close();
    }

    dismissDeleteUserDialog() {
        this.deleteUserDialog.close();
    }

    dismissNewUserReviewDialog(){
        this.newUserReviewDialog.close();
    }

    nameOrSurnameChanged(){
        this.userId = this.name.toLowerCase( ) + "." + this.surname.toLowerCase( );
    }
}
