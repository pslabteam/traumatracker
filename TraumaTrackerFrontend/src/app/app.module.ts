import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppStateComponent } from './app-state/app-state.component';

import { Dashboard } from './dashboard/dashboard.component';

import { RecentReportsComponent } from './recent-reports/recent-reports.component';
import { ReportCompactDetailsComponent } from './report-compact-details/report-compact-details.component';
import { FilterTimePipe, IsValidValuePipe, GetValueOrDashPipe, GetYesNoValuePipe } from './shared.pipe';

import { DataAnalysisComponent } from './data-analysis/data-analysis.component';
import { FindReportComponent } from './find-report/find-report.component';
import { DeleteReportComponent } from './delete-report/delete-report.component';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';

import { ViewReportComponent } from './view-report/view-report.component';

import { GeneralInfoComponent, FilterOperatorNamePipe, FilterTeamMembersPipe  } from './view-report/general-info/general-info.component';
import { TraumaInfoComponent } from './view-report/trauma-info/trauma-info.component';
import { MajorTraumaCriteriaComponent, IsMajorTraumaCriteriaNotEmptyPipe } from './view-report/major-trauma-criteria/major-trauma-criteria.component';
import { AnamnesiComponent, IsAnamnesyNotEmptyPipe } from './view-report/anamnesi/anamnesi.component';
import { PreHComponent, IsPreHNotEmptyPipe } from './view-report/pre-h/pre-h.component';
import { EventTraceComponent, IsEventPhotoPipe, IsEventAudioPipe, IsEventVideoPipe, FilterEventContentPipe } from './view-report/event-trace/event-trace.component';
import { EventsChartComponent, IsVitalSignsDataNotEmpty } from './view-report/events-chart/events-chart.component';
import { PatientInitialStatusComponent, GetFirsAvailablePipe, IsAirwayNotEmptyPipe, IsPatientInitialConditionNotEmpty,
          IsVitalParamsNotEmptyPipe, IsClinicalPictureNotEmpty, IsNeuroNotEmptyPipe, IsHemorrhageNotEmpty,
          IsOrtopNotEmptyPipe, IsBurnNotEmptyPipe } from './view-report/patient-initial-status/patient-initial-status.component';

import { GetAppVersionService } from './app-state/get-version.service';
import { ReportService } from './shared/report.service';
import { StatService } from './shared/stat.service';
import { AppStateService } from './shared/app-state.service';
import { TraumaTrackerAPIService } from './shared/trauma-tracker-api.service';
import { TraumaStatApiService } from './shared/trauma-stat-api.service';
import { UserService } from './shared/user.service';
import { ExportReportLib } from './shared/export-csv';
import { LoginService } from './shared/login.service';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: Dashboard, canActivate: [AppStateService] },
  { path: 'recent-reports', component: RecentReportsComponent, canActivate: [AppStateService] },
  { path: 'find-report', component: FindReportComponent, canActivate: [AppStateService] },
  { path: 'elab-data', component: DataAnalysisComponent, canActivate: [AppStateService] },
  { path: 'users', component: UserComponent, canActivate: [AppStateService] },
  { path: 'admin', component: AdminComponent, canActivate: [AppStateService] },
  { path: '**', redirectTo: 'login'}
];


@NgModule( {
    declarations: [
        AdminComponent,
        AppComponent,
        AppStateComponent,
        Dashboard,
        DeleteReportComponent,
        FindReportComponent,
        GeneralInfoComponent,
        RecentReportsComponent,
        UserComponent,
        ViewReportComponent,
        DataAnalysisComponent,
        LoginComponent,

        FilterOperatorNamePipe,
        FilterTimePipe,
        FilterTeamMembersPipe,
        FilterEventContentPipe,

        IsEventPhotoPipe,
        IsEventAudioPipe,
        IsEventVideoPipe,
        GetFirsAvailablePipe,
        IsValidValuePipe,
        GetValueOrDashPipe,
        GetYesNoValuePipe,
        IsVitalSignsDataNotEmpty,
  
        TraumaInfoComponent,
        MajorTraumaCriteriaComponent, IsMajorTraumaCriteriaNotEmptyPipe,
        AnamnesiComponent, IsAnamnesyNotEmptyPipe,
        PreHComponent, IsPreHNotEmptyPipe,
        EventTraceComponent,
        EventsChartComponent,
        PatientInitialStatusComponent, IsAirwayNotEmptyPipe, IsVitalParamsNotEmptyPipe,
                    IsNeuroNotEmptyPipe, IsHemorrhageNotEmpty, IsClinicalPictureNotEmpty,
                    IsOrtopNotEmptyPipe, IsBurnNotEmptyPipe, IsPatientInitialConditionNotEmpty,
        ReportCompactDetailsComponent,
    ],
    imports: [
        RouterModule.forRoot(
        appRoutes,
        { enableTracing: true } // <-- debugging purposes only
        ),
        BrowserModule,
        FormsModule,
        HttpClientModule,
        NgbModule
    ],
    providers: [
        AppStateService,
        TraumaTrackerAPIService,
        TraumaStatApiService,
        ReportService,
        GetAppVersionService,
        UserService,
        ExportReportLib,
        LoginService,
        StatService
    ],
    entryComponents: [],

    bootstrap: [AppComponent]
} )
export class AppModule { }
