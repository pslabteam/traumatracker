package t4c.tt.service.management;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class GetStateHandler extends AbstractServiceHandler {

	public GetStateHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling GetState from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		JsonObject reply = new JsonObject()
							.put("version", getConfig().getVersion())	
							.put("startTime", getStartTime());
		response.putHeader("content-type", "application/json").end(reply.encodePrettily());		
	}	
}
