package t4c.tt.service.management;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class GetVersionHandler extends AbstractServiceHandler {

	public GetVersionHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Get Version from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		JsonObject vers = new JsonObject();
		vers.put("version", this.getConfig().getVersion());		
		response
			.putHeader("content-type", "application/json")
			.end(vers.encodePrettily());
	}	
}
