package t4c.tt.service.sessions;

import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.ontology.TTDataModel;

public class LoginHandler extends AbstractServiceHandler {

	public LoginHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling login from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		JsonObject user = routingContext.getBodyAsJson();
		TTDataModel model = TTDataModel.instance();
		boolean ok = model.checkUserCorrectness(user);
		if (!ok) {
			logError("Malformed user:\n " + routingContext.getBodyAsString());
			sendError(400, response);
		} else {
			try {
				String id = user.getString("userId");
				String pwd = user.getString("pwd");
				
				JsonObject query = new JsonObject().put("userId", id);
				JsonObject info = new JsonObject();
				findInUsers(query, res -> {
					if (res.succeeded()) {
						List<JsonObject> list = res.result();
						if (list.isEmpty()) {
							log("User not found: "+id);
							info.put("result", "invalid userId");
							response.putHeader("content-type", "application/json").end(info.encodePrettily());							
						} else {
							JsonObject userInfo = list.get(0);
							String storedPwd = userInfo.getString("pwd");
							if (pwd.equals(storedPwd)) {
								info.put("result", "ok");
								info.put("role",userInfo.getString("role"));
								info.put("name",userInfo.getString("name"));
								info.put("surname",userInfo.getString("surname"));
								info.put("email",userInfo.getString("email"));
								info.put("token", userInfo.getString("token"));
								response.putHeader("content-type", "application/json").end(info.encodePrettily());
							} else {
								info.put("result", "invalid pwd");
								response.putHeader("content-type", "application/json").end(info.encodePrettily());
							}
						}
					} else {
						log("User not found: "+id);
						info.put("result", "invalid userId");
						response.putHeader("content-type", "application/json").end(info.encodePrettily());							
					}
				});
			} catch (Exception ex) {
				logError("Malformed data logging in:\n " + routingContext.getBodyAsString());
				sendError(400, response);
			}
		}	
	}
	
}
