package t4c.tt.service.sessions;

import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.ontology.TTDataModel;
import t4c.tt.util.MailLib;

import javax.mail.MessagingException;

import io.vertx.core.*;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class EmailPwdHandler extends AbstractServiceHandler {

	public EmailPwdHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling send pwd by email from " + routingContext.request().absoluteURI());
		
		final HttpServerResponse response = routingContext.response();
		final JsonObject user = routingContext.getBodyAsJson();
		
		if (!TTDataModel.instance().checkUserCorrectness(user)) {
			logError("Malformed user:\n " + routingContext.getBodyAsString());
			sendError(400, response);
		} else {
			try {
				final String id = user.getString("userId");
				
				findInUsers(new JsonObject().put("userId", id), res -> {
					if (res.succeeded()) {
						if (res.result().isEmpty()) {
							log("User not found: " + id);
							sendResponse(response, "invalid userId");							
						} else {
							final JsonObject userInfo = res.result().get(0);						
							final String email = userInfo.getString("email");
							
							log("Sending pwd for " + id + " to " + email);
							
							getCore().executeBlocking((Promise<Void> f) -> {
								try {
									MailLib.getInstance().sendEmail("TraumaTracker", 
											email, 
											"[TraumaTracker] Recupero password utente", 
											String.format("Gent.mo %s %s,"
															+"\n"
															+"a seguire le sue credenziali per l'accesso al sistema TraumaTracker."
															+ "\n\n"
															+"Nome Utente: %s"
															+"\n"
															+"Password: %s"
															+"\n\n"
															+"--\nTraumaTracker Team\nPSLab (UniBo, Cesena)", 
													userInfo.getString("name"),
													userInfo.getString("surname"),
													userInfo.getString("_id"),
													userInfo.getString("pwd")));
									log("Email sent.");
									f.complete();
								} catch (final MessagingException ex) {
									f.fail(ex.getMessage());
									ex.printStackTrace();
								}
							}, result -> {
								sendResponse(response, result.succeeded() ? "ok" : "send failed");
							});
						}
					} else {
						log("User not found: "+id);
						sendResponse(response, "invalid userId");						
					}
				});
			} catch (Exception ex) {
				logError("Malformed data logging in:\n " + routingContext.getBodyAsString());
				sendError(400, response);
			}
		}	
	}
	
	private void sendResponse(final HttpServerResponse response, final String result) {
		response.putHeader("content-type", "application/json").end(
			new JsonObject().put("result", result).encodePrettily());	
	}
}
