package t4c.tt.service;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.mongo.MongoClient;
import t4c.tt.service.management.GetVersionHandler;
import t4c.tt.service.reports.*;
import t4c.tt.service.sessions.EmailPwdHandler;
import t4c.tt.service.sessions.LoginHandler;
import t4c.tt.service.users.*;
import t4c.tt.service.util.*;
import t4c.tt.util.MailLib;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class TTService extends AbstractVerticle {

	private static final Logger LOG = LoggerFactory.getLogger(TTService.class.getName());
	
	private TTConfig config;
	
	private MongoClient store;
	
	private Date startTime;
	private HashMap<String,JsonObject> cachedUsers;
	
	private static final String API_BASE_PATH = "/gt2/traumatracker/api";
	private static final String VS_API_BASE_PATH = "/gt2/vsservice/api";

	
	public TTService(final TTConfig config){
		this.config = config;
		this.cachedUsers = new HashMap<String,JsonObject>();
	}
	
	@Override
	public void start(final Promise<Void> startPromise) {		
		final HttpServer server = vertx.createHttpServer().requestHandler(createRouter());
    	
    	final int servicePort = config.getPort();
    			
		server.listen(servicePort, res -> {
			if (res.succeeded()) {
				startTime = new Date();
				LOG.info("Http server is running on port " + servicePort);
				startPromise.complete();
			} else {
				startPromise.fail(res.cause());
			}
		});
		
		initEmailService();
	}
	
	private Router createRouter() {
	    final Router mainRouter = Router.router(vertx);
	    
	    store = MongoClient.create(vertx, new JsonObject()
				.put("host", config.getDBHost())
				.put("port", config.getDBPort())
				.put("db_name", config.getDBName()));
	    	    
	    final Set<String> allowHeaders = new HashSet<>();
		allowHeaders.add("x-requested-with");
		allowHeaders.add("Access-Control-Allow-Origin");
		allowHeaders.add("origin");
		allowHeaders.add("Content-Type");
		allowHeaders.add("accept");
	
		final Set<HttpMethod> allowMethods = new HashSet<>();
		allowMethods.add(HttpMethod.GET);
		allowMethods.add(HttpMethod.POST);
		allowMethods.add(HttpMethod.PUT);
		allowMethods.add(HttpMethod.DELETE);
		allowMethods.add(HttpMethod.OPTIONS);
		
		mainRouter.route().handler(CorsHandler.create("*")
				.allowedHeaders(allowHeaders)
				.allowedMethods(allowMethods));
		
		mainRouter.route().handler(BodyHandler.create());
		
		// management 
		mainRouter.get(API_BASE_PATH + "/version").handler(new GetVersionHandler(this));
		mainRouter.get(API_BASE_PATH + "/state").handler(new GetVersionHandler(this));
		
		// reports
		mainRouter.get(API_BASE_PATH + "/reports").handler(new GetReportsHandler(this));
		mainRouter.post(API_BASE_PATH + "/reports").handler(new AddReportHandler(this));
		mainRouter.delete(API_BASE_PATH + "/reports").handler(new DeleteReportHandler(this));
		mainRouter.put(API_BASE_PATH + "/reports/:reportID").handler(new UpdateReportHandler(this));
		mainRouter.put(API_BASE_PATH + "/reports/:reportID/validation").handler(new ValidateReportHandler(this));
		mainRouter.put(API_BASE_PATH + "/reports/:reportID/iss").handler(new UpdateISSHandler(this));
		mainRouter.put(API_BASE_PATH + "/reports/:reportID/vitalsigns").handler(new UpdateVitalSignsHandler(this));
		mainRouter.put(API_BASE_PATH + "/reports/:reportID/clinicalpicture").handler(new UpdateClinicalPictureHandler(this));
		mainRouter.get(API_BASE_PATH + "/reports/:reportID").handler(new GetReportInfoHandler(this));
		mainRouter.delete(API_BASE_PATH + "/reports/:reportID").handler(new DeleteReportHandler(this));
		mainRouter.delete(API_BASE_PATH + "/reports/:reportID/events/:eventID").handler(new DeleteEventInReportHandler(this));
		
		// users
		mainRouter.get(API_BASE_PATH + "/users").handler(new GetUsersHandler(this));
		mainRouter.get(API_BASE_PATH + "/members").handler(new GetMembersHandler(this));
		mainRouter.post(API_BASE_PATH + "/users").handler(new AddUserHandler(this));
		mainRouter.delete(API_BASE_PATH + "/users/:userId").handler(new DeleteUserHandler(this));
		mainRouter.delete(API_BASE_PATH + "/users").handler(new DeleteAllUsersHandler(this));
		mainRouter.put(API_BASE_PATH+"/users/:userID").handler(new UpdateUserHandler(this));
		
		// setting dir upload for multimedia files
		mainRouter.post("/gt2/traumatracker/photo").handler(new UploadPhotoHandler(this));	
		mainRouter.post("/gt2/traumatracker/video").handler(new UploadVideoHandler(this));	
		mainRouter.post("/gt2/traumatracker/audio").handler(new UploadAudioHandler(this));
		
		// sessions
		mainRouter.post(API_BASE_PATH + "/login").handler(new LoginHandler(this));
		mainRouter.post(API_BASE_PATH + "/login/pwd-recovery").handler(new EmailPwdHandler(this));
		
		mainRouter.route("/gt2/traumatracker/static/*").handler(StaticHandler.create()); 
		
		mainRouter.get("/gt2/traumatracker/static/:fileId").handler(rc -> {
			rc.response().sendFile(new File("").getAbsolutePath()
					+ File.separatorChar
					+ "file-uploads"
					+ File.separatorChar
					+ rc.request().getParam("fileId")).end();
		});
		
		return mainRouter;
	}
	
	private void initEmailService() {
		MailLib.makeInstance(
				config.getSmtpAccountConf().host(),
				config.getSmtpAccountConf().port(),
				config.getSmtpAccountConf().account(),
				config.getSmtpAccountConf().pwd());
	}
	
	
	private void fetchUserInfo(String userId, Future<JsonObject> fut) {
		JsonObject user = cachedUsers.get(userId);
		if (user == null) {
			Future<Void> f = Future.future();
			cacheUsers(f);
			f.setHandler(res -> {
				JsonObject user1 = cachedUsers.get(userId);
				fut.complete(user1);
			});
		}
	}
	
	public void fetchVitalParams(JsonObject rep, Future<JsonObject> fut){
		int vitalSignsServicePort = this.getConfig().getVitalSignsServicePort();
		HttpClient client = vertx.createHttpClient(new HttpClientOptions().setDefaultHost("localhost"/*args[0]*/).setDefaultPort(vitalSignsServicePort));
		String repId = rep.getString("_id");
		client.get(VS_API_BASE_PATH + "/sessions/" + repId).handler(res -> {
			log("vital sign service response status " + res.statusCode());
			res.bodyHandler(res2 -> {
				JsonObject data = res2.toJsonObject();
				fut.complete(data);
			});
		})
		.exceptionHandler(handler -> {
			log("unable to connect to vitalsigns service!");
			fut.fail("connection refused");
		})
		.setTimeout(5000).end();
	}
	
	public void integrateVitalParamsInfo(JsonObject report, Optional<JsonObject> vsInfo) {
		if (vsInfo.isPresent()) {
			JsonArray mes = vsInfo.get().getJsonArray("measurements");
			if (mes != null) {
				report.put("vitalSignsObservations", mes);
			} else {
				report.put("vitalSignsObservations", new JsonArray());				
			}
		} else {
			report.put("vitalSignsObservations", new JsonArray());
		}
	}
	
	private void cacheUsers(Future<Void> fut) {
		JsonObject query = new JsonObject();
		getStore().find(getConfig().getUsersCollectionName(), query, res -> {
			if (res.succeeded()) {
				JsonArray users = new JsonArray();
				for (JsonObject rep : res.result()) {
					users.add(rep);
					cachedUsers.put(rep.getString("userId"), rep);
				}
				log("Cached "+users.size()+" users");
				fut.complete();
			} else {
				fut.fail(res.cause().getMessage());
			}
		});
	}		
	
	public void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}
	
	public MongoClient getStore() {
		return store;		
	}
	
	public void log(String msg) {
		LOG.info("[TTService] "+msg);
	}

	public void logError(String msg) {
		LOG.error("[TTService] "+msg);
	}
	
	public TTConfig getConfig() {
		return config;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	public FileSystem getFS() {
		return vertx.fileSystem();
	}
}
