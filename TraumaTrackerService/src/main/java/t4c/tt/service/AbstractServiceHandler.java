package t4c.tt.service;

import io.vertx.ext.mongo.*;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.util.TTConfig;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.file.FileSystem;
import io.vertx.core.AsyncResult;
import java.util.Date;
import java.util.List;

public abstract class AbstractServiceHandler implements Handler<RoutingContext> {

	TTService service;
	
	public AbstractServiceHandler(TTService service){
		this.service = service;
	}

	abstract public void handle(RoutingContext routingContext);
		
	protected Vertx getCore() {
		return service.getVertx();
	}
	
	protected void sendError(int statusCode, HttpServerResponse response){
		response.setStatusCode(statusCode).end();
	}

	protected void sendErrorNotFound(HttpServerResponse response){
		response.setStatusCode(404).end();
	}

	protected void sendError(int statusCode, JsonObject desc, HttpServerResponse response){
		response.setStatusCode(statusCode).end(desc.encode());
	}

	protected void sendErrorSyntax(JsonObject desc, HttpServerResponse response){
		response.setStatusCode(400).end(desc.encode());
	}

	protected void sendErrorSyntax(HttpServerResponse response){
		response.setStatusCode(400).end();
	}

	protected void sendErrorConflict(JsonObject desc, HttpServerResponse response){
		response.setStatusCode(409).end(desc.encode());
	}

	protected void sendErrorInternal(HttpServerResponse response){
		response.setStatusCode(500).end();
	}
	
	protected void log(String msg){
		service.log(msg);
	}

	protected void logError(String msg){
		service.logError(msg);
	}
	
	protected MongoClient getStore(){
		return service.getStore();
	}
	
	protected TTConfig getConfig(){
		return service.getConfig();
	}
	
	protected Date getStartTime() {
		return service.getStartTime();
	}
	
	protected void findInUsers(JsonObject query, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().find(getConfig().getUsersCollectionName(), query, handler);		
	}

	protected void storeInUsers(JsonObject user, Handler<AsyncResult<String>> handler){
		service.getStore().insert(getConfig().getUsersCollectionName(), user, handler);		
	}

	protected void updateInUsers(JsonObject user, Handler<AsyncResult<String>> handler){
		service.getStore().save(getConfig().getUsersCollectionName(), user, handler);		
	}

	protected void findInReports(JsonObject query, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().find(getConfig().getReportsCollectionName(), query, handler);		
	}

	protected void saveInReports(JsonObject doc, Handler<AsyncResult<String>> handler){
		service.getStore().save(getConfig().getReportsCollectionName(), doc, handler);		
	}
	
	
	protected void findInReportsWithOptions(JsonObject query, JsonObject opt, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().findWithOptions(getConfig().getReportsCollectionName(), query, new FindOptions(opt), handler);		
	}
	
	protected FileSystem getFS() {
		return service.getFS();
	}

	protected TTService getService() {
		return service;
	}

}
