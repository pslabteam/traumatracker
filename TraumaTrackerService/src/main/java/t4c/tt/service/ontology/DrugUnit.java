package t4c.tt.service.ontology;

public enum DrugUnit {
    ML, G, MG, UNITA, POOL
}
