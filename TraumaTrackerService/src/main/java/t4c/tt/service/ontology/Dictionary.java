package t4c.tt.service.ontology;

public interface Dictionary {
	
	String get(DrugAndInfusions term);
	
	String get(VitalSigns term);
	
	String get(ResuscitationManeuvers term);

	String get(Place term);
	
	String get(FinalDestination dest);
	
}
