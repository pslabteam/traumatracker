package t4c.tt.service.ontology;

public enum MedicalTermsType {
    RESUSCITATION_MANEUVERS,
    VITAL_SIGNS,
    DRUG_AND_INFUSIONS
}
