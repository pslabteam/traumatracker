package t4c.tt.service.ontology;

public enum FinalDestination {
	DEPARTMENT,
	INTENSIVE_CARE,
	INTENSIVE_CARE_SUB,
	MORTUARY    
}
