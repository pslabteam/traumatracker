package t4c.tt.service;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import t4c.tt.service.util.InvalidConfigFileException;
import t4c.tt.service.util.TTConfig;

public class Launcher {
	
	private final static Logger LOG = LoggerFactory.getLogger(Launcher.class.getName());
	
	public static void main(final String[] args) {
        final Vertx vertx = Vertx.vertx();
        
        vertx.fileSystem().readFile(getConfigFile(args), e -> {
        	deployTraumaTrackerService(vertx, new JsonObject(e.result()));
        });
    }
	
	private static void deployTraumaTrackerService(final Vertx vertx, final JsonObject configFile) {
      	
		TTConfig config;
    	
    	try {
    		config = new TTConfig(configFile);
		} catch (InvalidConfigFileException e) {
			LOG.error("Config file not found or invalid");
			config = new TTConfig();
		}
    	
		LOG.info("Booting TraumaTracker System....");
		
		TTService ttService = new TTService(config);
		vertx.deployVerticle(ttService);
    }
	
	private static String getConfigFile(final String... params) {
    	if(params.length == 0) {
        	return "config.json";
        } else {      	
        	switch(params[0]) {
        		case "docker": return "config-docker.json";
        		default: return "config.json";
        	}
        }
    }
	
	/*
	private static final String TT_CONFIG_FILE = "./config.json";
	
	public static void main(String[] args) throws Exception {
				
		System.out.println("Booting TraumaTracker System.");

		TTConfig config;
		try {
			String configFile = TT_CONFIG_FILE;
			if (args.length == 1) {
				configFile = args[0];
			}
			config = new TTConfig(configFile);
		} catch (Exception ex){
			System.err.println("Config file not found or invalid");
			config = new TTConfig();
		}
		
		TTLogger logger = new TTLogger(config.isLogGUIEnabled());
		logger.log("Booting TraumaTracker System.");

		// new VPNManagerAgentOSX(logger,config).start();
		// Thread.sleep(2000);

		Vertx vertx = Vertx.vertx();
		
		//MongoDBAgent mongodb = new MongoDBAgent(logger,config);
		//vertx.deployVerticle(mongodb);

		Thread.sleep(5000);

		TTService ttService = new TTService(logger,config);
		vertx.deployVerticle(ttService);

		// TTServiceAgent srvag = new TTServiceAgent(logger,config);
		// vertx.deployVerticle(srvag);
	}
	
	*/
}
