package t4c.tt.service.util;

import io.vertx.core.json.JsonObject;

public class TTConfig {
	
	private JsonObject config;
		
	public TTConfig(final JsonObject configFile) throws InvalidConfigFileException {
		config = configFile;
	}
	
	public TTConfig(){
		
		/*
		 * DEFAULT CONFIGURATION
		 */
		
		config.put("version", "0.0-DEFAULT");
		config.put("numRecentReports", 20);
		
		final JsonObject mongoConf = new JsonObject()
				.put("host", "127.0.0.1")
				.put("port", 27017)
				.put("dbName", "ttservicedb")
				.put("reportsCollectionName", "reports")
				.put("usersCollectionName", "users");
		
		config.put("mongo", mongoConf);
		
		final JsonObject httpConf = new JsonObject()
				.put("port",8080)
				.put("vitalSignsServicePort", 8082);
				
		config.put("http", httpConf);
		
		final JsonObject smtpConf = new JsonObject()
				.put("host", "")
				.put("port", 0)
				.put("account", "")
				.put("pwd", "");
				
		config.put("smtp", smtpConf);
	}
	
	public String getVersion(){
		return config.getString("version");
	}
	
	public int getNumRecentReports(){
		return config.getInteger("numRecentReports");
	}
	
	public String getDBHost(){
		return config.getJsonObject("mongo").getString("host");
	}
	
	public int getDBPort(){
		return config.getJsonObject("mongo").getInteger("port");
	}
	
	public String getDBName(){
		return config.getJsonObject("mongo").getString("dbName");
	}
	
	public String getUsersCollectionName(){
		return config.getJsonObject("mongo").getString("usersCollectionName");
	}

	public String getReportsCollectionName(){
		return config.getJsonObject("mongo").getString("reportsCollectionName");
	}
	
	public int getPort(){
		return config.getJsonObject("http").getInteger("port");		
	}
	
	public int getVitalSignsServicePort(){
		return config.getJsonObject("http").getInteger("vitalSignsServicePort");		
	}
	
	public SmtpAccountConf getSmtpAccountConf() {
		return new SmtpAccountConf(
			config.getJsonObject("smtp").getString("host"),
			config.getJsonObject("smtp").getInteger("port"),
			config.getJsonObject("smtp").getString("account"),
			config.getJsonObject("smtp").getString("pwd")
		);
	}
	
	/*
	public String getMongoPath() {
		return config.getString(mongoPathKey);
	}

	public String getDBPath() {
		return config.getString(dbPathKey);
	}
	public boolean isLogGUIEnabled() {
		return config.getBoolean(logGUIEnabled);
	}
	
	public JsonArray getDefUsers() {
		return config.getJsonArray(defaultUsers);
	}*/
	
	public class SmtpAccountConf{
		private String host;
		private int port;
		private String account, pwd;
		
		public SmtpAccountConf(final String host, final int port, final String account, final String pwd) {
			this.host = host;
			this.port = port;
			this.account = account;
			this.pwd = pwd;
		}
		
		public String host() {
			return host;
		}
		
		public int port() {
			return port;
		}
		
		public String account() {
			return account;
		}
		
		public String pwd() {
			return pwd;
		}
	}
}
