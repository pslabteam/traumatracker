package t4c.tt.service.reports;

import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.MediaFileManager;
import t4c.tt.service.TTService;

public class UploadAudioHandler extends AbstractServiceHandler implements MediaFileManager {

	public UploadAudioHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext){
		log("Handling Uploading Audios from " + routingContext.request().absoluteURI());
		storeUploadedFiles(getFS(), routingContext.fileUploads(), super::log);
		routingContext.response().end();
	}
}
