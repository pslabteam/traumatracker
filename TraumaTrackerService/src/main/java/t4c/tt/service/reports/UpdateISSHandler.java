package t4c.tt.service.reports;

import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.ontology.TraumaReportDomainModel;

public class UpdateISSHandler extends AbstractServiceHandler {

	public UpdateISSHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		log("Handling update iss report from "+routingContext.request().absoluteURI());
		String reportID = routingContext.request().getParam("reportID");
		if (reportID == null) {
			log("Invalid report id: " + reportID);
			sendError(400, response);
		} else {
			JsonObject query = new JsonObject().put("_id", reportID);
			JsonObject issInfo = routingContext.getBodyAsJson();
			findInReports(query, res -> {
				if (res.succeeded()) {
					List<JsonObject> list = res.result();
					if (!list.isEmpty()) {
						JsonObject report = list.get(0);
						log("Updating ISS of report: " + reportID);

						TraumaReportDomainModel mod = new TraumaReportDomainModel(report);
						
							try {
								mod.updateISS(issInfo);
								getStore().save(getConfig().getReportsCollectionName(), mod.getReport(), res2 -> {
									if (res2.succeeded()) {										
										log("Report updated (iss).");
										response.end();
									} else {
										log("ERROR: not able to update the db.");
										sendErrorInternal(response);
									}
								});
							
							} catch (Exception ex) {
								JsonObject errorDesc = new JsonObject();
								errorDesc.put("error", "invalid iss info");
								errorDesc.put("info", issInfo);
								sendErrorConflict(errorDesc, response);
							}
					} else {
						log("ERROR: Report " + reportID + " not found");
						sendErrorNotFound(response);
					}
				} else {
					log("ERROR: Report " + reportID + " not found");
					sendErrorNotFound(response);
				}
			});
		}	
	}
	
}
