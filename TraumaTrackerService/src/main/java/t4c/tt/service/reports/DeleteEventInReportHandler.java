package t4c.tt.service.reports;

import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class DeleteEventInReportHandler extends AbstractServiceHandler {

	public DeleteEventInReportHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Delete event from "+routingContext.request().absoluteURI());
		String reportID = routingContext.request().getParam("reportID");
		String eventID = routingContext.request().getParam("eventID");
		log("Deleting event: " + eventID +" from report:" + reportID);

		HttpServerResponse response = routingContext.response();

		try {
			int evId = Integer.parseInt(eventID);
			JsonObject rep = new JsonObject().put("_id", reportID);
			findInReports(rep, res -> {
				if (res.succeeded()) {
					log("Report found. Going to remove the event.. ");
					List<JsonObject> list = res.result();
					if (list.size() > 0){
						JsonObject repo = list.get(0);
						JsonArray events = repo.getJsonArray("events");
						for (int i = 0; i < events.size(); i++){
							if (events.getJsonObject(i).getInteger("eventId") == evId){
								log("Event found.");
								events.remove(i);
								try {
									getStore().save(getConfig().getReportsCollectionName(), repo, res2 -> {
											if (res2.succeeded()) {
												log("Report updated: \n" + repo.encodePrettily());
												try {
													response.putHeader("content-type", "application/json").end(repo.encodePrettily());
												} catch (Exception ex){
													log("Error in closing the connection after updating a report: ");
													for (StackTraceElement e: ex.getStackTrace()){
														log("  "+e.toString());
													}
												}
											} else {
												log("Error in updating the report: ");
												for (StackTraceElement e: res2.cause().getStackTrace()){
													log("  "+e.toString());
												}
												sendError(400, response);
											}
										});
								} catch (Exception ex) {
									logError("Malformed data when adding a report:\n " + routingContext.getBodyAsString());
									sendError(400, response);
								}							
								
							}
						}
					} else {
						log("Report not found.");
						sendError(404, response);
					}
				} else {
					log("Report not found.");
					sendError(404, response);
				}
			});
		} catch (Exception ex){
			log("Report not found.");
			sendError(404, response);
		}
	}
	
}
