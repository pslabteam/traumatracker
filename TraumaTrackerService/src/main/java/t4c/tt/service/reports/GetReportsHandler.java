package t4c.tt.service.reports;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.util.TTConfig;

public class GetReportsHandler extends AbstractServiceHandler {

	public GetReportsHandler(TTService service) {
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		TTConfig config = getConfig();
		
		HttpServerResponse response = routingContext.response();
		
		JsonObject query = new JsonObject();
		JsonObject options = new JsonObject();

		String userId = routingContext.request().getParam("userId");
		
		String opID = routingContext.request().getParam("opId");
		if (opID != null && !opID.equals("")) {
			query.put("startOperatorId", opID);
		}
		
		int numRecentReports = -1;
		String mostRecent = routingContext.request().getParam("mostRecent");
		if (mostRecent != null && !mostRecent.equals("")) {
			try {
				numRecentReports = Integer.parseInt(mostRecent);
				if (numRecentReports > 0 && numRecentReports < config.getNumRecentReports()) {
					options.put("limit", numRecentReports);
				} else {
					sendError(400, response);
					return;
				}
			} catch (Exception ex) {
				sendError(400, response);
				return;
			}
		}

		/* sorted by date - ascending */
		options.put("sort", new JsonObject().put("startDate", -1).put("startTime", -1));

		/*
		if (userId != null) {
			Future<JsonObject> fut = Future.future();
			this.getService().fetchUserInfo(userId, fut);
			fut.setHandler(ures -> {				
				if (ures.succeeded() && ures.result() != null) {	
					JsonObject userInfo = ures.result();
					String userRole = userInfo.getString("role");
					*/
		
					findInReportsWithOptions(query, options, res -> {						
						if (res.succeeded()) {
							String dateFrom = routingContext.request().getParam("dateFrom");				
							String dateTo = routingContext.request().getParam("dateTo");
							String iss = routingContext.request().getParam("iss");
							int issValue = -1;
							
							String dateFromFlat = dateFrom != null ? (dateFrom.equals("") ? null : dateFrom) : null ; 
							String dateToFlat = dateTo != null ? (dateTo.equals("") ? null : dateTo) : null; 
							
							if (iss != null && !iss.equals("")) {
								issValue = Integer.parseInt(iss);
							}
							
							JsonArray reports = new JsonArray();
							for (JsonObject rep : res.result()) {
								
								String startDate = rep.getString("startDate");
								
								boolean toBeAdded = true;
									
								/* if (userRole.equals("member")) {
									String opId = rep.getString("startOperatorId");
									if (userId != null) {
										if (!userId.equals(opId)) {
											JsonArray events = rep.getJsonArray("events");
											Iterator<Object> it = events.iterator();
											boolean found = false;
											while (it.hasNext() && !found) {
												JsonObject ev = (JsonObject) it.next();
												if (ev.getString("type").equals("trauma-leader")) {
													if (ev.getJsonObject("content").getString("userId").equals(userId)) {
														found = true;
													}
												}
											}
											if (!found) {
												toBeAdded = false;
											}
										}
									}
								}*/
								
								
								if (toBeAdded && dateFromFlat != null) {
									toBeAdded = startDate.compareTo(dateFromFlat) >= 0;
								}
								
								if (toBeAdded) {
									if (dateToFlat != null) {
										toBeAdded = startDate.compareTo(dateToFlat) <= 0;
									}
								}
								
								if (toBeAdded) {
									if (issValue != -1) {
										JsonObject issObj = rep.getJsonObject("iss");
										if (issObj != null) {
											toBeAdded = Integer.parseInt(issObj.getString("total-iss")) >= issValue;
										}
									}
								}
								
								if (toBeAdded) {
									
									/* don't include vital signs */
									rep.put("vitalSignsObservations", new JsonArray());
									
									reports.add(rep);
								}					
							}
							//log("GETTING "+reports.size()+" REPORTS");
							
							/* TMP PATCH */
							/*
							if (reports.size() > 0) {
								JsonObject last = reports.getJsonObject(reports.size() - 1);
								
								Future<JsonObject> vs = Future.future();
								this.getService().fetchVitalParams(last, vs);
								
								vs.setHandler(vsRes -> {
									if (vsRes.succeeded()) {
										for (int i = 0; i < reports.size(); i++) {
											JsonObject rep = reports.getJsonObject(i);
											this.getService().integrateVitalParamsInfo(rep, Optional.of(vsRes.result()));
										}
										response.putHeader("content-type", "application/json").end(reports.encodePrettily());
									} else {
										for (int i = 0; i < reports.size(); i++) {
											JsonObject rep = reports.getJsonObject(i);
											this.getService().integrateVitalParamsInfo(last, Optional.empty());
										}
										response.putHeader("content-type", "application/json").end(reports.encodePrettily());
									}
								});
							}*/

							response.putHeader("content-type", "application/json").end(reports.encodePrettily());

						} else {
							sendError(404, response);
						}
					});
					/*			
					
				} else {
					response.putHeader("content-type", "application/json").end(new JsonArray().encodePrettily());
				}
			});
		}*/
		

	}

}
