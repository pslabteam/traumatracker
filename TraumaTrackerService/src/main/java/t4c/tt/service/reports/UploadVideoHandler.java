package t4c.tt.service.reports;

import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.MediaFileManager;
import t4c.tt.service.TTService;

public class UploadVideoHandler extends AbstractServiceHandler implements MediaFileManager {

	public UploadVideoHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext){
		log("Handling Uploading Videos from " + routingContext.request().absoluteURI());
		storeUploadedFiles(getFS(), routingContext.fileUploads(), super::log);
		routingContext.response().end();
	}	
	
}
