package t4c.tt.service.reports;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class UpdateReportHandler extends AbstractServiceHandler {

	public UpdateReportHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Update report from "+routingContext.request().absoluteURI());
		String reportID = routingContext.request().getParam("reportID");
		log("Updating report: " + reportID);
		HttpServerResponse response = routingContext.response();
		if (reportID == null) {
			sendError(400, response);
		} else {
			JsonObject report = routingContext.getBodyAsJson();
			log("Report data: " + report);
			if (report == null) {
				sendError(400, response);
			} else {
				report.put("_id", reportID);
				getStore().save(getConfig().getReportsCollectionName(), report, res -> {
					if (res.succeeded()) {
						log("Report updated.");
						response.end();
					} else {
						log("Report update error.");
						sendError(400, response);
					}
				});

			}
		}	
	}
	
}
