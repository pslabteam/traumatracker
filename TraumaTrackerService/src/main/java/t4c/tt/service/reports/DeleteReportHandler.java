package t4c.tt.service.reports;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class DeleteReportHandler extends AbstractServiceHandler {

	public DeleteReportHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Delete Report from "+routingContext.request().absoluteURI());
		String reportID = routingContext.request().getParam("reportID");
		log("Deleting report: " + reportID);
		HttpServerResponse response = routingContext.response();
		JsonObject doc = new JsonObject().put("_id", reportID);
		getStore().removeDocument(getConfig().getReportsCollectionName(), doc, res -> {
			if (res.succeeded()) {
				log("Report deleted.");
				response.end("Report deleted");
			} else {
				log("Report not found.");
				sendError(404, response);
			}
		});

	}

	
}
