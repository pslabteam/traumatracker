package t4c.tt.service.reports;

import java.util.Calendar;
import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.ontology.TraumaReportDomainModel;

public class ValidateReportHandler extends AbstractServiceHandler {

	public ValidateReportHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		log("Handling validate report from "+routingContext.request().absoluteURI());
		String reportID = routingContext.request().getParam("reportID");
		if (reportID == null) {
			log("Invalid report id: " + reportID);
			sendError(400, response);
		} else {
			JsonObject query = new JsonObject().put("_id", reportID);
			findInReports(query, res -> {
				if (res.succeeded()) {
					List<JsonObject> list = res.result();
					if (!list.isEmpty()) {
						JsonObject report = list.get(0);
						log("Validating report: " + reportID);

						TraumaReportDomainModel mod = new TraumaReportDomainModel(report);
						
						JsonObject validationInfo = routingContext.getBodyAsJson();
						String operatorId = validationInfo.getString("operatorId");
						
					    Calendar now = Calendar.getInstance();
					    String validationDate = "" + now.get(Calendar.YEAR) + "-" + withZeros("" + (now.get(Calendar.MONTH)+1)) + "-" + withZeros(""+now.get(Calendar.DAY_OF_MONTH));
					    String validationTime = "" + withZeros(""+now.get(Calendar.HOUR_OF_DAY)) + ":" + withZeros(""+now.get(Calendar.MINUTE)) + ":" + withZeros(""+now.get(Calendar.SECOND));
						
						if (operatorId != null) {
							try {
								mod.validateReport(operatorId, validationDate, validationTime);							
								getStore().save(getConfig().getReportsCollectionName(), mod.getReport(), res2 -> {
									if (res2.succeeded()) {										
										log("Report validated.");
										response.end();
									} else {
										log("ERROR: not able to update the db.");
										sendErrorInternal(response);
									}
								});
							
							} catch (Exception ex) {
								JsonObject errorDesc = new JsonObject();
								errorDesc.put("error", "report already validated");
								sendErrorConflict(errorDesc, response);
							}
						} else {
							JsonObject errorDesc = new JsonObject();
							errorDesc.put("error", "invalid validation info");
							errorDesc.put("info", validationInfo);
							sendErrorSyntax(errorDesc, response);
						}
					} else {
						log("ERROR: Report " + reportID + " not found");
						sendErrorNotFound(response);
					}
				} else {
					log("ERROR: Report " + reportID + " not found");
					sendErrorNotFound(response);
				}
			});
		}	
	}
	
	private String withZeros(String st) {
		return st.length() < 2 ? "0" + st : st;
	}
	
	
}
