package t4c.tt.service.reports;

import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.ontology.TraumaReportDomainModel;

public class UpdateVitalSignsHandler extends AbstractServiceHandler {

	public UpdateVitalSignsHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		log("Handling update vital signs report from "+routingContext.request().absoluteURI());
		String reportID = routingContext.request().getParam("reportID");
		if (reportID == null) {
			log("Invalid report id: " + reportID);
			sendError(400, response);
		} else {
			JsonObject query = new JsonObject().put("_id", reportID);
			JsonObject vitalSignsInfo = routingContext.getBodyAsJson();
			findInReports(query, res -> {
				if (res.succeeded()) {
					List<JsonObject> list = res.result();
					if (!list.isEmpty()) {
						JsonObject report = list.get(0);
						log("Updating Vital Signs of report: " + reportID);
						TraumaReportDomainModel mod = new TraumaReportDomainModel(report);
						
							try {
								mod.updateVitalSigns(vitalSignsInfo);
								getStore().save(getConfig().getReportsCollectionName(), mod.getReport(), res2 -> {
									if (res2.succeeded()) {										
										log("Report updated (vital signs).");
										response.end();
									} else {
										log("ERROR: not able to update the db.");
										sendErrorInternal(response);
									}
								});
							
							} catch (Exception ex) {
								JsonObject errorDesc = new JsonObject();
								errorDesc.put("error", "invalid vital signs info");
								errorDesc.put("info", vitalSignsInfo);
								sendErrorConflict(errorDesc, response);
							}
					} else {
						log("ERROR: Report " + reportID + " not found");
						sendErrorNotFound(response);
					}
				} else {
					log("ERROR: Report " + reportID + " not found");
					sendErrorNotFound(response);
				}
			});
		}	
	}
	
}
