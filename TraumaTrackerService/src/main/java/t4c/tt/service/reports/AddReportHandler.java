package t4c.tt.service.reports;

import java.util.Optional;

import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.ontology.TTDataModel;

public class AddReportHandler extends AbstractServiceHandler {

	public AddReportHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling add report from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		JsonObject report = routingContext.getBodyAsJson();
		TTDataModel model = TTDataModel.instance();
		boolean ok = model.checkReportCorrectness(report);
		if (!ok) {
			logError("Malformed report:\n " + routingContext.getBodyAsString());
			sendError(400, response);
		} else {
			this.getService().integrateVitalParamsInfo(report, Optional.empty());
			saveInReports(report, res2 -> {
				if (res2.succeeded()) {
					log("New report saved (vital params to be added): \n" + report.encodePrettily());
					try {
						/* don't make the mobile user app wait */
						response.end();

						/* retrieve vital signs */
						String id = report.getString("_id");
						Future<JsonObject> vs = Future.future();
						vs.setHandler(vsRes -> {
							try {				
								if (vsRes.succeeded()) {
									/* update the report on the DB */
									this.getService().integrateVitalParamsInfo(report, Optional.of(vsRes.result()));
									saveInReports(report, res3 -> {
										log("Report " + id + "updated with vital params. \n");
									});
								}								
							} catch (Exception ex) {
								logError("Error in updating report " + id + " with vital params.");
							}
						});
						log("Fetching vital params for report " + id);
						this.getService().fetchVitalParams(report, vs);
					} catch (Exception ex){
						log("Error in closing the connection after saving a report");
						for (StackTraceElement e: res2.cause().getStackTrace()){
							log("  "+e.toString());
						}
					}
				} else {
					log("Error in saving the report: ");
					for (StackTraceElement e: res2.cause().getStackTrace()){
						log("  "+e.toString());
					}
					sendError(400, response);
				}
			});
			
			
			/*
			Future<JsonObject> vs = Future.future();
			vs.setHandler(vsRes -> {
				try {				
					if (vsRes.succeeded()) {
						this.getService().integrateVitalParamsInfo(report, Optional.of(vsRes.result()));
					} else {
						this.getService().integrateVitalParamsInfo(report, Optional.empty());
					}
					
					saveInReports(report, res2 -> {
							if (res2.succeeded()) {
								log("New report saved: \n" + report.encodePrettily());
								try {
									response.end();
								} catch (Exception ex){
									log("Error in closing the connection after saving a report");
									for (StackTraceElement e: res2.cause().getStackTrace()){
										log("  "+e.toString());
									}
								}
							} else {
								log("Error in saving the report: ");
								for (StackTraceElement e: res2.cause().getStackTrace()){
									log("  "+e.toString());
								}
								sendError(400, response);
							}
						});
				} catch (Exception ex) {
					logError("Malformed data when adding a report:\n " + routingContext.getBodyAsString());
					sendError(400, response);
				}
			});
			this.getService().fetchVitalParams(report, vs);
			*/
		}
	}
	
}
