package t4c.tt.service.reports;

import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.MediaFileManager;

public class UploadPhotoHandler extends AbstractServiceHandler implements MediaFileManager{
	
	public UploadPhotoHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext){
		log("Handling Uploading Photos from " + routingContext.request().absoluteURI());
		storeUploadedFiles(getFS(), routingContext.fileUploads(), super::log);
		routingContext.response().end();
	}
}
