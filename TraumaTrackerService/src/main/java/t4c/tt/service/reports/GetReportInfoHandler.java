package t4c.tt.service.reports;

import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class GetReportInfoHandler extends AbstractServiceHandler {

	public GetReportInfoHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Get report info from "+routingContext.request().absoluteURI());
		String reportID = routingContext.request().getParam("reportID");
		HttpServerResponse response = routingContext.response();
		if (reportID == null) {
			log("Invalid report id: " + reportID);
			sendError(400, response);
		} else {
			log("Getting report: " + reportID);
			JsonObject query = new JsonObject().put("_id", reportID);
			findInReports(query, res -> {
				if (res.succeeded()) {
					List<JsonObject> list = res.result();
					if (!list.isEmpty()) {
						JsonObject report = list.get(0);
						response.putHeader("content-type", "application/json").end(report.encodePrettily());
						log("Report sent");
					} else {
						sendError(404, response);
					}
				} else {
					log("ERROR: Report " + reportID + " not found");
					sendError(404, response);
				}
			});
		}
	}
	
}
