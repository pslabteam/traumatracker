package t4c.tt.service.users;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class DeleteUserHandler extends AbstractServiceHandler {

	public DeleteUserHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Delete User from "+routingContext.request().absoluteURI());
		String userId = routingContext.request().getParam("userId");
		log("Deleting user: " + userId);
		HttpServerResponse response = routingContext.response();
		JsonObject doc = new JsonObject().put("_id", userId);
		getStore().removeDocument(getConfig().getUsersCollectionName(), doc, res -> {
			if (res.succeeded()) {
				log("User deleted.");
				response.end();
			} else {
				log("User not found.");
				sendError(404, response);
			}
		});

	}

	
}
