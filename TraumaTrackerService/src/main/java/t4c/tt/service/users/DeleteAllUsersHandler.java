package t4c.tt.service.users;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class DeleteAllUsersHandler extends AbstractServiceHandler {

	public DeleteAllUsersHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Delete all Users from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		try {
			getStore().dropCollection(getConfig().getUsersCollectionName(), res -> {
					if (res.succeeded()) {
						log("Users collection reset done.");
						JsonObject reply = new JsonObject().put("req","delete all users").put("res", "done");
						response.putHeader("content-type", "application/json").end(reply.encodePrettily());
					} else {
						log("Reports collection reset error.");
						JsonObject reply = new JsonObject().put("req","delete all users").put("res", "error");
						response.putHeader("content-type", "application/json").end(reply.encodePrettily());
					}
			}); 
		} catch (Exception ex){
			log("Malformed request");
			sendError(400, response);
		}

	}
	
}
