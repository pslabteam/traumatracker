package t4c.tt.service.users;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;

public class GetUsersHandler extends AbstractServiceHandler {

	public GetUsersHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Get Users from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		
		JsonObject query = new JsonObject();
		
		FindOptions options = new FindOptions();
		options.setSort(new JsonObject().put("surname", 1));
		
		getStore().findWithOptions(getConfig().getUsersCollectionName(), query, options, res -> {
			if (res.succeeded()) {
				JsonArray users = new JsonArray();
				for (JsonObject rep : res.result()) {
					rep.remove("pwd");
					users.add(rep);
				}
				response.putHeader("content-type", "application/json").end(users.encodePrettily());
			} else {
				sendError(404, response);
			}
		});
	}
	
}
