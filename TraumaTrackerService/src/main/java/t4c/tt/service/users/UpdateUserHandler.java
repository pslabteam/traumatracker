package t4c.tt.service.users;

import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import t4c.tt.service.AbstractServiceHandler;
import t4c.tt.service.TTService;
import t4c.tt.service.ontology.TTDataModel;

public class UpdateUserHandler extends AbstractServiceHandler {

	public UpdateUserHandler(TTService service){
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		log("Handling Update User from "+routingContext.request().absoluteURI());
		HttpServerResponse response = routingContext.response();
		JsonObject user = routingContext.getBodyAsJson();
		TTDataModel model = TTDataModel.instance();
		boolean ok = model.checkUserCorrectness(user);
		if (!ok) {
			logError("Malformed user:\n " + routingContext.getBodyAsString());
			sendError(400, response);
		} else {
			try {
				String id = user.getString("userId");
				JsonObject query = new JsonObject().put("userId", id);
				findInUsers(query, res -> {
					if (res.succeeded()) {
						List<JsonObject> list = res.result();
						
						JsonObject userObj = model.makeUser(user.getString("surname"), user.getString("name"), user.getString("role"), 
											user.getString("pwd") != null ? user.getString("pwd") : list.get(0).getString("pwd"), 
											user.getString("email"));
						userObj.put("_id", id);
						updateInUsers(userObj, res2 -> {
							if (res2.succeeded()) {
									log("User updated");
									response.end();
							} else {
									log("User update failed: "+id);
									sendError(400, response);
							}
						});				
					} else {
						sendError(404, response);
					}
				});
			} catch (Exception ex) {
				logError("Malformed data when adding a user:\n " + routingContext.getBodyAsString());
				sendError(400, response);
			}
		}	
	}
	
}
