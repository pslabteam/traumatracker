package t4c.tt.service;

import java.io.File;
import java.util.Set;
import java.util.function.Consumer;

import io.vertx.core.file.FileSystem;
import io.vertx.ext.web.FileUpload;

public interface MediaFileManager {
	
	static final String FILE_UPLOAD_FOLDER = "file-uploads";

	default void storeUploadedFiles(final FileSystem fs, final Set<FileUpload> uploads, final Consumer<String> onCompleted) {
		uploads.forEach( f -> {
			//log(f.fileName()+"\n"+f.contentTransferEncoding()+"\n"+f.contentType()+"\n"+f.uploadedFileName()+"\n");
			
			final String fileTo = new File("").getAbsolutePath()
					+ File.separatorChar
					+ FILE_UPLOAD_FOLDER
					+ File.separatorChar
					+ f.fileName();
						
			fs.move(f.uploadedFileName(), fileTo, res -> {
				if(res.succeeded()) {
					onCompleted.accept(f.fileName() + " uploaded!");
				} else {
					onCompleted.accept("Error in uploading file");
				}
			});
		});
	}
}
