package t4c.tt.service.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class TSFilterValidReportsAndMapNewVersion extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(TSFilterValidReportsAndMapNewVersion.class);
	private Vertx vertx;
	private String targetFile;
	private String sourceFile;
	
	public TSFilterValidReportsAndMapNewVersion(String sourceFile, String targetFile){		
		this.targetFile = targetFile;
		this.sourceFile = sourceFile;
	}
	
	@Override
	public void start() {
	
			try {
				FileReader fis = new FileReader(sourceFile);
				BufferedReader br = new BufferedReader(fis);

				List<JsonObject> reps = new ArrayList<JsonObject>();				
				try {
					while (br.ready()) {
						JsonObject obj = new JsonObject(br.readLine());
						reps.add(obj);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
					br.close();
				}
				// System.out.println("N. tot reps " + reps.size());

				List<JsonObject> goodReports = reps.stream().filter(rep -> {
					JsonObject iss = rep.getJsonObject("iss");
					if (iss != null) {
						String totalIss = iss.getString("totalIss");
						if (totalIss != null && !totalIss.trim().equals("")) {
							// System.out.println(totalIss);
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}).map((JsonObject source) ->  {
					JsonObject target = new JsonObject();

					ifs (source, target, "_id");
					ifs (source, target, "_version");
					ifs (source, target, "startOperatorId");
					ifs (source, target, "startOperatorDescription");
					
					JsonObject delayedActivation = new JsonObject();
					target.put("delayedActivation", delayedActivation);
					String sec = source.getString("secondaryActivation");
					if (sec != null && sec.equals("si")) {
						delayedActivation.put("isDelayedActivation",true);
					} else {
						delayedActivation.put("isDelayedActivation",false);
					}
					
					ifa (source, target, "traumaTeamMembers");
					ifs (source, target, "startDate");
					ifs (source, target, "startTime");
					ifs (source, target, "endDate");
					ifs (source, target, "endTime");

					JsonObject iss = source.getJsonObject("iss");
					if (iss != null) {
						JsonObject niss = new JsonObject();
						target.put("iss", niss);
						String tot = iss.getString("totalIss");
						if (tot != null) {
							niss.put("totalIss",Integer.parseInt(tot));
						}
						JsonObject hg = iss.getJsonObject("headGroup");
						if (hg != null) {
							JsonObject nhg = new JsonObject();
							niss.put("headGroup", nhg);
							ifs2n(hg, nhg, "groupTotalIss");
							ifs2n(hg, nhg, "headNeckAis");
							ifs2n(hg, nhg, "brainAis");
							ifs2n(hg, nhg, "cervicalSpineAis");
						}
						JsonObject fg = iss.getJsonObject("faceGroup");
						if (fg != null) {
							JsonObject nfg = new JsonObject();
							niss.put("faceGroup", nfg);
							ifs2n(fg, nfg, "groupTotalIss");
							ifs2n(fg, nfg, "faceAis");
						}
						JsonObject tg = iss.getJsonObject("toraxGroup");
						if (tg != null) {
							JsonObject ntg = new JsonObject();
							niss.put("toraxGroup", ntg);
							ifs2n(tg, ntg, "groupTotalIss");
							ifs2n(tg, ntg, "toraxAis");
							ifs2n(tg, ntg, "spineAis");
						}
						JsonObject ag = iss.getJsonObject("abdomenGroup");
						if (ag != null) {
							JsonObject nag = new JsonObject();
							niss.put("abdomenGroup", nag);
							ifs2n(ag, nag, "groupTotalIss");
							ifs2n(ag, nag, "abdomenAis");
							ifs2n(ag, nag, "lumbarSpineAis");
						}
						JsonObject eg = iss.getJsonObject("extremitiesGroup");
						if (eg != null) {
							JsonObject neg = new JsonObject();
							niss.put("extremitiesGroup", neg);
							ifs2n(eg, neg, "groupTotalIss");
							ifs2n(eg, neg, "upperExtremitiesAis");
							ifs2n(hg, neg, "lowerExtremitiesAis");
							ifs2n(hg, neg, "pelvicGirdleAis");
						}
						JsonObject exg = iss.getJsonObject("externaGroup");
						if (exg != null) {
							JsonObject nxg = new JsonObject();
							niss.put("externaGroup", nxg);
							ifs2n(exg, nxg, "groupTotalIss");
							ifs2n(exg, nxg, "externaAis");
						}
						
					}

					ifs (source, target, "finalDestination");
					
					JsonObject pat = source.getJsonObject("patientInfo");
					if (pat != null) {
						JsonObject tr = new JsonObject();
						target.put("traumaInfo", tr);
						ifs (pat, tr, "code");
						ifs (pat, tr, "sdo");
						ifs2b (pat, tr, "erDeceased");
						ifs (pat, tr, "admissionCode");
						ifs (pat, tr, "gender");
						ifs (pat, tr, "type");
						ifs (pat, tr, "age");
						ifs (pat, tr, "accidentDate");
						ifs (pat, tr, "accidentTime");
						ifs (pat, tr, "accidentType");
						ifs (pat, tr, "vehicle");
						ifs2b (pat, tr, "fromOtherEmergency");
						ifs (pat, tr, "otherEmergency");
					}
					
					JsonObject an = source.getJsonObject("anamnesi");
					if (an != null) {
						JsonObject tan = new JsonObject();
						target.put("anamnesi", tan);
						ifs2b (an, tan, "antiplatelets");
						ifs2b (an, tan, "anticoagulants");
						ifs2b (an, tan, "nao");
					}

					JsonObject preh = source.getJsonObject("preh");
					if (preh != null) {
						JsonObject tpreh = new JsonObject();
						target.put("preh", tpreh);
						ifs (preh, tpreh, "aValue");
						ifs2b (preh, tpreh, "bPleuralDecompression");
						ifs2b (preh, tpreh, "cBloodProtocol");
						ifs2b (preh, tpreh, "cTpod");
						ifs2b (preh, tpreh, "cPatientStationary");
						ifs2n (preh, tpreh, "dGcsTotal");
						ifs2b (preh, tpreh, "dAnisocoria");
						ifs2b (preh, tpreh, "dMidriasi");
						ifs2b (preh, tpreh, "eMotility");
					}

					JsonObject vs = source.getJsonObject("startVitalSigns");
					if (vs != null) {
						JsonObject tvs = new JsonObject();
						target.put("startVitalSigns", tvs);
						ifs (vs, tvs, "Temp");
						ifs2n (vs, tvs, "TempValue");
						ifs (vs, tvs, "HR");
						ifs2n (vs, tvs, "HRValue");
						ifs (vs, tvs, "BP");
						ifs2n (vs, tvs, "BPValue");
						ifs (vs, tvs, "SpO2");
						ifs2n (vs, tvs, "SpO2Value");
						ifs (vs, tvs, "EtCO2");
						ifs2b (vs, tvs, "ExtBleeding");
						ifs (vs, tvs, "Airway");
						ifs2b (vs, tvs, "Tracheo");
						ifs2b (vs, tvs, "IntubationFailed");
						ifs (vs, tvs, "ChestTube");
						ifs2n (vs, tvs, "OxygenPercentage");
						ifs2n (vs, tvs, "GCSTotal");
						ifs (vs, tvs, "GCSMotor");
						ifs (vs, tvs, "GCSVerbal");
						ifs (vs, tvs, "GCSEyes");
						ifs2b (vs, tvs, "Sedated");
						ifs (vs, tvs, "Pupils");
						ifs2b (vs, tvs, "LimbsFracture");
						ifs2b (vs, tvs, "FractureExposition");
						ifs (vs, tvs, "Burn");	
						
						// ifs2n (vs, tvs, "BurnDegree");
						String val = vs.getString("BurnDegree");
						if ( val != null && !val.equals("")) { 
							tvs.put("BurnDegree", Integer.parseInt(val.substring(0, val.indexOf("°"))));
						}
						
						ifs2n (vs, tvs, "BurnPercentage");
					}
					
					JsonArray evl = source.getJsonArray("events");
					if (evl != null && evl.size() > 0) {
						JsonArray nevl = new JsonArray();
						target.put("events", nevl);

						Iterator<Object> it = evl.iterator();
						while (it.hasNext()) {
							JsonObject ev = (JsonObject) it.next();
							
							JsonObject nev = new JsonObject();
							nevl.add(nev);
														
							ifsn2n (ev, nev, "eventId");

							ifs (ev, nev, "date");
							ifs (ev, nev, "time");
							ifs (ev, nev, "place");
							ifs (ev, nev, "type");
	
							String evType = ev.getString("type");
							
							JsonObject co = ev.getJsonObject("content");
							if (co != null) {
								JsonObject nco = new JsonObject();
								nev.put("content",nco);
								
								ifs (co, nco, "procedureId");
								ifs (co, nco, "procedureDescription");
								ifs (co, nco, "procedureType");
								ifs (co, nco, "event");
	
								ifs2b (co, nco, "difficultAirway");
								ifs2b (co, nco, "inhalation");
								ifs2b (co, nco, "videolaringo");
								ifs2b (co, nco, "frova");
								
								ifs2b (co, nco, "right");
								ifs2b (co, nco, "left");
	
								ifs (co, nco, "diagnosticId");
								ifs (co, nco, "diagnosticDescription");
								
								ifs2n (co, nco, "lactates");
								ifs2n (co, nco, "be");
								ifs2n (co, nco, "ph");
								ifs2n (co, nco, "hb");
						
								ifs (co, nco, "fibtem");
								ifs (co, nco, "extem");
								ifs2b (co, nco, "hyperfibrinolysis");
								
								ifs (co, nco, "drugId");
								ifs (co, nco, "drugDescription");
								ifs (co, nco, "administrationType");
								ifn (co, nco, "qty");
								ifs (co, nco, "unit");
								ifn (co, nco, "duration");
								
								
								String name = co.getString("name");
								if (name != null) {
									if (evType.equals("vital-sign")) {
										nco.put("vsId", name);
										// nco.put("vsDescription", "Variazione parametro vitale: "+name);
									} else {
										nco.put("name", name);
									}
								}
								ifs (co, nco, "value");
								
								ifn (co, nco, "Temp");
								ifn (co, nco, "HR");
								ifn (co, nco, "DIA");
								ifn (co, nco, "SYS");
								ifn (co, nco, "SpO2");
								ifn (co, nco, "EtCO2");
							
								ifs (co, nco, "text");
	
								ifs (co, nco, "userId");
								// ifs (co, nco, "name");
								ifs (co, nco, "surname");
	
								ifs (co, nco, "place");
							}
						}
						
					}
					return target;
				}).collect(Collectors.toList());


				
				System.out.println("Writing file: "+targetFile);
				try {
					FileWriter writer = new FileWriter(targetFile);
					for (JsonObject rep: goodReports) {
						writer.write(rep + "\n");
					}
					writer.close();
					System.out.println("Done: "+goodReports.size() +" reports written.");
					System.exit(0);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				// System.out.println("N. reps " + goodReports.size());
			} catch (Exception ex) {
				ex.printStackTrace();
				System.exit(-1);
			}
		
	}
		
	private void ifs(JsonObject source, JsonObject target, String key) {
		String val = source.getString(key);
		if ( val != null && !val.equals("")) { 
			val = val.replaceAll(",", " ");
			target.put(key, val); 
		};
	}

	private void ifs2n(JsonObject source, JsonObject target, String key) {
		String val = source.getString(key);
		if ( val != null && !val.equals("")) { 
			try {
				target.put(key, Integer.parseInt(val)); 
			} catch (Exception ex) {
				target.put(key, Double.parseDouble(val)); 
			}
		};
	}

	private void ifsn2n(JsonObject source, JsonObject target, String key) {
		Object val = source.getValue(key);
		if (val instanceof String) {
			if ( val != null && !val.equals("")) { 
				target.put(key, Integer.parseInt((String)val)); 
			}
		} else if (val instanceof Number){
			target.put(key, val); 
		} else {
			throw new IllegalArgumentException();
		}
	}

	private void ifs2b(JsonObject source, JsonObject target, String key) {
		String val = source.getString(key);
		if ( val != null && !val.equals("")) { 
			if (val.equals("si") || (val.equals("true"))) {
				target.put(key, true); 
			} else {
				target.put(key, false);
			}
		};
	}

	private void ifn(JsonObject source, JsonObject target, String key) {
		Double val = source.getDouble(key);
		if ( val != null) { 
			target.put(key, val); 
		};
	}
	
	private void ifb(JsonObject source, JsonObject target, String key) {
		Boolean val = source.getBoolean(key);
		if ( val != null) { 
			target.put(key, val); 
		};
	}
	
	private void ifa(JsonObject source, JsonObject target, String key) {
		JsonArray val = source.getJsonArray(key);
		if ( val != null) { 
			target.put(key, val); 
		};
	}
	
	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static void main(String[] args) throws Exception {
		String destFileName = "data/20181018/updated-reports.json";
		// String srcFileName = "data/reports.json";
		String srcFileName = "data/20181018/reports-with-updated-users.json";
		if (args.length == 2) {
			srcFileName = args[0];
			destFileName = args[1];
		}
		Vertx vertx = Vertx.vertx();
		TSFilterValidReportsAndMapNewVersion app = new TSFilterValidReportsAndMapNewVersion(srcFileName, destFileName);
		vertx.deployVerticle(app);				
	}
		
}
