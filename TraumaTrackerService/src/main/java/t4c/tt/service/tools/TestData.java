package t4c.tt.service.tools;

import java.util.Calendar;

public class TestData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    Calendar now = Calendar.getInstance();
	    String validationDate = "" + now.get(Calendar.YEAR) + "-" + withZeros("" + (now.get(Calendar.MONTH)+1)) + "-" + withZeros(""+now.get(Calendar.DAY_OF_MONTH));
	    String validationTime = "" + withZeros(""+now.get(Calendar.HOUR_OF_DAY)) + ":" + withZeros(""+now.get(Calendar.MINUTE)) + ":" + withZeros(""+now.get(Calendar.SECOND));
	    System.out.println(validationDate + " " + validationTime);
	}

	static String withZeros(String st) {
		if (st.length() < 2) {
			return "0" + st;
		} else {
			return st;
		}
	}
}
