package t4c.tt.service.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class FilterConvertToMod1_3 extends AbstractVerticle {

	private String targetFile;
	private String sourceFile;
	
	public FilterConvertToMod1_3(String sourceFile, String targetFile) throws Exception {			
		this.targetFile = targetFile;
		this.sourceFile = sourceFile;
	}

	@Override
	public void start() {
		System.out.println("Converting to Domain Model v. 1.3...");
		try {
			FileReader fis = new FileReader(sourceFile);
			BufferedReader br = new BufferedReader(fis);

			List<JsonObject> reps = new ArrayList<JsonObject>();				
			try {
				while (br.ready()) {
					JsonObject obj = new JsonObject(br.readLine());
					reps.add(obj);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				br.close();
			}
			// System.out.println("N. tot reps " + reps.size());

			int nMissingVersion = 0;
			int nVers1_2 = 0;
			int nWrongVersion = 0;
			
			List<JsonObject> newReps = new ArrayList<JsonObject>();				

			for (JsonObject report: reps) {

				String version = report.getString("_version"); 
				if (version == null) {
					version = report.getString("version"); 	/* fixing a bug */
					if (version != null) {
						report.remove("version");
					}
				}
				if (version == null) {
					System.out.println("WARNING: report " + report.getString("_id") + " with no version - not included");
					nMissingVersion++;
				} else if (version.equals("1.3")) {
					System.out.println("WARNING: report " + report.getString("_id") + " with a version equals to 1.3 - not processed");
					newReps.add(report);
				} else if (!version.equals("1.2")) {
					System.out.println("WARNING: report " + report.getString("_id") + " with a version not equals to 1.2 - not included");
					nWrongVersion++;
				} else {
					/* Converting to version 1.3 version of the model */

					System.out.println("Processing report: " + report.getString("_id") + " - source version: " + version);
				
					report.put("_version", "1.3");

					/* iss info */
					
					JsonObject iss = report.getJsonObject("iss");
					iss.put("isPresumed", false); // default value
					report.put("iss", iss);
					
					/* new validation obj - default values */
					
					JsonObject validation = new JsonObject();
					validation.put("isValidated", true);
					validation.put("operatorId", report.getString("startOperatorId"));
					String validationDate = null;
					String validationTime = null;
					try {
						validationDate = report.getString("endDate");
						validationTime = report.getString("endTime");
					} catch (Exception ex) {
						validationDate = "2020-06-01";
						validationTime = "00:00:00"; 
					}
					validation.put("validationDate", validationDate);
					validation.put("validationTime", validationTime);
					report.put("_validation", validation);
					
					JsonArray empty = new JsonArray();
					report.put("changelog", empty);

					newReps.add(report);
				}
			}

			System.out.println("Writing file: "+targetFile);
			try {
				FileWriter writer = new FileWriter(targetFile);
				for (JsonObject rep: newReps) {
					writer.write(rep + "\n");
				}
				writer.close();
				System.out.println("N. reps processed: " + reps.size());
				System.out.println("N. good reps: " + newReps.size());
				System.out.println("N. reps with missing version: " + nMissingVersion);
				System.out.println("N. reps with wrong version: " + nWrongVersion);
				System.exit(0);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		
			
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

	}

	public static void main(String[] args) throws Exception {
		System.out.println("-------------------------------------------------------------" );
		System.out.println("TraumaTracker Model version updater - from version 1.2 to 1.3" );
		System.out.println("-------------------------------------------------------------" );
		String destFileName = "data/20200604/reports-updated_1_3.json";
		String srcFileName = "data/20200604/reports.json";		
		
		if (args.length == 2) {
			srcFileName = args[0];
			destFileName = args[1];
		}
		Vertx vertx = Vertx.vertx();
		FilterConvertToMod1_3 app = new FilterConvertToMod1_3(srcFileName, destFileName);
		vertx.deployVerticle(app);				
	}

}
