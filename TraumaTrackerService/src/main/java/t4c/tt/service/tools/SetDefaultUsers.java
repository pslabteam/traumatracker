package t4c.tt.service.tools;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import t4c.tt.service.ontology.TTDataModel;

public class SetDefaultUsers {

	public static void main(String[] args) throws Exception {

		String address = "localhost"; 
		if (args.length > 0) {
			address = args[0];
		}

		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx
				.createHttpClient(new HttpClientOptions().setDefaultHost(address).setDefaultPort(8080));
		
		clearUsers(client);
		
		Thread.sleep(1000);

		TTDataModel model = TTDataModel.instance();

		submitUser(client, model.makeUser("Agnoletti", "Vanni", "director", "agno", "vanni.agnoletti@auslromagna.it"));
		submitUser(client, model.makeUser("Martino", "Costanza", "data-analyst", "data", "costanza.martino@auslromagna.it"));
		submitUser(client, model.makeUser("Albarello", "Vittorio", "member", "trauma", "vittorio.albarello@auslromagna.it"));
		submitUser(client, model.makeUser("Gamberini", "Emiliano", "member", "trauma", "emiliano.gamberini@auslromagna.it"));
		submitUser(client, model.makeUser("Russo", "Emanuele", "member", "trauma", "emanuele.russo@auslromagna.it"));
		submitUser(client, model.makeUser("Savelli", "Flavia", "member", "trauma", "flavia.savelli@auslromagna.it"));
		submitUser(client, model.makeUser("Benni", "Marco", "member", "trauma", "marco.benni@auslromagna.it"));
		submitUser(client, model.makeUser("Scognamiglio", "Giovanni", "member", "trauma", "giovanni.scognamiglio@auslromagna.it"));
		submitUser(client, model.makeUser("Sabia", "Giuseppe", "member", "trauma", "giuseppe.sabia@auslromagna.it"));
		submitUser(client, model.makeUser("Meca", "Manlio", "member", "trauma", "manlio.meca@auslromagna.it"));
		submitUser(client, model.makeUser("Circelli", "Alessandro", "member", "trauma", "alessandro.circelli@auslromagna.it"));
		submitUser(client, model.makeUser("Pizzilli", "Giacinto", "member", "trauma", "giacinto.pizzilli@auslromagna.it"));
		submitUser(client, model.makeUser("Oliva", "Alessandro", "member", "trauma", "alessandro.oliva@auslromagna.it"));
		submitUser(client, model.makeUser("Sisto", "Adiletta", "member", "trauma", "diletta.sisto@auslromagna.it"));
		submitUser(client, model.makeUser("Benini", "Beatrice", "member", "trauma", "beatrice.benini@auslromagna.it"));
		submitUser(client, model.makeUser("Cittadini", "Alessio", "member", "trauma", "alessio.cittadini@auslromagna.it"));
		submitUser(client, model.makeUser("Bissoni", "Luca", "member", "trauma", "luca.bissoni@auslromagna.it"));
		submitUser(client, model.makeUser("Tiricanti", "Laura", "member", "trauma", "laura.tiricanti@auslromagna.it"));
		submitUser(client, model.makeUser("Spada", "Tiziana", "member", "trauma", "tiziana.spada@auslromagna.it"));
		submitUser(client, model.makeUser("Mastronardi", "Costantino", "member", "trauma", "costantino.mastronardi@auslromagna.it"));
		submitUser(client, model.makeUser("Vitali", "Sofia", "member", "trauma", "sofia.vitali@auslromagna.it"));
		submitUser(client, model.makeUser("Ravaldini", "Maurizio", "data-analyst", "trauma", "maurizio.ravaldini@auslromagna.it"));

		submitUser(client, model.makeUser("Ricci", "Alessandro", "admin", "admin", "a.ricci@unibo.it"));
		submitUser(client, model.makeUser("Croatti", "Angelo", "admin", "admin", "a.croatti@unibo.it"));
		submitUser(client, model.makeUser("Montagna", "Sara", "data-analyst", "data", "sara.montagna@unibo.it"));
		submitUser(client, model.makeUser("test", "test", "member", "test", "a.ricci@unibo.it"));
		
		
		System.out.println("OK.");
		
	}

	
	public static void clearUsers(HttpClient client) {
		client
		.delete("/gt2/traumatracker/api/users").handler(res -> {
			System.out.println("response status " + res.statusCode());
		})
		.end();

	}

	
	public static void submitUser(HttpClient client, JsonObject user) {		
		Buffer buffer = Buffer.buffer(user.toString());
		client
				.post("/gt2/traumatracker/api/users").handler(res -> {
					System.out.println("response status " + res.statusCode());
				})
				.setTimeout(1000).putHeader("content-type", "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer).end();
	}


}
