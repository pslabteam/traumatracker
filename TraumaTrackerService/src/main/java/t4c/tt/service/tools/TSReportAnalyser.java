package t4c.tt.service.tools;

import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class TSReportAnalyser extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(TSReportAnalyser.class);
	private Vertx vertx;
	
	public TSReportAnalyser(){		
	}
	
	@Override
	public void start() {
		vertx = this.getVertx();
		JsonObject mongoConfig = new JsonObject().put("db_name", "ttservicedb"); //put("host", ipDB).put("port", portDB).;			
		store = MongoClient.createNonShared(vertx, mongoConfig);
		log("DB connection created.");
		
		JsonObject query = new JsonObject();
		store.find("reports", query, res -> {
			System.out.println("Got something "+(res.succeeded() ? "ok" : res.cause()));
			if (res.succeeded()) {				
				List<JsonObject> reps = res.result();				
				System.out.println("N. tot reps " + reps.size());

				List<JsonObject> goodReports = reps.stream().filter(rep -> {
					JsonObject iss = rep.getJsonObject("iss");
					if (iss != null) {
						String totalIss = iss.getString("totalIss");
						if (totalIss != null && !totalIss.trim().equals("")) {
							// System.out.println(totalIss);
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}).collect(Collectors.toList());

				System.out.println("N. reps " + goodReports.size());
				
			} else {
			}
		});
		
	}
	
	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		TSReportAnalyser app = new TSReportAnalyser();
		vertx.deployVerticle(app);				
	}
		
}
