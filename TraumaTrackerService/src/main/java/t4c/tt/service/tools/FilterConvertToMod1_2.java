package t4c.tt.service.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class FilterConvertToMod1_2 extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(FilterConvertToMod1_2.class);
	private Vertx vertx;
	private String targetFile;
	private String sourceFile;
	
	/* for versions prior 1.1 */
	private String oldUsersFile;
	private HashMap<String,String> usersOldNameMap;

	public FilterConvertToMod1_2(String sourceFile, String targetFile) throws Exception {			
		this.targetFile = targetFile;
		this.sourceFile = sourceFile;
	}

	@Override
	public void start() {
		System.out.println("Converting to Domain Model v. 1.2.");
		try {
			FileReader fis = new FileReader(sourceFile);
			BufferedReader br = new BufferedReader(fis);

			List<JsonObject> reps = new ArrayList<JsonObject>();				
			try {
				while (br.ready()) {
					JsonObject obj = new JsonObject(br.readLine());
					reps.add(obj);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				br.close();
			}
			// System.out.println("N. tot reps " + reps.size());

			List<JsonObject> goodReports = reps.stream().map((JsonObject source) ->  {
				JsonObject target = new JsonObject();

				boolean versBefore1_2 = false;
				boolean vers1_2 = false;
				boolean versAfter1_2 = false;

				String version = source.getString("_version");
				if (version == null) {
					version = "1.1";
				}

				if (version.equals("1.1")) {
					versBefore1_2 = true;
				} else if (version.equals("1.2")) {
					vers1_2 = true;
				} else {
					versAfter1_2 = true;
				}

				if (vers1_2 || versAfter1_2) {
					/* no conversion needed*/
					return source;
				} else {
					/* Converting to version 1.1 version of the model */

					System.out.println("Processing report: " + source.getString("_id") + " - source version: " + version);
					
					ifs (source, target, "_id");
					// ifs (source, target, "_version");					
					target.put("version", "1.2");
					ifs (source, target, "startOperatorId");					
					ifs (source, target, "startOperatorDescription");

					ifo (source, target, "delayedActivation");

					ifa (source, target, "traumaTeamMembers");
					ifs (source, target, "startDate");
					ifs (source, target, "startTime");
					ifs (source, target, "endDate");
					ifs (source, target, "endTime");

					ifo (source, target, "iss");
					ifs (source, target, "finalDestination");

					/* compiling TRAUMA INFO */

					JsonObject ti = source.getJsonObject("traumaInfo");
					if (ti != null) {
						ti.remove("type");
						String age = (String) ti.remove("age");
						try {
							if (age != null) {
								ti.put("age", Integer.parseInt(age));
							}
						} catch (Exception ex) {
							// ex.printStackTrace();
							System.out.println("- traumaInfo.age is a range => inserting ageRange");
							ti.put("ageRange", age);
						}
						
					}
					target.put("traumaInfo", ti);
					
					/* in version <= 1.0 it was patient info */ 
					JsonObject pat = source.getJsonObject("patientInfo");
					if (pat != null) {
						JsonObject tr = new JsonObject();
						target.put("traumaInfo", tr);
						ifs (pat, tr, "code");
						ifs (pat, tr, "sdo");
						ifs2b (pat, tr, "erDeceased");
						ifs (pat, tr, "admissionCode");
						ifs (pat, tr, "name");
						ifs (pat, tr, "surname");
						ifs (pat, tr, "gender");
						ifs (pat, tr, "gender");
						ifs (pat, tr, "dob");
						ifs (pat, tr, "age");
						ifs (pat, tr, "accidentDate");
						ifs (pat, tr, "accidentTime");
						ifs (pat, tr, "accidentType");
						ifs (pat, tr, "vehicle");
						ifs2b (pat, tr, "fromOtherEmergency");
						ifs (pat, tr, "otherEmergency");
					}

					ifo (source, target, "majorTraumaCriteria");
					ifo (source, target, "anamnesi");
					ifo (source, target, "preh");
					ifo (source, target, "preh");
					
					/* from startVitalSigns to vitalSigns & clinicalPicture */
					
					JsonObject vs = source.getJsonObject("startVitalSigns");
					if (vs != null) {
						JsonObject tvs = new JsonObject();
						target.put("patientInitialCondition", tvs);
						
						JsonObject vits = new JsonObject();
						tvs.put("vitalSigns", vits);
						
						ifss (vs, vits, "Temp","temp");
						ifss (vs, vits, "HR","hr");
						ifss (vs, vits, "BP","bp");
						ifss (vs, vits, "SpO2","spo2");
						ifss (vs, vits, "EtCO2","etco2");

						JsonObject cp = new JsonObject();
						tvs.put("clinicalPicture", cp);

						ifnn (vs, cp, "GCSTotal","gcsTotal");
						ifss (vs, cp, "GCSMotor","gcsMotor");
						ifss (vs, cp, "GCSVerbal","gcsVerbal");
						ifss (vs, cp, "GCSEyes","gcsEyes");
						ifbb (vs, cp, "Sedated","sedated");
						ifss (vs, cp, "Pupils","pupils");
						
						ifss (vs, cp, "Airway","airway");
						ifbb (vs, cp, "Tracheo","positiveInhalation");
						ifbb (vs, cp, "IntubationFailed","intubationFailed");
						ifss (vs, cp, "ChestTube", "chestTube");
						ifnn (vs, cp, "OxygenPercentage", "oxygenPercentage");
						ifbb (vs, cp, "ExtBleeding","hemorrhage");
						ifbb (vs, cp, "LimbsFracture", "limbsFracture");
						ifbb (vs, cp, "FractureExposition", "fractureExposition");
						ifss (vs, cp, "Burn", "burn");
					}

					JsonArray evl = source.getJsonArray("events");
					if (evl != null && evl.size() > 0) {
						JsonArray nevl = new JsonArray();
						target.put("events", nevl);

						Iterator<Object> it = evl.iterator();
						
						int iEvent = 0;
						
						while (it.hasNext()) {
							try {
								JsonObject ev = (JsonObject) it.next();
	
								JsonObject nev = new JsonObject();
								nevl.add(nev);
	
								try {
									Integer evId = ev.getInteger("eventId");
									if (evId != null) {
										nev.put("eventId", evId);
									} else {
										System.out.println("> found event without eventId (event index: " + iEvent + ")");
									}
								} catch (Exception ex) {
									String evId = ev.getString("eventId");
									System.out.println("> found event with eventId string: " + evId + " (event index: " + iEvent + ")");
									nev.put("eventId", Integer.parseInt(evId));
								}
								
								// ifn (ev, nev, "eventId");
	
								ifs (ev, nev, "date");
								ifs (ev, nev, "time");
								ifs (ev, nev, "place");
								ifs (ev, nev, "type");
	
								String evType = ev.getString("type");
	
								JsonObject co = ev.getJsonObject("content");
								if (co != null) {
									JsonObject nco = new JsonObject();
									nev.put("content",nco);
	
									if (evType.equals("procedure")) {
										ifs (co, nco, "procedureId");
										ifs (co, nco, "procedureDescription");
										ifs (co, nco, "procedureType");
										ifs (co, nco, "event");
										ifb (co, nco, "difficultAirway");
										ifb (co, nco, "inhalation");
										ifb (co, nco, "videolaringo");
										ifb (co, nco, "frova");
										ifb (co, nco, "right");
										ifb (co, nco, "left");
									} else if (evType.equals("diagnostic")) {
										ifs (co, nco, "diagnosticId");
										ifs (co, nco, "diagnosticDescription");
					
										ifn (co, nco, "lactates");
										ifn (co, nco, "be");
										ifn (co, nco, "ph");
										ifn (co, nco, "hb");
	
										ifs (co, nco, "fibtem");
										ifs (co, nco, "extem");
										ifs2b (co, nco, "hyperfibrinolysis");
									} else if (evType.equals("drug")) {
									
										String did = co.getString("drugId");
										
										/* check for blood products */
										if (did.equals("mtp") || 
											did.equals("emazies") ||	
											did.equals("fresh-frozen-plasma") ||
											did.equals("platelets") ||
											did.equals("fibrinogen") ||
											did.equals("prothrombinic-complex")) {
											
											ev.remove("type");										
											nev.put("type","blood-product");										
	
											ifss (co, nco, "drugId","bloodProductId");
											ifss (co, nco, "drugDescription", "bloodProductDescription");
											
											String at = co.getString("administrationType");
											if (at.equals("one-shot")) {
												nco.put("administrationType", at);
											} else {
												nco.put("administrationType", "blood-protocol");
											}
											
											ifn (co, nco, "qty");
											ifs (co, nco, "unit");
											ifs (co, nco, "bagCode");
											
										} else {
											ifs (co, nco, "drugId");
											ifs (co, nco, "drugDescription");
											ifs (co, nco, "administrationType");
											ifs (co, nco, "event");
											ifn (co, nco, "qty");
											ifs (co, nco, "unit");
											ifn (co, nco, "duration");
										}
	
									} else if (evType.equals("vital-sign")) {
										nev.remove("type");
										nev.put("type", "clinical-variation");
										ifss (co, nco, "vsId", "variationId");
										ifss (co, nco, "vsDescription", "variationDescription");
										ifs (co, nco, "value");
										
									} else if (evType.equals("vital-signs-mon")) {
										
										ifn (co, nco, "Temp");
										ifn (co, nco, "HR");
										ifn (co, nco, "DIA");
										ifn (co, nco, "SYS");
										ifn (co, nco, "SpO2");
										ifn (co, nco, "EtCO2");
										ifn (co, nco, "NBP-D");
										ifn (co, nco, "NBP-S");
										
									} else if (evType.equals("photo")) {
									} else if (evType.equals("video")) {
									} else if (evType.equals("vocal-note")) {
									} else if (evType.equals("text-note")) {
										ifs (co, nco, "text");
									} else if (evType.equals("trauma-leader")) {
										ifs (co, nco, "userId");
										ifs (co, nco, "surname");
										ifs (co, nco, "place");
									} else if (evType.equals("room-in")) {
										ifs (co, nco, "place");
									} else if (evType.equals("room-out")) {
										ifs (co, nco, "place");
									} else if (evType.equals("patient-accepted")) {
									} else if (evType.equals("report-reactivation")) {
									}
								}
								iEvent++;
							} catch (Exception ex) {
								System.out.println("- error in processing event: " + iEvent);
								ex.printStackTrace();
							}
						}

					}
					
					JsonArray vso = source.getJsonArray("vitalSignsObservations");
					if (vso != null) {
						target.put("vitalSignsObservations", vso);
					} else {
						JsonArray nevl = new JsonArray();
						target.put("vitalSignsObservations", nevl);
					}
					
					return target;
				}
			}).collect(Collectors.toList());



			System.out.println("Writing file: "+targetFile);
			try {
				FileWriter writer = new FileWriter(targetFile);
				for (JsonObject rep: goodReports) {
					writer.write(rep + "\n");
				}
				writer.close();
				System.out.println("Done: "+goodReports.size() +" reports written.");
				System.exit(0);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			// System.out.println("N. reps " + goodReports.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

	}

	private void ifs(JsonObject source, JsonObject target, String key) {
		String val = source.getString(key);
		if ( val != null && !val.equals("")) { 
			val = val.replaceAll(",", " ");
			target.put(key, val); 
		};
	}

	private void ifss(JsonObject source, JsonObject target, String oldkey, String newkey) {
		String val = source.getString(oldkey);
		if ( val != null && !val.equals("")) { 
			val = val.replaceAll(",", " ");
			target.put(newkey, val); 
		};
	}

	private void ifs2n(JsonObject source, JsonObject target, String key) {
		Object val = source.getValue(key);
		if (val instanceof String) {
			if ( val != null && !val.equals("")) { 
				try {
					target.put(key, Integer.parseInt((String) val)); 
				} catch (NumberFormatException ex) {
					target.put(key, Double.parseDouble((String) val)); 
				}
			}
		} else if (val instanceof Number){
			target.put(key, val); 
		}  /* else {
			throw new IllegalArgumentException();
		} */
	}

	private void ifs2b(JsonObject source, JsonObject target, String key) {
		Object val = source.getValue(key);
		if (val instanceof String) {
			String sval = (String) val;
			if ( sval != null && !sval.equals("")) { 
				if (sval.toLowerCase().equals("si") || (sval.toLowerCase().equals("true"))) {
					target.put(key, true); 
				} else {
					target.put(key, false);
				}
			}
		} else if (val instanceof Boolean){
			target.put(key, val); 
		} /* else {
			throw new IllegalArgumentException();
		} */
	}
	
	private void ifn(JsonObject source, JsonObject target, String key) {
		Double val = source.getDouble(key);
		if ( val != null) { 
			target.put(key, val); 
		};
	}

	private void ifnn(JsonObject source, JsonObject target, String oldkey, String newkey) {
		Double val = source.getDouble(oldkey);
		if ( val != null) { 
			target.put(newkey, val); 
		};
	}

	private void ifb(JsonObject source, JsonObject target, String key) {
		Boolean val = source.getBoolean(key);
		if ( val != null) { 
			target.put(key, val); 
		};
	}

	private void ifbb(JsonObject source, JsonObject target, String oldkey, String newkey) {
		Boolean val = source.getBoolean(oldkey);
		if ( val != null) { 
			target.put(newkey, val); 
		};
	}

	private void ifa(JsonObject source, JsonObject target, String key) {
		JsonArray val = source.getJsonArray(key);
		if ( val != null) { 
			target.put(key, val); 
		};
	}

	private void ifo(JsonObject source, JsonObject target, String key) {
		JsonObject val = source.getJsonObject(key);
		if ( val != null) { 
			target.put(key, val); 
		};
	}

	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	public static void main(String[] args) throws Exception {
		/*
		String destFileName = "data/20181018/updated-reports.json";
		String srcFileName = "data/20181018/reports-with-updated-users.json";
		*/
		System.out.println("-------------------------------------------------------------" );
		System.out.println("TraumaTracker Model version updater - from version 1.1 to 1.2" );
		System.out.println("-------------------------------------------------------------" );
		String destFileName = "data/20190624/reports-updated_1_2.json";
		String srcFileName = "data/20190624/reports.json";		
		
		if (args.length == 2) {
			srcFileName = args[0];
			destFileName = args[1];
		}
		Vertx vertx = Vertx.vertx();
		FilterConvertToMod1_2 app = new FilterConvertToMod1_2(srcFileName, destFileName);
		vertx.deployVerticle(app);				
	}

}
