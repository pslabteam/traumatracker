package t4c.tt.service.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class TSMapNewUsers extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(TSMapNewUsers.class);
	private Vertx vertx;
	private String targetFile;
	private String oldUsersFile;
	private HashMap<String,String> usersOldNameMap;
	
	public TSMapNewUsers(String oldUsersFile,  String targetFile) throws Exception {		
		this.targetFile = targetFile;
		this.oldUsersFile = oldUsersFile;
		
		usersOldNameMap = new HashMap<String,String>();

		BufferedReader fr = new BufferedReader(new FileReader(oldUsersFile));
		while (fr.ready()) {
			String sobj = fr.readLine();
			JsonObject obj = new JsonObject(sobj);
			String userId = obj.getString("userId");
			String name = obj.getString("name").toLowerCase();
			String surname = obj.getString("surname").toLowerCase();
			usersOldNameMap.put(userId, name + "." + surname);
		}
		fr.close();
		/*
{"_id":"agno","name":"Vanni","surname":"Agnoletti","userId":"agno"}
{"_id":"alba","name":"Vittorio","surname":"Albarello","userId":"alba"}
{"_id":"marti","name":"Costanza","surname":"Martino","userId":"marti"}
{"_id":"gambe","name":"Emiliano","surname":"Gamberini","userId":"gambe"}
{"_id":"russo","name":"Emanuele","surname":"Russo","userId":"russo"}
{"_id":"flasa","name":"flavia","surname":"savelli","userId":"flasa"}
{"_id":"mabe","name":"marco","surname":"benni","userId":"mabe"}
{"_id":"giosco","name":"giovanni","surname":"scognamiglio","userId":"giosco"}
{"_id":"giusa","name":"giuseppe","surname":"sabia","userId":"giusa"}
{"_id":"mame","name":"manlio","surname":"meca","userId":"mame"}
{"_id":"alci","name":"alessandro","surname":"circelli","userId":"alci"}
{"_id":"gpizz","name":"Giacinto","surname":"Pizzilli","userId":"gpizz"}
{"_id":"aoliva","name":"Alessandro","surname":"Oliva","userId":"aoliva"}
{"_id":"sadi","name":"Sisto","surname":"Adiletta","userId":"sadi"}
{"_id":"bea","name":"Beatrice","surname":"Benini","userId":"bea"}
{"_id":"citta","name":"Alessio","surname":"Cittadini","userId":"citta"}
{"_id":"bisso","name":"Luca","surname":"Bissoni","userId":"bisso"}
{"_id":"tiri","name":"Laura","surname":"Tirincanti","userId":"tiri"}
{"_id":"sword","name":"Tiziana","surname":"Spada","userId":"sword"}
{"_id":"mastro","name":"Costantino","surname":"Mastronardi","userId":"mastro"}
{"_id":"svitali","name":"Sofia","surname":"Vitali","userId":"svitali"}

		 */
	}
		
	@Override
	public void start() {
		vertx = this.getVertx();
		JsonObject mongoConfig = new JsonObject().put("db_name", "ttservicedb"); //put("host", ipDB).put("port", portDB).;			
		store = MongoClient.createNonShared(vertx, mongoConfig);
		log("DB connection created.");
		
		
		JsonObject query = new JsonObject();
		System.out.println("Fetching reports...");
		store.find("reports", query, res -> {
			// System.out.println("Got something "+(res.succeeded() ? "ok" : res.cause()));
			if (res.succeeded()) {				
				List<JsonObject> reps = res.result();				
				// System.out.println("N. tot reps " + reps.size());

				List<JsonObject> updatedReports = reps.stream().map(rep -> {
					String userId = rep.getString("startOperatorId");

					String newName = this.usersOldNameMap.get(userId);
					if (newName != null) {
						rep.put("startOperatorId", newName);
					}
					
					JsonArray events = rep.getJsonArray("events");
					Iterator<Object> it = events.iterator();
					
					while (it.hasNext()) {
						JsonObject ev = (JsonObject) it.next();
						if (ev.getString("type").equals("trauma-leader")) {
								newName = this.usersOldNameMap.get(ev.getJsonObject("content").getString("userId"));
								if (newName != null) {
									ev.getJsonObject("content").put("userId", newName);
								}
						}
					}
					
					return rep;
					
				}).collect(Collectors.toList());

				System.out.println("Writing file: "+targetFile);
				try {
					FileWriter writer = new FileWriter(targetFile);
					// writer.write("[ ");
					for (JsonObject rep: updatedReports) {
						writer.write(rep + "\n");
					}
					writer.close();
					// writer.write(updatedReports.get(updatedReports.size() - 1) + "]");
					System.out.println("Done.");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				// System.out.println("N. reps " + goodReports.size());
				
			} else {
			}
		});
		
	}
	
	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static void main(String[] args) throws Exception {
		String srcFileName = "data/old-users.json";
		String destFileName = "data/reports-with-updated-users.json";
		if (args.length == 1) {
			destFileName = args[0];
		}
		Vertx vertx = Vertx.vertx();
		TSMapNewUsers app = new TSMapNewUsers(srcFileName, destFileName);
		vertx.deployVerticle(app);				
	}
		
}
