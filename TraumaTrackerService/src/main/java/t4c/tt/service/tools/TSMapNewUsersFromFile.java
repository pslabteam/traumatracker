package t4c.tt.service.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.*;

public class TSMapNewUsersFromFile extends AbstractVerticle {

	private MongoClient store;
	private Logger logger = LoggerFactory.getLogger(TSMapNewUsersFromFile.class);
	private String targetFile;
	private String sourceFile;
	private String oldUsersFile;
	private HashMap<String,String> usersOldNameMap;
	
	public TSMapNewUsersFromFile(String oldUsersFile, String sourceFile, String targetFile) {		
		this.targetFile = targetFile;
		this.oldUsersFile = oldUsersFile;
		this.sourceFile = sourceFile;		
	}
		
	@Override
	public void start() {
		try {

			usersOldNameMap = new HashMap<String,String>();
			BufferedReader fr = new BufferedReader(new FileReader(oldUsersFile));
			while (fr.ready()) {
				String sobj = fr.readLine();
				JsonObject obj = new JsonObject(sobj);
				String userId = obj.getString("userId");
				String name = obj.getString("name").toLowerCase();
				String surname = obj.getString("surname").toLowerCase();
				usersOldNameMap.put(userId, name + "." + surname);
			}
			fr.close();
			
			
			FileReader fis = new FileReader(sourceFile);
			BufferedReader br = new BufferedReader(fis);

			List<JsonObject> reps = new ArrayList<JsonObject>();				
			try {
				while (br.ready()) {
					JsonObject obj = new JsonObject(br.readLine());
					reps.add(obj);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				br.close();
			}
			

				List<JsonObject> updatedReports = reps.stream().map(rep -> {
					String userId = rep.getString("startOperatorId");

					String newName = this.usersOldNameMap.get(userId);
					if (newName != null) {
						rep.put("startOperatorId", newName);
					}
					
					JsonArray events = rep.getJsonArray("events");
					Iterator<Object> it = events.iterator();
					
					while (it.hasNext()) {
						JsonObject ev = (JsonObject) it.next();
						if (ev.getString("type").equals("trauma-leader")) {
								newName = this.usersOldNameMap.get(ev.getJsonObject("content").getString("userId"));
								if (newName != null) {
									ev.getJsonObject("content").put("userId", newName);
								}
						}
					}
					
					return rep;
					
				}).collect(Collectors.toList());

				System.out.println("Writing file: "+targetFile);
				try {
					FileWriter writer = new FileWriter(targetFile);
					// writer.write("[ ");
					for (JsonObject rep: updatedReports) {
						writer.write(rep + "\n");
					}
					writer.close();
					// writer.write(updatedReports.get(updatedReports.size() - 1) + "]");
					System.out.println("Done - " + updatedReports.size() + " reports updated.");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				// System.out.println("N. reps " + goodReports.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
					
	}
	
	public void log(String msg){
		logger.info(msg);
	}

	protected List<JsonObject> elementsFromTo(List<JsonObject> recs, String dateFrom, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0) &&  (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsAfter(List<JsonObject> recs, String dateFrom){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateFrom) >= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}

	protected List<JsonObject> elementsBefore(List<JsonObject> recs, String dateTo){
		return recs.stream().filter(rec -> {
			String startDate = rec.getString("startDate"); // yyyy-mm-dd
			if (startDate != null && !startDate.equals("")) {
				return (startDate.compareTo(dateTo) <= 0);
			} else {
				return false;
			}
		}).collect(Collectors.toList());
	}
	
	public static void main(String[] args) throws Exception {
		String srcUserFileName = "data/20181018/users.json";
		String destRepFileName = "data/20181018/reports-with-updated-users.json";
		String srcRepFileName = "data/20181018/reports.json";
		if (args.length == 1) {
			destRepFileName = args[0];
		}
		Vertx vertx = Vertx.vertx();
		TSMapNewUsersFromFile app = new TSMapNewUsersFromFile(srcUserFileName, srcRepFileName,destRepFileName);
		vertx.deployVerticle(app);				
	}
		
}
