package t4c.tt.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailLib {
	
	private String smtpHost;
	private int smtpPort;
	private String authUser;
	private String authPwd;
	
    private Properties properties = new Properties();
    private Authenticator authenticator;

    private static MailLib instance;
    
    static public MailLib makeInstance(final String smtpHost, final int smtpPort, final String authUser, final String authPwd) {
		synchronized (MailLib.class) {
			instance = new MailLib(smtpHost, smtpPort, authUser, authPwd);
			return instance;
		}
    }
    
    static public MailLib getInstance() {
    	return instance;
    }
    
	private MailLib(final String smtpHost, final int smtpPort, final String authUser, final String authPwd) {
        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.authUser = authUser;
        this.authPwd = authPwd;
        
        configureMailClientProperties();
        
        this.authenticator = generateAuthenticator();
    }
	
	private void configureMailClientProperties() {
		properties.put("mail.smtp.host", smtpHost);
        properties.put("mail.smtp.port", String.valueOf(smtpPort));
        properties.put("mail.smtp.ssl.enable", String.valueOf(true));
        properties.put("mail.smtp.auth", String.valueOf(true));
        properties.put("mail.debug", String.valueOf(false));
	}
	
	private Authenticator generateAuthenticator() {
		return new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(authUser, authPwd);
            }
        };
	}
	
	public void sendEmail(final String fromLabel, final String to, final String subject, final String content) throws MessagingException {
		final MimeMessage message = new MimeMessage(Session.getInstance(properties, authenticator));
		
		message.setFrom(new InternetAddress(String.format("%s <%s>", fromLabel, authUser)));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject(subject);
		message.setText(content);
		
		
		Transport.send(message);
	}
}
