package unused;

import java.io.FileWriter;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TTLogger  {

	private TTLoggerView view;
	private FileWriter logFile;
	private boolean hasGUI;
	private Executor exec;
	
	public TTLogger(boolean hasGUI) {
		this.hasGUI = hasGUI;
		exec = Executors.newSingleThreadExecutor();
		if (hasGUI){
			view = new TTLoggerView();
			view.setVisible(true);
		}
		try {
			logFile = new FileWriter("logTT-"+(new Date().toString().replace(" ", "-").replace(":", ""))+".txt");
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void log(String msg){
		String logmsg = "["+new Date()+"] "+msg;
		if (hasGUI){
			view.log(logmsg);	
		} else {
			synchronized (System.out) {
				System.out.println(logmsg);
			}
		}
		exec.execute(() -> {
			try {
				synchronized(logFile){
					logFile.write(logmsg+"\n");
					logFile.flush();
				}
			} catch (Exception ex){
				ex.printStackTrace();
			}
		});
	}

}
