package unused;

import io.vertx.core.json.*;
import t4c.tt.service.util.InvalidConfigFileException;

public class Config {
	
	protected JsonObject config;
	
	public Config(JsonObject configFile) throws InvalidConfigFileException {	
		config = configFile;
	}
	
	public Config() {
		config = new JsonObject();
	}
	
	
	public int getInt(String key){
		return config.getInteger(key);
	}

	public String getString(String key){
		return config.getString(key);
	}

	public boolean getBoolean(String key){
		return config.getBoolean(key);
	}
	
	public JsonObject getJsonObject(String key){
		return config.getJsonObject(key);
	}
	
	public JsonArray getJsonArray(String key) {
		return config.getJsonArray(key);
	}

}
