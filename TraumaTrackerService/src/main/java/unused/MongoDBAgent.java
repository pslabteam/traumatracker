package unused;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import io.vertx.core.AbstractVerticle;
import io.vertx.ext.mongo.MongoClient;
import t4c.tt.service.util.TTConfig;

public class MongoDBAgent extends AbstractVerticle {

	private int port;
	private String host;
	private MongoClient client;
	private Date lastPingTime;
	private TTLogger view;
	private TTConfig config;

	public MongoDBAgent(TTLogger view, TTConfig config){
		host = "localhost";
		port = 27017;
		this.view = view;
		this.config = config;

	}

	public void start() {
		log("Setting up the MongoDB server.");
		setupServer();
	}

	private void setupServer(){
		this.getVertx().executeBlocking(f -> {
			try {
				Process start = Runtime.getRuntime().exec(new String[]{ /*config.getMongoPath(), "--dbpath", config.getDBPath()*/});
				BufferedReader reader = new BufferedReader(new InputStreamReader(start.getInputStream()));
				reader.lines().forEach( s -> {
					System.out.println(s);
				});
			} catch (Exception ex){
				ex.printStackTrace();
				log("MongoDB already running.");
			}
			f.complete();
		}, res -> {
			if (res.succeeded()) {
				log("MongoDB setup succeeded.");
			} else {
				log("MongoDB setup failed.");
			}
			/*
			try {
				log("Connecting to the DB...");
				MongoClientOptions clientOptions = new MongoClient.Builder()	
						.addServerMonitorListener(new ServerListener())	                
						.build();

				client = new MongoClient(new ServerAddress(host, port), clientOptions);
				log(""+client.getAddress());

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			*/
		});
	}

	private void log(String msg){
		System.out.println("[TTMongoDBAgent] "+msg);
		view.log("[TTMongoDBAgent] "+msg);
	}

	/*
	class ServerListener implements ServerMonitorListener {
		@Override
		public void serverHearbeatStarted(ServerHeartbeatStartedEvent serverHeartbeatStartedEvent) {
			// Ping Started
			// log("Checking is mongodb is alive..");
		}

		@Override
		public void serverHeartbeatSucceeded(ServerHeartbeatSucceededEvent serverHeartbeatSucceededEvent) {
			// Ping Succeed, Connected to server
			lastPingTime = new Date();
			// log("Server is alive.");
		}

		@Override
		public void serverHeartbeatFailed(ServerHeartbeatFailedEvent serverHeartbeatFailedEvent) {
			// Ping failed, server down or connection lost
			log("Server down or connection lost => Setting up the server");
			setupServer();	    	
		}
	}	*/

}
