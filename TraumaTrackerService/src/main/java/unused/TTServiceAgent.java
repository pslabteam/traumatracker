package unused;

import io.vertx.core.AbstractVerticle;
import t4c.tt.service.TTService;
import t4c.tt.service.util.TTConfig;

public class TTServiceAgent extends AbstractVerticle {

	private String ttServiceAddress = "localhost";
	private TTLogger view;
	private TTConfig config;
	
	public TTServiceAgent(TTLogger view, TTConfig config) {
		this.view = view;
		this.config = config;
	}
	
	public void start(){
		log("Setting up TT Service ...");
		setupService();
		
		/*
		getVertx().setPeriodic(10000, id -> {
			log("Check service.");
			WebClient client = getVertx()
				.createHttpClient(new HttpClientOptions().setDefaultHost(ttServiceAddress).setDefaultPort(config.getPort()));				
			
			client.get("http://"+ttServiceAddress + "/gt2/traumatracker/api/state")
				.handler(this::handleGetReportInfo)
				.exceptionHandler(h -> {
					log("exception raised " + h);
					this.getVertx().setTimer(5000, id2 -> {
						setupService();
					});					
				})
			.end();
		});
		*/
	};

	private void setupService(){
		try {
			TTService ttService = new TTService(config);
			this.getVertx().deployVerticle(ttService);
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	/*
	protected void handleGetReportInfo(HttpClientResponse res) {
		log("Check service result: " + res.statusCode());
		res.bodyHandler(data -> {
			JsonObject ob = data.toJsonObject();			
			log("Check service state - version: " + ob.getString("version")+ " | startTime: "+ob.getString("startTime"));
		});
	}
	*/
	

	private void log(String msg){
		System.out.println("[TTServiceAgent] "+msg);
		view.log("[TTServiceAgent] "+msg);
	}	
	
}
