#!/bin/bash

# removes log files in <path/to/traumatracker/TraumaTrackerService>
cd '../'
rm logTT*.txt

# start mongodb
sudo systemctl start mongodb

# check mogodb status
#systemctl status mongodb
