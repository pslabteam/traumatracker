#!/bin/bash

# TT script
echo "[TT MAJORDOMO] Hello Trauma Team. Booting TRAUMA-TRACKER..."

echo "[TT MAJORDOMO] Configuring paths"

TT_HOME=/Users/aricci/Development/Git-Projects/traumatracker/TraumaTrackerService
JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home
PATH=$PATH:/Users/aricci/Development/Tools/gradle-2.5/bin      

cd $TT_HOME

export JAVA_HOME=$JAVA_HOME
export PATH=$PATH

echo "[TT MAJORDOMO] connecting to the VPN."
./scripts/connect_vpn.sh

sleep 5

echo "[TT MAJORDOMO] spawning the DB daemon."
mongod --dbpath $TT_HOME/data/db > logDB &

sleep 5 

echo "[TT MAJORDOMO] spawning TT Service."
gradle runService > logTT &

echo "[TT MAJORDOMO] Done."

