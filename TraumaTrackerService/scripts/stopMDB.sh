#!/bin/bash

# removes log files in <path/to/traumatracker/TraumaTrackerService>
cd '../'
rm logTT*.txt

# stop mongodb
sudo systemctl stop mongodb

