package test;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

public class WebSocketTestServer extends AbstractVerticle {
	
	private static final int LISTENING_PORT = 8083;

	public WebSocketTestServer(final Vertx vertx) {
		this.vertx = vertx;
	}

  @Override
  public void start() throws Exception {
    vertx.createHttpServer()
    	.websocketHandler(ws -> {
    		System.out.println("Connection received from " + ws.remoteAddress());
    		
    		new Thread(new Runnable() {
    			@Override
    			public void run() {
    				for(int i = 0; i < 5 ; i++) {
    					try {
        					Thread.sleep(1000);
        				} catch (InterruptedException e) {
        					e.printStackTrace();
        				}
        				
    					ws.writeTextMessage("myRoom"+i);
    					System.out.println("Sended new place: " + "myRoom"+i);
    				}
    			}
    		}).start();
    		
    	})
    	.listen(LISTENING_PORT);
    
    log("Listening on port " + LISTENING_PORT);
  }
  
  private void log(final String msg) {
	  System.out.println("[" + WebSocketTestServer.class.getSimpleName() + "] " + msg);
  }
  
  public static void main(final String[] args) {
		 
	    try {
			new WebSocketTestServer(Vertx.vertx()).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	  }
}