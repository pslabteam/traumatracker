package resthandler;

import app.ServerAgent;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;

/**
 * Handler for "get any" requests.
 */
public class GetAny extends BaseHandler {
    
    /**
     * Constructor of this class.
     * @param c
     *     Type of collection depending on the request
     */
    public GetAny(final String c) {
        super(c);
    }
    
    @Override
    public void handle(final RoutingContext rc) {
        DB.get().getConnection().find(this.getCollection(), new JsonObject(), res -> {
            if (res.result().size() > 0) {
                final JsonArray ja = new JsonArray();
                res.result().sort((x, y) -> x.getString(DB.GENERIC_OBJECT_ID_KEY).compareTo(y.getString(DB.GENERIC_OBJECT_ID_KEY)));
                res.result().stream().forEach(e -> {
                    e.put(ServerAgent.REST_JSON_ID, e.getString(DB.GENERIC_OBJECT_ID_KEY)).remove(DB.GENERIC_OBJECT_ID_KEY);
                    ja.add(e);
                });
                rc.response().end(ja.encode());
            } else {
                rc.response().end("[]");
            }
        });
    }
}
