package resthandler;

import app.Region;
import app.ServerAgent;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;
import utils.DBCollections;
import utils.GatewayJsonProperty;

/**
 * Handler for REST delete "all" requests (both for tags and gateways) 
 */
public class DeleteAny extends BaseHandler {

    private final ServerAgent server;
    
    /**
     * Constructor  of this class.
     * @param c
     *     The type of collection considered
     * @param b
     *     The event bus used in the application
     * @param sa
     *     The server agent of the application that received the request
     */
    public DeleteAny(final String c, final EventBus b, final ServerAgent sa) {
        super(c, b);
        this.server = sa;
    }

    @Override
    public void handle(final RoutingContext rc) {

        //Check if there are listened tag
        if (this.getCollection().equals(DBCollections.TAGS) && this.server.getListenedTag().size() > 0) {
            rc.response().end(RESTReplyMsg.ANY_OCCUPIED_TAG);
            return;
        }
        
        DB.get().getConnection().removeDocuments(this.getCollection(), new JsonObject(), res -> {
            if (res.succeeded()) {
                if (this.getCollection().equals(DBCollections.REGIONS)) {
                    //Update gateways set region
                    final JsonObject updateQuery = new JsonObject().put(DB.SET_FIELD_UPDATE_OPERATOR, 
                                                                        new JsonObject().put(GatewayJsonProperty.PLACE_PROPERTY, Region.NO_REGION));
                    DB.get().getConnection()
                            .updateCollectionWithOptions(DBCollections.GATEWAYS, new JsonObject(), updateQuery, DB.get().getMultiOpt(), r2 -> {
                        rc.response().end(r2.succeeded() ? RESTReplyMsg.SATISFIED_REQUEST : RESTReplyMsg.GENERIC_DB_ERROR); 
                    });
                } else {
                    this.getBus().publish(
                            this.getCollection().equals(DBCollections.TAGS) ? BusMsgType.SYNC_TAG_DATA : BusMsgType.SYNC_GW_DATA,
                            "");
                    rc.response().end(RESTReplyMsg.SATISFIED_REQUEST);
                }
            } else {
                rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
            }
        });
    }
}
