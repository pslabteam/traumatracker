package resthandler;

import java.util.Optional;
import java.util.function.Predicate;

import io.netty.util.internal.shaded.org.jctools.queues.MessagePassingQueue.Supplier;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;

/**
 * Base handler for REST requests.
 */
public abstract class BaseHandler implements Handler<RoutingContext> {

    private final String collection;
    private final Optional<EventBus> bus;
    
    /**
     * Constructor of this class with collection type.
     * @param c
     *     Type of collection depending on the request
     */
    public BaseHandler(final String c) {
        this.collection = c;
        this.bus = Optional.empty();
    }
    
    /**
     * Constructor of the class with hook to bus.
     * @param c
     *     Type of collection depending on the request
     * @param b
     *     The event bus used in the application
     */
    public BaseHandler(final String c, final EventBus b) {
        this.collection = c;
        this.bus = Optional.of(b);
    }
    
    /**
     * Getter of the collection type to use in the request handler.
     * @return
     *     The type of collection to be used
     */
    protected String getCollection() {
        return this.collection;
    }
    
    /**
     * Getter of the event bus used in the application.
     * @return
     *     The event bus used
     */
    protected EventBus getBus() {
        return this.bus.get();
    }
    
    /**
     * Check correctness of a field.
     * @param p
     *     The predicate used to evaluate correctness.
     * @param s
     *     A supplier of the value of the field
     * @return
     *     True if ti's all ok, false otherwise
     */
    protected boolean baseCheckCorrectness(final Predicate<String> p, final Supplier<String> s) {
        try {
            return p.test(s.get());
        } catch (ClassCastException | NullPointerException e) {
            return false;
        }
    }
    
    /**
     * Check if a item with a specified field value already exists; useful for primary and secondary keys.
     * @param query
     *     The query to use in the research
     * @param f
     *     A future used to do operations in async way
     */
    protected void baseCheckExistence(final JsonObject query, final Future<Boolean> f) {
        DB.get().getConnection().count(this.collection, query, res -> f.complete(res.result() > 0));
    }
    
    /**
     * Check if a item with a specified field value already exists; useful for primary and secondary keys.
     * @param coll
     *     The collection where the query will be evaluated
     * @param query
     *     The query to use in the research
     * @param f
     *     A future used to do operations in async way
     */
    protected void baseCheckExistence(final String coll, final JsonObject query, final Future<Boolean> f) {
        DB.get().getConnection().count(coll, query, res -> f.complete(res.result() > 0));
    }
    
    /**
     * Check if the id is already in use.
     * @param id
     *     The "id" field value
     * @param f
     *     A future used to do operations in async way
     */
    protected void checkIdExistence(final String id, final Future<Boolean> f) {
        this.baseCheckExistence(new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, id), f);
    }
    
    @Override
    public abstract void handle(RoutingContext rc);
    
    /**
     * Elaborate the request but handles exceptions (DecodeException, ...). 
     * @param rc
     *     The routing context of the request
     * @param f
     *     The real handler of the request
     */
    protected void handleWihException(final RoutingContext rc, final CallableHandler f) {
        try {
            f.call(rc);
        } catch (DecodeException e) {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST); //Failed to parse body to JSON
        }
    }
}
