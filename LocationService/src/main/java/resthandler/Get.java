package resthandler;

import app.ServerAgent;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;

/**
 * Handler for get requests.
 */
public class Get extends BaseHandler {
    
    /**
     * Constructor of this class.
     * @param c
     *     Type of collection depending on the request
     */
    public Get(final String c) {
        super(c);
    }
    
    @Override
    public void handle(final RoutingContext rc) {
        final JsonObject query = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, rc.request().getParam(ServerAgent.REST_JSON_ID));
        DB.get().getConnection().find(this.getCollection(), query, res -> {
            if (res.succeeded()) {
                if (res.result().size() > 0) {
                    final JsonObject jo = res.result().get(0);
                    jo.put(ServerAgent.REST_JSON_ID, jo.getString(DB.GENERIC_OBJECT_ID_KEY)).remove(DB.GENERIC_OBJECT_ID_KEY);          
                    rc.response().end(jo.encode());
                } else {
                    rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
                }
            } else {
                rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
            }
        });   
    }
}
