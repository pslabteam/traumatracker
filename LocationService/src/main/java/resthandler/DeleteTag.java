package resthandler;

import app.ServerAgent;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;

/**
 * Delete tag REST request handler.
 */
public class DeleteTag extends TagBaseHandler {

    private final ServerAgent server;
    
    /**
     * Constructor of this class.
     * @param b
     *     The event bus used in the application
     * @param sa
     *     The server agent of the application that received the request
     */
    public DeleteTag(final EventBus b, final ServerAgent sa) {
        super(b);
        this.server = sa;
    }

    @Override
    public void handle(final RoutingContext rc) {
        final JsonObject tag = new JsonObject().put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        final Future<Boolean> occupiedFuture = Future.future();
        
        //Check id
        if (this.checkIdCorrectness(tag)) {
            this.checkIdExistence(tag.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check id occupied
        idFuture.setHandler(res -> {
            if (idFuture.result()) {
                this.checkIfOccupied(tag.getString(ServerAgent.REST_JSON_ID), occupiedFuture, this.server.getListenedTag());
            } else {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
                return;
            }
        });
        
        //Delete tag
        occupiedFuture.setHandler(res -> {
            if (occupiedFuture.result()) {
                rc.response().end(RESTReplyMsg.OCCUPIED_TAG);
            } else {
                final JsonObject query = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, tag.getString(ServerAgent.REST_JSON_ID));
                DB.get().getConnection().removeDocument(this.getCollection(), query, r -> {
                    if (r.succeeded()) {
                        this.getBus().publish(BusMsgType.SYNC_TAG_DATA, "");
                        rc.response().end(RESTReplyMsg.SATISFIED_REQUEST);
                    } else {
                        rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                    }
                });
            }
        });
    }
}
