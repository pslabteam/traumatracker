package resthandler;

import app.ServerAgent;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;

/**
 * Handler for regions' POST requests.
 */
public class PostRegion extends RegionBaseHandler implements CallableHandler {

    /**
     * Constructor of this class.
     * @param bus
     *     The event bus used in the application
     */
    public PostRegion(final EventBus bus) {
        super(bus);
    }

    @Override
    public void handle(final RoutingContext rc) {
        this.handleWihException(rc, this::call);   
    }

    @Override
    public void call(final RoutingContext rc) {
        final JsonObject region = rc.getBody().toJsonObject();
        region.put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        final Future<Boolean> nameFuture = Future.future();
        
        //Check id
        if (this.checkIdCorrectness(region)) {
            this.checkIdExistence(region.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check name
        if (this.checkNameCorrectness(region)) {
            nameFuture.complete();
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Insert region
        CompositeFuture.all(idFuture, nameFuture).setHandler(res -> {
            if (idFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            } else {
                region.put(DB.GENERIC_OBJECT_ID_KEY, region.getString(ServerAgent.REST_JSON_ID)).remove(ServerAgent.REST_JSON_ID);
                DB.get().getConnection().insert(this.getCollection(), region, r -> {
                    rc.response().end(r.succeeded() ? RESTReplyMsg.SATISFIED_REQUEST : RESTReplyMsg.GENERIC_DB_ERROR);
                });
            }
        });
    }
}
