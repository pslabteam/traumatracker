package resthandler;

import io.vertx.ext.web.RoutingContext;

/**
 * Functional interface responsible for the request handler calling.
 */
public interface CallableHandler {

    /**
     * Function that will be called.
     * @param rc
     *     The routing context of the request
     */
    void call(RoutingContext rc);
}
