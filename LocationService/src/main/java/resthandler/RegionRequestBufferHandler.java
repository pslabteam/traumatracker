package resthandler;

import java.util.concurrent.ArrayBlockingQueue;

import app.ServerAgent;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;
import utils.DBCollections;

public class RegionRequestBufferHandler extends Thread {
    
    private static final int MAX_RC_LIST_SIZE = 10;
    
    private final EventBus bus;

    private String tagData;
    private Future<Void> waitTagInfo;
    private final ArrayBlockingQueue<RoutingContext> rcQueue = new ArrayBlockingQueue<>(MAX_RC_LIST_SIZE);
    private boolean freeHandler;
    
    /**
     * Constructor of this class.
     * @param b
     *     The event bus used in the application
     */
    public RegionRequestBufferHandler(final EventBus b) {
        this.bus = b;
        this.freeHandler = true;
        b.consumer(BusMsgType.SND_TAG_REGION, msg -> {
            this.tagData = msg.body().toString(); //Set current tag data
            this.waitTagInfo.complete(); //"Notify" that it can proceed on retrieving data about regions and send reply
        });
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                //Take the next request if the previous one has been processed, this way it avoid requests' overlapping
                if (this.freeHandler) {
                    this.process(this.rcQueue.take());
                } 
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Add a new request in the queue.
     * @param rc
     *     The RoutingContext associated to the new request to be satisfied
     */
    public void addReq(final RoutingContext rc) {
        this.rcQueue.offer(rc);
    }
    
    private void process(final RoutingContext rc) {

        this.freeHandler = false;
        final String regionId = rc.request().getParam(ServerAgent.REST_JSON_ID);
        this.waitTagInfo = Future.future(); //Reset future
        this.bus.publish(BusMsgType.ASK_TAG_REGION, regionId); //Ask for tag current location
        this.waitTagInfo.setHandler(h -> { //Retrieve regions and send reply only when I already have all data about tags
            final JsonObject query = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, regionId);
            DB.get().getConnection().find(DBCollections.REGIONS, query, res -> {
                if (res.succeeded()) {
                    if (res.result().size() > 0) {
                        final JsonObject jo = res.result().get(0);
                        jo.put(ServerAgent.REST_JSON_ID, jo.getString(DB.GENERIC_OBJECT_ID_KEY)).remove(DB.GENERIC_OBJECT_ID_KEY);
                        jo.put("tags", this.tagData);
                        rc.response().end(jo.encode());    
                        this.freeHandler = true;
                    } else {
                        rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
                    }
                } else {
                    rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                }
            });  
        });
    }
}
