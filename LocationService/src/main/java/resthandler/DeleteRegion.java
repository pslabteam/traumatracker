package resthandler;

import app.Region;
import app.ServerAgent;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.DBCollections;
import utils.GatewayJsonProperty;

public class DeleteRegion extends RegionBaseHandler {

    /**
     * Constructor of this class.
     * @param bus
     *     The event bus used in the application
     */
    public DeleteRegion(final EventBus bus) {
        super(bus);
    }

    @Override
    public void handle(final RoutingContext rc) {
        final JsonObject region = new JsonObject().put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
    
        //Check id
        if (this.checkIdCorrectness(region)) {
            this.checkIdExistence(region.getString(ServerAgent.REST_JSON_ID), idFuture);   
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Delete region
        idFuture.setHandler(res -> {
            if (!idFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            }
            else {
                final JsonObject query = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, region.getString(ServerAgent.REST_JSON_ID));
                DB.get().getConnection().removeDocuments(this.getCollection(), query, r -> {
                    if (r.succeeded()) {
                        //Update gateway associated region
                        final JsonObject selectQuery = new JsonObject().put(GatewayJsonProperty.PLACE_PROPERTY, region.getString(ServerAgent.REST_JSON_ID));
                        final JsonObject gwUpdate = new JsonObject().put(GatewayJsonProperty.PLACE_PROPERTY, Region.NO_REGION);
                        final JsonObject updateQuery = new JsonObject().put(DB.SET_FIELD_UPDATE_OPERATOR, gwUpdate);
                        DB.get().getConnection().updateCollection(DBCollections.GATEWAYS, selectQuery, updateQuery, r2 -> {
                            rc.response().end(r2.succeeded() ? RESTReplyMsg.SATISFIED_REQUEST : RESTReplyMsg.GENERIC_DB_ERROR);
                        });
                    } else {
                        rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                    } 
                });
            }
        });
    }

}
