package resthandler;

import app.ServerAgent;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;
import utils.TagJsonProperty;

/**
 * Handler for REST put requests regarding tags.
 */
public class PutTag extends TagBaseHandler implements CallableHandler {

    private final ServerAgent server;
    
    /**
     * Constructor of this class.
     * @param b
     *     The event bus used in the application
     *  @param sa
     *     The server agent of the application that received the request
     */
    public PutTag(final EventBus b, final ServerAgent sa) {
        super(b);
        this.server = sa;
    }

    @Override
    public void handle(final RoutingContext rc) {
        this.handleWihException(rc, this::call);
    }

    @Override
    public void call(final RoutingContext rc) {
        final JsonObject tag = rc.getBody().toJsonObject();
        tag.put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        final Future<Boolean> serialFuture = Future.future();
        final Future<Boolean> occupiedFuture = Future.future();
        
        //Check id
        if (this.checkIdCorrectness(tag)) {
            this.checkIdExistence(tag.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }

        //Check serial
        if (this.checkSerialCorrectness(tag)) {
            this.checkSerialExistence(tag.getInteger(TagJsonProperty.SERIAL), serialFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check if occupied
        idFuture.setHandler(h -> {
            if (idFuture.result()) {
                this.checkIfOccupied(tag.getString(ServerAgent.REST_JSON_ID), occupiedFuture, this.server.getListenedTag());
            } else {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
                return;
            }
        });

        //Update tag
        CompositeFuture.all(serialFuture, occupiedFuture).setHandler(res -> {
            if (occupiedFuture.result()) {
                rc.response().end(RESTReplyMsg.OCCUPIED_TAG);
            } else if (serialFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            } else {
                final JsonObject selectQuery = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, tag.getString(ServerAgent.REST_JSON_ID));
                final JsonObject updateField = new JsonObject().put(TagJsonProperty.SERIAL, tag.getInteger(TagJsonProperty.SERIAL));
                final JsonObject updateQuery = new JsonObject().put(DB.SET_FIELD_UPDATE_OPERATOR, updateField);
                DB.get().getConnection().updateCollection(this.getCollection(), selectQuery, updateQuery, r -> {
                    if (r.succeeded()) {
                        this.getBus().publish(BusMsgType.SYNC_TAG_DATA, "");
                        rc.response().end(RESTReplyMsg.SATISFIED_REQUEST);
                    } else {
                        rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                    }
                });
            }
        });
    }
}
