package resthandler;

import app.ServerAgent;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.DBCollections;
import utils.GatewayJsonProperty;

public abstract class GatewayBaseHandler extends BaseHandler {

    private static final String ID_REGEX_CHECK = "gw[0-9]{2,3}";
    private static final String IP_REGEX_CHECK = "((25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])";
    private static final int THRESHOLD_MIN = -100;
    private static final int THRESHOLD_MAX = 0;
    
    /**
     * Constructor of this class.
     * @param b
     *     Reference to the bus to use
     */
    public GatewayBaseHandler(final EventBus b) {
        super(DBCollections.GATEWAYS, b);
    }
    
    /**
     * Check the correctness of the of the id field.
     * @param tag
     *     The gw expressed as a JSON object
     * @return
     *     True if it's all ok, false otherwise
     */
    protected boolean checkIdCorrectness(final JsonObject gw) {
        return this.baseCheckCorrectness(s -> s.matches(ID_REGEX_CHECK), () -> gw.getString(ServerAgent.REST_JSON_ID));
    }
    
    /**
     * Check the correctness of the of the ipAddress field.
     * @param gw
     *     The gw expressed as a JSON object
     * @return
     *     True if it's all ok, false otherwise
     */
    protected boolean checkAddressCorrectness(final JsonObject gw) {
        return this.baseCheckCorrectness(s -> s.matches(IP_REGEX_CHECK), () -> gw.getString(GatewayJsonProperty.ADDRESS_PROPERTY));
    }
    
    /**
     * Check if the address is already in use.
     * @param address
     *     The "id" field value
     * @param f
     *     A future used to do operations in async way
     */
    protected void checkAddressExistence(final String address, final Future<Boolean> f) {
        this.baseCheckExistence(new JsonObject().put(GatewayJsonProperty.ADDRESS_PROPERTY, address), f);
    }
    
    /**
     * Check the correctness of the of the place field.
     * @param gw
     *     The gw expressed as a JSON object
     * @return
     *     True if it's all ok, false otherwise
     */
    protected boolean checkPlaceCorrectness(final JsonObject gw) {
        return this.baseCheckCorrectness(s -> s.matches(RegionBaseHandler.ID_REGEX_CHECK), () -> gw.getString(GatewayJsonProperty.PLACE_PROPERTY));
    }
    
    /**
     * Check if the place exists.
     * @param address
     *     The "id" field value
     * @param f
     *     A future used to do operations in async way
     */
    protected void checkPlaceExistence(final String place, final Future<Boolean> f) {
        this.baseCheckExistence(DBCollections.REGIONS, new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, place), f);
    }
    
    /**
     * Check the correctness of the threshold field.
     * @param gw
     *     The gw expressed as a JSON object
     * @return
     *     True if it's all ok, false otherwise
     */
    protected boolean checkThresholdCorrectness(final JsonObject gw) {
        return this.baseCheckCorrectness(s -> Integer.valueOf(s) >= THRESHOLD_MIN &&  Integer.valueOf(s) <= THRESHOLD_MAX, 
                                         () -> gw.getInteger(GatewayJsonProperty.THRESHOLD_PROPERTY).toString());
    }
    
    @Override
    public abstract void handle(RoutingContext rc);
}
