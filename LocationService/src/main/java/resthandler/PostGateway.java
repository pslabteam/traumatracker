package resthandler;

import app.ServerAgent;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;
import utils.GatewayJsonProperty;

/**
 * Handler for POST requests regarding gateways.
 */
public class PostGateway extends GatewayBaseHandler implements CallableHandler {

    /**
     * Constructor of this class.
     * @param b
     *     Reference to the bus to use
     */
    public PostGateway(final EventBus b) {
        super(b);
    }
    
    @Override
    public void handle(final RoutingContext rc) {
        this.handleWihException(rc, this::call);
    }

    @Override
    public void call(final RoutingContext rc) {
        final JsonObject gw = rc.getBody().toJsonObject();
        gw.put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        final Future<Boolean> addressFuture = Future.future();
        final Future<Boolean> placeFuture = Future.future();
        final Future<Boolean> thresholdFuture = Future.future();
    
        //Check id
        if (this.checkIdCorrectness(gw)) {
            this.checkIdExistence(gw.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }

        //Check address
        if (this.checkAddressCorrectness(gw)) {
            this.checkAddressExistence(gw.getString(GatewayJsonProperty.ADDRESS_PROPERTY), addressFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check place
        if (this.checkPlaceCorrectness(gw)) {
            this.checkPlaceExistence(gw.getString(GatewayJsonProperty.PLACE_PROPERTY), placeFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check threshold
        thresholdFuture.complete(this.checkThresholdCorrectness(gw));
        
        //Insert gw
        CompositeFuture.all(idFuture, addressFuture, placeFuture, thresholdFuture).setHandler(res -> {
            if (idFuture.result() || addressFuture.result() || !placeFuture.result() || !thresholdFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            } else {
                gw.put(DB.GENERIC_OBJECT_ID_KEY, gw.getString(ServerAgent.REST_JSON_ID)).remove(ServerAgent.REST_JSON_ID);
                gw.put(GatewayJsonProperty.ONLINE_PROPERTY, false);
                DB.get().getConnection().insert(this.getCollection(), gw, r -> {
                    if (r.succeeded()) {
                        this.getBus().publish(BusMsgType.SYNC_GW_DATA, "");
                        rc.response().end(RESTReplyMsg.SATISFIED_REQUEST);
                    } else {
                        rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                    }
                });
            }
        }); 
    }
}
