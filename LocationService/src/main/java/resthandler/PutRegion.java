package resthandler;

import app.ServerAgent;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;

/**
 * Handler for PUT requests regarding regions.
 */
public class PutRegion extends RegionBaseHandler implements CallableHandler {

    /**
     * Constructor of this class.
     * @param bus
     *     The event bus used in the application
     */
    public PutRegion(final EventBus bus) {
        super(bus);
    }

    @Override
    public void handle(final RoutingContext rc) {
        this.handleWihException(rc, this::call);  
    }

    @Override
    public void call(final RoutingContext rc) {
        final JsonObject region = rc.getBody().toJsonObject();
        region.put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        final Future<Boolean> nameFuture = Future.future();
        
        //Check id
        if (this.checkIdCorrectness(region)) {
            this.checkIdExistence(region.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check name
        nameFuture.complete(this.checkNameCorrectness(region));  
        
        //Update region
        CompositeFuture.all(idFuture, nameFuture).setHandler(res -> {
            if (!idFuture.result() || !nameFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            } else {
                final String id = region.getString(ServerAgent.REST_JSON_ID);
                region.remove(ServerAgent.REST_JSON_ID);
                final JsonObject selectQuery = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, id);
                final JsonObject updateQuery = new JsonObject().put(DB.SET_FIELD_UPDATE_OPERATOR, region);
                DB.get().getConnection().updateCollection(this.getCollection(), selectQuery, updateQuery, r -> {
                rc.response().end(r.succeeded() ? RESTReplyMsg.SATISFIED_REQUEST : RESTReplyMsg.GENERIC_DB_ERROR);
                });
            }
        });
    }
}
