package resthandler;

import app.ServerAgent;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import utils.DBCollections;
import utils.RegionJsonProperty;

/**
 * Base handler for regions' requests; it contains some basic common functions.
 */
public abstract class RegionBaseHandler extends BaseHandler {

    /**
     * Regex id of regions.
     */
    public static final String ID_REGEX_CHECK = "region[0-9]{2,3}";
    private static final int PLACE_NAME_MAX_LEN = 30;
    
    /**
     * Constructor of this class.
     * @param b
     *     Reference to the bus to use
     */
    public RegionBaseHandler(final EventBus b) {
        super(DBCollections.REGIONS, b);
    }
    
    /**
     * Check the correctness of the of the id field.
     * @param region
     *     The region expressed as a JSON object
     * @return
     *     True if it's all ok, false otherwise
     */
    protected boolean checkIdCorrectness(final JsonObject region) {
        return this.baseCheckCorrectness(s -> s.matches(ID_REGEX_CHECK), () -> region.getString(ServerAgent.REST_JSON_ID));
    }
    
    /**
     * Check the correctness of the of the place name field.
     * @param region
     *     The region expressed as a JSON object
     * @return
     *     True if it's all ok, false otherwise
     */
    protected boolean checkNameCorrectness(final JsonObject region) {
        return this.baseCheckCorrectness(s -> s.length() <= PLACE_NAME_MAX_LEN, () -> region.getString(RegionJsonProperty.NAME_PROPERTY));
    }
}
