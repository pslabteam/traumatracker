package resthandler;

import app.ServerAgent;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;

/**
 * Delete gateway REST request handler.
 */
public class DeleteGateway extends GatewayBaseHandler {

    /**
     * Constructor of this class.
     * @param b
     *     The event bus used in the application
     */
    public DeleteGateway(final EventBus b) {
        super(b);
    }

    @Override
    public void handle(final RoutingContext rc) {
        final JsonObject gw = new JsonObject().put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        
        //Check id
        if (this.checkIdCorrectness(gw)) {
            this.checkIdExistence(gw.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Delete gw
        idFuture.setHandler(res -> {
            if (!idFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            } else {
                final JsonObject query = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, gw.getString(ServerAgent.REST_JSON_ID));
                DB.get().getConnection().removeDocument(this.getCollection(), query, r -> {
                    if (r.succeeded()) {
                        this.getBus().publish(BusMsgType.SYNC_GW_DATA, "");
                        rc.response().end(RESTReplyMsg.SATISFIED_REQUEST);
                    } else {
                        rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                    }
                });
            }
        });
    }
}
