package resthandler;

import app.ServerAgent;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;
import utils.DBCollections;
import utils.GatewayJsonProperty;

/**
 * Handler for REST put requests regarding gateways.
 */
public class PutGateway extends GatewayBaseHandler implements CallableHandler {

    /**
     * Constructor of this class.
     * @param b
     *     The event bus used in the application
     */
    public PutGateway(final EventBus b) {
        super(b);
    }

    @Override
    public void handle(final RoutingContext rc) {
        this.handleWihException(rc, this::call);
    }

    @Override
    public void call(final RoutingContext rc) {
        final JsonObject gw = rc.getBody().toJsonObject();
        gw.put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        final Future<Boolean> addressFuture = Future.future();
        final Future<Boolean> addressFuture2 = Future.future();
        final Future<Boolean> placeFuture = Future.future();
        final Future<Boolean> thresholdFuture = Future.future();
    
        //Check id
        if (this.checkIdCorrectness(gw)) {
            this.checkIdExistence(gw.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check address
        if (this.checkAddressCorrectness(gw)) {
            this.checkAddressExistence(gw.getString(GatewayJsonProperty.ADDRESS_PROPERTY), addressFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        addressFuture.setHandler(r -> {
            if (addressFuture.result()) { //IP is in use but we need to check that it's not the IP of the gw itself!
                final JsonObject query = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, gw.getString(ServerAgent.REST_JSON_ID));
                DB.get().getConnection().find(DBCollections.GATEWAYS, query, res -> {
                    //false -> ok because IP occupied by such gw; true -> IP occupied by another gw 
                    addressFuture2.complete(!res.result().get(0).getString(GatewayJsonProperty.ADDRESS_PROPERTY).equals(gw.getString(GatewayJsonProperty.ADDRESS_PROPERTY)));
                });
            } else {
                addressFuture2.complete(false); //false -> IP free
            }
        });
        
        //Check place
        if (this.checkPlaceCorrectness(gw)) {
            this.checkPlaceExistence(gw.getString(GatewayJsonProperty.PLACE_PROPERTY), placeFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check threshold
        thresholdFuture.complete(this.checkThresholdCorrectness(gw));
        
        //Update gw
        CompositeFuture.all(idFuture, addressFuture2, placeFuture, thresholdFuture).setHandler(res -> {
            if (!idFuture.result() || addressFuture2.result() || !placeFuture.result() || !thresholdFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            } else {
                final String id = gw.getString(ServerAgent.REST_JSON_ID);
                gw.remove(ServerAgent.REST_JSON_ID);
                final JsonObject selectQuery = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, id);
                final JsonObject updateQuery = new JsonObject().put(DB.SET_FIELD_UPDATE_OPERATOR, gw);
                DB.get().getConnection().updateCollection(this.getCollection(), selectQuery, updateQuery, r -> {
                    if (r.succeeded()) {
                        this.getBus().publish(BusMsgType.SYNC_GW_DATA, "");
                        rc.response().end(RESTReplyMsg.SATISFIED_REQUEST);
                    } else {
                        rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                    }
                });
            }
        });
    }
}
