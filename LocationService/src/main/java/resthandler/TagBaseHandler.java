package resthandler;

import java.util.Set;

import app.ServerAgent;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.DBCollections;
import utils.TagJsonProperty;

/**
 * Handler for requests about tags; it has some basic common basic useful functions for tags handle.
 */
public abstract class TagBaseHandler extends BaseHandler {

    private static final String ID_REGEX_CHECK = "tag[0-9]{2,3}";
    private static final String SERIAL_REGEX_CHECK = "[0-9]{1,4}";
    
    /**
     * Constructor of this class.
     * @param b
     *     Reference to the bus to use
     */
    public TagBaseHandler(final EventBus b) {
        super(DBCollections.TAGS, b);
    }
    
    /**
     * Check the correctness of the of the id field.
     * @param tag
     *     The tag expressed as a JSON object
     * @return
     *     True if it's all ok, false otherwise
     */
    protected boolean checkIdCorrectness(final JsonObject tag) {
        return this.baseCheckCorrectness(s -> s.matches(ID_REGEX_CHECK), () -> tag.getString(ServerAgent.REST_JSON_ID));
    }
    
    /**
     * Check the correctness of the serial number parameter.
     * @param tag
     *     The tag expressed as a JSON object
     * @return
     *     True if ti's all ok, false otherwise
     */
    protected boolean checkSerialCorrectness(final JsonObject tag) {
        return this.baseCheckCorrectness(s-> s.matches(SERIAL_REGEX_CHECK), () -> tag.getInteger(TagJsonProperty.SERIAL).toString());
    }
    
    /**
     * Check if the serial number is already in use (indeed it's a secondary key).
     * @param serial
     *     The "serial" field value
     * @param f
     *     A future used to do operations in async way
     */
    protected void checkSerialExistence(final int serial, final Future<Boolean> f) {
        this.baseCheckExistence(new JsonObject().put(TagJsonProperty.SERIAL, serial), f);
    }
    
    /**
     * It checks if a tag is occupied or not.
     * @param id
     *     The id of the tag
     * @param f
     *     A future to do async operations
     * @param tagList
     *     A list of currently occupied tags
     */
    protected void checkIfOccupied(final String id, final Future<Boolean> f, final Set<Integer> tagList) {
        final JsonObject query = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, id);
        DB.get().getConnection().find(DBCollections.TAGS, query, r -> {
            final int serial = r.result().get(0).getInteger(TagJsonProperty.SERIAL);
            f.complete(tagList.contains(serial));
        });
    }
    
    @Override
    public abstract void handle(RoutingContext rc);
}
