package resthandler;

import io.vertx.core.json.JsonObject;

public class RESTReplyMsg {
    
    /**
     * Satisfied request msg.
     */
    public static final String SATISFIED_REQUEST = createReply(0, "Resquest satisfied correctly!");
    
    /**
     * Non compliant request error msg.
     */
    public static final String NON_COMPLIANT_REQUEST = createReply(1, "Request rejected, it's not compliant to the protocol!");
    
    /**
     * Generic error msg.
     */
    public static final String GENERIC_DB_ERROR = createReply(2, "An error occured while doing the selected operation, please try again!");
    
    
    /**
     * Occupied tag, impossible to update.
     */
    public static final String OCCUPIED_TAG = createReply(3, "The tag is currently occupied!");
    
    /**
     * At least 1 occupied tag, impossible to delete all.
     */
    public static final String ANY_OCCUPIED_TAG = createReply(4, "At least one tag is occupied!");
    
    private static final String CODE_KEY = "code";
    private static final String MSG_KEY = "msg";
    
    private static String createReply(final int code, final String msg) {
        return new JsonObject()
                .put(CODE_KEY, code)
                .put(MSG_KEY, msg)
                .encode();
    }
}
