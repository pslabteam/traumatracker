package resthandler;

import app.ServerAgent;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;
import utils.TagJsonProperty;

/**
 * Handler of POST requests.
 */
public class PostTag extends TagBaseHandler implements CallableHandler {
    
    private static final int INIT_BATTERY_VALUE = 100;
    private static final String INIT_TIMESTAMP_VALUE = "Not detected yet";
    
    /**
     * Constructor of this class.
     * @param b
     *     Reference to the bus to use
     */
    public PostTag(final EventBus b) {
        super(b);
    }

    @Override
    public void handle(final RoutingContext rc) {
        this.handleWihException(rc, this::call);
    }

    @Override
    public void call(final RoutingContext rc) {
        final JsonObject tag = rc.getBody().toJsonObject();
        tag.put(ServerAgent.REST_JSON_ID, rc.request().getParam(ServerAgent.REST_JSON_ID));
        final Future<Boolean> idFuture = Future.future();
        final Future<Boolean> serialFuture = Future.future();

        //Check id
        if (this.checkIdCorrectness(tag)) {
            this.checkIdExistence(tag.getString(ServerAgent.REST_JSON_ID), idFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Check serial
        if (this.checkSerialCorrectness(tag)) {
            this.checkSerialExistence(tag.getInteger(TagJsonProperty.SERIAL), serialFuture);
        } else {
            rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            return;
        }
        
        //Insert tag
        CompositeFuture.all(idFuture, serialFuture).setHandler(res -> {
            if (idFuture.result() || serialFuture.result()) {
                rc.response().end(RESTReplyMsg.NON_COMPLIANT_REQUEST);
            } else {
                final JsonObject t = new JsonObject()
                    .put(DB.GENERIC_OBJECT_ID_KEY, tag.getString(ServerAgent.REST_JSON_ID))
                    .put(TagJsonProperty.SERIAL, tag.getInteger(TagJsonProperty.SERIAL))
                    .put(TagJsonProperty.BATTERY, INIT_BATTERY_VALUE)
                    .put(TagJsonProperty.TIMESTAMP, INIT_TIMESTAMP_VALUE);
                DB.get().getConnection().insert(this.getCollection(), t, r -> {
                    if (r.succeeded()) {
                        this.getBus().publish(BusMsgType.SYNC_TAG_DATA, "");
                        rc.response().end(RESTReplyMsg.SATISFIED_REQUEST);
                    } else {
                        rc.response().end(RESTReplyMsg.GENERIC_DB_ERROR);
                    }
                });
            }
        });
    }
}
