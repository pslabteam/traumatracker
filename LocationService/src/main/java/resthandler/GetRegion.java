package resthandler;

import io.vertx.core.eventbus.EventBus;
import io.vertx.ext.web.RoutingContext;

/**
 * Handler for get requests.
 */
public class GetRegion extends BaseHandler {

    private final RegionRequestBufferHandler bh;
    
    /**
     * Constructor of this class.
     * @param c
     *     Type of collection depending on the request
     * @param b
     *     The event bus used in the application
     */
    public GetRegion(final String c, final EventBus b) {
        super(c, b);

        this.bh = new RegionRequestBufferHandler(b);
        this.bh.start();
    }
    
    @Override
    public void handle(final RoutingContext rc) {
        this.bh.addReq(rc);
    }
}
