package utils;

/**
 * Regions' JSON properties utility class
 */
public class RegionJsonProperty {

    /**
     * Key used for the region "name" property.
     */
    public static final String NAME_PROPERTY = "name";    
}
