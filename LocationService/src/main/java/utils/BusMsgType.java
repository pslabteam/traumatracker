package utils;

/**
 * Utility class of the possible msg(s) that could be sent on the "vertx" bus.
 */
public final class BusMsgType {

    /**
     * Notify the "Server agent" that a web socket must be notified because of new info about the listened beacon.
     * Data required to send the notification are omitted.
     */
    public static final String NOTIFY_SOCKET = "notifySocket";

    /**
     * It represents a notification to send to a web socket (all required data are already in the msg body).
     */
    public static final String NOTIFICATION_TO_SEND = "notificationToSend";

    /**
     * Request of the info about a specified beacon. 
     */
    public static final String GET_INIT_TAG_INFO = "initTagInfo";
    
    /**
     * It notifies that the data about GWs must be synchronized with the one in the DB.
     */
    public static final String SYNC_GW_DATA = "syncGWData";
    
    /**
     * It notifies that the data about tags must be synchronized with the one in the DB.
     */
    public static final String SYNC_TAG_DATA = "syncTagData";
    
    /**
     * It asks someone to send the data about location of tags.
     */
    public static final String ASK_TAG_REGION = "askTagRegion";
    
    /**
     * It contains the data about location of tags.
     */
    public static final String SND_TAG_REGION = "sndTagRegion";

    private BusMsgType() { }
}
