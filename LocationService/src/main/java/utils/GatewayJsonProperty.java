package utils;

/**
 * Gateways' JSON properties utility class.
 */
public class GatewayJsonProperty {

    /**
     * Name of the "room name" property used in the DB.
     */
    public static final String PLACE_PROPERTY = "place";
    
    /**
     * Name of the "IP address" property used in the DB.
     */
    public static final String ADDRESS_PROPERTY = "ipAddress";
    
    /**
     * Name of the "threshold" property used in the DB.
     */
    public static final String THRESHOLD_PROPERTY = "threshold";
    
    /**
     * Name of the "online" status property used in the DB.
     */
    public static final String ONLINE_PROPERTY = "online";
}
