package utils;

public final class DBCollections {

    /**
     * Tags collection.
     */
    public static final String TAGS = "tags";
    
    /**
     * Gateways collection.
     */
    public static final String GATEWAYS = "gateways";
    
    /**
     * Regions colletion.
     */
    public static final String REGIONS = "regions";
}
