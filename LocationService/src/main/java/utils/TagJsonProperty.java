package utils;

/**
 * Tags' JSON properties utility class.
 */
public final class TagJsonProperty {

    /**
     * The tag main "id" property name.
     */
    public static final String ID = "id";
    
    /**
     * The tag "serial number" property name (key).
     */
    public static final String SERIAL = "serialNumber";
    
    /**
     * The tag "serial number" property name used in incoming BlueUp JSON data.
     */
    public static final String BLUEUP_SERIAL = "serial";

    /**
     * The tag "battery" property name (key).
     */
    public static final String BATTERY = "battery";
    
    /**
     * The tag "timestamp" property name (key).
     */
    public static final String TIMESTAMP = "timestamp";
    
    /**
     * The tag "RSSI" property name (key).
     */
    public static final String RSSI = "rssi";
    
    /**
     * The current region id where the tag currently is.
     */
    public static final String PLACE = "place";
    
    private TagJsonProperty() { }
}
