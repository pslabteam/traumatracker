package utils;

import io.vertx.core.json.JsonObject;

/**
 * Standard web-socket reply.
 */
public class WebsocketReply {

    public static final String WEBSOCKET_REPLY_ID_KEY = "tagId";
    public static final String WEBSOCKET_REPLY_PLACE_KEY = "place";

    private final JsonObject reply;
    
    /**
     * Constructor of this class.
     * @param tagId
     *    Id of the tag
     * @param tagPlace
     *    Place where the tag currently is
     */
    public WebsocketReply(final String tagId, final String tagPlace) {
        this.reply = new JsonObject().put(WEBSOCKET_REPLY_ID_KEY, tagId)
                                     .put(WEBSOCKET_REPLY_PLACE_KEY, tagPlace);
    }
    
    /**
     * Encode the JSON reply.
     * @return
     *     The JSON reply as encoded string
     */
    public String encode() {
        return this.reply.encode();
    }
}
