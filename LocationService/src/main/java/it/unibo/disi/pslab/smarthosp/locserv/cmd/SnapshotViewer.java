package it.unibo.disi.pslab.smarthosp.locserv.cmd;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.*;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import io.vertx.core.json.*;

public class SnapshotViewer extends JFrame {

	private JTextArea gwView;
	private JTextArea tagView;
	
	public SnapshotViewer() {
		super(".:: Beacon TAG Monitor ::.");
		this.initComponents();
	}

	private void initComponents() {         
         
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));

        gwView = new JTextArea();
        gwView.setColumns(20);
        gwView.setLineWrap(true);
        gwView.setRows(25);
        gwView.setWrapStyleWord(true);        
        mainPanel.add(gwView);
        
        tagView = new JTextArea();
        tagView.setColumns(60);
        tagView.setLineWrap(true);
        tagView.setRows(25);
        tagView.setWrapStyleWord(true);        
        mainPanel.add(tagView);

        Container contentPane = getContentPane();
        contentPane.add(mainPanel, BorderLayout.CENTER);
        
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();         
    }
	
	public void update(JsonObject snap) {
		SwingUtilities.invokeLater(() -> {
			StringBuffer sb = new StringBuffer();
			JsonArray gwInfo = snap.getJsonArray("gwInfo");
			Iterator<Object> it = gwInfo.iterator();
			while (it.hasNext()) {
				JsonObject gwo = (JsonObject) it.next();
				sb.append("GW "+gwo.getString("gwId") + " - region: " + gwo.getString("region") + "\n");
				sb.append("- IP "+gwo.getString("gwIp") + "\n");
				JsonArray ptags = gwo.getJsonArray("perceivedTags");
				for (int i = 0; i < ptags.size(); i++) {
					JsonObject ptag = ptags.getJsonObject(i);
					sb.append("- TAG " + ptag.getString("tag") + " RSSI: " + ptag.getInteger("rssi") + " Battery: " + ptag.getInteger("battery") + "\n");
				}
				sb.append("\n");
			}
			gwView.setText(sb.toString());
			
			JsonArray tagInfo = snap.getJsonArray("tagInfo");
			it = tagInfo.iterator();
			sb.setLength(0);
			
			while (it.hasNext()) {
				JsonObject tag = (JsonObject) it.next();
				// BeaconTag.TagGWInfo loc = tag.getStrongestGWInfo();
				sb.append("TAG "+tag.getString("tagId") + " - Battery: " + tag.getInteger("battery"));
				JsonObject strongest = tag.getJsonObject("strongestGW");
				
				if (strongest != null) {
					long dt = System.currentTimeMillis() - strongest.getLong("ts");
					sb.append(" - Strongest GW: " + strongest.getString("gwId") + " | " + strongest.getString("region") + " (RSSI: " + strongest.getInteger("rssi") + ", time: " + dt + ") ");
					String stable = tag.getString("stableGWId");
					
					if (stable != null) {
						sb.append("- Stable GW: " + stable +" | " + tag.getString("stableGWReg") + "\n");
					} else {
						sb.append("\n");
					}
				} else {
					sb.append("\n");
				}
				
				JsonArray gwi = tag.getJsonArray("gws");
				// Collection<BeaconTag.TagGWInfo> gws = tag.getGWInfo();
				Iterator<Object> it2 = gwi.iterator();
				while (it2.hasNext()) {
					JsonObject gw = (JsonObject) it2.next();
					long dt = System.currentTimeMillis() - gw.getLong("ts");
					sb.append("- GW " + gw.getString("gwId") + " | " + gw.getString("region")+ " RSSI: " + gw.getInteger("rssi") + "  time: " + dt + "\n");
				}
				
				JsonArray hs = tag.getJsonArray("history");
				it2 = hs.iterator();
				sb.append("- HISTORY: [ ");
				if (it2.hasNext()) {
					JsonObject last = (JsonObject) it2.next();
					sb.append(last.getString("gwId"));
					long ref = last.getLong("dt");
					while (it2.hasNext()) {
						JsonObject ti = (JsonObject) it2.next();
						sb.append(", " + ti.getString("gwId") + " (for "+ (ref - ti.getLong("dt")) + "ms)");
						ref = ti.getLong("dt");
					}
				}
				sb.append(" ]\n");
				sb.append("- CURRENT REGIONS: [ ");

				boolean first = true;
				JsonArray regs = tag.getJsonArray("regions");
				it2 = regs.iterator();
				
				while (it2.hasNext()) {
					String name = (String) it2.next();
					sb.append(first ? name : ", " + name );
					first = false;
				};
				sb.append(" ]\n\n");
						
			}
			tagView.setText(sb.toString());
		});
	}
}
