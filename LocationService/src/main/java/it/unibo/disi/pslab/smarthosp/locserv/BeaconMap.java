package it.unibo.disi.pslab.smarthosp.locserv;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.smarthosp.locserv.BeaconTag.TagRegionInfo;

public class BeaconMap {

    // private static final int INFO_MAX_AGE = 5000; // milliseconds
    private HashMap<String, BeaconTag> tagMap;
    private HashMap<String, BeaconGateway> gwMap;

    public BeaconMap() {
		tagMap = new HashMap<String, BeaconTag>();
		gwMap = new HashMap<String, BeaconGateway>();
    }
    
    public synchronized void addTag(String sid) {
        BeaconTag btag = new BeaconTag(sid);
        // btag.addListener(this);
        tagMap.put(sid, btag);
    }
    
    public synchronized void checkHistory() {
		for (BeaconTag tag: tagMap.values()) {
			tag.checkHistory();
		};
    }
    
    public synchronized void checkFreshness(int maxAge) {
		for (BeaconTag tag: tagMap.values()) {
			tag.checkFreshness(maxAge);
		};

		for (BeaconGateway gw: gwMap.values()) {
			gw.checkFreshness(maxAge);
		}
    }
    
    public synchronized JsonObject getTagInfo(String sid) {
		BeaconTag btag = tagMap.get(sid);
		if (btag != null) {
			JsonObject reply = new JsonObject();
			JsonArray array = new JsonArray();
			btag.getCurrentRegions().forEach(reg -> {
        			JsonObject r = new JsonObject();
        			r.put("name", reg.getName());
        			r.put("rssi", reg.getRSSI());
        			array.add(r);
			});
        		reply
    				.put("tagId", btag.getSerial())
    				.put("battery", btag.getBattery())
    				.put("places", array);            
        		return reply;
    		} else {
    			return null;
    		}
    }
    
    public synchronized JsonArray getAllTagsInfo() {
		JsonArray array = new JsonArray();
		tagMap.values().forEach(btag -> {
			JsonObject obj = new JsonObject();
			JsonArray regs = new JsonArray();
			btag.getCurrentRegions().forEach(reg -> {
        			JsonObject r = new JsonObject();
        			r.put("name", reg.getName());
        			r.put("rssi", reg.getRSSI());
        			regs.add(r);
			});
			obj
				.put("tagId", btag.getSerial())
				.put("battery", btag.getBattery())
				.put("places", regs);            
			array.add(obj);
		});
		return array;
    }
    
    public synchronized void updateGWInfo(String sid, int battery, BeaconGateway gw,  int rssi) throws NoTagException {
    	BeaconTag btag = tagMap.get(sid);
    	
        if (btag != null) {
            btag.updateGWInfo(gw, battery, rssi, System.currentTimeMillis());
        } else {
        	throw new NoTagException();
        }
    }
    
    public synchronized JsonObject getSnapshot() {
		
    		JsonArray tags = new JsonArray();
		for (BeaconTag tag: tagMap.values()) {
			JsonObject ta = new JsonObject();
			ta.put("tagId", tag.getSerial());
			ta.put("battery",tag.getBattery());
			BeaconTag.TagGWInfo loc = tag.getStrongestGWInfo();
			if (loc != null) {
				JsonObject st = new JsonObject();
				st.put("gwId", loc.getGW().getId());
				st.put("rssi", loc.getRSSI());
				st.put("region", loc.getGW().getRegion());
				st.put("ts", loc.getTimestamp());
				ta.put("strongestGW", st);
								
				BeaconTag.TagGWInfo stable = tag.getStableGWInfo();
				if (stable != null) {
					ta.put("stableGWId", tag.getStableGWInfo().getGW().getId());
					ta.put("stableGWReg", tag.getStableGWInfo().getGW().getRegion());
				} else {
					ta.put("stableGWId", "");
				}
			} else {
				ta.put("strongestGW", (JsonObject) null);
				ta.put("stableGWId", "");
			}

			JsonArray gwi = new JsonArray();
			Collection<BeaconTag.TagGWInfo> gws = tag.getGWInfo();
			for (BeaconTag.TagGWInfo gw: gws) {
				JsonObject bt = new JsonObject();
				bt.put("gwId", gw.getGW().getId() );
				bt.put("rssi", gw.getRSSI());
				bt.put("region", gw.getGW().getRegion());
				bt.put("ts",  gw.getTimestamp());
				gwi.add(bt);
			}
			ta.put("gws", gwi);
			
			BeaconTag.TagGWHistory his = tag.getTagHistory();
			JsonArray hs = new JsonArray();
			Iterator<BeaconTag.TagGWHistory.TagHistoricalInfo> it = his.getHistory().iterator();
			if (it.hasNext()) {
				BeaconTag.TagGWHistory.TagHistoricalInfo last = it.next();
				long ref = last.getTimestamp();
				JsonObject hi = new JsonObject();
				if (last.isValidTag()) {
					hi.put("gwId", last.getGWInfo().getGW().getId());
				} else {
					hi.put("gwId", "-");
				}
				hi.put("dt",0);
				hs.add(hi);
				while (it.hasNext()) {
					BeaconTag.TagGWHistory.TagHistoricalInfo ti = it.next();
					JsonObject hin = new JsonObject();
					if (ti.isValidTag()) {
						hin.put("gwId", ti.getGWInfo().getGW().getId());
					} else {
						hin.put("gwId", "-");
					}
					hin.put("dt", ti.getTimestamp() - ref);
					hs.add(hin);
					ref = ti.getTimestamp();
				}
			}
			ta.put("history", hs);
			JsonArray regs = new JsonArray();
			for (TagRegionInfo reg: tag.getCurrentRegions()) {
				regs.add(reg.getName());
			};
			ta.put("regions", regs);
			tags.add(ta);
		}		
		
		JsonArray gwa = new JsonArray();
		for (BeaconGateway gw: this.gwMap.values()) {
			JsonObject gwo = new JsonObject();
			gwo.put("gwIp", gw.getIp());
			gwo.put("gwId", gw.getId());
			gwo.put("region", gw.getRegion());
			JsonArray ts = new JsonArray();
			Collection<BeaconGateway.GWPerceivedTag> ptags = gw.getTags().values();
			for (BeaconGateway.GWPerceivedTag tag: ptags) {
				JsonObject ta = new JsonObject();
				ta.put("tag", tag.getSerial());
				ta.put("rssi", tag.getRSSI());
				ta.put("battery", tag.getBattery());
				ta.put("ts", tag.getLastUpdateTimestamp());
				ts.add(ta);
			}
			gwo.put("perceivedTags", ts);
			gwa.add(gwo);
		}
		
		JsonObject snap = new JsonObject();
		snap.put("tagInfo", tags);
		snap.put("gwInfo", gwa);		
		return snap;
    }

    public synchronized Collection<BeaconGateway> getCurrentGateways() {
		return gwMap.values();
    }


    public synchronized void addGW(String id, String ip, int threshold, String region) {
    		gwMap.put(ip, new BeaconGateway(id, ip, threshold, region));
    }

    
}
