package it.unibo.disi.pslab.smarthosp.locserv.cmd;

import io.vertx.*;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class TestNotifySnapshots {

	static public void main(String[] args) {

		final Vertx vertx = Vertx.vertx();
		HttpClient client = vertx.createHttpClient();
		 
		client.websocket(8083, "localhost", "/gt2/locationservice/api/snapshot", websocket -> {
			  System.out.println("Connected!");
			  websocket.textMessageHandler(msg -> {
				 JsonObject obj = new JsonObject(msg); 
				 System.out.println(obj.encodePrettily());
			  });
		});

	}
}
