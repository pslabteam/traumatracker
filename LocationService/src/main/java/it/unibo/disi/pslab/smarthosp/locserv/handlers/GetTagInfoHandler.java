package it.unibo.disi.pslab.smarthosp.locserv.handlers;

import io.vertx.core.eventbus.*;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.*;

public class GetTagInfoHandler extends AbstractServiceHandler {

	public GetTagInfoHandler(LocationService service) {
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		EventBus bus = this.getService().getVertx().eventBus();		
		String tagId = routingContext.request().getParam("tagID");	
		JsonObject msg = this.getService().getBeaconMap().getTagInfo(tagId);
		response.putHeader("content-type", "application/json").end(msg.encodePrettily());
	}

}
