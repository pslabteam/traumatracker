package it.unibo.disi.pslab.smarthosp.locserv;

import java.util.*;

import utils.TagJsonProperty;

public class BeaconGateway {
    
    private final String id;
    private final String ip;
    private final int threshold;
    private final String region;
    
    private HashMap<String, GWPerceivedTag> tags;
    
    public BeaconGateway( String gwId, String gatewayIp, int threshold, String region) {
        this.ip = gatewayIp;
        this.id = gwId;
        this.threshold = threshold;
        this.region = region;
        this.tags = new HashMap<String,GWPerceivedTag>();
    }

    public String getIp() {
        return this.ip;
    }
    
    public String getId() {
        return this.id;
    }
    
    public int getThreshold() {
    		return threshold;
    }
    
    public String getRegion() {
    		return region;
    }
    
    public void checkFreshness(long maxAge) {
        List<GWPerceivedTag> tagsToRemove = new ArrayList<GWPerceivedTag>();
        long now =  System.currentTimeMillis();
    		tags.values().forEach(tag -> {
    			long age = now - tag.getLastUpdateTimestamp();
    			if (age > maxAge) {
    				tagsToRemove.add(tag);
    			}
    		});
    		tagsToRemove.forEach(tag -> tags.remove(tag.getSerial()));
    }
    
    public void updateTagInfo(int serial, int battery, int rssi) {
        String id = ""+serial;
        GWPerceivedTag tag = tags.get(id);
        if (tag == null) {
        		tag = new GWPerceivedTag(id, battery, rssi);
        		tags.put(id, tag);
        }
        tag.update(battery, rssi);
    }
    
    public HashMap<String,GWPerceivedTag> getTags() {
        return tags;
    }

    
    class GWPerceivedTag {
        
        private String serial;
        private int rssi;
        private int battery;
        private long lastUpdateTimestamp;

         public GWPerceivedTag(String serial, int rssi, int battery) {
            this.serial = serial;
            this.battery = battery;
            this.rssi = rssi;
            lastUpdateTimestamp = System.currentTimeMillis();
        }

        public String getSerial() {
            return this.serial;
        }
        

        public void update(int battery, int rssi) {
            this.battery = battery;
            this.rssi = rssi;
            lastUpdateTimestamp = System.currentTimeMillis();
        }
        
        public int getBattery() {
        		return battery;
        }
        
        public long getLastUpdateTimestamp() {
        		return lastUpdateTimestamp;
        }
        
        public int getRSSI() {
            return this.rssi;
        }
    }    
}
