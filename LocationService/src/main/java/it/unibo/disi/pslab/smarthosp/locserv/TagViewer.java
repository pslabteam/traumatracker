package it.unibo.disi.pslab.smarthosp.locserv;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.*;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import it.unibo.disi.pslab.smarthosp.locserv.BeaconTag.TagRegionInfo;

public class TagViewer extends JFrame {

	private JTextArea gwView;
	private JTextArea tagView;
	
	public TagViewer() {
		super(".:: Beacon TAG Monitor ::.");
		this.initComponents();
	}

	private void initComponents() {         
         
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));

        gwView = new JTextArea();
        gwView.setColumns(20);
        gwView.setLineWrap(true);
        gwView.setRows(25);
        gwView.setWrapStyleWord(true);        
        mainPanel.add(gwView);
        
        tagView = new JTextArea();
        tagView.setColumns(60);
        tagView.setLineWrap(true);
        tagView.setRows(25);
        tagView.setWrapStyleWord(true);        
        mainPanel.add(tagView);

        Container contentPane = getContentPane();
        contentPane.add(mainPanel, BorderLayout.CENTER);
        pack();         
    }
	
	public void update(Collection<BeaconGateway> gwList, Collection<BeaconTag> tagList) {
		SwingUtilities.invokeLater(() -> {
			StringBuffer sb = new StringBuffer();
			for (BeaconGateway gw: gwList) {
				sb.append("GW "+gw.getIp() + "\n");
				Collection<BeaconGateway.GWPerceivedTag> tags = gw.getTags().values();
				for (BeaconGateway.GWPerceivedTag tag: tags) {
					sb.append("- TAG " + tag.getSerial() + " RSSI: " + tag.getRSSI() + " Battery: " + tag.getBattery() + "\n");
				}
				sb.append("\n");
			}
			gwView.setText(sb.toString());

			sb.setLength(0);
			for (BeaconTag tag: tagList) {
				BeaconTag.TagGWInfo loc = tag.getStrongestGWInfo();
				sb.append("TAG "+tag.getSerial() + " - Battery: " + tag.getBattery());
				if (loc != null) {
					long dt = System.currentTimeMillis() - loc.getTimestamp();
					sb.append(" - Strongest GW: " + loc.getGW().getId() + " (RSSI: " + loc.getRSSI() + ", time: " + dt + ") ");
					
					BeaconTag.TagGWInfo stable = tag.getStableGWInfo();
					if (stable != null) {
						sb.append("- Stable GW: " + tag.getStableGWInfo().getGW().getId() +"\n");
					} else {
						sb.append("\n");
					}
				} else {
					sb.append("\n");
				}
				Collection<BeaconTag.TagGWInfo> gws = tag.getGWInfo();
				for (BeaconTag.TagGWInfo gw: gws) {
					long dt = System.currentTimeMillis() - gw.getTimestamp();
					sb.append("- GW " + gw.getGW().getId() + " RSSI: " + gw.getRSSI() + "  time: " + dt + "\n");
				}
				
				BeaconTag.TagGWHistory his = tag.getTagHistory();
				sb.append("- HISTORY: [ ");
				Iterator<BeaconTag.TagGWHistory.TagHistoricalInfo> it = his.getHistory().iterator();
				if (it.hasNext()) {
					BeaconTag.TagGWHistory.TagHistoricalInfo last = it.next();
					if (last.isValidTag()) {
						sb.append(last.getGWInfo().getGW().getId());
					} else {
						sb.append("-");
					}
					long ref = last.getTimestamp();
					while (it.hasNext()) {
						BeaconTag.TagGWHistory.TagHistoricalInfo ti = it.next();
						if (ti.isValidTag()) {
							sb.append(", " + ti.getGWInfo().getGW().getId()+" (for "+ (ref - ti.getTimestamp()) + "ms)");
						} else {
							sb.append(", - (for "+ (ref - ti.getTimestamp()) + "ms)");
						}
						ref = ti.getTimestamp();
					}
				}
				sb.append(" ]\n");
				sb.append("- CURRENT REGIONS: [ ");
				boolean first = true;
				for (TagRegionInfo reg: tag.getCurrentRegions()) {
					sb.append(first ? reg.getName() : ", " + reg.getName() );
					first = false;
				};
				sb.append(" ]\n\n");
						
			}
			tagView.setText(sb.toString());
		
		});
	}
}
