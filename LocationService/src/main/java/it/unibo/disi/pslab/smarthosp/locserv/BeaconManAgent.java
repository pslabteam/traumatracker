package it.unibo.disi.pslab.smarthosp.locserv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import app.DB;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.smarthosp.utils.*;
import it.unibo.disi.pslab.smarthosp.locserv.BeaconTag.TagRegionInfo;

public class BeaconManAgent extends AbstractVerticle /* implements BeaconTagListener */ { 
	
    private static final int DEFAULT_REQUEST_PORT = 80;
    private static final String RELATIVE_URL = "/api/beacons/blueup";

    private static final int BEACON_GW_TIMEOUT = 30000; // milliseconds
    private static final int BEACON_MAP_UPDATE_PERIOD = 500; // milliseconds
    private static final int TAG_HISTORY_REFRESH_PERIOD = 1000; // milliseconds
    private static final int CHECK_FRESHNESS_PERIOD = 2000; // milliseconds    
    private static final int INFO_MAX_AGE = 5000; // milliseconds
    
    private HttpClient client;
    private BeaconMap bmap;
    
    private TagViewer viewer;
    private static boolean VIEW_ENABLED = false; 
    
	private LSConfig config;
	private LSLogger lsLogger;
    
    public BeaconManAgent(BeaconMap bmap, LSLogger logger, LSConfig config) {
		this.config = config;
		this.lsLogger = logger;
    		this.bmap = bmap; 
	}
	
	public void start() {
		
		viewer = new TagViewer();
		
		if (VIEW_ENABLED) {
			viewer.setVisible(true);
		};
		
        final Vertx v = getVertx();
        
        DB.get().start(v);
        
        this.client = v.createHttpClient();
       
        Future<Void> gwCompleted = Future.future();
        Future<Void> tagCompleted = Future.future();

        /* retrieve data config about GW and Tags */
        
        syncGWData(gwCompleted);
        syncTagData(tagCompleted);
        
        
        
        CompositeFuture.all(gwCompleted, tagCompleted).setHandler(res -> {
       		if (res.succeeded()) {
       			//System.out.println(bmap.getTagInfo("3047").encodePrettily());
        		this.defineHandlers();
          	} else {
          		try {
          			this.stop();
          		} catch (Exception ex) {
          		}
       		}
        });
        
	}
	
	private void defineHandlers() {			
		/* periodic retrieval of data from beacon GW */
		
        final Vertx v = getVertx();
        
        /* periodic refresh of the beacon map */
        
        v.setPeriodic(BEACON_MAP_UPDATE_PERIOD, id -> {
    			for (BeaconGateway gw: bmap.getCurrentGateways()) {
    				this.getTagInfoFromGW(gw);
    			};
        });

		/* periodic refresh of the tag history info */
	
		v.setPeriodic(TAG_HISTORY_REFRESH_PERIOD, id -> {
			bmap.checkHistory();
		});
		
		/* periodic freshness check */
		
		v.setPeriodic(CHECK_FRESHNESS_PERIOD, id -> {
			bmap.checkFreshness(INFO_MAX_AGE);
		});	
		
	}
	

	private void getTagInfoFromGW(BeaconGateway gw) {
		this.client.get(DEFAULT_REQUEST_PORT, gw.getIp(), RELATIVE_URL, res -> {
            res.bodyHandler(buf -> {
                try {
                		final JsonObject tagObj = buf.toJsonObject();
                		final JsonArray tagList = tagObj.getJsonArray("beacons");     
	           		tagList.forEach(tagobj -> {
	                		JsonObject tag = (JsonObject) tagobj;	
	                     int serial = tag.getInteger(TagJsonProperty.BLUEUP_SERIAL);
	                     int battery = tag.getInteger(TagJsonProperty.BATTERY);
	                     int rssi = tag.getInteger(TagJsonProperty.RSSI);
	                     gw.updateTagInfo(serial, battery, rssi);
	                    
	             	 	String sid = "" + serial;
	             	 	try {
	             	 		bmap.updateGWInfo(sid, battery, gw, rssi);
	             	 	} catch (Exception ex) {
	             	 		ex.printStackTrace();
	             	 	}
	                });
                } catch (Exception ex) {
                		// ex.printStackTrace();
                }
            });
		}).exceptionHandler(exc -> { 
			log("Exeption fetching from " + gw.getId() + " | " + gw.getIp() + " \n(" + exc.getMessage() + ")");
		}).setTimeout(BEACON_GW_TIMEOUT)
		.end();
	}
	
	private void syncGWData(Future<Void> future) {
		DB.get().getConnection().find(DBCollections.GATEWAYS, new JsonObject(), res -> {
	            if (res.succeeded()) {
	                res.result().stream()
	                            .forEach(g -> {
	                                String id = g.getString(DB.GENERIC_OBJECT_ID_KEY);
	                                String ip = g.getString(GatewayJsonProperty.ADDRESS_PROPERTY);
	                                int threshold = g.getInteger(GatewayJsonProperty.THRESHOLD_PROPERTY);
	                                String region = g.getString(GatewayJsonProperty.PLACE_PROPERTY);
	                                bmap.addGW(id, ip, threshold, region);
	                            });
	                future.complete();
	            } else {
	            		future.fail("DB error about GW");
	                log("Failed to read gateways set from the DB.");
	            }
	        });
	}
	
    private void syncTagData(Future<Void> future) {
        DB.get().getConnection().find(DBCollections.TAGS, new JsonObject(), res -> {
            if (res.succeeded()) {
                res.result().stream()
                            .forEach(t -> {
                                // Fill again map
                                int id = t.getInteger(TagJsonProperty.SERIAL);
                                String sid = "" + id;
                                bmap.addTag(sid);
                            });      
                	future.complete();     
            } else {
        			future.fail("DB error about Tags");
                log("Failed to read tags set from the DB.");
            }
        });
    }
	
	private void log(String msg) {
		lsLogger.log("[GWMon] " + msg);
	}

}
