package it.unibo.disi.pslab.smarthosp.locserv;

import java.util.Collection;

import it.unibo.disi.pslab.smarthosp.locserv.BeaconTag.TagRegionInfo;

interface BeaconTagListener {
	void notifyPrimaryRegionUpdated(String tagId, int battery, Collection<TagRegionInfo> regions);
}
