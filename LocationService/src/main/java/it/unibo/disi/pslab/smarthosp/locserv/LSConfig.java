package it.unibo.disi.pslab.smarthosp.locserv;

import io.vertx.core.json.JsonArray;
import it.unibo.disi.pslab.smarthosp.utils.Config;
import it.unibo.disi.pslab.smarthosp.utils.InvalidConfigFileException;

public class LSConfig extends Config {

	private static final String portKey = "port";
	private static final String dbNameKey = "dbName";
	private static final String tagsCollectionNameKey = "tagsCollectionName";
	private static final String gwCollectionNameKey = "gwCollectionName";
	private static final String lsVersionKey = "version";
	private static final String dbPathKey = "dbPath";
	private static final String mongoPathKey = "mongoPath";
	private static final String logGUIEnabled = "logGUIEnabled";
	private static final String defaultUsers = "defaultUsers";
	
	public LSConfig(String fileName) throws InvalidConfigFileException {
		super(fileName);
	}
	
	/* default */
	public LSConfig(){
		config.put(dbNameKey, "ttlocationdb");
		config.put(portKey, 8083);
		config.put(tagsCollectionNameKey, "tags");
		config.put(gwCollectionNameKey, "gateways");
		config.put(lsVersionKey,"1.0");
		config.put(dbPathKey,"./data/db");
		config.put(mongoPathKey,"/usr/local/bin/mongod");
		config.put(logGUIEnabled,false);
	}
	
	public String getMongoPath() {
		return config.getString(mongoPathKey);
	}
	
	public String getDBPath() {
		return config.getString(dbPathKey);
	}
	
	public String getDBName(){
		return config.getString(dbNameKey);
	}

	public String getVersion(){
		return config.getString(lsVersionKey);
	}
	
	public int getPort(){
		return config.getInteger(portKey);		
	}
	
	public String getTagsCollectionName(){
		return config.getString(tagsCollectionNameKey);
	}

	public String getGWCollectionName(){
		return config.getString(gwCollectionNameKey);
	}
	
	public boolean isLogGUIEnabled() {
		return config.getBoolean(logGUIEnabled);
	}
	
	public JsonArray getDefUsers() {
		return config.getJsonArray(defaultUsers);
	}

}
