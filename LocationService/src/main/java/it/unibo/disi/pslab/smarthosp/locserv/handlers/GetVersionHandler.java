package it.unibo.disi.pslab.smarthosp.locserv.handlers;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.AbstractServiceHandler;
import it.unibo.disi.pslab.smarthosp.locserv.LocationService;

public class GetVersionHandler extends AbstractServiceHandler{
	
	private String version;
	
	public GetVersionHandler(LocationService service) {
		super(service);
		version = service.getConfig().getVersion();
	}
	
	public void handle(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		JsonObject reply = new JsonObject().put("version", version);
		response.putHeader("content-type", "application/json").end(reply.encodePrettily());
	}
}
