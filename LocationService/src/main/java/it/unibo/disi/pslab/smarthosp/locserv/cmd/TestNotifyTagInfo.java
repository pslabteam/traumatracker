package it.unibo.disi.pslab.smarthosp.locserv.cmd;

import io.vertx.*;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class TestNotifyTagInfo {

	static public void main(String[] args) {

		final Vertx vertx = Vertx.vertx();
		HttpClient client = vertx.createHttpClient();
		 
		client.websocket(8083, "localhost" /*"172.25.142.41"*/, "/gt2/locationservice/api/tags/3047", websocket -> {
			  System.out.println("Connected!");
			  websocket.textMessageHandler(msg -> {
				 System.out.println("REC msg: " + msg);
				 JsonObject obj = new JsonObject(msg); 
				 String tid = obj.getString("tagId");
				 JsonArray arr = obj.getJsonArray("places");
				 if (tid != null && arr != null) {
					 System.out.println("TAG " + tid + " " + arr.encodePrettily());
				 }
			  });
		});

	}
}
