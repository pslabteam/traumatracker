package it.unibo.disi.pslab.smarthosp.locserv.cmd;

import java.util.HashMap;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.smarthosp.locserv.LSConfig;
import it.unibo.disi.pslab.smarthosp.locserv.LSLogger;

public class BeaconViewerAgent extends AbstractVerticle { 
	
	private SnapshotViewer viewer;
	private LSConfig config;
	private LSLogger lsLogger;
    
	public BeaconViewerAgent(LSLogger logger, LSConfig config) {
		this.config = config;
		this.lsLogger = logger;
	}
		
	public void start() {		
		viewer = new SnapshotViewer();
		viewer.setVisible(true);		        
		HttpClient client = vertx.createHttpClient();
		 
		client.websocket(config.getPort(), /*"172.25.142.41"*/ "localhost", "/gt2/locationservice/api/snapshot", websocket -> {
			  System.out.println("Connected!");
			  websocket.textMessageHandler(msg -> {
				 // System.out.println("==> \n" + msg);
				 JsonObject obj = new JsonObject(msg); 
				 viewer.update(obj);
			  });
		});        
	}
	
	private static final String TT_CONFIG_FILE = "./config.json";

	public static void main(final String[] args) {
		LSConfig config;
		try {
			String configFile = TT_CONFIG_FILE;
			if (args.length == 1) {
				configFile = args[0];
			}
			config = new LSConfig(configFile);
		} catch (Exception ex){
			System.err.println("Config file not found or invalid");
			config = new LSConfig();
		}
		
		LSLogger logger = new LSLogger(config.isLogGUIEnabled());
		logger.log("Booting Beacon Viewer - port: " + config.getPort());

        final Vertx v = Vertx.vertx();
	    v.deployVerticle(new BeaconViewerAgent(logger, config));  
    }	
}
