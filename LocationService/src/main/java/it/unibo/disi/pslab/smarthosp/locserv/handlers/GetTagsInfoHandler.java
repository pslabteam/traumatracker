package it.unibo.disi.pslab.smarthosp.locserv.handlers;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;
import it.unibo.disi.pslab.smarthosp.locserv.*;

public class GetTagsInfoHandler extends AbstractServiceHandler {

	public GetTagsInfoHandler(LocationService service) {
		super(service);
	}
	
	public void handle(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		JsonArray reply = this.getService().getBeaconMap().getAllTagsInfo();
		response.putHeader("content-type", "application/json").end(reply.encodePrettily());
	}

}
