package it.unibo.disi.pslab.smarthosp.locserv.cmd;

import app.Gateway;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.GatewayJsonProperty;
import utils.TagJsonProperty;

public class SetDefaultHospitalSetting {

    public static final String REST_JSON_ID = "id";
    

	public static void main(String[] args) throws Exception {

		Vertx vertx = Vertx.vertx();

        /* final String id = g.getString(DB.GENERIC_OBJECT_ID_KEY);
        final String ip = g.getString(GatewayJsonProperty.ADDRESS_PROPERTY);
        final int threshold = g.getInteger(GatewayJsonProperty.THRESHOLD_PROPERTY);
        final String region = g.getString(GatewayJsonProperty.PLACE_PROPERTY);
        final boolean online = g.getBoolean(GatewayJsonProperty.ONLINE_PROPERTY);
        this.currGwMap.put(ip, new Gateway(id, ip, threshold, region, online)); */
		
		JsonObject[] gws = {
			new JsonObject()
				.put("_id", "gw01")
				.put("id", "gw01")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.5")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-100)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "Shock-Room")
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "gw02")
				.put("id", "gw02")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.3")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-90)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "TAC PS") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "gw03")
				.put("id", "gw03")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.2")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-90)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "Blocco Operatorio") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "gw04")
				.put("id", "gw04")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.1")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-100)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "Neuroradiologia") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "gw05")
				.put("id", "gw05")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.8")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-90)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "Neurochirurgia") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "gw06")
				.put("id", "gw06")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.4")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-90)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "Ortopedia") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "gw07")
				.put("id", "gw07")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.6")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-80)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "Terapia Intensiva T1") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "gw08")
				.put("id", "gw08")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "172.31.38.7")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-90)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "Terapia Intensiva T2") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),

		};

		JsonObject mongoConfig = new JsonObject().put("db_name", "ttlocationdb"); //put("host", ipDB).put("port", portDB).;			
		MongoClient store = MongoClient.createNonShared(vertx, mongoConfig);
	
		for (JsonObject gw: gws) {
			store.save("gateways", gw, res -> { System.out.println(gw.toString() + "\n ==> " + res.succeeded()); });
		}	

		JsonObject[] tags = {
			new JsonObject()
				.put("_id", "3047")
				.put(TagJsonProperty.ID, "3047")
				.put(TagJsonProperty.SERIAL, 3047)
				.put(TagJsonProperty.BATTERY, 100),
			new JsonObject()
				.put("_id", "3048")
				.put(TagJsonProperty.ID, "3048")
				.put(TagJsonProperty.SERIAL, 3048)
				.put(TagJsonProperty.BATTERY, 100),
			new JsonObject()
				.put("_id", "3049")
				.put(TagJsonProperty.ID, "3049")
				.put(TagJsonProperty.SERIAL, 3049)
				.put(TagJsonProperty.BATTERY, 100),
			new JsonObject()
				.put("_id", "3044")
				.put(TagJsonProperty.ID, "3044")
				.put(TagJsonProperty.SERIAL, 3044)
				.put(TagJsonProperty.BATTERY, 100),
			new JsonObject()
				.put("_id", "3045")
				.put(TagJsonProperty.ID, "3045")
				.put(TagJsonProperty.SERIAL, 3045)
				.put(TagJsonProperty.BATTERY, 100),
			new JsonObject()
				.put("_id", "3046")
				.put(TagJsonProperty.ID, "3046")
				.put(TagJsonProperty.SERIAL, 3046)
				.put(TagJsonProperty.BATTERY, 100),
		};
				
		for (JsonObject tag: tags) {
			store.save("tags", tag, res -> { System.out.println(tag.toString() + "\n ==> " + res.succeeded()); });
		}	
		
		System.out.println("OK.");
		Thread.sleep(5000);
		
	}


}

