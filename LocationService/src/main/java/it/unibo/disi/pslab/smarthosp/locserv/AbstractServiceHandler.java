package it.unibo.disi.pslab.smarthosp.locserv;

import io.vertx.ext.mongo.*;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.file.FileSystem;
import io.vertx.core.AsyncResult;
import java.util.Date;
import java.util.List;

public abstract class AbstractServiceHandler implements Handler<RoutingContext> {

	LocationService service;
	
	public AbstractServiceHandler(LocationService service){
		this.service = service;
	}

	abstract public void handle(RoutingContext routingContext);
		
	protected Vertx getCore() {
		return service.getVertx();
	}
	protected void sendError(int statusCode, HttpServerResponse response){
		response.setStatusCode(statusCode).end();
	}
	
	protected void log(String msg){
		service.log(msg);
	}

	protected void logError(String msg){
		service.logError(msg);
	}
	
	protected MongoClient getStore(){
		return service.getStore();
	}
	
	protected LSConfig getConfig(){
		return service.getConfig();
	}
	
	protected Date getStartTime() {
		return service.getStartTime();
	}
	
	protected void findInTags(JsonObject query, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().find(getConfig().getTagsCollectionName(), query, handler);		
	}

	protected void storeInTags(JsonObject tag, Handler<AsyncResult<String>> handler){
		service.getStore().insert(getConfig().getTagsCollectionName(), tag, handler);		
	}

	protected void updateInTags(JsonObject user, Handler<AsyncResult<String>> handler){
		service.getStore().save(getConfig().getTagsCollectionName(), user, handler);		
	}

	protected void findInGWs(JsonObject query, Handler<AsyncResult<List<JsonObject>>> handler){
		service.getStore().find(getConfig().getGWCollectionName(), query, handler);		
	}

	protected void storeInGWs(JsonObject gw, Handler<AsyncResult<String>> handler){
		service.getStore().insert(getConfig().getGWCollectionName(), gw, handler);		
	}

	protected void updateInGWs(JsonObject gw, Handler<AsyncResult<String>> handler){
		service.getStore().save(getConfig().getGWCollectionName(), gw, handler);		
	}

	
	
	protected FileSystem getFS() {
		return service.getFS();
	}

	protected LocationService getService() {
		return service;
	}

}
