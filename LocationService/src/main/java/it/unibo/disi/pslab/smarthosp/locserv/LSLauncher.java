package it.unibo.disi.pslab.smarthosp.locserv;

import io.vertx.core.Vertx;

public final class LSLauncher {

	private static final String TT_CONFIG_FILE = "./config.json";

	public static void main(final String[] args) {
		LSConfig config;
		try {
			String configFile = TT_CONFIG_FILE;
			if (args.length == 1) {
				configFile = args[0];
			}
			config = new LSConfig(configFile);
		} catch (Exception ex){
			System.err.println("Config file not found or invalid");
			config = new LSConfig();
		}
		
		LSLogger logger = new LSLogger(config.isLogGUIEnabled());
		logger.log("Booting LocationService System - port: " + config.getPort());

        final Vertx v = Vertx.vertx();
        DB.get().start(config, v);
        
		BeaconMap bmap = new BeaconMap();
		
	    v.deployVerticle(new BeaconManAgent(bmap, logger, config));  
        v.deployVerticle(new LocationService(bmap, logger, config));  
    }
}