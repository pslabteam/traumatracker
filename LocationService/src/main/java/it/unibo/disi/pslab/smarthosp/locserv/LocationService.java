package it.unibo.disi.pslab.smarthosp.locserv;

import java.util.Date;
import java.util.HashMap;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.mongo.*;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import it.unibo.disi.pslab.smarthosp.locserv.handlers.*;

public class LocationService extends AbstractVerticle {

	private LSConfig config;
	private LSLogger ttLogger;
	private MongoClient store;
	private Router router;
	private Logger logger = LoggerFactory.getLogger(LocationService.class);
	private Date startTime;
	
	private static final String API_BASE_PATH = "/gt2/locationservice/api";
	private static final String TAGS_PATH = "/gt2/locationservice/api/tags/";
	private static final String SNAPSHOT_PATH = "/gt2/locationservice/api/snapshot";

	private static final int POLL_BEACON_MAP_PERIOD = 1000;
	private static final int POLL_TAG_PERIOD = 1000;
	
    private BeaconMap bmap;
	
	public LocationService(BeaconMap bmap, LSLogger view, LSConfig config){
		this.config = config;
		this.ttLogger = view;
		this.bmap = bmap;
	}
	
	@Override
	public void start() {
		initAPI();	
		// initDB();
		initWS();
	}
	
	private void initAPI() {
		router = Router.router(vertx);
		
		router.route().handler(CorsHandler.create("*")
				.allowedMethod(io.vertx.core.http.HttpMethod.GET)
				.allowedMethod(io.vertx.core.http.HttpMethod.POST)
				.allowedMethod(io.vertx.core.http.HttpMethod.PUT)
				.allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
				.allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
				.allowedHeader("Access-Control-Request-Method")
				.allowedHeader("Access-Control-Allow-Credentials")
				.allowedHeader("Access-Control-Allow-Origin")
				.allowedHeader("Access-Control-Allow-Headers")
				.allowedHeader("Content-Type"));

		router.route().handler(BodyHandler.create());

		// management 
		router.get(API_BASE_PATH + "/version").handler(new GetVersionHandler(this));
		// router.get(API_BASE_PATH + "/state").handler(new GetVersionHandler(this));
		
		// tags
		router.get(API_BASE_PATH + "/tags").handler(new GetTagsInfoHandler(this));
		router.get(API_BASE_PATH + "/tags/:tagID").handler(new GetTagInfoHandler(this));
		// router.put(API_BASE_PATH + "/tags/:tagID").handler(new UpdateReportHandler(this));
		// router.delete(API_BASE_PATH + "/tags/:tagID").handler(new UpdateReportHandler(this));
		
		// gws
		// router.get(API_BASE_PATH + "/gateways").handler(new GetGWsInfoHandler(this));
		// router.get(API_BASE_PATH + "/gateways/:gwID").handler(new GetGWInfoHandler(this));
		// router.put(API_BASE_PATH + "/gateways/:gwID").handler(new UpdateReportHandler(this));
		// router.delete(API_BASE_PATH + "/gateways/:gwID").handler(new UpdateReportHandler(this));
		
				
		router.route("/gt2/locationservice/static/*")
			.handler(StaticHandler.create()); 		 
	}

	private void initDB() {		
		JsonObject mongoConfig = new JsonObject().put("db_name", config.getDBName()); 
		store = MongoClient.createNonShared(vertx, mongoConfig);
		log("DB connection created.");
	}

	private void initWS() {
		vertx.createHttpServer()
			.requestHandler(router::accept)
			.websocketHandler(websocket -> {
				String path = websocket.path();
				log("New WS req: " + websocket.remoteAddress() + " for " + path);
				if (path.startsWith(TAGS_PATH)) {
					String tagId = path.substring(TAGS_PATH.length());
					log("WS req " + websocket.remoteAddress() + " for " + tagId);
					JsonObject first = bmap.getTagInfo(tagId);
					String tag = first.encodePrettily(); 
					// log("new tag info: \n" + tag);
					try {
						websocket.writeTextMessage(tag);
						vertx.setPeriodic(POLL_TAG_PERIOD, id -> {
							JsonObject next = bmap.getTagInfo(tagId);
							String stag = next.encodePrettily(); 
							// log("new tag info: \n" + stag);
							try {
								websocket.writeTextMessage(stag);
							} catch (Exception ex){
								try {
									websocket.close();
								} catch (Exception ex2) {}
								vertx.cancelTimer(id);
								log("WS req closed (" + websocket.remoteAddress() + " for " + path + ").");
							}
						});
					} catch (Exception ex){
						try {
							websocket.close();
						} catch (Exception ex2) {}
						log("WS req closed (" + websocket.remoteAddress() + " for " + path + ").");
					}
					
				} else if (path.startsWith(SNAPSHOT_PATH)) {
					log("WS req " + websocket.remoteAddress() + " for snapshot");
					JsonObject first = bmap.getSnapshot();
					String tag = first.encodePrettily(); 
					// log("new snapshot: \n" + tag);
					try {
						websocket.writeTextMessage(tag);
						vertx.setPeriodic(POLL_BEACON_MAP_PERIOD, id -> {
							JsonObject next = bmap.getSnapshot();
							String stag = next.encodePrettily(); 
							// log("new snapshot: \n" + stag);
							try {
								websocket.writeTextMessage(stag);
							} catch (Exception ex){
								try {
									websocket.close();
								} catch (Exception ex2) {}
								vertx.cancelTimer(id);
								log("WS req closed (" + websocket.remoteAddress() + " for " + path + ").");
							}
						});
					} catch (Exception ex){
						try {
							websocket.close();
						} catch (Exception ex2) {}
						log("WS req closed (" + websocket.remoteAddress() + " for " + path + ").");
					}
				} else {
				    // Do something
					websocket.reject();
				}
			})
			.listen(config.getPort(), result -> {
					if (result.succeeded()) {
						startTime = new Date();
						log("Ready.");
					} else {
						log("Failed: "+result.cause());
					}
				});		
	}
	
	public void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}
	
	public BeaconMap getBeaconMap() {
		return bmap;
	}
	
	public MongoClient getStore() {
		return store;		
	}
	
	public void log(String msg) {
		logger.info("[LocationService] "+msg);
	}

	public void logError(String msg) {
		logger.error("[LocationService] "+msg);
		ttLogger.log("[LocationService] "+msg);
	}
	
	public LSConfig getConfig() {
		return config;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	public FileSystem getFS() {
		return vertx.fileSystem();
	}

}
