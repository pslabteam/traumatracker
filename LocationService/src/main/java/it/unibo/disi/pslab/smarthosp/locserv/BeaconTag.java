package it.unibo.disi.pslab.smarthosp.locserv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.unibo.disi.pslab.smarthosp.locserv.BeaconGateway.GWPerceivedTag;
import it.unibo.disi.pslab.smarthosp.locserv.BeaconTag.TagRegionInfo;

public class BeaconTag {
    
    private String serial;
    private int battery;
    private HashMap<String,TagGWInfo> gws;
    private TagGWInfo strongestGW;
    private TagGWInfo stableGW;
    private TagGWHistory history;
    // private String primaryRegionName;
    
    public static long HISTORY_TIME_WINDOW = 30*60*1000; /* in ms */
    public static long STABLEGW_STABILITY_TIME_PARAM = 2*1000; /* in ms */
    public static long REGION_STABILITY_TIME_PARAM = 3*1000; /* in ms */
    		
    private HashMap<String,TagRegionInfo> currentRegions;
    private List<BeaconTagListener> listeners;
    
    public BeaconTag(String serial) {
    		this(serial,-1, HISTORY_TIME_WINDOW);
    }

    public BeaconTag(String serial, int battery, long historyTimeWindow) {
        this.serial = serial;
        this.battery = battery;
        gws = new HashMap<String,TagGWInfo>();
        history = new TagGWHistory(historyTimeWindow);
        currentRegions = new HashMap<String,TagRegionInfo>();
        listeners = new LinkedList<BeaconTagListener>();
    }

    public void addListener(BeaconTagListener l) {
    		listeners.add(l);
    }
    
    private void notifyTagPrimaryRegionUpdated() {
    		String tid = this.getSerial();
    		listeners.forEach(l -> l.notifyPrimaryRegionUpdated(tid, this.getBattery(), this.getCurrentRegions()));
    }
    
    public String getSerial() {
        return this.serial;
    }
    
    public int getBattery() {
        return this.battery;
    }
     
    public TagGWInfo getStrongestGWInfo() {
    		return strongestGW;
    }
    
    public TagGWInfo getStableGWInfo() {
		return stableGW;
    }

    public Collection<TagGWInfo> getGWInfo(){
    		return gws.values();
    }

    /*
    public Collection<TagRegionInfo> getCurrentRegions(){
		return currentRegions.values();
    }
    */
    
    public Collection<TagRegionInfo> getCurrentRegions(){
        ArrayList<TagRegionInfo> list = new ArrayList<TagRegionInfo>();
        currentRegions.values().forEach(reg -> list.add(reg));
        list.sort((a, b) -> { 
        		return a.getRSSI() < b.getRSSI() ? 1 : -1;	
        	});
		return list;
    }
    
    public void updateGWInfo(BeaconGateway gw, int battery, int rssi, long timestamp) {
    		this.battery = battery;
    		TagGWInfo gwInfo = gws.get(gw.getId());
    		if (gwInfo == null) {
    			gwInfo = new TagGWInfo(this, gw, rssi, timestamp);
    			gws.put(gw.getId(), gwInfo);    			
    		} else {
    			gwInfo.update(rssi, timestamp);
    		}    		
		
    		if (strongestGW == null || (strongestGW != gwInfo && (strongestGW.getRSSI() < rssi))) {
			strongestGW = gwInfo;
    			history.update(strongestGW);    			
		} 
    		
    		String region = gw.getRegion();
    		int thr = gw.getThreshold();
    		
    		
    		if (rssi > thr) {
    			TagRegionInfo reg = currentRegions.get(region);
    			if (reg == null) {
    				reg = new TagRegionInfo(gwInfo, rssi, timestamp);
    				currentRegions.put(region, reg);
    			} else {
    				reg.refresh(rssi, timestamp);
    			}
			// notifyTagRefRegionUpdated();
    		} else {
    			TagRegionInfo reg = currentRegions.get(region);
    			if (reg != null) {
	    			long age = timestamp - reg.getTimestamp();
	    			if (age > REGION_STABILITY_TIME_PARAM) {
	    				currentRegions.remove(region);
	    				// notifyTagRefRegionUpdated();
	    			}
    			}
    		}
 
    		/*
    		TagRegionInfo rmax = null;
    		int maxRSSI = Integer.MIN_VALUE;
    		Collection<TagRegionInfo> currs = currentRegions.values();
    		for (TagRegionInfo ri: currs) {
    			if (ri.getRSSI() > maxRSSI) {
    				rmax = ri;
    				maxRSSI = ri.getRSSI();
    			}
    		}
    		    		
    		if (this.primaryRegionName == null) {
    			if (rmax != null) {
    				primaryRegionName = rmax.getName();
    				notifyTagPrimaryRegionUpdated();
    			}
    		} else if (!this.primaryRegionName.equals(rmax.getName())) {
			primaryRegionName = rmax.getName();
			notifyTagPrimaryRegionUpdated();
    		}
    		*/
    		
    }

    public void checkFreshness(long maxAge) {  		
        List<TagGWInfo> gwsToRemove = new ArrayList<TagGWInfo>();
        long now =  System.currentTimeMillis();
        gws.values().forEach(gw -> {
        		long age = now - gw.getTimestamp(); 
        		if (age > maxAge) {
        			gwsToRemove.add(gw);
        		}
        	});
        	gwsToRemove.forEach(gw -> gws.remove(gw.getGW().getId()));

        if 	(strongestGW != null && (now - strongestGW.getTimestamp() > maxAge)) {
        		strongestGW = null;
        }
        
        if 	(stableGW != null && (now - stableGW.getTimestamp() > maxAge)) {
        		stableGW = null;
        }
        
        if (!history.isEmpty()) {
	        TagGWHistory.TagHistoricalInfo tagh = history.getFirst();
	    		if (tagh.isValidTag() && (now - tagh.getGWInfo().getTimestamp() > maxAge)) {
	    			history.updateWithNotTag();
	    		}
        }
        
    		List<String> regsToRemove = new ArrayList<String>();
        currentRegions.values().forEach(reg -> {
        		long age = now - reg.getTimestamp(); 
            	if (age > maxAge) {
            		regsToRemove.add(reg.getGWInfo().getGW().getRegion());
            	}
        });
        regsToRemove.forEach(gw -> currentRegions.remove(gw));
        if (regsToRemove.size() > 0) {
			notifyTagPrimaryRegionUpdated();
        }
    }
    
    public void checkHistory() {
    		history.checkHistory();
    		TagGWHistory.TagHistoricalInfo first = history.getFirst();
    		if (first != null && first.isValidTag()) {
			long dt = System.currentTimeMillis() - first.getTimestamp();
			if (dt > STABLEGW_STABILITY_TIME_PARAM) {
				stableGW = first.getGWInfo();
			}
    		}
    }

    public TagGWHistory getTagHistory() {
    		return history;
    }
    
    class TagGWHistory {
    		
    		private LinkedList<TagHistoricalInfo> history;
    		private long timeWindow;
    		
    		public TagGWHistory(long timeWindow) {
    			this.timeWindow = timeWindow;
    			history = new LinkedList<TagHistoricalInfo>();
    		}
    		
    		public void update(TagGWInfo gw) {
    			history.addFirst(new TagHistoricalInfo(gw, gw.getTimestamp()));
    		}

    		public void updateWithNotTag() {
    			history.addFirst(new TagHistoricalInfo(null, System.currentTimeMillis()));
    		}

    		public boolean isEmpty() {
    			return history.isEmpty();
    		}
    		
    		public TagHistoricalInfo getFirst() {
    			if (!history.isEmpty()) {
    				return history.getFirst();
    			} else {
    				return null;
    			}
    		}
    		
    		public Collection<TagHistoricalInfo> getHistory(){
    			return history;
    		}
    		
    		public void checkHistory() {
    			long now = System.currentTimeMillis();
    			Iterator<TagHistoricalInfo> it = history.iterator();
    			while (it.hasNext()) {
    				TagHistoricalInfo tag = it.next();
    				if (tag.getTimestamp() - now > timeWindow) {
    					it.remove();
    				}
    			}
    		}
    		
    		class TagHistoricalInfo {
    		    	
    	    		private long timestamp;
    	    		private TagGWInfo gw;
    	    		
    	    		public TagHistoricalInfo(TagGWInfo gw, long timestamp) {
    	    			this.gw = gw;
    	    			this.timestamp = timestamp;
    	    		}
    	    		
    	    		public TagGWInfo getGWInfo() {
    	    			return gw;
    	    		}
    	    		
    	    		public long getTimestamp() {
    	    			return timestamp;
    	    		}
    	    		
    	    		public boolean isValidTag() {
    	    			return gw != null;
    	    		}
    	    }
    		
    }
    
    class TagGWInfo {
    	
    		private int intensity;
    		private long timestamp;
    		private BeaconGateway gw;
    		private BeaconTag tag; 
    		
    		public TagGWInfo(BeaconTag tag, BeaconGateway gw, int intensity, long timestamp) {
    			this.gw = gw;
    			this.tag = tag;
    			this.intensity = intensity;
    			this.timestamp = timestamp;
    		}
    		
    		public int getRSSI() {
    			return intensity;
    		}
    		
    		public BeaconGateway getGW() {
    			return gw;
    		}
    		
    		public long getTimestamp() {
    			return timestamp;
    		}
    		
    		public BeaconTag getTag() {
    			return tag;
    		}
    		
    		public void update(int intensity, long timestamp) {
    			this.intensity = intensity;
    			this.timestamp = timestamp;
    		}
    }

    class TagRegionInfo {
    	
		private TagGWInfo gw; 
		private long timestamp;
		private int intensity;
		
		public TagRegionInfo(TagGWInfo gw, int intensity, long timestamp) {
			this.gw = gw;
			this.intensity = intensity;
			this.timestamp = timestamp;
		}
		
		public String getName() {
			return gw.getGW().getRegion();
		}
		
		public int getRSSI() {
			return intensity;
		}
		
		public TagGWInfo getGWInfo() {
			return gw;
		}
		
		public void refresh(int intensity, long timestamp) {
			this.timestamp = timestamp;
			this.intensity = intensity;
		}
		
		public long getTimestamp() {
			return timestamp;
		}
    }
}
