package it.unibo.disi.pslab.smarthosp.locserv.cmd;

import app.Gateway;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.GatewayJsonProperty;
import utils.TagJsonProperty;

public class SetDefaultData {

    public static final String REST_JSON_ID = "id";
    

	public static void main(String[] args) throws Exception {

		Vertx vertx = Vertx.vertx();

        /* final String id = g.getString(DB.GENERIC_OBJECT_ID_KEY);
        final String ip = g.getString(GatewayJsonProperty.ADDRESS_PROPERTY);
        final int threshold = g.getInteger(GatewayJsonProperty.THRESHOLD_PROPERTY);
        final String region = g.getString(GatewayJsonProperty.PLACE_PROPERTY);
        final boolean online = g.getBoolean(GatewayJsonProperty.ONLINE_PROPERTY);
        this.currGwMap.put(ip, new Gateway(id, ip, threshold, region, online)); */
		
		JsonObject[] gws = {
			new JsonObject()
				.put("_id", "23")
				.put("id", "23")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "192.168.1.23")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-80)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "kitchen")
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
			new JsonObject()
				.put("_id", "22")
				.put("id", "22")
				.put(GatewayJsonProperty.ADDRESS_PROPERTY, "192.168.1.22")
				.put(GatewayJsonProperty.THRESHOLD_PROPERTY,-90)
				.put(GatewayJsonProperty.PLACE_PROPERTY, "bedroom") 
				.put(GatewayJsonProperty.ONLINE_PROPERTY, true),
		};

		JsonObject mongoConfig = new JsonObject().put("db_name", "ttlocationdb"); //put("host", ipDB).put("port", portDB).;			
		MongoClient store = MongoClient.createNonShared(vertx, mongoConfig);
	
		for (JsonObject gw: gws) {
			store.save("gateways", gw, res -> { System.out.println(gw.toString() + "\n ==> " + res.succeeded()); });
		}	

		JsonObject[] tags = {
			new JsonObject()
				.put("_id", "3047")
				.put(TagJsonProperty.ID, "3047")
				.put(TagJsonProperty.SERIAL, 3047)
				.put(TagJsonProperty.BATTERY, 100),
			new JsonObject()
				.put("_id", "3048")
				.put(TagJsonProperty.ID, "3048")
				.put(TagJsonProperty.SERIAL, 3048)
				.put(TagJsonProperty.BATTERY, 100),
			new JsonObject()
				.put("_id", "3049")
				.put(TagJsonProperty.ID, "3049")
				.put(TagJsonProperty.SERIAL, 3049)
				.put(TagJsonProperty.BATTERY, 100),
		};
				
		for (JsonObject tag: tags) {
			store.save("tags", tag, res -> { System.out.println(tag.toString() + "\n ==> " + res.succeeded()); });
		}	
		
		System.out.println("OK.");
		Thread.sleep(5000);
		
	}


}

