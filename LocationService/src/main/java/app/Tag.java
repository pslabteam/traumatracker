package app;

/**
 * Basic element of the system; it permits to perform localization.
 */
public class Tag {
    
    private final String id;
    private final int serial;
    private boolean updated;
    private String location;
    private int battery;

    /**
     * Constructor of this class. 
     * @param tagId
     *     The id to assign to the tag 
     * @param serialNum
     *     Serial number of the tag
     * @batteryPerc
     *     The remaining battery percentage of the tag
     */
    public Tag(final String tagId, final int serialNum, final int batteryPerc) {
        this.id = tagId;
        this.serial = serialNum;
        this.updated = false;
        this.location = Region.NO_PLACE;
        this.battery = batteryPerc;
    }

    /**
     * Getter of the tag id.
     * @return
     *     The id assigned to the tag
     */
    public String getId() {
        return this.id;
    }

    /**
     * Getter of the tag "updated" status.
     * @return
     *     true if the tag has been updated during the last scan.
     */
    public boolean isUpdated() {
        return this.updated;
    }

    /**
     * Getter of the tag location.
     * @return
     *     The current tag location
     */
    public String getLocation() {
        return this.location;
    }

    /**
     * Getter of the serial number of the tag.
     * @return
     *     The serial number of the tag
     */
    public int getSerial() {
        return this.serial;
    }
    
    /**
     * Setter of the tag "update" status. 
     * @param b
     *     The new status of the tag
     */
    public void setUpdated(final boolean b) {
        this.updated = b;
    }

    /**
     * Setter of the tag location. 
     * @param newLocation
     *     The new location of the tag
     */
    public void setLocation(final String newLocation) {
        this.location = newLocation;
    }

    /**
     * Setter of the battery percentage.
     * @param newBattery
     *     The new battery value
     */
    public void setBattery(final int newBattery) {
        this.battery = newBattery;
    }
    
    /**
     * Getter of the battery percentage power.
     * @return
     *     The remaining power expressed as percentage
     */
    public int getBattery() {
        return this.battery;
    }
    
    @Override
    public String toString() {
        return "\nId: " + this.id + 
                "; Serial: " + this.serial + 
                "; Updated: " + this.updated + 
                "; Location: " + this.location +
                "; Battery: " + this.battery;
    }
}
