package app;

import io.vertx.core.Vertx;

/**
 * Main class of the application.
 */
public final class App {

    private App() { }

    /**
     * Main of the application.
     * @param args
     *     ignored
     */
    public static void main(final String[] args) {

        System.out.println("DEBUG: App started");
        final Vertx v = Vertx.vertx();
        DB.get().start(v);
        
        System.out.println("DEBUG: Start agents");
        v.deployVerticle(new TagManagerAgent());
        v.deployVerticle(new ServerAgent());  
    }
}