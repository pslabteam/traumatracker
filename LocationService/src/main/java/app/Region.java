package app;

/**
 * A generic region in the system.
 */
public class Region {

    /**
     * Special region "none": none of the room of the system, could be an aisle, a room without a gateway and so on...
     */
    public static final String NO_PLACE = "no-place";
    
    /**
     * Special region: the gateway has no region set.
     */
    public static final String NO_REGION = "no-region";
}
