package app;

/**
 * Class that represents a gateway of the system.
 */
public class Gateway {
    
    private final String id;
    private final String ip;
    private final int threshold;
    private final String region;
    private boolean rcvReply;
    private boolean online;

    /**
     * Constructor of this class.
     * @param gatewayIp
     *     The IP to assign to the gateway.
     * @param gwId
     *     The id of the gateway 
     * @param thres
     *     Threshold of the gateway's region
     * @param regionName
     *     The region id set in the gateway
     * @param status
     *     The "online" status of the gw
     */
    public Gateway(final String gwId, final String gatewayIp, final int thres, final String regionName, final boolean status) {
        this.ip = gatewayIp;
        this.id = gwId;
        this.threshold = thres;
        this.region = regionName;
        this.rcvReply = false;
        this.online = status;
    }

    /**
     * Getter of the gateway IP.
     * @return
     *     The gateway IP
     */
    public String getIp() {
        return this.ip;
    }
    
    /**
     * Getter of the gateway's Id.
     * @return
     *     The gateway id
     */
    public String getId() {
        return this.id;
    }
    
    /**
     * Getter of the threshold parameter value.
     * @return
     *     The threshold set value 
     */
    public int getThreshold() {
        return this.threshold;
    }
    
    /**
     * Getter of the gateway's region id.
     * @return
     *     The id of the region set in the gateway 
     */
    public String getRegion() {
        return this.region;
    }
    
    /**
     * Getter of the rcvReply status.
     * @return
     *     true is the reply has arrived from this gateway, else otherwise
     */
    public boolean isRcvReply() {
        return this.rcvReply;
    }
    
    /**
     * Setter of the rcvReply status.
     * @param b
     *     The new status of the rcvReply
     */
    public void setRcvReply(final boolean b) {
        this.rcvReply = b;
    }
    
    /**
     * Getter of the gw "online" status.
     * @return
     *     true if the gw is online, false otherwise
     */
    public boolean isOnline() {
        return this.online;
    }
    
    /**
     * Set of the gw online status between "online"/"offline".
     * @param s
     *     The new status of the gw
     */
    public void setOnlineStatus(final boolean s) {
        this.online = s;
    }
    
    @Override
    public String toString() {
        return "\nId: " + this.id + 
                "; IP: " + this.ip + 
                "; Threshold: " + this.threshold + 
                "; Region: " + this.region +
                "; Online: " + this.online +
                "; Rcv reply: " + this.rcvReply;
    }
}
