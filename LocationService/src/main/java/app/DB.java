package app;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;

/**
 * This class represents the DB used by the application; singleton pattern used.
 */
public final class DB {

    /**
     * Object key used to identify objects into the DB.
     */
    public static final String GENERIC_OBJECT_ID_KEY = "_id"; 
    
    /**
     * Operator used for updating operations.
     */
    public static final String SET_FIELD_UPDATE_OPERATOR = "$set";
    
    private static final String DB_NAME_KEY = "db_name";
    private static final String DB_NAME = "ttlocationdb";
    
    private static final DB SINGLETON = new DB();
    
    private final UpdateOptions multiOpt; 
    
    private MongoClient dbConn;
    
    private DB() {
        this.multiOpt = new UpdateOptions().setMulti(true);
    }
    
    /**
     * Getter of the DB.
     * @return
     *     The DB unique instance.
     */
    public static DB get() {
        return SINGLETON;
    }
    
    /** It creates the client used to connect to the DB.
     * @param v
     *     The Vertx instance used in order to create the DB client
     */
    public void start(final Vertx v) {
        this.dbConn = MongoClient.createNonShared(v, new JsonObject().put(DB_NAME_KEY, DB_NAME));
        
    }
    
    /**
     * Getter of the connection to the DB.
     * @return
     *     The MongoClient instance that permits to connect to the DB
     */
    public MongoClient getConnection() {
        return this.dbConn;
    }
    
    /**
     * Getter of the update multiple documents options.
     * @return
     *     The object used to set the multiple update
     */
    public UpdateOptions getMultiOpt() {
        return this.multiOpt;
    }
}
