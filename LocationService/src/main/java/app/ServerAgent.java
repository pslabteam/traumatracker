package app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import resthandler.DeleteAny;
import resthandler.DeleteGateway;
import resthandler.DeleteRegion;
import resthandler.DeleteTag;
import resthandler.Get;
import resthandler.GetAny;
import resthandler.GetRegion;
import resthandler.PostGateway;
import resthandler.PostRegion;
import resthandler.PostTag;
import resthandler.PutGateway;
import resthandler.PutRegion;
import resthandler.PutTag;
import utils.BusMsgType;
import utils.DBCollections;
import utils.RegionJsonProperty;
import utils.WebsocketReply;

/**
 * The agent that is responsible for satisfying the users' requests about beacons' info.
 */
public class ServerAgent extends AbstractVerticle {

    /**
     * Identifier used in the external applications which interact with the location service through REST calls.
     */
    public static final String REST_JSON_ID = "id";
    
    private static final int PORT_TO_LISTEN = 8083;
    private static final String BASE_URL = "/gt2/locationservice/api"; //REST base URL
    private static final String WS_URL_REGEX_CHECK = "/gt2/traumatracker/api/tags/[0-9]{2,4}/place"; //Websocket URL
    
    private static final String TAGS_ID_URL_SUFFIX = "/tags/:id";
    private static final String GW_ID_URL_SUFFIX = "/gateways/:id";
    private static final String REGIONS_ID_URL_SUFFIX = "/regions/:id";
    
    private EventBus bus;
    private final Map<Integer, List<ServerWebSocket>> listenedTagMap = new HashMap<>();
    
    @Override
    public void start() {
        final Vertx v = getVertx();
        this.bus = v.eventBus();
        this.setBusConsumer();
        
        v.setPeriodic(3000, id -> { System.out.println("WS MAP: " + this.listenedTagMap); });
        
        final Router router = Router.router(v);
        this.setRoutes(router);
        
        v.createHttpServer()
        .requestHandler(router::accept)
        .websocketHandler(ws -> {
            final Optional<Integer> beaconNum = this.getSerialFromUri(ws.uri());
            if (beaconNum.isPresent()) {
                /*
                 *  "Someone" was already listening to the tag, just add to the list;
                 *  Actually it's the same person but there can be duplicated sockets (newtwork shut down makes ws neither work or close)
                 */
                if (this.listenedTagMap.containsKey(beaconNum.get())) {
                    this.listenedTagMap.get(beaconNum.get()).add(ws);
                } else { //No one was listening to the tag, so a new entry in the map must be created 
                    this.listenedTagMap.put(beaconNum.get(), new ArrayList<>(Arrays.asList(ws)));
                }
                this.bus.publish(BusMsgType.GET_INIT_TAG_INFO, beaconNum.get());
                ws.closeHandler(h -> {
                   //By hypothesis, only one person can listen to the same beacon, so a close request makes delete all duplicate sockets
                   this.listenedTagMap.entrySet().removeIf(entry -> {
                      for (final ServerWebSocket w : entry.getValue()) {
                          if (w.toString().equals(ws.toString())) {
                              return true;
                          } 
                      }
                      return false;
                   });
                });
            } else {
                ws.reject(); //Wrong url request
            }
        })
        .listen(PORT_TO_LISTEN);     
    }
    
    private void setRoutes(final Router r) {

        r.route().handler(CorsHandler.create("*")
                .allowedMethod(io.vertx.core.http.HttpMethod.GET)
                .allowedMethod(io.vertx.core.http.HttpMethod.POST)
                .allowedMethod(io.vertx.core.http.HttpMethod.PUT)
                .allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
                .allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
                .allowedHeader("Access-Control-Request-Method")
                .allowedHeader("Access-Control-Allow-Credentials")
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Headers")
                .allowedHeader("Content-Type"));
        
        r.route().handler(BodyHandler.create());
        
        //GET
        /*
         * Don't change the order of requests!
         * If "simple" GET requests are put previously of the "any" one, the router considers the "any" as the ":id" parameter;
         * This way "any" is evaluated before. 
         */
        r.get(BASE_URL + "/tags/any").handler(new GetAny(DBCollections.TAGS));
        r.get(BASE_URL + "/gateways/any").handler(new GetAny(DBCollections.GATEWAYS));
        r.get(BASE_URL + "/regions/any").handler(new GetAny(DBCollections.REGIONS));
        r.get(BASE_URL + TAGS_ID_URL_SUFFIX).handler(new Get(DBCollections.TAGS));
        r.get(BASE_URL + GW_ID_URL_SUFFIX).handler(new Get(DBCollections.GATEWAYS));
        r.get(BASE_URL + REGIONS_ID_URL_SUFFIX).handler(new GetRegion(DBCollections.REGIONS, this.bus));
        
        //POST
        r.post(BASE_URL + TAGS_ID_URL_SUFFIX).handler(new PostTag(this.bus));
        r.post(BASE_URL + GW_ID_URL_SUFFIX).handler(new PostGateway(this.bus));
        r.post(BASE_URL + REGIONS_ID_URL_SUFFIX).handler(new PostRegion(this.bus));
        
        //PUT
        r.put(BASE_URL + TAGS_ID_URL_SUFFIX).handler(new PutTag(this.bus, this));
        r.put(BASE_URL + GW_ID_URL_SUFFIX).handler(new PutGateway(this.bus));
        r.put(BASE_URL + REGIONS_ID_URL_SUFFIX).handler(new PutRegion(this.bus));
        
        //DELETE
        /*
         * Don't change the order of requests! (same motivation of the GET one...)
         */
        r.delete(BASE_URL + "/tags/any").handler(new DeleteAny(DBCollections.TAGS, this.bus, this));
        r.delete(BASE_URL + "/gateways/any").handler(new DeleteAny(DBCollections.GATEWAYS, this.bus, this));
        r.delete(BASE_URL + "/regions/any").handler(new DeleteAny(DBCollections.REGIONS, this.bus, this));
        r.delete(BASE_URL + TAGS_ID_URL_SUFFIX).handler(new DeleteTag(this.bus, this));
        r.delete(BASE_URL + GW_ID_URL_SUFFIX).handler(new DeleteGateway(this.bus));
        r.delete(BASE_URL + REGIONS_ID_URL_SUFFIX).handler(new DeleteRegion(this.bus));
    }
    
    private Optional<Integer> getSerialFromUri(final String uri) {
        // /gt2/traumatracker/api/tags/:tagId/place
        if (uri.matches(WS_URL_REGEX_CHECK)) {
            final String[] uriParts = uri.split("/");
            /*
            0 : 
            1 : gt2
            2 : traumatracker
            3 : api
            4 : tags
            5 : :tagId
            6 : place
            */
            return Optional.of(Integer.valueOf(uriParts[uriParts.length - 2]));
        } else {
            return Optional.empty();
        }
    }
    
    private void setBusConsumer() {   
        this.bus.consumer(BusMsgType.NOTIFY_SOCKET, msg -> {
            final String[] info = msg.body().toString().split(" ");
            if (this.listenedTagMap.containsKey(Integer.valueOf(info[0]))) { //If the beacon is listened by someone
                this.findReplyInfo(info[0], info[1]);
            }
        });
        
        this.bus.consumer(BusMsgType.NOTIFICATION_TO_SEND, msg -> {
            final JsonObject reply = new JsonObject(msg.body().toString());
            for (final ServerWebSocket ws : this.listenedTagMap.get(Integer.valueOf(reply.getString(WebsocketReply.WEBSOCKET_REPLY_ID_KEY)))) {
                try {
                    ws.writeTextMessage(reply.encode());
                } catch (IllegalStateException e) {
                    //Closed websocket
                    System.out.println("Detected duplicated closed web...");
                }
            }
        });
    }
    
    private void findReplyInfo(final String tagId, final String regionId) {
        if (regionId.equals(Region.NO_PLACE)) {
            this.bus.publish(BusMsgType.NOTIFICATION_TO_SEND, new WebsocketReply(tagId, Region.NO_PLACE).encode());
        } else {
            final JsonObject selectQuery = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, regionId);
            DB.get().getConnection().find(DBCollections.REGIONS, selectQuery, res -> {
                if (res.succeeded() && res.result().size() > 0) {
                    final String regionName = res.result().get(0).getString(RegionJsonProperty.NAME_PROPERTY);
                    this.bus.publish(BusMsgType.NOTIFICATION_TO_SEND,  new WebsocketReply(tagId, regionName).encode());
                } else {
                    this.bus.publish(BusMsgType.NOTIFICATION_TO_SEND,  new WebsocketReply(tagId, Region.NO_PLACE).encode());
                }
            });
        }
    }
    
    /**
     * Getter of the currently occupied tags.
     * @return
     *     A set containing the serial numbers of the occupied tags
     */
    public Set<Integer> getListenedTag() {
       return Collections.unmodifiableSet(this.listenedTagMap.keySet()); 
    }
}
