package app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.smarthosp.locserv.DB;
import utils.BusMsgType;
import utils.DBCollections;
import utils.GatewayJsonProperty;
import utils.TagJsonProperty;

/**
- * The agent that is responsible for getting the info about the beacons and store it.
- */
public class TagManagerAgent extends AbstractVerticle {

    private static final int DEFAULT_REQUEST_PORT = 80;
    private static final String RELATIVE_URL = "/api/beacons/blueup";
    private static final int UPDATE_PERIOD = 5000; //milliseconds
    private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    
    private HttpClient client;
    private EventBus bus;
    
    /*
     * To let the application work, a list for tags and a list for gateways could be enough but, by using maps, we create 
     * a sort of index in order directly to access the interested element (indeed the map keys are redundant data). 
     */
    //Tag vars
    private final Map<Integer, Tag> tagMap = new HashMap<>(); //serial, tag
    private boolean pendingTagUpdate;
    private Future<Void> doingTagDataSync;

    //Gw vars
    private final Map<String, Gateway> currGwMap = new HashMap<>(); //IP, gateway
    private boolean pendingGWUpdate;
    private Future<Void> doingGWDataSync;

    private int getCounter;
    private int maxCounter;
    
    /**
     * Constructor of this class.
     */
    public TagManagerAgent() {
        this.getCounter = 0;
    }
    
    @Override
    public void start() {
        final Vertx v = getVertx();
        this.client = v.createHttpClient();
        this.bus = v.eventBus();
        

        //Set starting data
        
        this.doingGWDataSync = Future.future();
        this.syncGWData();
        
        this.doingTagDataSync = Future.future();
        this.syncTagData();
        
        this.setBusConsumer();

        v.setPeriodic(UPDATE_PERIOD, id -> {
            //The agent must have finished to sync the data if it's doing it now...
            if (this.doingGWDataSync.isComplete() && this.doingTagDataSync.isComplete()) {
                this.allGet();
            }
        });
    }
    
    private void setBusConsumer() {
        this.bus.consumer(BusMsgType.GET_INIT_TAG_INFO, msg -> {
            final int beaconSerial = Integer.valueOf(msg.body().toString());
            this.bus.publish(BusMsgType.NOTIFY_SOCKET, beaconSerial + " " + this.tagMap.get(beaconSerial).getLocation());
        });
        this.bus.consumer(BusMsgType.SYNC_GW_DATA, msg -> this.pendingGWUpdate = true);
        this.bus.consumer(BusMsgType.SYNC_TAG_DATA, msg -> this.pendingTagUpdate = true);
        this.bus.consumer(BusMsgType.ASK_TAG_REGION, msg -> {
            final String regionId = msg.body().toString();
            final JsonArray ja = new JsonArray();
            this.tagMap.values()
                       .stream()
                       .filter(t -> t.getLocation().equals(regionId))
                       .forEach(t -> ja.add(t.getId()));
            this.bus.publish(BusMsgType.SND_TAG_REGION, ja.encode());
        });
    }
    
    private void syncGWData() {
        this.pendingGWUpdate = false;
        this.currGwMap.clear(); 
                
        this.doingGWDataSync = Future.future(); //It prevents from sending request while the data sync has not finished yet
        DB.get().getConnection().find(DBCollections.GATEWAYS, new JsonObject(), res -> {
            if (res.succeeded()) {
                res.result().stream()
                            .forEach(g -> {
                                final String id = g.getString(DB.GENERIC_OBJECT_ID_KEY);
                                final String ip = g.getString(GatewayJsonProperty.ADDRESS_PROPERTY);
                                final int threshold = g.getInteger(GatewayJsonProperty.THRESHOLD_PROPERTY);
                                final String region = g.getString(GatewayJsonProperty.PLACE_PROPERTY);
                                // final boolean online = g.getBoolean(GatewayJsonProperty.ONLINE_PROPERTY);
                                this.currGwMap.put(ip, new Gateway(id, ip, threshold, region, true));
                            });
                this.maxCounter = this.currGwMap.size();
                this.doingGWDataSync.complete(); //Get requests can be sent again
            } else {
                System.out.println("[BeaconMangerAgent] DEBUG: Failed to read gateways' set from the DB!");
            }
        });
    }
    
    private void syncTagData() {
        this.pendingTagUpdate = false;   
        this.doingTagDataSync = Future.future(); //It prevents from sending request while the data sync has not finished yet
        DB.get().getConnection().find(DBCollections.TAGS, new JsonObject(), res -> {
            if (res.succeeded()) {
                final Map<Integer, Tag> tmpMap = new HashMap<>(this.tagMap);
                this.tagMap.clear();
                res.result().stream()
                            .forEach(t -> {
                                //Fill again map
                                final int id = t.getInteger(TagJsonProperty.SERIAL);
                                this.tagMap.put(id, new Tag(t.getString(DB.GENERIC_OBJECT_ID_KEY),
                                                            t.getInteger(TagJsonProperty.SERIAL),
                                                            t.getInteger(TagJsonProperty.BATTERY)));    
                                //Keep previous location info
                                if (tmpMap.containsKey(id)) {
                                    this.tagMap.get(id).setLocation(tmpMap.get(id).getLocation());
                                }
                            });      
                this.doingTagDataSync.complete(); //Get requests can be sent again    
            } else {
                System.out.println("[BeaconManagerAgent] DEBUG: Failed to read tags' set from the DB!");
            }
        });
    }
    
    private void allGet() {
    		System.out.println("ALL GET "+maxCounter);
    		//Reset flag map
        this.tagMap.values().stream()
                               .forEach(tag -> tag.setUpdated(false));
        if (this.maxCounter > 0) { //There must be at least 1 GW
            //Send get request(s)
            this.currGwMap.keySet()
                          .stream()
                          .forEach(ip -> this.httpGet(ip));
        } else { //There are no GWs but must call incCounter() function to sync the beacon map the first time there are no GWs and
                 //check whenever there are new info about GWs in order to exit from stalemate status
            this.getCounter = -1; // 0 after inc, max counter is 0 now (0 gw) -> ok...
            this.incCounter();
        }
    }
    
    private void httpGet(final String ip) {
    		// System.out.println("GETTING INFO ABOUT " + ip);
    		this.client.get(DEFAULT_REQUEST_PORT, ip, RELATIVE_URL, resp -> {
            resp.bodyHandler(buf -> {
                if (this.currGwMap.containsKey(ip)) { //The reply must be from a current existing gateway!
                                                      //Indeed it could happen that a request is sent but the gateway is deleted
                                                      //before the reply arrived -> if it' s the case it must be ignored
                		//Set the gw to online if offline
                		this.updateGwStatus(ip, true, p -> !p); 
                   final JsonObject selectQuery = new JsonObject().put(GatewayJsonProperty.ADDRESS_PROPERTY, ip);
                   DB.get().getConnection().find(DBCollections.GATEWAYS, selectQuery, res-> {
                       if (res.result().size() > 0) {
                            final String regionId = res.result().get(0).getString(GatewayJsonProperty.PLACE_PROPERTY);
                            try {
                                this.processBuffer(buf, ip, regionId);
                            } catch (DecodeException e) {
                                //It generally occurs after the GW just restarted and it' s not ready to accept requests yet -> 
                                //wait for the GW; it may occur even when the src is not a gw -> the reply parsing fails.
                                this.updateGwStatus(ip,  false, p -> p);
                                System.out.println("DEBUG: Catch decode  exception...!");
                            }
                        }            
                        //If I have not received a reply from this GW the counter is increased, not otherwise because this is a 
                        //reply of a second (or third and so on...) request to the same GW -> Indeed a get request failed or its reply 
                        //not arrived yet, so the counter must not be increased to avoid lags on the total view sync made after at least
                        //1 get reply arrived from all the GW (even failed timeout connection is considered a sort of reply...).
                        if (!this.currGwMap.get(ip).isRcvReply()) {
                            this.currGwMap.get(ip).setRcvReply(true);
                            this.incCounter(); //The agent finished to elaborate the reply so the counter must be updated.
                        }
                    });
                }
            });
        }).exceptionHandler(exc -> { //It generally occurs when the GW is off-line, so the request fails and when the reply timeout
                                     //triggers an exceptions is thrown
            if (this.currGwMap.containsKey(ip)) { //The (failed request) reply must be from a current existing gateway!
                System.out.println("DEBUG: Reply timeout from GW: " + ip);
                if (!this.currGwMap.get(ip).isRcvReply()) {
                    this.currGwMap.get(ip).setRcvReply(true);
                    this.incCounter();
                }
                //Set the gw to offline if online
                this.updateGwStatus(ip, false, p -> p);
            }
        })
        .end();
    }
    
    private void processBuffer(final Buffer b, final String ip, final String regionId) throws DecodeException {
    		final Gateway gw = this.currGwMap.get(ip);
    		final JsonArray tagList = b.toJsonObject().getJsonArray("beacons");     
        tagList.forEach(tagobj -> {
        			  JsonObject tag = (JsonObject) tagobj;	
                   final int serial = tag.getInteger(TagJsonProperty.BLUEUP_SERIAL);
                   final int battery = tag.getInteger(TagJsonProperty.BATTERY);
                   final int rssi = tag.getInteger(TagJsonProperty.RSSI);
                   log("GW " + gw.getIp() + " TAG " + serial + " RSSI " + rssi);

                   Tag localTag = (Tag) tagMap.get(serial);
                   
                   //Update battery value
                   if (localTag != null && battery != localTag.getBattery())  {
                	   
                       //final String timestamp = tag.getString(TagJsonProperty.TIMESTAMP); //BlueUp wrong timestamp
                       final String timestamp = new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date());                      
                       localTag.setBattery(battery);
                        
                       final JsonObject selectQuery = new JsonObject().put(TagJsonProperty.SERIAL, serial);
                       final JsonObject updateField = new JsonObject().put(TagJsonProperty.BATTERY, battery)
                                                                      .put(TagJsonProperty.TIMESTAMP, timestamp);
                       final JsonObject updateQuery = new JsonObject().put(DB.SET_FIELD_UPDATE_OPERATOR, updateField);
                       DB.get().getConnection().updateCollection(DBCollections.TAGS, selectQuery, updateQuery, r -> { });
                   }
                   
                   //Update beacon place
                   
                   if (localTag != null && rssi > gw.getThreshold()) {
                       final String prevLocation = localTag.getLocation();
                       localTag.setLocation(regionId);
                       localTag.setUpdated(true);
                       // log("Updated tag: " + localTag.getId()+ " RSSI for "+gw.getRegion() + " " + rssi);
                       if (!prevLocation.equals(regionId)) {
                           this.bus.publish(BusMsgType.NOTIFY_SOCKET, serial + " " + regionId);
                       }
                   }
               });
    }

    private void log(String msg) {
    		System.out.println("[TagManagerAgent] " + msg);
    }
    private void incCounter() {
        this.getCounter++;
        if (this.getCounter == this.maxCounter) {
            this.getCounter = 0;

            //Info from all gateways are updated, now set to "none" the place of all beacons that have not been updated yet
            //(if a beacon has not been updated yet it means that no gateway detected it so it' s moving through rooms...)
            for (final Integer key: this.tagMap.keySet()) {
                //If in the previous detection it was in a region but now it isn't
                if (!this.tagMap.get(key).isUpdated() && !this.tagMap.get(key).getLocation().equals(Region.NO_PLACE)) {
                    this.bus.publish(BusMsgType.NOTIFY_SOCKET, key + " " + Region.NO_PLACE);
                    this.tagMap.get(key).setLocation(Region.NO_PLACE);
                }
            }
            
            //Reset reply map
            this.currGwMap.values()
                          .stream()
                          .forEach(gw -> gw.setRcvReply(false));
            
            //Update GW map if required
            if (this.pendingGWUpdate) {
                this.syncGWData();
            }
            
            //Update tag map if required
            if (this.pendingTagUpdate) {
                this.syncTagData();
            }
            
            //System.out.println("DEBUG: " + this.currGwMap.values());
            // System.out.println("DEBUG: " + this.tagMap.values());
        }
    }
    
    private void updateGwStatus(final String ip, final boolean newStatus, final Predicate<Boolean> p) {
        if (this.currGwMap.containsKey(ip)) { // && p.test(this.currGwMap.get(ip).isOnline())) {
            this.currGwMap.get(ip).setOnlineStatus(newStatus);
            final JsonObject select = new JsonObject().put(DB.GENERIC_OBJECT_ID_KEY, this.currGwMap.get(ip).getId());
            final JsonObject updateField = new JsonObject().put(GatewayJsonProperty.ONLINE_PROPERTY, newStatus);
            final JsonObject update = new JsonObject().put(DB.SET_FIELD_UPDATE_OPERATOR, updateField);
            DB.get().getConnection().updateCollection(DBCollections.GATEWAYS, select, update, r -> { });
        }
    }
}