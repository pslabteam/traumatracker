ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From TraumaTracker-SmartphoneApp:
* ic_launcher-web.png
* proguard-project.txt
From TraumaTracker-SpeechRecognizerLib:
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:19.1.0

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In TraumaTracker-SpeechRecognizerLib:
* AndroidManifest.xml => traumaTrackerSpeechRecognizerLib\src\main\AndroidManifest.xml
* res\ => traumaTrackerSpeechRecognizerLib\src\main\res\
* src\ => traumaTrackerSpeechRecognizerLib\src\main\java\
In TraumaTracker-SmartphoneApp:
* AndroidManifest.xml => traumaTrackerSmartphoneApp\src\main\AndroidManifest.xml
* libs\cartago-2.2-SNAPSHOT.jar => traumaTrackerSmartphoneApp\libs\cartago-2.2-SNAPSHOT.jar
* libs\jaca-2.2-SNAPSHOT.jar => traumaTrackerSmartphoneApp\libs\jaca-2.2-SNAPSHOT.jar
* libs\jason-core-2.0.jar => traumaTrackerSmartphoneApp\libs\jason-core-2.0.jar
* libs\javax.json-api-1.0.jar => traumaTrackerSmartphoneApp\libs\javax.json-api-1.0.jar
* libs\lipermi-0.4.jar => traumaTrackerSmartphoneApp\libs\lipermi-0.4.jar
* res\ => traumaTrackerSmartphoneApp\src\main\res\
* src\ => traumaTrackerSmartphoneApp\src\main\java\
* src\jason\infra\centralised\.DS_Store => traumaTrackerSmartphoneApp\src\main\resources\jason\infra\centralised\.DS_Store
* src\traumatracker\main\project.mas2j => traumaTrackerSmartphoneApp\src\main\resources\traumatracker\main\project.mas2j
* src\traumatracker\main\speechRecognizerAgent.asl => traumaTrackerSmartphoneApp\src\main\resources\traumatracker\main\speechRecognizerAgent.asl
* src\traumatracker\main\trackerAgent.asl => traumaTrackerSmartphoneApp\src\main\resources\traumatracker\main\trackerAgent.asl

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
