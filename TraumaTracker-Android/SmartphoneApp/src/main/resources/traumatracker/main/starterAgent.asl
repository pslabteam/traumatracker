!init.

+!init
	<-  makeArtifact("localizationManager","it.unibo.disi.pslab.traumatracker.smartphoneapp.LocalizationManager",[],LocalizationManager);
	    makeArtifact("starterUI", "it.unibo.disi.pslab.traumatracker.smartphoneapp.StarterUI",[],StarterUI);
    	focus(StarterUI);
    	makeArtifact("ttService","it.unibo.disi.pslab.traumatracker.smartphoneapp.TTService",[],TTService);
        makeArtifact("fileSystem","it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem",[],FileSystem);
    	println("StarterUI artifact created & focussed.").

+ui_ready [artifact_name(Id,starterUI)]
    <- println("StarterUI ready.").

+downloadUsersList
    <-  requestUsersList(UsersList);
        saveUsersList(UsersList);
        onUsersListAvailable(UsersList).

+downloadRoomsList
    <- requestRoomsList(RoomsList);
       saveRoomsList(RoomsList);
       onRoomsListAvailable(RoomsList).

+uploadReport(Id, Content)
    <-  .concat("upload request: ", Id, Msg);
        println(Msg);
        sendReport(Content, Res);
        if(Res == 200){
            println("start upload media...");
            uploadReportMedia(Content);
            println("uploading done!");
        }
        onReportSent(Id, Res).
