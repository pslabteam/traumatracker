bt_server_status("off").

!preinit.

+!preinit
    <- makeArtifact("bluetoothChannel", "it.unibo.disi.pslab.traumatracker.smartphoneapp.BluetoothChannel", [], Id);
       +art_id(bluetoothChannel, Id);
       focus(Id);
       !discover("starterUI");
       lookupArtifact("starterUI", UiId);
       focus(UiId);
       println("Waiting...").

+!discover(Name)
	<- lookupArtifact(Name, Id);
	   +art_id(Name,Id).

-!discover(Name)
	<-  .wait(1000);
		!discover(Name).

+activate_glasses : bt_server_status("off")
    <-  println("GLASS SUPPORT ACTIVE");
        turnOnBluetoothServer;
        -+bt_server_status("on").

+deactivate_glasses : bt_server_status("on")
    <-  println("GLASS SUPPORT INACTIVE");
        turnOffBluetoothServer;
        -+bt_server_status("off").

+glass_status("connected")
    <-  println("glass attached!");
        !discover("externalDisplay");
        lookupArtifact("externalDisplay", ExternalDisplayId);
        focus(ExternalDisplayId);
        enableExternalDisplay.

+glass_status("disconnected")
    <-  !discover("externalDisplay");
        lookupArtifact("externalDisplay", ExternalDisplayId);
        focus(ExternalDisplayId);
        disableExternalDisplay;
        println("glass not attached!").