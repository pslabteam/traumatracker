!init.

+!init
	<-  !setupTools;
		println("Ready to go, if enabled!").

+start_speech_recognizer
    <-  println("Speech Recognizer enabled!");
        startSpeechRecognizer;
        println("SpeechRecognizer Turned On");
        .wait(1000).
        //!test.
		
+!test 
	<-  notifyTakeSnapshot;
	    .wait(2000);
	    notifyNewDrug("midazolam","mg",1000);
	    .wait(1000);
	    notifyNewProcedure("catetere alto flusso");
		.wait(2000);
	    notifyGetVitalSigns;
		.wait(5000);
		!test.

+listening(IsListening).
    //<-  showSpeechRecognizerStatus(IsListening).

+tracking_status(Status)
    <-  println("new tracking status... ",Status).
        //showTrackingStatus(Status).

+speech_cmd(snap)
    <-  notifyTakeSnapshot.

+speech_cmd(newProc(Proc))
    <-  println("new procedure.. ",Proc);
        notifyNewProcedure(Proc).

+speech_cmd(newDrug(Type,Qty,Unit))
    <-  println("new drug.. ",Type,Qty,Unit);
        notifyNewDrug(Type,Unit,Qty).

+speech_cmd(getVitalSigns)
    <-  println("get vital signs.. ");
        notifyGetVitalSigns.

+cmd_not_recognized
    <- println("cmd not recognized.").
  //    showCmdNotRecognized("cmd not recognized.").

+!setupTools
	<-	!discover("console");
		!discover("userCmds");
		!discover("display");
		!discover("mainUI");
		println("creating the speechBoard");
		makeArtifact("speechBoard", "it.unibo.disi.pslab.traumatracker.smartphoneapp.SpeechBoard", [], Id);
		focus(Id);
		lookupArtifact("mainUI", Idx);
        focus(Idx).

+!discover(Name) 
	<- lookupArtifact(Name, Id);
	   +art_id(Name,Id).

-!discover(Name)
	<-  .wait(100);
		!discover(Name).
