!preinit.

+!preinit
    <- !discover("starterUI");
       lookupArtifact("starterUI", Id);
       focus(Id);
       println("Waiting...").

+!discover(Name)
	<- lookupArtifact(Name, Id);
	   +art_id(Name,Id).

-!discover(Name)
	<-  .wait(100);
		!discover(Name).

+start_tracker(PrevReportId)
    <-  println("Ready to go...");
        +prevReportId(PrevReportId);
        !init.

+!init
	<-  makeArtifact("mainUI", "it.unibo.disi.pslab.traumatracker.smartphoneapp.MainUI", [], Id);
		println("mainUI built: ",Id);
    	+art_id(mainUI, Id);
    	focus(Id);
    	println("MainUI artifact created & focussed.").

+ui_ready [artifact_name(Id,mainUI)] : prevReportId(ID)
    <-  println("MainUI ready.");
	    !setupTools;
	    onEnvironmentSetupDone(ID);
	    .send(hospnet, tell, active)
	    println("Environment setup, start tracking.").
	 		
+!setupTools
	<-  println("Setting up artifacts...");
		//makeArtifact("userCmds","it.unibo.disi.pslab.traumatracker.smartphoneapp.UserCmdChannel",[],Queue);
		//focus(Queue);
		//makeArtifact("externalCamera","it.unibo.disi.pslab.traumatracker.smartphoneapp.ExternalCamera",[],_);
		//makeArtifact("externalDisplay","it.unibo.disi.pslab.traumatracker.smartphoneapp.ExternalDisplay",[],ExternalDisplay);
		//lookupArtifact("bluetoothChannel", BTChannel);
        //linkArtifacts(ExternalDisplay,"out-bt",BTChannel);
		makeArtifact("notes", "it.unibo.disi.pslab.traumatracker.smartphoneapp.NoteStream",[],Notes);
		focus(Notes);
		makeArtifact("events", "it.unibo.disi.pslab.traumatracker.smartphoneapp.EventStream",[],Events);
        linkArtifacts(Notes,"eventStream",Events);
        makeArtifact("timeSheet","it.unibo.disi.pslab.traumatracker.smartphoneapp.TimeSheet",[],TimeSheet);
        focus(TimeSheet);
        makeArtifact("activeTrauma","it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma",[],ActiveTrauma);
        focus(ActiveTrauma);
        makeArtifact("barcodeReader","it.unibo.disi.pslab.traumatracker.smartphoneapp.BarcodeReader",["mainUI"],Barcode);
        focus(Barcode);
        makeArtifact("locationService","it.unibo.disi.pslab.traumatracker.smartphoneapp.LocationService",["push-notifications"],LocationService);
		focus(LocationService);
		makeArtifact("vitalSignsService","it.unibo.disi.pslab.traumatracker.smartphoneapp.VitalSignsService",["real-monitor-mode",300000],_);
		makeArtifact("prehService","it.unibo.disi.pslab.traumatracker.smartphoneapp.PrehService",[],PrehService).

-!setupTools
    <-  println("Not first time. just focus");
        //lookupArtifact("userCmds", Queue);
        //focus(Queue);
        lookupArtifact("locationService", LocationService);
        focus(LocationService);
        lookupArtifact("activeTrauma", ActiveTrauma);
        focus(ActiveTrauma);
        lookupArtifact("timeSheet", TimeSheet);
        focus(TimeSheet).

+user_changed(User) : place(Where)
    <- updateTraumaLeaderInfoOnUIStatusBar(User);
       addTraumaLeaderNote(User,Where).

+place(P)
    <-  !registerRoomEvent(P);
        !updateLocationOnUI(P);
        notifyRoomChangeToTimeSheet(P).
        //updatePlaceInfo(P).

+!registerRoomEvent(P) : current_place(CP)
    <-  registerRoomOutEvent(CP);
        registerRoomInEvent(P);
        -+current_place(P).

+!registerRoomEvent(P)
    <-  registerRoomInEvent(P);
        -+current_place(P).

+!updateLocationOnUI(P)
    <-  updateLocationOnUIStatusBar(P).

-!updateLocationOnUI(_).

+user_cmd("start_tracking", Room, TraumaLeader, DelayedActivation, OriginalActivationDate, OriginalActivationTime)
    <-  println("> START TRACKING");
        registerStartEvent(TraumaLeader, DelayedActivation, OriginalActivationDate, OriginalActivationTime, ReportId);
        changePlaceInfo(Room);
        .send(hospnet, achieve, setupHospNet(ReportId));
        .send(backup, tell, active).

+user_cmd("restart_tracking", Id)
    <-  println("> RESTART TRACKING ");
        //startPlaceDetection;
        registerRestartEvent(Id);
        restorePlaceInfo(Id, P);
        //+current_place(P);
        //!updateLocationOnUI(P);
        //notifyRoomChangeToTimeSheet(P);
        //updatePlaceInfo(P);
        addReportReactivationNote(P);
        .send(backup, tell, active).

+user_cmd("end_tracking", Destination, Iss)
    <-  registerTerminationEvent(Destination, Iss);
        .send(hospnet, achieve, stopHospNet);
        changePlaceInfo("");
        dismissTimeSheet;
        println("> END TRACKING ");
        //.send(vitalSignsObserver, tell, stop);
        waitForServerFeedback;
        buildReport(Report, ReportName);
        println(Report);
        saveReportToLocal(Report, ReportName);
        println("Report saved to local storage")
        sendReport(Report, Res);
        if(Res == 200){
            moveSyncReport(ReportName);
            uploadReportMedia(Report);
        }
        onServerFeedback(Res);
        .send(backup, untell, active).

+user_cmd("end_tracking")
    <-  .send(hospnet, achieve, stopHospNet);
        changePlaceInfo("");
        println("> END TRACKING WITHOUT SAVING");
        //.send(vitalSignsObserver, tell, stop);
        .send(backup, untell, active).

+user_cmd("change_room", Place)
    <- changePlaceInfo(Place).

+user_cmd("patient_accessed_er") : place(Where)
    <-  registerPatientAccessedErEvent(Where);
        .send(hospnet, achieve, startHospNet).
        //.send(vitalSignsObserver, tell, start).

+user_cmd("new_procedure",Procedure,Options) : place(Where)
	<-  //showNewProcedurePerformedMessage(Procedure);
	  	addProcedureNote(Where, Procedure, Options).

+user_cmd("new_procedure",Procedure,Options, Where)
	<-  .wait(500);
	    //showNewProcedurePerformedMessage(Procedure);
	  	addProcedureNote(Where, Procedure, Options).

+user_cmd("new_time_dependent_procedure",Procedure,EventType,Options) : place(Where)
	<-  addTimeDependentProcedureNote(Where, Procedure, EventType, Options);
	  	notifyTimeDependentProcedureNewState(Procedure, EventType).

+user_cmd("new_diagnostic",Diagnostic,Options) : place(Where)
	<-  //showNewDiagnosticPerformedMessage(Diagnostic);
	  	addDiagnosticNote(Where, Diagnostic, Options).

+user_cmd("new_diagnostic",Diagnostic,Options, Where)
	<-  .wait(500);
	    //showNewDiagnosticPerformedMessage(Diagnostic);
	  	addDiagnosticNote(Where, Diagnostic, Options).

+user_cmd("new_drug",Drug,Qty) : place(Where)
	<- 	//showNewDrugMessage(Drug, Qty);
	  	addDrugNote(Where, Drug, Qty).

+user_cmd("new_drug",Drug) : place(Where)
	<- 	//showNewDrugMessage(Drug);
	  	addDrugNote(Where, Drug).

+user_cmd("new_blood_product",BloodProduct,Qty) : place(Where)
	<- 	addBloodProductNote(Where, BloodProduct, Qty).

+user_cmd("new_blood_product",BloodProduct,Qty,BagCode) : place(Where)
	<- 	addBloodProductNote(Where, BloodProduct, Qty, BagCode).

+user_cmd("new_blood_product",BloodProduct) : place(Where)
	<- 	addBloodProductNote(Where, BloodProduct).

+user_cmd("start_continuous_infusion_drug",Drug,Qty) : place(Where)
	<- 	addStartContinuousInfusionDrugNote(Where, Drug, Qty).

+user_cmd("variate_continuous_infusion_drug",Drug,Qty) : place(Where)
	<- 	addVariationContinuousInfusionDrugNote(Where, Drug, Qty).

+user_cmd("stop_continuous_infusion_drug",Drug,Duration) : place(Where)
	<- 	addStopContinuousInfusionDrugNote(Where, Drug, Duration).

+user_cmd("vital_signs_monitor_status", Sys, Dia, Hr, Etco2, Spo2, Temp) : place(Where)
    <-   addVitalSignMonitorNote(Where, Sys, Dia, Hr, Etco2, Spo2, Temp).

+user_cmd("vital_sign_variation", VitalSign, Value) : place(Where)
    <-  //showVitalSignVariationMessage(VitalSign, Value);
        addVitalSignVariationNote(Where, VitalSign, Value).

+user_cmd("update_event_info", EventId, NewDate, NewTime, NewPlace)
    <-  updateInfoForEvent(EventId, NewDate, NewTime, NewPlace).

+user_cmd("take_snap")
	<-  //showCmdOk("snap");
		snap(Ref);
		+photo_available(Ref).

+photo_available(Ref) : place(Where)
    <-  println("snap available: ",Ref);
        addSnapshotNote(Where,Ref).
        //showCmdDone("snap").

//+user_cmd("take_video").

+video_available(Ref) : place(Where)
    <-  println("video available: ",Ref);
        addVideoNote(Where,Ref).
        //showCmdDone("video").

//+user_cmd("take_vocal_note").

+vocal_note_available(Ref) : place(Where)
    <-  println("vocal note available: ",Ref);
        addVocalNote(Where,Ref).
        //showCmdDone("vocal note").

+text_note_available(Note) : place(Where)
    <-  println("Text note available: ",Note);
        addTextNote(Where,Note).

+barcode_request(Type)
    <-  readBarcode(Type).

+barcode_available(Type, Content)
    <-  updateBarcodeOnUI(Type, Content).

+retrieve_preh_missions
    <- waitForPrehDataRetrieval;
       retrievePrehMissions(Res, List);
       onPreHMissionsListReceived(Res, List).

+retrieve_preh_mission_data(MissionId)
    <- waitForPrehDataRetrieval;
       retrievePrehMissionData(MissionId, Res, Patient, VitalSigns);
       onPreHMissionDataReceived(Res, Patient, VitalSigns).
