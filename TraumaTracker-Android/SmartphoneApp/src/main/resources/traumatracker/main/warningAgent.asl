!preinit.

+!preinit
    <- !discover("starterUI");
       lookupArtifact("starterUI", Id);
       focus(Id);
       println("Waiting...").

+!discover(Name)
	<- lookupArtifact(Name, Id);
	   +art_id(Name,Id).

-!discover(Name)
	<-  .wait(100);
		!discover(Name).

+init_warning_agent
    <-  println("Initialized!");
        !discover("events");
        lookupArtifact("events", EventStream);
        focus(EventStream);
        makeArtifact("warningDisplay","it.unibo.disi.pslab.traumatracker.smartphoneapp.WarningDisplay",[],WarningDisplay);
        focus(WarningDisplay);
        println("Started!").

+new_event(E)
    <-  timestamp(T);
        +event(E, T).
        //println("added to BB! event(",E,",",T,")").

// ---- RULE-2 ----
// Se somministro zero negativo 3U e acido tranexamico e fibrinogeno compare "attivare PTM?"

+event(drug("zero-negative",_),T) : zero_negative(3) & event(drug("tranex",_),_) & event(drug("fibrinogen",_),_)
    <-  +warning("activatePTM",T).

+event(drug("tranex",_),T) : zero_negative(3) & event(drug("fibrinogen",_),_)
    <-  +warning("activatePTM",T).

+event(drug("fibrinogen",_),T) : zero_negative(3) & event(drug("tranex",_),_)
        <-  +warning("activatePTM",T).

+warning("activatePTM", T)
    <-  displayWarning("Activate PTM?", T).

// ---- RULE-1 ----
// Se somministro "zero negativo" dopo 5min compare "hai sommninistrato fibrinogeno
// e acido tranexamico?" se ancora non sono stati cliccati e/o somministrati.

+event(drug("zero-negative",_),_)
    <-  .wait(3000); //300000
        !checkTranexAndFibrinogen.

+!checkTranexAndFibrinogen : not event(drug("tranex",_),_) | not event(drug("fibrinogen",_),_)
    <-  timestamp(T);
        +warning("checkTranexAndFibrinogen", T).

+event(drug("tranex",_), _) : warning("checkTranexAndFibrinogen") & event(drug("fibrinogen",_),_)
	<- -warning("checkTranexAndFibrinogen", _).

+event(drug("fibrinogen",_), _) : warning("checkTranexAndFibrinogen") & event(drug("tranex",_),_)
	<- -warning("checkTranexAndFibrinogen", _).

+warning("checkTranexAndFibrinogen", T)
    <- displayWarning("Tranex and Fibrinogen already administered?", T).

// ---- RULE-3 ----
// Se clicco "TIOPENTONE" compare "attenzione all' ipotensione!"

+event(drug("thiopental",_),T)
    <-  +warning("checkHypotension", T).

+warning("checkHypotension", T)
    <- displayWarning("Consider patient hypotension!", T).

// ---- RULE-4 & RULE-5 ----
// (4) Se inizio ALS ogni 3 min compare "somministra adrenalina 1mg"
// (5) Se inizio ALS dopo 5min compare "decompressione pleurica?" se non già fatta

+event(procedureStarted("als"),T)
    <-  +als(active);
        !!manageAdrenalineAdministration;
        !!managePleuralDecompressionCheck.

+event(procedureStopped("als"),_)
	<- -als(active).

+!manageAdrenalineAdministration : als(active)
    <-  .wait(1000); //180000
        timestamp(T);
        +warning("adminAdrenaline", T);
        !manageAdrenalineAdministration.

+warning("adminAdrenaline", T)
    <- displayWarning("Administer Adrenaline 1mg", T).

+!managePleuralDecompressionCheck
    <-  .wait(3000); //300000
        !checkPleuralDecompression.

+!checkPleuralDecompression : not event(procedure("chest-tube"),_)
    <-  timestamp(T);
        +warning("pleuralDecompression",T).

+warning("pleuralDecompression", T)
    <-  displayWarning("Pleural Decompression ?", T).

// ---- RULE-6 ----
// Se clicco "Frattura Esposta SI" quando cambio stanza uscendo da shock room e non ho ancora
// somministrato antibiotico chiedere "antibiotico?"

+event(roomOut("Shock-Room"),T) : fractures(true) & not event(drug("antibiotic-prophylaxis", _),_)
	<- +warning("checkABT", T).

+warning("checkABT", T)
    <- displayWarning("Administer Antibiotic?", T).

// ---- RULE-7 ----
// ogni 15min dopo inizio delle manovre "TOURNIQUET" o "REBOA" o "TORACOTOMIA RESUSCITATIVA"
// fare comparire " TOURNIQUET (o REBOA o TORACO...) da 15 min, poi ...da 30min ... poi "da 45 min" ecc..

//TODO: serve un warning con parametro?

// ---- RULE-8 ----
// Quando cambio stanza in uscita da shock room compare "hai posizionato SNG e foley vescicale?"

+event(roomOut("Shock-Room"),T)
    <- +warning("ShockRoomExit", T).

+warning("ShockRoomExit", T)
    <-  displayWarning("SNG and bladder foley correctly setted?", T).

// ---- RULE-9 ----
// Se paziente con tubo tracheale o dopo intubazione tracheale
// dopo 5 min compare "check EtCO2" se ancora non è arrivato alcun dato di EtCO2

//TODO: completare con tracheo

+event(procedure("intubation"),T) : not event(vs("etco2-value",_),_)
    <-  .wait(3000); //300000
        !checkEtco2.

+!checkEtco2 : not event(vs("etco2-value",_),_)
    <-  timestamp(T);
        +warning("checkEtco2", T).

+warning("checkEtco2", T)
    <-  displayWarning("Check EtCO2", T).


// ---- RULE-10 ----
// Se SpO2 < 90% compare "FiO2 impostata al 100% ?"

+event(vs("spo2-value",Spo2),T) : Spo2 < 90
    <-  +warning("CheckFio2", T).

+warning("CheckFio2", T)
    <-  displayWarning("FiO2 setted to 100%?", T).

// ---- RULE-11 ----
// Se clicco "infusione a pressione" e la pressione arteriosa sistolica
// supera 110mmHg compare "sospendere infusione a pressione?"

+event(vs("bp-value",Bp),T) : event(procedure("infuser"), _) & Bp > 110
    <-  +warning("InterruptInfuser", T).

+warning("InterruptInfuser", T)
    <-  displayWarning("Interrupt Infuser?", T).


// ---- PLANS FOR ACTIONS ----

+event(action("ignoreWarning",W)) : warning(W,_)
<-  -warning(W,_).