backup_interval(5000).

!preinit.

+!preinit
    <- !discover("starterUI");
       lookupArtifact("starterUI", Id);
       focus(Id);
       println("Waiting...").

+!discover(Name)
	<- lookupArtifact(Name, Id);
	   +art_id(Name,Id).

-!discover(Name)
	<-  .wait(100);
		!discover(Name).

+init_backup_agent(Id)
    <-  +reportId(Id).

@start[atomic]
+active
    <-  -+state("running");
        println("new state -> running");
        !!waitForBackup.

@stop[atomic]
-active : reportId(Id)
    <-  -+state("idle");
        println("new state -> idle");
        deleteBackupReport(Id);
        .concat("temp report (",Id,") removed",Msg);
        println(Msg).

+!waitForBackup : backup_interval(T)
    <-  .wait(T);
        !backup.

+!backup : state("running")
    <-  buildReport(Report, Id);
        updateBackupReport(Report, Id);
        -+reportId(Id);
        //println("backup done!");
        !!waitForBackup.

+!backup : state("idle").