!init.

+!init
    <-  println("Waiting for artifacts...");
        !discover("timeSheet");
        lookupArtifact("timeSheet", TimeSheet);
        focus(TimeSheet);
        !discover("monitor");
        lookupArtifact("monitor", Monitor);
        focus(Monitor);
        !discover("activeTrauma");
        lookupArtifact("activeTrauma",ActiveTrauma);
        focus(ActiveTrauma).

+!discover(Name)
	<- lookupArtifact(Name, Id);
	   +art_id(Name,Id).

-!discover(Name)
	<-  .wait(100);
		!discover(Name).

+start
    <-  +state("running");
        !!requireVitalSigns.

+restart
    <-  -+state("running");
        !contactMonitor.

+stop
    <-  -+state("idle").

+!requireVitalSigns : interval(T) & state("running")
    <-  -+state("running")
        //!contactMonitor;
        .wait(T);
        !requireVitalSigns.

+!requireVitalSigns : interval(T) & state("idle").

+!contactMonitor
    <-  getVitalSigns(Res, Sys, Dia, Hr, Etco2, Spo2, Temp);
        if(Res == 200){
          notifyVitalSignsValues(Sys, Dia, Hr, Etco2, Spo2, Temp);
        } else {
          notifyMonitorUnreachableAlert;
        }.