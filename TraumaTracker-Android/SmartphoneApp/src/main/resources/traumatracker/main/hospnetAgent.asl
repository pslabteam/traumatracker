!preinit.

+!preinit
    <- !discover("starterUI");
       lookupArtifact("starterUI", Id);
       focus(Id);
       println("Waiting...").

+!discover(Name)
	<- lookupArtifact(Name, Id);
	   +art_id(Name,Id).

-!discover(Name)
	<-  .wait(100);
		!discover(Name).

@start[atomic]
+active
    <-  -+state("running");
        println("new state -> running");
        lookupArtifact("locationService", LocationService);
        lookupArtifact("vitalSignsService", VitalSignsService);
        -+vss_status("unreachable");
        -+ls_status("unreachable").

@stop[atomic]
-active : reportId(Id)
    <-  -+state("idle");
        println("new state -> idle").

+!setupHospNet(Sid)
    <-  println("Called SETUP");
        !!setupVSS(Sid);
        !!setupLS.

+!setupVSS(Sid) : vss_status("unreachable")
    <-  checkVitalSignsServiceReachability(Status);
        if(Status == "idle") {
            -+vss_status("idle")
            onVitalSignsServiceStatusChange(Status);
            createVitalSignsSession(Sid, 30000, Res);
            println("VS Reachable. Created VS Session.");
        } else {
            println("VSS Waiting 1s and retry...");
            .wait(1000);
            !setupVSS(Sid);
        }.

+!setupLS : ls_status("unreachable")
    <-  checkLocationServiceReachability(Status);
        if(Status == "idle") {
            -+ls_status("idle");
            onLocationServiceStatusChange(Status);
            println("LS Reachable");
        } else {
            println("LS Waiting 1s and retry...");
            .wait(1000);
            !setupLS;
        }.

+!startHospNet
    <-  println("Called START");
        !!startVsMonitoring;
        !!startPlaceDetection.

+!startVsMonitoring : vss_status("unreachable")
    <-  .wait(1000);
        !startVsMonitoring.

+!startVsMonitoring : vss_status("idle")
    <-  startVitaSignsSession(Status);
        onVitalSignsServiceStatusChange(Status);
        -+vss_status(Status);
        println("Monitoring started").

-!startVsMonitoring
    <-  println("VSS nothing to do").

+!startPlaceDetection : ls_status("unreachable")
    <-  .wait(1000);
        !startPlaceDetection.

+!startPlaceDetection : ls_status("idle")
    <-  startPlaceDetection(Status);
        onLocationServiceStatusChange(Status);
        -+ls_status(Status);
        println("Place Detection started").

-!startPlaceDetection
    <-  println("LS nothing to do").

+!stopHospNet
    <-  println("Called STOP");
        !!stopVsMonitoring;
        !!stopPlaceDetection.

+!stopVsMonitoring : vss_status("active")
    <-  stopVitaSignsSession(Status);
        onVitalSignsServiceStatusChange(Status);
        -+vss_status("unreachable");
        println("Monitoring Stopped").

+!stopPlaceDetection : ls_status("active")
    <-  stopPlaceDetection(Status);
        onLocationServiceStatusChange(Status);
        -+ls_status("unreachable");
        println("Place detection stopped").

