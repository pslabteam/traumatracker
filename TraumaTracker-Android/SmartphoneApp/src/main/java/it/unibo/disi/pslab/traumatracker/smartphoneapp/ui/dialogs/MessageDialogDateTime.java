package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.DatePicker;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTime;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerTimePicker;

public class MessageDialogDateTime extends TTDialog {

    private DateTime dt;

    MessageDialogDateTime(Context context, String title, DateTime initialDT, final Listener listener) {
        super(context, R.layout.dialog_datetime);

        if(initialDT != null){
            dt = initialDT;
        } else {
            dt = new DateTime();
        }

        intiUI(title, listener);
    }

    private void intiUI(String title, final Listener listener) {
        (((TextView) findViewById(R.id.title))).setText(title);

        final DatePicker dp = findViewById(R.id.datePicker);
        dp.setCalendarViewShown(false);
        dp.setMaxDate(System.currentTimeMillis());

        final TraumaTrackerTimePicker tp = findViewById(R.id.timePicker);
        tp.setIs24HourView(true);

        if(dt.isDateInit()){
            dp.updateDate(dt.year(), dt.month() - 1, dt.day());
        }

        if(dt.isTimeInit()){
            tp.setCurrentHour(dt.hour());
            tp.setCurrentMinute(dt.minute());
            tp.setCurrentSecond(dt.second());
        }

        findViewById(R.id.yesButton).setOnClickListener(v -> {
            if (listener != null) {

                dt.setDate(dp.getYear(), dp.getMonth() + 1, dp.getDayOfMonth());
                dt.setTime(tp.getCurrentHour(), tp.getCurrentMinute(), tp.getCurrentSeconds());

                listener.onYesButtonClicked(dt);
            }

            dismiss();
        });

        findViewById(R.id.noButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onNoButtonClicked();
            }

            dismiss();
        });
    }

    public interface Listener {
        void onNoButtonClicked();
        void onYesButtonClicked(DateTime dt);
    }
}
