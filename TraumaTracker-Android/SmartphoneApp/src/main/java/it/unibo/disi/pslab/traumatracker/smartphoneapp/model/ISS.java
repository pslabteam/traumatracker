package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class ISS {
    private boolean isPresumed;

    private Map<Element, String> aisMap;

    private static final int ISS_MIN_VALUE = 0;
    private static final int ISS_MAX_VALUE = 75;

    private static final int ISS_UNSURVIVABLE_VALUE = 6;

    private static final int ISS_CRITIC_THRESHOLD = 3;

    public ISS() {
        aisMap = new HashMap<>();
        for (Element e : Element.values()) {
            aisMap.put(e, "");
        }

        isPresumed = false;
    }

    public String getElementAIS(Element e) {
        return aisMap.get(e);
    }

    public void setElementAIS(Element e, String ais) {
        aisMap.put(e, ais);
    }

    public String getGroupISS(Group g) {
        int higherAis = 0;

        boolean setted = false;

        for (final Element e : Element.values()) {
            if (e.group.equals(g) && !aisMap.get(e).isEmpty()) {
                int currentAis = Integer.parseInt(aisMap.get(e));
                higherAis = Math.max(currentAis, higherAis);
                setted = true;
            }
        }

        if (setted) {
            int iss = higherAis * higherAis;

            return "" + iss;
        } else {
            return "";
        }
    }

    public String getTotalISS() {
        List<Integer> issList = new ArrayList<>();

        boolean setted = false;

        for (Group g : Group.values()) {
            if (!getGroupISS(g).isEmpty()) {
                issList.add(Integer.parseInt(getGroupISS(g)));
                setted = true;
            }
        }

        if (setted) {
            Collections.sort(issList);
            Collections.reverse(issList);

            int totalIss = ISS_MIN_VALUE;

            for (int i = 0; i < ISS_CRITIC_THRESHOLD && i < issList.size(); i++) {
                int currentIss = issList.get(i);

                if (currentIss == (ISS_UNSURVIVABLE_VALUE * ISS_UNSURVIVABLE_VALUE)) {
                    return "" + ISS_MAX_VALUE;
                }

                totalIss += currentIss;
            }

            return "" + totalIss;
        } else {
            return "";
        }
    }

    public boolean isPresumed(){
        return isPresumed;
    }

    public void setIsPresumed(boolean isFinal){
        this.isPresumed = isFinal;
    }

    public JSONObject toJSONRepresentation() throws JSONException {
        return new JSONObject()
                .putOpt(C.report.iss.IS_PRESUMED, isPresumed)
                .putOpt(C.report.iss.TOTAL_ISS, getTotalISS().isEmpty()
                        ? null
                        : Integer.parseInt(getTotalISS()))
                .putOpt(C.report.iss.AIS_MAP_TYPE, AisMapType.CENTER_TBI.toString())
                .putOpt(C.report.iss.AIS_MAP, new JSONObject()
                        .putOpt(C.report.iss.aisMap.EXTERNA_GROUP, new JSONObject()
                                .putOpt(C.report.iss.aisMap.externaGroup.TOTAL_ISS, getGroupISS(Group.EXTERNA).isEmpty()
                                        ? null
                                        : Integer.parseInt(getGroupISS(Group.EXTERNA)))
                                .putOpt(C.report.iss.aisMap.externaGroup.EXTERNA_AIS, getElementAIS(Element.EXTERNA).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.EXTERNA))))
                        .putOpt(C.report.iss.aisMap.HEAD_GROUP, new JSONObject()
                                .putOpt(C.report.iss.aisMap.headGroup.TOTAL_ISS, getGroupISS(Group.HEAD).isEmpty()
                                        ? null
                                        : Integer.parseInt(getGroupISS(Group.HEAD)))
                                .putOpt(C.report.iss.aisMap.headGroup.HEAD_NECK_AIS, getElementAIS(Element.HEAD_NECK).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.HEAD_NECK)))
                                .putOpt(C.report.iss.aisMap.headGroup.BRAIN_AIS, getElementAIS(Element.BRAIN).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.BRAIN)))
                                .putOpt(C.report.iss.aisMap.headGroup.CERVICAL_SPINE_AIS, getElementAIS(Element.CERVICAL_SPINE).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.CERVICAL_SPINE)))
                                .putOpt(C.report.iss.aisMap.headGroup.FACE_AIS, getElementAIS(Element.FACE).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.FACE))))
                        .putOpt(C.report.iss.aisMap.TORAX_GROUP, new JSONObject()
                                .putOpt(C.report.iss.aisMap.toraxGroup.TOTAL_ISS, getGroupISS(Group.TORAX).isEmpty()
                                        ? null
                                        : Integer.parseInt(getGroupISS(Group.TORAX)))
                                .putOpt(C.report.iss.aisMap.toraxGroup.TORAX_AIS, getElementAIS(Element.TORAX).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.TORAX)))
                                .putOpt(C.report.iss.aisMap.toraxGroup.SPINE_AIS, getElementAIS(Element.SPINE).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.SPINE))))
                        .putOpt(C.report.iss.aisMap.ABDOMEN_GROUP, new JSONObject()
                                .putOpt(C.report.iss.aisMap.abdomenGroup.TOTAL_ISS, getGroupISS(Group.ABDOMEN).isEmpty()
                                        ? null
                                        : Integer.parseInt(getGroupISS(Group.ABDOMEN)))
                                .putOpt(C.report.iss.aisMap.abdomenGroup.ABDOMEN_AIS, getElementAIS(Element.ABDOMEN).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.ABDOMEN)))
                                .putOpt(C.report.iss.aisMap.abdomenGroup.LUMBAR_SPINE_AIS, getElementAIS(Element.LUMBAR_SPINE).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.LUMBAR_SPINE))))
                        .putOpt(C.report.iss.aisMap.PELVIC_GIRDLE_GROUP, new JSONObject()
                                .putOpt(C.report.iss.aisMap.pelvicGirdleGroup.TOTAL_ISS, getGroupISS(Group.PELVIC_GIRDLE).isEmpty()
                                        ? null
                                        : Integer.parseInt(getGroupISS(Group.PELVIC_GIRDLE)))
                                .putOpt(C.report.iss.aisMap.pelvicGirdleGroup.PELVIC_GIRDLE_AIS, getElementAIS(Element.PELVIC_GIRDLE).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.PELVIC_GIRDLE))))
                        .putOpt(C.report.iss.aisMap.EXTREMITIES_GROUP, new JSONObject()
                                .putOpt(C.report.iss.aisMap.extremitiesGroup.TOTAL_ISS, getGroupISS(Group.EXTREMITIES).isEmpty()
                                        ? null
                                        : Integer.parseInt(getGroupISS(Group.EXTREMITIES)))
                                .putOpt(C.report.iss.aisMap.extremitiesGroup.UPPER_EXTREMITIES_AIS, getElementAIS(Element.UPPER_EXTREMITIES).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.UPPER_EXTREMITIES)))
                                .putOpt(C.report.iss.aisMap.extremitiesGroup.LOWER_EXTREMITIES_AIS, getElementAIS(Element.LOWER_EXTREMITIES).isEmpty()
                                        ? null
                                        : Integer.parseInt(getElementAIS(Element.LOWER_EXTREMITIES)))));
    }

    public enum AisMapType{
        DEFAULT("Default"),
        CENTER_TBI("CenterTBI");

        private final String description;

        AisMapType(final String description){
            this.description = description;
        }

        @NonNull
        public String toString() {
            return this.description;
        }
    }

    public enum Element {
        EXTERNA(Group.EXTERNA),
        HEAD_NECK(Group.HEAD),
        BRAIN(Group.HEAD),
        CERVICAL_SPINE(Group.HEAD),
        FACE(Group.HEAD),
        TORAX(Group.TORAX),
        SPINE(Group.TORAX),
        ABDOMEN(Group.ABDOMEN),
        LUMBAR_SPINE(Group.ABDOMEN),
        PELVIC_GIRDLE(Group.PELVIC_GIRDLE),
        UPPER_EXTREMITIES(Group.EXTREMITIES),
        LOWER_EXTREMITIES( Group.EXTREMITIES);

        private final Group group;

        Element(Group group) {
            this.group = group;
        }

        public Group getGroup() {
            return group;
        }
    }

    public enum Group {
        EXTERNA,
        HEAD,
        TORAX,
        ABDOMEN,
        PELVIC_GIRDLE,
        EXTREMITIES
    }
}
