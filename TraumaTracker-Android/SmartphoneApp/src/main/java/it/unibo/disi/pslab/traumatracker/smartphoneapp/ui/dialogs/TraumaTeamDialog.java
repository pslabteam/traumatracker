package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.TraumaTeamMember;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.UserRole;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class TraumaTeamDialog extends TTDialog {
    private Map<TraumaTeamMember, Integer> ttcb = new HashMap<TraumaTeamMember, Integer>(){{
        put(TraumaTeamMember.EMERGENCY_MEDIC, R.id.tt_member_cbox_er_physician);
        put(TraumaTeamMember.ANAESTHETIST, R.id.tt_member_cbox_second_anesthesiologist);
        put(TraumaTeamMember.RADIOLOGIST, R.id.tt_member_cbox_radiologist);
        put(TraumaTeamMember.EMERGENCY_SURGEON, R.id.tt_member_cbox_urgence_surgeon);
        put(TraumaTeamMember.NEUROSURGEON, R.id.tt_member_cbox_neurological_surgeon);
        put(TraumaTeamMember.ORTHOPEDIC_SURGEON, R.id.tt_member_cbox_orthopedic_surgeon);
        put(TraumaTeamMember.MAXILLO_FACIAL, R.id.tt_member_cbox_maxillofacial);
        put(TraumaTeamMember.UROLOGIST, R.id.tt_member_cbox_urologist);
    }};

    private Spinner traumaLeaderSpinner;
    private List<User> users;

    public TraumaTeamDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_trauma_team);

        try {
            users = FileSystem.getUserList(UserRole.DIRECTOR, UserRole.MEMBER);
        } catch (IOException e) {
            e.printStackTrace();
        }

        initUI(listener);
    }

    private void initUI(final Listener listener){
        findViewById(R.id.exitLabel).setOnClickListener(v -> {
            onExitDialog(listener);
            ActiveTrauma.notifyTraumaTeamUpdate();
        });

        initTraumaLeaderSpinner(users, ActiveTrauma.traumaTeam().currentTraumaLeader());

        Arrays.asList(TraumaTeamMember.values()).forEach(this::initCheckBox);
    }

    private void onExitDialog(final Listener listener){
        final User selectedTraumaLeader = (User) traumaLeaderSpinner.getSelectedItem();

        if(!ActiveTrauma.traumaTeam().currentTraumaLeader().equals(selectedTraumaLeader)){
            ActiveTrauma.traumaTeam().updateTraumaLeader(selectedTraumaLeader);
            listener.onTraumaLeaderChanged(selectedTraumaLeader);
        }

        dismiss();
    }

    private void initTraumaLeaderSpinner(final List<User> users, final User currentTraumaLeader){
        traumaLeaderSpinner = findViewById(R.id.tl_spinner);

        findViewById(R.id.tl_spinner_icon)
                .setOnClickListener(v -> traumaLeaderSpinner.performClick());

        final ArrayAdapter<User> spinnerAdapter = new ArrayAdapter<>(getContext(), R.layout.traumatracker_spinner_item, users);
        spinnerAdapter.setDropDownViewResource(R.layout.traumatracker_spinner_dropdown);

        traumaLeaderSpinner.setAdapter(spinnerAdapter);
        traumaLeaderSpinner.setSelection(users.indexOf(currentTraumaLeader));
    }

    private void initCheckBox(final TraumaTeamMember member) {
        final CheckBox cb = findViewById(ttcb.get(member));

        cb.setOnClickListener(v -> {
            if(((CheckBox) v).isChecked()){
                ActiveTrauma.traumaTeam().addTeamMember(member.toString());
            } else {
                ActiveTrauma.traumaTeam().removeTeamMember(member.toString());
            }
        });

        cb.setChecked(ActiveTrauma.traumaTeam().isMemberPresent(member.toString()));
    }

    public interface Listener{
        void onTraumaLeaderChanged(User user);
    }
}
