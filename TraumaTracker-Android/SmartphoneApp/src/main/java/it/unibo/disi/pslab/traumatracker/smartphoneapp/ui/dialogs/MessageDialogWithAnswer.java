package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class MessageDialogWithAnswer extends TTDialog {

    public MessageDialogWithAnswer(Context context, String title, String message, final Listener listener) {
        super(context, R.layout.dialog_message_with_answer);

        intiUI(title, message, listener);
    }

    private void intiUI(String title, String message, final Listener listener) {
        (((TextView) findViewById(R.id.title))).setText(title);
        (((TextView) findViewById(R.id.noUsersLabel))).setText(message);

        findViewById(R.id.yesButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onYesButtonClicked(((EditText) findViewById(R.id.answer)).getText().toString().trim());
            }

            dismiss();
        });

        findViewById(R.id.noButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onNoButtonClicked();
            }

            dismiss();
        });
    }

    public interface Listener {
        void onNoButtonClicked();
        void onYesButtonClicked(String answer);
    }
}
