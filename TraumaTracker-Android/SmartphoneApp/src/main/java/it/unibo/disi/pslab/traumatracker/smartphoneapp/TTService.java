package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.os.Environment;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cartago.OPERATION;
import cartago.OpFeedbackParam;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.tools.NetworkManager;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.MultipartUtility;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class TTService extends JaCaArtifact {

    void init() { }

    @OPERATION
    public void sendReport(JSONObject report, OpFeedbackParam<Integer> result) {
        int res = -1;

        try {
            res = NetworkManager.doPOST(formatURL(C.url.POST_REPORT), report.toString());
            result.set(res);
        } catch (IOException e) {
            Log.e(C.LOG_TAG, "Unable to send report to TTService <" + e.getMessage() + ">");
            result.set(res);
        }
    }

    @OPERATION
    public void uploadReportMedia(JSONObject reportContent) {
        try {
            List<Pair<String, String>> photos = extractMediaFromReport(reportContent, C.report.events.type.PHOTO);
            List<Pair<String, String>> videos = extractMediaFromReport(reportContent, C.report.events.type.VIDEO);
            List<Pair<String, String>> vocals = extractMediaFromReport(reportContent, C.report.events.type.VOCAL_NOTE);

            log("Media within the Report [Photos: " + photos.size() + "; Videos: " + videos.size() + "; Vocals: " + vocals.size() + "]");

            for (Pair<String, String> photo : photos) {
                uploadSingleMedia(C.url.POST_PHOTOS, C.fs.PICTURES_FOLDER, photo, C.fs.PHOTO_FILE_EXTENSION);
            }

            for (Pair<String, String> video : videos) {
                uploadSingleMedia(C.url.POST_VIDEOS, C.fs.VIDEOS_FOLDER, video, C.fs.VIDEO_FILE_EXTENSION);
            }

            for (Pair<String, String> vocal : vocals) {
                uploadSingleMedia(C.url.POST_VOCALS, C.fs.VOCAL_NOTES_FOLDER, vocal, C.fs.VOCAL_FILE_EXTENSION);
            }

        } catch (JSONException e) {
            Log.e(C.LOG_TAG, "Unable to send media(s) to TTService <" + e.getMessage() + ">");
        }
    }

    @OPERATION
    public void requestUsersList(OpFeedbackParam<JSONArray> usersList) {
        try {
            final Pair<Integer, String> res = NetworkManager.doGET(formatURL(C.url.GET_USERS));

            if (res.first == 200) {
                JSONArray response = new JSONArray(res.second);
                usersList.set(response);
            } else {
                usersList.set(new JSONArray());
            }
        } catch (IOException | JSONException e) {
            usersList.set(new JSONArray());
        }
    }

    @OPERATION
    public void requestRoomsList(OpFeedbackParam<JSONArray> roomsList) {
        //TODO: al momento implementazione fake

        final String[] rooms = new String[]{
                C.place.TRANSPORT,
                C.place.SHOCK_ROOM,
                C.place.ER_CT,
                C.place.GENERAL_OPERATING_ROOM,
                C.place.NEUROSURGERY_OPERATING_ROOM,
                C.place.ORTOPEDICAL_OPERATIG_ROOM,
                C.place.ANGIOGRAPHIC_ROOM,
                C.place.ICU_1,
                C.place.ICU_2
        };

        JSONArray response = new JSONArray();

        try {
            for (int i = 0; i < rooms.length; i++) {
                response.put(new JSONObject()
                                .put("_id", "room-" + i)
                                .put("description", rooms[i]));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        roomsList.set(response);
    }

    private String formatURL(String url) {
        return C.url.HTTP_PROTOCOL_PREFIX
                + readSharedPreference(Preferences.ID, Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP)
                + ":"
                + C.url.GT2_TT_SERVICE_PORT
                + url;
    }

    private void uploadSingleMedia(final String url, final String folder, final Pair<String, String> file, final String extension) {
        String mutipartFieldName = "image";

        FileSystemUtils.renameFile(folder, file.first, file.second + extension);
        String filePath = Environment.getExternalStorageDirectory() + folder + "/" + file.second + extension;

        try {
            MultipartUtility multipart = new MultipartUtility(formatURL(url));
            multipart.addFilePart(mutipartFieldName, new File(filePath));
            multipart.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Pair<String, String>> extractMediaFromReport(JSONObject reportContent, String type) throws JSONException {
        List<Pair<String, String>> pairs = new ArrayList<>();

        String reportID = reportContent.getString(C.report.ID);
        JSONArray reportEvents = reportContent.getJSONArray(C.report.EVENTS);

        for (int i = 0; i < reportEvents.length(); i++) {
            JSONObject event = reportEvents.getJSONObject(i);

            if (event.getString(C.report.events.TYPE).equals(type)) {
                pairs.add(new Pair<>(event.getJSONObject(C.report.events.CONTENT).getString("description"),
                        reportID + "-" + event.getInt(C.report.events.ID)));
            }
        }

        return pairs;
    }
}