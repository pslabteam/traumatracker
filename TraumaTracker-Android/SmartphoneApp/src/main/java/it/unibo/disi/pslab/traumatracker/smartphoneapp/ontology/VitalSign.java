package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.LocalizationManager;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.LocalizationManager.Localization;

public enum VitalSign {
    //Vital Signs
    TEMPERATURE("temp", Temperature.values(), VitalSignUnit.CELSIUS),
    HEART_RATE("hr", HeartRate.values(), VitalSignUnit.BPM),
    BLOOD_PRESSURE("bp", BloodPressure.values(), VitalSignUnit.MMHG),
    SPO2("spo2", Spo2.values(), VitalSignUnit.PERCENTAGE),
    ETCO2("etco2", Etco2.values(), VitalSignUnit.MMHG),
    //Neurological Exam
    GCS_TOTAL("gcs-total", null),
    GCS_MOTOR("gcs-motor", GcsMotors.values()),
    GCS_VERBAL("gcs-verbal", GcsVerbal.values()),
    GCS_EYES("gcs-eyes", GcsEyes.values()),
    SEDATED("sedated", GenericNoYes.values()),
    PUPILS("pupils", Pupils.values()),
    //Airways and Breathing
    AIRWAYS("airways", Airways.values()),
    OXYGEN_PERCENTAGE("oxygen-percentage", null),
    POSITIVE_INHALATION("positive-inhalation", GenericNoYes.values()),
    INTUBATION_FAILED("intubation-failed", GenericNoYes.values()),
    CHEST_TUBE("chest-tube", PleuralDecompression.values()),
    //Bleedings
    HEMORRHAGE("hemorrhage", GenericNoYes.values()),
    //Orthopedic Injuries
    LIMBS_FRACTURE("limbs-fracture",GenericNoYes.values()),
    FRACTURE_EXPOSITION("fracture-exposition", GenericNoYes.values()),
    //Burn
    BURN("burn", Burn.values());

    private String id;
    private Enum<?>[] values;
    private VitalSignUnit unit;

    VitalSign(String id, Enum<?>[] values, VitalSignUnit unit) {
        this.id = id;
        this.values = values;
        this.unit = unit;
    }

    VitalSign(String id, Enum<?>[] values) {
        this(id, values,VitalSignUnit.EMPTY);
    }

    public String getId(){
        return id;
    }

    public Enum<?>[] getValues() {
        return values;
    }

    public VitalSignUnit getUnit() {
        return unit;
    }

    public List<String> getValuesList(){
        List<String> l = new ArrayList<>();

        for (Enum<?> value : values) {
            l.add(value.toString().toUpperCase());
        }

        return l;
    }

    private final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
        put(Localization.EN, new String[]{"Temperature", "Heart Rate", "Blood Pressure", "Saturation (SpO2)", "EtCO2",
                "GCS Total", "GCS Motor", "GCS Verbal", "GCS Eyes", "Sedated Patient", "Pupils",
                "Airways", "% O2", "Tracheo", "Intubation Failed", "Pleural Decompression",
                "External Bleeding",
                "Limbs Fracture", "Fracture Exposition",
                "Burn"});

        put(Localization.IT, new String[]{"Temperatura", "Frequenza Cardiaca", "Pressione Arteriosa", "Saturazione (SpO2)", "EtCO2",
                "GCS Totale", "GCS Motorio", "GCS Verbale", "GCS Occhi", "Paziente Sedato", "Pupille",
                "Vie Aeree", "% O2", "Tracheo", "IOT Fallita", "Decompressione Pleurica",
                "Emorragie Esterne",
                "Fratture Arti", "Frattura Esposta",
                "Ustione"});
    }};

    @NonNull
    public String toString() {
        return LocalizationManager.localizeElement(this, localizationMap);
    }

    enum Temperature {
        NORMOTHERMIC,
        HYPOTHERMIC,
        HYPERTHERMIC;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Normothermic", "Hypothermic", "Hyperthermic"});
            put(Localization.IT, new String[]{"Normotermico", "Ipotermico", "Ipertermico"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum HeartRate {
        NORMAL,
        BRADYCARDIC,
        TACHYCARDIC;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Normal", "Bradycardic", "Tachycardic"});
            put(Localization.IT, new String[]{"Normale", "Bradicardico", "Tachicardico"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum BloodPressure {
        NORMAL,
        HYPOTENSIVE,
        HYPERTENSIVE;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Normal", "Hypotensive", "Hypertensive"});
            put(Localization.IT, new String[]{"Normale", "Ipoteso", "Iperteso"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum Spo2 {
        NORMAL,
        HYPOXIC;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Normal", "Hypoxic"});
            put(Localization.IT, new String[]{"Normale", "Ipossico"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum Etco2 {
        NORMAL,
        HYPOCAPNIC,
        HYPERCAPNIC;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Normal", "Hypocapnic", "Hypercapnic"});
            put(Localization.IT, new String[]{"Normale", "Ipocapnico", "Ipercapnico"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum Airways {
        SPONTANEOUS_BREATHING,
        SUPRAGLOTTIC_PRESIDIUM,
        TRACHEAL_TUBE;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Spontaneous Breathing", "Supraglottic Presidium", "Tracheal Tube"});
            put(Localization.IT, new String[]{"Respiro Spontaneo", "Presidio Sovraglottico", "Tubo Tracheale"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum PleuralDecompression {
        NOT_PRESENT,
        RIGHT,
        LEFT,
        BILATERAL;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Not Present", "Right", "Left", "Bilateral"});
            put(Localization.IT, new String[]{"Non Presente", "Destra", "Sinistra", "Bilaterale"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum Pupils {
        NORMAL,
        ANISO,
        MIDRIASI;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Normal", "Aniso", "Midriasi"});
            put(Localization.IT, new String[]{"Normale", "Aniso", "Midriasi"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    public enum GcsMotors {
        GCS_MOTOR_6,
        GCS_MOTOR_5,
        GCS_MOTOR_4,
        GCS_MOTOR_3,
        GCS_MOTOR_2,
        GCS_MOTOR_1;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"(6) Esegue", "(5) Localizza", "(4) Retrae", "(3) Flessione", "(2) Estende", "(1) Nulla"});
            put(Localization.IT, new String[]{"(6) Esegue", "(5) Localizza", "(4) Retrae", "(3) Flessione", "(2) Estende", "(1) Nulla"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    public enum GcsVerbal {
        GCS_VERBAL_5,
        GCS_VERBAL_4,
        GCS_VERBAL_3,
        GCS_VERBAL_2,
        GCS_VERBAL_1;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"(5) Orienta", "(4) Confusa", "(3) Inappropriate", "(2) Incomprensibile", "(1) Nulla"});
            put(Localization.IT, new String[]{"(5) Orienta", "(4) Confusa", "(3) Inappropriate", "(2) Incomprensibile", "(1) Nulla"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    public enum GcsEyes {
        GCS_EYES_4,
        GCS_EYES_3,
        GCS_EYES_2,
        GCS_EYES_1;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"(4) Spontanea", "(3) Chiamata", "(2) Dolore", "(1) Nulla"});
            put(Localization.IT, new String[]{"(4) Spontanea", "(3) Chiamata", "(2) Dolore", "(1) Nulla"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum Burn {
        NOT_PRESENT,
        THERMAL,
        CHEMICAL;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"Not Present", "Thermal", "Chemical"});
            put(Localization.IT, new String[]{"Non Presente", "Termica", "Chimica"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum GenericYesNo {
        NO,
        YES;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"yes", "no"});
            put(Localization.IT, new String[]{"si", "no"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }

    enum GenericNoYes {
        NO,
        YES;

        private static final Map<Localization, String[]> localizationMap = new HashMap<Localization, String[]>() {{
            put(Localization.EN, new String[]{"no", "yes"});
            put(Localization.IT, new String[]{"no", "si"});
        }};

        @NonNull
        public String toString() {
            return LocalizationManager.localizeElement(this, localizationMap);
        }
    }
}
