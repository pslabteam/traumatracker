package it.unibo.disi.pslab.traumatracker.smartphoneapp;

//import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.exceptions.UserNotFoundException;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.TraumaInfoElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.PrehElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.StarterUiTabsAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TabsChangeListener;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerPager;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ArchiveWaitingDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.SettingsDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.StartTraumaDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.CompletedTraumasFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;

import it.unibo.pslab.jaca_android.core.ActivityArtifact;
import it.unibo.pslab.jaca_android.core.JaCaBaseActivity;

public class StarterUI extends ActivityArtifact {

    public static class StarterActivity extends JaCaBaseActivity {}

    public static StarterUI instance;

    private static final String START_TRACKER_OBS_PROP = "start_tracker";
    private static final String INIT_WARNING_AGENT_OBS_PROP = "init_warning_agent";
    private static final String INIT_BACKUP_AGENT_OBS_PROP = "init_backup_agent";

    private ArchiveWaitingDialog awd;
    private MultipleReportSendingSession mrss;

    private Menu optionsMenu;

    public void init() {
        super.init(StarterActivity.class, R.layout.activity_starter, R.menu.activity_starter_menu, true);

        instance = this;
    }

    @INTERNAL_OPERATION
    protected void setup() {
        initUI();

        keepScreenOn();

        bindOnCreateOptionsMenu("onCreateOptionsMenu");
        bindOnOptionsItemSelectedToOp("onOptionsItemSelected");

        signal(readSharedPreference(Preferences.ID, Preferences.GLASSES_SUPPORT, false) ? "activate_glasses": "deactivate_glasses");
    }

    private void initUI() {
        findUIElement(R.id.startButton).setOnClickListener(this::activateTraumaTeamButtonHandler);

        initTabs();
    }

    private void activateTraumaTeamButtonHandler(final View v){
        new StartTraumaDialog(getActivityContext(), (user, details) -> {

            try {
                ActiveTrauma.initialize(user, details);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            beginExternalSession();
            defineObsProperty(START_TRACKER_OBS_PROP, "");
            defineObsProperty(INIT_BACKUP_AGENT_OBS_PROP, "");
            //defineObsProperty(INIT_WARNING_AGENT_OBS_PROP);
            endExternalSession(true);

            ActiveTrauma.notifyTraumaActivation();

        }).show();
    }

    private void initTabs(){
        final int INITIAL_TAB_INDEX = 1;

        final View[] tabsRes = new View[]{
                findUIElement(R.id.tab_paused_traumas),
                findUIElement(R.id.tab_completed_traumas)
        };

        final TraumaTrackerPager tabs = (TraumaTrackerPager) findUIElement(R.id.tabs_container);

        execute(() -> {
            tabs.setAdapter(new StarterUiTabsAdapter(getWrappedActivity().getSupportFragmentManager()));

            tabs.addOnPageChangeListener(new TabsChangeListener() {
                @Override
                public void onPageSelected(final int position) {
                    tabs.changeCurrentPage(tabsRes, position);
                }
            });

            tabs.setCurrentItem(INITIAL_TAB_INDEX);
            tabs.changeCurrentPage(tabsRes, INITIAL_TAB_INDEX);

            for(int i = 0; i < tabsRes.length; i++){
                final int position = i;
                tabsRes[i].setOnClickListener(v -> tabs.setCurrentItem(position));
            }
        });


    }

    private String generateBleTagDescription(){

        final StringBuilder sb = new StringBuilder();

        final String bleTagId = readSharedPreference(Preferences.ID, Preferences.BLE_TAG_ID, C.url.DEFAULT_BLE_TAG_ID);

        sb.append(bleTagId);

        /*
        final String url = C.url.HTTP_PROTOCOL_PREFIX
                + readSharedPreference(Preferences.ID, Preferences.GT2_HOST_IP, C.url.DEFAULT_GT2_HOST_IP)
                + ":"
                + C.url.GT2_LOCATIONSERVICE_PORT
                + "/gt2/locationservice/api/tags/"
                + bleTagId;

        try {
            final Pair<Integer,String> response = NetworkManager.doGET(url);
            if(response.first.equals(HttpsURLConnection.HTTP_OK)){
                sb.append(" (").append(new JSONObject(response.second).getInt("battery")).append("%)");
            }
        } catch (IOException e) {
            Log.d(C.LOG_TAG, "Unable to retrieve tag battery level!");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        */

        return sb.toString();
    }

    @OPERATION
    public boolean onCreateOptionsMenu(Menu menu){
        optionsMenu = menu;

        if(menu != null){
            final String title = generateBleTagDescription();
            execute(() -> menu.findItem(R.id.activity_starter_menu_tag_ble).setTitle(title));
        }

        return true;
    }

    @OPERATION
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activity_starter_menu_tag_ble:
                execute(() -> new MessageDialog(getActivityContext(),
                        getStringFromResource(R.string.information),
                        "A questa istanza di TraumaTracker è associato il Beacon Tag n. "
                                + readSharedPreference(Preferences.ID, Preferences.BLE_TAG_ID, C.url.DEFAULT_BLE_TAG_ID)
                                + ". Questa opzione è modificabile nella sezione \"Impostazioni\"",
                        null).show());
                return true;

            case R.id.activity_starter_menu_settings:
                final String title = generateBleTagDescription();

                execute(() -> new SettingsDialog(getActivityContext(), new SettingsDialog.Listener() {
                    @Override
                    public void onSettingsSaved() {
                        if(optionsMenu != null){
                            optionsMenu.findItem(R.id.activity_starter_menu_tag_ble).setTitle(title);
                        }
                    }

                    @Override
                    public void onRequestUserListButtonClicked() {
                        externalSignal("downloadUsersList");
                    }

                    @Override
                    public void onRequestRoomsListButtonClicked() {
                        externalSignal("downloadRoomsList");
                    }

                    @Override
                    public void onGlassesSupportStatusChanged(boolean supportActive) {
                        externalSignal(supportActive ? "activate_glasses" : "deactivate_glasses");
                    }
                }).show());

                return true;
        }

        return false;
    }

    @OPERATION
    public void onUsersListAvailable(JSONArray usersList) {
        final String message = usersList.length() != 0
                ? getStringFromResource(R.string.users_list_dialog_message_ok)
                : getStringFromResource(R.string.users_list_dialog_message_no);

        execute(() -> new MessageDialog(getActivityContext(), getStringFromResource(R.string.users_list_dialog_title), message, null).show());
    }

    @OPERATION
    public void onRoomsListAvailable(JSONArray roomsList) {
        final String message = roomsList.length() != 0
                ? getStringFromResource(R.string.rooms_list_dialog_message_ok)
                : getStringFromResource(R.string.rooms_list_dialog_message_no);

        execute(() -> new MessageDialog(getActivityContext(), getStringFromResource(R.string.rooms_list_dialog_title), message, null).show());
    }

    @OPERATION
    public void onReportSent(final String reportId, final int res) {
        if(res == 200){
            execute(() -> awd.changeMessage(getStringFromResource(R.string.report_ongoing_sync_msg_part_1)
                    + mrss.N_REPORTS_SYNC
                    + getStringFromResource(R.string.report_ongoing_sync_msg_part_2)
                    + mrss.N_REPORTS_TO_SYNC
                    + getStringFromResource(R.string.report_ongoing_sync_msg_part_3)));

            FileSystemUtils.moveFile(reportId + C.fs.REPORT_FILE_EXTENSION, C.fs.REPORTS_TOSYNC_FOLDER, C.fs.REPORTS_SYNC_FOLDER);
            mrss.registerNewFeedback(true);
        } else {
            mrss.registerNewFeedback(false);
        }

        if(mrss.synchronizationCompleted()){
            final String exitMessage = mrss.synchronizationCompletedSuccessfully()
                    ? getStringFromResource(R.string.report_synced_msg_part_1)
                        + mrss.N_REPORTS_SYNC
                        + getStringFromResource(R.string.report_synced_msg_part_2)
                        + mrss.N_REPORTS_TO_SYNC
                        + getStringFromResource(R.string.report_synced_msg_part_3)
                    : getStringFromResource(R.string.report_sync_error_msg_part_1)
                        + mrss.N_ERRORS
                        + getStringFromResource(R.string.report_sync_error_msg_part_2);

            execute(() -> {
                awd.changeMessage(exitMessage);
                awd.toggleButtonVisibility(true);
            });
        }
    }

    public void manageReportSyncRequest(final String id, CompletedTraumasFragment.Events el){
        manageReportsSyncRequest(Collections.singletonList(id), el);
    }

    public void manageReportsSyncRequest(final List<String> ids, CompletedTraumasFragment.Events el){
        mrss = new MultipleReportSendingSession(ids.size());

        awd = new ArchiveWaitingDialog(StarterUI.this.getActivityContext(),
                StarterUI.this.getStringFromResource(R.string.report_sync_message_title),
                StarterUI.this.getStringFromResource(R.string.report_sync_message_part_1)
                        + " (" + mrss.N_REPORTS_SYNC
                        + " " + StarterUI.this.getStringFromResource(R.string.report_sync_message_part_2) + " "
                        + mrss.N_REPORTS_TO_SYNC + ")",
                () -> {
                    if(el != null && mrss.synchronizationCompletedSuccessfully()) { el.onReportSyncCompleted(); }
                });
        awd.toggleButtonVisibility(false);
        awd.show();

        for (final String id : ids) {
            try {
                externalSignal("uploadReport", id, new JSONObject(FileSystemUtils.loadFile(C.fs.REPORTS_TOSYNC_FOLDER, id + C.fs.REPORT_FILE_EXTENSION)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void restartReportRequest(final String reportId){
        final String reportContent = FileSystemUtils.loadFile(C.fs.TEMP_FOLDER, reportId + C.fs.REPORT_FILE_EXTENSION);

        if(reportContent.isEmpty()){
            return;
        }


        final Report restoredReport = Report.build(reportContent, false);

        if(restoredReport != null) {
            final String lastUserId = findLastTraumaLeaderId(reportContent);

            final User traumaLeader;

            try {
                traumaLeader = lastUserId.isEmpty() ? User.getById(restoredReport.operator()) : User.getById(lastUserId);
            } catch (UserNotFoundException e) {
                e.printStackTrace();
                return;
            }

            ActiveTrauma.initialize(traumaLeader, restoredReport.delayedActivation(),
                    restoredReport.delayedActivationDate(),
                    restoredReport.delayedActivationTime(),
                    checkPatientAccessed(reportContent),
                    restoredReport.traumaTeam(),
                    createVitalSignsMap(restoredReport.initialVitalSigns()),
                    restoredReport.traumaInfo().mtc(),
                    restoredReport.traumaInfo().anamnesi(),
                    createPatientInfoMap(restoredReport.traumaInfo()),
                    createPrehMap(restoredReport.preh())
            );

            //TODO: init time dependend and room -> agent?

            beginExternalSession();
            defineObsProperty(START_TRACKER_OBS_PROP, reportId);
            defineObsProperty(INIT_BACKUP_AGENT_OBS_PROP, reportId);
            endExternalSession(true);
        }
    }

    private Map<VitalSign, String> createVitalSignsMap(Report.InitialVitalSigns ivs){
        return new HashMap<VitalSign, String>(){{
            put(VitalSign.TEMPERATURE, ivs.temperature());
            put(VitalSign.HEART_RATE, ivs.heartRate());
            put(VitalSign.BLOOD_PRESSURE, ivs.bloodPressure());
            put(VitalSign.SPO2, ivs.spo2());
            put(VitalSign.ETCO2, ivs.etco2());

            put(VitalSign.HEMORRHAGE, ivs.externalBleedings());

            put(VitalSign.AIRWAYS, ivs.airways());
            put(VitalSign.OXYGEN_PERCENTAGE, ivs.oxygenPercentage());
            put(VitalSign.POSITIVE_INHALATION, ivs.tracheo());
            put(VitalSign.INTUBATION_FAILED, ivs.failedIntubation());
            put(VitalSign.CHEST_TUBE, ivs.pleuralDecompression());

            put(VitalSign.GCS_TOTAL, ivs.gcsTotal());
            put(VitalSign.GCS_MOTOR, ivs.gcsMotor());
            put(VitalSign.GCS_VERBAL, ivs.gcsVerbal());
            put(VitalSign.GCS_EYES, ivs.gcsEyes());
            put(VitalSign.SEDATED, ivs.sedated());
            put(VitalSign.PUPILS, ivs.pupils());

            put(VitalSign.LIMBS_FRACTURE, ivs.limbs_fracture());
            put(VitalSign.FRACTURE_EXPOSITION, ivs.exposed_fracture());

            put(VitalSign.BURN, ivs.burn());
        }};
    }

    private Map<TraumaInfoElement, String> createPatientInfoMap(Report.TraumaInfo pi){
        return new HashMap<TraumaInfoElement, String>(){{
            put(TraumaInfoElement.CODE, pi.code());
            put(TraumaInfoElement.GENDER, pi.gender());
            put(TraumaInfoElement.AGE, pi.age());
            put(TraumaInfoElement.ACCIDENT_DATE, pi.accidentDate());
            put(TraumaInfoElement.ACCIDENT_TIME, pi.accidentTime());
            put(TraumaInfoElement.VEHICLE, pi.vehicle());
            put(TraumaInfoElement.FROM_OTHER_EMERGENCY, pi.fromOtherEmergency());
            put(TraumaInfoElement.OTHER_EMERGENCY, pi.otherEmergency());
        }};
    }

    private Map<PrehElement, String> createPrehMap(Report.Preh preh){
        return new HashMap<PrehElement, String>(){{
            put(PrehElement.A, preh.a());
            put(PrehElement.B_PLEURAL_DECOMPRESSION, preh.bPleuralDecompression());
            put(PrehElement.C_BLOOD_PROTOCOL, preh.cBloodProtocol());
            put(PrehElement.C_TPOD, preh.cTpod());
            put(PrehElement.D_GCS_TOTAL, preh.dGcsTot());
            put(PrehElement.D_ANISOCORIA, preh.dAnisocoria());
            put(PrehElement.D_MIDRIASI, preh.dMidriasi());
            put(PrehElement.E_PARA_TETRA_PARESI, preh.eParaTetraParesi());
        }};
    }

    private String findLastTraumaLeaderId(String reportContent){
        String id = "";

        try {
            JSONArray eventsArray = new JSONObject(reportContent).getJSONArray(C.report.EVENTS);

            for(int i = 0; i < eventsArray.length(); i++){
                final JSONObject event = eventsArray.getJSONObject(i);

                if(event.getString(C.report.events.TYPE).equals(C.report.events.type.TRAUMA_LEADER)){
                    id = event.getJSONObject(C.report.events.CONTENT).getString(C.report.events.traumaLeader.ID);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return id;
    }

    private boolean checkPatientAccessed(String reportContent){
        try {
            JSONArray eventsArray = new JSONObject(reportContent).getJSONArray(C.report.EVENTS);

            for(int i = 0; i < eventsArray.length(); i++){
                final JSONObject event = eventsArray.getJSONObject(i);

                if(event.getString(C.report.events.TYPE).equals(C.report.events.type.PATIENT_ACCEPTED)){
                    return true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void externalSignal(String type, Object... objects){
        beginExternalSession();
        signal(type, objects);
        endExternalSession(true);
    }

    /**
     *
     */
    private class MultipleReportSendingSession {
        private int N_REPORTS_TO_SYNC, N_REPORTS_SYNC, N_ERRORS, N_RECEIVED_FEEDBACKS;

        MultipleReportSendingSession(final int nReportsToSync){
            N_REPORTS_TO_SYNC = nReportsToSync;
            N_REPORTS_SYNC = 0;
            N_ERRORS = 0;
            N_RECEIVED_FEEDBACKS = 0;
        }

        boolean synchronizationCompleted(){
            return N_RECEIVED_FEEDBACKS == N_REPORTS_TO_SYNC;
        }

        boolean synchronizationCompletedSuccessfully(){
            return N_REPORTS_SYNC == N_REPORTS_TO_SYNC;
        }

        void registerNewFeedback(final boolean sync){
            N_RECEIVED_FEEDBACKS++;

            if(sync){
                N_REPORTS_SYNC++;
            } else {
                N_ERRORS++;
            }
        }
    }
}
