package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum Procedure {
    //--- A ---
    INTUBATION("intubation", R.string.ontology_procedure_intubation, ProcedureCategory.A, false),
    SUPRAGLOTTIC_PRESIDIUM("supraglottic-presidium", R.string.ontology_procedure_supraglotticpresidium, ProcedureCategory.A, false),
    FIBROSCOPY("fibroscopy", R.string.ontology_procedure_fibroscopy, ProcedureCategory.A, false),
    TRACHEOSTOMY("tracheostomy", R.string.ontology_procedure_trachestomy, ProcedureCategory.A, false),
    //--- B ---
    DRAINAGE("drainage", R.string.ontology_procedure_drainage, ProcedureCategory.B, false),
    CHEST_TUBE("chest-tube", R.string.ontology_procedure_chesttube, ProcedureCategory.B, false),
    //--- c ---
    INFUSER("infuser", R.string.ontology_procedure_infuser, ProcedureCategory.C, false),
    INTRAOSSEOUS("intraosseous", R.string.ontology_procedure_intraosseous, ProcedureCategory.C, false),
    ARTERIAL_CATHETER("arterial-catheter", R.string.ontology_procedure_arterialcatheter, ProcedureCategory.C, false),
    PELVIC_BINDER("pelvic-binder", R.string.ontology_procedure_pelvicbinder, ProcedureCategory.C, false),
    PALVIC_PACKING("pelvic-packing", R.string.ontology_procedure_pelvicpacking, ProcedureCategory.C, false),
    EXTERNAL_FIXATOR("fixator", R.string.ontology_procedure_fixator, ProcedureCategory.C, false),
    TOURNIQUET("tourniquet", R.string.ontology_procedure_tourniquet, ProcedureCategory.C, true),
    REBOA("reboa", R.string.ontology_procedure_reboa, ProcedureCategory.C, true),
    THORACOTOMY("thoracotomy", R.string.ontology_procedure_thoracotomy, ProcedureCategory.C, true),
    //--- Others ---
    GASTRIC_PROBE("gastric-probe", R.string.ontology_procedure_gastric_probe, ProcedureCategory.OTHERS, false),
    BLADDER_FOLEY("bladder-foley", R.string.ontology_procedure_bladderfoley, ProcedureCategory.OTHERS, false),
    ALS("als", R.string.ontology_procedure_als, ProcedureCategory.ALS, true);

    private final String id;
    private final int labelRes;
    private final boolean timeDependency;
    private final ProcedureCategory category;

    Procedure(final String id, final int labelRes, final ProcedureCategory category, final boolean timeDependency){
        this.id = id;
        this.labelRes = labelRes;
        this.category = category;
        this.timeDependency = timeDependency;
    }

    public String getId(){
        return id;
    }

    public boolean isTimeDependent(){
        return timeDependency;
    }

    public ProcedureCategory getCategory() {
        return category;
    }
    
    public String toMultilineString(){
        return App.getAppResources().getString(labelRes);
    }

    public String toString() {
        return App.getAppResources().getString(labelRes).replace("\n", " ");
    }

    public static Procedure getById(String id){
        for(Procedure p : Procedure.values()){
            if(p.getId().equals(id)){
                return  p;
            }
        }

        return null;
    }
}
