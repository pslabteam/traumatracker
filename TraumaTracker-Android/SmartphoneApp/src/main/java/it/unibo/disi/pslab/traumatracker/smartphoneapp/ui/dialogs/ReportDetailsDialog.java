package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.AnamnesiElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.MajorTraumaCriteriaElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class ReportDetailsDialog extends TTDialog {

    private static final String EMPTY_DATA = "--";

    public ReportDetailsDialog(Context context, Report report) {
        super(context, R.layout.dialog_report_details);

        intiUI(report);
    }

    private void intiUI(Report report) {
        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());

        ((TextView) findViewById(R.id.title)).setText(report.id());

        fillReportGeneralInfo(report);
        fillReportPreh(report.preh());
        fillPatientInitialVitalSigns(report.initialVitalSigns());
        fillEventsTrack(report.eventTrack());
    }

    private void fillReportGeneralInfo(final Report report){

        /* --- Trauma Team ---*/
        final User operator = FileSystem.getUserById(report.operator());

        ((TextView) findViewById(R.id.operator)).setText(operator != null
                ? String.format("%s %s", operator.name(), operator.surname())
                : EMPTY_DATA);

        ((TextView) findViewById(R.id.traumaTeam)).setText(produceTraumaTeamDescription(report.traumaTeam()));

        /* --- Intervento ---*/

        ((TextView) findViewById(R.id.delayedActivationCheckBox)).setText(report.delayedActivation()
                ? String.format("%s (originaria: %s %s)", convertYesNo(report.delayedActivation()), report.delayedActivationDate(), report.delayedActivationTime())
                : String.format("%s", convertYesNo(report.delayedActivation())));

        ((TextView) findViewById(R.id.reportStartDate)).setText(String.format("%s (%s)", report.startDate(), report.startTime()));
        ((TextView) findViewById(R.id.reportEndDate)).setText(String.format("%s (%s)", report.endDate(), report.endTime()));
        ((TextView) findViewById(R.id.reportIss)).setText(String.format("%s %s", report.iss(), report.issIsPresumed() ? "(presunto)" : ""));
        ((TextView) findViewById(R.id.reportDestination)).setText(report.finalDestination());

        /* --- Informazioni Paziente ---*/

        final Report.TraumaInfo pInfo = report.traumaInfo();

        ((TextView) findViewById(R.id.patientCode)).setText(pInfo.code().isEmpty()
                ? EMPTY_DATA : String.format("%s", pInfo.code()));
        ((TextView) findViewById(R.id.patientSdo)).setText(pInfo.sdo().isEmpty()
                ? EMPTY_DATA : String.format("%s", pInfo.sdo()));
        ((TextView) findViewById(R.id.patientAdmissionCode)).setText(pInfo.admissionCode().isEmpty()
                ? EMPTY_DATA : String.format("%s", pInfo.admissionCode()));
        ((TextView) findViewById(R.id.patientName)).setText(pInfo.name().isEmpty() && pInfo.surname().isEmpty()
                ? EMPTY_DATA : String.format("%s %s", pInfo.name(), pInfo.surname()));
        ((TextView) findViewById(R.id.patientGender)).setText(pInfo.gender().isEmpty()
                ? EMPTY_DATA : String.format("%s", pInfo.gender()));
        ((TextView) findViewById(R.id.patientAge)).setText(pInfo.age().isEmpty()
                ? EMPTY_DATA : String.format("%s", pInfo.age()));
        ((TextView) findViewById(R.id.patientDob)).setText(pInfo.dob().isEmpty()
                ? EMPTY_DATA : String.format("%s", pInfo.dob()));
        ((TextView) findViewById(R.id.patientAnamnesi)).setText(produceAnamnesiDescription(pInfo.anamnesi()));
        ((TextView) findViewById(R.id.patientAccidentDateTime)).setText(pInfo.accidentDate().isEmpty() && pInfo.accidentTime().isEmpty()
                ? EMPTY_DATA : String.format("%s %s", pInfo.accidentDate(), pInfo.accidentTime()));
        ((TextView) findViewById(R.id.patientVehicle)).setText(pInfo.vehicle().isEmpty()
                ? EMPTY_DATA : String.format("%s", pInfo.vehicle()));
        ((TextView) findViewById(R.id.patientFromOtherEmergency)).setText(
                String.format("%s %s", convertYesNo(Boolean.parseBoolean(pInfo.fromOtherEmergency())), pInfo.otherEmergency().isEmpty() ? "" : "(" + pInfo.otherEmergency() + ")"));
        ((TextView) findViewById(R.id.patientMtc)).setText(produceMajorTraumaCriteriaDescription(pInfo.mtc()));
    }

    private void fillReportPreh(final Report.Preh preh){
        ((TextView) findViewById(R.id.preh_territorialArea_value)).setText(preh.territorialArea());
        ((TextView) findViewById(R.id.preh_carAccident_value)).setText(convertYesNo(Boolean.parseBoolean(preh.carAccident())));
        ((TextView) findViewById(R.id.preh_a_value_1)).setText(preh.a());
        ((TextView) findViewById(R.id.preh_b_value_1)).setText(convertYesNo(Boolean.parseBoolean(preh.bPleuralDecompression())));
        ((TextView) findViewById(R.id.preh_c_value_1)).setText(convertYesNo(Boolean.parseBoolean(preh.cBloodProtocol())));
        ((TextView) findViewById(R.id.preh_c_value_2)).setText(convertYesNo(Boolean.parseBoolean(preh.cTpod())));
        ((TextView) findViewById(R.id.preh_d_value_1)).setText(preh.dGcsTot());
        ((TextView) findViewById(R.id.preh_d_value_2)).setText(convertYesNo(Boolean.parseBoolean(preh.dAnisocoria())));
        ((TextView) findViewById(R.id.preh_d_value_3)).setText(convertYesNo(Boolean.parseBoolean(preh.dMidriasi())));
        ((TextView) findViewById(R.id.preh_e_value_1)).setText(convertYesNo(Boolean.parseBoolean(preh.eParaTetraParesi())));
        ((TextView) findViewById(R.id.preh_wrostPAS_value)).setText(preh.wrostBloodPressure());
        ((TextView) findViewById(R.id.preh_wrostRR_value)).setText(preh.wrostRespiratoryRate());
    }

    private void fillPatientInitialVitalSigns(final Report.InitialVitalSigns initialVitalSigns){
        /*
         * Vital Signs
         */
        ((TextView) findViewById(R.id.vs_temperature)).setText(String.format("%s",
                initialVitalSigns.temperature().isEmpty() ? EMPTY_DATA : initialVitalSigns.temperature()));
        ((TextView) findViewById(R.id.vs_heart_rate)).setText(String.format("%s",
                initialVitalSigns.heartRate().isEmpty() ? EMPTY_DATA : initialVitalSigns.heartRate()));
        ((TextView) findViewById(R.id.vs_blood_pressure)).setText(String.format("%s",
                initialVitalSigns.bloodPressure().isEmpty() ? EMPTY_DATA : initialVitalSigns.bloodPressure()));
        ((TextView) findViewById(R.id.vs_spo2)).setText(String.format("%s",
                initialVitalSigns.spo2().isEmpty() ? EMPTY_DATA : initialVitalSigns.spo2()));
        ((TextView) findViewById(R.id.vs_etco2)).setText(String.format("%s",
                initialVitalSigns.etco2().isEmpty() ? EMPTY_DATA : initialVitalSigns.etco2()));

        /*
         * Neurological Exam
         */
        ((TextView) findViewById(R.id.vs_gcs)).setText(String.format("%s%s%s%s%s",
                (initialVitalSigns.gcsTotal().isEmpty()) ? "" : (initialVitalSigns.gcsTotal().equals("0") ? EMPTY_DATA : initialVitalSigns.gcsTotal()) + "\n",
                (initialVitalSigns.gcsMotor().isEmpty()) ? "" : " - " + getString(R.string.vs_gcs_motors_title) + " : " + initialVitalSigns.gcsMotor() + "\n",
                (initialVitalSigns.gcsVerbal().isEmpty()) ? "" : " - " + getString(R.string.vs_gcs_verbal_title) + " : " + initialVitalSigns.gcsVerbal() + "\n",
                (initialVitalSigns.gcsEyes().isEmpty()) ? "" : " - " + getString(R.string.vs_gcs_eyes_title) + " : " + initialVitalSigns.gcsEyes() + "\n",
                (initialVitalSigns.sedated().isEmpty()) ? "" : " - " + getString(R.string.vs_sedated_patient_title) + " : " + convertYesNo(Boolean.parseBoolean(initialVitalSigns.sedated()))));

        ((TextView) findViewById(R.id.vs_pupils)).setText(initialVitalSigns.pupils().isEmpty()
                ? EMPTY_DATA : initialVitalSigns.pupils());

        /*
         * Airways and Breathing
         */
        ((TextView) findViewById(R.id.vs_airways)).setText(initialVitalSigns.airways().isEmpty()
                ? EMPTY_DATA : initialVitalSigns.airways());
        ((TextView) findViewById(R.id.vs_oxygenPercentage)).setText(initialVitalSigns.oxygenPercentage().isEmpty()
                ? EMPTY_DATA : initialVitalSigns.oxygenPercentage());
        ((TextView) findViewById(R.id.vs_tracheo)).setText(convertYesNo(Boolean.parseBoolean(initialVitalSigns.tracheo())));
        ((TextView) findViewById(R.id.vs_failed_iot)).setText(convertYesNo(Boolean.parseBoolean(initialVitalSigns.failedIntubation())));
        ((TextView) findViewById(R.id.vs_pleural_decompression)).setText(initialVitalSigns.pleuralDecompression().isEmpty()
                ? EMPTY_DATA : initialVitalSigns.pleuralDecompression());

        /*
         * Hemorrage and Burn
         */

        ((TextView) findViewById(R.id.vs_external_bleedings)).setText(convertYesNo(Boolean.parseBoolean(initialVitalSigns.externalBleedings())));
        ((TextView) findViewById(R.id.vs_burn)).setText(initialVitalSigns.burn().isEmpty()
                ? EMPTY_DATA : initialVitalSigns.burn());

        /*
         * Fractures
         */
        ((TextView) findViewById(R.id.vs_fractures)).setText(convertYesNo(Boolean.parseBoolean(initialVitalSigns.limbs_fracture())));
        ((TextView) findViewById(R.id.vs_exposed_fracture)).setText(convertYesNo(Boolean.parseBoolean(initialVitalSigns.exposed_fracture())));

        hideEmptyRows();
    }

    private String convertYesNo(final boolean value){
        return value ? App.getAppResources().getString(R.string.switch_yes_label).toLowerCase() : App.getAppResources().getString(R.string.switch_no_label).toLowerCase();
    }

    private String produceTraumaTeamDescription(final List<String> ttList) {
        final StringBuilder ttMembers = new StringBuilder();

        for(final String member : ttList){
            ttMembers.append(member).append(", ");
        }

        return ttMembers.length() == 0
                ? EMPTY_DATA
                : ttMembers.deleteCharAt(ttMembers.length() - 1).deleteCharAt(ttMembers.length() - 1).toString();
    }

    private String produceAnamnesiDescription(final Map<AnamnesiElement, Boolean> anamnesiMap){
        final String[] anamnesiDescriptionParts = new String[]{
                " - " + AnamnesiElement.ANTIPLATELETS.toString() + ": " + convertYesNo(anamnesiMap.get(AnamnesiElement.ANTIPLATELETS)),
                " - " + AnamnesiElement.ANTICOAGULANTS.toString() + ": " + convertYesNo(anamnesiMap.get(AnamnesiElement.ANTICOAGULANTS)),
                " - " + AnamnesiElement.NAO.toString() + ": " + convertYesNo(anamnesiMap.get(AnamnesiElement.NAO)),
        };

        final StringBuilder anamnesi = new StringBuilder();

        for (final String anamnesiDescriptionPart : anamnesiDescriptionParts) {
            if (!anamnesi.toString().isEmpty()) {
                anamnesi.append("\n");
            }

            if (!anamnesiDescriptionPart.isEmpty()) {
                anamnesi.append(anamnesiDescriptionPart);
            }
        }

        return anamnesi.length() == 0 ? EMPTY_DATA : anamnesi.toString();
    }

    private String produceMajorTraumaCriteriaDescription(Map<MajorTraumaCriteriaElement, Boolean> mtcMap){
        final String[] mtcDescriptionParts = new String[]{
                " - " + MajorTraumaCriteriaElement.DYNAMIC.toString() + ": " + convertYesNo(mtcMap.get(MajorTraumaCriteriaElement.DYNAMIC)),
                " - " + MajorTraumaCriteriaElement.PHYSIOLOGICAL.toString() + ": " + convertYesNo(mtcMap.get(MajorTraumaCriteriaElement.PHYSIOLOGICAL)),
                " - " + MajorTraumaCriteriaElement.ANATOMICAL.toString() + ": " + convertYesNo(mtcMap.get(MajorTraumaCriteriaElement.ANATOMICAL))
        };

        final StringBuilder mtc = new StringBuilder();

        for (final String mtcDescriptionPart : mtcDescriptionParts) {
            if (!mtc.toString().isEmpty()) {
                mtc.append("\n");
            }

            if (!mtcDescriptionPart.isEmpty()) {
                mtc.append(mtcDescriptionPart);
            }
        }

        return mtc.length() == 0 ? EMPTY_DATA : mtc.toString();
    }

    private void hideEmptyRows(){
        Map<Integer, int[]> groups = new HashMap<Integer, int[]>(){{
            put(R.id.title_vs, new int[]{R.id.vs_temperature, R.id.vs_heart_rate, R.id.vs_blood_pressure, R.id.vs_spo2, R.id.vs_etco2});
            put(R.id.title_neurological, new int[]{R.id.vs_gcs, R.id.vs_pupils});
            put(R.id.title_airways, new int[]{R.id.vs_airways, R.id.vs_oxygenPercentage, R.id.vs_tracheo, R.id.vs_failed_iot, R.id.vs_pleural_decompression});
            put(R.id.title_hemorrhage, new int[]{R.id.vs_external_bleedings, R.id.vs_burn});
        }};

        for(int i = 0; i < groups.size(); i++){
            int groupTitleRes = (Integer) Objects.requireNonNull(groups.keySet().toArray())[i];
            int cnt = 0;

            for (int e : Objects.requireNonNull(groups.get(groupTitleRes))) {
                if (((TextView) findViewById(e)).getText().equals("")) {
                    ((View) findViewById(e).getParent()).setVisibility(View.GONE);
                    cnt++;
                }
            }

            if(cnt == Objects.requireNonNull(groups.get(groupTitleRes)).length){
                findViewById(groupTitleRes).setVisibility(View.GONE);
            }
        }
    }

    private void fillEventsTrack(final Report.EventsTrack eventsTrack){
        final TableRow.LayoutParams titleRowLayout = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f);
        final TableRow.LayoutParams eventRowLayout = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1.0f);
        final TableLayout.LayoutParams rowLayout = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);

        if (eventsTrack.isEmpty()) {
            final TableRow row = new TableRow(getContext());
            row.setLayoutParams(titleRowLayout);
            row.addView(createLabelForRow("--", 1));
            row.addView(createLabelForRow("--", 2));
            row.addView(createLabelForRow("--", 4));

            ((TableLayout) findViewById(R.id.events_table)).addView(row, rowLayout);

            return;
        }

        final List<Report.Event> events = new ArrayList<>(eventsTrack.getEvents());

        Collections.sort(events, (e1, e2) -> (e1.date() + " " + e1.time()).compareTo(e2.date() + " " + e2.time()));

        for (int i = 0; i < events.size(); i++) {
            final TableRow row = new TableRow(getContext());
            row.setLayoutParams(eventRowLayout);
            row.setBackgroundColor(getResources().getColor(i % 2 == 0 ? android.R.color.transparent : R.color.tt_light_grey));

            final Report.Event event = events.get(i);

            row.addView(createLabelForRow(event.date() + "\n" +event.time(), 1));
            row.addView(createLabelForRow(event.place(), 2));
            row.addView(createLabelForRow(event.description(), 4));

            ((TableLayout) findViewById(R.id.events_table)).addView(row, rowLayout);
        }
    }

    private TextView createLabelForRow(String text, int weight) {
        final TextView txt = new TextView(getContext());
        txt.setText(text);
        txt.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, weight));
        txt.setPadding(2, 5, 2, 5);
        txt.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);

        return txt;
    }
}
