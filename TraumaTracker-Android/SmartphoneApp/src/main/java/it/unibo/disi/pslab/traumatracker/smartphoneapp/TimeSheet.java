package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import java.util.ArrayList;
import java.util.List;

import cartago.OPERATION;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Room;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TimeDependentProcedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class TimeSheet extends JaCaArtifact {

    private Room currentRoom;

    private List<TimeDependentProcedure> tdProceduresList;

    private List<Listener> listeners;

    private static TimeSheet me;

    private static final int TIMER_TICK = 1000;

    public static TimeSheet reference() {
        return me;
    }

    public void init() {
        me = this;

        new Timer(TIMER_TICK).start();

        listeners = new ArrayList<>();

        currentRoom = new Room();

        tdProceduresList = new ArrayList<>();

        for (Procedure p : Procedure.values()) {
            if (p.isTimeDependent()) {
                tdProceduresList.add(new TimeDependentProcedure(p, TimeDependentProcedure.Status.STOPPED));
            }
        }
    }

    public void registerListener(Listener listener) {
        this.listeners.add(listener);

        runOnMainLooper(() -> {
            for (Listener l : listeners) {
                l.updateRoomInfo(currentRoom);

                for (TimeDependentProcedure tdp : tdProceduresList) {
                    l.updateTimeDependentProcedureInfo(tdp);
                }
            }
        });
    }

    public void unregisterListener(Listener listener) {
        listeners.remove(listener);
    }

    public Room getCurrentRoom(){
        return currentRoom;
    }

    @OPERATION
    public void dismissTimeSheet(){
        currentRoom = new Room();

        tdProceduresList = new ArrayList<>();

        for (Procedure p : Procedure.values()) {
            if (p.isTimeDependent()) {
                tdProceduresList.add(new TimeDependentProcedure(p, TimeDependentProcedure.Status.STOPPED));
            }
        }
    }

    @OPERATION
    public void notifyRoomChangeToTimeSheet(String roomDescription) {
        if (roomDescription.equals(""))
            return;

        currentRoom.init(roomDescription, DateTimeUtils.getCurrentDate(), DateTimeUtils.getCurrentTime());

        runOnMainLooper(() -> {
            for (Listener l : listeners) {
                l.updateRoomInfo(currentRoom);
            }
        });
    }

    @OPERATION
    public void notifyTimeDependentProcedureNewState(Procedure p, String event) {

        TimeDependentProcedure currentTdp = null;

        for (TimeDependentProcedure tdp : tdProceduresList) {
            if (tdp.procedure().equals(p)) {
                currentTdp = tdp;
            }
        }

        if(currentTdp == null)
            return;

        if (event.equals(C.report.events.procedure.event.START)){
            currentTdp.startDate(DateTimeUtils.getCurrentDate());
            currentTdp.startTime(DateTimeUtils.getCurrentTime());
            currentTdp.duration(0);
            currentTdp.status(TimeDependentProcedure.Status.STARTED);
        }

        if(event.equals(C.report.events.procedure.event.END)){
            currentTdp.status(TimeDependentProcedure.Status.STOPPED);
        }

        final TimeDependentProcedure tdp = currentTdp;

        runOnMainLooper(() -> {
            for (Listener l : listeners) {
                l.updateTimeDependentProcedureInfo(tdp);
            }
        });
    }

    /*
    @OPERATION
    public void notifyVitalSignsValues(final int sys, final int dia, final int hr, final int etco2, final int spo2, final int temp){
        runOnMainLooper(() -> {
            for (Listener l : listeners) {
                l.updateVitalSigns(sys, dia, hr, etco2, spo2, temp);
            }
        });

        ActiveTrauma.registerVitalSignsFromMonitorNote(sys, dia, hr, etco2, spo2, temp);
    }

    @OPERATION
    public void notifyMonitorUnreachableAlert(){
        runOnMainLooper(() -> {
            for (Listener l : listeners) {
                l.notifyMonitorUnreachableAlert();
            }
        });
    }
    */

    /**
     *
     */
    public interface Listener {
        void updateRoomInfo(Room room);
        //void updateVitalSigns(final int sys, final int dia, final int hr, final int etco2, final int spo2, final int temp);
        //void notifyMonitorUnreachableAlert();
        void updateTimeDependentProcedureInfo(TimeDependentProcedure tdp);
    }

    /**
     *
     */
    private class Timer extends Thread {

        private final int interval;
        private boolean stop = false;

        Timer(int interval) {
            this.interval = interval;
        }

        @Override
        public void run() {
            while (!stop) {
                try {
                    sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (getActivity("mainUI") == null) {
                    stop = true;
                    return;
                }

                if (currentRoom != null && currentRoom.isInitialized()) {
                    currentRoom.updatePermanenceTime();

                    runOnMainLooper(() -> {
                        for (Listener l : listeners) {
                            l.updateRoomInfo(currentRoom);
                        }
                    });
                }

                if(tdProceduresList != null){
                    for(final TimeDependentProcedure tdp : tdProceduresList){
                        if(tdp.status().equals(TimeDependentProcedure.Status.STARTED)){
                            tdp.duration(tdp.duration() + 1);

                            runOnMainLooper(() -> {
                                for (Listener l : listeners) {
                                    l.updateTimeDependentProcedureInfo(tdp);
                                }
                            });
                        }
                    }
                }
            }
        }
    }
}

