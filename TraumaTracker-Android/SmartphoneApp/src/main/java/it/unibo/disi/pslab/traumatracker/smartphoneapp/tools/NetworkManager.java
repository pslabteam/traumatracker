package it.unibo.disi.pslab.traumatracker.smartphoneapp.tools;

import android.util.Pair;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Handler;

public class NetworkManager {

    private static final int TIMEOUT = 10000; //ms
    private static final int SERVER_OK_RESULT = 200;

    public static Pair<Integer, String> doGET(String url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setConnectTimeout(TIMEOUT);
        urlConnection.setReadTimeout(TIMEOUT);

        int res = urlConnection.getResponseCode();
        StringBuilder response = new StringBuilder();

        if (res == SERVER_OK_RESULT) {
            BufferedReader buff = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));

            String chunks;

            while ((chunks = buff.readLine()) != null) {
                response.append(chunks);
            }
        }

        return new Pair<>(res, response.toString());
    }

    public static Integer doPOST(String url, String content) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        urlConnection.setConnectTimeout(TIMEOUT);
        urlConnection.setReadTimeout(TIMEOUT);
        urlConnection.setRequestProperty("Content-Type", "application/json");
        urlConnection.getOutputStream().write(content.getBytes(StandardCharsets.UTF_8));

        return urlConnection.getResponseCode();
    }

    //----------------------------------

    public static void doPOST(final String url, final String content, final HttpResponseHandler responseHandler) throws IOException {
        final HttpURLConnection urlConnection = createRestRequest("POST", url);
        urlConnection.getOutputStream().write(content.getBytes(StandardCharsets.UTF_8));

        responseHandler.onResponseReceived(urlConnection.getResponseCode(), urlConnection.getResponseMessage(), readResponse(urlConnection));
    }

    public static void doPUT(final String url, final String content, final HttpResponseHandler responseHandler) throws IOException {
        final HttpURLConnection urlConnection = createRestRequest("PUT", url);
        urlConnection.getOutputStream().write(content.getBytes(StandardCharsets.UTF_8));

        responseHandler.onResponseReceived(urlConnection.getResponseCode(), urlConnection.getResponseMessage(), readResponse(urlConnection));
    }

    private static HttpURLConnection createRestRequest(final String type, final String url) throws IOException{
        final HttpURLConnection request = (HttpURLConnection) new URL(url).openConnection();
        request.setRequestMethod(type);
        request.setDoInput(true);
        request.setDoOutput(true);
        request.setConnectTimeout(TIMEOUT);
        request.setReadTimeout(TIMEOUT);
        request.setRequestProperty("Content-Type", "application/json");

        return request;
    }

    private static String readResponse( HttpURLConnection urlConnection) throws IOException{
        try(BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }

            return response.toString();
        }
    }

    public interface HttpResponseHandler{
        void onResponseReceived(final int code, final String message, final String responseBody);
    }
}
