package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.ProcedureCategory;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ProcedureConfirmationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.templates.TraumaTrackerButtonsFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class ProceduresFragment extends TraumaTrackerButtonsFragment {
    private static final int COLUMNS = 3;

    private List<ProcedureCategory> toSkipList = Collections.singletonList(ProcedureCategory.ALS);

    private static Map<Procedure, String> timeDependencyStatusMap = new HashMap<>();

    public ProceduresFragment() {
        for(Procedure p : Procedure.values()){
            if(p.isTimeDependent()){
                timeDependencyStatusMap.put(p, C.report.events.procedure.event.END);
            }
        }
    }

    public static ProceduresFragment newInstance() {
        return new ProceduresFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_procedures, container, false);

        initUI(mainView);

        return mainView;
    }

    private void initUI(View mainView){
        Map<ProcedureCategory, List<Button>> procedureButtonsMap = new TreeMap<>(new ProcedureCategory.ResuscitationOperationCategoryComparator());

        for (ProcedureCategory category : ProcedureCategory.values()) {

            if (toSkipList.contains(category))
                continue;

            List<Button> currentCategoryButtons = new ArrayList<>();

            for (int i = 0; i < Procedure.values().length; i++) {
                if (Procedure.values()[i].getCategory().toString().equals(category.toString())) {
                    final Procedure p = Procedure.values()[i];

                    final int bgRes;

                    switch (p.getCategory()) {
                        case A:
                            bgRes = R.drawable.traumatracker_button_bg_green;
                            break;
                        case B:
                            bgRes = R.drawable.traumatracker_button_bg_yellow;
                            break;
                        case C:
                            bgRes = R.drawable.traumatracker_button_bg_red;
                            break;
                        case OTHERS:
                            bgRes = R.drawable.traumatracker_button_bg_blue;
                            break;

                        default:
                            bgRes = R.drawable.traumatracker_button;
                            break;
                    }

                    int iconRes = p.isTimeDependent() ?
                            (timeDependencyStatusMap.get(p).equals(C.report.events.procedure.event.END) ?
                                    R.drawable.icon_clock_small
                                    : R.drawable.icon_clock_small_green)
                            : 0;

                    currentCategoryButtons.add(buildButton(getContext(), p.toMultilineString(), bgRes, iconRes, v -> {
                        if(!ActiveTrauma.isPatientAcceptedInER()){
                            new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.patient_not_arrived_alert_message), null).show();
                            return;
                        }

                        if(p.isTimeDependent()){
                            final String event = timeDependencyStatusMap.get(p).equals(C.report.events.procedure.event.END)
                                    ? C.report.events.procedure.event.START
                                    : C.report.events.procedure.event.END;

                            new ProcedureConfirmationDialog(getActivity(), p, event, optionsMap -> {
                                timeDependencyStatusMap.put(p, event);

                                ActiveTrauma.registerNewTimeDependentProcedure(p, event, optionsMap);

                                int iconRes1 = timeDependencyStatusMap.get(p).equals(C.report.events.procedure.event.END)
                                        ? R.drawable.icon_clock_small
                                        : R.drawable.icon_clock_small_green;

                                ((Button) v).setCompoundDrawablesWithIntrinsicBounds(iconRes1, 0, 0, 0);
                            }).show();
                        } else {
                            new ProcedureConfirmationDialog(getActivity(), p, "", optionsMap
                                    -> ActiveTrauma.registerNewProcedure(p, optionsMap)).show();
                        }
                    }));
                }
            }

            procedureButtonsMap.put(category, currentCategoryButtons);
        }

        buildButtonsGrid(mainView, procedureButtonsMap, COLUMNS);
    }
}

