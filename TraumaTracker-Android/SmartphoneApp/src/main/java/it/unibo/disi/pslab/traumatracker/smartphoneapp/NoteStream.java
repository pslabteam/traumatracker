package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cartago.ARTIFACT_INFO;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.OpFeedbackParam;
import cartago.OperationException;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.exceptions.UserNotFoundException;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.AnamnesiElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.BloodProduct;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.MajorTraumaCriteriaElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.PrehElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.TraumaInfoElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.MapUtils;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

@ARTIFACT_INFO(outports = {@OUTPORT(name = "eventStream")})
public class NoteStream extends JaCaArtifact {

    private List<Note> notes;
    private ReportDetails reportDetails;

    private static NoteStream me;

    private String reportId;

    void init() {
        me = this;

        notes = new ArrayList<>();
        reportDetails = new ReportDetails();
    }

    @OPERATION
    void registerStartEvent(User traumaLeader, boolean delayedActivation, String delayedActivationDate, String delayedActivationTime, OpFeedbackParam<String> reportId) {

        Note.Counter.init();

        reportDetails = new ReportDetails();

        reportDetails.initialTraumaLeader(traumaLeader);
        reportDetails.delayedActivation(delayedActivation);
        reportDetails.delayedActivationDate(delayedActivationDate);
        reportDetails.delayedActivationTime(delayedActivationTime);
        reportDetails.startDate(DateTimeUtils.getCurrentDate());
        reportDetails.startTime(DateTimeUtils.getCurrentTime());

        notes = new ArrayList<>();

        me.reportId = C.fs.REPORT_FILE_NAME_PREFIX
                + reportDetails.startDate().replace("-", "")
                + "-"
                + reportDetails.startTime().replace(":", "");

        reportId.set(me.reportId);
    }

    @OPERATION
    void registerRestartEvent(String reportId) {
        String reportContent = FileSystemUtils.loadFile(C.fs.TEMP_FOLDER, reportId + C.fs.REPORT_FILE_EXTENSION);

        Report r = Report.build(reportContent, false);

        if (r != null) {
            try {
                Note.Counter.init(r.eventTrack().getEvents().size() + 1);

                reportDetails = new ReportDetails();

                reportDetails.initialTraumaLeader(User.getById(r.operator()));
                reportDetails.delayedActivation(r.delayedActivation());
                reportDetails.delayedActivationDate(r.delayedActivationDate());
                reportDetails.delayedActivationTime(r.delayedActivationTime());
                reportDetails.startDate(r.startDate());
                reportDetails.startTime(r.startTime());

                notes = new ArrayList<>();

                final JSONArray eventsArray = new JSONObject(reportContent).getJSONArray(C.report.EVENTS);

                for (int i = 0; i < eventsArray.length(); i++) {
                    notes.add(Note.restoreNote(eventsArray.getJSONObject(i)));
                }

                me.reportId = reportId;
            } catch (JSONException | UserNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @OPERATION
    void registerTerminationEvent(String destination, String iss) {
        reportDetails.endDate(DateTimeUtils.getCurrentDate());
        reportDetails.endTime(DateTimeUtils.getCurrentTime());
        reportDetails.iss(iss);
        reportDetails.finalDestination(destination);
    }

    @OPERATION
    void registerPatientAccessedErEvent(String place) {
        addNote(Note.makePatientAccessedErNote(place));
    }

    @OPERATION
    void addReportReactivationNote(String place) {
        addNote(Note.makeReportReactivationNote(place));
    }

    @OPERATION
    void addProcedureNote(String place, Procedure procedure, String options) {
        final Note note = Note.makeProcedureNote(place, procedure, MapUtils.deserializeMap(options));
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());

        addEvent("registerProcedureEvent", procedure);
    }

    @OPERATION
    void addTimeDependentProcedureNote(String place, Procedure procedure, String eventType, String options) {
        final Note note = Note.makeTimeDependentProcedureNote(place, procedure, eventType, MapUtils.deserializeMap(options));
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());

        addEvent("registerTimeDependentProcedureEvent", procedure, eventType);
    }

    @OPERATION
    void addDiagnosticNote(String place, Diagnostics diagnostic, String options) {
        final Note note = Note.makeDiagnosticNote(place, diagnostic, MapUtils.deserializeMap(options));
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());

        addEvent("registerDiagnosticEvent", diagnostic);
    }

    @OPERATION
    void addDrugNote(String place, Drug drug, double qty) {
        final Note note = Note.makeDrugNote(place, drug, qty);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());

        addEvent("registerDrugEvent", drug, qty);
    }

    @OPERATION
    void addDrugNote(String place, Drug drug) {
        final Note note = Note.makeDrugNote(place, drug);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());
    }

    @OPERATION
    void addBloodProductNote(String place, BloodProduct bloodProduct, double qty) {
        final Note note = Note.makeBloodProductNote(place, bloodProduct, qty);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());
    }

    @OPERATION
    void addBloodProductNote(String place, BloodProduct bloodProduct, double qty, String bagCode) {
        final Note note = Note.makeBloodProductNote(place, bloodProduct, qty, bagCode);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());
    }

    @OPERATION
    void addBloodProductNote(String place, BloodProduct bloodProduct) {
        final Note note = Note.makeBloodProductNote(place, bloodProduct);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());

        //addEvent("registerDrugEvent", drug);
    }

    @OPERATION
    void addStartContinuousInfusionDrugNote(String place, Drug drug, double qty) {
        final Note note = Note.makeContinuousInfusionStartDrugNote(place, drug, qty);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());
    }

    @OPERATION
    void addVariationContinuousInfusionDrugNote(String place, Drug drug, double qty) {
        final Note note = Note.makeContinuousInfusionVariationDrugNote(place, drug, qty);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());
    }

    @OPERATION
    void addStopContinuousInfusionDrugNote(String place, Drug drug, int duration) {
        final Note note = Note.makeContinuousInfusionStopDrugNote(place, drug, duration);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());
    }

    @OPERATION
    void addVitalSignMonitorNote(String place, int sys, int dia, int hr, int etco2, int spo2, int temp) {
        addNote(Note.makeVitalSignsMonitorNote(place, sys, dia, hr, etco2, spo2, temp));
    }

    @OPERATION
    void addVitalSignVariationNote(String place, VitalSign vs, String value) {
        addNote(Note.makeVitalSignNote(place, vs, value));

        addEvent("registerVitalSignEvent", vs, value);
    }

    @OPERATION
    void addSnapshotNote(String place, String filename) {
        addNote(Note.makeSnapNote(place, filename));
    }

    @OPERATION
    void addVideoNote(String place, String filename) {
        addNote(Note.makeVideoNote(place, filename));
    }

    @OPERATION
    void addVocalNote(String place, String filename) {
        addNote(Note.makeVocalNote(place, filename));
    }

    @OPERATION
    void addTextNote(String place, String note) {
        addNote(Note.makeTextNote(place, note));
    }

    @OPERATION
    void addTraumaLeaderNote(User user, String place) {
        addNote(Note.makeTraumaLeaderNote(place, user));
    }

    @OPERATION
    void registerRoomInEvent(String place) {
        if (place.isEmpty()) {
            return;
        }

        final Note note = Note.makeRoomInNote(place);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());

        addEvent("registerRoomEnterEvent", place);
    }

    @OPERATION
    void registerRoomOutEvent(String place) {
        if (place.isEmpty() || place.equals(C.place.PREH)) {
            return;
        }

        final Note note = Note.makeRoomOutNote(place);
        addNote(note);

        ActiveTrauma.notifyEvent(note.getContent());

        addEvent("registerRoomExitEvent", place);
    }

    @OPERATION
    void updateInfoForEvent(int id, String newDate, String newTime, String newPlace) {
        for (Note n : notes) {
            try {
                if (n.getContent().getInt(C.report.events.ID) == id) {
                    Note newNote = Note.updateNote(n, newDate, newTime, newPlace);
                    notes.set(notes.indexOf(n), newNote);
                    log("updated previous note: " + newNote.getContent().toString());
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void clearNotesList() {
        try {
            for (final NoteSet ns : checkTransportErrorRule(getPlaceNotes(notes))) {

                final Iterator<Note> i = notes.iterator();

                while (i.hasNext()) {
                    final Note n = i.next();

                    if (ns.list().contains(n)) {
                        i.remove();
                        continue;
                    }

                    final int id = n.getContent().getInt(C.report.events.ID);

                    if (id > ns.startIndex() && id < ns.endIndex()) {
                        n.updateNotePlace(ns.label());
                    }
                }
            }
        } catch (final JSONException e) {
            e.printStackTrace();
        }
    }

    private List<Note> getPlaceNotes(final List<Note> notes) throws JSONException {
        final List<Note> placeNotes = new ArrayList<>();

        for (final Note n : notes) {
            if (n.getContent().getString(C.report.events.TYPE).equals(C.report.events.type.ROOM_IN)
                    || n.getContent().getString(C.report.events.TYPE).equals(C.report.events.type.ROOM_OUT)) {
                placeNotes.add(n);
            }
        }

        return placeNotes;
    }

    private List<NoteSet> checkTransportErrorRule(List<Note> list) throws JSONException {
        final List<NoteSet> toBeRemoved = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            final Note currentNote = list.get(i);

            boolean isTransportIn = currentNote.getContent().getString(C.report.events.TYPE).equals(C.report.events.type.ROOM_IN)
                    && currentNote.getContent().getString(C.report.events.PLACE).equals(C.place.TRANSPORT);

            if (isTransportIn) {
                try {
                    final Note prev1 = list.get(i - 1);
                    final Note next1 = list.get(i + 1);
                    final Note next2 = list.get(i + 2);

                    if (prev1.getContent().getString(C.report.events.PLACE).equals(next2.getContent().getString(C.report.events.PLACE))) {
                        final List<Note> notes = new ArrayList<>();
                        notes.add(prev1);
                        notes.add(currentNote);
                        notes.add(next1);
                        notes.add(next2);

                        toBeRemoved.add(new NoteSet(
                                notes,
                                currentNote.getContent().getInt(C.report.events.ID),
                                next1.getContent().getInt(C.report.events.ID),
                                prev1.getContent().getString(C.report.events.PLACE)
                        ));
                    }

                } catch (IndexOutOfBoundsException ignored) {
                }
            }
        }

        return toBeRemoved;
    }

    @OPERATION
    void buildReport(OpFeedbackParam<JSONObject> report, OpFeedbackParam<String> reportName) {

        final JSONObject reportContent = new JSONObject();

        try {
            clearNotesList();

            final JSONArray eventsArray = new JSONArray();

            for (final Note note : notes) {
                eventsArray.put(note.getContent());
            }

            reportContent
                    .put(C.report.ID, reportId)
                    .put(C.report.VERSION, C.REPORT_VERSION)
                    .put(C.report.VALIDATION, new JSONObject()
                            .put(C.report.validation.IS_VALIDATED, false)
                            .put(C.report.validation.OPERATOR_ID, "")
                            .put(C.report.validation.DATE, "")
                            .put(C.report.validation.TIME, ""))
                    .put(C.report.START_OPERATOR_ID, reportDetails.initialTraumaLeader().id())
                    .put(C.report.START_OPERATOR_DESCRIPTION, reportDetails.initialTraumaLeader().toString())
                    .put(C.report.DELAYED_ACTIVATION, new JSONObject()
                            .putOpt(C.report.delayedActivation.STATUS, reportDetails.delayedActivation())
                            .putOpt(C.report.delayedActivation.ORIGINAL_ACCESS_DATE, reportDetails.delayedActivationDate().isEmpty()
                                    ? null : reportDetails.delayedActivationDate())
                            .putOpt(C.report.delayedActivation.ORIGINAL_ACCESS_TIME, reportDetails.delayedActivationTime().isEmpty()
                                    ? null : reportDetails.delayedActivationTime()))
                    .put(C.report.TRAUMA_TEAM_MEMBERS, new JSONArray(ActiveTrauma.traumaTeam().getTeam()))
                    .put(C.report.START_DATE, reportDetails.startDate())
                    .put(C.report.START_TIME, reportDetails.startTime())
                    .put(C.report.END_DATE, reportDetails.endDate())
                    .put(C.report.END_TIME, reportDetails.endTime())
                    .put(C.report.ISS, reportDetails.iss().isEmpty()
                            ? new JSONObject()
                            : new JSONObject(reportDetails.iss()))
                    .put(C.report.FINAL_DESTINATION, reportDetails.finalDestination())
                    .put(C.report.TRAUMA_INFO, new JSONObject()
                            .putOpt(C.report.traumaInfo.CODE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.CODE).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.CODE))
                            .putOpt(C.report.traumaInfo.SDO, ActiveTrauma.getTraumaInfo(TraumaInfoElement.SDO).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.SDO))
                            .putOpt(C.report.traumaInfo.ER_DECEASED, convertYesNo(ActiveTrauma.getTraumaInfo(TraumaInfoElement.ER_DECEASED)))
                            .putOpt(C.report.traumaInfo.ADMISSION_CODE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ADMISSION_CODE).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ADMISSION_CODE))
                            .putOpt(C.report.traumaInfo.NAME, ActiveTrauma.getTraumaInfo(TraumaInfoElement.NAME).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.NAME))
                            .putOpt(C.report.traumaInfo.SURNAME, ActiveTrauma.getTraumaInfo(TraumaInfoElement.SURNAME).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.SURNAME))
                            .putOpt(C.report.traumaInfo.GENDER, ActiveTrauma.getTraumaInfo(TraumaInfoElement.GENDER).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.GENDER))
                            .putOpt(C.report.traumaInfo.DOB, ActiveTrauma.getTraumaInfo(TraumaInfoElement.DOB).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.DOB))
                            .putOpt(C.report.traumaInfo.AGE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.AGE).isEmpty()
                                    ? null
                                    : Integer.parseInt(ActiveTrauma.getTraumaInfo(TraumaInfoElement.AGE)))
                            .putOpt(C.report.traumaInfo.ACCIDENT_DATE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_DATE).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_DATE))
                            .putOpt(C.report.traumaInfo.ACCIDENT_TIME, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TIME).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TIME))
                            .putOpt(C.report.traumaInfo.ACCIDENT_TYPE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TYPE).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TYPE))
                            .putOpt(C.report.traumaInfo.VEHICLE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.VEHICLE).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.VEHICLE))
                            .putOpt(C.report.traumaInfo.FROM_OTHER_EMERGENCY, convertYesNo(ActiveTrauma.getTraumaInfo(TraumaInfoElement.FROM_OTHER_EMERGENCY)))
                            .putOpt(C.report.traumaInfo.OTHER_EMERGENCY, ActiveTrauma.getTraumaInfo(TraumaInfoElement.OTHER_EMERGENCY).isEmpty()
                                    ? null
                                    : ActiveTrauma.getTraumaInfo(TraumaInfoElement.OTHER_EMERGENCY)))
                    .put(C.report.MAJOR_TRAUMA_CRITERIA, new JSONObject()
                            .putOpt(C.report.majorTraumaCriteria.DYNAMIC, ActiveTrauma.getMajorTraumaCriteriaElement(MajorTraumaCriteriaElement.DYNAMIC))
                            .putOpt(C.report.majorTraumaCriteria.PHYSIOLOGICAL, ActiveTrauma.getMajorTraumaCriteriaElement(MajorTraumaCriteriaElement.PHYSIOLOGICAL))
                            .putOpt(C.report.majorTraumaCriteria.ANATOMICAL, ActiveTrauma.getMajorTraumaCriteriaElement(MajorTraumaCriteriaElement.ANATOMICAL)))
                    .put(C.report.ANAMNESI, new JSONObject()
                            .putOpt(C.report.anamnesi.ANTIPLATELETS, ActiveTrauma.getAnamnesiElement(AnamnesiElement.ANTIPLATELETS))
                            .putOpt(C.report.anamnesi.ANTICOAGULANTS, ActiveTrauma.getAnamnesiElement(AnamnesiElement.ANTICOAGULANTS))
                            .putOpt(C.report.anamnesi.NAO, ActiveTrauma.getAnamnesiElement(AnamnesiElement.NAO)))
                    .put(C.report.PREH, new JSONObject()
                            .putOpt(C.report.preh.TERRITORIAL_AREA, ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA).isEmpty()
                                    ? null
                                    : ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA))
                            .putOpt(C.report.preh.CAR_ACCIDENT, ActiveTrauma.getPrehElement(PrehElement.CAR_ACCIDENT).isEmpty()
                                    ? null
                                    : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.CAR_ACCIDENT)))
                            .putOpt(C.report.preh.A_AIRWAYS, ActiveTrauma.getPrehElement(PrehElement.A).isEmpty()
                                    ? null
                                    : ActiveTrauma.getPrehElement(PrehElement.A))
                            .putOpt(C.report.preh.B_PLEURAL_DECOMPRESSION, ActiveTrauma.getPrehElement(PrehElement.B_PLEURAL_DECOMPRESSION).isEmpty()
                                    ? null
                                    : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.B_PLEURAL_DECOMPRESSION)))
                            .putOpt(C.report.preh.C_BLOOD_PROTOCOL, ActiveTrauma.getPrehElement(PrehElement.C_BLOOD_PROTOCOL).isEmpty()
                                    ? null
                                    : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.C_BLOOD_PROTOCOL)))
                            .putOpt(C.report.preh.C_TPOD, ActiveTrauma.getPrehElement(PrehElement.C_TPOD).isEmpty()
                                    ? null
                                    : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.C_TPOD)))
                            .putOpt(C.report.preh.D_GCS_TOTAL, ActiveTrauma.getPrehElement(PrehElement.D_GCS_TOTAL).isEmpty()
                                    ? null
                                    : Double.parseDouble(ActiveTrauma.getPrehElement(PrehElement.D_GCS_TOTAL)))
                            .putOpt(C.report.preh.D_ANISOCORIA, ActiveTrauma.getPrehElement(PrehElement.D_ANISOCORIA).isEmpty()
                                    ? null
                                    : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.D_ANISOCORIA)))
                            .putOpt(C.report.preh.D_MIDRIASI, ActiveTrauma.getPrehElement(PrehElement.D_MIDRIASI).isEmpty()
                                    ? null
                                    : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.D_MIDRIASI)))
                            .putOpt(C.report.preh.E_MOTILITY, ActiveTrauma.getPrehElement(PrehElement.E_PARA_TETRA_PARESI).isEmpty()
                                    ? null
                                    : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.E_PARA_TETRA_PARESI)))
                            .putOpt(C.report.preh.WORST_BLOOD_PRESSURE, ActiveTrauma.getPrehElement(PrehElement.WROST_BLOOD_PRESSURE).isEmpty()
                                    ? null
                                    : Double.parseDouble(ActiveTrauma.getPrehElement(PrehElement.WROST_BLOOD_PRESSURE)))
                            .putOpt(C.report.preh.WORST_RESPIRATORY_RATE, ActiveTrauma.getPrehElement(PrehElement.WROST_RESPIRATORY_RATE).isEmpty()
                                    ? null
                                    : Double.parseDouble(ActiveTrauma.getPrehElement(PrehElement.WROST_RESPIRATORY_RATE))))
                    .put(C.report.PATIENT_INITIAL_CONDITION, new JSONObject()
                            .put(C.report.patientInitialCondition.VITAL_SIGNS, new JSONObject()
                                    .putOpt(C.report.patientInitialCondition.vitalSigns.TEMPERATURE, ActiveTrauma.getInitialVitalSign(VitalSign.TEMPERATURE).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.TEMPERATURE))
                                    .putOpt(C.report.patientInitialCondition.vitalSigns.HEART_RATE, ActiveTrauma.getInitialVitalSign(VitalSign.HEART_RATE).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.HEART_RATE))
                                    .putOpt(C.report.patientInitialCondition.vitalSigns.BLOOD_PRESSURE, ActiveTrauma.getInitialVitalSign(VitalSign.BLOOD_PRESSURE).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.BLOOD_PRESSURE))
                                    .putOpt(C.report.patientInitialCondition.vitalSigns.SPO2, ActiveTrauma.getInitialVitalSign(VitalSign.SPO2).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.SPO2))
                                    .putOpt(C.report.patientInitialCondition.vitalSigns.ETCO2, ActiveTrauma.getInitialVitalSign(VitalSign.ETCO2).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.ETCO2)))
                            .put(C.report.patientInitialCondition.CLINICAL_PICTURE, new JSONObject()
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_TOTAL, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_TOTAL).isEmpty()
                                            ? null
                                            : Double.parseDouble(ActiveTrauma.getInitialVitalSign(VitalSign.GCS_TOTAL)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_MOTOR, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_MOTOR).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.GCS_MOTOR))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_VERBAL, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_VERBAL).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.GCS_VERBAL))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_EYES, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_EYES).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.GCS_EYES))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.SEDATED, ActiveTrauma.getInitialVitalSign(VitalSign.SEDATED).isEmpty()
                                            ? null
                                            : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.SEDATED)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.PUPILS, ActiveTrauma.getInitialVitalSign(VitalSign.PUPILS).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.PUPILS))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.AIRWAYS, ActiveTrauma.getInitialVitalSign(VitalSign.AIRWAYS).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.AIRWAYS))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.POSITIVE_INHALATION, ActiveTrauma.getInitialVitalSign(VitalSign.POSITIVE_INHALATION).isEmpty()
                                            ? null
                                            : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.POSITIVE_INHALATION)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.INTUBATION_FAILED, ActiveTrauma.getInitialVitalSign(VitalSign.INTUBATION_FAILED).isEmpty()
                                            ? null
                                            : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.INTUBATION_FAILED)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.CHEST_TUBE, ActiveTrauma.getInitialVitalSign(VitalSign.CHEST_TUBE).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.CHEST_TUBE))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.OXYGEN_PERCENTAGE, ActiveTrauma.getInitialVitalSign(VitalSign.OXYGEN_PERCENTAGE).isEmpty()
                                            ? null
                                            : Double.parseDouble(ActiveTrauma.getInitialVitalSign(VitalSign.OXYGEN_PERCENTAGE)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.HEMORRHAGE, ActiveTrauma.getInitialVitalSign(VitalSign.HEMORRHAGE).isEmpty()
                                            ? null
                                            : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.HEMORRHAGE)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.LIMBS_FRACTURE, ActiveTrauma.getInitialVitalSign(VitalSign.LIMBS_FRACTURE).isEmpty()
                                            ? null
                                            : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.LIMBS_FRACTURE)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.FRACTURE_EXPOSITION, ActiveTrauma.getInitialVitalSign(VitalSign.FRACTURE_EXPOSITION).isEmpty()
                                            ? null
                                            : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.FRACTURE_EXPOSITION)))
                                    .putOpt(C.report.patientInitialCondition.clinicalPicture.BURN, ActiveTrauma.getInitialVitalSign(VitalSign.BURN).isEmpty()
                                            ? null
                                            : ActiveTrauma.getInitialVitalSign(VitalSign.BURN))))
                    .put(C.report.EVENTS, eventsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        report.set(reportContent);
        reportName.set(reportId);
    }

    private boolean convertYesNo(final String value) {

        boolean ret = false;

        if (value.equals(App.getAppResources().getString(R.string.switch_yes_label).toLowerCase())) {
            ret = true;
        } else if (value.equals(App.getAppResources().getString(R.string.switch_no_label).toLowerCase())) {
            ret = false;
        }

        return ret;
    }

    private void addNote(Note note) {
        notes.add(note);
        log("added new note: " + note.getContent().toString());
    }

    private void addEvent(String event, Object... params) {
        try {
            execLinkedOp("eventStream", event, params);
        } catch (OperationException e) {
            e.printStackTrace();
        }
    }

    public static void requestEventInfoUpdate(int eventId, String newDate, String newTime, String newPlace) {
        me.beginExternalSession();
        me.signal("user_cmd", "update_event_info", eventId, newDate, newTime, newPlace);
        me.endExternalSession(true);
    }

    public static List<Note> requestEventsList() {
        return new ArrayList<>(me.notes);
    }


    private class NoteSet {
        private List<Note> list;
        private int startIndex, endIndex;
        private String label;

        NoteSet(final List<Note> list, final int starIndex, final int endIndex, final String label) {
            this.list = list;
            this.startIndex = starIndex;
            this.endIndex = endIndex;
            this.label = label;
        }

        public List<Note> list() {
            return list;
        }

        int startIndex() {
            return startIndex;
        }

        int endIndex() {
            return endIndex;
        }

        String label() {
            return label;
        }
    }

    private class ReportDetails {
        private String startDate = "", startTime = "", endDate = "", endTime = "";
        private boolean delayedActivation;
        private String delayedActivationDate = "";
        private String delayedActivationTime = "";
        private String finalDestination = "", iss = "";
        private User initialTraumaLeader;

        String startDate() {
            return startDate;
        }

        String startTime() {
            return startTime;
        }

        String endDate() {
            return endDate;
        }

        String endTime() {
            return endTime;
        }

        boolean delayedActivation() {
            return delayedActivation;
        }

        String delayedActivationDate() {
            return delayedActivationDate;
        }

        String delayedActivationTime() {
            return delayedActivationTime;
        }

        String iss() {
            return iss;
        }

        String finalDestination() {
            return finalDestination;
        }

        User initialTraumaLeader() {
            return initialTraumaLeader;
        }

        void startDate(String startDate) {
            this.startDate = startDate;
        }

        void startTime(String startTime) {
            this.startTime = startTime;
        }

        void endDate(String endDate) {
            this.endDate = endDate;
        }

        void endTime(String endTime) {
            this.endTime = endTime;
        }

        void delayedActivation(boolean delayedActivation) {
            this.delayedActivation = delayedActivation;
        }

        void delayedActivationDate(String delayedActivationDate) {
            this.delayedActivationDate = delayedActivationDate;
        }

        void delayedActivationTime(String delayedActivationTime) {
            this.delayedActivationTime = delayedActivationTime;
        }

        void iss(String iss) {
            this.iss = iss;
        }

        void finalDestination(String finalDestination) {
            this.finalDestination = finalDestination;
        }

        void initialTraumaLeader(User initialTraumaLeader) {
            this.initialTraumaLeader = initialTraumaLeader;
        }
    }
}

