package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum VitalSignUnit {
    CELSIUS(R.string.vs_unit_celsius),
    BPM(R.string.vs_unit_bpm),
    MMHG(R.string.vs_unit_mmhg),
    PERCENTAGE(R.string.vs_unit_percentage),
    EMPTY(R.string.vs_unit_empty);

    private final int labelRes;

    VitalSignUnit(final int labelRes){
        this.labelRes = labelRes;
    }

    public String toString(){
        return App.getAppResources().getString(labelRes);
    }
}
