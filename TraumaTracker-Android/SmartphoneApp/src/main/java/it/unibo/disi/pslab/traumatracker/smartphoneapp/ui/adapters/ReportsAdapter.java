package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import android.support.annotation.NonNull;

import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.StarterUI;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ReportDetailsDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ReportDeleteDialog;

public class ReportsAdapter extends ArrayAdapter<Report> {

    private static class ViewHolder {
        TextView reportIdLabel,operatorLabel, startDateLabel, startTimeLabel, endDateLabel, endTimeLabel, destinationLabel, issLabel;
        ImageView statusIcon;
        TextView detailsButton, syncButton, deleteButton;
    }

    public ReportsAdapter(Context context, List<Report> data) {
        super(context, R.layout.item_reports_archive, data);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull final ViewGroup parent) {
        final Report report = getItem(position);

        final ViewHolder vh;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_reports_archive, parent, false);

            vh = new ViewHolder();

            vh.reportIdLabel = convertView.findViewById(R.id.reportID);
            vh.operatorLabel = convertView.findViewById(R.id.operator);
            vh.startDateLabel = convertView.findViewById(R.id.reportStartDate);
            vh.startTimeLabel = convertView.findViewById(R.id.reportStartTime);
            vh.endDateLabel = convertView.findViewById(R.id.reportEndDate);
            vh.endTimeLabel = convertView.findViewById(R.id.reportEndTime);
            vh.destinationLabel = convertView.findViewById(R.id.reportDestination);
            vh.issLabel = convertView.findViewById(R.id.reportIss);
            vh.statusIcon = convertView.findViewById(R.id.reportStatus);
            vh.detailsButton = convertView.findViewById(R.id.reportDetails);
            vh.syncButton = convertView.findViewById(R.id.reportSync);
            vh.deleteButton = convertView.findViewById(R.id.reportDelete);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        if(report != null){
            vh.reportIdLabel.setText(report.id());

            final User operator = FileSystem.getUserById(report.operator());
            if(operator != null){
                vh.operatorLabel.setText(operator.toString());
            }

            vh.startDateLabel.setText(report.startDate());
            vh.startTimeLabel.setText(report.startTime());
            vh.endDateLabel.setText(report.endDate());
            vh.endTimeLabel.setText(report.endTime());
            vh.destinationLabel.setText(report.finalDestination());
            vh.issLabel.setText(report.iss());

            vh.statusIcon.setImageResource(report.isSync() ? R.drawable.icon_sync : R.drawable.icon_not_sync);

            vh.detailsButton.setOnClickListener(v -> new ReportDetailsDialog(getContext(), report).showFullscreen());

            vh.syncButton.setOnClickListener(v -> {
                if(report.isSync()){
                    new MessageDialog(getContext(), getContext().getString(R.string.warning), "Report già sincronizzato!", null).show();
                } else {
                    StarterUI.instance.manageReportSyncRequest(report.id(), () -> syncReport(report));
                }
            });

            vh.deleteButton.setOnClickListener(v -> new ReportDeleteDialog<>(getContext(), report, () -> removeReport(report)).show());
        }

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    private void removeReport(final Report report){
        remove(report);
        notifyDataSetChanged();
    }

    private void syncReport(final Report report){
        report.sync(true);
        notifyDataSetChanged();
    }
}
