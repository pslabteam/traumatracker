package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cartago.LINK;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import cartago.Tuple;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class EventStream extends JaCaArtifact {

    private static final String ZERO_NEGATIVE_OBS_PROP = "zero_negative";
    private static final String FRACTURES_OBS_PROP = "fractures";

    void init(){
        defineObsProperty(ZERO_NEGATIVE_OBS_PROP, 0.0);
        defineObsProperty(FRACTURES_OBS_PROP, false);
    }

    @LINK public void registerDrugEvent(Drug drug, Double qty){

        switch(drug.id()){
            case "zero-negative":
                double value = getObsProperty(ZERO_NEGATIVE_OBS_PROP).doubleValue() + qty;
                updateObsProperty(ZERO_NEGATIVE_OBS_PROP, value);
                break;
        }

        notifyEvent(new Tuple("drug", drug.id(), qty));
    }

    @LINK public void registerProcedureEvent(Procedure procedure){
        notifyEvent(new Tuple("procedure", procedure.getId()));
    }

    @LINK public void registerTimeDependentProcedureEvent(Procedure procedure, String eventType){
        if(eventType.equals("start")){
            notifyEvent(new Tuple("procedureStarted", procedure.getId()));
        }

        if(eventType.equals("end")){
            notifyEvent(new Tuple("procedureStopped", procedure.getId()));
        }
    }

    @LINK public void registerDiagnosticEvent(Diagnostics diagnostic){
        notifyEvent(new Tuple("diagnostic", diagnostic.id()));
    }

    @LINK public void registerRoomEnterEvent(String room){
        notifyEvent(new Tuple("roomIn", room));
    }

    @LINK public void registerRoomExitEvent(String room){
        notifyEvent(new Tuple("roomOut", room));
    }

    @LINK public void registerVitalSignEvent(VitalSign vs, String value){
        try {
            notifyEvent(new Tuple("vs", vs.getId(), Double.valueOf(value)));
        } catch (NumberFormatException ignored){ }
    }

    private void notifyEvent(Tuple event){
        signal("new_event", event);
    }

    // ---- UTILS ----

    @OPERATION public void timestamp(OpFeedbackParam<String> timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        timestamp.set(sdf.format(new Date()));
    }

    //---- TEST ----

    @OPERATION public void printEventList(Object[] list){
        for(Object obj : list){
            System.out.println("[warningManager] "+obj.toString());
        }
    }
}
