package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;


public class MediaShowDialog extends TTDialog {

    public MediaShowDialog(final Context context, final String type, final String filename) {
        super(context, R.layout.dialog_media_show);

        initUI(type, filename);
    }

    private void initUI(final String type, final String filename){
        findViewById(R.id.exitLabel).setOnClickListener(view -> dismiss());
        ((TextView) findViewById(R.id.title)).setText(filename);

        findViewById(R.id.photo_container).setVisibility(View.GONE);
        findViewById(R.id.video_container).setVisibility(View.GONE);

        findViewById(R.id.media_start_button).setVisibility(View.GONE);
        findViewById(R.id.media_stop_button).setVisibility(View.GONE);

        switch(type){
            case C.report.events.type.PHOTO:
                showPhoto(filename);
                break;

            case C.report.events.type.VIDEO:
                showVideo(filename);
                break;

            case C.report.events.type.VOCAL_NOTE:
                break;
        }
    }

    private void showPhoto(final String filename){
        findViewById(R.id.photo_container).setVisibility(View.VISIBLE);

        File photo = new File(Environment.getExternalStorageDirectory() + C.fs.PICTURES_FOLDER + "/" + filename);
        if(photo.exists()) {
            ((ImageView)findViewById(R.id.photo_container)).setImageURI(Uri.fromFile(photo));
        }
    }

    private void showVideo(final String filename){
        findViewById(R.id.video_container).setVisibility(View.VISIBLE);

        findViewById(R.id.media_start_button).setVisibility(View.VISIBLE);
        findViewById(R.id.media_stop_button).setVisibility(View.VISIBLE);

        File video = new File(Environment.getExternalStorageDirectory() + C.fs.VIDEOS_FOLDER + "/" + filename);
        VideoView videoView = findViewById(R.id.video_container);

        if(video.exists()){
            videoView.setVideoURI(Uri.fromFile(video));
            videoView.setOnCompletionListener(mediaPlayer -> {
                findViewById(R.id.media_start_button).setEnabled(true);
                findViewById(R.id.media_stop_button).setEnabled(false);
            });
            videoView.requestFocus();
        }

        findViewById(R.id.media_start_button).setOnClickListener(view -> {
            if(videoView != null && video.exists()){
                videoView.resume();
                videoView.start();
                findViewById(R.id.media_start_button).setEnabled(false);
                findViewById(R.id.media_stop_button).setEnabled(true);
            }
        });

        findViewById(R.id.media_stop_button).setOnClickListener(view -> {
            if(videoView != null && video.exists() && videoView.isPlaying()){
                videoView.stopPlayback();

                findViewById(R.id.media_start_button).setEnabled(true);
                findViewById(R.id.media_stop_button).setEnabled(false);
            }
        });
    }
}
