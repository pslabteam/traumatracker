package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public final class LockDialog extends TTDialog {

    public LockDialog(final Context context, final String message) {
        super(context, R.layout.dialog_lock);

        initUI(message);
    }

    private void initUI(final String message) {
        ((TextView)findViewById(R.id.noUsersLabel)).setText(message);

        findViewById(R.id.restoreButton).setOnClickListener(v -> new MessageDialogWithChoice(getContext(), getString(R.string.warning), getString(R.string.lock_dialog_confirmation_message), new MessageDialogWithChoice.Listener() {
            @Override
            public void onNoButtonClicked() {
                //Nothing to do!
            }

            @Override
            public void onYesButtonClicked() {
                dismiss();
            }
        }).show());
    }
}
