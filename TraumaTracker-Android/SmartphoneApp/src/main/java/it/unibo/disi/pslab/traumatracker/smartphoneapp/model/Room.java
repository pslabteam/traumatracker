package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class Room {
    private String description;
    private String inDate, inTime;
    private int permanenceTime;

    private boolean isInitialized;

    public Room() {
        isInitialized = false;
    }

    public void init(String description, String inDate, String inTime) {
        this.description = description;
        this.inDate = inDate;
        this.inTime = inTime;
        this.permanenceTime = 0;
        this.isInitialized = true;
    }

    public boolean isInitialized(){
        return isInitialized;
    }

    public String description() {
        return description;
    }

    public String inDate() {
        return inDate;
    }

    public String inTime() {
        return inTime;
    }

    public String permanenceTime() {
        return DateTimeUtils.convertSecondsToString(permanenceTime);
    }

    public void updatePermanenceTime() {
        permanenceTime++;
    }
}
