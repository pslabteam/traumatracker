package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import java.util.Objects;

public class TTDialog extends Dialog {

    public TTDialog(final Context context, final int layout){
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(layout);
        setCancelable(false);
        setFullHorizontalLayout();
    }

    protected String getString(final int resId){
        return  getContext().getString(resId);
    }

    protected Resources getResources(){
        return  getContext().getResources();
    }

    protected void hideKeyboard(final int res) {
        final View view = findViewById(res);

        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getContext().getSystemService(Context.INPUT_METHOD_SERVICE)))
                    .hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showFullscreen(){
        Objects.requireNonNull(getWindow()).setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        show();
    }

    private void setFullHorizontalLayout(){
        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        final Window window = getWindow();

        if(window != null){
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }
    }

    protected int dpi(int value){
        return (int) (value * getResources().getDisplayMetrics().density);
    }
}
