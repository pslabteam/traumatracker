package it.unibo.disi.pslab.traumatracker.smartphoneapp;

//import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.ISS;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.TraumaInfoElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools.LocationDetector;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.FragmentTabsAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TabsChangeListener;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerPager;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ClinicalPictureVariationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.LockDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialogWithChoice;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MultimediaNotesDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.PatientInitialStatusDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.PrehDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.PrehImportDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ProcedureConfirmationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ReportOverviewDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.TraumaInfoDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.TraumaPathDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.TraumaTeamDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.TraumaTerminationCodesWarningDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.TraumaTerminationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.BloodProductsFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.DrugsFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;
import it.unibo.pslab.jaca_android.core.ActivityArtifact;
import it.unibo.pslab.jaca_android.core.JaCaBaseActivity;

public class MainUI extends ActivityArtifact{

    public static class MainActivity extends JaCaBaseActivity {}
    
    private static final String USER_CMD = "user_cmd";

    //private static final String START_SPEECHRECOGNIZER_OBS_PROP = "start_speech_recognizer";
    //private static boolean speech_recognizer_active = false;

    private MessageDialog feedbackDialog = null;
    private MessageDialog prehDataRetrievalDialog = null;
    private PrehImportDialog prehImportDialog;

    private TraumaInfoDialog traumaInfoDialog = null;
    private TraumaTerminationCodesWarningDialog terminationCodesWarningsDialog = null;
    private TraumaTerminationDialog traumaTerminationDialog = null;

    private Menu optionsMenu;

    public void init() {
        super.init(MainActivity.class, R.layout.activity_main, R.menu.activity_main_menu, true);
    }

    @INTERNAL_OPERATION
    protected void setup() {
        initUI();

        keepScreenOn();
        disableBackButton();

        bindOnCreateOptionsMenu("onCreateOptionsMenu");
        bindOnOptionsItemSelectedToOp("onOptionsItemSelected");
    }

    private void initUI() {
        initTabs();
        initBarButtons();
        initGeneralButtons();

        setUIText(R.id.traumaLeader_label, ActiveTrauma.traumaTeam().currentTraumaLeader().toString(), false);
    }

    private void initTabs(){
        final View[] tabsRes = new View[]{
                findUIElement(R.id.tab_drugs),
                findUIElement(R.id.tab_blood_products),
                findUIElement(R.id.tab_drug_continuous_inf),
                findUIElement(R.id.tab_procedure),
                findUIElement(R.id.tab_dignostics)
        };

        final TraumaTrackerPager tabs = (TraumaTrackerPager) findUIElement(R.id.container);

        execute(() -> {
            tabs.setAdapter(new FragmentTabsAdapter(getWrappedActivity().getSupportFragmentManager()));
            tabs.addOnPageChangeListener(new TabsChangeListener() {
                @Override
                public void onPageSelected(final int position) {
                    tabs.changeCurrentPage(tabsRes, position);
                }
            });

            tabs.setCurrentItem(0);
            tabs.changeCurrentPage(tabsRes, 0);
        });

        for(int i = 0; i < tabsRes.length; i++){
            final int position = i;
            tabsRes[i].setOnClickListener(v -> tabs.setCurrentItem(position));
        }
    }

    private void initBarButtons(){
        findUIElement(R.id.buttonPlaceUpdate).setOnClickListener(v -> {

            if(!ActiveTrauma.isPatientAcceptedInER()){
                showPatientNotInERWarningDialog("Impossibile modificare la stanza finché non si registra l'accesso del paziente in PS.");
                return;
            }

            new TraumaPathDialog(getActivityContext(), placeManualUpdated ->
                    externalSignal(USER_CMD, "change_room", placeManualUpdated)).show();
        });

        findUIElement(R.id.buttonTraumaTeam).setOnClickListener(v ->
                new TraumaTeamDialog(getActivityContext(), traumaLeader ->
                        externalSignal("user_changed", traumaLeader)).show());

        findUIElement(R.id.buttonTimeSheet).setOnClickListener(v -> new ReportOverviewDialog(getActivityContext()).showFullscreen());

        findUIElement(R.id.terminateTrauma).setOnClickListener(v -> {
            if(ActiveTrauma.getTraumaInfo(TraumaInfoElement.CODE).isEmpty()){
                showCodeAbsentsWarningDialog(ActiveTrauma.getTraumaInfo(TraumaInfoElement.CODE), ActiveTrauma.getTraumaInfo(TraumaInfoElement.SDO));
            } else {
                showFinalDestinationDialog();
            }
        });
    }

    private void initGeneralButtons(){
        //findUIElement(R.id.prehButton).setOnClickListener(v -> new PrehDialog(getActivityContext()).show());

        findUIElement(R.id.patientDataButton).setOnClickListener(v -> {
            traumaInfoDialog = new TraumaInfoDialog(getActivityContext(), new TraumaInfoDialog.Listener() {
                @Override
                public void newBarcodeRequest(final String barcodeType) {
                    externalSignal("barcode_request", barcodeType);
                }

                @Override
                public void retrievePrehMissions() {
                    externalSignal("retrieve_preh_missions");
                }
            });

            traumaInfoDialog.show();
        });

        findUIElement(R.id.initialVitalSignsButton).setOnClickListener(v -> {
            if(!ActiveTrauma.isPatientAcceptedInER()){
                showPatientNotInERWarningDialog(getStringFromResource(R.string.patient_not_accessed_er_warning_patient_initial_condition));
                return;
            }

            new PatientInitialStatusDialog(getActivityContext()).show();
        });

        findUIElement(R.id.clinicalPictureVariation).setOnClickListener(v -> {
            if(!ActiveTrauma.isPatientAcceptedInER()){
                showPatientNotInERWarningDialog(getStringFromResource(R.string.patient_not_accessed_er_warning_patient_clinical_picture_variations));
                return;
            }

            new ClinicalPictureVariationDialog(getActivityContext()).show();
        });
        findUIElement(R.id.multimediaNotesButton).setOnClickListener(v ->
                new MultimediaNotesDialog(getActivityContext(), this::externalSignal).show());

        findUIElement(R.id.patientAccessButton).setOnClickListener(v -> new MessageDialogWithChoice(getActivityContext(),
                getStringFromResource(R.string.warning), getStringFromResource(R.string.patient_access_er_alert_message),
                getStringFromResource(R.string.pid_warning_message),
                new MessageDialogWithChoice.Listener() {
                    @Override
                    public void onNoButtonClicked() {
                        //Nothing to do!
                    }

                    @Override
                    public void onYesButtonClicked() {
                        findUIElement(R.id.patientAccessButton).setVisibility(View.GONE);
                        findUIElement(R.id.clinicalPictureVariation).setVisibility(View.VISIBLE);
                        findUIElement(R.id.initialVitalSignsButton).setVisibility(View.VISIBLE);
                        findUIElement(R.id.alsButton).setVisibility(View.VISIBLE);

                        ActiveTrauma.registerPatientAccessToER();
                        ActiveTrauma.notifyTraumaCurrentStatus("active");
                    }
                }).show());

        findUIElement(R.id.alsButton).setOnClickListener(v -> {
            final Button b = (Button) v;

            if(b.getText().toString().equals(getStringFromResource(R.string.startAlsButton_text))){
                //START ALS
                new ProcedureConfirmationDialog(getActivityContext(), Procedure.ALS, C.report.events.procedure.event.START, optionsMap -> {
                    ActiveTrauma.registerNewTimeDependentProcedure(Procedure.ALS, C.report.events.procedure.event.START, optionsMap);
                    b.setText(getStringFromResource(R.string.stopAlsButton_text));
                    b.setBackgroundResource(R.drawable.traumatracker_button_bg_red);
                    DrugsFragment.toggleAlsDrugs(true);
                }).show();
            } else {
                //STOP ALS
                new ProcedureConfirmationDialog(getActivityContext(), Procedure.ALS, C.report.events.procedure.event.END, optionsMap -> {
                    ActiveTrauma.registerNewTimeDependentProcedure(Procedure.ALS, C.report.events.procedure.event.END, optionsMap);
                    b.setText(getStringFromResource(R.string.startAlsButton_text));
                    b.setBackgroundResource(R.drawable.traumatracker_button);
                    DrugsFragment.toggleAlsDrugs(false);
                }).show();
            }
        });

        if(ActiveTrauma.isPatientAcceptedInER()){
            findUIElement(R.id.alsButton).setVisibility(View.VISIBLE);
            findUIElement(R.id.clinicalPictureVariation).setVisibility(View.VISIBLE);
            findUIElement(R.id.initialVitalSignsButton).setVisibility(View.VISIBLE);
            findUIElement(R.id.patientAccessButton).setVisibility(View.GONE);
        } else {
            findUIElement(R.id.alsButton).setVisibility(View.GONE);
            findUIElement(R.id.clinicalPictureVariation).setVisibility(View.GONE);
            findUIElement(R.id.initialVitalSignsButton).setVisibility(View.GONE);
            findUIElement(R.id.patientAccessButton).setVisibility(View.VISIBLE);
        }
    }

    /*
    @OPERATION
    public void configureConnections() {
        speech_recognizer_active = checkSpeechRecognizerActivation();
    }
    */

    @OPERATION public void onVitalSignsServiceStatusChange(final String vsServiceStatus){
        if(vsServiceStatus.equals(VitalSignsService.Status.UNREACHABLE.value())){
            execute(() -> optionsMenu.findItem(R.id.activity_main_menu_vs_service).setIcon(R.drawable.icon_monitor_gray));
        } else if(vsServiceStatus.equals(VitalSignsService.Status.IDLE.value())){
            execute(() -> optionsMenu.findItem(R.id.activity_main_menu_vs_service).setIcon(R.drawable.icon_monitor_yellow));
        } else if(vsServiceStatus.equals(VitalSignsService.Status.ACTIVE.value())){
            execute(() -> optionsMenu.findItem(R.id.activity_main_menu_vs_service).setIcon(R.drawable.icon_monitor_green));
        }
    }

    @OPERATION public void onLocationServiceStatusChange(final String locationServiceStatus){
        if(locationServiceStatus.equals(LocationDetector.Status.UNREACHABLE.value())) {
            execute(() -> optionsMenu.findItem(R.id.activity_main_menu_location_service).setIcon(R.drawable.icon_place_grey));
        } else if(locationServiceStatus.equals(LocationDetector.Status.IDLE.value())){
            execute(() -> optionsMenu.findItem(R.id.activity_main_menu_location_service).setIcon(R.drawable.icon_place_yellow));
        } else if(locationServiceStatus.equals(LocationDetector.Status.ACTIVE.value())){
            execute(() -> optionsMenu.findItem(R.id.activity_main_menu_location_service).setIcon(R.drawable.icon_place_green));
        }
    }

    @OPERATION public void onEnvironmentSetupDone(String prevReportId){
        if(prevReportId.isEmpty()){
            //It's a new tracking session!
            signal(USER_CMD, "start_tracking",
                    ActiveTrauma.isDelayedActivation() ? C.place.SHOCK_ROOM : C.place.PREH,
                    ActiveTrauma.traumaTeam().currentTraumaLeader(),
                    ActiveTrauma.delayedActivation(),
                    ActiveTrauma.delayedActivationDate(),
                    ActiveTrauma.delayedActivationTime());
        } else {
            //It's a previous report reactivation!
            signal(USER_CMD, "restart_tracking", prevReportId);
        }
    }

    @OPERATION public boolean onCreateOptionsMenu(Menu menu){
        optionsMenu = menu;

        if(menu != null){
            execute(() -> {
                menu.findItem(R.id.activity_main_menu_vs_service).setTitle(ActiveTrauma.patientId());
                menu.findItem(R.id.activity_main_menu_location_service).setTitle(readSharedPreference(Preferences.ID, Preferences.BLE_TAG_ID, C.url.DEFAULT_BLE_TAG_ID));
            });
        }

        return true;
    }

    @OPERATION public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activity_main_menu_action_lock:
                execute(() -> new LockDialog(getActivityContext(), getStringFromResource(R.string.lock_dialog_message)).showFullscreen());
                return true;

            case R.id.activity_main_menu_vs_service:
                return true;

            case R.id.activity_main_menu_location_service:
                return true;
        }

        return false;
    }

    private void showPatientNotInERWarningDialog(String message){
        new MessageDialog(getActivityContext(), getStringFromResource(R.string.warning), message, null).show();
    }

    private void showCodeAbsentsWarningDialog(String patientCode, String patientSdo){

        terminationCodesWarningsDialog = new TraumaTerminationCodesWarningDialog(getActivityContext(), patientCode, patientSdo, new TraumaTerminationCodesWarningDialog.Listener() {
            @Override
            public void onBackButtonClicked() {
                //nothing to do!
            }

            @Override
            public void newBarcodeRequest(String type) {
                externalSignal("barcode_request", type);
            }

            @Override
            public void onConfirmationButtonClicked(String patientCode, String patientSDO, boolean erDeceasedPatient) {
                ActiveTrauma.updatePatientInfo(TraumaInfoElement.CODE, patientCode);
                ActiveTrauma.updatePatientInfo(TraumaInfoElement.SDO, patientSDO);

                if(erDeceasedPatient){
                    ActiveTrauma.updatePatientInfo(TraumaInfoElement.ER_DECEASED, getStringFromResource(R.string.switch_yes_label).toLowerCase());
                }

                showFinalDestinationDialog();
            }

            @Override
            public void onExitWithoutSaving() {
                externalSignal(USER_CMD, "end_tracking");
                dispose();
            }
        });

        terminationCodesWarningsDialog.show();
    }

    private void showFinalDestinationDialog(){
        traumaTerminationDialog = new TraumaTerminationDialog(getActivityContext(), new TraumaTerminationDialog.Listener() {
            @Override
            public void newBarcodeRequest(String type) {
                externalSignal("barcode_request", type);
            }

            @Override
            public void onExitWithSaving(final String dest, final ISS iss) {
                try {
                    externalSignal(USER_CMD, "end_tracking", dest, iss.toJSONRepresentation().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onExitWithoutSaving() {
                externalSignal(USER_CMD, "end_tracking");

                dispose();
            }
        });

        traumaTerminationDialog.show();
    }

    @OPERATION public void waitForServerFeedback(){
        execute(() -> {
            feedbackDialog = new MessageDialog(getActivityContext(),
                    getStringFromResource(R.string.report_sending_title),
                    getStringFromResource(R.string.report_sending_message),
                    this::dispose);

            feedbackDialog.toggleButtonVisibility(false);
            feedbackDialog.show();
        });
    }

    @OPERATION public void onServerFeedback(final int res){
        execute(() -> {
            feedbackDialog.changeTitle(res == 200
                    ? getStringFromResource(R.string.success)
                    : getStringFromResource(R.string.warning));
            feedbackDialog.changeMessage(res == 200
                    ? getStringFromResource(R.string.report_sended_ok)
                    : getStringFromResource(R.string.report_sended_no));
            feedbackDialog.toggleButtonVisibility(true);
        });
    }

    @OPERATION public void waitForPrehDataRetrieval(){
        execute(() -> {
            prehDataRetrievalDialog = new MessageDialog(getActivityContext(),
                    getStringFromResource(R.string.preh_retrieval_waiting_title),
                    getStringFromResource(R.string.preh_retrieval_waiting_message),
                    null);

            prehDataRetrievalDialog.toggleButtonVisibility(false);
            prehDataRetrievalDialog.show();
        });
    }

    @OPERATION public void onPreHMissionsListReceived(int res, JSONArray list){
        execute(() -> {
            if(res == 200){
                prehDataRetrievalDialog.dismiss();

                prehImportDialog = new PrehImportDialog(getActivityContext(), list, new PrehImportDialog.Listener() {
                    @Override
                    public void onImportMission(String missionId) {
                        prehImportDialog.dismiss();
                        externalSignal("retrieve_preh_mission_data", missionId);
                    }
                });

                prehImportDialog.show();
            } else {
                prehDataRetrievalDialog.changeTitle(getStringFromResource(R.string.warning));
                prehDataRetrievalDialog.changeMessage(getStringFromResource(R.string.preh_retrieval_error_response));
                prehDataRetrievalDialog.toggleButtonVisibility(true);
            }
        });
    }

    @OPERATION public void onPreHMissionDataReceived(int res, JSONObject patient, JSONObject vs){
        execute(() -> {
            if(res == 200){
                prehDataRetrievalDialog.dismiss();
                traumaInfoDialog.updatePreHInfo(patient, vs);
            } else {
                prehDataRetrievalDialog.changeTitle(getStringFromResource(R.string.warning));
                prehDataRetrievalDialog.changeMessage(getStringFromResource(R.string.preh_retrieval_error_response));
                prehDataRetrievalDialog.toggleButtonVisibility(true);
            }
        });
    }

    @OPERATION public void updateLocationOnUIStatusBar(final String place) {
        setUIText(R.id.location_label, place, false);
    }

    @OPERATION public void updateBarcodeOnUI(final String request, final String barcodeContent) {
        switch (request){
            case BarcodeReader.Requests.PATIENT_CODE:
                execute(() -> {
                    if (traumaInfoDialog != null) {
                        traumaInfoDialog.updatePatientCode(barcodeContent);
                    }
                });
                break;

            case BarcodeReader.Requests.PATIENT_SDO:
                execute(() -> {
                    if (traumaInfoDialog != null) {
                        traumaInfoDialog.updatePatientSdo(barcodeContent);
                    }
                });
                break;

            case BarcodeReader.Requests.PATIENT_CODE_FINAL:
                execute(() -> {
                    if (terminationCodesWarningsDialog != null) {
                        terminationCodesWarningsDialog.updatePatientCode(barcodeContent);
                    }
                });
                break;

            case BarcodeReader.Requests.PATIENT_SDO_FINAL:
                execute(() -> {
                    if (terminationCodesWarningsDialog != null) {
                        terminationCodesWarningsDialog.updatePatientSdo(barcodeContent);
                    }
                });
                break;

            case BarcodeReader.Requests.PATIENT_SDO_BY_DESTINATION:
                execute(() -> {
                    if (traumaTerminationDialog != null) {
                        traumaTerminationDialog.updatePatientSdo(barcodeContent);
                    }
                });
                break;

            case BarcodeReader.Requests.BLOOD_BAG_CODE:
                execute(() -> BloodProductsFragment.updateBloodBagCode(barcodeContent));
                break;
        }
    }

    @OPERATION public void updateTraumaLeaderInfoOnUIStatusBar(User user) {
        setUIText(R.id.traumaLeader_label, user.toString(), false);
    }

    /*
    private boolean checkSpeechRecognizerActivation() {
        if (readSharedPreference(Preferences.ID, Preferences.VOICE, false)) {
            beginExternalSession();
            defineObsProperty(START_SPEECHRECOGNIZER_OBS_PROP);
            endExternalSession(true);
            return true;
        }

        return false;
    }
    */

    private void externalSignal(String type, Object... objects){
        beginExternalSession();
        signal(type, objects);
        endExternalSession(true);
    }
}
