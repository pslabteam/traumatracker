package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.UserRole;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTime;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class StartTraumaDialog extends TTDialog {
    private List<User> users;

    private DateTime currentDealyedActivationDateTime = new DateTime();

    public StartTraumaDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_start_trauma);

        try {
            users = FileSystem.getUserList(UserRole.DIRECTOR, UserRole.MEMBER);

            initUI(listener, users.isEmpty());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initUI(Listener listener, boolean emptyList){
        final Spinner tlSpinner = initTraumaLeaderSpinner();

        final EditText delayedActivationDateTimeEditText = findViewById(R.id.dalayedActivationDateTimeEditText);

        findViewById(R.id.tl_spinner_icon).setOnClickListener(v -> tlSpinner.performClick());

        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.startTraumaButton).setOnClickListener(v -> {
            final boolean delayedActivation = ((CheckBox) findViewById(R.id.delayedActivationCheckBox)).isChecked();

            String date = "", time = "";

            if(delayedActivation){
                final String datetime = delayedActivationDateTimeEditText.getText().toString();

                if(datetime.isEmpty()){
                    new MessageDialog(getContext(),
                            getString(R.string.warning),
                            "In caso di attivazione ritardata va obbligatoriamente specificato il dato temporale sulla prima attivazione",null)
                        .show();
                    return;
                }

                final DateTime delayedActivationDateTime = DateTimeUtils.createDateTime(datetime);

                date = !delayedActivationDateTime.isDateInit() ? "" : DateTimeUtils.formatDate(delayedActivationDateTime.year(), delayedActivationDateTime.month(), delayedActivationDateTime.day());
                time = !delayedActivationDateTime.isTimeInit() ? "" : DateTimeUtils.formatTime(delayedActivationDateTime.hour(), delayedActivationDateTime.minute(), delayedActivationDateTime.second());
            }

            final String pid = ((RadioButton) findViewById(((RadioGroup) findViewById(R.id.vsService_switch)).getCheckedRadioButtonId())).getText().toString();

            final User traumaLeader = (User) tlSpinner.getSelectedItem();

            try {
                listener.onStartTrauma(traumaLeader, new JSONObject()
                                                        .put("delayed-activation", delayedActivation)
                                                        .put("delayed-activation-date", date)
                                                        .put("delayed-activation-time", time)
                                                        .put("patient-id", pid));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            dismiss();
        });

        findViewById(R.id.delayedActivationCheckBox).setOnClickListener(v -> {
            if(((CheckBox) v).isChecked()){
                findViewById(R.id.delayedActivationDateTimeContainer).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.delayedActivationDateTimeContainer).setVisibility(View.GONE);
            }
        });

        delayedActivationDateTimeEditText.setOnClickListener(v -> new MessageDialogDateTime(getContext(), "Data e Ora della prima attivazione", currentDealyedActivationDateTime, new MessageDialogDateTime.Listener() {
            @Override
            public void onNoButtonClicked() {
                //nothing to do!
            }

            @Override
            public void onYesButtonClicked(DateTime dt) {
                currentDealyedActivationDateTime = dt;
                delayedActivationDateTimeEditText.setText(dt.toString());
            }
        }).show());

        findViewById(R.id.delayedActivationDateTimeContainer).setVisibility(View.GONE);

        findViewById(R.id.startTraumaButton).setEnabled(false);

        findViewById(R.id.noUsersLabel).setVisibility(emptyList ? View.VISIBLE : View.GONE);
    }

    public interface Listener {
        void onStartTrauma(final User traumaLeader, final JSONObject details);// final boolean delayedActivation, final String delayedActivationDate, final String delayedActivationTime);
    }

    private Spinner initTraumaLeaderSpinner(){
        final String EMPTY_USER_ID = "empty";

        final Spinner spinner = findViewById(R.id.tl_spinner);

        users.add(0, new User(EMPTY_USER_ID, getString(R.string.unselected_user_text), "", ""));

        ArrayAdapter<User> spinnerAdapter = new ArrayAdapter<>(getContext(), R.layout.traumatracker_spinner_item, users);
        spinnerAdapter.setDropDownViewResource(R.layout.traumatracker_spinner_dropdown);

        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final User selectedTraumaLeader = (User) parent.getAdapter().getItem(position);
                findViewById(R.id.startTraumaButton).setEnabled(!selectedTraumaLeader.id().equals(EMPTY_USER_ID));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        return spinner;
    }
}
