package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.BloodProduct;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.BloodProductConfirmationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.templates.TraumaTrackerButtonsFragment;

public class BloodProductsFragment extends TraumaTrackerButtonsFragment {

    public BloodProductsFragment() {}

    public static BloodProductsFragment newInstance() {
        return new BloodProductsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_blood_products, container, false);

        initUI(mainView);

        return mainView;
    }

    private void initUI(View mainView){
        List<Button> buttonsList = new ArrayList<>();

        for (int i = 0; i < BloodProduct.values().length; i++) {
            final BloodProduct bp = BloodProduct.values()[i];

            final int bgRes = R.drawable.traumatracker_button_bg_red;

            buttonsList.add(buildButton(getContext(), bp.toString(), bgRes, v -> {
                    if(!ActiveTrauma.isPatientAcceptedInER()){
                        new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.patient_not_arrived_alert_message), null).show();
                        return;
                    }

                    new BloodProductConfirmationDialog(getActivity(), bp, new BloodProductConfirmationDialog.Listener() {
                        @Override
                        public void newBarcodeRequest() {
                            ActiveTrauma.requestBloodBagCode();
                        }

                        @Override
                        public void onCloseButtonClicked() {
                            ActiveTrauma.registerNewBloodProduct(bp);
                        }

                        @Override
                        public void onCloseButtonClicked(double qty) {
                            ActiveTrauma.registerNewBloodProduct(bp, qty);
                        }

                        @Override
                        public void onCloseButtonClicked(double qty, String bagCode) {
                            ActiveTrauma.registerNewBloodProduct(bp, qty, bagCode);

                        }
                    }).show();
            }));
        }

        buildButtonsList(mainView, buttonsList);
    }

    public static void updateBloodBagCode(String value){
        BloodProductConfirmationDialog.updateRequestedBloodBagCode(value);
    }
}
