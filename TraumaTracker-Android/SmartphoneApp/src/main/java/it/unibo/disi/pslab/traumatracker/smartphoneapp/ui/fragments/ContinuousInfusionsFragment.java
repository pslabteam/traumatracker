package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Infusion;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.DrugCategory;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.ContinuousInfusionDrugsAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialogWithAnswer;

public class ContinuousInfusionsFragment extends Fragment {

    private ContinuousInfusionDrugsAdapter adapter;

    private static boolean firstLoad;
    private static int genericDrugsCnt;

    private List<Infusion> list = new ArrayList<>();

    private ListView infusionsListView;

    public ContinuousInfusionsFragment() {
        firstLoad = true;
        genericDrugsCnt = 0;
    }

    public static ContinuousInfusionsFragment newInstance() {
        return new ContinuousInfusionsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_drugs_continuous_infusion, container, false);

        List<DrugCategory> toSkipList = Arrays.asList(DrugCategory.INFUSIONS, DrugCategory.GENERAL_DRUGS,
                DrugCategory.ALS_DRUGS, DrugCategory.ANTIBIOTIC_PROPHYLAXIS);

        if (firstLoad) {
            for (DrugCategory dc : DrugCategory.values()) {
                if (toSkipList.contains(dc)) continue;

                for (int i = 0; i < Drug.values().length; i++) {

                    Drug d = Drug.values()[i];

                    if(d.equals(Drug.GENERIC_DRUG_CONTINUOUS_INF_1)
                            || d.equals(Drug.GENERIC_DRUG_CONTINUOUS_INF_2)
                            || d.equals(Drug.GENERIC_DRUG_CONTINUOUS_INF_3)
                            || d.equals(Drug.GENERIC_DRUG_CONTINUOUS_INF_4))
                        continue;

                    if (d.category().equals(dc)) {
                        list.add(new Infusion(d, Infusion.Status.STOPPED));
                    }
                }
            }

            new Timer(1000).start();

            firstLoad = false;
        }

        adapter = new ContinuousInfusionDrugsAdapter(getActivity(), list);
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
            }
        });

        infusionsListView = mainView.findViewById(R.id.drugs_list);
        infusionsListView.setAdapter(adapter);
        infusionsListView.setFooterDividersEnabled(true);
        infusionsListView.addFooterView(createNewDrugButton());

        return mainView;
    }

    private Button createNewDrugButton(){
        final Button btn = new Button(getActivity());
        btn.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
        btn.setBackgroundResource(R.drawable.traumatracker_button);
        btn.setText(String.format("%s\n(%d %s)", getString(R.string.generic_drug_button_text), Drug.GENERIC_DRUGS_MAX_AVAILABILITY - genericDrugsCnt, getString(R.string.generic_drug_button_text_2)));
        btn.setAllCaps(true);

        btn.setOnClickListener(v -> {
            if(genericDrugsCnt >= Drug.GENERIC_DRUGS_MAX_AVAILABILITY) {
                new MessageDialog(getActivity(), getString(R.string.warning),
                        getString(R.string.generic_infusions_drugs_error_message), null)
                        .show();
                return;
            }

            new MessageDialogWithAnswer(getActivity(), getString(R.string.generic_drug_button_text), getString(R.string.generic_drug_title_message), new MessageDialogWithAnswer.Listener() {
                @Override
                public void onNoButtonClicked() {
                    //Nothing to do!
                }

                @Override
                public void onYesButtonClicked(String answer) {
                    Drug[] genericDrugsList = new Drug[]{Drug.GENERIC_DRUG_CONTINUOUS_INF_1, Drug.GENERIC_DRUG_CONTINUOUS_INF_2,
                            Drug.GENERIC_DRUG_CONTINUOUS_INF_3, Drug.GENERIC_DRUG_CONTINUOUS_INF_4};

                    if(genericDrugsCnt < genericDrugsList.length){
                        list.add(new Infusion(genericDrugsList[genericDrugsCnt], answer, Infusion.Status.STOPPED));
                        adapter.notifyDataSetChanged();

                        genericDrugsCnt++;

                        btn.setText(String.format("%s\n(%d %s)",
                                getString(R.string.generic_drug_message_title),
                                Drug.GENERIC_DRUGS_MAX_AVAILABILITY - genericDrugsCnt,
                                getString(R.string.generic_drug_button_text_2)));

                        infusionsListView.post(() -> infusionsListView.setSelection(infusionsListView.getCount() - 1));
                    }
                }
            }).show();
        });

        return btn;
    }

    class Timer extends Thread{

        private final int interval;
        private boolean stop = false;

        Timer(int interval){
            this.interval = interval;
        }

        @Override
        public void run() {
            while(!stop){
                try {
                    sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(getActivity() == null){
                    stop = true;
                    return;
                }

                boolean modification = false;

                for(Infusion i: list){
                    if(i.status().equals(Infusion.Status.STARTED)){
                        i.duration(i.duration() + 1);
                        modification = true;
                    }
                }

                if(modification){
                    getActivity().runOnUiThread(() -> adapter.notifyDataSetChanged());
                }
            }
        }
    }
}
