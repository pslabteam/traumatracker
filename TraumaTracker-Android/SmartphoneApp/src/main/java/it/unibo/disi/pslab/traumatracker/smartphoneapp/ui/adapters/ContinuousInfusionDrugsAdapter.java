package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import android.support.annotation.NonNull;

import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Infusion;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.DrugConfirmationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialogWithChoice;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class ContinuousInfusionDrugsAdapter extends ArrayAdapter<Infusion> {

    private Context ctx;

    private static class ViewHolder {
        TextView status, name, start, duration, dosage;
        Button startbtn, variationBtn, stopBtn;
    }

    public ContinuousInfusionDrugsAdapter(Context context, List<Infusion> data) {
        super(context, R.layout.item_continuous_infusions, data);
        this.ctx = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull final ViewGroup parent) {
        final Infusion infusion = getItem(position);

        final ViewHolder vh;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_continuous_infusions, parent, false);

            vh = new ViewHolder();
            vh.status = convertView.findViewById(R.id.infusion_status);
            vh.name = convertView.findViewById(R.id.drug_name);
            vh.start = convertView.findViewById(R.id.infusion_start_date);
            vh.duration = convertView.findViewById(R.id.infusion_duration);
            vh.dosage = convertView.findViewById(R.id.infusion_dosage);
            vh.startbtn = convertView.findViewById(R.id.infusion_start_button);
            vh.variationBtn = convertView.findViewById(R.id.infusion_variation_button);
            vh.stopBtn = convertView.findViewById(R.id.infusion_stop_button);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        initItem(vh, infusion);

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    private void initItem(final ViewHolder vh, final Infusion infusion){
        vh.name.setText(infusion.drug().toString());

        vh.startbtn.setOnClickListener(v -> {
            if(!ActiveTrauma.isPatientAcceptedInER()){
                new MessageDialog(getContext(), ctx.getString(R.string.warning), ctx.getString(R.string.patient_not_arrived_alert_message), null).show();
                return;
            }

            new DrugConfirmationDialog(ctx, infusion.drug(), new DrugConfirmationDialog.Listener() {
                @Override
                public void onCloseButtonClicked() {
                    //nothing to do!
                }

                @Override
                public void onCloseButtonClicked(final double value) {
                    infusion.status(Infusion.Status.STARTED);
                    infusion.startDateTime(DateTimeUtils.getCurrentDate() + " " + DateTimeUtils.getCurrentTime());
                    infusion.duration(0);
                    infusion.dosage(value);

                    ActiveTrauma.registerStartContinuousInfusionDrug(infusion.drug(), infusion.dosage());

                    notifyInfusionStarted(vh, infusion);
                }
            }).show();
        });

        vh.variationBtn.setOnClickListener(v -> new DrugConfirmationDialog(ctx, infusion.drug(), new DrugConfirmationDialog.Listener() {
                @Override
                public void onCloseButtonClicked() {
                    //nothing to do!
                }

                @Override
                public void onCloseButtonClicked(final double value) {
                    infusion.dosage(value);

                    ActiveTrauma.registerVariationContinuousInfusionDrug(infusion.drug(), infusion.dosage());

                    notifyInfusionDosageVariation(vh, infusion);
                }
            }).show());

        vh.stopBtn.setOnClickListener(v -> new MessageDialogWithChoice(ctx, ctx.getResources().getString(R.string.warning),
                String.format("%s %s", ctx.getString(R.string.infusion_stop_message), infusion.drug().toString()),
                new MessageDialogWithChoice.Listener() {
                    @Override
                    public void onNoButtonClicked() {
                        //Nothing to do!
                    }

                    @Override
                    public void onYesButtonClicked() {
                        infusion.status(Infusion.Status.STOPPED);

                        ActiveTrauma.registerStopContinuousInfusionDrug(infusion.drug(), infusion.duration());

                        notifyInfusionStopped(vh);
                    }
                }).show());

        if (infusion.status().equals(Infusion.Status.STARTED)) {
            notifyInfusionStarted(vh, infusion);
        } else {
            notifyInfusionStopped(vh);
        }
    }

    private void notifyInfusionStarted(final ViewHolder vh, final Infusion infusion){
        vh.status.setBackgroundResource(R.drawable.icon_infusion_green);

        vh.start.setText(String.format("%s %s", ctx.getString(R.string.infusion_start_label).toUpperCase(), infusion.startDateTime()));
        vh.duration.setText(String.format("%s %s", ctx.getString(R.string.infusion_duration_label).toUpperCase(), DateTimeUtils.convertSecondsToString(infusion.duration())));
        vh.dosage.setText(String.format("%s %s %s", ctx.getString(R.string.infusion_dosage_label).toUpperCase(), infusion.dosage(), infusion.drug().unit()));

        vh.startbtn.setEnabled(false);
        vh.variationBtn.setEnabled(true);
        vh.stopBtn.setEnabled(true);
    }

    private void notifyInfusionDosageVariation(final ViewHolder vh, final Infusion infusion){
        vh.dosage.setText(String.format("%s %s %s", ctx.getString(R.string.infusion_dosage_label).toUpperCase(), infusion.dosage(), infusion.drug().unit()));
    }

    private void notifyInfusionStopped(final ViewHolder vh){
        vh.status.setBackgroundResource(R.drawable.icon_infusion_red);

        vh.start.setText("");
        vh.duration.setText(String.format("%s",ctx.getString(R.string.infusion_stopped_label)));
        vh.dosage.setText("");

        vh.startbtn.setEnabled(true);
        vh.variationBtn.setEnabled(false);
        vh.stopBtn.setEnabled(false);
    }
}
