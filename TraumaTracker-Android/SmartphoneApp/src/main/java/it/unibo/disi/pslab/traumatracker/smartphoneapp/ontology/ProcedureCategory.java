package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import java.util.Comparator;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum ProcedureCategory {
    A("a", R.string.ontology_procedure_category_a, 1),
    B("b", R.string.ontology_procedure_category_b, 2),
    C("c", R.string.ontology_procedure_category_c, 3),
    OTHERS("others", R.string.ontology_procedure_category_others, 4),
    ALS("als", R.string.ontology_procedure_category_als, 5);

    private final int labelRes;
    private final int position;

    ProcedureCategory(@SuppressWarnings("unused") final String id, final int labelRes, final int position){
        this.labelRes = labelRes;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }

    public static class ResuscitationOperationCategoryComparator implements Comparator<ProcedureCategory> {
        public int compare(ProcedureCategory c1, ProcedureCategory c2) {
            return Integer.compare(c1.getPosition(), c2.getPosition());
        }
    }
}
