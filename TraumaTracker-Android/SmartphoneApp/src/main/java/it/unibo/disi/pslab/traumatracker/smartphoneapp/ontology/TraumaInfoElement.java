package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;


public enum TraumaInfoElement {
    CODE,
    SDO,
    ER_DECEASED,
    ADMISSION_CODE,
    NAME,
    SURNAME,
    GENDER,
    DOB,
    AGE,
    ACCIDENT_DATE,
    ACCIDENT_TIME,
    ACCIDENT_TYPE,
    VEHICLE,
    FROM_OTHER_EMERGENCY,
    OTHER_EMERGENCY
}
