package it.unibo.disi.pslab.traumatracker.smartphoneapp.utils;

import android.support.annotation.NonNull;

public class DateTime {

    private int year, month, day, hour, minute, second;

    private boolean dateInit, timeInit;

    public void setDate(int h, int m, int d){
        year = h;
        month = m;
        day = d;

        dateInit = true;
    }

    public void setTime(int h, int m, int s){
        hour = h;
        minute = m;
        second = s;

        timeInit = true;
    }

    public boolean isDateInit(){
        return dateInit;
    }

    public boolean isTimeInit(){
        return timeInit;
    }

    public int year(){
        return year;
    }

    public int month(){
        return month;
    }

    public int day(){
        return day;
    }

    public int hour(){
        return hour;
    }

    public int minute(){
        return minute;
    }

    public int second() {
        return second;
    }

    @NonNull
    @Override
    public String toString(){
        return DateTimeUtils.formatDate(year, month, day) + " " + DateTimeUtils.formatTime(hour, minute, second);
    }
}
