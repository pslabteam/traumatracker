package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;

public class Infusion {
    private Drug drug;

    private Status status;

    private String startDateTime;
    private int duration;
    private double dosage;

    public Infusion(Drug drug, Status status){
        this.drug = drug;
        this.status = status;
    }

    public Infusion(Drug drug, String optionalDescription, Status status){
        this.drug = drug;
        this.drug.setOptionalDescription(optionalDescription);
        this.status = status;
    }

    public Drug drug() {
        return drug;
    }

    public Status status() {
        return status;
    }

    public void status(Status status) {
        this.status = status;
    }

    public String startDateTime() {
        return startDateTime;
    }

    public void startDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public int duration() {
        return duration;
    }

    public void duration(int duration) {
        this.duration = duration;
    }

    public double dosage() {
        return dosage;
    }

    public void dosage(double dosage) {
        this.dosage = dosage;
    }

    public enum Status {
        STARTED, STOPPED
    }
}
