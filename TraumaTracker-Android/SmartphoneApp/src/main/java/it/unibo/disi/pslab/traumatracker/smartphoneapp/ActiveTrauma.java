package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.AnamnesiElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.BloodProduct;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.MajorTraumaCriteriaElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.TraumaInfoElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.PrehElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.tools.NetworkManager;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.MapUtils;

import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class ActiveTrauma extends JaCaArtifact {

    private static ActiveTrauma me;

    void init() {
        me = this;
    }

    private static TraumaTeam traumaTeam;
    private static boolean delayedActivation;
    private static String delayedActivationDate;
    private static String delayedActivationTime;
    private static String patientId;
    private static boolean patientArrived;

    private static boolean alsActive;

    private static Map<PrehElement, String> preh = new HashMap<>();
    private static Map<TraumaInfoElement, String> traumaInfo = new HashMap<>();
    private static Map<MajorTraumaCriteriaElement, Boolean> mtc = new HashMap<>();
    private static Map<AnamnesiElement, Boolean> anamnesi = new HashMap<>();
    private static Map<VitalSign, String> initialStatusMap = new HashMap<>();
    private static Map<VitalSign, String> variationsMap = new HashMap<>();

    public static void initialize(final User tl, final JSONObject details) throws JSONException {

        traumaTeam = new TraumaTeam(tl);

        delayedActivation = details.getBoolean("delayed-activation");
        delayedActivationDate = details.getString("delayed-activation-date");
        delayedActivationTime = details.getString("delayed-activation-time");
        patientId = details.getString("patient-id");
        patientArrived = details.getBoolean("delayed-activation");
        alsActive = false;

        for(PrehElement e : PrehElement.values()){
            updatePrehElement(e, "");
        }

        for (TraumaInfoElement e : TraumaInfoElement.values()) {
            updatePatientInfo(e, "");
        }

        for (MajorTraumaCriteriaElement e : MajorTraumaCriteriaElement.values()) {
            setMajorTraumaCriteriaElement(e, false);
        }

        for (AnamnesiElement e : AnamnesiElement.values()) {
            setAnamnesiElement(e, false);
        }

        for (VitalSign vs : VitalSign.values()) {
            setInitialVitalSign(vs, "");
        }
    }

    public static void initialize(User tl, boolean da, String daDate, String daTime, boolean accessed, List<String> tt, Map<VitalSign, String> vitalSignMap, Map<MajorTraumaCriteriaElement, Boolean> mtcMap, Map<AnamnesiElement, Boolean> anamnesiMap, Map<TraumaInfoElement, String> patientInfoMap, Map<PrehElement, String> prehMap){

        traumaTeam = new TraumaTeam(tl, tt);
        delayedActivation = da;
        delayedActivationDate = daDate;
        delayedActivationTime = daTime;
        patientArrived = accessed;
        alsActive = false;

        for(PrehElement e : PrehElement.values()){
            updatePrehElement(e, prehMap.get(e) != null ? prehMap.get(e) : "");
        }

        for (TraumaInfoElement e : TraumaInfoElement.values()) {
            updatePatientInfo(e, patientInfoMap.get(e) != null ? patientInfoMap.get(e) : "");
        }

        for (MajorTraumaCriteriaElement e : MajorTraumaCriteriaElement.values()) {
            setMajorTraumaCriteriaElement(e, mtcMap.get(e));
        }

        for (AnamnesiElement e : AnamnesiElement.values()) {
            setAnamnesiElement(e, anamnesiMap.get(e));
        }

        for (VitalSign vs : VitalSign.values()) {
            setInitialVitalSign(vs, vitalSignMap.get(vs) != null ? vitalSignMap.get(vs) : "");
        }
    }

    public static TraumaTeam traumaTeam(){
        return traumaTeam;
    }

    public static boolean delayedActivation(){
        return delayedActivation;
    }

    public static String patientId(){
        return patientId;
    }

    public static String delayedActivationDate(){
        return delayedActivationDate;
    }

    public static String delayedActivationTime(){
        return delayedActivationTime;
    }

    public static boolean isDelayedActivation() {
        return delayedActivation;
    }

    public static boolean isPatientAcceptedInER() {
        return patientArrived;
    }

    public static void registerPatientAccessToER(){
        patientArrived = true;
        externalSignal("user_cmd","patient_accessed_er");
        externalSignal("user_cmd", "change_room", "Shock-Room");
    }

    public static void setInitialVitalSign(VitalSign vs, String value) {
        initialStatusMap.put(vs, value);
    }

    public static String getInitialVitalSign(VitalSign vs) {
        return initialStatusMap.get(vs);
    }

    public static String getVariationVitalSign(VitalSign vs) {
        return variationsMap.get(vs);
    }

    public static void setMajorTraumaCriteriaElement(MajorTraumaCriteriaElement element, boolean value) {
        mtc.put(element, value);
    }

    public static boolean getMajorTraumaCriteriaElement(MajorTraumaCriteriaElement element) {
        return mtc.get(element);
    }

    public static void setAnamnesiElement(AnamnesiElement element, boolean value) {
        anamnesi.put(element, value);
    }

    public static boolean getAnamnesiElement(AnamnesiElement element) {
        return anamnesi.get(element);
    }

    public static void initializeVariationsMap() {
        variationsMap.putAll(initialStatusMap);
    }

    public static void updatePatientInfo(TraumaInfoElement element, String value) {
        traumaInfo.put(element, value);
    }

    public static String getTraumaInfo(TraumaInfoElement element) {
        if (!traumaInfo.containsKey(element)) {
            return "";
        }

        return traumaInfo.get(element);
    }

    public static void updatePrehElement(PrehElement element, String value) {
        preh.put(element, value);
    }

    public static String getPrehElement(PrehElement element) {
        if (!preh.containsKey(element)) {
            return "";
        }

        return preh.get(element);
    }

    public static void registerNewProcedure(Procedure op, Map<String, Object> optionsMap) {
        externalSignal("user_cmd", "new_procedure", op, MapUtils.serializeMap(optionsMap));

    }

    public static void registerNewTimeDependentProcedure(Procedure procedure, String eventType, Map<String, Object> optionsMap) {

        if(procedure.equals(Procedure.ALS)){
           switch(eventType){
               case C.report.events.procedure.event.START:
                   alsActive = true;
                   break;

               case C.report.events.procedure.event.END:
                   alsActive = false;
                   break;
           }
        }

        externalSignal("user_cmd", "new_time_dependent_procedure", procedure, eventType, MapUtils.serializeMap(optionsMap));
    }

    public static void registerNewDiagnostic(Diagnostics diagnostic, Map<String, Object> optionsMap) {
        /*switch (diagnostic){
            case TC_CEREBRAL_CERVICAL:
            case TC_THORACOABDOMINAL_MDC:
            case WB_TC:
                externalSignal("user_cmd", "change_room", "TAC PS");
                externalSignal("user_cmd", "new_diagnostic", diagnostic, MapUtils.serializeMap(optionsMap), "TAC PS");
                break;

            default:
                externalSignal("user_cmd", "new_diagnostic", diagnostic, MapUtils.serializeMap(optionsMap));
                break;
        }*/

        externalSignal("user_cmd", "new_diagnostic", diagnostic, MapUtils.serializeMap(optionsMap));
    }

    public static void registerNewDrug(Drug drug) {
        externalSignal("user_cmd", "new_drug", drug);
    }

    public static void registerNewDrug(Drug drug, double qty) {
        externalSignal("user_cmd", "new_drug", drug, qty);
    }

    public static void registerNewBloodProduct(BloodProduct bloodProduct) {
        externalSignal("user_cmd", "new_blood_product", bloodProduct);
    }

    public static void registerNewBloodProduct(BloodProduct bloodProduct, double qty) {
        externalSignal("user_cmd", "new_blood_product", bloodProduct, qty);
    }

    public static void registerNewBloodProduct(BloodProduct bloodProduct, double qty, String bagCode) {
        externalSignal("user_cmd", "new_blood_product", bloodProduct, qty, bagCode);
    }

    public static void registerStartContinuousInfusionDrug(Drug drug, double qty){
        externalSignal("user_cmd", "start_continuous_infusion_drug", drug, qty);
    }

    public static void registerVariationContinuousInfusionDrug(Drug drug, double qty){
        externalSignal("user_cmd", "variate_continuous_infusion_drug", drug, qty);
    }

    public static void registerStopContinuousInfusionDrug(Drug drug, int duration){
        externalSignal("user_cmd", "stop_continuous_infusion_drug", drug, duration);
    }

    public static void registerVitalSignVariation(VitalSign vs, String value) {
        if (!value.equals(variationsMap.get(vs))) {
            externalSignal("user_cmd", "vital_sign_variation", vs, value);
            variationsMap.put(vs, value);
        }
    }

    public static void registerVitalSignsFromMonitorNote(int sys, int dia, int hr, int etco2, int spo2, int temp){
        externalSignal("user_cmd", "vital_signs_monitor_status", sys, dia, hr, etco2, spo2, temp);
    }

    public static boolean isAlsActive(){
        return alsActive;
    }

    public static void requestBloodBagCode(){
        externalSignal("barcode_request", BarcodeReader.Requests.BLOOD_BAG_CODE);
    }

    private static void externalSignal(String type, Object... objects) {
        me.beginExternalSession();
        me.signal(type, objects);
        me.endExternalSession(true);
    }

    //--- SMART SHOCK ROOM (TEST) ----------------------

    private static final boolean SSR_TEST_ACTIVE = false;
    private static final String SSR_HTTP_PREFIX = "http://0c2898830c58.ngrok.io/api/trauma";

    private static String SSR_CURRENT_TRAUMA_ID;

    public static void notifyTraumaActivation(){
        ssrInvokeOnThread(() -> {
            try {
                ssrLog("called notifyTraumaActivation()");

                JSONObject content = new JSONObject();
                content.put(C.report.START_OPERATOR_ID, traumaTeam.user.id())
                    .put(C.report.START_OPERATOR_DESCRIPTION, traumaTeam.user.toString())
                    .put(C.report.DELAYED_ACTIVATION, new JSONObject().put(C.report.delayedActivation.STATUS, isDelayedActivation()))
                    .put(C.report.TRAUMA_TEAM_MEMBERS, new JSONArray(traumaTeam.team.toArray()));

                NetworkManager.doPOST(SSR_HTTP_PREFIX, content.toString(), (code, message, responseBody) -> {
                    ssrLog("...> " + code + "\n" + responseBody);

                    try {
                        JSONObject resObj = new JSONObject(responseBody.trim());
                        ssrLog(resObj.toString());
                        SSR_CURRENT_TRAUMA_ID = resObj.getString("_id");

                        ActiveTrauma.notifyTraumaCurrentStatus("arriving");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                });
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public static void notifyTraumaTeamUpdate() {
        ssrInvokeOnThread(() -> {
            try {
                ssrLog("called notifyTraumaTeamUpdate()");

                JSONObject content = new JSONObject();
                content.put(C.report.TRAUMA_TEAM_MEMBERS, new JSONArray(traumaTeam.team.toArray()));

                NetworkManager.doPUT(SSR_HTTP_PREFIX +"/" + SSR_CURRENT_TRAUMA_ID + "/trauma_team", content.toString(), (code, message, responseBody) -> {
                    ssrLog("...> " + code + "\n" + responseBody);
                });
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public static void notifyTraumaCurrentStatus(final String status){
        ssrInvokeOnThread(() -> {
            try {
                ssrLog("called notifyTraumaCurrentStatus()");

                JSONObject content = new JSONObject();
                content.put("trauma_current_status", status);

                NetworkManager.doPUT(SSR_HTTP_PREFIX +"/" + SSR_CURRENT_TRAUMA_ID + "/trauma_current_status", content.toString(), (code, message, responseBody) -> {
                    ssrLog("...> " + code + "\n" + responseBody);
                });
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public static void notifyPreH(){
        ssrInvokeOnThread(() -> {
            try {
                ssrLog("called notifyPreH()");

                JSONObject content = new JSONObject();
                content.put(C.report.PREH, new JSONObject()
                        .putOpt(C.report.preh.TERRITORIAL_AREA, ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA).isEmpty()
                                ? null
                                : ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA))
                        .putOpt(C.report.preh.CAR_ACCIDENT, ActiveTrauma.getPrehElement(PrehElement.CAR_ACCIDENT).isEmpty()
                                ? null
                                : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.CAR_ACCIDENT)))
                        .putOpt(C.report.preh.A_AIRWAYS, ActiveTrauma.getPrehElement(PrehElement.A).isEmpty()
                                ? null
                                : ActiveTrauma.getPrehElement(PrehElement.A))
                        .putOpt(C.report.preh.B_PLEURAL_DECOMPRESSION, ActiveTrauma.getPrehElement(PrehElement.B_PLEURAL_DECOMPRESSION).isEmpty()
                                ? null
                                : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.B_PLEURAL_DECOMPRESSION)))
                        .putOpt(C.report.preh.C_BLOOD_PROTOCOL, ActiveTrauma.getPrehElement(PrehElement.C_BLOOD_PROTOCOL).isEmpty()
                                ? null
                                : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.C_BLOOD_PROTOCOL)))
                        .putOpt(C.report.preh.C_TPOD, ActiveTrauma.getPrehElement(PrehElement.C_TPOD).isEmpty()
                                ? null
                                : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.C_TPOD)))
                        .putOpt(C.report.preh.D_GCS_TOTAL, ActiveTrauma.getPrehElement(PrehElement.D_GCS_TOTAL).isEmpty()
                                ? null
                                : Double.parseDouble(ActiveTrauma.getPrehElement(PrehElement.D_GCS_TOTAL)))
                        .putOpt(C.report.preh.D_ANISOCORIA, ActiveTrauma.getPrehElement(PrehElement.D_ANISOCORIA).isEmpty()
                                ? null
                                : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.D_ANISOCORIA)))
                        .putOpt(C.report.preh.D_MIDRIASI, ActiveTrauma.getPrehElement(PrehElement.D_MIDRIASI).isEmpty()
                                ? null
                                : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.D_MIDRIASI)))
                        .putOpt(C.report.preh.E_MOTILITY, ActiveTrauma.getPrehElement(PrehElement.E_PARA_TETRA_PARESI).isEmpty()
                                ? null
                                : convertYesNo(ActiveTrauma.getPrehElement(PrehElement.E_PARA_TETRA_PARESI)))
                        .putOpt(C.report.preh.WORST_BLOOD_PRESSURE, ActiveTrauma.getPrehElement(PrehElement.WROST_BLOOD_PRESSURE).isEmpty()
                                ? null
                                : Double.parseDouble(ActiveTrauma.getPrehElement(PrehElement.WROST_BLOOD_PRESSURE)))
                        .putOpt(C.report.preh.WORST_RESPIRATORY_RATE, ActiveTrauma.getPrehElement(PrehElement.WROST_RESPIRATORY_RATE).isEmpty()
                                ? null
                                : Double.parseDouble(ActiveTrauma.getPrehElement(PrehElement.WROST_RESPIRATORY_RATE))));

                NetworkManager.doPUT(SSR_HTTP_PREFIX +"/" + SSR_CURRENT_TRAUMA_ID + "/preH", content.toString(), (code, message, responseBody) -> {
                    ssrLog("...> " + code + "\n" + responseBody);
                });
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public static void notifyTraumaInfo(){
        ssrInvokeOnThread(() -> {
            try {
                ssrLog("called notifyTraumaInfo()");

                JSONObject content = new JSONObject();

                content.put(C.report.TRAUMA_INFO, new JSONObject()
                        .putOpt(C.report.traumaInfo.CODE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.CODE).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.CODE))
                        .putOpt(C.report.traumaInfo.SDO, ActiveTrauma.getTraumaInfo(TraumaInfoElement.SDO).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.SDO))
                        .putOpt(C.report.traumaInfo.ER_DECEASED, convertYesNo(ActiveTrauma.getTraumaInfo(TraumaInfoElement.ER_DECEASED)))
                        .putOpt(C.report.traumaInfo.ADMISSION_CODE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ADMISSION_CODE).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ADMISSION_CODE))
                        .putOpt(C.report.traumaInfo.NAME, ActiveTrauma.getTraumaInfo(TraumaInfoElement.NAME).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.NAME))
                        .putOpt(C.report.traumaInfo.SURNAME, ActiveTrauma.getTraumaInfo(TraumaInfoElement.SURNAME).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.SURNAME))
                        .putOpt(C.report.traumaInfo.GENDER, ActiveTrauma.getTraumaInfo(TraumaInfoElement.GENDER).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.GENDER))
                        .putOpt(C.report.traumaInfo.DOB, ActiveTrauma.getTraumaInfo(TraumaInfoElement.DOB).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.DOB))
                        .putOpt(C.report.traumaInfo.AGE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.AGE).isEmpty()
                                ? null
                                : Integer.parseInt(ActiveTrauma.getTraumaInfo(TraumaInfoElement.AGE)))
                        .putOpt(C.report.traumaInfo.ACCIDENT_DATE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_DATE).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_DATE))
                        .putOpt(C.report.traumaInfo.ACCIDENT_TIME, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TIME).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TIME))
                        .putOpt(C.report.traumaInfo.ACCIDENT_TYPE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TYPE).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.ACCIDENT_TYPE))
                        .putOpt(C.report.traumaInfo.VEHICLE, ActiveTrauma.getTraumaInfo(TraumaInfoElement.VEHICLE).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.VEHICLE))
                        .putOpt(C.report.traumaInfo.FROM_OTHER_EMERGENCY, convertYesNo(ActiveTrauma.getTraumaInfo(TraumaInfoElement.FROM_OTHER_EMERGENCY)))
                        .putOpt(C.report.traumaInfo.OTHER_EMERGENCY, ActiveTrauma.getTraumaInfo(TraumaInfoElement.OTHER_EMERGENCY).isEmpty()
                                ? null
                                : ActiveTrauma.getTraumaInfo(TraumaInfoElement.OTHER_EMERGENCY))
                        .put(C.report.MAJOR_TRAUMA_CRITERIA, new JSONObject()
                                .putOpt(C.report.majorTraumaCriteria.DYNAMIC, ActiveTrauma.getMajorTraumaCriteriaElement(MajorTraumaCriteriaElement.DYNAMIC))
                                .putOpt(C.report.majorTraumaCriteria.PHYSIOLOGICAL, ActiveTrauma.getMajorTraumaCriteriaElement(MajorTraumaCriteriaElement.PHYSIOLOGICAL))
                                .putOpt(C.report.majorTraumaCriteria.ANATOMICAL, ActiveTrauma.getMajorTraumaCriteriaElement(MajorTraumaCriteriaElement.ANATOMICAL)))
                        .put(C.report.ANAMNESI, new JSONObject()
                                .putOpt(C.report.anamnesi.ANTIPLATELETS, ActiveTrauma.getAnamnesiElement(AnamnesiElement.ANTIPLATELETS))
                                .putOpt(C.report.anamnesi.ANTICOAGULANTS, ActiveTrauma.getAnamnesiElement(AnamnesiElement.ANTICOAGULANTS))
                                .putOpt(C.report.anamnesi.NAO, ActiveTrauma.getAnamnesiElement(AnamnesiElement.NAO))));

                NetworkManager.doPUT(SSR_HTTP_PREFIX +"/" + SSR_CURRENT_TRAUMA_ID + "/trauma_info", content.toString(), (code, message, responseBody) -> {
                    ssrLog("...> " + code + "\n" + responseBody);
                });
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public static void notifyPatientInitialConditions(){
        ssrInvokeOnThread(() -> {
            try {
                ssrLog("called notifyPatientInitialConditions()");

                JSONObject content = new JSONObject();
                content.put(C.report.PATIENT_INITIAL_CONDITION, new JSONObject()
                        .put(C.report.patientInitialCondition.VITAL_SIGNS, new JSONObject()
                                .putOpt(C.report.patientInitialCondition.vitalSigns.TEMPERATURE, ActiveTrauma.getInitialVitalSign(VitalSign.TEMPERATURE).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.TEMPERATURE))
                                .putOpt(C.report.patientInitialCondition.vitalSigns.HEART_RATE, ActiveTrauma.getInitialVitalSign(VitalSign.HEART_RATE).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.HEART_RATE))
                                .putOpt(C.report.patientInitialCondition.vitalSigns.BLOOD_PRESSURE, ActiveTrauma.getInitialVitalSign(VitalSign.BLOOD_PRESSURE).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.BLOOD_PRESSURE))
                                .putOpt(C.report.patientInitialCondition.vitalSigns.SPO2, ActiveTrauma.getInitialVitalSign(VitalSign.SPO2).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.SPO2))
                                .putOpt(C.report.patientInitialCondition.vitalSigns.ETCO2, ActiveTrauma.getInitialVitalSign(VitalSign.ETCO2).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.ETCO2)))
                        .put(C.report.patientInitialCondition.CLINICAL_PICTURE, new JSONObject()
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_TOTAL, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_TOTAL).isEmpty()
                                        ? null
                                        : Double.parseDouble(ActiveTrauma.getInitialVitalSign(VitalSign.GCS_TOTAL)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_MOTOR, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_MOTOR).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.GCS_MOTOR))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_VERBAL, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_VERBAL).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.GCS_VERBAL))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.GCS_EYES, ActiveTrauma.getInitialVitalSign(VitalSign.GCS_EYES).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.GCS_EYES))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.SEDATED, ActiveTrauma.getInitialVitalSign(VitalSign.SEDATED).isEmpty()
                                        ? null
                                        : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.SEDATED)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.PUPILS, ActiveTrauma.getInitialVitalSign(VitalSign.PUPILS).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.PUPILS))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.AIRWAYS, ActiveTrauma.getInitialVitalSign(VitalSign.AIRWAYS).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.AIRWAYS))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.POSITIVE_INHALATION, ActiveTrauma.getInitialVitalSign(VitalSign.POSITIVE_INHALATION).isEmpty()
                                        ? null
                                        : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.POSITIVE_INHALATION)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.INTUBATION_FAILED, ActiveTrauma.getInitialVitalSign(VitalSign.INTUBATION_FAILED).isEmpty()
                                        ? null
                                        : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.INTUBATION_FAILED)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.CHEST_TUBE, ActiveTrauma.getInitialVitalSign(VitalSign.CHEST_TUBE).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.CHEST_TUBE))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.OXYGEN_PERCENTAGE, ActiveTrauma.getInitialVitalSign(VitalSign.OXYGEN_PERCENTAGE).isEmpty()
                                        ? null
                                        : Double.parseDouble(ActiveTrauma.getInitialVitalSign(VitalSign.OXYGEN_PERCENTAGE)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.HEMORRHAGE, ActiveTrauma.getInitialVitalSign(VitalSign.HEMORRHAGE).isEmpty()
                                        ? null
                                        : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.HEMORRHAGE)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.LIMBS_FRACTURE, ActiveTrauma.getInitialVitalSign(VitalSign.LIMBS_FRACTURE).isEmpty()
                                        ? null
                                        : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.LIMBS_FRACTURE)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.FRACTURE_EXPOSITION, ActiveTrauma.getInitialVitalSign(VitalSign.FRACTURE_EXPOSITION).isEmpty()
                                        ? null
                                        : convertYesNo(ActiveTrauma.getInitialVitalSign(VitalSign.FRACTURE_EXPOSITION)))
                                .putOpt(C.report.patientInitialCondition.clinicalPicture.BURN, ActiveTrauma.getInitialVitalSign(VitalSign.BURN).isEmpty()
                                        ? null
                                        : ActiveTrauma.getInitialVitalSign(VitalSign.BURN))));

                NetworkManager.doPUT(SSR_HTTP_PREFIX +"/" + SSR_CURRENT_TRAUMA_ID + "/patient_initial_condition", content.toString(), (code, message, responseBody) -> {
                    ssrLog("...> " + code + "\n" + responseBody);
                });
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public static void notifyEvent(final JSONObject content){
        ssrInvokeOnThread(() -> {
            try {
                ssrLog("called notifyEvent()");

                JSONObject obj = new JSONObject();
                obj.put("event", content);

                NetworkManager.doPOST(SSR_HTTP_PREFIX +"/" + SSR_CURRENT_TRAUMA_ID + "/events", obj.toString(), (code, message, responseBody) -> {
                    ssrLog("...> " + code + "\n" + responseBody);
                });
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        });
    }

    private static void ssrInvokeOnThread(final Runnable runnable){
        if(SSR_TEST_ACTIVE){
            new Thread(runnable).start();
        }
    }

    private static void ssrLog(String msg){
        Log.d("[SSR-TEST]",msg);
    }

    private static boolean convertYesNo(final String value) {

        boolean ret = false;

        if (value.equals(App.getAppResources().getString(R.string.switch_yes_label).toLowerCase())) {
            ret = true;
        } else if (value.equals(App.getAppResources().getString(R.string.switch_no_label).toLowerCase())) {
            ret = false;
        }

        return ret;
    }

    //--------------------------------------------------

    /**
     *
     */
    public static class TraumaTeam {

        private User user;
        private List<String> team;

        TraumaTeam(User tl){
            this(tl, new ArrayList<>());
        }

        TraumaTeam(User tl, List<String> tt){
            user = tl;
            team = tt;
        }

        public void updateTraumaLeader(User u){
            user = u;
        }

        public User currentTraumaLeader() {
            return user;
        }

        public void addTeamMember(String description){
            if(!isMemberPresent(description)) {
                team.add(description);
            }
        }

        public void removeTeamMember(String description){
            team.remove(description);
        }

        public boolean isMemberPresent(String description){
            return team.contains(description);
        }

        public List<String> getTeam(){
            return team;
        }
    }
}
