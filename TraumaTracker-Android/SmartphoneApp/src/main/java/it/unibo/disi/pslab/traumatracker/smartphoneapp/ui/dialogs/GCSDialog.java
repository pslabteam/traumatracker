package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.GCS;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class GCSDialog extends TTDialog {

    private TextView gcsTotalLabel;

    private GCS currentGCS;

    public GCSDialog(Context context, GCS initialGCS, final Listener listener) {
        super(context, R.layout.dialog_gcs);

        currentGCS = initialGCS;

        intiUI(listener);
    }

    private void intiUI(final Listener listener) {
        findViewById(R.id.yesButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onConfirmationButtonClicked(currentGCS);
            }

            dismiss();
        });

        findViewById(R.id.noButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onBackButtonClicked();
            }

            dismiss();
        });

        int[] gcsSpinners = new int[]{R.id.gcs_motor_spinner, R.id.gcs_verbal_spinner, R.id.gcs_eyes_spinner};
        VitalSign[] gcsParts = new VitalSign[]{VitalSign.GCS_MOTOR, VitalSign.GCS_VERBAL, VitalSign.GCS_EYES};
        String[] gcsValues = new String[]{currentGCS.motor(), currentGCS.verbal(), currentGCS.eyes()};

        for (int i = 0; i < gcsSpinners.length; i++) {
            Spinner s = initSpinner(gcsSpinners[i], gcsParts[i]);

            for (int j = 0; j < s.getAdapter().getCount(); j++) {
                if (s.getAdapter().getItem(j).equals(gcsValues[i])) {
                    s.setSelection(j);
                }
            }
        }

        gcsTotalLabel = findViewById(R.id.gcs_total_value_label);

        if (currentGCS.total() != 0) {
            gcsTotalLabel.setText(String.valueOf(currentGCS.total()));
        }

        if(currentGCS.sedated()){
            ((CheckBox)findViewById(R.id.tt_gcs_sedated)).setChecked(true);
        }

        ((CheckBox)findViewById(R.id.tt_gcs_sedated)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            currentGCS.sedated(isChecked);
        });
    }

    private Spinner initSpinner(int spinnerRes, final VitalSign vs) {
        List<String> elements = vs.getValuesList();
        elements.add(0, "");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, elements);

        Spinner spinner = findViewById(spinnerRes);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                switch (vs) {
                    case GCS_MOTOR:
                        currentGCS.motor(parent.getItemAtPosition(pos).toString());
                        break;
                    case GCS_VERBAL:
                        currentGCS.verbal(parent.getItemAtPosition(pos).toString());
                        break;
                    case GCS_EYES:
                        currentGCS.eyes(parent.getItemAtPosition(pos).toString());
                        break;
                }

                if (currentGCS.total() != 0) {
                    gcsTotalLabel.setText(String.valueOf(currentGCS.total()));
                } else {
                    gcsTotalLabel.setText("--");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        return spinner;
    }

    public interface Listener {
        void onBackButtonClicked();
        void onConfirmationButtonClicked(GCS newGCS);
    }
}
