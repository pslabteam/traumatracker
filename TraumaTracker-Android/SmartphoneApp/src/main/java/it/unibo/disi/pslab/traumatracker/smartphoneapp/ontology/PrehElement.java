package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

public enum PrehElement {
    TERRITORIAL_AREA,
    CAR_ACCIDENT,
    A,
    B_PLEURAL_DECOMPRESSION,
    C_BLOOD_PROTOCOL,
    C_TPOD,
    C_PATIENT_STATIONARY,
    D_GCS_TOTAL,
    D_ANISOCORIA,
    D_MIDRIASI,
    E_PARA_TETRA_PARESI,
    WROST_BLOOD_PRESSURE,
    WROST_RESPIRATORY_RATE
}
