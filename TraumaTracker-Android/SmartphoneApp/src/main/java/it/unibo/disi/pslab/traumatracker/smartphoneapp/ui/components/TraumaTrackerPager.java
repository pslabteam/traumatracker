package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components;

import android.content.Context;
import android.graphics.Typeface;

import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public class TraumaTrackerPager extends ViewPager {

    public TraumaTrackerPager(Context context) {
        super(context);
    }

    public TraumaTrackerPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void changeCurrentPage(View[] pagesTabs, int newTab){
        for(View t : pagesTabs){
            updateTabView((TextView) t, R.drawable.traumatracker_tab, Typeface.NORMAL);
        }

        updateTabView((TextView) pagesTabs[newTab], R.drawable.traumatracker_tab_selected, Typeface.BOLD);
    }

    private void updateTabView(TextView tab, int backgroundRes, int textStyle){
        tab.setBackgroundResource(backgroundRes);
        tab.setTypeface(null, textStyle);
    }
}
