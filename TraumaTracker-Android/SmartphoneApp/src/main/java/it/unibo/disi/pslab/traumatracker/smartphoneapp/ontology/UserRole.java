package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

public enum UserRole {
    ADMIN("admin"),
    MEMBER("member"),
    DIRECTOR("director"),
    DATA_ANALYST("data-analyst");

    private final String description;

    UserRole(final String description){
        this.description = description;
    }

    public String description(){
        return description;
    }
}
