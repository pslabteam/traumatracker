package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.BloodProduct;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class Note {

    private JSONObject content;

    private Note(JSONObject content) {
        this.content = content;
    }

    public JSONObject getContent() {
        return content;
    }

    static Note makePatientAccessedErNote(String place){
        try {
            JSONObject event = createEvent(place, C.report.events.type.PATIENT_ACCEPTED, new JSONObject());
            return new Note(event);
        } catch (JSONException e) {
            return null;
        }
    }

    static Note makeProcedureNote(String place, Procedure procedure, Map<String, Object> options) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.procedure.ID, procedure.getId())
                    .put(C.report.events.procedure.DESCRIPTION, procedure.toString())
                    .put(C.report.events.procedure.TYPE, C.report.events.procedure.type.ONE_SHOT);

            if (options != null) {
                if(procedure.equals(Procedure.INTUBATION)){
                    content.putOpt(C.report.events.procedure.option.INTUBATION_DIFFICULT_AIRWAY, options.get(C.report.events.procedure.option.INTUBATION_DIFFICULT_AIRWAY));
                    content.putOpt(C.report.events.procedure.option.INTUBATION_INHALATION,options.get(C.report.events.procedure.option.INTUBATION_INHALATION));
                    content.putOpt(C.report.events.procedure.option.INTUBATION_VIDEOLARINGO,options.get(C.report.events.procedure.option.INTUBATION_VIDEOLARINGO));
                    content.putOpt(C.report.events.procedure.option.INTUBATION_FROVA,options.get(C.report.events.procedure.option.INTUBATION_FROVA));
                }

                if(procedure.equals(Procedure.DRAINAGE)){
                    content.putOpt(C.report.events.procedure.option.DRAINAGE_LEFT,options.get(C.report.events.procedure.option.DRAINAGE_LEFT));
                    content.putOpt(C.report.events.procedure.option.DRAINAGE_RIGHT,options.get(C.report.events.procedure.option.DRAINAGE_RIGHT));
                }

                if(procedure.equals(Procedure.CHEST_TUBE)){
                    content.putOpt(C.report.events.procedure.option.CHESTTUBE_LEFT,options.get(C.report.events.procedure.option.CHESTTUBE_LEFT));
                    content.putOpt(C.report.events.procedure.option.CHESTTUBE_RIGHT,options.get(C.report.events.procedure.option.CHESTTUBE_RIGHT));
                }
            }

            JSONObject event = createEvent(place, C.report.events.type.PROCEDURE, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeTimeDependentProcedureNote(String place, Procedure procedure, String eventType, Map<String, Object> options) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.procedure.ID, procedure.getId())
                    .put(C.report.events.procedure.DESCRIPTION, procedure.toString())
                    .put(C.report.events.procedure.TYPE, C.report.events.procedure.type.TIME_DEPENDENT)
                    .put(C.report.events.procedure.EVENT, eventType);

            if (options != null) {
                for (String key : options.keySet()) {
                    content.put(key, options.get(key));
                }
            }

            JSONObject event = createEvent(place, C.report.events.type.PROCEDURE, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeDiagnosticNote(String place, Diagnostics diagnostic, Map<String, Object> options) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.diagnostic.ID, diagnostic.id())
                    .put(C.report.events.diagnostic.DESCRIPTION, diagnostic.toString());

            if (options != null) {

                if(diagnostic.equals(Diagnostics.ABG)){
                    content.putOpt(C.report.events.diagnostic.option.ABG_LACTATES,options.get(C.report.events.diagnostic.option.ABG_LACTATES));
                    content.putOpt(C.report.events.diagnostic.option.ABG_BE,options.get(C.report.events.diagnostic.option.ABG_BE));
                    content.putOpt(C.report.events.diagnostic.option.ABG_PH,options.get(C.report.events.diagnostic.option.ABG_PH));
                    content.putOpt(C.report.events.diagnostic.option.ABG_HB,options.get(C.report.events.diagnostic.option.ABG_HB));
                }

                if(diagnostic.equals(Diagnostics.ROTEM)){
                    content.putOpt(C.report.events.diagnostic.option.ROTEM_FIBTEM,options.get(C.report.events.diagnostic.option.ROTEM_FIBTEM));
                    content.putOpt(C.report.events.diagnostic.option.ROTEM_EXTEM,options.get(C.report.events.diagnostic.option.ROTEM_EXTEM));
                    content.putOpt(C.report.events.diagnostic.option.ROTEM_HYPERFIBRINOLYSIS,options.get(C.report.events.diagnostic.option.ROTEM_HYPERFIBRINOLYSIS));
                }
            }

            JSONObject event = createEvent(place, C.report.events.type.DIAGNOSTIC, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeDrugNote(String place, Drug drug, double qty) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.drug.ID, drug.id())
                    .put(C.report.events.drug.DESCRIPTION, drug.toString())
                    .put(C.report.events.drug.ADMINISTRATION_TYPE, C.report.events.drug.type.ONE_SHOT)
                    .put(C.report.events.drug.QUANTITY, qty)
                    .put(C.report.events.drug.UNIT, drug.unit());

            JSONObject event = createEvent(place, C.report.events.type.DRUG, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeDrugNote(String place, Drug drug) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.drug.ID, drug.id())
                    .put(C.report.events.drug.DESCRIPTION, drug.toString())
                    .put(C.report.events.drug.ADMINISTRATION_TYPE, C.report.events.drug.type.DRUG_PROTOCOL);

            JSONObject event = createEvent(place, C.report.events.type.DRUG, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeBloodProductNote(String place, BloodProduct bloodProduct, double qty) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.bloodProduct.ID, bloodProduct.id())
                    .put(C.report.events.bloodProduct.DESCRIPTION, bloodProduct.toString())
                    .put(C.report.events.bloodProduct.ADMINISTRATION_TYPE, C.report.events.bloodProduct.type.ONE_SHOT)
                    .put(C.report.events.bloodProduct.QUANTITY, qty)
                    .put(C.report.events.bloodProduct.UNIT, bloodProduct.unit());

            JSONObject event = createEvent(place, C.report.events.type.BLOOD_PRODUCT, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeBloodProductNote(String place, BloodProduct bloodProduct, double qty, String bagCode) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.bloodProduct.ID, bloodProduct.id())
                    .put(C.report.events.bloodProduct.DESCRIPTION, bloodProduct.toString())
                    .put(C.report.events.bloodProduct.ADMINISTRATION_TYPE, C.report.events.bloodProduct.type.ONE_SHOT)
                    .put(C.report.events.bloodProduct.QUANTITY, qty)
                    .put(C.report.events.bloodProduct.UNIT, bloodProduct.unit())
                    .put(C.report.events.bloodProduct.BAG_CODE, bagCode);

            JSONObject event = createEvent(place, C.report.events.type.BLOOD_PRODUCT, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeBloodProductNote(String place, BloodProduct bloodProduct) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.bloodProduct.ID, bloodProduct.id())
                    .put(C.report.events.bloodProduct.DESCRIPTION, bloodProduct.toString())
                    .put(C.report.events.bloodProduct.ADMINISTRATION_TYPE, C.report.events.bloodProduct.type.BLOOD_PROTOCOL);

            JSONObject event = createEvent(place, C.report.events.type.BLOOD_PRODUCT, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeContinuousInfusionStartDrugNote(String place, Drug drug, double qty) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.drug.ID, drug.id())
                    .put(C.report.events.drug.DESCRIPTION, drug.toString())
                    .put(C.report.events.drug.ADMINISTRATION_TYPE, C.report.events.drug.type.CONTINUOUS_INFUSION)
                    .put(C.report.events.drug.EVENT, C.report.events.drug.event.START)
                    .put(C.report.events.drug.QUANTITY, qty)
                    .put(C.report.events.drug.UNIT, drug.unit());

            JSONObject event = createEvent(place, C.report.events.type.DRUG, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeContinuousInfusionVariationDrugNote(String place, Drug drug, double qty) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.drug.ID, drug.id())
                    .put(C.report.events.drug.DESCRIPTION, drug.toString())
                    .put(C.report.events.drug.ADMINISTRATION_TYPE, C.report.events.drug.type.CONTINUOUS_INFUSION)
                    .put(C.report.events.drug.EVENT, C.report.events.drug.event.VARIATION)
                    .put(C.report.events.drug.QUANTITY, qty)
                    .put(C.report.events.drug.UNIT, drug.unit());

            JSONObject event = createEvent(place, C.report.events.type.DRUG, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeContinuousInfusionStopDrugNote(String place, Drug drug, int duration) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.drug.ID, drug.id())
                    .put(C.report.events.drug.DESCRIPTION, drug.toString())
                    .put(C.report.events.drug.ADMINISTRATION_TYPE, C.report.events.drug.type.CONTINUOUS_INFUSION)
                    .put(C.report.events.drug.EVENT, C.report.events.drug.event.STOP)
                    .put(C.report.events.drug.DURATION, duration);

            JSONObject event = createEvent(place, C.report.events.type.DRUG, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeVitalSignNote(String place, VitalSign vs, String value) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.clinicalVariation.ID, vs.getId())
                    .put(C.report.events.clinicalVariation.DESCRIPTION, vs.toString());


            if(vs.equals(VitalSign.SEDATED)){
                //inviato come stringa per uniformità con gli altri valori del clinical variation
                content.put(C.report.events.clinicalVariation.VALUE, "" + convertYesNo(value));
            } else {
                content.put(C.report.events.clinicalVariation.VALUE, value);
            }

            JSONObject event = createEvent(place, C.report.events.type.CLINICAL_VARIATION, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeVitalSignsMonitorNote(String place, int sys, int dia, int hr, int etco2, int spo2, int temp) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.vitalSignMon.SYS, sys)
                    .put(C.report.events.vitalSignMon.DIA, dia)
                    .put(C.report.events.vitalSignMon.HR, hr)
                    .put(C.report.events.vitalSignMon.ETCO2, etco2)
                    .put(C.report.events.vitalSignMon.SPO2, spo2)
                    .put(C.report.events.vitalSignMon.TEMP, temp);

            JSONObject event = createEvent(place, C.report.events.type.VITAL_SIGNS_MON, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeSnapNote(String place, String filename) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.photo.DESCRIPTION, filename);

            JSONObject event = createEvent(place, C.report.events.type.PHOTO, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeVideoNote(String place, String filename) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.video.DESCRIPTION, filename);

            JSONObject event = createEvent(place, C.report.events.type.VIDEO, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeVocalNote(String place, String filename) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.vocalNote.DESCRIPTION, filename);

            JSONObject event = createEvent(place, C.report.events.type.VOCAL_NOTE, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeTextNote(String place, String note) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.textNote.TEXT, note);

            JSONObject event = createEvent(place, C.report.events.type.TEXT_NOTE, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeTraumaLeaderNote(String place, User user) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.traumaLeader.ID, user.id())
                    .put(C.report.events.traumaLeader.NAME, user.name())
                    .put(C.report.events.traumaLeader.SURNAME, user.surname());

            JSONObject event = createEvent(place, C.report.events.type.TRAUMA_LEADER, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeRoomInNote(String place) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.roomIn.PLACE, place);

            JSONObject event = createEvent(place, C.report.events.type.ROOM_IN, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeRoomOutNote(String place) {
        try {
            JSONObject content = new JSONObject()
                    .put(C.report.events.roomOut.PLACE, place);

            JSONObject event = createEvent(place, C.report.events.type.ROOM_OUT, content);

            return new Note(event);
        } catch (Exception ex) {
            return null;
        }
    }

    static Note makeReportReactivationNote(String place) {
        try {
            JSONObject event = createEvent(place, C.report.events.type.REPORT_REACTIVATION, new JSONObject());
            return new Note(event);
        } catch (JSONException e) {
            return null;
        }
    }

    static Note updateNote(final Note note, final String newDate, final String newTime, final String newPlace) throws JSONException {
        return new Note(new JSONObject()
                .put(C.report.events.ID, note.getContent().getInt(C.report.events.ID))
                .put(C.report.events.DATE, newDate)
                .put(C.report.events.TIME, newTime)
                .put(C.report.events.PLACE, newPlace)
                .put(C.report.events.TYPE, note.getContent().getString(C.report.events.TYPE))
                .put(C.report.events.CONTENT, note.getContent().getJSONObject(C.report.events.CONTENT)));
    }

    public void updateNotePlace(final String newPlace) throws JSONException {
        this.content = new JSONObject()
                .put(C.report.events.ID, this.getContent().getInt(C.report.events.ID))
                .put(C.report.events.DATE, this.getContent().getString(C.report.events.DATE))
                .put(C.report.events.TIME, this.getContent().getString(C.report.events.TIME))
                .put(C.report.events.PLACE, newPlace)
                .put(C.report.events.TYPE, this.getContent().getString(C.report.events.TYPE))
                .put(C.report.events.CONTENT, this.getContent().getJSONObject(C.report.events.CONTENT));
    }

    static Note restoreNote(final JSONObject content) {
        return new Note(content);
    }

    private static JSONObject createEvent(final String place, final String type, final JSONObject content) throws JSONException {
        return new JSONObject()
                .put(C.report.events.ID, Counter.next())
                .put(C.report.events.DATE, DateTimeUtils.getCurrentDate())
                .put(C.report.events.TIME, DateTimeUtils.getCurrentTime())
                .put(C.report.events.PLACE, place)
                .put(C.report.events.TYPE, type)
                .put(C.report.events.CONTENT, content);
    }

    private static boolean convertYesNo(final String value){

        boolean ret = false;

        if(value.equals(App.getAppResources().getString(R.string.switch_yes_label).toLowerCase())){
            ret = true;
        }

        if(value.equals(App.getAppResources().getString(R.string.switch_no_label).toLowerCase())){
            ret = false;
        }
        return ret;
    }

    @NonNull
    @Override
    public String toString() {
        return content.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Note){
            Note n = (Note) obj;
            return this.getContent().toString().equals(n.toString());
        }

        return super.equals(obj);
    }

    /**
     * An internal counter for Notes to obtain a new EVENT_ID for any new note
     */
    public static class Counter {
        private static int cnt = 0;

        static int next() {
            return cnt++;
        }

        public static void init() {
            cnt = 0;
        }

        public static void init(int n) {
            cnt = n;
        }
    }
}
