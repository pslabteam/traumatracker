package it.unibo.disi.pslab.traumatracker.smartphoneapp.utils;

public class Preferences {
    public static final String ID = "tt-preferences";

    public static final String GLASSES_SUPPORT = "GLASSES";
    public static final String VOICE = "VOICE";

    public static final String LOGGING = "LOGGING";

    public static final String TT_SERVICE_IP = "TT_SERVICE_IP";
    public static final String PREH_SERVICE_IP = "PREH_SERVICE_IP";

    public static final String BLE_TAG_ID = "BLE_TAG_ID";

    public static final String LANGUAGE = "LANGUAGE";
}
