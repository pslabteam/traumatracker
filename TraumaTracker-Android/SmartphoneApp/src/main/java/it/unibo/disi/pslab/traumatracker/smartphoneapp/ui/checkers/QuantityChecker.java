package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers;


public interface QuantityChecker {
    boolean checkInput(String input);
    String errorMessage();
}
