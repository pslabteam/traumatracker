package it.unibo.disi.pslab.traumatracker.smartphoneapp.tools;

import android.bluetooth.BluetoothSocket;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import it.unibo.disi.pslab.bluetoothlib.exceptions.BluetoothMessageTooBigException;

public class GlassBridgeManager extends Thread {

    private BluetoothSocket btSocket;

    private boolean stop = true;

    private HashMap<String, String> pendingRequests;

    private static GlassBridgeManager instance = new GlassBridgeManager();

    private GlassBridgeManager() {
        pendingRequests = new HashMap<>();
    }

    public static GlassBridgeManager getInstance() {
        return instance;
    }

    public boolean setChannel(BluetoothSocket socket) {
        btSocket = socket;

        stop = false;

        return true;
    }

    public void run() {
        while (!stop) {
            try {
                int size = btSocket.getInputStream().read();

                int i = 0;
                StringBuffer buf = new StringBuffer();

                while (i < size) {
                    int data = btSocket.getInputStream().read();
                    buf.append((char) data);
                    i++;
                }

                JSONObject object = new JSONObject(buf.toString());

                switch (object.getString("message")) {
                    case "picture_stored":
                        pendingRequests.put(object.getString("key"), object.getString("name"));
                        notifyPictureTaken();
                        break;

                    default:
                        break;
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void tearDown() {
        stop = true;

        try {
            btSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return !stop;
    }


    /**
     * @param available
     */
    public synchronized void sendSpeechRecognizerStatus(boolean available) {
        try {
            JSONObject content = new JSONObject();
            content.put("listening", available);

            sendRequest("update_speechrecognizer_status", content);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public synchronized String sendTakePictureRequest(String key) throws JSONException, InterruptedException {
        JSONObject messageObj = new JSONObject();
        messageObj.put("type", "camera");
        messageObj.put("request", new JSONObject().put("cmd", "take-picture")
                .put("key", key));

        try {
            write(messageObj.toString());
        } catch (BluetoothMessageTooBigException | IOException e) {
            e.printStackTrace();
        }

        boolean responseReceived = false;

        while (!responseReceived) {
            wait();

            if (pendingRequests.containsKey(key)) {
                responseReceived = true;
            }
        }

        String path = pendingRequests.get(key);
        pendingRequests.remove(key);

        return path;
    }

    public synchronized void notifyPictureTaken() {
        notifyAll();
    }

    public synchronized void pushMediaToServer() throws JSONException {
        JSONObject messageObj = new JSONObject();
        messageObj.put("type", "media");
        messageObj.put("request", new JSONObject()
                .put("cmd", "push-media-to-server"));

        try {
            write(messageObj.toString());
        } catch (BluetoothMessageTooBigException | IOException e) {
            e.printStackTrace();
        }
    }


    private void sendRequest(String request_type, JSONObject content) throws JSONException {
        try {
            JSONObject request = new JSONObject();
            request.put("request", request_type);
            request.put("content", content);

            write(request.toString());
        } catch (BluetoothMessageTooBigException | IOException e) {
            e.printStackTrace();
        }
    }

    private void write(String msg) throws BluetoothMessageTooBigException, IOException {
        if (btSocket == null)
            return;

        if (msg.length() > 255) {
            throw new BluetoothMessageTooBigException();
        }

        char[] array = msg.toCharArray();
        byte[] bytes = new byte[array.length + 1];
        bytes[0] = (byte) msg.length();

        for (int i = 0; i < array.length; i++) {
            bytes[i + 1] = (byte) array[i];
        }

        btSocket.getOutputStream().write(bytes);
        btSocket.getOutputStream().flush();
    }
}
