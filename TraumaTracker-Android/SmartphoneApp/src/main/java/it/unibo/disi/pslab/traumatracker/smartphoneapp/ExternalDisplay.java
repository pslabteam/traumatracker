package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import org.json.JSONException;
import org.json.JSONObject;

import cartago.ARTIFACT_INFO;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.OperationException;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

@ARTIFACT_INFO(outports = {@OUTPORT(name = "out-bt")})
public class ExternalDisplay extends JaCaArtifact {

    private volatile boolean enabled = false;

    void init(){}

    @OPERATION
    public void enableExternalDisplay(){
        enabled = true;
    }

    @OPERATION
    public void disableExternalDisplay(){
        enabled = false;
    }

    @OPERATION
    void showTrackingStatus(String status) throws OperationException {
        if(!enabled){
            return;
        }

        execLinkedOp("out-bt","sendTrackingStatus", status);
    }

    @OPERATION
    void updatePlaceInfo(String place) throws OperationException, JSONException {
        if(!enabled) {
            return;
        }

        sendRequestMessage(new JSONObject()
                .put("not_alarm", true)
                .put("type", "setRoom")
                .put("room", place));
    }

    @OPERATION
    void showNewProcedurePerformedMessage(Procedure procedure) throws OperationException, JSONException {
        if(!enabled) {
            return;
        }

        String message = "MANOVRA - " + procedure.toString();

        sendRequestMessage(new JSONObject()
                .put("text", message)
                .put("priority", 5)
                .put("update", false));
    }

    @OPERATION
    void showNewDiagnosticPerformedMessage(Diagnostics diagnostic) throws OperationException, JSONException {
        if(!enabled) {
            return;
        }

        String message = "DIAGNOSTICA - " + diagnostic.toString();

        sendRequestMessage(new JSONObject()
                .put("text", message)
                .put("priority", 5)
                .put("update", false));
    }

    @OPERATION
    void showNewDrugMessage(Drug drug, double qty) throws OperationException, JSONException {
        if(!enabled) {
            return;
        }

        String message = "FARMACO - " + drug.toString() + " [" + qty + " " + drug.unit() + "]";

        sendRequestMessage(new JSONObject()
                .put("text", message)
                .put("priority", 5)
                .put("update", false));
    }

    @OPERATION
    void showNewDrugMessage(Drug drug) throws OperationException, JSONException {
        if(!enabled) {
            return;
        }

        String message = "FARMACO - " + drug.toString();

        sendRequestMessage(new JSONObject()
                .put("text", message)
                .put("priority", 5)
                .put("update", false));
    }

    @OPERATION
    void showVitalSignVariationMessage(VitalSign vs, String newValue) throws OperationException, JSONException {
        if(!enabled) {
            return;
        }

        String message = "VARIAZIONE VS - " + vs.toString() + ": " + newValue;

        sendRequestMessage(new JSONObject()
                .put("text", message)
                .put("priority", 5)
                .put("update", false));
    }


    private void sendRequestMessage(JSONObject requestContent) throws OperationException {
        execLinkedOp("out-bt","sendRequest", requestContent.toString());
    }

    private void sendRequestResponseMessage(int requestId, JSONObject requestContent, BluetoothChannel.Events responseListener) throws OperationException {
        execLinkedOp("out-bt", "sendRequestResponse", requestId, requestContent.toString(), responseListener);
    }
}
