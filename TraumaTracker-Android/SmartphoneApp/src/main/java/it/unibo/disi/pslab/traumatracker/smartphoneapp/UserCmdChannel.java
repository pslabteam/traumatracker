package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import cartago.*;

public class UserCmdChannel extends Artifact {

	void init(){}
	
	@OPERATION
	void notifyTakeSnapshot(){
		signal("user_cmd","take_snap");
	}

	@OPERATION
    void notifyNewDrug(String type, String unit, int qty){
		signal("user_cmd","new_drug",type,unit,qty);
	}

	@OPERATION
    void notifyNewProcedure(String type){
		signal("user_cmd","new_procedure",type);
	}

	@OPERATION
    void notifyGetVitalSigns(){
		signal("user_cmd","get_vital_signs");
	}
}
