package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TinyReport;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.TemporaryReportsAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;

public class RestartTrackingDialog extends TTDialog {

    private TemporaryReportsAdapter adapter;

    public RestartTrackingDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_restart_tracking);

        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());

        adapter = new TemporaryReportsAdapter(context, getReportsFromFolder(C.fs.TEMP_FOLDER));
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                updateDialogUI();
            }
        });

        updateDialogUI();
    }

    private void updateDialogUI(){
        if(adapter.isEmpty()){
            ((TextView) findViewById(R.id.title)).setText(getString(R.string.warning));
            findViewById(R.id.noUsersLabel).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.noUsersLabel)).setText(getString(R.string.temporary_reports_unavailable));
            findViewById(R.id.reports_list).setVisibility(View.GONE);
        } else {
            ((TextView) findViewById(R.id.title)).setText(getString(R.string.restart_tracking_dialog_title));
            findViewById(R.id.noUsersLabel).setVisibility(View.GONE);

            final ListView list = findViewById(R.id.reports_list);
            list.setVisibility(View.VISIBLE);
            list.setAdapter(adapter);
        }
    }

    private List<TinyReport> getReportsFromFolder(String folder) {
        List<TinyReport> tinyReports = new ArrayList<>();

        File[] files = FileSystemUtils.getFilesFromDirectory(folder);

        for (File file : files) {
            final String reportContent = FileSystemUtils.loadFile(folder, file.getName());
            if(!reportContent.isEmpty()){
                tinyReports.add(new TinyReport(reportContent));
            }
        }

        return tinyReports;
    }

    public interface Listener {
        void onReportSelected(String id);
    }
}
