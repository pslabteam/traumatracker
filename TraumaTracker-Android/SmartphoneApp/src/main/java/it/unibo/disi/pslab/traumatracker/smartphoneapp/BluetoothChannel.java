package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

import org.json.JSONObject;

import java.io.IOException;

import cartago.LINK;
import cartago.OPERATION;
import it.unibo.disi.pslab.bluetoothlib.BluetoothServer;
import it.unibo.disi.pslab.bluetoothlib.exceptions.ServerRunningException;
import it.unibo.disi.pslab.bluetoothlib.utils.BluetoothError;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class BluetoothChannel extends JaCaArtifact implements BluetoothServer.Events {
    private BluetoothServer btServer;
    private BluetoothDevice connectedDevice;

    public void init() {
        btServer = BluetoothServer.getInstance();
        btServer.registerEventListener(this);
    }

    @OPERATION
    public void turnOnBluetoothServer() {
        if (!btServer.isRunning()) {
            try {
                btServer.startServer(C.bt.DEFAULT_SERVER_NAME, C.bt.UUID);
            } catch (ServerRunningException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    @OPERATION
    public void turnOffBluetoothServer() {
        if (btServer.isRunning()) {
            btServer.stopServer();
        }
    }

    @LINK
    public void sendRequest(String messageContent){
        btServer.sendMessage(connectedDevice, messageContent);
    }

    @LINK
    public void sendRequestResponse(int requestId, String requestContent, Events responseListener){
        btServer.sendMessage(connectedDevice, requestContent);
        //TODO : salvare il listener in modo da sapere a chi inoltrare la risposta
    }

    interface Events {
        void onResponseAvailable(int requestId, JSONObject responseContent);
    }

    /**
     * ---------------------------------------------------------------------------------------------
     * ---------------------------------------------------------------------------------------------
     * ---------- BluetoothServer.Events Listener Implementation below -----------------------------
     * ---------------------------------------------------------------------------------------------
     * ---------------------------------------------------------------------------------------------
     */

    private static final int BT_SERVER_ACTIVE_NOTIFICATION_ID = 1, GLASS_CONNECTED_NOTIFICATION_ID = 2;

    @Override
    public void onServerStarted() {
        log("BT server turned on!");
        showNotification(BT_SERVER_ACTIVE_NOTIFICATION_ID);
    }

    @Override
    public void onServerStopped() {
        log("BT server turned off!");
        hideNotification(BT_SERVER_ACTIVE_NOTIFICATION_ID);
        hideNotification(GLASS_CONNECTED_NOTIFICATION_ID);
    }

    @Override
    public void onServerError(BluetoothError error) {
        switch (error) {
            case BLUETOOTH_NOT_ENABLED:
                break;
            case GENERIC_EXCEPTION:
                break;
        }
    }

    @Override
    public void onDeviceConnected(BluetoothDevice device) {
        hideNotification(BT_SERVER_ACTIVE_NOTIFICATION_ID);
        showNotification(GLASS_CONNECTED_NOTIFICATION_ID);

        connectedDevice = device;

        beginExternalSession();
        signal("glass_status", "connected");
        endExternalSession(true);
    }

    @Override
    public void onDeviceDisconnected() {
        hideNotification(GLASS_CONNECTED_NOTIFICATION_ID);
        showNotification(BT_SERVER_ACTIVE_NOTIFICATION_ID);

        beginExternalSession();
        signal("glass_status", "disconnected");
        endExternalSession(true);
    }

    @Override
    public void onNewMessageReceived(String message) {
        log("----------> new message : " + message);
    }

    private void showNotification(final int id) {
        final Notification notification;

        switch (id) {
            case BT_SERVER_ACTIVE_NOTIFICATION_ID:
                notification = new Notification.Builder(getApplicationContext())
                        .setContentTitle(App.getAppResources().getString(R.string.app_name))
                        .setContentText("Server BT Attivo. In attesa della connessione degli smartglasses.")
                        .setSmallIcon(R.drawable.icon_bluetooth)
                        .build();
                break;

            case GLASS_CONNECTED_NOTIFICATION_ID:
                notification = new Notification.Builder(getApplicationContext())
                        .setContentTitle(App.getAppResources().getString(R.string.app_name))
                        .setContentText("SmartGlasses connessi.")
                        .setSmallIcon(R.drawable.icon_glass)
                        .build();
                break;

            default:
                notification = new Notification.Builder(getApplicationContext()).build();
                break;
        }

        runOnMainLooper(() -> ((NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE)).notify(id, notification));

    }

    private void hideNotification(final int id) {
        runOnMainLooper(() -> ((NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE)).cancel(id));
    }
}
