package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;


public enum Dosage {
    FIXED,
    VARIABLE,
    NULL
}
