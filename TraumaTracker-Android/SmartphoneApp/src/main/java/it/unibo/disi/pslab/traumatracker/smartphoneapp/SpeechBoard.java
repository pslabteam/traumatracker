package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import cartago.Tuple;
import cartago.OPERATION;

import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.exceptions.SpeechRecognizerManagerUninitialized;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model.CommandCode;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model.MedicalTerm;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology.MedicalTermsType;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.tools.SpeechRecognizerManager;

import it.unibo.pslab.jaca_android.core.JaCaArtifact;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class SpeechBoard extends JaCaArtifact {

	private final String SPEECH_CMD_OBS_PROP = "speech_cmd";
    private final String LISTENING_CMD_OBS_PROP = "listening";
    private final String TRACKING_STATUS_OBS_PROP = "tracking_status";
    private final String RECO_ERROR_OBS_PROP = "reco_error";

    private Handler uiHandler;
	
	public void init(){
		defineObsProperty(SPEECH_CMD_OBS_PROP,"");
        defineObsProperty(LISTENING_CMD_OBS_PROP,false);
		defineObsProperty(TRACKING_STATUS_OBS_PROP,0);

		final SpeechBoard artifact = this;
		
		uiHandler = new Handler(Looper.getMainLooper()){
			public void handleMessage(Message msg) {
				try {
					artifact.beginExternalSession();

                    switch(msg.what){
                        case CommandCode.START_LISTENING:
                            updateListening(true);
                            break;

                        case CommandCode.END_OF_SPEECH:
                            updateListening(false);
                            break;

						case CommandCode.STRINGS_RECOGNIZED:
                            MedicalTerm medicalCmd= (MedicalTerm) msg.obj;

                            MedicalTermsType type = medicalCmd.getType();
                            if (type.equals(MedicalTermsType.RESUSCITATION_MANEUVERS)){
                                log("[COMMAND RECOGNIZED] new procedure: "+medicalCmd.getTerm());
                                updateSpeechCmd(new Tuple("newProc",medicalCmd.getTerm()));
                            } else if (type.equals(MedicalTermsType.DRUG_AND_INFUSIONS)){
                                log("[COMMAND RECOGNIZED] new drug: "+medicalCmd.getTerm());
                                updateSpeechCmd(new Tuple("newDrug",medicalCmd.getTerm(),medicalCmd.getQuantity(),medicalCmd.getUnit().toString()));
                            } else if (type.equals(MedicalTermsType.LIFE_PARAMETERS)){
                                log("[COMMAND RECOGNIZED] currentTraumaLeader vital signs");
                                updateSpeechCmd(new Tuple("getVitalSigns"));
                            }
							break;

                        case CommandCode.STRINGS_NOT_RECOGNIZED:
                            log("[CMD NOT RECOGNIZED]");
                            signal("cmd_not_recognized");
                            break;

                        case CommandCode.START_MISSION:
                            log("[COMMAND RECOGNIZED] start tracking");
                            updateSpeechCmd(new Tuple("start_tracking"));
							updateTrackingStatus(1);
							break;

						case CommandCode.END_MISSION:
                            log("[COMMAND RECOGNIZED] end tracking");
                            updateSpeechCmd(new Tuple("end_tracking"));
                            updateTrackingStatus(3);
							break;

						case CommandCode.PAUSE_RECOGNITION:
                            log("[COMMAND RECOGNIZED] pause tracking");
                            updateTrackingStatus(2);
							break;

						case CommandCode.RESUME_RECOGNITION:
                            log("[COMMAND RECOGNIZED] resume tracking");
                            updateTrackingStatus(1);
							// stopOperation(msg.obj.toString());
							break;

						case CommandCode.TAKE_PICTURE:
                            updateSpeechCmd(new Tuple("snap"));
							break;

						case CommandCode.START_VIDEO_RECORDING:
                            updateSpeechCmd(new Tuple("startRec"));
							break;

						case CommandCode.STOP_VIDEO_RECORDING:
                            updateSpeechCmd(new Tuple("stopRec"));
                            break;

						case CommandCode.MESSAGE_ERROR:
							String error = (String) msg.obj;
						    updateError(error);
							log("[ERROR IN RECOGNITION] Error Type: " + error);
							break;

						default:
							break;
					}

					artifact.endExternalSession(true);
				} catch (Exception ex){
					artifact.endExternalSession(false);
				}
			}
		};

		final Context context = this.getApplicationContext();

        runOnMainLooper(() -> SpeechRecognizerManager.getInstance().initialize(context, uiHandler));
	}
	
	private void updateSpeechCmd(Tuple value){
		this.getObsProperty(SPEECH_CMD_OBS_PROP).updateValue(value);
	}

    private void updateError(String value){
        this.getObsProperty(RECO_ERROR_OBS_PROP).updateValue(value);
    }

    private void updateListening(Boolean isListening){
        this.getObsProperty(LISTENING_CMD_OBS_PROP).updateValue(isListening);
    }

	private void updateTrackingStatus(int status){
		this.getObsProperty(TRACKING_STATUS_OBS_PROP).updateValue(status);
	}

	@OPERATION
    public void startSpeechRecognizer(){
		runOnMainLooper(() -> {
            try {
                SpeechRecognizerManager.getInstance().start();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        });
	}
	
	@OPERATION
    public void stopSpeechRecognizer(){
		runOnMainLooper(() -> {
            try {
                SpeechRecognizerManager.getInstance().stop();
            } catch (SpeechRecognizerManagerUninitialized e) {
                e.printStackTrace();
            }
        });
	}
}
