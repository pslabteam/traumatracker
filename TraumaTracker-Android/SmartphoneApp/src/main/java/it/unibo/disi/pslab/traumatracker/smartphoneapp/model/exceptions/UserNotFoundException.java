package it.unibo.disi.pslab.traumatracker.smartphoneapp.model.exceptions;

public class UserNotFoundException extends Exception {}
