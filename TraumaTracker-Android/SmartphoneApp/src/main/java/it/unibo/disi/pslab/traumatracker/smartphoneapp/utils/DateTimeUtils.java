package it.unibo.disi.pslab.traumatracker.smartphoneapp.utils;

import java.util.GregorianCalendar;

public class DateTimeUtils {

    private static final String H = "h ";
    private static final String M = "m ";
    private static final String S = "s";

    public static String getCurrentDate(){
        return formatDate(new GregorianCalendar());
    }
    public static String getCurrentTime(){
        return formatTime(new GregorianCalendar());
    }

    private static String formatDate(GregorianCalendar c){
        if(c == null)
            return "";

        return formatDate(c.get(GregorianCalendar.YEAR),
                c.get(GregorianCalendar.MONTH) + 1,
                c.get(GregorianCalendar.DAY_OF_MONTH));
    }

    public static String formatDate(int y, int m, int d){
        return y
                + "-"
                + (m < 10 ? "0" + m : String.valueOf(m))
                + "-"
                + (d < 10 ? "0" + d : String.valueOf(d));
    }

    private static String formatTime(GregorianCalendar c){
        if(c == null)
            return "";

        return formatTime(c.get(GregorianCalendar.HOUR_OF_DAY),
                c.get(GregorianCalendar.MINUTE),
                c.get(GregorianCalendar.SECOND));
    }

    public static String formatTime(int h, int m, int s){
        return (h < 10 ? "0" + h : String.valueOf(h))
                + ":"
                + (m < 10 ? "0" + m : String.valueOf(m))
                + ":"
                + (s < 10 ? "0" + s : String.valueOf(s));
    }

    public static DateTime createDateTime(String dtRep){
        return createDateTime(dtRep, " ");
    }

    public static DateTime createDateTime(String dtRep, String separator){
        String[] s1 = dtRep.split(separator);
        String[] dateParts = s1[0].split("-");
        String[] timeParts = s1[1].split(":");

        DateTime dt = new DateTime();
        dt.setDate(Integer.parseInt(dateParts[0]),Integer.parseInt(dateParts[1]),Integer.parseInt(dateParts[2]));
        dt.setTime(Integer.parseInt(timeParts[0]),Integer.parseInt(timeParts[1]),Integer.parseInt(timeParts[2]));

        return dt;
    }

    public static String convertSecondsToString(int s){
        long m = s / 60;
        long h = m / 60;

        String rep = "";

        rep += h > 0 ? h + H : "";
        rep += m > 0 ? (m - (h * 60)) + M : "";

        return rep + (s - (m * 60)) + S;
    }
}
