package it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.logging.Logger;

public final class PollingLocationDetector implements LocationDetector, Runnable{

    private volatile boolean stop = false;
    private String url;
    private int timeout;

    private LocationDetector.Listener listener = null;

    private Logger logger;

    public PollingLocationDetector(final String url, final int timeout){
        this(url, timeout, null);
    }

    public PollingLocationDetector(final String url, final int timeout, final Logger logger){
        this.url = url;
        this.timeout = timeout;
        this.logger = logger;
    }

    @Override
    public void startDetection() {
        new Thread(this).start();
    }

    @Override
    public void stopDetection() {
        stop = true;
    }

    @Override
    public void setOnPlaceUpdateAvailableListener(final LocationDetector.Listener listener){
        this.listener = listener;
    }

    @Override
    public void run() {

        log("Polling started!");

        while(!stop){

            //TODO: do GET
            //final Pair<Integer, String> res = NetworkManager.doGET(url);
            log("sended GET to LocationService");

            if(listener != null){
                listener.placeUpdate(""); //TODO
            }

            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        log("Polling stopped!");
    }

    private void log(String msg){
        if(logger != null){
            logger.writeOnLog(msg);
        }
    }
}
