package it.unibo.disi.pslab.traumatracker.smartphoneapp.vs_monitors;

import java.util.Map;

public interface VitalSignsMonitor {
    Map<String, Integer> getPatientVitalSigns();
}
