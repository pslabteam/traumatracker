package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import android.support.annotation.NonNull;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum Drug {
    //Infusions
    CRYSTALLOID("crystalloid", R.string.ontology_drug_crystalloid, DrugCategory.INFUSIONS, Dosage.FIXED, Uom.ML, 500),
    HYPERTONIC_SOLUTION("hypertonic", R.string.ontology_drug_hypertonic, DrugCategory.INFUSIONS, Dosage.FIXED, Uom.ML, 250),

    //General Drugs
    KETAMINE("ketamine", R.string.ontology_drug_ketamine, DrugCategory.GENERAL_DRUGS, Dosage.VARIABLE, Uom.MG, 100),
    SUCCINYLCHOLINE("succinylcholine", R.string.ontology_drug_succinylcholine, DrugCategory.GENERAL_DRUGS, Dosage.VARIABLE, Uom.MG, 100),
    MIDAZOLAM("midazolam", R.string.ontology_drug_midazolam, DrugCategory.GENERAL_DRUGS, Dosage.VARIABLE, Uom.MG, 5),
    THIOPENTAL("thiopental", R.string.ontology_drug_thiopental, DrugCategory.GENERAL_DRUGS, Dosage.VARIABLE, Uom.MG, 250),
    FENTANIL("fentanil", R.string.ontology_drug_fentanil, DrugCategory.GENERAL_DRUGS, Dosage.VARIABLE, Uom.MCG, 100),
    CURARE("curare", R.string.ontology_drug_curare, DrugCategory.GENERAL_DRUGS, Dosage.VARIABLE, Uom.MG, 10),
    TRANEXAMIC_ACID("tranex", R.string.ontology_drug_tranex, DrugCategory.GENERAL_DRUGS, Dosage.FIXED, Uom.G, 1),
    MANNITOL("mannitol", R.string.ontology_drug_mannitol, DrugCategory.GENERAL_DRUGS, Dosage.VARIABLE, Uom.ML, 300),

    //Continuous Infusion Drugs
    ADRENALINE_CONTINUOUS_INF("adreanaline-continuous-inf", R.string.ontology_drug_adreanalinecontinuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),
    NORADRENALINE_CONTINUOUS_INF("noradrenaline-continuous-inf", R.string.ontology_drug_noradrenalinecontinuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),
    PROPOFOL_CONTINUOUS_INF("propofol-continuous-inf", R.string.ontology_drug_propofolcontinuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),
    KETANEST_CONTINUOUS_INF("ketanest-continuous-inf", R.string.ontology_drug_ketanestcontinuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),
    GENERIC_DRUG_CONTINUOUS_INF_1("generic-drug-continuous-inf", R.string.ontology_drug_genericdrug1continuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),
    GENERIC_DRUG_CONTINUOUS_INF_2("generic-drug-continuous-inf", R.string.ontology_drug_genericdrug2continuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),
    GENERIC_DRUG_CONTINUOUS_INF_3("generic-drug-continuous-inf", R.string.ontology_drug_genericdrug3continuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),
    GENERIC_DRUG_CONTINUOUS_INF_4("generic-drug-continuous-inf", R.string.ontology_drug_genericdrug4continuousinf, DrugCategory.CONTINUOUS_INFUSION_DRUGS, Dosage.VARIABLE, Uom.MLH, 0),

    //ALS Drugs
    ATROPINE_ALS("atropine-als", R.string.ontology_drug_atropineals, DrugCategory.ALS_DRUGS, Dosage.VARIABLE, Uom.MG, 1),
    ADRENALINE_ALS("adrenaline-als", R.string.ontology_drug_adrenalineals, DrugCategory.ALS_DRUGS, Dosage.VARIABLE, Uom.MG, 1),
    ELECTRIC_SHOCK("electric-shock", R.string.ontology_drug_electricshock, DrugCategory.ALS_DRUGS, Dosage.NULL),

    //Antibiotic prophylaxis
    ANTIBIOTIC_PROPHYLAXIS("antibiotic-prophylaxis", R.string.ontology_drug_antibioticprophylaxis, DrugCategory.ANTIBIOTIC_PROPHYLAXIS, Dosage.NULL);

    public static final int GENERIC_DRUGS_MAX_AVAILABILITY = 4;

    private final String id;
    private final int labelRes;
    private final DrugCategory category;
    private final Uom unit;
    private final double defaultValue;
    private final Dosage dosageType;

    private String optionalDescription;

    Drug(final String id, final int labelRes, final DrugCategory category, final Dosage dosageType, final Uom unit, final double defaultValue){
        this.id = id;
        this.labelRes = labelRes;
        this.category = category;
        this.unit = unit;
        this.defaultValue = defaultValue;
        this.dosageType = dosageType;
        this.optionalDescription = "";
    }

    Drug(final String id, final int labelRes, final DrugCategory category, final Dosage dosageType){
        this.id = id;
        this.labelRes = labelRes;
        this.category = category;
        this.unit = null;
        this.defaultValue = 0;
        this.dosageType = dosageType;
        this.optionalDescription = "";
    }

    public String id(){
        return id;
    }

    public DrugCategory category() {
        return category;
    }

    public Uom unit() {
        return unit;
    }

    public double defaultValue() {
        return defaultValue;
    }
    
    public Dosage dosageType(){
        return dosageType;
    }

    public void setOptionalDescription(String description){
        this.optionalDescription = description;
    }

    @NonNull
    public String toString() {
        boolean genericCondition = this.equals(GENERIC_DRUG_CONTINUOUS_INF_1)
                || this.equals(GENERIC_DRUG_CONTINUOUS_INF_2)
                || this.equals(GENERIC_DRUG_CONTINUOUS_INF_3)
                || this.equals(GENERIC_DRUG_CONTINUOUS_INF_4);

        if(genericCondition && !this.optionalDescription.isEmpty()){
            return optionalDescription;
        }

        return App.getAppResources().getString(labelRes);
    }
}
