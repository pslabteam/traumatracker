package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Pair;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.GCS;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerEditText;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class PatientInitialStatusDialog extends TTDialog {
    private static Map<VitalSign, Pair<Integer, Integer[]>> radioGroupsMap = new HashMap<VitalSign, Pair<Integer, Integer[]>>(){{
        //Vital Signs
        put(VitalSign.TEMPERATURE, new Pair<>(R.id.vs_temp_selector, new Integer[]{R.id.vs_temp_normal, R.id.vs_temp_ipo, R.id.vs_temp_iper}));
        put(VitalSign.HEART_RATE, new Pair<>(R.id.vs_hr_selector, new Integer[]{R.id.vs_hr_normal, R.id.vs_hr_bradi, R.id.vs_hr_tachi}));
        put(VitalSign.BLOOD_PRESSURE, new Pair<>(R.id.vs_pa_selector, new Integer[]{R.id.vs_pa_normal, R.id.vs_pa_ipo, R.id.vs_pa_iper}));
        put(VitalSign.SPO2, new Pair<>(R.id.vs_sat_selector, new Integer[]{R.id.vs_sat_normal, R.id.vs_sat_ipo}));
        put(VitalSign.ETCO2, new Pair<>(R.id.vs_etco2_selector, new Integer[]{R.id.vs_etco2_normal, R.id.vs_etco2_ipo, R.id.vs_etco2_iper}));
        //Clinical Picture
        put(VitalSign.PUPILS, new Pair<>(R.id.vs_eye_selector, new Integer[]{R.id.vs_eye_normal, R.id.vs_eye_aniso, R.id.vs_eye_midriasi}));
        put(VitalSign.AIRWAYS, new Pair<>(R.id.vs_air_selector, new Integer[]{R.id.vs_air_sb, R.id.vs_air_sga, R.id.vs_air_tt}));
        put(VitalSign.CHEST_TUBE, new Pair<>(R.id.vs_dp_selector, new Integer[]{R.id.vs_dp_no, R.id.vs_dp_dx, R.id.vs_dp_sx, R.id.vs_dp_bi}));
        put(VitalSign.BURN, new Pair<>(R.id.vs_ustione_selector, new Integer[]{R.id.vs_ustione_not_present, R.id.vs_ustione_termica, R.id.vs_ustione_chimica}));
    }};
    private static Map<VitalSign, Integer> checkBoxMap = new HashMap<VitalSign, Integer>(){{
        //Clinical Picture
        put(VitalSign.POSITIVE_INHALATION, R.id.vs_tracheo);
        put(VitalSign.INTUBATION_FAILED, R.id.vs_failed_iot);
        put(VitalSign.HEMORRHAGE, R.id.vs_emo);
        put(VitalSign.LIMBS_FRACTURE, R.id.vs_limbs_fractures);
        put(VitalSign.FRACTURE_EXPOSITION, R.id.vs_fracture_exposition);
    }};
    private static Map<VitalSign, Integer> editTextsMap = new HashMap<VitalSign, Integer>(){{
        //CLinical Picture
        put(VitalSign.OXYGEN_PERCENTAGE, R.id.oxygen_percentage_edit);
    }};

    public PatientInitialStatusDialog(Context context) {
        super(context, R.layout.dialog_patient_initial_status);

        intiUI();
        initTabs();
    }

    private void intiUI() {
        findViewById(R.id.exitLabel).setOnClickListener(v -> {
            for(int i = 0; i < editTextsMap.size(); i++){
                ActiveTrauma.setInitialVitalSign((VitalSign) Objects.requireNonNull(editTextsMap.keySet().toArray())[i],
                        ((EditText) findViewById(editTextsMap.get(editTextsMap.keySet().toArray()[i]))).getText().toString());
            }

            dismiss();

            ActiveTrauma.notifyPatientInitialConditions();
        });

        findViewById(R.id.gcs_button).setOnClickListener(v -> {
            GCS currentGCS = new GCS(ActiveTrauma.getInitialVitalSign(VitalSign.GCS_MOTOR),
                    ActiveTrauma.getInitialVitalSign(VitalSign.GCS_VERBAL),
                    ActiveTrauma.getInitialVitalSign(VitalSign.GCS_EYES),
                    Boolean.parseBoolean(ActiveTrauma.getInitialVitalSign(VitalSign.SEDATED)));

            new GCSDialog(getContext(), currentGCS, new GCSDialog.Listener() {
                @Override
                public void onBackButtonClicked() {
                    //nothing to do!
                }

                @Override
                public void onConfirmationButtonClicked(GCS newGCS) {
                    ActiveTrauma.setInitialVitalSign(VitalSign.GCS_MOTOR, newGCS.motor());
                    ActiveTrauma.setInitialVitalSign(VitalSign.GCS_VERBAL, newGCS.verbal());
                    ActiveTrauma.setInitialVitalSign(VitalSign.GCS_EYES, newGCS.eyes());
                    ActiveTrauma.setInitialVitalSign(VitalSign.GCS_TOTAL, String.valueOf(newGCS.total()));
                    ActiveTrauma.setInitialVitalSign(VitalSign.SEDATED, String.valueOf(newGCS.sedated()));
                }
            }).show();
        });

        radioGroupsMap.forEach((vs,resPair) -> initVitalSignRadioGroup(resPair.first, vs, resPair.second));
        checkBoxMap.forEach((vs,res) -> initVitalSignCheckBox(res, vs));
        editTextsMap.forEach((vs,res) -> initVitalSignEditText(res, vs));
    }

    private void initTabs(){
        final int[] tabs = new int[]{R.id.tab_vs, R.id.tab_clinical};

        for(int i = 0; i < tabs.length; i++){
            final int tabId = i;
            findViewById(tabs[i]).setOnClickListener(v -> switchTab(tabs, tabId));
        }

        switchTab(tabs, 0);
    }

    private void switchTab(int[] tabs, int id){
        int[] containers = new int[]{R.id.tab_vs_container, R.id.tab_clinical_container};

        for(int i = 0; i < tabs.length; i++){
            TextView tab = findViewById(tabs[i]);
            tab.setBackgroundResource(R.drawable.traumatracker_tab);
            tab.setTypeface(null, Typeface.NORMAL);

            findViewById(containers[i]).setVisibility(View.GONE);
        }

        TextView selectedTab = findViewById(tabs[id]);
        selectedTab.setBackgroundResource(R.drawable.traumatracker_tab_selected);
        selectedTab.setTypeface(null, Typeface.BOLD);

        findViewById(containers[id]).setVisibility(View.VISIBLE);
    }

    private void initVitalSignCheckBox(int res, final VitalSign vs){
        final CheckBox cb = findViewById(res);

        if(!ActiveTrauma.getInitialVitalSign(vs).isEmpty()) {
            final String value = ActiveTrauma.getInitialVitalSign(vs);

            if(value.equals(getString(R.string.switch_yes_label).toLowerCase())){
                cb.setChecked(true);
            } else if (value.equals(getString(R.string.switch_no_label).toLowerCase())){
                cb.setChecked(false);
            }
        }

        cb.setOnClickListener(v -> ActiveTrauma.setInitialVitalSign(vs, getString(cb.isChecked() ? R.string.switch_yes_label : R.string.switch_no_label).toLowerCase()));
    }

    private void initVitalSignRadioGroup(int rgRes, final VitalSign vs, Integer[] rbResList){
        ((RadioGroup) findViewById(rgRes)).setOnCheckedChangeListener((radioGroup, i) -> ActiveTrauma.setInitialVitalSign(vs, findViewById(i).getTag().toString()));

        Enum<?>[] values = vs.getValues();

        for(int i = 0; i < values.length; i++){
            RadioButton rb = findViewById(rbResList[i]);
            rb.setText(values[i].toString().toUpperCase());
            rb.setTag(values[i]);

            if (values[i].toString().equals(ActiveTrauma.getInitialVitalSign(vs))) {
                ((RadioGroup) findViewById(rgRes)).check(rbResList[i]);
            }
        }
    }

    private void initVitalSignEditText(int res, VitalSign vs){
        final TraumaTrackerEditText edit = findViewById(res);
        edit.setText(ActiveTrauma.getInitialVitalSign(vs));
        edit.setOnClickListener(getContext(), vs.toString(), vs.getUnit().toString(), new QuantityChecker() {
            @Override
            public boolean checkInput(final String qty) {
                double input = Double.parseDouble(qty);
                if (vs == VitalSign.OXYGEN_PERCENTAGE) {
                    return input >= 21 && input <= 100 && (input % 1 == 0);
                }
                return true;
            }

            @Override
            public String errorMessage() {
                if (vs == VitalSign.OXYGEN_PERCENTAGE) {
                    return getContext().getString(R.string.oxygen_percentage_error);
                }
                return "";
            }
        });
    }
}
