package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.BarcodeReader;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class TraumaTerminationCodesWarningDialog extends TTDialog {

    private String patientCode, patientSdo;
    private boolean erDeceasedPatient = false;

    private Button confirmButton;

    public TraumaTerminationCodesWarningDialog(Context context, String previousPatientCode, String previousPatientSdo, final Listener listener) {
        super(context, R.layout.dialog_trauma_termination_codes_warning);

        this.patientCode = previousPatientCode;
        this.patientSdo = previousPatientSdo;

        intiUI(listener);
    }

    private void intiUI(final Listener listener) {
        findViewById(R.id.closeButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onBackButtonClicked();
            }

            dismiss();
        });

        findViewById(R.id.exitNoSavingButton).setOnClickListener(v -> new MessageDialogWithChoice(getContext(), getString(R.string.warning), getString(R.string.unsave_exit_message), new MessageDialogWithChoice.Listener() {
            @Override
            public void onNoButtonClicked() {
                //nothing to do!
            }

            @Override
            public void onYesButtonClicked() {
                listener.onExitWithoutSaving();
                dismiss();
            }
        }).show());

        confirmButton = findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(v -> {
            if (listener != null) {
                listener.onConfirmationButtonClicked(patientCode, patientSdo, erDeceasedPatient);
            }

            dismiss();
        });

        confirmButton.setEnabled(false);

        initIdCode(getString(R.string.er_code_label), findViewById(R.id.patient_code_text), patientCode, this::updatePatientCode);
        findViewById(R.id.readBagCodeButton).setOnClickListener(v -> listener.newBarcodeRequest(BarcodeReader.Requests.PATIENT_CODE_FINAL));

        initIdCode(getString(R.string.sdo_code_label), findViewById(R.id.patient_sdo_text), patientSdo, this::updatePatientSdo);
        findViewById(R.id.readSdoButton).setOnClickListener(v -> listener.newBarcodeRequest(BarcodeReader.Requests.PATIENT_SDO_FINAL));

        ((CheckBox) findViewById(R.id.cb_er_deceased_patient)).setOnCheckedChangeListener((v, isChecked) -> {
            if(isChecked){
                confirmButton.setEnabled(true);
                erDeceasedPatient = true;
            } else {
                if(patientCode.isEmpty()) {
                    confirmButton.setEnabled(false);
                }
                erDeceasedPatient = false;
            }
        });
    }

    private void initIdCode(final String description, final EditText editText, final String currentValue, KeyboardDialog.Listener onConfirmation){
        editText.setOnClickListener(v -> new KeyboardDialog(getContext(), description, currentValue, "", onConfirmation, new QuantityChecker() {

            @Override
            public boolean checkInput(String code) {
                return code.length() <= C.PATIENT_CODE_MAX_DIGITS;
            }

            @Override
            public String errorMessage() {
                return "Il codice deve essere di massimo " + C.PATIENT_CODE_MAX_DIGITS + " cifre!";
            }
        }).setIntegerValue(true).setUnsignedValue(true).show());

        if(currentValue.equals("")){
            editText.setText("---");
        } else {
            editText.setText(currentValue);
        }
    }

    public void updatePatientCode(String code) {
        if(code.equals("empty"))
            return;

        patientCode = code;
        ((EditText) findViewById(R.id.patient_code_text)).setText(code);

        if(patientCode.isEmpty()){
            confirmButton.setEnabled(false);
        } else {
            confirmButton.setEnabled(true);
        }
    }

    public void updatePatientSdo(String sdo) {
        if(sdo.equals("empty"))
            return;

        patientSdo = sdo;
        ((EditText) findViewById(R.id.patient_sdo_text)).setText(sdo);
    }

    public interface Listener {
        void onBackButtonClicked();
        void newBarcodeRequest(String type);
        void onConfirmationButtonClicked(String patientCode, String patientSDO, boolean erDeceasedPatient);
        void onExitWithoutSaving();
    }
}
