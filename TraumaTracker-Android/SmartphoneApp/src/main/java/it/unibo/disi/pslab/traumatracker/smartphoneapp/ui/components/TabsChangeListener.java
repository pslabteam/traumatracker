package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components;


import android.support.v4.view.ViewPager;

public abstract class TabsChangeListener implements ViewPager.OnPageChangeListener {

    @Override
    public void onPageScrolled(int i, float v, int i1) { }

    @Override
    public void onPageScrollStateChanged(int i) { }

    @Override
    public abstract void onPageSelected(int i);
}
