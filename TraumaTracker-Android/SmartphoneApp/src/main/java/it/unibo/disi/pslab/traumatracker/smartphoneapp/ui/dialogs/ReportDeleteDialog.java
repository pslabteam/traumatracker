package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TinyReport;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class ReportDeleteDialog<T> extends TTDialog {

    public ReportDeleteDialog(Context context, final T report, final Listener listener) {
        super(context, R.layout.dialog_reports_archive_delete);
        initUI(report, listener);
    }

    private void initUI(final T report, final Listener listener){
        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.closeButton).setOnClickListener(v -> {
            boolean success = deleteReport(report);

            if (success) {
                listener.onReportSuccessfullyDeleted();
            } else {
                new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.delete_report_error_message), null).show();
            }

            dismiss();
        });
    }

    private boolean deleteReport(final T report){
        if(report instanceof Report){
            Report r = (Report) report;
            return FileSystem.deleteReportFromLocal(r.id(), r.isSync());
        }

        if(report instanceof TinyReport){
            TinyReport r = (TinyReport) report;
            return FileSystem.deleteTemporayReportFromLocal(r.id());
        }

        return false;
    }

    public interface Listener{
        void onReportSuccessfullyDeleted();
    }
}
