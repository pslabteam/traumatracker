package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.KeyboardDialog;

@SuppressLint("AppCompatCustomView")
public class TraumaTrackerEditText extends EditText {

    private TraumaTrackerEditText.Event eventListener;

    public TraumaTrackerEditText(Context context) {
        super(context);
    }

    public TraumaTrackerEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TraumaTrackerEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnClickListener(final Context ctx, final String title, final String unit, boolean signed, boolean decimal, final QuantityChecker checker) {
        setOnClickListener(v -> new KeyboardDialog(ctx, title, getText().toString(), unit, value -> {
            setText(String.format("%s", value));
            if (eventListener != null) {
                eventListener.onTextUpdated(value);
            }
        }, checker).setUnsignedValue(!signed).setIntegerValue(!decimal).show());
    }

    public void setOnClickListener(final Context ctx, final String title, final String unit, final QuantityChecker checker) {
        setOnClickListener(v -> new KeyboardDialog(ctx, title, getText().toString(), unit, value -> {
            setText(String.format("%s", value));
            if (eventListener != null) {
                eventListener.onTextUpdated(value);
            }
        }, checker).show());
    }

    public void registerTextUpdateListener(TraumaTrackerEditText.Event event){
        eventListener = event;
    }

    public interface Event {
        void onTextUpdated(String text);
    }
}
