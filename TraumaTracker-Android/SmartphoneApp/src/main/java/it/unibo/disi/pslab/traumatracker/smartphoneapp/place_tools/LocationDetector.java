package it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools;

public interface LocationDetector {

    void startDetection();
    void stopDetection();
    void setOnPlaceUpdateAvailableListener(final LocationDetector.Listener listener);

    interface Listener{
        void placeUpdate(final String place);
    }

    enum Status {
        UNREACHABLE("unreachable"),
        IDLE("idle"),
        ACTIVE("active");

        private String value;

        Status(String value){
            this.value = value;
        }

        public String value() {
            return value;
        }
    }
}
