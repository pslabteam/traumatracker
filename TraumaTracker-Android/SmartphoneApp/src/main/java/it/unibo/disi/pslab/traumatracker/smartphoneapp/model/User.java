package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import android.support.annotation.NonNull;

import java.io.IOException;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.exceptions.UserNotFoundException;

public class User {
    private String id;
    private String name;
    private String surname;
    private String role;

    public User(String id, String name, String surname, String role){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.role = role;
    }

    public String id(){
        return id;
    }

    public String name() {
        return name;
    }

    public String surname() {
        return surname;
    }

    public String role() {
        return role;
    }

    @NonNull
    public String toString(){
        return name + " " + surname;
    }

    public static User getById(String id) throws UserNotFoundException {
        try {
            for (User u : FileSystem.getUserList()) {
                if(u.id().equals(id)){
                    return u;
                }
            }
            throw new UserNotFoundException();
        } catch (IOException e) {
            throw new UserNotFoundException();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof User){
            return id.equals(((User) obj).id)
                    && name.equals(((User) obj).name)
                    && surname.equals(((User) obj).surname)
                    && role.equals(((User) obj).role);
        }

        return super.equals(obj);
    }
}
