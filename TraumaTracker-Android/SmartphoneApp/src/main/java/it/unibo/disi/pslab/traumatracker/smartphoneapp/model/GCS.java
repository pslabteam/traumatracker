package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;

public class GCS {
    private String m, v, e;
    private boolean sedated;

    public GCS(String m, String v, String e, boolean sedated) {
        this.m = m;
        this.v = v;
        this.e = e;
        this.sedated = sedated;
    }

    public String motor() {
        return m;
    }

    public String verbal() {
        return v;
    }

    public String eyes() {
        return e;
    }

    public boolean sedated(){
        return sedated;
    }

    public int total() {
        int m = convertGcsValue(VitalSign.GCS_MOTOR, this.m);
        int v = convertGcsValue(VitalSign.GCS_VERBAL, this.v);
        int e = convertGcsValue(VitalSign.GCS_EYES, this.e);

        if(m == 0 || v == 0 || e == 0)
            return 0;

        return m + v + e;
    }

    public void motor(String m) {
        this.m = m;
    }

    public void verbal(String v) {
        this.v = v;
    }

    public void eyes(String e) {
        this.e = e;
    }

    public void sedated(boolean s){
        this.sedated = s;
    }

    @NonNull
    @Override
    public String toString() {
        return total() + " (" + m + "m " + v + "v " + e + "e)";
    }

    private static int convertGcsValue(VitalSign vs, String value) {
        Map<String, Integer> mMap = new HashMap<String, Integer>() {{
            put(VitalSign.GcsMotors.GCS_MOTOR_6.toString().toUpperCase(), 6);
            put(VitalSign.GcsMotors.GCS_MOTOR_5.toString().toUpperCase(), 5);
            put(VitalSign.GcsMotors.GCS_MOTOR_4.toString().toUpperCase(), 4);
            put(VitalSign.GcsMotors.GCS_MOTOR_3.toString().toUpperCase(), 3);
            put(VitalSign.GcsMotors.GCS_MOTOR_2.toString().toUpperCase(), 2);
            put(VitalSign.GcsMotors.GCS_MOTOR_1.toString().toUpperCase(), 1);
        }};

        Map<String, Integer> vMap = new HashMap<String, Integer>() {{
            put(VitalSign.GcsVerbal.GCS_VERBAL_5.toString().toUpperCase(), 5);
            put(VitalSign.GcsVerbal.GCS_VERBAL_4.toString().toUpperCase(), 4);
            put(VitalSign.GcsVerbal.GCS_VERBAL_3.toString().toUpperCase(), 3);
            put(VitalSign.GcsVerbal.GCS_VERBAL_2.toString().toUpperCase(), 2);
            put(VitalSign.GcsVerbal.GCS_VERBAL_1.toString().toUpperCase(), 1);
        }};

        Map<String, Integer> eMap = new HashMap<String, Integer>() {{
            put(VitalSign.GcsEyes.GCS_EYES_4.toString().toUpperCase(), 4);
            put(VitalSign.GcsEyes.GCS_EYES_3.toString().toUpperCase(), 3);
            put(VitalSign.GcsEyes.GCS_EYES_2.toString().toUpperCase(), 2);
            put(VitalSign.GcsEyes.GCS_EYES_1.toString().toUpperCase(), 1);
        }};

        if (vs.equals(VitalSign.GCS_MOTOR) && mMap.containsKey(value)) {
            return mMap.get(value);
        }

        if (vs.equals(VitalSign.GCS_VERBAL) && vMap.containsKey(value)) {
            return vMap.get(value);
        }

        if (vs.equals(VitalSign.GCS_EYES) && eMap.containsKey(value)) {
            return eMap.get(value);
        }

        return 0;
    }
}
