package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.LocationService;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.Note;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.NoteStream;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.TimeSheet;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Room;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TimeDependentProcedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class TraumaPathDialog extends TTDialog {

    public TraumaPathDialog(final Context context, TraumaPathDialog.Listener listener) {
        super(context, R.layout.dialog_trauma_path);

        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());

        findViewById(R.id.btn_manual_place_update).setOnClickListener(v -> new PlaceSelectionDialog(getContext(), selectedPlace -> {
            listener.onManualPlaceSelectionPerformed(selectedPlace);
            dismiss();
        }).show());

        findViewById(R.id.location_detection_status_warning_label)
                .setVisibility(LocationService.isLocationDetectionActive() ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        TimeSheet ts = TimeSheet.reference();

        if(ts != null){
            ts.registerListener(timeSheetListener);
        }

        final Report.EventsTrack currentEventsTrack = new Report.EventsTrack();
        final List<Note> events = NoteStream.requestEventsList();

        for(int i = 0; i < events.size(); i++){
            final JSONObject event = events.get(i).getContent();

            try {
                final String eventType = event.optString(C.report.events.TYPE);
                if(eventType.equals(C.report.events.type.ROOM_IN) || eventType.equals(C.report.events.type.ROOM_OUT) || eventType.equals(C.report.events.type.PATIENT_ACCEPTED)){
                    currentEventsTrack.add(new Report.Event(
                            event.optInt(C.report.events.ID),
                            event.optString(C.report.events.DATE),
                            event.optString(C.report.events.TIME),
                            event.optString(C.report.events.PLACE),
                            event.optString(C.report.events.TYPE),
                            event.getJSONObject(C.report.events.CONTENT)
                    ));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        fillEventsView(currentEventsTrack);
    }

    @Override
    protected void onStop() {
        super.onStop();

        TimeSheet ts = TimeSheet.reference();

        if(ts != null){
            ts.unregisterListener(timeSheetListener);
        }
    }

    private void fillEventsView(final Report.EventsTrack eventsTrack){
        TableLayout eventsTable = findViewById(R.id.table_events);

        eventsTable.removeAllViews();

        if (eventsTrack.isEmpty()) {
            TableRow row = new TableRow(getContext());
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));

            row.addView(createLabelForRow("--"));
            row.addView(createLabelForRow("--"));
            row.addView(createLabelForRow("--"));

            eventsTable.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));

            return;
        }

        final List<Report.Event> events = new ArrayList<>(eventsTrack.getEvents());
        Collections.reverse(events);

        for (int i = 0; i < events.size(); i++) {
            Report.Event event = events.get(i);

            TableRow row = new TableRow(getContext());
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
            row.setPadding(2,2,2,2);

            row.addView(createLabelForRow(event.time()));
            row.addView(createIconForRow(event.type(), event.place()));
            row.addView(createLabelForRow(event.description()));

            eventsTable.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
        }
    }

    private ImageView createIconForRow(final String eventType, final String eventPlace){
        ImageView icon = new ImageView(getContext());

        if(eventType.equals(C.report.events.type.ROOM_IN)) {
            if(eventPlace.equals(C.place.PREH)){
                icon.setImageResource(R.drawable.icon_place_team);
            } else if(eventPlace.equals(C.place.TRANSPORT)){
                icon.setImageResource(R.drawable.icon_place_transport);
            } else {
                icon.setImageResource(R.drawable.icon_place_enter);
            }
        }

        if(eventType.equals(C.report.events.type.ROOM_OUT)) {
            if(eventPlace.equals(C.place.TRANSPORT)){
                icon.setImageResource(R.drawable.icon_place_transport);
            } else {
                icon.setImageResource(R.drawable.icon_place_exit);
            }

        }

        if(eventType.equals(C.report.events.type.PATIENT_ACCEPTED)) {
            icon.setImageResource(R.drawable.icon_place_emergency);
        }

        TableRow.LayoutParams params = new TableRow.LayoutParams(60, 60);
        params.setMargins(3,3,3,3);

        icon.setLayoutParams(params);
        icon.setPadding(5, 0, 5, 0);

        return icon;
    }

    private TextView createLabelForRow(final String text) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER_VERTICAL;

        TextView txt = new TextView(getContext());
        txt.setText(text);
        txt.setLayoutParams(params);
        txt.setTextSize(14f);

        return txt;
    }

    private TimeSheet.Listener timeSheetListener = new TimeSheet.Listener() {
        @Override
        public void updateRoomInfo(Room room) {
            ((TextView)findViewById(R.id.timesheet_room_description)).setText(room.description());
            ((TextView)findViewById(R.id.timesheet_room_datetime)).setText(String.format(getString(R.string.time_sheet_room_label), room.inTime(), room.inDate()));
            ((TextView)findViewById(R.id.timesheet_room_permanence)).setText(String.format(getString(R.string.time_sheet_room_duration), room.permanenceTime()));
        }

        /*
        @Override
        public void updateVitalSigns(final int sys, final int dia, final int hr, final int etco2, final int spo2, final int temp){

        }

        @Override
        public void notifyMonitorUnreachableAlert(){

        }*/

        @Override
        public void updateTimeDependentProcedureInfo(TimeDependentProcedure currentTdp) {

        }
    };


    public interface Listener{
        void onManualPlaceSelectionPerformed(final String place);
    }
}
