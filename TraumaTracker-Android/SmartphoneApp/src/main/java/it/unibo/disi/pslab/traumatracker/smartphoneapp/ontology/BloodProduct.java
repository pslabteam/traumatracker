package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum BloodProduct {
    PTM("mtp", R.string.ontology_drug_ptm, Dosage.NULL, false),
    CONDENSED_EMAZIES("emazies", R.string.ontology_drug_emazies, Dosage.FIXED, true, Uom.UI, 1),
    PLATELETS("platelets", R.string.ontology_drug_platelets, Dosage.FIXED, true, Uom.UI, 1),
    FRESH_FROZEN_PLASMA("fresh-frozen-plasma", R.string.ontology_drug_freshfrozenplasma, Dosage.FIXED, true, Uom.UI, 1),
    FIBRINOGEN("fibrinogen", R.string.ontology_drug_fibrinogen, Dosage.FIXED, false, Uom.G, 1),
    PROTHROMBINIC_COMPLEX("prothrombinic-complex", R.string.ontology_drug_prothrombiniccomplex, Dosage.FIXED, false, Uom.UI, 500);

    private final String id;
    private final int labelRes;
    private final Dosage dosageType;
    private final boolean hasBag;
    private final Uom unit;
    private final double defaultValue;

    BloodProduct(final String id, final int labelRes, final Dosage dosageType, final boolean hasBag, final Uom unit, final double defaultValue){
        this.id = id;
        this.labelRes = labelRes;
        this.dosageType = dosageType;
        this.hasBag = hasBag;
        this.unit = unit;
        this.defaultValue = defaultValue;
    }

    BloodProduct(final String id, final int labelRes, final Dosage dosageType, final boolean hasBag){
        this.id = id;
        this.labelRes = labelRes;
        this.dosageType = dosageType;
        this.hasBag = hasBag;
        this.unit = null;
        this.defaultValue = 0;
    }

    public String id(){
        return id;
    }

    public Dosage dosageType(){
        return dosageType;
    }

    public boolean hasBag() {
        return hasBag;
    }

    public Uom unit() {
        return unit;
    }

    public double defaultValue() {
        return defaultValue;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }
}
