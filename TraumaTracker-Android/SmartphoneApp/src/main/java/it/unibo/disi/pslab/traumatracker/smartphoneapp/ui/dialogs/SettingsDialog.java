package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;

public class SettingsDialog extends TTDialog {

    private SharedPreferences preferences;

    private String currentLanguage;

    public SettingsDialog(final Context context, final Listener listener) {
        super(context, R.layout.dialog_settings);

        preferences = getContext().getSharedPreferences(Preferences.ID, Context.MODE_PRIVATE);

        initUI(listener);
    }

    private void initUI(final Listener listener){
        findViewById(R.id.exitLabel).setOnClickListener(v -> {
            saveSharedPreferences();
            listener.onSettingsSaved();
            dismiss();
        });

        initCreditsSection();
        initConfigurationSection(listener);
        initInfrastructureSection(listener);
        initLanguageSection();
    }

    private void initCreditsSection(){
        try {
            ((TextView) findViewById(R.id.version_label)).setText(String.format("TRAUMA TRACKER\n ver. %s",
                    getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ApplySharedPref")
    private void initConfigurationSection(final Listener listener){
        ((RadioGroup) findViewById(R.id.log_switch)).setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = findViewById(checkedId);
            preferences.edit()
                    .putBoolean(Preferences.LOGGING, rb.getText().equals(getString(R.string.switch_on_label)))
                    .commit();
        });

        ((RadioButton) findViewById(preferences.getBoolean(Preferences.LOGGING, C.LOG_ACTIVE) ? R.id.log_on : R.id.log_off)).setChecked(true);

        findViewById(R.id.clearLogButton).setOnClickListener(v -> {
            FileSystemUtils.emptyFolder(C.fs.LOG_FOLDER);
            new MessageDialog(getContext(), "App Log", "Tutti i file di Log sono stati cancellati!", null).show();
        });

        ((RadioGroup) findViewById(R.id.glass_switch)).setOnCheckedChangeListener((radioGroup, checkedRadioButton) -> {
            RadioButton rb = findViewById(checkedRadioButton);

            if (rb.getText().equals(getString(R.string.switch_on_label))) {
                listener.onGlassesSupportStatusChanged(true);
                preferences.edit()
                        .putBoolean(Preferences.GLASSES_SUPPORT, true)
                        .commit();
            } else {
                listener.onGlassesSupportStatusChanged(false);
                preferences.edit()
                        .putBoolean(Preferences.GLASSES_SUPPORT, false)
                        .commit();
            }
        });

        ((RadioButton) findViewById(preferences.getBoolean(Preferences.GLASSES_SUPPORT, false) ? R.id.glass_on : R.id.glass_off)).setChecked(true);
    }

    private void initInfrastructureSection(final Listener listener){
        ((EditText) findViewById(R.id.tt_service_ip)).setText(preferences.getString(Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP));
        ((EditText) findViewById(R.id.preh_service_ip)).setText(preferences.getString(Preferences.PREH_SERVICE_IP, C.url.DEFAULT_PREH_SERVICE_IP));
        ((EditText) findViewById(R.id.ble_tag_id)).setText(preferences.getString(Preferences.BLE_TAG_ID, C.url.DEFAULT_BLE_TAG_ID));

        findViewById(R.id.requestUsersListButton).setOnClickListener(v -> {
            saveSharedPreferences();
            listener.onRequestUserListButtonClicked();
        });
        findViewById(R.id.requestRoomsListButton).setOnClickListener(v -> {
            saveSharedPreferences();
            listener.onRequestRoomsListButtonClicked();
        });
    }

    @SuppressLint("ApplySharedPref")
    private void initLanguageSection(){
        ((RadioGroup) findViewById(R.id.language_switch)).setOnCheckedChangeListener((group, checkedId) -> {
            final String newLanguage = findViewById(checkedId).getTag().toString();

            preferences.edit()
                    .putString(Preferences.LANGUAGE, newLanguage)
                    .commit();

            findViewById(R.id.languageMessageLabel).setVisibility(currentLanguage.equals(newLanguage) ? View.GONE : View.VISIBLE);
        });

        currentLanguage = preferences.getString(Preferences.LANGUAGE, C.languages.EN);

        switch (currentLanguage){
            case C.languages.EN:
                ((RadioButton) findViewById(R.id.language_switch_en)).setChecked(true);
                break;

            case C.languages.IT:
                ((RadioButton) findViewById(R.id.language_switch_it)).setChecked(true);
                break;
        }

        findViewById(R.id.languageMessageLabel).setVisibility(View.GONE);
    }

    @SuppressLint("ApplySharedPref")
    private void saveSharedPreferences(){
        preferences.edit()
                .putString(Preferences.TT_SERVICE_IP, ((EditText) findViewById(R.id.tt_service_ip)).getText().toString())
                .putString(Preferences.PREH_SERVICE_IP, ((EditText) findViewById(R.id.preh_service_ip)).getText().toString())
                .putString(Preferences.BLE_TAG_ID, ((EditText) findViewById(R.id.ble_tag_id)).getText().toString())
                .commit();
    }

    public interface Listener {
        void onSettingsSaved();
        void onRequestUserListButtonClicked();
        void onRequestRoomsListButtonClicked();
        void onGlassesSupportStatusChanged(boolean supportActive);
    }
}
