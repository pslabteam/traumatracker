package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.AnamnesiElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.MajorTraumaCriteriaElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class Report {
    private String id;
    private String operator;
    private List<String> traumaTeam;
    private String startDate, startTime;
    private String endDate, endTime;
    private boolean delayedActivation;
    private String delayedActivationDate;
    private String delayedActivationTime;
    private String destination;
    private String iss;
    private boolean issIsPresumed;

    private Preh preh;
    private TraumaInfo ti;
    private InitialVitalSigns vs;
    private EventsTrack et;

    private boolean syncFlag;

    public Report() { }

    public String id() {
        return id;
    }

    public String operator() {
        return operator;
    }

    public List<String> traumaTeam() {
        return traumaTeam;
    }

    public String startDate() {
        return startDate;
    }

    public String startTime() {
        return startTime;
    }

    public String endDate() {
        return endDate;
    }

    public String endTime() {
        return endTime;
    }

    public boolean delayedActivation() {
        return delayedActivation;
    }

    public String delayedActivationDate() {
        return delayedActivationDate;
    }

    public String delayedActivationTime() {
        return delayedActivationTime;
    }

    public String finalDestination() {
        return destination;
    }

    public String iss() {
        return iss;
    }

    public boolean issIsPresumed() { return issIsPresumed; }

    public TraumaInfo traumaInfo() {
        return this.ti;
    }

    public Preh preh() {
        return this.preh;
    }

    public InitialVitalSigns initialVitalSigns() {
        return this.vs;
    }

    public EventsTrack eventTrack() {
        return this.et;
    }

    public boolean isSync() {
        return syncFlag;
    }

    public void sync(boolean value){
        syncFlag = value;
    }

    public static Report build(final String content, boolean sync) {
        try {
            final JSONObject obj = new JSONObject(content);

            final Report.Builder reportBuilder = new Report.Builder();

            reportBuilder
                    .id(obj.optString(C.report.ID))
                    .operator(obj.optString(C.report.START_OPERATOR_ID))
                    .traumaTeam(buildTraumaTeamMembers(obj.optJSONArray(C.report.TRAUMA_TEAM_MEMBERS)))
                    .startDate(obj.optString(C.report.START_DATE))
                    .startTime(obj.optString(C.report.START_TIME))
                    .endDate(obj.optString(C.report.END_DATE))
                    .endTime(obj.optString(C.report.END_TIME))
                    .destination(obj.optString(C.report.FINAL_DESTINATION));

            final JSONObject delayedActivationObj = obj.optJSONObject(C.report.DELAYED_ACTIVATION);

            if(delayedActivationObj != null){
                reportBuilder
                        .delayedActivation(delayedActivationObj.optBoolean(C.report.delayedActivation.STATUS))
                        .delayedActivationDate(delayedActivationObj.optString(C.report.delayedActivation.ORIGINAL_ACCESS_DATE))
                        .delayedActivationTime(delayedActivationObj.optString(C.report.delayedActivation.ORIGINAL_ACCESS_TIME));
            }

            final JSONObject issObj = obj.optJSONObject(C.report.ISS);

            if(issObj != null){
                reportBuilder.iss(issObj.optString(C.report.iss.TOTAL_ISS));
                reportBuilder.issFinal(issObj.optBoolean(C.report.iss.IS_PRESUMED));
            } else {
                reportBuilder.iss("");
                reportBuilder.issFinal(false);
            }

            JSONObject prehObj = obj.optJSONObject(C.report.PREH);

            if(prehObj != null){
                reportBuilder.preh(new Preh(
                        prehObj.optString(C.report.preh.TERRITORIAL_AREA),
                        prehObj.optString(C.report.preh.CAR_ACCIDENT),
                        prehObj.optString(C.report.preh.A_AIRWAYS),
                        prehObj.optString(C.report.preh.B_PLEURAL_DECOMPRESSION),
                        prehObj.optString(C.report.preh.C_BLOOD_PROTOCOL),
                        prehObj.optString(C.report.preh.C_TPOD),
                        prehObj.optString(C.report.preh.D_GCS_TOTAL),
                        prehObj.optString(C.report.preh.D_ANISOCORIA),
                        prehObj.optString(C.report.preh.D_MIDRIASI),
                        prehObj.optString(C.report.preh.E_MOTILITY),
                        prehObj.optString(C.report.preh.WORST_BLOOD_PRESSURE),
                        prehObj.optString(C.report.preh.WORST_RESPIRATORY_RATE)));
            } else {
                reportBuilder.preh(new Preh());
            }

            JSONObject traumaObj = obj.optJSONObject(C.report.TRAUMA_INFO);
            JSONObject mtcObj = obj.optJSONObject(C.report.MAJOR_TRAUMA_CRITERIA);
            JSONObject anamnesiObj = obj.optJSONObject(C.report.ANAMNESI);

            if(traumaObj != null && anamnesiObj != null && mtcObj != null){
                reportBuilder.traumaInfo(new TraumaInfo(
                    traumaObj.optString(C.report.traumaInfo.CODE),
                    traumaObj.optString(C.report.traumaInfo.SDO),
                    traumaObj.optString(C.report.traumaInfo.ADMISSION_CODE),
                    traumaObj.optString(C.report.traumaInfo.NAME),
                    traumaObj.optString(C.report.traumaInfo.SURNAME),
                    traumaObj.optString(C.report.traumaInfo.GENDER),
                    traumaObj.optString(C.report.traumaInfo.AGE),
                    traumaObj.optString(C.report.traumaInfo.DOB),
                    buildMtcDescription(mtcObj),
                    buildAnamnesiDescription(anamnesiObj),
                    traumaObj.optString(C.report.traumaInfo.ACCIDENT_DATE),
                    traumaObj.optString(C.report.traumaInfo.ACCIDENT_TIME),
                    traumaObj.optString(C.report.traumaInfo.VEHICLE),
                    traumaObj.optString(C.report.traumaInfo.FROM_OTHER_EMERGENCY),
                    traumaObj.optString(C.report.traumaInfo.OTHER_EMERGENCY)));
            } else {
                reportBuilder.traumaInfo(new TraumaInfo());
            }

            final JSONObject vsObj = obj.optJSONObject(C.report.PATIENT_INITIAL_CONDITION).optJSONObject(C.report.patientInitialCondition.VITAL_SIGNS);
            final JSONObject cpObj = obj.optJSONObject(C.report.PATIENT_INITIAL_CONDITION).optJSONObject(C.report.patientInitialCondition.CLINICAL_PICTURE);

            if(vsObj != null && cpObj != null){
                reportBuilder.initialVitalSigns(new Report.InitialVitalSigns(
                        vsObj.optString(C.report.patientInitialCondition.vitalSigns.TEMPERATURE),
                        vsObj.optString(C.report.patientInitialCondition.vitalSigns.HEART_RATE),
                        vsObj.optString(C.report.patientInitialCondition.vitalSigns.BLOOD_PRESSURE),
                        vsObj.optString(C.report.patientInitialCondition.vitalSigns.SPO2),
                        vsObj.optString(C.report.patientInitialCondition.vitalSigns.ETCO2),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.HEMORRHAGE),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.AIRWAYS),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.OXYGEN_PERCENTAGE),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.POSITIVE_INHALATION),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.INTUBATION_FAILED),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.CHEST_TUBE),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.GCS_TOTAL),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.GCS_MOTOR),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.GCS_VERBAL),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.GCS_EYES),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.SEDATED),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.PUPILS),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.LIMBS_FRACTURE),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.FRACTURE_EXPOSITION),
                        cpObj.optString(C.report.patientInitialCondition.clinicalPicture.BURN)));
            } else {
                reportBuilder.initialVitalSigns(new Report.InitialVitalSigns());
            }

            JSONArray events = obj.optJSONArray(C.report.EVENTS);

            EventsTrack reportEventTrack = new EventsTrack();

            if(events != null) {
                for (int i = 0; i < events.length(); i++) {
                    JSONObject event = events.getJSONObject(i);

                    reportEventTrack.add(new Report.Event(
                            event.optInt(C.report.events.ID),
                            event.optString(C.report.events.DATE),
                            event.optString(C.report.events.TIME),
                            event.optString(C.report.events.PLACE),
                            event.optString(C.report.events.TYPE),
                            event.optJSONObject(C.report.events.CONTENT)
                    ));
                }
            }

            reportBuilder.eventTrack(reportEventTrack);

            return reportBuilder.syncFlag(sync).build();

        } catch (JSONException e) {
            Log.e(C.LOG_TAG, "Error in building report. Check Reports folder!");
        }

        return null;
    }

    private static Map<AnamnesiElement, Boolean> buildAnamnesiDescription(JSONObject anamnesiObj) {
        Map<AnamnesiElement, Boolean> anamnesi = new HashMap<>();

        if(anamnesiObj != null){
            anamnesi.put(AnamnesiElement.ANTIPLATELETS, anamnesiObj.optBoolean(C.report.anamnesi.ANTIPLATELETS));
            anamnesi.put(AnamnesiElement.ANTICOAGULANTS, anamnesiObj.optBoolean(C.report.anamnesi.ANTICOAGULANTS));
            anamnesi.put(AnamnesiElement.NAO, anamnesiObj.optBoolean(C.report.anamnesi.NAO));
        }

        return anamnesi;
    }

    private static Map<MajorTraumaCriteriaElement, Boolean> buildMtcDescription(JSONObject mtcObj) {
        Map<MajorTraumaCriteriaElement, Boolean> mtc = new HashMap<>();

        if(mtcObj != null){
            mtc.put(MajorTraumaCriteriaElement.DYNAMIC, mtcObj.optBoolean(C.report.majorTraumaCriteria.DYNAMIC));
            mtc.put(MajorTraumaCriteriaElement.PHYSIOLOGICAL, mtcObj.optBoolean(C.report.majorTraumaCriteria.PHYSIOLOGICAL));
            mtc.put(MajorTraumaCriteriaElement.ANATOMICAL, mtcObj.optBoolean(C.report.majorTraumaCriteria.ANATOMICAL));
        }

        return mtc;
    }

    private static List<String> buildTraumaTeamMembers(JSONArray ttMembers) throws JSONException {
        List<String> members = new ArrayList<>();

        if (ttMembers != null && ttMembers.length() > 0) {
            for (int i = 0; i < ttMembers.length(); i++) {
                members.add(ttMembers.getString(i));
            }
        }

        return members;
    }

    private static class Builder {
        private String id;
        private String operator;
        private List<String> traumaTeam;
        private String startDate, startTime;
        private String endDate, endTime;
        private boolean delayedActivation;
        private String delayedActivationDate;
        private String delayedActivationTime;
        private String destination;
        private String iss;
        private boolean issFinal;

        private Preh preh;
        private TraumaInfo ti;
        private InitialVitalSigns vs;
        private EventsTrack et;

        private boolean syncFlag;

        Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        Builder operator(String operator) {
            this.operator = operator;
            return this;
        }

        Builder traumaTeam(List<String> traumaTeam) {
            this.traumaTeam = traumaTeam;
            return this;
        }

        Builder startDate(String startDate) {
            this.startDate = startDate;
            return this;
        }

        Builder startTime(String startTime) {
            this.startTime = startTime;
            return this;
        }

        Builder endDate(String endDate) {
            this.endDate = endDate;
            return this;
        }

        Builder endTime(String endTime) {
            this.endTime = endTime;
            return this;
        }

        Builder delayedActivation(boolean delayedActivation) {
            this.delayedActivation = delayedActivation;
            return this;
        }

        Builder delayedActivationDate(String delayedActivationDate) {
            this.delayedActivationDate = delayedActivationDate;
            return this;
        }

        Builder delayedActivationTime(String delayedActivationTime) {
            this.delayedActivationTime = delayedActivationTime;
            return this;
        }

        Builder destination(String destination) {
            this.destination = destination;
            return this;
        }

        Builder iss(String iss) {
            this.iss = iss;
            return this;
        }

        Builder issFinal(boolean issFinal) {
            this.issFinal = issFinal;
            return this;
        }

        Builder preh(Preh preh) {
            this.preh = preh;
            return this;
        }

        Builder traumaInfo(TraumaInfo ti) {
            this.ti = ti;
            return this;
        }

        Builder initialVitalSigns(InitialVitalSigns vs) {
            this.vs = vs;
            return this;
        }

        Builder eventTrack(EventsTrack et) {
            this.et = et;
            return this;
        }

        Builder syncFlag(boolean syncFlag) {
            this.syncFlag = syncFlag;
            return this;
        }

        Report build() {
            Report report = new Report();

            report.id = id;
            report.operator = operator;
            report.traumaTeam = traumaTeam;
            report.startDate = startDate;
            report.startTime = startTime;
            report.endDate = endDate;
            report.endTime = endTime;
            report.delayedActivation = delayedActivation;
            report.delayedActivationDate = delayedActivationDate;
            report.delayedActivationTime = delayedActivationTime;
            report.destination = destination;
            report.iss = iss;
            report.issIsPresumed = issFinal;
            report.preh = preh;
            report.ti = ti;
            report.vs = vs;
            report.et = et;
            report.syncFlag = syncFlag;

            return report;
        }
    }

    public static class Preh {
        private String territorialArea, carAccident,
                a, bPleuralDecompression, cBloodProtocol, cTpod, dGcsTot, dAnisocoria, dMidriasi, eParaTetraParesi,
                wrostBloodPressure, wrostRespiratoryRate;

        Preh(){ }

        Preh(String territorialArea, String carAccident, String a, String bPleuralDecompression, String cBloodProtocol, String cTpod, String dGcsTot, String dAnisocoria, String dMidriasi, String eParaTetraParesi, String wrostBloodPressure, String wrostRespiratoryRate) {
            this.territorialArea = territorialArea;
            this.carAccident = carAccident;
            this.a = a;
            this.bPleuralDecompression = bPleuralDecompression;
            this.cBloodProtocol = cBloodProtocol;
            this.cTpod = cTpod;
            this.dGcsTot = dGcsTot;
            this.dAnisocoria = dAnisocoria;
            this.dMidriasi = dMidriasi;
            this.eParaTetraParesi = eParaTetraParesi;
            this.wrostBloodPressure = wrostBloodPressure;
            this.wrostRespiratoryRate = wrostRespiratoryRate;
        }

        public String territorialArea(){
            return territorialArea;
        }

        public String carAccident(){
            return carAccident;
        }

        public String a() {
            return a;
        }

        public String bPleuralDecompression() {
            return bPleuralDecompression;
        }

        public String cBloodProtocol() {
            return cBloodProtocol;
        }

        public String cTpod() {
            return cTpod;
        }

        public String dGcsTot() {
            return dGcsTot;
        }

        public String dAnisocoria() {
            return dAnisocoria;
        }

        public String dMidriasi() {
            return dMidriasi;
        }

        public String eParaTetraParesi() {
            return eParaTetraParesi;
        }

        public String wrostBloodPressure(){
            return wrostBloodPressure;
        }

        public String wrostRespiratoryRate(){
            return wrostRespiratoryRate;
        }
    }

    public static class TraumaInfo {
        private String code, sdo, admissionCode, name, surname, gender, age, dob, accidentDate, accidentTime, vehicle, fromOtherEmergency, otherEmergency;
        private Map<MajorTraumaCriteriaElement, Boolean> mtc = new HashMap<>();
        private Map<AnamnesiElement, Boolean> anamnesi = new HashMap<>();

        TraumaInfo(){}

        TraumaInfo(String code, String sdo, String admissionCode, String name, String surname, String gender, String age, String dob, Map<MajorTraumaCriteriaElement, Boolean> mtc, Map<AnamnesiElement, Boolean> anamnesi, String accidentDate, String accidentTime, String vehicle, String fromOtherEmergency, String otherEmergency) {
            this.code = code;
            this.sdo = sdo;
            this.admissionCode = admissionCode;
            this.name = name;
            this.surname = surname;
            this.gender = gender;
            this.age = age;
            this.dob = dob;
            this.mtc = mtc;
            this.anamnesi = anamnesi;
            this.accidentDate = accidentDate;
            this.accidentTime = accidentTime;
            this.vehicle = vehicle;
            this.fromOtherEmergency = fromOtherEmergency;
            this.otherEmergency = otherEmergency;
        }

        public String code() {
            return code;
        }

        public String sdo() {
            return sdo;
        }

        public String admissionCode() { return admissionCode; }

        public String name() {
            return name;
        }

        public String surname() {
            return surname;
        }

        public String gender() {
            return gender;
        }

        public String age() {
            return age;
        }

        public String dob() {
            return dob;
        }

        public Map<MajorTraumaCriteriaElement, Boolean> mtc() {
            return mtc;
        }

        public Map<AnamnesiElement, Boolean> anamnesi() {
            return anamnesi;
        }

        public String accidentDate() {
            return accidentDate;
        }

        public String accidentTime() {
            return accidentTime;
        }

        public String vehicle() {
            return vehicle;
        }

        public String fromOtherEmergency() {
            return fromOtherEmergency;
        }

        public String otherEmergency() {
            return otherEmergency;
        }
    }

    public static class InitialVitalSigns {
        private String temp;
        private String heart_rate;
        private String blood_pressure;
        private String spo2;
        private String etco2;

        private String external_bleedings;

        private String airways;
        private String oxygenPercentage;
        private String tracheo;
        private String failedIntubation;
        private String pleural_decompression;

        private String gcs_total;
        private String gcs_motor;
        private String gcs_verbal;
        private String gcs_eyes;
        private String sedated;
        private String pupils;

        private String limbs_fracture;
        private String exposed_fracture;

        private String burn;

        InitialVitalSigns(){}

        InitialVitalSigns(String temp, String heart_rate, String blood_pressure, String spo2, String etco2,
                          String external_bleedings,
                          String airways, String oxygenPercentage, String tracheo, String failedIntubation, String pleural_decompression,
                          String gcs_total, String gcs_motor, String gcs_verbal, String gcs_eyes, String sedated, String pupils,
                          String limbs_fracture, String exposed_fracture,
                          String burn) {
            this.temp = temp;
            this.heart_rate = heart_rate;
            this.blood_pressure = blood_pressure;
            this.spo2 = spo2;
            this.etco2 = etco2;

            this.external_bleedings = external_bleedings;

            this.airways = airways;
            this.oxygenPercentage = oxygenPercentage;
            this.tracheo = tracheo;
            this.failedIntubation = failedIntubation;
            this.pleural_decompression = pleural_decompression;

            this.gcs_total = gcs_total;
            this.gcs_motor = gcs_motor;
            this.gcs_verbal = gcs_verbal;
            this.gcs_eyes = gcs_eyes;
            this.sedated = sedated;
            this.pupils = pupils;

            this.limbs_fracture = limbs_fracture;
            this.exposed_fracture = exposed_fracture;

            this.burn = burn;
        }

        public String temperature() {
            return temp;
        }

        public String heartRate() {
            return heart_rate;
        }

        public String bloodPressure() {
            return blood_pressure;
        }

        public String spo2() {
            return spo2;
        }

        public String etco2() {
            return etco2;
        }

        public String externalBleedings() {
            return external_bleedings;
        }

        public String airways() {
            return airways;
        }

        public String oxygenPercentage() {
            return oxygenPercentage;
        }

        public String tracheo() {
            return tracheo;
        }

        public String failedIntubation() {
            return failedIntubation;
        }

        public String pleuralDecompression() {
            return pleural_decompression;
        }

        public String gcsTotal() {
            return gcs_total;
        }

        public String gcsMotor() {
            return gcs_motor;
        }

        public String gcsVerbal() {
            return gcs_verbal;
        }

        public String gcsEyes() {
            return gcs_eyes;
        }

        public String sedated() {
            return sedated;
        }

        public String pupils() {
            return pupils;
        }

        public String exposed_fracture() {
            return exposed_fracture;
        }

        public String limbs_fracture() {
            return limbs_fracture;
        }

        public String burn() {
            return burn;
        }
    }

    public static class EventsTrack {
        private List<Event> events;

        public EventsTrack() {
            events = new ArrayList<>();
        }

        public void add(Event event) {
            events.add(event);
        }

        public void updateEventTemporalInfo(final int eventId, final String date, final String time, final String place) {
            final Event event = getEventById(eventId);

            if (event != null) {
                event.date = date;
                event.time = time;
                event.place = place;
            }
        }

        public List<Event> getEvents() {
            return this.events;
        }

        public boolean isEmpty() {
            return this.events.size() == 0;
        }

        private Event getEventById(final int eventId) {
            for (final Event e : events) {
                if (e.id() == eventId) {
                    return e;
                }
            }

            return null;
        }
    }

    public static class Event {
        private int id;
        private String date;
        private String time;
        private String place;
        private String type;
        private String description;

        public Event(int id, String date, String time, String place, String type, JSONObject eventContent) throws JSONException {
            this.id = id;
            this.date = date;
            this.time = time;
            this.place = place;
            this.type = type;
            this.description = buildDescription(type, eventContent);
        }

        public int id() {
            return id;
        }

        public String date() {
            return date;
        }

        public String time() {
            return time;
        }

        public String place() {
            return place;
        }

        public String type() {
            return type;
        }

        public String description() {
            return description;
        }

        private String convertYesNo(final boolean value){
            return value
                    ? App.getAppResources().getString(R.string.switch_yes_label).toLowerCase()
                    : App.getAppResources().getString(R.string.switch_no_label).toLowerCase();
        }

        private String buildDescription(final String type, final JSONObject eventContent) {
            String description = getString(R.string.report_description_unavailable);

            if (type.equals(C.report.events.type.PROCEDURE)) {
                if (eventContent.optString(C.report.events.procedure.TYPE).equals(C.report.events.procedure.type.ONE_SHOT)) {
                    description = "Manovra: " + eventContent.optString(C.report.events.procedure.DESCRIPTION);

                    Procedure currentProcedure = Procedure.getById(eventContent.optString(C.report.events.procedure.ID));

                    if(currentProcedure != null){
                        switch (currentProcedure){
                            case INTUBATION:
                                description += eventContent.optString(C.report.events.procedure.option.INTUBATION_DIFFICULT_AIRWAY).isEmpty()
                                        ? "" : "\n - Vie Aeree Difficili: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.INTUBATION_DIFFICULT_AIRWAY)));
                                description += eventContent.optString(C.report.events.procedure.option.INTUBATION_INHALATION).isEmpty()
                                        ? "" : "\n - Inalazione: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.INTUBATION_INHALATION)));
                                description += eventContent.optString(C.report.events.procedure.option.INTUBATION_VIDEOLARINGO).isEmpty()
                                        ? "" : "\n - Videolaringo: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.INTUBATION_VIDEOLARINGO)));
                                description += eventContent.optString(C.report.events.procedure.option.INTUBATION_FROVA).isEmpty()
                                        ? "" : "\n - Frova: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.INTUBATION_FROVA)));
                                break;

                            case DRAINAGE:
                                description += eventContent.optString(C.report.events.procedure.option.DRAINAGE_LEFT).isEmpty()
                                        ? "" : "\n - Sinistro: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.DRAINAGE_LEFT)));
                                description += eventContent.optString(C.report.events.procedure.option.DRAINAGE_RIGHT).isEmpty()
                                        ? "" : "\n - Destro: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.DRAINAGE_RIGHT)));
                                break;

                            case CHEST_TUBE:
                                description += eventContent.optString(C.report.events.procedure.option.CHESTTUBE_LEFT).isEmpty()
                                        ? "" : "\n - Sinistro: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.CHESTTUBE_LEFT)));
                                description += eventContent.optString(C.report.events.procedure.option.CHESTTUBE_RIGHT).isEmpty()
                                        ? "" : "\n - Destro: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.procedure.option.CHESTTUBE_RIGHT)));
                                break;
                        }
                    }
                }

                if (eventContent.optString(C.report.events.procedure.TYPE).equals(C.report.events.procedure.type.TIME_DEPENDENT) && eventContent.optString(C.report.events.procedure.EVENT).equals(C.report.events.procedure.event.START)) {
                    description = String.format("%s - Inizio Manovra", eventContent.optString(C.report.events.procedure.DESCRIPTION));
                }

                if (eventContent.optString(C.report.events.procedure.TYPE).equals(C.report.events.procedure.type.TIME_DEPENDENT) && eventContent.optString(C.report.events.procedure.EVENT).equals(C.report.events.procedure.event.END)) {
                    description = String.format("%s - Fine manovra", eventContent.optString(C.report.events.procedure.DESCRIPTION));
                }

            }

            if (type.equals(C.report.events.type.DIAGNOSTIC)) {
                description = eventContent.optString(C.report.events.diagnostic.DESCRIPTION);

                Diagnostics currentDiagnostics = Diagnostics.getById(eventContent.optString(C.report.events.diagnostic.ID));

                if(currentDiagnostics != null){
                    switch (currentDiagnostics){
                        case ABG:
                            description += eventContent.optString(C.report.events.diagnostic.option.ABG_LACTATES).isEmpty()
                                    ? "" : "\n - Lattati: " + eventContent.optString(C.report.events.diagnostic.option.ABG_LACTATES);
                            description += eventContent.optString(C.report.events.diagnostic.option.ABG_BE).isEmpty()
                                    ? "" : "\n - BE: " + eventContent.optString(C.report.events.diagnostic.option.ABG_BE);
                            description += eventContent.optString(C.report.events.diagnostic.option.ABG_PH).isEmpty()
                                    ? "" : "\n - pH: " + eventContent.optString(C.report.events.diagnostic.option.ABG_PH);
                            description += eventContent.optString(C.report.events.diagnostic.option.ABG_HB).isEmpty()
                                    ? "" : "\n - Hb: " + eventContent.optString(C.report.events.diagnostic.option.ABG_HB);
                            break;

                        case ROTEM:
                            description += eventContent.optString(C.report.events.diagnostic.option.ROTEM_FIBTEM).isEmpty()
                                    ? "" : "\n - Fibtem: " + eventContent.optString(C.report.events.diagnostic.option.ROTEM_FIBTEM);
                            description += eventContent.optString(C.report.events.diagnostic.option.ROTEM_EXTEM).isEmpty()
                                    ? "" : "\n - Extem: " + eventContent.optString(C.report.events.diagnostic.option.ROTEM_EXTEM);
                            description += eventContent.optString(C.report.events.diagnostic.option.ROTEM_HYPERFIBRINOLYSIS).isEmpty()
                                    ? "" : "\n - Iperfibrinolisi: " + convertYesNo(Boolean.parseBoolean(eventContent.optString(C.report.events.diagnostic.option.ROTEM_HYPERFIBRINOLYSIS)));
                            break;
                    }
                }
            }

            if (type.equals(C.report.events.type.DRUG)) {
                if (eventContent.optString(C.report.events.drug.ADMINISTRATION_TYPE).equals(C.report.events.drug.type.ONE_SHOT)) {
                    description = String.format("Somministrazione di %s\nDosaggio: %s %s",
                            eventContent.optString(C.report.events.drug.DESCRIPTION),
                            eventContent.optString(C.report.events.drug.QUANTITY),
                            eventContent.optString(C.report.events.drug.UNIT));
                }

                if (eventContent.optString(C.report.events.drug.ADMINISTRATION_TYPE).equals(C.report.events.drug.type.DRUG_PROTOCOL)) {
                    description = String.format("%s",
                            eventContent.optString(C.report.events.drug.DESCRIPTION));
                }

                if (eventContent.optString(C.report.events.drug.ADMINISTRATION_TYPE).equals(C.report.events.drug.type.CONTINUOUS_INFUSION) && eventContent.optString(C.report.events.drug.EVENT).equals(C.report.events.drug.event.START)) {
                    description = String.format("Inizio Infusione continua di %s\nDosaggio: %s %s",
                            eventContent.optString(C.report.events.drug.DESCRIPTION),
                            eventContent.optString(C.report.events.drug.QUANTITY),
                            eventContent.optString(C.report.events.drug.UNIT));
                }

                if (eventContent.optString(C.report.events.drug.ADMINISTRATION_TYPE).equals(C.report.events.drug.type.CONTINUOUS_INFUSION) && eventContent.optString(C.report.events.drug.EVENT).equals(C.report.events.drug.event.VARIATION)) {
                    description = String.format("Infusione continua di %s\nNuovo dosaggio: %s %s",
                            eventContent.optString(C.report.events.drug.DESCRIPTION),
                            eventContent.optString(C.report.events.drug.QUANTITY),
                            eventContent.optString(C.report.events.drug.UNIT));
                }

                if (eventContent.optString(C.report.events.drug.ADMINISTRATION_TYPE).equals(C.report.events.drug.type.CONTINUOUS_INFUSION) && eventContent.optString(C.report.events.drug.EVENT).equals(C.report.events.drug.event.STOP)) {
                    description = String.format("Fine Infusione continua di %s\nDurata: %s",
                            eventContent.optString(C.report.events.drug.DESCRIPTION),
                            DateTimeUtils.convertSecondsToString(Integer.parseInt(eventContent.optString(C.report.events.drug.DURATION))));
                }
            }

            if (type.equals(C.report.events.type.BLOOD_PRODUCT)) {
                if (eventContent.optString(C.report.events.bloodProduct.ADMINISTRATION_TYPE).equals(C.report.events.drug.type.ONE_SHOT)) {
                    description = String.format("Somministrazione di %s\nDosaggio: %s %s",
                            eventContent.optString(C.report.events.bloodProduct.DESCRIPTION),
                            eventContent.optString(C.report.events.bloodProduct.QUANTITY),
                            eventContent.optString(C.report.events.bloodProduct.UNIT));

                    final String bagCode =  eventContent.optString(C.report.events.bloodProduct.BAG_CODE);

                    if(!bagCode.isEmpty()){
                        description += String.format("\nCodice Sacca: %s", bagCode);
                    }
                }

                if (eventContent.optString(C.report.events.bloodProduct.ADMINISTRATION_TYPE).equals(C.report.events.bloodProduct.type.BLOOD_PROTOCOL)) {
                    description = String.format("%s",
                            eventContent.optString(C.report.events.bloodProduct.DESCRIPTION));
                }
            }

            if (type.equals(C.report.events.type.VITAL_SIGNS_MON)) {
                description = String.format("%s\n%s: %s %s\n%s: %s %s\n%s: %s %s\n%s: %s %s\n%s: %s %s\n%s: %s %s",
                        getString(R.string.report_description_monitor_vs),
                        getString(R.string.report_description_vs_sys),
                        eventContent.optString(C.report.events.vitalSignMon.SYS),
                        getString(R.string.report_description_vs_sys_unit),
                        getString(R.string.report_description_vs_dia),
                        eventContent.optString(C.report.events.vitalSignMon.DIA),
                        getString(R.string.report_description_vs_dia_unit),
                        getString(R.string.report_description_vs_hr),
                        eventContent.optString(C.report.events.vitalSignMon.HR),
                        getString(R.string.report_description_vs_hr_unit),
                        getString(R.string.report_description_vs_etco2),
                        eventContent.optString(C.report.events.vitalSignMon.ETCO2),
                        getString(R.string.report_description_vs_etco2_unit),
                        getString(R.string.report_description_vs_spo2),
                        eventContent.optString(C.report.events.vitalSignMon.SPO2),
                        getString(R.string.report_description_vs_spo2_unit),
                        getString(R.string.report_description_vs_temp),
                        eventContent.optString(C.report.events.vitalSignMon.TEMP),
                        getString(R.string.report_description_vs_temp_unit));
            }

            if (type.equals(C.report.events.type.CLINICAL_VARIATION)) {
                description = String.format("%s\n%s: %s",
                        getString(R.string.report_description_vs_variation),
                        eventContent.optString(C.report.events.clinicalVariation.DESCRIPTION),
                        eventContent.optString(C.report.events.clinicalVariation.VALUE));
            }

            if (type.equals(C.report.events.type.PHOTO)) {
                description = String.format("%s [%s]",
                        getString(R.string.report_description_photo),
                        eventContent.optString(C.report.events.photo.DESCRIPTION));
            }

            if (type.equals(C.report.events.type.VIDEO)) {
                description = String.format("%s [%s]",
                        getString(R.string.report_description_video),
                        eventContent.optString(C.report.events.video.DESCRIPTION));
            }

            if (type.equals(C.report.events.type.VOCAL_NOTE)) {
                description = String.format("%s [%s]",
                        getString(R.string.report_description_vocal_note),
                        eventContent.optString(C.report.events.vocalNote.DESCRIPTION));
            }

            if (type.equals(C.report.events.type.TEXT_NOTE)) {
                description = String.format("%s\n%s",
                        getString(R.string.report_description_text_note),
                        eventContent.optString(C.report.events.textNote.TEXT));
            }

            if (type.equals(C.report.events.type.TRAUMA_LEADER)) {
                description = String.format("%s: %s %s",
                        getString(R.string.report_description_new_tl),
                        eventContent.optString(C.report.events.traumaLeader.NAME),
                        eventContent.optString(C.report.events.traumaLeader.SURNAME));
            }

            if (type.equals(C.report.events.type.ROOM_IN)) {
                String place = eventContent.optString(C.report.events.roomIn.PLACE);
                if(place.equals(C.place.PREH)){
                    description = getString(R.string.report_description_trauma_team_activation);
                } else if(place.equals(C.place.TRANSPORT)) {
                    description = "Inizio Trasporto";
                } else {
                    description = getString(R.string.report_description_room_in) + eventContent.optString(C.report.events.roomIn.PLACE);
                }
            }

            if (type.equals(C.report.events.type.ROOM_OUT)) {
                if(place.equals(C.place.TRANSPORT)){
                    description = "Fine Trasporto";
                } else {
                    description = getString(R.string.report_description_room_out) + eventContent.optString(C.report.events.roomOut.PLACE);
                }
            }

            if (type.equals(C.report.events.type.REPORT_REACTIVATION)) {
                description = getString(R.string.report_description_report_reactivation);
            }

            if (type.equals(C.report.events.type.PATIENT_ACCEPTED)) {
                description = getString(R.string.report_description_patient_accepted);
            }

            return description;
        }

        private String getString(int res) {
            return App.getAppResources().getString(res);
        }
    }
}
