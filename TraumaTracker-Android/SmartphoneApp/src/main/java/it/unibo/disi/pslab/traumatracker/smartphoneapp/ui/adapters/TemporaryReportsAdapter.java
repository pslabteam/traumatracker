package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import android.support.annotation.NonNull;

import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.StarterUI;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TinyReport;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialogWithChoice;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.ReportDeleteDialog;

public class TemporaryReportsAdapter extends ArrayAdapter<TinyReport> {

    //private final Context ctx;

    private static class ViewHolder {
        TextView operator, txtId, txtStartDate, txtStartTime;
        Button restartBtn, deleteBtn;
    }

    public TemporaryReportsAdapter(Context context, List<TinyReport> data) {
        super(context, R.layout.item_temporary_reports_archive, data);
        //this.ctx = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull final ViewGroup parent) {
        final TinyReport report = getItem(position);

        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_temporary_reports_archive, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.txtId = convertView.findViewById(R.id.reportID);
            viewHolder.operator = convertView.findViewById(R.id.operator);
            viewHolder.txtStartDate = convertView.findViewById(R.id.reportStartDate);
            viewHolder.txtStartTime = convertView.findViewById(R.id.reportStartTime);
            viewHolder.restartBtn = convertView.findViewById(R.id.restart_button);
            viewHolder.deleteBtn = convertView.findViewById(R.id.delete_button);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (report != null) {
            viewHolder.txtId.setText(report.id());

            final User operator = FileSystem.getUserById(report.operatorId());
            if(operator != null){
                viewHolder.operator.setText(operator.toString());
            }

            viewHolder.txtStartDate.setText(report.startDate());
            viewHolder.txtStartTime.setText(report.startTime());

            viewHolder.restartBtn.setOnClickListener((View v) -> {
                new MessageDialogWithChoice(getContext(), App.getAppResources().getString(R.string.warning), "Ripristinare lo stato del trauma scelto?", new MessageDialogWithChoice.Listener() {
                    @Override
                    public void onNoButtonClicked() {
                        //Nothing to do!
                    }

                    @Override
                    public void onYesButtonClicked() {
                        StarterUI.instance.restartReportRequest(report.id());
                    }
                }).show();

                //new MessageDialog(getContext(), App.getAppResources().getString(R.string.warning), "Funzionalità momentaneamente sospesa!", null).show();
            });

            viewHolder.deleteBtn.setOnClickListener(v -> new ReportDeleteDialog<>(getContext(), report, () -> removeReport(report)).show());
        }

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    private void removeReport(final TinyReport report){
        remove(report);
        notifyDataSetChanged();
    }
}
