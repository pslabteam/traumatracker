package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import android.util.Log;

import org.json.JSONObject;

public class PrehMission {
    private String missionId;
    private String rescuer, vehicle, mission_code;
    private JSONObject tracking;

    public PrehMission(String missionId, String rescuer, String vehicle, String mission_code, JSONObject tracking){
        this.missionId = missionId;
        this.rescuer = rescuer;
        this.vehicle = vehicle;
        this.mission_code = mission_code;
        this.tracking = tracking;
    }

    public String missionId() { return missionId; }

    public String rescuer(){
        return rescuer;
    }

    public String vehicle(){
        return vehicle;
    }

    public String missionCode(){
        return mission_code;
    }

    public JSONObject tracking(){
        return tracking;
    }
}
