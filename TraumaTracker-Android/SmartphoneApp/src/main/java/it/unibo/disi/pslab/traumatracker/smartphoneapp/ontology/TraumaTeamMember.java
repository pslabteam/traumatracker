package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum TraumaTeamMember {
    EMERGENCY_MEDIC("emergency-medic", R.string.ontology_traumateammember_emergencymedic),
    ANAESTHETIST("anaesthesist", R.string.ontology_traumateammember_anaesthesist),
    EMERGENCY_SURGEON("emergency-surgeon", R.string.ontology_traumateammember_emergencysurgeon),
    ORTHOPEDIC_SURGEON("orthopedic-surgeon", R.string.ontology_traumateammember_orthopedicsurgeon),
    NEUROSURGEON("neurosurgeon", R.string.ontology_traumateammember_neurosurgeon),
    RADIOLOGIST("radiologist", R.string.ontology_traumateammember_radiologist),
    MAXILLO_FACIAL("maxillo-facial", R.string.ontology_traumateammember_maxillofacial),
    UROLOGIST("urologist", R.string.ontology_traumateammember_urologist);

    private final int labelRes;

    TraumaTeamMember(@SuppressWarnings("unused") final String id, final int labelRes){
        this.labelRes = labelRes;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }
}
