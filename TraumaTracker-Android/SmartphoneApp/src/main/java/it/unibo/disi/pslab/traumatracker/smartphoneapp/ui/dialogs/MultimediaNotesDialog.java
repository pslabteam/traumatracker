package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.Note;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.NoteStream;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class MultimediaNotesDialog extends TTDialog {

    private static final String PHOTO_AVAILABLE_MSG = "photo_available";
    private static final String VIDEO_AVAILABLE_MSG = "video_available";
    private static final String VOCAL_NOTE_AVAILABLE_MSG = "vocal_note_available";
    private static final String TEXT_NOTE_AVAILABLE_MSG = "text_note_available";

    public MultimediaNotesDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_multimedia_notes);

        initUI(context, listener);
    }

    private void initUI(Context context, final Listener listener){
        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());

        findViewById(R.id.photoButton).setOnClickListener(v -> {
            dismiss();

            new MediaCaptureDialog(context, getString(R.string.camera_dialog_title), false, false,
                    filename -> listener.onMediaAvailable(PHOTO_AVAILABLE_MSG, filename)).show();
        });

        findViewById(R.id.videoButton).setOnClickListener(v -> {
            dismiss();

            new MediaCaptureDialog(context, getString(R.string.video_dialog_title), true, false,
                    filename -> listener.onMediaAvailable(VIDEO_AVAILABLE_MSG, filename)).show();
        });

        findViewById(R.id.vocalNoteButton).setOnClickListener(v -> {
            dismiss();

            new MediaCaptureDialog(context, getString(R.string.audio_dialog_title), true, true,
                    filename -> listener.onMediaAvailable(VOCAL_NOTE_AVAILABLE_MSG, filename)).show();
        });

        findViewById(R.id.textNoteButton).setOnClickListener(v -> {
            dismiss();

            new TextNoteDialog(context,
                    note -> listener.onMediaAvailable(TEXT_NOTE_AVAILABLE_MSG, note)).show();
        });

        initUIEventTrack();
    }

    private void initUIEventTrack(){
        Report.EventsTrack currentEventsTrack = new Report.EventsTrack();

        List<Note> events = NoteStream.requestEventsList();

        for(int i = 0; i < events.size(); i++){
            JSONObject event = events.get(i).getContent();

            try {
                String type = event.optString(C.report.events.TYPE);

                if(type.equals(C.report.events.type.PHOTO)
                        || type.equals(C.report.events.type.VIDEO)
                        || type.equals(C.report.events.type.VOCAL_NOTE)
                        || type.equals(C.report.events.type.TEXT_NOTE)) {
                    currentEventsTrack.add(new Report.Event(
                            event.optInt(C.report.events.ID),
                            event.optString(C.report.events.DATE),
                            event.optString(C.report.events.TIME),
                            event.optString(C.report.events.PLACE),
                            event.optString(C.report.events.TYPE),
                            event.getJSONObject(C.report.events.CONTENT)
                    ));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(currentEventsTrack.isEmpty()){
            findViewById(R.id.noUsersLabel).setVisibility(View.VISIBLE);
            findViewById(R.id.events_table).setVisibility(View.GONE);
        } else {
            findViewById(R.id.noUsersLabel).setVisibility(View.GONE);
            findViewById(R.id.events_table).setVisibility(View.VISIBLE);
            fillEventsTrack(currentEventsTrack);
        }
    }

    private void fillEventsTrack(Report.EventsTrack eventsTrack){
        TableLayout eventsTable = findViewById(R.id.events_table);
        eventsTable.removeAllViews();

        TableLayout.LayoutParams rowLayout = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
        rowLayout.bottomMargin = 5;

        String lastPlace = "";
        String lastDate = "";

        List<Report.Event> events = new ArrayList<>(eventsTrack.getEvents());
        Collections.sort(events, (e1, e2) -> {
            String time1 = e1.date() + " " + e1.time();
            String time2 = e2.date() + " " + e2.time();
            return time1.compareTo(time2);
        });

        for (int i = 0; i < events.size(); i++) {
            Report.Event event = events.get(i);

            if(!event.date().equals(lastDate)){
                lastDate = event.date();

                TableRow row = new TableRow(getContext());
                row.addView(createDateLabel(lastDate));

                eventsTable.addView(row, rowLayout);
            }

            if(!event.place().equals(lastPlace)){
                lastPlace = event.place();

                TableRow row = new TableRow(getContext());
                row.addView(createPlaceLabel(lastPlace));

                eventsTable.addView(row, rowLayout);
            }

            TableRow row = new TableRow(getContext());
            row.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1.0f));
            row.setBackgroundColor(getResources().getColor(R.color.tt_light_grey));

            row.addView(createLabelForRow(event.time(), 1f));
            row.addView(createLabelForRow(event.description(), 5f));

            if(!event.type().equals(C.report.events.type.TEXT_NOTE)){
                row.addView(createUpdateButton(event.type(), event.description()));
            } else {
                row.addView(new Space(getContext()));
            }

            eventsTable.addView(row, rowLayout);
        }
    }

    private TextView createPlaceLabel(String place){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        params.span = 2;

        TextView placeLabel = new TextView(getContext());
        placeLabel.setText(place);
        placeLabel.setTypeface(null, Typeface.BOLD);
        placeLabel.setPadding(5, 5, 5, 5);
        placeLabel.setTextColor(getResources().getColor(R.color.tt_white));
        placeLabel.setBackgroundResource(R.color.tt_red);
        placeLabel.setLayoutParams(params);

        return placeLabel;
    }

    private TextView createDateLabel(String date){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        params.span = 2;
        params.topMargin = 5;

        TextView dateLabel = new TextView(getContext());
        dateLabel.setText(date);
        dateLabel.setTypeface(null, Typeface.BOLD);
        dateLabel.setLayoutParams(params);
        dateLabel.setGravity(Gravity.START);

        return dateLabel;
    }

    private TextView createLabelForRow(String text, float weight) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, weight);
        params.gravity = Gravity.CENTER;
        params.leftMargin = 5;

        TextView txt = new TextView(getContext());
        txt.setText(text);
        txt.setPadding(2, 5, 2, 5);
        txt.setLayoutParams(params);

        return txt;
    }

    private Button createUpdateButton(final String type, final String description){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        params.gravity = Gravity.CENTER_VERTICAL;
        params.bottomMargin = 5;
        params.rightMargin = 5;

        Button btn = new Button(getContext());
        btn.setText(getString(R.string.open_button_text));
        btn.setTextSize(12);
        btn.setBackgroundResource(R.drawable.traumatracker_sizable_button);
        btn.setPadding(2, 2, 2, 2);
        btn.setLayoutParams(params);
        btn.setOnClickListener(view -> //new MessageDialog(getContext(), getString(R.string.warning),"Funzionalità non ancora disponibile!", null).show());
                new MediaShowDialog(getContext(), type, description.substring(description.indexOf("[") +1 , description.indexOf("]"))).showFullscreen());

        return btn;
    }

    public interface Listener {
        void onMediaAvailable(String message, String content);
    }
}
