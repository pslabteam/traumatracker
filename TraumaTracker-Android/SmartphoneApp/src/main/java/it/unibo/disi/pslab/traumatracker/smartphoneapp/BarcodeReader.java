package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.content.Intent;
import android.util.ArraySet;
//import android.support.v4.util.ArraySet;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import cartago.OPERATION;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class BarcodeReader extends JaCaArtifact {

    private static final String BARCODE_AVAILABLE_OBS_PROP = "barcode_available";
    private static final String BARCODE_FORMAT_CODE_128 = "CODE_128";
    private static final int ZXING_SCAN_REQUEST_CODE = 49374;

    private String resultsProviderArtifact;

    private String type;

    public void init(String resultsProviderArtifact){
        this.resultsProviderArtifact = resultsProviderArtifact;

        defineObsProperty(BARCODE_AVAILABLE_OBS_PROP, "", "empty");

        //TODO: modify (if possible) JCaM to support (following) subscription during init phase
        //subscribeForActivityResults(resultsProviderArtifact, id(), "onNewBarcodeAvailable");
    }

    @OPERATION
    public void readBarcode(String type){
        subscribeForActivityResults(resultsProviderArtifact, getId(), "onResultsAvailable");

        this.type = type;

        ArraySet<String> barcodeFormats = new ArraySet<>();
        barcodeFormats.add(BARCODE_FORMAT_CODE_128);


        IntentIntegrator integrator = new IntentIntegrator(getActivity(resultsProviderArtifact));
        integrator.setPrompt(App.getAppResources().getString(R.string.barcode_view_message));
        integrator.setBeepEnabled(true);
        integrator.setDesiredBarcodeFormats(barcodeFormats);
        integrator.setOrientationLocked(false);
        integrator.initiateScan();


    }

    @OPERATION
    public void onResultsAvailable(int requestCode, int resultCode, Intent data){
        if(requestCode == ZXING_SCAN_REQUEST_CODE){
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

            if (scanningResult != null) {
                String barcodeContent = scanningResult.getContents();
                if(barcodeContent != null) {
                    updateObsProperty(BARCODE_AVAILABLE_OBS_PROP, type, barcodeContent);
                }
            }
        }
    }

    public static class Requests{
        public static final String PATIENT_CODE = "code";
        public static final String PATIENT_SDO = "sdo";
        public static final String PATIENT_CODE_FINAL = "code-final";
        public static final String PATIENT_SDO_FINAL = "sdo-final";
        public static final String PATIENT_SDO_BY_DESTINATION = "sdo-by-destination";
        public static final String BLOOD_BAG_CODE = "blood-bag-code";
    }
}
