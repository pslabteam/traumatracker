package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cartago.Artifact;
import cartago.OPERATION;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.User;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.UserRole;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;

public class FileSystem extends Artifact {

    private static final String USERS_FILE_NAME = "users.txt";
    private static final String ROOMS_FILE_NAME = "rooms.txt";

    void init(){ }

    @OPERATION
    public void saveReportToLocal(JSONObject report, String reportName){
        String fileName = reportName + C.fs.REPORT_FILE_EXTENSION;
        FileSystemUtils.saveFile(C.fs.REPORTS_TOSYNC_FOLDER, fileName, report.toString());
    }

    @OPERATION
    public void moveSyncReport(String reportName){
        FileSystemUtils.moveFile(reportName + C.fs.REPORT_FILE_EXTENSION, C.fs.REPORTS_TOSYNC_FOLDER, C.fs.REPORTS_SYNC_FOLDER);
    }

    @OPERATION
    public void updateBackupReport(JSONObject report, String reportName){
        String fileName = reportName + C.fs.REPORT_FILE_EXTENSION;
        FileSystemUtils.saveFile(C.fs.TEMP_FOLDER, fileName, report.toString());
    }

    @OPERATION
    public void deleteBackupReport(String reportName){
        String fileName = reportName + C.fs.REPORT_FILE_EXTENSION;
        FileSystemUtils.deleteFile(C.fs.TEMP_FOLDER, fileName);
    }

    @OPERATION
    public void saveUsersList(JSONArray list){
        if(list.length() != 0){
            FileSystemUtils.saveFile(C.fs.CONFIG_FOLDER, USERS_FILE_NAME, list.toString());
        }
    }

    @OPERATION
    public void saveRoomsList(JSONArray list){
        if(list.length() != 0){
            FileSystemUtils.saveFile(C.fs.CONFIG_FOLDER, ROOMS_FILE_NAME, list.toString());
        }
    }

    public static boolean deleteTemporayReportFromLocal(String filename){
        return FileSystemUtils.deleteFile(C.fs.TEMP_FOLDER, filename + C.fs.REPORT_FILE_EXTENSION);
    }

    public static boolean deleteReportFromLocal(String filename, boolean sync){
        return FileSystemUtils.deleteFile(sync ? C.fs.REPORTS_SYNC_FOLDER : C.fs.REPORTS_TOSYNC_FOLDER, filename + C.fs.REPORT_FILE_EXTENSION);
    }

    public static List<User> getUserList(final UserRole... roles) throws IOException {
        final List<User> list = new ArrayList<>();

        try {
            final JSONArray users = new JSONArray(FileSystemUtils.loadFile(C.fs.CONFIG_FOLDER, USERS_FILE_NAME));

            for (int i = 0; i < users.length(); i++) {
                final JSONObject obj = (JSONObject) users.get(i);

                final User user = new User(obj.getString(C.user.USER_ID),
                        obj.getString(C.user.NAME),
                        obj.getString(C.user.SURNAME),
                        obj.getString(C.user.ROLE));

                if(roles.length == 0){
                    list.add(user);
                } else {
                    for(final UserRole role : roles){
                        if(role.description().equals(user.role())){
                            list.add(user);
                        }
                    }
                }
            }
        } catch(JSONException e){
            //Empty file
        }

        return list;
    }

    public static User getUserById(final String id) {
        try {
            List<User> users = getUserList();

            for(User u : users){
                if(u.id().equals(id)) {
                    return u;
                }
            }
        } catch (IOException e) {
            Log.e(C.LOG_TAG,"Unable to find requested user <" + e.getMessage() + ">");
        }

        return null;
    }

    public static List<String> getRoomsList() throws JSONException, IOException{
        final String fileContent = FileSystemUtils.loadFile(C.fs.CONFIG_FOLDER, ROOMS_FILE_NAME);

        if(fileContent == null) {
            return new ArrayList<>();
        }

        final JSONArray rooms = new JSONArray(fileContent);
        final List<String> list = new ArrayList<>();

        for (int i = 0; i < rooms.length(); i++) {
            list.add(((JSONObject) rooms.get(i)).getString("description"));
        }

        return list;
    }
}
