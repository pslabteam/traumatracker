package it.unibo.disi.pslab.traumatracker.smartphoneapp.vs_monitors;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.logging.Logger;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class SimulatedVitalSignsMonitor implements VitalSignsMonitor {

    private final Logger logger;

    public SimulatedVitalSignsMonitor() {
        this(null);
    }

    public SimulatedVitalSignsMonitor(final Logger logger) {
        this.logger = logger;
    }

    @Override
    public Map<String, Integer> getPatientVitalSigns() {
        Map<String, Integer> values = new HashMap<>();

        values.put(C.vs.VS_SYS, ThreadLocalRandom.current().nextInt(100, 125 + 1));
        values.put(C.vs.VS_DIA, ThreadLocalRandom.current().nextInt(70, 85 + 1));
        values.put(C.vs.VS_HR, ThreadLocalRandom.current().nextInt(55, 110 + 1));
        values.put(C.vs.VS_ETCO2, ThreadLocalRandom.current().nextInt(32, 35 + 1));
        values.put(C.vs.VS_SPO2, ThreadLocalRandom.current().nextInt(97, 99 + 1));
        values.put(C.vs.VS_TEMP, ThreadLocalRandom.current().nextInt(35, 37 + 1));

        return values;
    }
}
