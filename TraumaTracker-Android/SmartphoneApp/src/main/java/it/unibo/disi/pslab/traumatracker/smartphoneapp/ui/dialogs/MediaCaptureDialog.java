package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;

public class MediaCaptureDialog extends TTDialog implements SurfaceHolder.Callback {

    private Camera mCamera;
    private MediaRecorder recorder;

    private final boolean video;
    private final boolean audioOnly;

    private final Button saveButton;

    private TextView mediaStatusLabel;
    private boolean recording, actionDone;

    private SurfaceHolder surfaceHolder;

    private Bitmap picture;
    private String recordingDestrinationPath;

    private static final int MEDIA_FILE_MAX_SIZE = 5000000; //50MB
    private static final int MEDIA_DURATION = 0; //0 > no limits!

    public MediaCaptureDialog(final Context context, String title, final boolean video, final boolean audioOnly, final Listener listener) {
        super(context, R.layout.dialog_camera);

        this.video = video;
        this.audioOnly = audioOnly;

        ((TextView) findViewById(R.id.title)).setText(title);

        initCameraUI();

        recording = false;
        actionDone = false;

        Button cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(v -> {
            if(!video){
                if(mCamera != null) {
                    mCamera.stopPreview();
                }
            } else {
                if(recording){
                    stopRecording();
                    FileSystemUtils.deleteMedia(recordingDestrinationPath);
                }

                if(actionDone){
                    FileSystemUtils.deleteMedia(recordingDestrinationPath);
                }
            }

            if(mCamera != null){
                mCamera.release();
                mCamera = null;
            }

            dismiss();
        });

        saveButton = findViewById(R.id.closeButton);
        saveButton.setOnClickListener(v -> {
            if(audioOnly){
                if(((Button)v).getText().equals(context.getString(R.string.vocal_note_start))){
                    startRecording(Environment.getExternalStorageDirectory() + C.fs.VOCAL_NOTES_FOLDER);
                    mediaStatusLabel.setText(context.getString(R.string.recording_ongoing));
                    saveButton.setText(context.getString(R.string.vocal_note_stop));
                    return;
                }

                if(((Button)v).getText().equals(context.getString(R.string.vocal_note_stop))){
                    stopRecording();
                    actionDone = true;
                    mediaStatusLabel.setText(context.getString(R.string.recording_done));
                    saveButton.setText(context.getString(R.string.vocal_note_save));
                    return;
                }

                if(((Button)v).getText().equals(context.getString(R.string.vocal_note_save))){
                    String[] elements = recordingDestrinationPath.split("/");
                    listener.onSaveButtonClicked(elements[elements.length - 1]);
                    dismiss();
                    return;
                }
            }

            if(!video){
                try {
                    String filename = new Date().getTime() + C.fs.PHOTO_FILE_EXTENSION;
                    listener.onSaveButtonClicked(FileSystemUtils.savePicture(picture, filename));
                } catch (IOException e){
                    listener.onSaveButtonClicked(null);
                }
            } else {
                String[] elements = recordingDestrinationPath.split("/");
                listener.onSaveButtonClicked(elements[elements.length - 1]);
            }

            dismiss();
        });

        if(audioOnly){
            saveButton.setText(context.getString(R.string.vocal_note_start));
        } else {
            saveButton.setEnabled(false);
        }
    }

    private void initCameraUI() {
        mediaStatusLabel = findViewById(R.id.recording_status_label);
        mediaStatusLabel.setVisibility(View.VISIBLE);
        mediaStatusLabel.setText(getString(R.string.recording_idle));

        SurfaceView surfaceView = findViewById(R.id.surfaceview);

        if(audioOnly){
            surfaceView.setVisibility(View.GONE);
            mediaStatusLabel.setText("");
        } else {
            surfaceView.setOnClickListener(v -> {
                if (actionDone) {
                    return;
                }

                if (!video) {
                    takePicture();
                } else {
                    if (!recording) {
                        //PLAY
                        startRecording(Environment.getExternalStorageDirectory() + C.fs.VIDEOS_FOLDER);
                        mediaStatusLabel.setText(getString(R.string.recording_ongoing));
                    } else {
                        //STOP
                        stopRecording();
                        actionDone = true;
                        mediaStatusLabel.setText(getString(R.string.recording_done));
                        saveButton.setEnabled(true);
                    }
                }
            });

            surfaceHolder = surfaceView.getHolder();
            surfaceHolder.addCallback(MediaCaptureDialog.this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

            mCamera = Camera.open();
        }
    }

    private void takePicture(){
        mCamera.takePicture(null, null, null, (data, camera) -> {
            camera.stopPreview();
            camera.release();

            mCamera = null;

            Bitmap source = BitmapFactory.decodeByteArray(data, 0, data.length);

            Matrix matrix = new Matrix();
            matrix.postRotate(90);

            picture = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

            actionDone = true;
            mediaStatusLabel.setText(getString(R.string.photo_taken));
            saveButton.setEnabled(true);
        });
    }

    private void startRecording(String videoFolderPath){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);

        if(audioOnly){
            recordingDestrinationPath = videoFolderPath + "/audio_" + sdf.format(new Date()) + C.fs.VOCAL_FILE_EXTENSION;
        } else {
            recordingDestrinationPath = videoFolderPath + "/video_" + sdf.format(new Date()) + C.fs.VIDEO_FILE_EXTENSION;
        }

        recorder = new MediaRecorder();

        if(!audioOnly) {
            mCamera.stopPreview();
            mCamera.unlock();
        }

        if(audioOnly){
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setOutputFile(recordingDestrinationPath);
            recorder.setMaxDuration(MEDIA_DURATION);
            recorder.setMaxFileSize(MEDIA_FILE_MAX_SIZE);
        } else {
            recorder.setCamera(mCamera);
            recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
            recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
            recorder.setOutputFile(recordingDestrinationPath);
            recorder.setMaxDuration(MEDIA_DURATION);
            recorder.setMaxFileSize(MEDIA_FILE_MAX_SIZE);
            recorder.setOrientationHint(90);
            recorder.setPreviewDisplay(surfaceHolder.getSurface());
        }

        try {
            recorder.prepare();
        } catch (IllegalStateException | IOException e) {
            e.printStackTrace();
        }

        recorder.start();

        recording = true;
    }

    private void stopRecording(){
        recorder.stop();
        recorder.release();

        recording = false;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewDisplay(holder);

            try {
                Camera.Parameters params = mCamera.getParameters();
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                mCamera.setParameters(params);
            } catch(RuntimeException re){
                System.out.println("Impossibile impostare parametro autofocus per la camera!");
            }

            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public interface Listener{
        void onSaveButtonClicked(String filename);
    }
}
