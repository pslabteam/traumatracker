package it.unibo.disi.pslab.traumatracker.smartphoneapp.utils;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileSystemUtils {

    public static String loadFile(final String folder, final String filename) {
        try {
            final File f = new File(Environment.getExternalStorageDirectory() + folder, filename);
            return f.length() != 0 ? getStringFromFile(f) : "";
        } catch (IOException e) {
            return "";
        }
    }

    public static boolean saveFile(String folder, String filename, String filecontent) {
        String pathname = Environment.getExternalStorageDirectory() + folder;

        try {
            FileOutputStream fos = new FileOutputStream(new File(pathname, filename));
            fos.write(filecontent.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public static boolean moveFile(String file, String fromFolder, String toFolder) {
        return new File(Environment.getExternalStorageDirectory() + fromFolder + "/" + file)
                .renameTo(new File(Environment.getExternalStorageDirectory() + toFolder + "/" + file));
    }

    public static boolean moveFolderContent(String fromFolder, String toFolder){
        File folder = new File(Environment.getExternalStorageDirectory() + fromFolder);

        if(folder.isDirectory()){
            File[] files = folder.listFiles();

            for(File f : files)
                f.renameTo(new File(Environment.getExternalStorageDirectory() + toFolder + "/" + f.getName()));

            return true;
        }

        return false;
    }

    public static boolean renameFile(String folder, String filename, String newname) {
        File dir = new File(Environment.getExternalStorageDirectory() + folder);

        if (dir.exists()) {
            File from = new File(dir, filename);
            File to = new File(dir, newname);
            if (from.exists()) {
                return from.renameTo(to);
            }
        }

        return false;
    }

    public static boolean deleteFile(String folder, String filename) {
        return new File(Environment.getExternalStorageDirectory() + folder, filename).delete();
    }

    public static File[] getFilesFromDirectory(String dir) {
        return new File(Environment.getExternalStorageDirectory() + dir).listFiles();
    }

    public static File getLastFileFromDirectory(String dir) {
        File[] allFiles = getFilesFromDirectory(dir);

        long date = 0;
        File file = null;

        for (File f : allFiles){
            if(f.lastModified() > date){
                file = f;
                date = f.lastModified();
            }
        }

        return file;
    }

    public static void emptyFolder(String dir){
        File folder = new File(Environment.getExternalStorageDirectory() + dir);

        if(folder.isDirectory()){
            File[] files = folder.listFiles();

            for(File f : files)
                f.delete();
        }
    }

    public static String savePicture(Bitmap image, String fileName) throws IOException {
        final String pathname = Environment.getExternalStorageDirectory() + C.fs.PICTURES_FOLDER + "/" + fileName;

        final int COMPRESSION_QUALITY = 30;

        FileOutputStream fos = new FileOutputStream(new File(pathname));
        image.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY, fos);
        fos.close();

        return fileName;
    }

    public static boolean deleteMedia(String path) {
        return new File(path).delete();
    }

    private static String getStringFromFile(File file) throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        final StringBuilder sb = new StringBuilder();

        String line;

        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }

        reader.close();

        return sb.toString();
    }
}
