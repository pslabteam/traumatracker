package it.unibo.disi.pslab.traumatracker.smartphoneapp.logging;

public interface Logger {
    void writeOnLog(final String msg);
}
