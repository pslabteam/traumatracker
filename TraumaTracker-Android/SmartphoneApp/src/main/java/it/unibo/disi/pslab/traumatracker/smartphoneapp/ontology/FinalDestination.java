package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum FinalDestination {
    EMERGENCY_ROOM("er", R.string.ontology_finaldestination_er),
    INTENSIVE_CARE_UNIT("icu", R.string.ontology_finaldestination_icu),
    EMERGENCY_MEDICINE("em", R.string.ontology_finaldestination_em),
    OTHER("other", R.string.ontology_finaldestination_other);

    private final int labelRes;

    FinalDestination(@SuppressWarnings("unused") final String id, final int labelRes){
        this.labelRes = labelRes;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }
}
