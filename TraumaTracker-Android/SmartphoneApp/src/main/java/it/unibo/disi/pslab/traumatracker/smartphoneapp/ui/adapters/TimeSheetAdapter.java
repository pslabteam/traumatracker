package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.support.annotation.NonNull;

import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TimeDependentProcedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class TimeSheetAdapter extends ArrayAdapter<TimeDependentProcedure> {

    private static class ViewHolder {
        TextView[] labels;
        TextView status, name, startTime, duration, message;
    }

    public TimeSheetAdapter(Context context, List<TimeDependentProcedure> data) {
        super(context, R.layout.item_time_dependent_procedure, data);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull final ViewGroup parent) {
        final TimeDependentProcedure tdp = getItem(position);

        final ViewHolder vh;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_time_dependent_procedure, parent, false);

            vh = new ViewHolder();
            vh.labels = new TextView[]{
                    convertView.findViewById(R.id.procedure_label_1),
                    convertView.findViewById(R.id.procedure_label_2),
                    convertView.findViewById(R.id.procedure_label_3)
            };
            vh.status = convertView.findViewById(R.id.procedure_status);
            vh.name = convertView.findViewById(R.id.procedure_name);
            vh.startTime = convertView.findViewById(R.id.procedure_start_time);
            vh.duration = convertView.findViewById(R.id.procedure_duration);
            vh.message = convertView.findViewById(R.id.procedure_message);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        initItem(vh, tdp);

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    private void initItem(final ViewHolder vh, TimeDependentProcedure tdp) {
        vh.name.setText(tdp.procedure().toString());

        if (tdp.status().equals(TimeDependentProcedure.Status.STOPPED)) {
            vh.status.setBackgroundResource(R.drawable.icon_clock_small);
            vh.startTime.setText("");
            vh.startTime.setVisibility(View.GONE);
            vh.duration.setText("");
            vh.duration.setVisibility(View.GONE);
            vh.message.setText(R.string.procedure_not_active_label);
            vh.message.setVisibility(View.VISIBLE);

            for (int i = 0; i < vh.labels.length; i++) {
                vh.labels[i].setVisibility(View.GONE);
            }
        }

        if (tdp.status().equals(TimeDependentProcedure.Status.STARTED)) {
            vh.status.setBackgroundResource(R.drawable.icon_clock_small_green);

            vh.startTime.setText(tdp.startTime());
            vh.startTime.setVisibility(View.VISIBLE);
            vh.duration.setText(DateTimeUtils.convertSecondsToString(tdp.duration()));
            vh.duration.setVisibility(View.VISIBLE);
            vh.message.setText("");
            vh.message.setVisibility(View.GONE);

            for (int i = 0; i < vh.labels.length; i++) {
                vh.labels[i].setVisibility(View.VISIBLE);
            }
        }
    }

    public class DataSetObserver extends android.database.DataSetObserver {

        private LinearLayout container;

        public DataSetObserver(LinearLayout container) {
            this.container = container;
        }

        @Override
        public void onChanged() {
            super.onChanged();

            container.removeAllViews();

            for (int i = 0; i < getCount(); i += 2) {
                LinearLayout ll = new LinearLayout(getContext());
                ll.setOrientation(LinearLayout.HORIZONTAL);

                if (i < getCount()) {
                    ll.addView(getView(i, null, ll));
                }

                if (i + 1 < getCount()) {
                    ll.addView(getView(i + 1, null, ll));
                }

                container.addView(ll);
            }
        }
    }
}
