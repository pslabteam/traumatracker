package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.LocationService;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.Note;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.NoteStream;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.TimeSheet;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Room;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TimeDependentProcedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.TimeSheetAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialogWithTabs;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class ReportOverviewDialog extends TTDialogWithTabs {

    private Report.EventsTrack currentEventsTrack;

    private TimeSheetAdapter adapter;

    private final List<TimeDependentProcedure> procedures = new ArrayList<>();

    private TimeSheet.Listener timeSheetListener = new TimeSheet.Listener() {
        @Override
        public void updateRoomInfo(Room room) {
            ((TextView)findViewById(R.id.timesheet_room_description)).setText(room.description());
            ((TextView)findViewById(R.id.timesheet_room_datetime)).setText(String.format(getString(R.string.time_sheet_room_label), room.inTime(), room.inDate()));
            ((TextView)findViewById(R.id.timesheet_room_permanence)).setText(String.format(getString(R.string.time_sheet_room_duration), room.permanenceTime()));
        }

        @Override
        public void updateTimeDependentProcedureInfo(TimeDependentProcedure currentTdp) {
            boolean attached = false;

            for(TimeDependentProcedure tdp : procedures){
                if(tdp.procedure().equals(currentTdp.procedure())){
                    attached = true;
                    procedures.set(procedures.indexOf(tdp), currentTdp);
                }
            }

            if(!attached){
                procedures.add(currentTdp);
            }

            adapter.notifyDataSetChanged();
        }
    };

    public ReportOverviewDialog(final Context context) {
        super(context, R.layout.dialog_report_overview,
                new int[]{R.id.tab_realtime, R.id.tab_events},
                new int[]{R.id.tab_realtime_container, R.id.tab_events_container});

        intiUI();

        adapter = new TimeSheetAdapter(getContext(), procedures);
        adapter.registerDataSetObserver(adapter.new DataSetObserver(findViewById(R.id.timeSheetContainer)));
    }

    @Override
    protected void onStart() {
        super.onStart();

        TimeSheet ts = TimeSheet.reference();

        if(ts != null){
            ts.registerListener(timeSheetListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        TimeSheet ts = TimeSheet.reference();

        if(ts != null){
            ts.unregisterListener(timeSheetListener);
        }
    }

    @Override
    protected void onTabSelected(int tabId){

        switch(tabId){
            case R.id.tab_events:
                initUIEventTrack();
                break;

            case R.id.tab_realtime:
                //Nothing to do!
                break;
        }
    }

    private void intiUI() {
        findViewById(R.id.exitLabel).setOnClickListener(view -> dismiss());

        findViewById(R.id.location_detection_status_warning_label)
                .setVisibility(LocationService.isLocationDetectionActive() ? View.GONE : View.VISIBLE);
    }

    private void initUIEventTrack(){
        currentEventsTrack = new Report.EventsTrack();

        List<Note> events = NoteStream.requestEventsList();

        for(int i = 0; i < events.size(); i++){
            JSONObject event = events.get(i).getContent();

            try {
                currentEventsTrack.add(new Report.Event(
                        event.optInt(C.report.events.ID),
                        event.optString(C.report.events.DATE),
                        event.optString(C.report.events.TIME),
                        event.optString(C.report.events.PLACE),
                        event.optString(C.report.events.TYPE),
                        event.getJSONObject(C.report.events.CONTENT)
                ));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        fillEventsTrack(currentEventsTrack);
    }

    private void fillEventsTrack(Report.EventsTrack eventsTrack){
        TableLayout eventsTable = findViewById(R.id.events_table);
        eventsTable.removeAllViews();

        if (eventsTrack.isEmpty()) {
            TableRow row = new TableRow(getContext());
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));

            row.addView(createLabelForRow("--", 1f));
            row.addView(createLabelForRow("--", 5f));
            row.addView(createLabelForRow("--", 0.5f));

            eventsTable.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

            return;
        }

        TableLayout.LayoutParams rowLayout = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
        rowLayout.bottomMargin = 5;

        String lastPlace = "";
        String lastDate = "";

        List<Report.Event> events = new ArrayList<>(eventsTrack.getEvents());
        Collections.sort(events, (e1, e2) -> {
            String time1 = e1.date() + " " + e1.time();
            String time2 = e2.date() + " " + e2.time();
            return time1.compareTo(time2);
        });

        for (int i = 0; i < events.size(); i++) {
            Report.Event event = events.get(i);

            if(!event.date().equals(lastDate)){
                lastDate = event.date();

                TableRow row = new TableRow(getContext());
                row.addView(createDateLabel(lastDate));

                eventsTable.addView(row, rowLayout);
            }

            if(!event.place().equals(lastPlace)){
                lastPlace = event.place();

                TableRow row = new TableRow(getContext());
                row.addView(createPlaceLabel(lastPlace));

                eventsTable.addView(row, rowLayout);
            }

            TableRow row = new TableRow(getContext());
            row.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1.0f));
            row.setBackgroundColor(getResources().getColor(R.color.tt_light_grey));

            row.addView(createLabelForRow(event.time(), 1f));
            row.addView(createLabelForRow(event.description(), 5f));

            if(!(event.type().equals(C.report.events.type.ROOM_IN) || event.type().equals(C.report.events.type.ROOM_OUT))){
                row.addView(createUpdateButton(event.id(), event.description(), event.date(), event.time(), event.place()));
            } else {
                row.addView(new Space(getContext()));
            }

            eventsTable.addView(row, rowLayout);
        }
    }

    private TextView createPlaceLabel(String place){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        params.span = 2;

        TextView placeLabel = new TextView(getContext());
        placeLabel.setText(place);
        placeLabel.setTypeface(null, Typeface.BOLD);
        placeLabel.setPadding(5, 5, 5, 5);
        placeLabel.setTextColor(getResources().getColor(R.color.tt_white));
        placeLabel.setBackgroundResource(R.color.tt_red);
        placeLabel.setLayoutParams(params);

        return placeLabel;
    }

    private TextView createDateLabel(String date){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        params.span = 2;
        params.topMargin = 5;

        TextView dateLabel = new TextView(getContext());
        dateLabel.setText(date);
        dateLabel.setTypeface(null, Typeface.BOLD);
        dateLabel.setLayoutParams(params);
        dateLabel.setGravity(Gravity.START);

        return dateLabel;
    }

    private TextView createLabelForRow(String text, float weight) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, weight);
        params.gravity = Gravity.CENTER;
        params.leftMargin = 5;

        TextView txt = new TextView(getContext());
        txt.setText(text);
        txt.setPadding(2, 5, 2, 5);
        txt.setLayoutParams(params);

        return txt;
    }

    private Button createUpdateButton(final int eventId, final String eventDescription, final String eventCurrentDate, final String eventCurrentTime, final String eventCurrentPlace){
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        params.gravity = Gravity.CENTER_VERTICAL;
        params.bottomMargin = 5;
        params.rightMargin = 5;

        Button btn = new Button(getContext());
        btn.setText(getString(R.string.update_button_text));
        btn.setTextSize(12);
        btn.setBackgroundResource(R.drawable.traumatracker_sizable_button);
        btn.setPadding(2, 2, 2, 2);
        btn.setLayoutParams(params);
        btn.setOnClickListener(view -> new UpdateEventInfoDialog(getContext(), eventDescription, eventCurrentDate, eventCurrentTime, eventCurrentPlace, (newDate, newTime, newPlace) -> {
            currentEventsTrack.updateEventTemporalInfo(eventId, newDate, newTime, newPlace);
            fillEventsTrack(currentEventsTrack);
            NoteStream.requestEventInfoUpdate(eventId, newDate, newTime, newPlace);
        }).show());

        return btn;
    }
}
