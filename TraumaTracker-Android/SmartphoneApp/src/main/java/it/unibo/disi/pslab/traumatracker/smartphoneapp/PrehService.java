package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.os.Environment;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cartago.OPERATION;
import cartago.OpFeedbackParam;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.tools.NetworkManager;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.MultipartUtility;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class PrehService extends JaCaArtifact {

    void init() { }

    @OPERATION public void retrievePrehMissions(OpFeedbackParam<Integer> rescode, OpFeedbackParam<JSONArray> missions){
        try {
            final Pair<Integer, String> res = NetworkManager.doGET(formatURL("/api/v1/missions?ongoing=true"));

            if (res.first == 200) {
                missions.set(new JSONArray(res.second));
            } else {
                missions.set(new JSONArray());
            }

            rescode.set(res.first);

        } catch (IOException | JSONException e) {
            rescode.set(404);
            missions.set(new JSONArray());
        }
    }

    @OPERATION public void retrievePrehMissionData(String missionId, OpFeedbackParam<Integer> rescode, OpFeedbackParam<JSONObject> patient, OpFeedbackParam<JSONObject> vs){
        try {
            final Pair<Integer, String> res = NetworkManager.doGET(formatURL("/api/v1/patients?missionId="+missionId));

            if (res.first == 200) {
                final JSONObject patientObj = new JSONArray(res.second).getJSONObject(0);
                patient.set(patientObj);

                final Pair<Integer, String> res2 = NetworkManager.doGET(formatURL("/api/v1/patients/"+patientObj.optString("_id")+"/vital_signs?limit=last"));

                if(res2.first == 200){

                    final JSONObject vsObj = new JSONObject(res2.second);
                    vs.set(vsObj);
                } else {
                    vs.set(new JSONObject());
                }
            } else {
                patient.set(new JSONObject());
                vs.set(new JSONObject());
            }

            rescode.set(res.first);
        } catch (IOException | JSONException e) {
            rescode.set(404);
            patient.set(new JSONObject());
            vs.set(new JSONObject());
        }
    }

    private String formatURL(String url) {
        return C.url.HTTP_PROTOCOL_PREFIX
                + readSharedPreference(Preferences.ID, Preferences.PREH_SERVICE_IP, C.url.DEFAULT_PREH_SERVICE_IP)
                + ":"
                + C.url.GT2_PREH_SERVICE_PORT
                + url;
    }
}