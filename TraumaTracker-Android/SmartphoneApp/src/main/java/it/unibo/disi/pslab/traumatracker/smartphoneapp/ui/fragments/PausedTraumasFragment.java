package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments;

import android.database.DataSetObserver;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.TinyReport;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.TemporaryReportsAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialogWithChoice;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;

public class PausedTraumasFragment extends Fragment {

    public static PausedTraumasFragment newInstance() {
        return new PausedTraumasFragment();
    }

    private TemporaryReportsAdapter adapter;

    private View mainView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_paused_traumas, container, false);

        adapter = new TemporaryReportsAdapter(getContext(), getTempReportsFromFolder());
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                updateUI();
            }
        });

        initUI();

        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAdapterData();
    }

    private void initUI() {
        ((ListView) mainView.findViewById(R.id.reports_list)).setAdapter(adapter);

        mainView.findViewById(R.id.buttonDeleteAll).setOnClickListener(v -> new MessageDialogWithChoice(getContext(), getString(R.string.warning), "Confermando saranno eliminati da questo dispositivo tutti i report temporanei.", new MessageDialogWithChoice.Listener() {
            @Override
            public void onNoButtonClicked() {
                //nothing to do!
            }

            @Override
            public void onYesButtonClicked() {
                FileSystemUtils.emptyFolder(C.fs.TEMP_FOLDER);
                new MessageDialog(getContext(), getString(R.string.success), "Tutti i report temporanei sono stati cancellati!", null).show();
                refreshAdapterData();
            }
        }).show());

        updateUI();
    }

    private void updateUI(){
        if(adapter.getCount() == 0){
            //NO REPORTS
            mainView.findViewById(R.id.noUsersLabel).setVisibility(View.VISIBLE);
            mainView.findViewById(R.id.commands).setVisibility(View.GONE);
            return;
        }

        mainView.findViewById(R.id.noUsersLabel).setVisibility(View.GONE);
        mainView.findViewById(R.id.commands).setVisibility(View.VISIBLE);
    }

    private void refreshAdapterData(){
        final List<TinyReport> reports = getTempReportsFromFolder();

        if(reports.size() != adapter.getCount()){
            adapter.clear();
            adapter.addAll(reports);
            adapter.notifyDataSetChanged();
        }
    }

    private List<TinyReport> getTempReportsFromFolder() {
        final List<TinyReport> tinyReports = new ArrayList<>();

        final File[] files = FileSystemUtils.getFilesFromDirectory(C.fs.TEMP_FOLDER);

        if(files != null && files.length > 0){
            for (final File file : files) {
                final String fileContent = FileSystemUtils.loadFile(C.fs.TEMP_FOLDER, file.getName());
                if(!fileContent.isEmpty()){
                    tinyReports.add(new TinyReport(fileContent));
                } else {
                    //nothing to do!
                }
            }
        }

        return tinyReports;
    }
}
