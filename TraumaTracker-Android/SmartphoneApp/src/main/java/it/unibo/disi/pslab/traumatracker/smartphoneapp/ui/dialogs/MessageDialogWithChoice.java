package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class MessageDialogWithChoice extends TTDialog {

    public MessageDialogWithChoice(Context context, String title, String message, final Listener listener) {
        super(context, R.layout.dialog_message_with_choice);

        intiUI(title, message, "", listener);
    }

    public MessageDialogWithChoice(Context context, String title, String message, String warningMessage, final Listener listener) {
        super(context, R.layout.dialog_message_with_choice);

        intiUI(title, message, warningMessage, listener);
    }

    private void intiUI(String title, String message, String warningMessage, final Listener listener) {
        (((TextView) findViewById(R.id.title))).setText(title);
        (((TextView) findViewById(R.id.noUsersLabel))).setText(message);

        findViewById(R.id.yesButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onYesButtonClicked();
            }

            dismiss();
        });

        findViewById(R.id.noButton).setOnClickListener(v -> {
            if (listener != null) {
                listener.onNoButtonClicked();
            }

            dismiss();
        });

        if(warningMessage.isEmpty()){
            findViewById(R.id.warning_label).setVisibility(View.GONE);
        } else {
            findViewById(R.id.warning_label).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.warning_label)).setText(warningMessage);
        }

    }

    public interface Listener {
        void onNoButtonClicked();
        void onYesButtonClicked();
    }
}
