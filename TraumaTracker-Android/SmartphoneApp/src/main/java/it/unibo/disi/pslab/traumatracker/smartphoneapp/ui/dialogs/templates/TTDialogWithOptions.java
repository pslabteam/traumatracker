package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates;

import android.content.Context;
import android.view.ViewStub;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.HashMap;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerEditText;

public abstract class TTDialogWithOptions<T> extends TTDialog {

    private Map<String, Object> optionsMap = new HashMap<>();

    public TTDialogWithOptions(Context context, int layout) {
        super(context, layout);
    }

    protected Map<String, Object> getOptionsMap(){
        return optionsMap;
    }

    protected void initializeOptionsMap(String... elements){
        for(final String e : elements){
            optionsMap.put(e, new Object());
        }
    }

    protected void updateOption(String key, Object value){
        optionsMap.put(key, value);
    }

    protected void inflateOptionsStub(int layout){
        final ViewStub stub = findViewById(R.id.optionsContainer);
        stub.setLayoutResource(layout);
        stub.inflate();
    }

    protected abstract void retrieveOptions(T obj);
    protected abstract void inflateOptions(T obj);

    protected boolean getCheckBoxStatus(int selector){
        return ((CheckBox) findViewById(selector)).isChecked();
    }

    protected String getSwitchCheckedText(int selector) {
        String value = "";

        int res = ((RadioGroup) findViewById(selector)).getCheckedRadioButtonId();

        if (res != -1) {
            value = ((RadioButton) findViewById(res)).getText().toString().toLowerCase();
        }

        return value;
    }

    protected String getFieldText(int res){
        return ((TraumaTrackerEditText) findViewById(res)).getText().toString();
    }

    protected void initFieldListener(int res, String title, String unit){
        ((TraumaTrackerEditText) findViewById(res)).setOnClickListener(getContext(), title, unit, new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                //TODO: dipende da quale option stiamo considerando (utilizzare res per lo switch?)
                return true;
            }

            @Override
            public String errorMessage() {
                return "";
            }
        });
    }
}