package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.CompletedTraumasFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.PausedTraumasFragment;

public class StarterUiTabsAdapter extends FragmentPagerAdapter {

    private static final int PAGES = 2;

    public StarterUiTabsAdapter(final FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(final int position) {

        if(position >= PAGES)
            return null;

        switch (position) {
            case 0: return PausedTraumasFragment.newInstance();
            case 1: return CompletedTraumasFragment.newInstance();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return PAGES;
    }
}
