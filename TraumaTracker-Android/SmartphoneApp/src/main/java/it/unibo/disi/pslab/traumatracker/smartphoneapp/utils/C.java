package it.unibo.disi.pslab.traumatracker.smartphoneapp.utils;

import android.support.annotation.NonNull;

public class C {

    public static final String LOG_TAG = "[TraumaTracker]";
    public static final boolean LOG_ACTIVE = false;

    public static final String REPORT_VERSION = "1.4";

    public static final int PATIENT_CODE_MAX_DIGITS = 11;

    public static class bt {
        public static final String DEFAULT_SERVER_NAME = "TT-Bluetooth-Server";
        public static final String UUID = "c4daba7b-14c1-4cc1-b117-87ff24b9f21d";
    }

    public static class fs {
        public static final String TRAUMA_TRACKER_HOME_DIR = "/TraumaTrackerMedia";

        public static final String CONFIG_FOLDER = TRAUMA_TRACKER_HOME_DIR + "/.config";
        public static final String LOG_FOLDER = TRAUMA_TRACKER_HOME_DIR + "/.log";
        public static final String TEMP_FOLDER = TRAUMA_TRACKER_HOME_DIR + "/.temp";

        public static final String REPORTS_FOLDER = TRAUMA_TRACKER_HOME_DIR + "/Reports";
        public static final String REPORTS_SYNC_FOLDER = REPORTS_FOLDER + "/Sync";
        public static final String REPORTS_TOSYNC_FOLDER = REPORTS_FOLDER + "/ToSync";

        public static final String PICTURES_FOLDER = TRAUMA_TRACKER_HOME_DIR + "/Pictures";
        public static final String VIDEOS_FOLDER = TRAUMA_TRACKER_HOME_DIR + "/Videos";
        public static final String VOCAL_NOTES_FOLDER = TRAUMA_TRACKER_HOME_DIR + "/AudioNotes";

        public static final String REPORT_FILE_NAME_PREFIX = "rep-";
        public static final String REPORT_FILE_EXTENSION = ".ttr";

        public static final String PHOTO_FILE_EXTENSION = ".jpg";
        public static final String VIDEO_FILE_EXTENSION = ".mp4";
        public static final String VOCAL_FILE_EXTENSION = ".aac";

        public static final String LOG_FILE_NAME_PREFIX = "logcat-";
        public static final String LOG_FILE_EXTENSION = ".txt";
    }

    public static class url {
        public static final String HTTP_PROTOCOL_PREFIX = "http://";
        public static final String WEB_SOCKET_PROTOCOL_PREFIX = "ws://";

        public static final String DEFAULT_TT_SERVICE_IP = "127.0.0.1";
        public static final String DEFAULT_PREH_SERVICE_IP = "127.0.0.1";

        public static final String GT2_TT_SERVICE_PORT = "8080";
        public static final String GT2_GATEWAY_SERVICE_PORT = "8082";
        public static final String GT2_LOCATION_SERVICE_PORT = "8083";

        public static final String GT2_PREH_SERVICE_PORT = "8080";

        public static final String DEFAULT_BLE_TAG_ID = "0000";

        public static final String GET_PLACE_PREFIX = "/gt2/locationservice/api/tags/";

        public static final String GET_VITALSIGNS_PREFIX = "/gt2/vsservice/api/monitors";
        public static final String GET_VITALSIGNS_SUFFIX = "/vitalsigns";

        public static final String GET_USERS = "/gt2/traumatracker/api/users";

        public static final String POST_REPORT = "/gt2/traumatracker/api/reports";
        public static final String POST_PHOTOS = "/gt2/traumatracker/photo";
        public static final String POST_VIDEOS = "/gt2/traumatracker/video";
        public static final String POST_VOCALS = "/gt2/traumatracker/audio";
    }

    public static class vs {
        public static final String VS_SYS = "SYS";
        public static final String VS_DIA = "DIA";
        public static final String VS_HR = "HR";
        public static final String VS_ETCO2 = "EtCO2";
        public static final String VS_SPO2 = "SpO2";
        public static final String VS_TEMP = "Temp";
    }

    public static class place {
        public static final String TRANSPORT = "Trasporto";
        public static final String PREH = "PRE-H";
        public static final String SHOCK_ROOM = "Shock-Room";
        public static final String ER_CT = "TAC PS";
        public static final String GENERAL_OPERATING_ROOM = "Sala Operatoria: Blocco";
        public static final String NEUROSURGERY_OPERATING_ROOM = "Sala Operatoria: Neurochirurgia";
        public static final String ORTOPEDICAL_OPERATIG_ROOM = "Sala Operatoria: Ortopedia";
        public static final String ANGIOGRAPHIC_ROOM = "Sala Angiografica";
        public static final String ICU_1 = "Terapia Intensiva: TI-1";
        public static final String ICU_2 = "Terapia Intensiva: TI-2";
    }

    public static class languages {
        public static final String IT = "IT";
        public static final String EN = "EN";
    }

    public static class user {
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String SURNAME = "surname";
        public static final String USER_ID = "userId";
        public static final String ROLE = "role";
        public static final String EMAIL = "email";
        public static final String TOKEN = "token";
    }

    public static class report {
        public static final String ID = "_id";
        public static final String VERSION = "_version";
        public static final String VALIDATION = "_validation";
        public static final String START_OPERATOR_ID = "startOperatorId";
        public static final String START_OPERATOR_DESCRIPTION = "startOperatorDescription";
        public static final String DELAYED_ACTIVATION = "delayedActivation";
        public static final String TRAUMA_TEAM_MEMBERS = "traumaTeamMembers";
        public static final String START_DATE = "startDate";
        public static final String START_TIME = "startTime";
        public static final String END_DATE = "endDate";
        public static final String END_TIME = "endTime";
        public static final String ISS = "iss";
        public static final String FINAL_DESTINATION = "finalDestination";
        public static final String TRAUMA_INFO = "traumaInfo";
        public static final String MAJOR_TRAUMA_CRITERIA = "majorTraumaCriteria";
        public static final String ANAMNESI = "anamnesi";
        public static final String PREH = "preh";
        public static final String PATIENT_INITIAL_CONDITION = "patientInitialCondition";
        public static final String EVENTS = "events";

        public static class validation {
            public static final String IS_VALIDATED = "isValidated";
            public static final String OPERATOR_ID = "operatorId";
            public static final String DATE = "validationDate";
            public static final String TIME = "validationTime";
        }

        public static class delayedActivation {
            public static final String STATUS = "isDelayedActivation";
            public static final String ORIGINAL_ACCESS_DATE = "originalAccessDate";
            public static final String ORIGINAL_ACCESS_TIME = "originalAccessTime";
        }

        public static class iss {
            public static final String IS_PRESUMED = "isPresumed";
            public static final String TOTAL_ISS = "totalIss";
            public static final String AIS_MAP_TYPE = "aisMapType";
            public static final String AIS_MAP = "aisMap";

            public static class aisMap {
                public static final String EXTERNA_GROUP = "externaGroup";
                public static final String HEAD_GROUP = "headGroup";
                public static final String TORAX_GROUP = "toraxGroup";
                public static final String ABDOMEN_GROUP = "abdomenGroup";
                public static final String PELVIC_GIRDLE_GROUP = "pelvicGirdleGroup";
                public static final String EXTREMITIES_GROUP = "extremitiesGroup";

                public static class externaGroup {
                    public static final String TOTAL_ISS = "groupTotalIss";
                    public static final String EXTERNA_AIS = "externaAis";
                }

                public static class headGroup {
                    public static final String TOTAL_ISS = "groupTotalIss";
                    public static final String HEAD_NECK_AIS = "headNeckAis";
                    public static final String BRAIN_AIS = "brainAis";
                    public static final String CERVICAL_SPINE_AIS = "cervicalSpineAis";
                    public static final String FACE_AIS = "faceAis";
                }

                public static class toraxGroup {
                    public static final String TOTAL_ISS = "groupTotalIss";
                    public static final String TORAX_AIS = "toraxAis";
                    public static final String SPINE_AIS = "spineAis";
                }

                public static class abdomenGroup {
                    public static final String TOTAL_ISS = "groupTotalIss";
                    public static final String ABDOMEN_AIS = "abdomenAis";
                    public static final String LUMBAR_SPINE_AIS = "lumbarSpineAis";
                }

                public static class pelvicGirdleGroup {
                    public static final String TOTAL_ISS = "groupTotalIss";
                    public static final String PELVIC_GIRDLE_AIS = "pelvicGirdleAis";
                }

                public static class extremitiesGroup {
                    public static final String TOTAL_ISS = "groupTotalIss";
                    public static final String UPPER_EXTREMITIES_AIS = "upperExtremitiesAis";
                    public static final String LOWER_EXTREMITIES_AIS = "lowerExtremitiesAis";
                }
            }
        }

        public static class traumaInfo {
            public static final String CODE = "code";
            public static final String SDO = "sdo";
            public static final String ER_DECEASED = "erDeceased";
            public static final String ADMISSION_CODE = "admissionCode";
            public static final String NAME = "name";
            public static final String SURNAME = "surname";
            public static final String GENDER = "gender";
            public static final String DOB = "dob";
            public static final String AGE = "age";
            public static final String ACCIDENT_DATE = "accidentDate";
            public static final String ACCIDENT_TIME = "accidentTime";
            public static final String ACCIDENT_TYPE = "accidentType";
            public static final String VEHICLE = "vehicle";
            public static final String FROM_OTHER_EMERGENCY = "fromOtherEmergency";
            public static final String OTHER_EMERGENCY = "otherEmergency";
        }

        public static class majorTraumaCriteria {
            public static final String DYNAMIC = "dynamic";
            public static final String PHYSIOLOGICAL = "physiological";
            public static final String ANATOMICAL = "anatomical";
        }

        public static class anamnesi {
            public static final String ANTIPLATELETS = "antiplatelets";
            public static final String ANTICOAGULANTS = "anticoagulants";
            public static final String NAO = "nao";
        }

        public static class preh {
            public static final String TERRITORIAL_AREA = "territorialArea";
            public static final String CAR_ACCIDENT = "isCarAccident";
            public static final String A_AIRWAYS = "aAirways";
            public static final String B_PLEURAL_DECOMPRESSION = "bPleuralDecompression";
            public static final String C_BLOOD_PROTOCOL = "cBloodProtocol";
            public static final String C_TPOD = "cTpod";
            public static final String D_GCS_TOTAL = "dGcsTotal";
            public static final String D_ANISOCORIA = "dAnisocoria";
            public static final String D_MIDRIASI = "dMidriasi";
            public static final String E_MOTILITY = "eMotility";
            public static final String WORST_BLOOD_PRESSURE = "worstBloodPressure";
            public static final String WORST_RESPIRATORY_RATE = "worstRespiratoryRate";
        }

        public static class patientInitialCondition {

            public static final String VITAL_SIGNS = "vitalSigns";
            public static final String CLINICAL_PICTURE = "clinicalPicture";

            public static class vitalSigns {
                public static final String TEMPERATURE = "temp";
                public static final String HEART_RATE = "hr";
                public static final String BLOOD_PRESSURE = "bp";
                public static final String SPO2 = "spo2";
                public static final String ETCO2 = "etco2";
            }

            public static class clinicalPicture {
                public static final String GCS_TOTAL = "gcsTotal";
                public static final String GCS_MOTOR = "gcsMotor";
                public static final String GCS_VERBAL = "gcsVerbal";
                public static final String GCS_EYES = "gcsEyes";
                public static final String SEDATED = "sedated";
                public static final String PUPILS = "pupils";
                public static final String AIRWAYS = "airway";
                public static final String POSITIVE_INHALATION = "positiveInhalation";
                public static final String INTUBATION_FAILED = "intubationFailed";
                public static final String CHEST_TUBE = "chestTube";
                public static final String OXYGEN_PERCENTAGE = "oxygenPercentage";
                public static final String HEMORRHAGE = "hemorrhage";
                public static final String LIMBS_FRACTURE = "limbsFracture";
                public static final String FRACTURE_EXPOSITION = "fractureExposition";
                public static final String BURN = "burn";
            }
        }

        public static class events {
            public static final String ID = "eventId";
            public static final String DATE = "date";
            public static final String TIME = "time";
            public static final String PLACE = "place";
            public static final String TYPE = "type";
            public static final String CONTENT = "content";

            public static class type {
                public static final String PROCEDURE = "procedure";
                public static final String DIAGNOSTIC = "diagnostic";
                public static final String DRUG = "drug";
                public static final String BLOOD_PRODUCT = "blood-product";
                public static final String VITAL_SIGNS_MON = "vital-signs-mon";
                public static final String CLINICAL_VARIATION = "clinical-variation";
                public static final String PHOTO = "photo";
                public static final String VIDEO = "video";
                public static final String VOCAL_NOTE = "vocal-note";
                public static final String TEXT_NOTE = "text-note";
                public static final String TRAUMA_LEADER = "trauma-leader";
                public static final String ROOM_IN = "room-in";
                public static final String ROOM_OUT = "room-out";
                public static final String REPORT_REACTIVATION = "report-reactivation";
                public static final String PATIENT_ACCEPTED = "patient-accepted";
            }

            public static class procedure {
                public static final String ID = "procedureId";
                public static final String DESCRIPTION = "procedureDescription";
                public static final String TYPE = "procedureType";
                public static final String EVENT = "event";

                public static class type {
                    public static final String ONE_SHOT = "one-shot";
                    public static final String TIME_DEPENDENT = "time-dependent";
                }

                public static class event {
                    public static final String START = "start";
                    public static final String END = "end";
                }

                public static class option {
                    public static final String INTUBATION_DIFFICULT_AIRWAY = "difficultAirway";
                    public static final String INTUBATION_INHALATION = "inhalation";
                    public static final String INTUBATION_VIDEOLARINGO = "videolaringo";
                    public static final String INTUBATION_FROVA = "frova";

                    public static final String DRAINAGE_LEFT = "left";
                    public static final String DRAINAGE_RIGHT = "right";

                    public static final String CHESTTUBE_LEFT = "left";
                    public static final String CHESTTUBE_RIGHT = "right";
                }
            }

            public static class diagnostic {
                public static final String ID = "diagnosticId";
                public static final String DESCRIPTION = "diagnosticDescription";

                public static class option {
                    public static final String ABG_LACTATES = "lactates";
                    public static final String ABG_BE = "be";
                    public static final String ABG_PH = "ph";
                    public static final String ABG_HB = "hb";

                    public static final String ROTEM_FIBTEM = "fibtem";
                    public static final String ROTEM_EXTEM = "extem";
                    public static final String ROTEM_HYPERFIBRINOLYSIS = "hyperfibrinolysis";
                }
            }

            public static class drug {
                public static final String ID = "drugId";
                public static final String DESCRIPTION = "drugDescription";
                public static final String ADMINISTRATION_TYPE = "administrationType";
                public static final String EVENT = "event";
                public static final String QUANTITY = "qty";
                public static final String UNIT = "unit";
                public static final String DURATION = "duration";

                public static class type {
                    public static final String ONE_SHOT = "one-shot";
                    public static final String CONTINUOUS_INFUSION = "continuous-infusion";
                    public static final String DRUG_PROTOCOL = "drug-protocol";
                }

                public static class event {
                    public static final String START = "start";
                    public static final String VARIATION = "variation";
                    public static final String STOP = "stop";
                }
            }

            public static class bloodProduct {
                public static final String ID = "bloodProductId";
                public static final String DESCRIPTION = "bloodProductDescription";
                public static final String ADMINISTRATION_TYPE = "administrationType";
                public static final String QUANTITY = "qty";
                public static final String UNIT = "unit";
                public static final String BAG_CODE = "bagCode";

                public static class type {
                    public static final String ONE_SHOT = "one-shot";
                    public static final String BLOOD_PROTOCOL = "blood-protocol";
                }
            }

            public static class clinicalVariation {
                public static final String ID = "variationId";
                public static final String DESCRIPTION = "variationDescription";
                public static final String VALUE = "value";
            }

            public static class vitalSignMon {
                public static final String TEMP = "Temp";
                public static final String HR = "HR";
                public static final String DIA = "DIA";
                public static final String SYS = "SYS";
                public static final String SPO2 = "SpO2";
                public static final String ETCO2 = "EtCO2";
            }

            public static class photo {
                public static final String DESCRIPTION = "description";
            }

            public static class video {
                public static final String DESCRIPTION = "description";
            }

            public static class vocalNote {
                public static final String DESCRIPTION = "description";
            }

            public static class textNote {
                public static final String TEXT = "text";
            }

            public static class traumaLeader {
                public static final String ID = "userId";
                public static final String NAME = "name";
                public static final String SURNAME = "surname";
            }

            public static class roomIn {
                public static final String PLACE = "place";
            }

            public static class roomOut {
                public static final String PLACE = "place";
            }
        }
    }
}
