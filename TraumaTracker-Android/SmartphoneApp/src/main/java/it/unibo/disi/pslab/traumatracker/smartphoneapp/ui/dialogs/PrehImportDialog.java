package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.PrehMission;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.PrehMissionAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class PrehImportDialog extends TTDialog {

    private PrehMissionAdapter adapter;

    public PrehImportDialog(Context context, JSONArray list, Listener listener) {
        super(context, R.layout.dialog_preh_import);

        final List<PrehMission> missions = new ArrayList<>();

        for(int i = 0; i < list.length(); i++){
            try {
                final JSONObject obj = list.getJSONObject(i);
                missions.add(new PrehMission(
                        obj.optString("_id"),
                        obj.optString("medic"),
                        obj.optString("vehicle"),
                        obj.optString("missionCode"),
                        obj.optJSONObject("tracking")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter = new PrehMissionAdapter(context, missions, listener);

        intiUI();
    }

    private void intiUI() {
        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());
        ((ListView) findViewById(R.id.missions_list)).setAdapter(adapter);
    }

    public interface Listener {
        void onImportMission(final String missionId);
    }
}
