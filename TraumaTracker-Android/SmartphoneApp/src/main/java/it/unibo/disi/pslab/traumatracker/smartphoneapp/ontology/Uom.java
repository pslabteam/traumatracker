package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import android.support.annotation.NonNull;

public enum Uom {
    ML("ml"),
    G("g"),
    MG("mg"),
    POOL("pool"),
    UI("UI"),
    MCG("mcg"),
    MLH("ml/h");

    private final String value;

    Uom(String value){
        this.value = value;
    }

    @NonNull
    public String toString(){
        return this.value;
    }
}
