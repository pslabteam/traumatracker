package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum MajorTraumaCriteriaElement {
    DYNAMIC("dynamic", R.string.mtc_dynamic_label),
    PHYSIOLOGICAL("physiological", R.string.mtc_physiological_label),
    ANATOMICAL("anatomical", R.string.mtc_anatomical_label);

    private final int labelRes;

    MajorTraumaCriteriaElement(@SuppressWarnings("unused")  final String id, final int labelRes){
        this.labelRes = labelRes;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }
}
