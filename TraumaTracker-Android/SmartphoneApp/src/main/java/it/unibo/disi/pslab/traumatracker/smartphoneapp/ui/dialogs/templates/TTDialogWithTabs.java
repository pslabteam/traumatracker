package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public class TTDialogWithTabs extends TTDialog {

    public TTDialogWithTabs(Context context, int layout, int[] tabs, int[] containers) {
        super(context, layout);

        initTabs(tabs, containers);
    }

    private void initTabs(final int[] tabs, final int[] containers){
        if(tabs.length == 0)
            return;

        for(int i = 0; i < tabs.length; i++){
            final int tabId = i;
            findViewById(tabs[i]).setOnClickListener(tab -> {
                switchTab(tabs, containers, tabId);
                onTabSelected(tabs[tabId]);
            });
        }

        switchTab(tabs, containers, 0);
    }

    private void switchTab(final int[] tabs, final int[] containers, final int id){
        for(int i = 0; i < tabs.length; i++){
            TextView tab = findViewById(tabs[i]);
            tab.setBackgroundResource(R.drawable.traumatracker_tab);
            tab.setTypeface(null, Typeface.NORMAL);

            findViewById(containers[i]).setVisibility(View.GONE);
        }

        TextView selectedTab = findViewById(tabs[id]);
        selectedTab.setBackgroundResource(R.drawable.traumatracker_tab_selected);
        selectedTab.setTypeface(null, Typeface.BOLD);

        findViewById(containers[id]).setVisibility(View.VISIBLE);
    }

    protected void onTabSelected(int tabRes){}
}
