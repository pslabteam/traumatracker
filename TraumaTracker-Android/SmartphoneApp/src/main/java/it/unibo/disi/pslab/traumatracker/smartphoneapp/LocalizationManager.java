package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import java.util.Locale;
import java.util.Map;

import cartago.OPERATION;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class LocalizationManager extends JaCaArtifact {

    private static Localization currentLocalization = Localization.EN;

    void init(){
        String currentLanguage = readSharedPreference(Preferences.ID, Preferences.LANGUAGE, C.languages.EN);

        switch (currentLanguage){
            case C.languages.EN:
                currentLocalization = Localization.EN;
                break;

            case C.languages.IT:
                currentLocalization = Localization.IT;
                break;

            default:
                currentLocalization = Localization.EN;
                break;
        }

        setUILanguage(currentLanguage.toLowerCase());
    }

    @OPERATION
    public void changeLocalization(){

        String currentLanguage = readSharedPreference(Preferences.ID, Preferences.LANGUAGE, C.languages.EN);

        switch (currentLanguage){
            case C.languages.EN:
                currentLocalization = Localization.EN;
                break;

            case C.languages.IT:
                currentLocalization = Localization.IT;
                break;

            default:
                currentLocalization = Localization.EN;
                break;
        }

        setUILanguage(currentLanguage.toLowerCase());
    }

    private void setUILanguage(String language){
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.locale = locale;

        Resources res = getApplicationContext().getResources();
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    public static <E extends Enum<E>> String localizeElement(E e, Map<Localization, String[]> map){
        return map.get(currentLocalization)[e.ordinal()];
    }

    public enum Localization{
        EN(C.languages.EN),
        IT(C.languages.IT);

        public String description;

        Localization(String description){
            this.description = description;
        }

        @NonNull
        @Override
        public String toString(){
            return description;
        }
    }
}
