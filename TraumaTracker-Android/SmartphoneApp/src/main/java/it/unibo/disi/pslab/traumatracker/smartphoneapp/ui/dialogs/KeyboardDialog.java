package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.exceptions.CheckerException;

public class KeyboardDialog extends TTDialog {

    private static final String ZERO = "0";
    private static final String COMMA = ",";
    private static final String CANC = "C";
    private static final String DOT = ".";
    private static final String MINUS = "-";

    private static final String EMPTY = "";

    private boolean firstDigit = true;

    private EditText number;

    private final Listener listener;
    private final QuantityChecker checker;

    private SparseArray<Button> keyboard = new SparseArray<>();

    public KeyboardDialog(Context context, String title, String previousValue, String unit, final Listener listener, final QuantityChecker checker) {
        super(context, R.layout.dialog_keyboard);

        this.listener = listener;
        this.checker = checker;

        initUI(title, previousValue, unit);

        showKeyboard();
    }

    public KeyboardDialog setIntegerValue(boolean setted){
        if(setted){
            keyboard.get(R.id.padComma).setVisibility(View.GONE);
        } else {
            keyboard.get(R.id.padComma).setVisibility(View.VISIBLE);
        }

        return this;
    }

    public KeyboardDialog setUnsignedValue(boolean setted){
        if(setted){
            keyboard.get(R.id.padMinus).setVisibility(View.GONE);
        } else {
            keyboard.get(R.id.padMinus).setVisibility(View.VISIBLE);
        }

        return this;
    }

    private void initUI(String title, String previousValue, String unit) {
        ((TextView) findViewById(R.id.title)).setText(title);

        number = findViewById(R.id.number);

        if(!previousValue.equals("")){
            number.setText(previousValue.replace(DOT, COMMA));
        }

        EditText unitEdit = findViewById(R.id.unit);

        if(unit.equals(EMPTY)){
            unitEdit.setVisibility(View.GONE);
        } else {
            unitEdit.setVisibility(View.VISIBLE);
            unitEdit.setText(unit);
        }

        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.closeButton).setOnClickListener(v -> {
            firstDigit = true;
            String content = number.getText().toString().replace(COMMA, DOT);

            try{
                if(checker != null && !checker.checkInput(content)){
                    throw new CheckerException();
                }
            } catch (NullPointerException | NumberFormatException e) {
                if(!content.equals(EMPTY)){
                    new MessageDialog(getContext(),
                            getString(R.string.warning),
                            getString(R.string.value_error_message),
                            null).show();
                    return;
                }
            } catch (CheckerException e){
                new MessageDialog(getContext(),
                        getString(R.string.warning),
                        getString(R.string.value_error_message) + "\n" + checker.errorMessage(),
                        null).show();
                return;
            }

            listener.onConfirmationButtonClicked(content);
            dismiss();
        });
    }

    private void showKeyboard() {
        int[] padButtons = new int[]{
                R.id.pad0, R.id.pad1, R.id.pad2, R.id.pad3, R.id.pad4, R.id.pad5,
                R.id.pad6, R.id.pad7, R.id.pad8, R.id.pad9, R.id.padC, R.id.padComma, R.id.padMinus
        };

        for (int padButton : padButtons) {
            Button b = findViewById(padButton);
            b.setText(b.getTag().toString());
            b.setOnClickListener(v -> manageNumberText(v.getTag().toString()));

            keyboard.append(padButton, b);
        }
    }

    private void manageNumberText(String value) {
       if (firstDigit) {
            number.setText(EMPTY);
            firstDigit = false;
        }

        if (value.equals(MINUS)) {
            if (number.getText().toString().equals("")) {
                number.setText(String.format("%s%s", MINUS, number.getText()));
            }

            return;
        }

        if (value.equals(COMMA)) {
            if (!number.getText().toString().contains(COMMA) && !number.getText().toString().equals("") && !number.getText().toString().equals(MINUS)) {
                number.setText(String.format("%s%s", number.getText(), COMMA));
            }
            return;
        }

        if (value.equals(CANC)) {
            if (!number.getText().toString().equals(EMPTY)) {
                number.setText(number.getText().subSequence(0, number.getText().length() - 1));
            }
            return;
        }

        if (number.getText().toString().equals(ZERO)) {
            number.setText(EMPTY);
        }

        number.setText(String.format("%s%s", number.getText(), value));
    }

    public interface Listener {
        void onConfirmationButtonClicked(String value);
    }
}
