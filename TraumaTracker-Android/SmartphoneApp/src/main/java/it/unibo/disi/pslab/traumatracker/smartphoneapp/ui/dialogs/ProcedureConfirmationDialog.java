package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.Button;
import android.widget.TextView;

import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialogWithOptions;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class ProcedureConfirmationDialog extends TTDialogWithOptions<Procedure> {

    public ProcedureConfirmationDialog(Context context, Procedure procedure, String eventType, final Listener listener) {
        super(context, R.layout.dialog_procedure_confirmation);

        initUI(procedure, eventType, listener);

        inflateOptions(procedure);
    }

    private void initUI(final Procedure procedure, final String eventType, final Listener listener) {
        ((TextView) findViewById(R.id.title)).setText(procedure.toString());

        if(procedure.isTimeDependent() && eventType.equals(C.report.events.procedure.event.START)){
            ((Button) findViewById(R.id.closeButton)).setText(getString(R.string.procedure_start_button_text));
        } else if(procedure.isTimeDependent() && eventType.equals(C.report.events.procedure.event.END)){
            ((Button) findViewById(R.id.closeButton)).setText(getString(R.string.procedure_end_button_text));
        }

        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.closeButton).setOnClickListener(v -> {
            retrieveOptions(procedure);
            listener.onCloseButtonClicked(getOptionsMap());
            dismiss();
        });
    }

    @Override
    protected void inflateOptions(Procedure procedure) {
        switch (procedure) {
            case INTUBATION:
                inflateOptionsStub(R.layout.options_procedure_intubation);
                initializeOptionsMap(C.report.events.procedure.option.INTUBATION_DIFFICULT_AIRWAY,
                        C.report.events.procedure.option.INTUBATION_INHALATION,
                        C.report.events.procedure.option.INTUBATION_VIDEOLARINGO,
                        C.report.events.procedure.option.INTUBATION_FROVA);
                break;

            case DRAINAGE:
                inflateOptionsStub(R.layout.options_procedure_drainage);
                initializeOptionsMap(C.report.events.procedure.option.DRAINAGE_LEFT,
                        C.report.events.procedure.option.DRAINAGE_RIGHT);
                break;

            case CHEST_TUBE:
                inflateOptionsStub(R.layout.options_procedure_chesttube);
                initializeOptionsMap(C.report.events.procedure.option.CHESTTUBE_LEFT,
                        C.report.events.procedure.option.CHESTTUBE_RIGHT);
                break;

            default:
                break;
        }
    }

    @Override
    protected void retrieveOptions(Procedure reOp){
        switch (reOp) {
            case INTUBATION:
                updateOption(C.report.events.procedure.option.INTUBATION_DIFFICULT_AIRWAY, getCheckBoxStatus(R.id.intubation_option_1));
                updateOption(C.report.events.procedure.option.INTUBATION_INHALATION, getCheckBoxStatus(R.id.intubation_option_2));
                updateOption(C.report.events.procedure.option.INTUBATION_VIDEOLARINGO, getCheckBoxStatus(R.id.intubation_option_3));
                updateOption(C.report.events.procedure.option.INTUBATION_FROVA, getCheckBoxStatus(R.id.intubation_option_4));
                break;

            case DRAINAGE:
                updateOption(C.report.events.procedure.option.DRAINAGE_LEFT, getCheckBoxStatus(R.id.drainage_option_1));
                updateOption(C.report.events.procedure.option.DRAINAGE_RIGHT, getCheckBoxStatus(R.id.drainage_option_2));
                break;

            case CHEST_TUBE:
                updateOption(C.report.events.procedure.option.CHESTTUBE_LEFT, getCheckBoxStatus(R.id.chesttube_option_1));
                updateOption(C.report.events.procedure.option.CHESTTUBE_RIGHT, getCheckBoxStatus(R.id.chesttube_option_2));
                break;

            default:
                break;
        }
    }

    public interface Listener {
        void onCloseButtonClicked(Map<String, Object> options);
    }
}
