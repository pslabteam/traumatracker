package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum Diagnostics {

    //Instrumental Diagnostic
    ECHOFAST("echofast", R.string.ontology_diagnostics_echofast, DiagnosticsCategory.INSTRUMENTAL),
    TC_CEREBRAL_CERVICAL("cerebral-cervical-tc", R.string.ontology_diagnostics_cerebralcervicaltc , DiagnosticsCategory.INSTRUMENTAL),
    RX_CHEST("chest-rx", R.string.ontology_diagnostics_chestrx  , DiagnosticsCategory.INSTRUMENTAL),
    TC_THORACOABDOMINAL_MDC("thoracoabdominal-tc", R.string.ontology_diagnostics_thoracoabdominaltc , DiagnosticsCategory.INSTRUMENTAL),
    RX_ABDOMEN("abdomen-rx", R.string.ontology_diagnostics_abdomenrx , DiagnosticsCategory.INSTRUMENTAL),
    WB_TC("wb-tc", R.string.ontology_diagnostics_wbtc , DiagnosticsCategory.INSTRUMENTAL),

    //Laboratory Diagnostic
    ABG("abg", R.string.ontology_diagnostics_abg , DiagnosticsCategory.LABORATORY),
    ROTEM("rotem", R.string.ontology_diagnostics_rotem , DiagnosticsCategory.LABORATORY);

    private final String id;
    private final int labelRes;
    private DiagnosticsCategory category;

    Diagnostics(final String id, final int labelRes, DiagnosticsCategory category){
        this.id = id;
        this.labelRes = labelRes;
        this.category = category;
    }

    public String id(){
        return id;
    }

    public DiagnosticsCategory category() {
        return category;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }

    public static Diagnostics getById(String id){
        for(Diagnostics d : Diagnostics.values()){
            if(d.id().equals(id)){
                return  d;
            }
        }

        return null;
    }
}
