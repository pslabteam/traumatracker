package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Procedure;

public class TimeDependentProcedure {
    private Procedure procedure;

    private Status status;

    private String startDate, startTime;
    private int duration;

    public TimeDependentProcedure(Procedure procedure, Status status){
        this.procedure = procedure;
        this.status = status;
    }

    public Procedure procedure() {
        return procedure;
    }

    public Status status() {
        return status;
    }

    public void status(Status status) {
        this.status = status;
    }

    public String startDate() {
        return startDate;
    }

    public void startDate(String startDate) {
        this.startDate = startDate;
    }

    public String startTime() {
        return startTime;
    }

    public void startTime(String startTime) {
        this.startTime = startTime;
    }

    public int duration() {
        return duration;
    }

    public void duration(int duration) {
        this.duration = duration;
    }

    public enum Status {
        STARTED, STOPPED
    }
}
