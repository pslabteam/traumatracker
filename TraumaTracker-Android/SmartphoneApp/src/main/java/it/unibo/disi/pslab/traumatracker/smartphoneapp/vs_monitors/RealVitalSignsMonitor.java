package it.unibo.disi.pslab.traumatracker.smartphoneapp.vs_monitors;

import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.logging.Logger;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.tools.NetworkManager;

public class RealVitalSignsMonitor implements VitalSignsMonitor {

    private String url;

    private Logger logger;

    public RealVitalSignsMonitor(final String url){
        this(url, null);
    }

    public RealVitalSignsMonitor(final String url, final Logger logger){
        this.url = url;
        this.logger = logger;
    }

    @Override
    public Map<String, Integer> getPatientVitalSigns() {
        logger.writeOnLog("called vs monitor on " + url);
        try {
            final Pair<Integer, String> res = NetworkManager.doGET(url);

            if(res.first.equals(HttpsURLConnection.HTTP_OK)) {
                return decodeTTGatewayResponse(res.second);
            }
        } catch (IOException | JSONException e) {
            logger.writeOnLog("Unable to retrieve Vital Signs: " + e.getMessage());
        }

        return null;
    }

    private Map<String, Integer> decodeTTGatewayResponse(final String response) throws JSONException {
        final JSONArray values = new JSONArray(response);

        final  Map<String, Integer> valuesMap = new HashMap<>();

        for (int i = 0; i < values.length(); i++) {
            valuesMap.put(
                    values.getJSONObject(i).optString("type"),
                    values.getJSONObject(i).optInt("measure")
            );
        }

        return valuesMap;
    }
}
