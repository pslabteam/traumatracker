package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.tools.GlassBridgeManager;
import cartago.*;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class ExternalCamera extends JaCaArtifact {
	
	void init(){
		defineObsProperty("recording",false);
	}

	@OPERATION
	void snap(OpFeedbackParam<String> filename){
		log("snap!");

		try {
			String path = GlassBridgeManager.getInstance().sendTakePictureRequest("key001");
			log("Got from glass: "+path);
            filename.set(path);
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

	@OPERATION
	void startRecording(){
		getObsProperty("recording").updateValue(true);
	}

	@OPERATION
	void stopRecording(OpFeedbackParam<String> filename){
		getObsProperty("recording").updateValue(false);
        filename.set(""); //TODO
	}
		
}
