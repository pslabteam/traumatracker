package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerTimePicker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTime;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class UpdateEventInfoDialog extends TTDialog {

    private String selectedRoom;

    UpdateEventInfoDialog(Context context, final String eventDescription, final String eventCurrentDate, final String eventCurrentTime, final String eventCurrentPlace, final Listener listener) {
        super(context, R.layout.dialog_update_event_info);

        this.selectedRoom = eventCurrentPlace;

        intiUI(getString(R.string.update_date_time_dialog_title),
                eventDescription,
                DateTimeUtils.createDateTime(eventCurrentDate + " " + eventCurrentTime),
                listener);
    }

    private void intiUI(String title, final String eventDescription, DateTime currentDateTime, final Listener listener) {
        (((TextView) findViewById(R.id.title))).setText(title);
        (((TextView) findViewById(R.id.noUsersLabel))).setText(eventDescription);

        final DatePicker dp = findViewById(R.id.datePicker);
        dp.setMaxDate(System.currentTimeMillis());
        dp.updateDate(currentDateTime.year(), currentDateTime.month(), currentDateTime.day());

        final TraumaTrackerTimePicker tp = findViewById(R.id.timePicker);
        tp.setIs24HourView(true);
        tp.setCurrentHour(currentDateTime.hour());
        tp.setCurrentMinute(currentDateTime.minute());
        tp.setCurrentSecond(currentDateTime.second());

        initRoomsButtons();

        findViewById(R.id.yesButton).setOnClickListener(v -> {
            if (listener != null) {
               listener.onYesButtonClicked(DateTimeUtils.formatDate(dp.getYear(), dp.getMonth() + 1, dp.getDayOfMonth()),
                       DateTimeUtils.formatTime(tp.getCurrentHour(), tp.getCurrentMinute(), tp.getCurrentSeconds()),
                       selectedRoom);
            }

            dismiss();
        });

        findViewById(R.id.noButton).setOnClickListener(v -> dismiss());
    }

    private void initRoomsButtons(){
        try {
            List<String> rooms = FileSystem.getRoomsList();

            if(!rooms.isEmpty()){
                final List<Button> buttons = new ArrayList<>();

                for (String room : rooms) {
                    buttons.add(buildButton(room));
                }

                createButtonsGrid(buttons);
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private void createButtonsGrid(List<Button> buttons) {
        LinearLayout grid = findViewById(R.id.roomButtonsContainer);

        for (int j = 0; j <= buttons.size(); j += 3) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.weight = 1;
            params.gravity = Gravity.CENTER_VERTICAL;

            LinearLayout layout = new LinearLayout(getContext());
            layout.setLayoutParams(params);
            layout.setOrientation(LinearLayout.HORIZONTAL);


            if (j < buttons.size())
                layout.addView(buttons.get(j));

            if ((j + 1) < buttons.size())
                layout.addView(buttons.get(j + 1));

            if ((j + 2) < buttons.size())
                layout.addView(buttons.get(j + 2));

            grid.addView(layout);
        }
    }

    private Button buildButton(String room) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);

        params.weight = 0.5f;
        params.gravity = Gravity.CENTER_VERTICAL;
        params.setMargins(5, 5, 5, 5);

        Button btn = new Button(getContext());
        btn.setLayoutParams(params);
        btn.setTextSize(14);
        btn.setAllCaps(true);

        if(selectedRoom.equals(room)){
            btn.setBackgroundResource(R.drawable.traumatracker_button_bg_red);
        } else {
            btn.setBackgroundResource(R.drawable.traumatracker_button);
        }

        btn.setTag(room);
        btn.setText(room.replace(": ", "\n"));
        btn.setOnClickListener(v -> {
            v.setBackgroundResource(R.drawable.traumatracker_button_bg_red);

            LinearLayout grid = findViewById(R.id.roomButtonsContainer);
            View btnClicked = grid.findViewWithTag(selectedRoom);
            if(btnClicked != null){
                btnClicked.setBackgroundResource(R.drawable.traumatracker_button);
            }

            selectedRoom = v.getTag().toString();
        });

        return btn;
    }

    public interface Listener {
        void onYesButtonClicked(String newDate, String newTime, String newPlace);
    }
}
