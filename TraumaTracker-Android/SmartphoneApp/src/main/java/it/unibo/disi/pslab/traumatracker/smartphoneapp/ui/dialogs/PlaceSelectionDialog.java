package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.FileSystem;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.TimeSheet;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class PlaceSelectionDialog extends TTDialog {

    public PlaceSelectionDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_room_selection);

        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());

        List<String> rooms = new ArrayList<>();

        try {
            rooms = FileSystem.getRoomsList();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        if(rooms.isEmpty()){
            ((TextView) findViewById(R.id.title)).setText(context.getString(R.string.warning));
            findViewById(R.id.noUsersLabel).setVisibility(View.VISIBLE);
            findViewById(R.id.roomButtonsContainer).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.noUsersLabel)).setText(context.getString(R.string.rooms_list_unavailable));
            ((Button) findViewById(R.id.cancelButton)).setText(context.getString(R.string.ok));

            return;
        }

        final List<Button> buttons = new ArrayList<>();

        ((TextView) findViewById(R.id.title)).setText(context.getString(R.string.rooms_selection_dialog_title));
        findViewById(R.id.noUsersLabel).setVisibility(View.GONE);

        for (String room : rooms) {
            buttons.add(buildButton(room, listener));
        }

        createButtonsGrid(buttons);
    }

    private Button buildButton(String room, final Listener listener) {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dpi(100));
        params.weight = 0.5f;
        params.gravity = Gravity.CENTER_VERTICAL;
        params.setMargins(dpi(5), dpi(5), dpi(5), dpi(5));

        final Button btn = new Button(getContext());
        btn.setLayoutParams(params);
        btn.setPadding(dpi(10), dpi(10), dpi(10), dpi(10));
        btn.setTextSize(16);
        btn.setAllCaps(true);
        btn.setTypeface(Typeface.DEFAULT_BOLD);

        btn.setBackgroundResource(room.equals(TimeSheet.reference().getCurrentRoom().description())
                ? R.drawable.traumatracker_button_bg_red
                : R.drawable.traumatracker_button);

        btn.setTag(room);
        btn.setText(room.replace(": ", "\n"));
        btn.setOnClickListener(v -> {
            listener.onRoomSelected(v.getTag().toString());
            dismiss();
        });

        return btn;
    }

    private void createButtonsGrid(List<Button> buttons) {
        final LinearLayout grid = findViewById(R.id.roomButtonsContainer);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        params.gravity = Gravity.CENTER_VERTICAL;

        for (int j = 0; j <= buttons.size(); j += 3) {
            final LinearLayout layout = new LinearLayout(getContext());
            layout.setLayoutParams(params);
            layout.setOrientation(LinearLayout.HORIZONTAL);

            if (j < buttons.size()) {
                layout.addView(buttons.get(j));
            }

            if ((j + 1) < buttons.size()) {
                layout.addView(buttons.get(j + 1));
            }

            if ((j + 2) < buttons.size()) {
                layout.addView(buttons.get(j + 2));
            }

            grid.addView(layout);
        }
    }

    public interface Listener {
        void onRoomSelected(String room);
    }
}
