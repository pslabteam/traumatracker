package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.BloodProduct;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Dosage;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class BloodProductConfirmationDialog extends TTDialog {
    private EditText number;

    private String currentBagCode = "";

    private static BloodProductConfirmationDialog me;

    public BloodProductConfirmationDialog(final Context context, final BloodProduct bloodProduct, final BloodProductConfirmationDialog.Listener listener) {
        super(context, R.layout.dialog_bloodproduct_confirmation);

        me = this;

        initUI(bloodProduct, listener);

        if(bloodProduct.dosageType().equals(Dosage.NULL)){
            number.setVisibility(View.GONE);
            findViewById(R.id.unit).setVisibility(View.GONE);
        }

        if(!bloodProduct.hasBag()){
            findViewById(R.id.bag_code_container).setVisibility(View.GONE);
        }
    }

    private void initUI(final BloodProduct bloodProduct, final BloodProductConfirmationDialog.Listener listener) {
        ((TextView) findViewById(R.id.title)).setText(bloodProduct.toString().toUpperCase());

        number = findViewById(R.id.number);

        if(!bloodProduct.dosageType().equals(Dosage.NULL)){
            number.setText(String.valueOf(new DecimalFormat("0.#").format(bloodProduct.defaultValue())));
            ((EditText) findViewById(R.id.unit)).setText(bloodProduct.unit().toString());
        }

        initBagCodeEditText(findViewById(R.id.bag_code_text), currentBagCode, this::updateBloodBagCode);

        findViewById(R.id.readBagCodeButton).setOnClickListener(v -> listener.newBarcodeRequest());

        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.closeButton).setOnClickListener(v -> {
            if(bloodProduct.dosageType().equals(Dosage.NULL)){
                listener.onCloseButtonClicked();
            } else {
                double qty;

                try {
                    qty = Double.parseDouble(number.getText().toString());
                } catch (NullPointerException | NumberFormatException e) {
                    new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.value_error_message), null).show();
                    return;
                }

                if(qty == 0){
                    new MessageDialog(getContext(), getString(R.string.warning),
                            getString(R.string.value_error_message), null).show();
                    return;
                }

                if(!bloodProduct.hasBag()){
                    listener.onCloseButtonClicked(qty);
                } else {
                    listener.onCloseButtonClicked(qty, currentBagCode);
                }
            }

            dismiss();
        });
    }

    private void initBagCodeEditText(final EditText editText, final String currentValue, KeyboardDialog.Listener onConfirmation){
        editText.setOnClickListener(v -> new KeyboardDialog(getContext(), "Codice Sacca", currentValue, "", onConfirmation, new QuantityChecker() {
            int max_digits = 15;

            @Override
            public boolean checkInput(String code) {
                return code.length() <= max_digits;
            }

            @Override
            public String errorMessage() {
                return "Il codice deve essere di massimo " + max_digits + " cifre!";
            }
        }).setIntegerValue(true).setUnsignedValue(true).show());

        if(currentValue.equals("")){
            editText.setText("---");
        } else {
            editText.setText(currentValue);
        }
    }

    private void updateBloodBagCode(final String code) {
        if(code.equals("empty"))
            return;

        currentBagCode = code;
        ((EditText) findViewById(R.id.bag_code_text)).setText(code);
    }

    public static void updateRequestedBloodBagCode(final String code){
        if(code.equals("empty"))
            return;

        me.currentBagCode = code;


        ((EditText) me.findViewById(R.id.bag_code_text)).setText(code);
    }

    public interface Listener {
        void newBarcodeRequest();
        void onCloseButtonClicked();
        void onCloseButtonClicked(double value);
        void onCloseButtonClicked(double value, String code);
    }
}
