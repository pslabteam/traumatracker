package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.BarcodeReader;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.ISS;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.FinalDestination;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.TraumaInfoElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class TraumaTerminationDialog extends TTDialog {

    private String patientSdo = "";
    private ISS currentISS = null;

    private Map<FinalDestination, Integer> destinations = new HashMap<FinalDestination, Integer>() {{
        put(FinalDestination.EMERGENCY_ROOM, R.id.destination_ordinary_ward);
        put(FinalDestination.INTENSIVE_CARE_UNIT, R.id.destination_intensive_care_unit);
        put(FinalDestination.EMERGENCY_MEDICINE, R.id.destination_emergency_medicine);
        put(FinalDestination.OTHER, R.id.destination_other);
    }};

    private FinalDestination selectedDestination;

    public TraumaTerminationDialog(final Context context, final Listener listener) {
        super(context, R.layout.dialog_trauma_termination);

        initUI(listener);
    }

    private void initUI(final Listener listener) {

        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.exitNoSavingButton).setOnClickListener(v -> new MessageDialogWithChoice(getContext(), getString(R.string.warning), getString(R.string.unsave_exit_message), new MessageDialogWithChoice.Listener() {
            @Override
            public void onNoButtonClicked() {
                //nothing to do!
            }

            @Override
            public void onYesButtonClicked() {
                listener.onExitWithoutSaving();
                dismiss();
            }
        }).show());

        findViewById(R.id.exitSavingButton).setOnClickListener(v -> {
            if (selectedDestination == null || currentISS == null) {
                new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.trauma_termination_error_message), null).show();
                return;
            }

            String destinationDetails = "";

            switch(selectedDestination){
                case EMERGENCY_ROOM:
                    //Nothing else to do!
                    break;

                case INTENSIVE_CARE_UNIT:
                    if(patientSdo.isEmpty()){
                        new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.destination_sdo_absent_warning_msg), null).show();
                        return;
                    } else {
                        ActiveTrauma.updatePatientInfo(TraumaInfoElement.SDO, patientSdo);
                    }
                    break;

                case EMERGENCY_MEDICINE:
                    //Nothing else to do!
                    break;

                case OTHER:
                    String otherDestinationDescription = ((EditText) findViewById(R.id.otherDestinationEditText)).getText().toString();

                    if (otherDestinationDescription.trim().isEmpty()) {
                        new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.other_destination_error_message), null).show();
                        return;
                    } else {
                        destinationDetails = " (" + otherDestinationDescription.trim() + ")";
                    }
                    break;
            }

            currentISS.setIsPresumed(((CheckBox) findViewById(R.id.cb_iss_presumed)).isChecked());

            listener.onExitWithSaving(selectedDestination.toString() + destinationDetails, currentISS);
            dismiss();

            ActiveTrauma.notifyTraumaCurrentStatus("done");
        });

        findViewById(R.id.iss_button).setOnClickListener(v -> new IssDialog(getContext(), currentISS, iss -> {
            currentISS = iss;

            if(!iss.getTotalISS().isEmpty()){
                ((TextView) findViewById(R.id.iss_total_text)).setText(iss.getTotalISS());
            }
        }).showFullscreen());

        for (FinalDestination fd : destinations.keySet()) {
            RadioButton button = findViewById(destinations.get(fd));
            button.setText(fd.toString().toUpperCase());
            button.setTag(fd);
        }

        ((RadioGroup) findViewById(R.id.destination_selector)).setOnCheckedChangeListener((radioGroup, i) -> {
            findViewById(R.id.sdo_container).setVisibility(View.GONE);
            findViewById(R.id.other_destination_container).setVisibility(View.GONE);
            hideKeyboard(R.id.otherDestinationEditText);

            if(findViewById(i).getTag().equals(FinalDestination.INTENSIVE_CARE_UNIT)){

                final String sdo = ActiveTrauma.getTraumaInfo(TraumaInfoElement.SDO);

                if(sdo.isEmpty()){
                    findViewById(R.id.sdo_container).setVisibility(View.VISIBLE);
                } else {
                    patientSdo = sdo;
                }

            }

            if (findViewById(i).getTag().equals(FinalDestination.OTHER)) {
                findViewById(R.id.other_destination_container).setVisibility(View.VISIBLE);
            }

            selectedDestination = (FinalDestination) findViewById(i).getTag();
        });

        initIdCode(getString(R.string.sdo_code_label), findViewById(R.id.patient_sdo_text), patientSdo, this::updatePatientSdo);
        findViewById(R.id.readSdoButton).setOnClickListener(v -> listener.newBarcodeRequest(BarcodeReader.Requests.PATIENT_SDO_BY_DESTINATION));

        findViewById(R.id.sdo_container).setVisibility(View.GONE);
        findViewById(R.id.other_destination_container).setVisibility(View.GONE);
    }

    private void initIdCode(final String description, final EditText editText, final String currentValue, KeyboardDialog.Listener onConfirmation){
        editText.setOnClickListener(v -> new KeyboardDialog(getContext(), description, currentValue, "", onConfirmation, new QuantityChecker() {
            int max_digits = 10;

            @Override
            public boolean checkInput(String code) {
                return code.length() <= max_digits;
            }

            @Override
            public String errorMessage() {
                return "Il codice deve essere di massimo " + max_digits + " cifre!";
            }
        }).setIntegerValue(true).setUnsignedValue(true).show());

        if(currentValue.equals("")){
            editText.setText("---");
        } else {
            editText.setText(currentValue);
        }
    }

    public void updatePatientSdo(String sdo) {
        if(sdo.equals("empty"))
            return;

        patientSdo = sdo;
        ((EditText) findViewById(R.id.patient_sdo_text)).setText(sdo);
    }

    public interface Listener {
        void newBarcodeRequest(String type);
        void onExitWithSaving(String dest, ISS iss);
        void onExitWithoutSaving();
    }
}
