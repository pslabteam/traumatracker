package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.templates;

import android.support.v4.app.Fragment;
import android.view.View;

public abstract class TraumaTrackerFragment extends Fragment {

    protected View mainView;

    protected abstract void initUI();
    protected abstract void updateUI();
}
