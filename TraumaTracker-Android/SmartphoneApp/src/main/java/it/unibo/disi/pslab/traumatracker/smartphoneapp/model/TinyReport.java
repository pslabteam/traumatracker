package it.unibo.disi.pslab.traumatracker.smartphoneapp.model;

import org.json.JSONException;
import org.json.JSONObject;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class TinyReport {
    private String id, operatorId, startDate, startTime;

    public TinyReport(String id, String operatorId, String startDate, String startTime){
        this.id = id;
        this.operatorId = operatorId;
        this.startDate = startDate;
        this.startTime = startTime;
    }

    public TinyReport(final String reportContent){
        try {
            final JSONObject obj = new JSONObject(reportContent);

            this.id = obj.optString(C.report.ID);
            this.operatorId = obj.optString(C.report.START_OPERATOR_ID);
            this.startDate = obj.optString(C.report.START_DATE);
            this.startTime = obj.optString(C.report.START_TIME);
        } catch (JSONException ignored) {

        }
    }

    public String id(){
        return id;
    }

    public String operatorId(){
        return operatorId;
    }

    public String startDate(){
        return startDate;
    }

    public String startTime(){
        return startTime;
    }
}
