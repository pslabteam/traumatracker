package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.DrugCategory;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.DrugConfirmationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.templates.TraumaTrackerButtonsFragment;

public class DrugsFragment extends TraumaTrackerButtonsFragment {
    private static final int COLUMNS = 3;

    private List<DrugCategory> toSkipList = Collections.singletonList(DrugCategory.CONTINUOUS_INFUSION_DRUGS);

    public DrugsFragment() {}

    private static View mainView;

    public static DrugsFragment newInstance() {
        return new DrugsFragment();
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_drugs, container, false);

        initUI(mainView);

        toggleAlsDrugs(ActiveTrauma.isAlsActive());

        return mainView;
    }

    public static void toggleAlsDrugs(final boolean active){
        mainView.findViewWithTag(DrugCategory.ALS_DRUGS).setVisibility(active? View.VISIBLE : View.GONE);
    }

    private void initUI(View mainView){
        Map<DrugCategory, List<Button>> drugsButtonsMap = new TreeMap<>(new DrugCategory.DrugCategoryComparator());

        for (DrugCategory category : DrugCategory.values()) {

            if(toSkipList.contains(category)){
                continue;
            }

            List<Button> currentCategoryButtons = new ArrayList<>();

            for (int i = 0; i < Drug.values().length; i++) {
                if (Drug.values()[i].category().equals(category)) {
                    final Drug d = Drug.values()[i];

                    final int bgRes;

                    switch (d.category()) {
                        case INFUSIONS:
                            bgRes = R.drawable.traumatracker_button_bg_blue;
                            break;

                        case GENERAL_DRUGS:
                        case ALS_DRUGS:
                            bgRes = R.drawable.traumatracker_button_bg_yellow;
                            break;

                        case ANTIBIOTIC_PROPHYLAXIS:
                            bgRes = R.drawable.traumatracker_button_bg_green;
                            break;

                        default:
                            bgRes = R.drawable.traumatracker_button;
                            break;
                    }

                    currentCategoryButtons.add(buildButton(getContext(), d.toString(), bgRes, v -> {
                        if(!ActiveTrauma.isPatientAcceptedInER()){
                            new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.patient_not_arrived_alert_message), null).show();
                            return;
                        }

                        new DrugConfirmationDialog(getActivity(), d, new DrugConfirmationDialog.Listener() {
                            @Override
                            public void onCloseButtonClicked() {
                                ActiveTrauma.registerNewDrug(d);
                            }

                            @Override
                            public void onCloseButtonClicked(double qty) {
                                ActiveTrauma.registerNewDrug(d, qty);
                            }
                        }).show();
                    }));
                }
            }

            drugsButtonsMap.put(category, currentCategoryButtons);
        }

        buildButtonsGrid(mainView, drugsButtonsMap, COLUMNS);
    }
}