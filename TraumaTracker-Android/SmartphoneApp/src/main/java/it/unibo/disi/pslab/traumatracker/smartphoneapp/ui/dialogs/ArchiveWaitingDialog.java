package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class ArchiveWaitingDialog extends TTDialog {

    private Button btn;
    private TextView messageTextView;
    private ProgressBar progressBar;

    public ArchiveWaitingDialog(Context context, String title, String message, final Listener listener) {
        super(context, R.layout.dialog_archive_waiting);

        intiUI(title, message, listener);
    }

    private void intiUI(String title, String message, final Listener listener) {
        ((TextView) findViewById(R.id.title)).setText(title);

        messageTextView = findViewById(R.id.noUsersLabel);
        messageTextView.setText(message);

        btn = findViewById(R.id.closeButton);
        btn.setOnClickListener(v -> {
            if (listener != null) {
                listener.onCloseButtonClicked();
            }

            dismiss();
        });

        progressBar = findViewById(R.id.progressBar);
    }

    public void toggleButtonVisibility(boolean visible) {
        if (visible) {
            btn.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        } else {
            btn.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public void changeMessage(String newMessage) {
        messageTextView.setText(newMessage);
    }

    public interface Listener {
        void onCloseButtonClicked();
    }
}
