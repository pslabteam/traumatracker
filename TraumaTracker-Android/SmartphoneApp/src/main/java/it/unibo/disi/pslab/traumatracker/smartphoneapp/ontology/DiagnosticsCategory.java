package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import java.util.Comparator;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum DiagnosticsCategory {
    INSTRUMENTAL("instrumental", R.string.ontology_diagnostics_category_instrumental, 1),
    LABORATORY("laboratory", R.string.ontology_diagnostics_category_laboratory, 2);

    private final int labelRes;
    private final int position;

    DiagnosticsCategory(@SuppressWarnings("unused") final String id, final int labelRes, final int position) {
        this.labelRes = labelRes;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }

    public static class DiagnosticCategoryComparator implements Comparator<DiagnosticsCategory> {
        public int compare(DiagnosticsCategory c1, DiagnosticsCategory c2) {
            return Integer.compare(c1.getPosition(), c2.getPosition());
        }
    }
}
