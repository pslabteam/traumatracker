package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.content.*;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;
import it.unibo.pslab.jaca_android.core.JaCaApplication;

public class App extends JaCaApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        checkHomeDirectory();

        SharedPreferences preferences = getSharedPreferences(Preferences.ID, android.content.Context.MODE_PRIVATE);
        
        if(preferences.getBoolean(Preferences.LOGGING, C.LOG_ACTIVE)){
            try {
                File logDir = new File(Environment.getExternalStorageDirectory() + C.fs.LOG_FOLDER);
                String logFileName = C.fs.LOG_FILE_NAME_PREFIX + System.currentTimeMillis() + C.fs.LOG_FILE_EXTENSION;

                activateLogOnFileMode(logDir, logFileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkHomeDirectory() {
        String[] dirs = new String[]{
                C.fs.TRAUMA_TRACKER_HOME_DIR,
                C.fs.CONFIG_FOLDER,
                C.fs.LOG_FOLDER,
                C.fs.TEMP_FOLDER,
                C.fs.REPORTS_FOLDER,
                C.fs.REPORTS_SYNC_FOLDER,
                C.fs.REPORTS_TOSYNC_FOLDER,
                C.fs.PICTURES_FOLDER,
                C.fs.VIDEOS_FOLDER,
                C.fs.VOCAL_NOTES_FOLDER
        };

        for (String path : dirs) {
            File dir = new File(Environment.getExternalStorageDirectory() + path);
            if (!dir.exists()) {
                dir.mkdir();
            }
        }
    }
}
