package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum AnamnesiElement {
    ANTIPLATELETS("antiplatelets", R.string.ontology_anamnesi_antiplatelets),
    ANTICOAGULANTS("anticoagulants", R.string.ontology_anamnesi_anticoagulants),
    NAO("nao", R.string.ontology_anamnesi_nao);

    private final int labelRes;

    AnamnesiElement(@SuppressWarnings("unused")  final String id, final int labelRes){
        this.labelRes = labelRes;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }
}
