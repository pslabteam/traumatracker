package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.text.Html;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.ISS;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class IssDialog extends TTDialog {

    private ISS issTable;

    IssDialog(Context context, ISS previousISS, final Listener listener) {
        super(context, R.layout.dialog_iss);

        issTable = previousISS != null ? previousISS : new ISS();

        intiUI(listener);
    }

    private void intiUI(final Listener listener) {
        findViewById(R.id.exitLabel).setOnClickListener(v -> {
            listener.onDialogClosed(issTable);
            dismiss();
        });

        ((TextView) findViewById(R.id.ais_legend_text)).setText(Html.fromHtml(getString(R.string.ais_legend_text)));

        initSwitches();
    }

    private void initSwitches() {
        final Map<ISS.Element, Integer> aisSwitches = new HashMap<ISS.Element, Integer>(){{
            put(ISS.Element.EXTERNA, R.id.iss_externa_ais_switch);
            put(ISS.Element.HEAD_NECK, R.id.iss_head_neck_ais_switch);
            put(ISS.Element.BRAIN, R.id.iss_brain_ais_switch);
            put(ISS.Element.CERVICAL_SPINE, R.id.iss_cervical_spine_ais_switch);
            put(ISS.Element.FACE, R.id.iss_face_ais_switch);
            put(ISS.Element.TORAX, R.id.iss_torax_ais_switch);
            put(ISS.Element.SPINE, R.id.iss_spine_ais_switch);
            put(ISS.Element.ABDOMEN, R.id.iss_abdomen_ais_switch);
            put(ISS.Element.LUMBAR_SPINE, R.id.iss_lumbar_spine_ais_switch);
            put(ISS.Element.PELVIC_GIRDLE, R.id.iss_pelvic_girdle_ais_switch);
            put(ISS.Element.UPPER_EXTREMITIES, R.id.iss_upper_extremities_ais_switch);
            put(ISS.Element.LOWER_EXTREMITIES, R.id.iss_lower_extremities_ais_switch);
        }};

        for(ISS.Element e : ISS.Element.values()){
            RadioGroup currentSwitch = findViewById(aisSwitches.get(e));

            currentSwitch.setOnCheckedChangeListener((group, checkedId) -> {
                issTable.setElementAIS(e, findViewById(checkedId).getTag().toString());
                updateGroupISS(e.getGroup());
                updateTotalISS();
            });

            if(!issTable.getElementAIS(e).isEmpty()){
                currentSwitch.check(currentSwitch.findViewWithTag(issTable.getElementAIS(e)).getId());
                updateGroupISS(e.getGroup());
                updateTotalISS();
            }
        }
    }

    private void updateGroupISS(ISS.Group g){
        switch (g){
            case EXTERNA:
                ((TextView) findViewById(R.id.iss_externa)).setText(issTable.getGroupISS(g));
                break;
            case HEAD:
                ((TextView) findViewById(R.id.iss_head_neck)).setText(issTable.getGroupISS(g));
                break;
            case TORAX:
                ((TextView) findViewById(R.id.iss_torax)).setText(issTable.getGroupISS(g));
                break;
            case ABDOMEN:
                ((TextView) findViewById(R.id.iss_abdomen)).setText(issTable.getGroupISS(g));
                break;
            case PELVIC_GIRDLE:
                ((TextView) findViewById(R.id.iss_pelvic_girdle)).setText(issTable.getGroupISS(g));
                break;
            case EXTREMITIES:
                ((TextView) findViewById(R.id.iss_extremities)).setText(issTable.getGroupISS(g));
                break;
        }
    }

    private void updateTotalISS() {
        ((TextView) findViewById(R.id.iss_total_text)).setText(issTable.getTotalISS());
    }

    public interface Listener {
        void onDialogClosed(ISS iss);
    }
}
