package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.ReportsAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;

public class ReportsArchiveDialog extends TTDialog {

    private ReportsAdapter adapter;

    private List<String> reportsToSync;

    public ReportsArchiveDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_reports_archive);

        adapter = new ReportsAdapter(context, getReportsList());
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();

                updateLocalReportsStatus();
            }
        });

        intiUI(listener);
    }

    private void intiUI(final Listener listener) {
        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());

        ((ListView) findViewById(R.id.reports_list)).setAdapter(adapter);

        updateLocalReportsStatus();
        updateTempReportStatus();

        findViewById(R.id.syncButton).setOnClickListener(v -> {
            listener.onSendReportsToServerPressed(reportsToSync);
            dismiss();
        });

        findViewById(R.id.tempImportButton).setOnClickListener(v -> {
            FileSystemUtils.moveFolderContent(C.fs.TEMP_FOLDER, C.fs.REPORTS_TOSYNC_FOLDER);
            new MessageDialog(getContext(), getString(R.string.success), "Tutti i report temporanei sono stati spostati nell'archivio!", this::dismiss).show();
        });

        findViewById(R.id.tempCancelButton).setOnClickListener(v -> {
            final MessageDialogWithChoice confDialog = new MessageDialogWithChoice(getContext(), getString(R.string.warning), "Vuoi veramente eliminare tutti i report temporanei?", new MessageDialogWithChoice.Listener() {
                @Override
                public void onNoButtonClicked() {
                    //nothing to do!
                }

                @Override
                public void onYesButtonClicked() {
                    FileSystemUtils.emptyFolder(C.fs.TEMP_FOLDER);
                    new MessageDialog(getContext(), getString(R.string.success), "Tutti i report temporanei sono stati cancellati!", null).show();
                    findViewById(R.id.tempFormContainer).setVisibility(View.GONE);
                }
            });

            confDialog.show();
        });
    }

    private void updateTempReportStatus(){
        List<Report> list = getReportsFromFolder(C.fs.TEMP_FOLDER, false);

        if(list.isEmpty()){
            findViewById(R.id.tempFormContainer).setVisibility(View.GONE);
        } else {
            ((TextView) findViewById(R.id.nTempReports)).setText(String.format("%d", list.size()));
        }
    }

    private void updateLocalReportsStatus(){
        if(adapter.getCount() == 0){
            //NO REPORTS
            findViewById(R.id.noUsersLabel).setVisibility(View.VISIBLE);
            findViewById(R.id.syncFormContainer).setVisibility(View.GONE);
            return;
        }

        reportsToSync = new ArrayList<>();

        findViewById(R.id.noUsersLabel).setVisibility(View.GONE);

        for (int i = 0; i < adapter.getCount(); i++) {
            Report r = adapter.getItem(i);
            if (r != null && !r.isSync()) {
                reportsToSync.add(r.id());
            }
        }

        if(reportsToSync.size() == 0){
            findViewById(R.id.syncFormContainer).setVisibility(View.GONE);
        } else {
            ((TextView) findViewById(R.id.nReportsToSync)).setText(String.format("%d", reportsToSync.size()));
        }
    }

    private List<Report> getReportsList(){
        List<Report> reports = new ArrayList<>();
        reports.addAll(getReportsFromFolder(C.fs.REPORTS_SYNC_FOLDER, true));
        reports.addAll(getReportsFromFolder(C.fs.REPORTS_TOSYNC_FOLDER, false));

        Collections.sort(reports, (r1, r2) -> (r1.startDate() + "-" + r1.startTime()).compareTo(r2.startDate() + "-" + r2.startTime()));

        Collections.reverse(reports);

        return reports;
    }

    private List<Report> getReportsFromFolder(String folder, boolean sync) {
        List<Report> reports = new ArrayList<>();

        File[] files = FileSystemUtils.getFilesFromDirectory(folder);

        for (File file : files) {
            final String fileContent = FileSystemUtils.loadFile(folder, file.getName());

            if(!fileContent.isEmpty()){
                Report currentReport = Report.build(fileContent, sync);
                if(currentReport != null){
                    reports.add(currentReport);
                }
            }

        }

        return reports;
    }

    public interface Listener{
        void onSendReportsToServerPressed(List<String> reportsId);
    }
}
