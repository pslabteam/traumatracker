package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.BarcodeReader;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.AnamnesiElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.MajorTraumaCriteriaElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.PrehElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.TraumaInfoElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerEditText;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTime;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class TraumaInfoDialog extends TTDialog {

    private final Map<TraumaInfoElement, String> infoMap = new HashMap<>();
    private final Map<AnamnesiElement, Boolean> anamnesiMap = new HashMap<>();
    private final Map<MajorTraumaCriteriaElement, Boolean> mtcMap = new HashMap<>();

    private DateTime currentAccidentDateTime = new DateTime();

    public TraumaInfoDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_trauma_info);

        intiUI(listener);
        initTabs();
    }

    private void intiUI(final Listener listener) {
        findViewById(R.id.exitLabel).setOnClickListener(v -> {
            savePatientInfo();
            dismiss();
            ActiveTrauma.notifyTraumaInfo();
            ActiveTrauma.notifyPreH();
        });

        findViewById(R.id.downloadPreHDataLabel).setOnClickListener(v -> {
            listener.retrievePrehMissions();
        });

        for(TraumaInfoElement pe : TraumaInfoElement.values()){
            infoMap.put(pe, ActiveTrauma.getTraumaInfo(pe));
        }

        for(MajorTraumaCriteriaElement mtce : MajorTraumaCriteriaElement.values()){
            mtcMap.put(mtce, ActiveTrauma.getMajorTraumaCriteriaElement(mtce));
        }

        for(AnamnesiElement ae : AnamnesiElement.values()){
            anamnesiMap.put(ae, ActiveTrauma.getAnamnesiElement(ae));
        }

        initRegistryUI(listener);
        initAccidentDetails();

        initPreH_A();
        initPreH_B();
        initPreH_C();
        initPreH_D();
        initPreH_E();
        initPreH_Others();
    }

    private void initRegistryUI(final Listener listener) {
        initIdCode(getString(R.string.er_code_label), findViewById(R.id.patient_code_text), Objects.requireNonNull(infoMap.get(TraumaInfoElement.CODE)), this::updatePatientCode);
        initIdCode(getString(R.string.sdo_code_label), findViewById(R.id.patient_sdo_text), Objects.requireNonNull(infoMap.get(TraumaInfoElement.SDO)), this::updatePatientSdo);

        findViewById(R.id.readBagCodeButton).setOnClickListener(v -> listener.newBarcodeRequest(BarcodeReader.Requests.PATIENT_CODE));
        findViewById(R.id.readSdoButton).setOnClickListener(v -> listener.newBarcodeRequest(BarcodeReader.Requests.PATIENT_SDO));

        RadioGroup admissionCodeSwitch = findViewById(R.id.er_code_switch);
        admissionCodeSwitch.setOnCheckedChangeListener((radioGroup, i) -> infoMap.put(TraumaInfoElement.ADMISSION_CODE, ((RadioButton) findViewById(i)).getText().toString()));

        if (Objects.equals(infoMap.get(TraumaInfoElement.ADMISSION_CODE), getString(R.string.er_admission_code_red_text))) {
            admissionCodeSwitch.check(R.id.er_code_switch_red);
        } else if (Objects.equals(infoMap.get(TraumaInfoElement.ADMISSION_CODE), getString(R.string.er_admission_code_yellow_text))) {
            admissionCodeSwitch.check(R.id.er_code_switch_yellow);
        }

        ((EditText) findViewById(R.id.nameEditText)).setText(infoMap.get(TraumaInfoElement.NAME));
        ((EditText) findViewById(R.id.surnameEditText)).setText(infoMap.get(TraumaInfoElement.SURNAME));

        RadioGroup genderSwitch = findViewById(R.id.gender_switch);
        genderSwitch.setOnCheckedChangeListener((radioGroup, i) -> infoMap.put(TraumaInfoElement.GENDER, ((RadioButton) findViewById(i)).getText().toString()));

        if (Objects.equals(infoMap.get(TraumaInfoElement.GENDER), getString(R.string.gender_male_label))) {
            genderSwitch.check(R.id.gender_switch_m);
        } else if (Objects.equals(infoMap.get(TraumaInfoElement.GENDER), getString(R.string.gender_female_label))) {
            genderSwitch.check(R.id.gender_switch_f);
        }

        ((EditText) findViewById(R.id.dobEditText)).setText(infoMap.get(TraumaInfoElement.DOB));

        TraumaTrackerEditText ageEditText = findViewById(R.id.ageEditText);
        ageEditText.setText(infoMap.get(TraumaInfoElement.AGE));
        ageEditText.setOnClickListener(getContext(), getString(R.string.age_label_text), "", new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                double qty = Double.parseDouble(input);
                return qty >= 0 && (qty % 1 == 0);
            }

            @Override
            public String errorMessage() {
                return getContext().getString(R.string.integer_positive_error_message);
            }
        });

        initAnamnesiCheckbox(R.id.anamnesi_cb_1, AnamnesiElement.ANTIPLATELETS);
        initAnamnesiCheckbox(R.id.anamnesi_cb_2, AnamnesiElement.ANTICOAGULANTS);
        initAnamnesiCheckbox(R.id.anamnesi_cb_3, AnamnesiElement.NAO);
    }

    private void initIdCode(final String description, final EditText editText, final String currentValue, KeyboardDialog.Listener onConfirmation){
        editText.setOnClickListener(v -> new KeyboardDialog(getContext(), description, currentValue, "", onConfirmation, new QuantityChecker() {

            @Override
            public boolean checkInput(String code) {
                return code.length() <= C.PATIENT_CODE_MAX_DIGITS;
            }

            @Override
            public String errorMessage() {
                return "Il codice deve essere di massimo " + C.PATIENT_CODE_MAX_DIGITS + " cifre!";
            }
        }).setIntegerValue(true).setUnsignedValue(true).show());

        if(currentValue.equals("")){
            editText.setText("---");
        } else {
            editText.setText(currentValue);
        }
    }

    private void initAccidentDetails() {
        /*
         * ---------- Accident Date/Time
         */
        EditText accidentDateTimeEditText = findViewById(R.id.accidentMomentEditText);
        accidentDateTimeEditText.setOnClickListener(v -> new MessageDialogDateTime(getContext(), "Data e Ora dell'incidente", currentAccidentDateTime, new MessageDialogDateTime.Listener() {
            @Override
            public void onNoButtonClicked() {
                //nothing to do!
            }

            @Override
            public void onYesButtonClicked(DateTime dt) {
                currentAccidentDateTime = dt;
                ((EditText) findViewById(R.id.accidentMomentEditText)).setText(dt.toString());
            }
        }).show());

        if (!infoMap.get(TraumaInfoElement.ACCIDENT_DATE).isEmpty()) {
            currentAccidentDateTime = DateTimeUtils.createDateTime(infoMap.get(TraumaInfoElement.ACCIDENT_DATE) + " " + infoMap.get(TraumaInfoElement.ACCIDENT_TIME));
            accidentDateTimeEditText.setText(currentAccidentDateTime.toString());
        }

        final RadioGroup accidentType = findViewById(R.id.accident_type_switch);

        accidentType.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = findViewById(checkedId);
            if (rb.getText().equals(getString(R.string.switch_accident_closed_label))) {
                infoMap.put(TraumaInfoElement.ACCIDENT_TYPE, getString(R.string.switch_accident_closed_label).toLowerCase());
            } else {
                infoMap.put(TraumaInfoElement.ACCIDENT_TYPE, getString(R.string.switch_accident_penetrating_label).toLowerCase());
            }
        });

        if (infoMap.get(TraumaInfoElement.ACCIDENT_TYPE).equals(getString(R.string.switch_accident_closed_label).toLowerCase())) {
            accidentType.check(R.id.accident_type_switch_closed);
        } else if (infoMap.get(TraumaInfoElement.ACCIDENT_TYPE).equals(getString(R.string.switch_accident_penetrating_label).toLowerCase())) {
            accidentType.check(R.id.accident_type_switch_penetrating);
        }

        /*
         * ------- Major Trauma Criteria
         */
        initMtcCheckbox(R.id.mtc_dynamic, MajorTraumaCriteriaElement.DYNAMIC);
        initMtcCheckbox(R.id.mtc_physiological, MajorTraumaCriteriaElement.PHYSIOLOGICAL);
        initMtcCheckbox(R.id.mtc_anatomical, MajorTraumaCriteriaElement.ANATOMICAL);

        /*
         * ------- Vehicle
         */

        final List<Pair<Integer, Integer>> vehicles = new ArrayList<Pair<Integer, Integer>>(){{
            add(new Pair<>(R.string.vehicle_elicopter_label, R.id.vehicle_switch_helicopter));
            add(new Pair<>(R.string.vehicle_ambulance_label, R.id.vehicle_switch_ambulance));
            add(new Pair<>(R.string.vehicle_medicalcar_label, R.id.vehicle_switch_medicalcar));
        }};


        ((RadioGroup) findViewById(R.id.vehicle_switch)).setOnCheckedChangeListener((radioGroup, i) -> {
            for (Pair<Integer, Integer> vehicle : vehicles){
                if (((RadioButton) findViewById(i)).getText().equals(getString(vehicle.first))) {
                    infoMap.put(TraumaInfoElement.VEHICLE, getString(vehicle.first));
                }
            }
        });

        for (int i = 0; i < vehicles.size(); i++) {
            if (infoMap.get(TraumaInfoElement.VEHICLE).equals(getString(vehicles.get(i).first))) {
                ((RadioGroup) findViewById(R.id.vehicle_switch)).check(vehicles.get(i).second);
            }
        }

        initFromOtherEmergency();
    }

    private void initFromOtherEmergency(){
        final CheckBox fromOtherEmergencyCheckbox = findViewById(R.id.from_other_emergency_cb);
        final RadioGroup otherEmergency1stLvlSwitch = findViewById(R.id.other_emergency_1st_level_switch);
        final RadioGroup otherEmergency2ndLvlSwitch = findViewById(R.id.other_emergency_2nd_level_switch);
        final LinearLayout otherEmergency3rdLvlContainer = findViewById(R.id.other_emergency_3rd_level_container);
        final EditText otherEmergency3rdLvlText = findViewById(R.id.other_emergency_3rd_level_text);

        final int[] places1stLevelLables = new int[]{R.string.place_ravenna, R.string.place_rimini, R.string.place_forli};
        final int[] places2ndLevelLables = new int[]{R.string.place_cattolica, R.string.place_faenza, R.string.place_lugo, R.string.place_riccione};

        final int[] places1stLevelSwitches = new int[]{R.id.other_emergency_1st_level_switch_ravenna, R.id.other_emergency_1st_level_switch_rimini, R.id.other_emergency_1st_level_switch_forli, R.id.other_emergency_1st_level_switch_more};
        final int[] places2ndLevelSwitches = new int[]{R.id.other_emergency_2nd_level_switch_cattolica, R.id.other_emergency_2nd_level_switch_faenza, R.id.other_emergency_2nd_level_switch_lugo, R.id.other_emergency_2nd_level_switch_riccione, R.id.other_emergency_2nd_level_switch_more};

        fromOtherEmergencyCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                infoMap.put(TraumaInfoElement.FROM_OTHER_EMERGENCY, getString(R.string.switch_yes_label).toLowerCase());
                otherEmergency1stLvlSwitch.setVisibility(View.VISIBLE);
            } else {
                infoMap.put(TraumaInfoElement.FROM_OTHER_EMERGENCY, getString(R.string.switch_no_label).toLowerCase());
                infoMap.put(TraumaInfoElement.OTHER_EMERGENCY, "");

                otherEmergency1stLvlSwitch.clearCheck();
                otherEmergency2ndLvlSwitch.clearCheck();
                otherEmergency3rdLvlText.setText("");

                otherEmergency1stLvlSwitch.setVisibility(View.GONE);
                otherEmergency2ndLvlSwitch.setVisibility(View.GONE);
                otherEmergency3rdLvlContainer.setVisibility(View.GONE);
            }
        });

       /* fromOtherEmergencySwitch.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = findViewById(checkedId);
            if (rb.getText().equals(getString(R.string.switch_yes_label))) {
                infoMap.put(TraumaInfoElement.FROM_OTHER_EMERGENCY, getString(R.string.switch_yes_label).toLowerCase());
                otherEmergency1stLvlSwitch.setVisibility(View.VISIBLE);
            } else {
                infoMap.put(TraumaInfoElement.FROM_OTHER_EMERGENCY, getString(R.string.switch_no_label).toLowerCase());
                infoMap.put(TraumaInfoElement.OTHER_EMERGENCY, "");

                otherEmergency1stLvlSwitch.clearCheck();
                otherEmergency2ndLvlSwitch.clearCheck();
                otherEmergency3rdLvlText.setText("");

                otherEmergency1stLvlSwitch.setVisibility(View.GONE);
                otherEmergency2ndLvlSwitch.setVisibility(View.GONE);
                otherEmergency3rdLvlContainer.setVisibility(View.GONE);
            }
        });*/

        otherEmergency1stLvlSwitch.setOnCheckedChangeListener((group, checkedId) -> {
            if(findViewById(checkedId) == null) {
                return;
            }

            if(((RadioButton) findViewById(checkedId)).getText().equals(getString(R.string.place_other))){
                otherEmergency2ndLvlSwitch.setVisibility(View.VISIBLE);
            } else {
                infoMap.put(TraumaInfoElement.OTHER_EMERGENCY, ((RadioButton) findViewById(checkedId)).getText().toString());

                otherEmergency2ndLvlSwitch.setVisibility(View.GONE);
                otherEmergency3rdLvlContainer.setVisibility(View.GONE);
                otherEmergency3rdLvlText.setText("");
            }
        });
        otherEmergency1stLvlSwitch.setVisibility(View.GONE);

        otherEmergency2ndLvlSwitch.setOnCheckedChangeListener((group, checkedId) -> {
            if(findViewById(checkedId) == null) {
                return;
            }

            if(((RadioButton) findViewById(checkedId)).getText().equals(getString(R.string.place_other))){
                otherEmergency3rdLvlContainer.setVisibility(View.VISIBLE);
            } else {
                infoMap.put(TraumaInfoElement.OTHER_EMERGENCY, ((RadioButton) findViewById(checkedId)).getText().toString());

                otherEmergency3rdLvlContainer.setVisibility(View.GONE);
                otherEmergency3rdLvlText.setText("");
            }
        });
        otherEmergency2ndLvlSwitch.setVisibility(View.GONE);

        otherEmergency3rdLvlText.setText("");
        otherEmergency3rdLvlContainer.setVisibility(View.GONE);

        if (infoMap.get(TraumaInfoElement.FROM_OTHER_EMERGENCY).equals(getString(R.string.switch_yes_label).toLowerCase())) {
            //fromOtherEmergencySwitch.check(R.id.other_emergency_switch_yes);
            fromOtherEmergencyCheckbox.setChecked(true);

            String otherEmergency = infoMap.get(TraumaInfoElement.OTHER_EMERGENCY);

            if(!otherEmergency.equals("")){
                otherEmergency1stLvlSwitch.setVisibility(View.VISIBLE);

                for(int i = 0; i < places1stLevelLables.length; i++){
                    if(otherEmergency.equals(getString(places1stLevelLables[i]))){
                        otherEmergency1stLvlSwitch.check(places1stLevelSwitches[i]);
                        return;
                    }
                }

                otherEmergency1stLvlSwitch.check(R.id.other_emergency_1st_level_switch_more);

                otherEmergency2ndLvlSwitch.setVisibility(View.VISIBLE);

                for(int i = 0; i < places2ndLevelLables.length; i++){
                    if(otherEmergency.equals(getString(places2ndLevelLables[i]))){
                        otherEmergency2ndLvlSwitch.check(places2ndLevelSwitches[i]);
                        return;
                    }
                }

                otherEmergency2ndLvlSwitch.check(R.id.other_emergency_2nd_level_switch_more);

                otherEmergency3rdLvlContainer.setVisibility(View.VISIBLE);

                otherEmergency3rdLvlText.setText(otherEmergency);
            }
        } else if (infoMap.get(TraumaInfoElement.FROM_OTHER_EMERGENCY).equals(getString(R.string.switch_no_label).toLowerCase())) {
            //fromOtherEmergencySwitch.check(R.id.other_emergency_switch_no);
            fromOtherEmergencyCheckbox.setChecked(false);

            otherEmergency1stLvlSwitch.setVisibility(View.GONE);
            otherEmergency2ndLvlSwitch.setVisibility(View.GONE);
            otherEmergency3rdLvlContainer.setVisibility(View.GONE);
        }
    }

    private void initMtcCheckbox(int res, final MajorTraumaCriteriaElement element){
        CheckBox cb = findViewById(res);

        cb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mtcMap.put(element, isChecked);
        });

        cb.setChecked(mtcMap.get(element));
    }

    private  void initAnamnesiCheckbox(int res, final AnamnesiElement element){
        CheckBox cb = findViewById(res);

        cb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            anamnesiMap.put(element, isChecked);
        });

        cb.setChecked(anamnesiMap.get(element));
    }

    private void initPreH_A() {
        final RadioGroup aSwitch = findViewById(R.id.preh_a_switch);

        aSwitch.setOnCheckedChangeListener((radioGroup, i) -> {
            String value = ((RadioButton) findViewById(i)).getText().toString();
            ActiveTrauma.updatePrehElement(PrehElement.A, value);
        });

        if(!ActiveTrauma.getPrehElement(PrehElement.A).isEmpty()){
            String value = ActiveTrauma.getPrehElement(PrehElement.A);

            if(value.equals(getString(R.string.preh_a_tot_label))){
                aSwitch.check(R.id.preh_a_switch_tot);
            } else if(value.equals(getString(R.string.preh_a_sga_label))){
                aSwitch.check(R.id.preh_a_switch_sga);
            } else if(value.equals(getString(R.string.preh_a_rs_label))){
                aSwitch.check(R.id.preh_a_switch_rs);
            } else if(value.equals(getString(R.string.preh_a_ct_label))){
                aSwitch.check(R.id.preh_a_switch_ct);
            }
        }
    }

    private void initPreH_B() {
        initPreHCheckBox(R.id.preh_b_cb_1, PrehElement.B_PLEURAL_DECOMPRESSION);
    }

    private void initPreH_C() {
        initPreHCheckBox(R.id.preh_c_cb_1, PrehElement.C_BLOOD_PROTOCOL);
        initPreHCheckBox(R.id.preh_c_cb_2, PrehElement.C_TPOD);
    }

    private void initPreH_D() {

        final TraumaTrackerEditText gcsEdit = findViewById(R.id.preh_d_gcs_edit);
        gcsEdit.setOnClickListener(getContext(), getString(R.string.vs_gcs_total_title), "", false, false, new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                if(!input.isEmpty()){
                    double n = Double.parseDouble(input);
                    return n >= 3 && n <= 15;
                }

                return true;
            }

            @Override
            public String errorMessage() {
                return getContext().getString(R.string.preh_gcs_error_message);
            }
        });

        gcsEdit.registerTextUpdateListener(text -> ActiveTrauma.updatePrehElement(PrehElement.D_GCS_TOTAL, gcsEdit.getText().toString()));
        gcsEdit.setText(ActiveTrauma.getPrehElement(PrehElement.D_GCS_TOTAL));

        initPreHCheckBox(R.id.preh_d_cb_1, PrehElement.D_ANISOCORIA);
        initPreHCheckBox(R.id.preh_d_cb_2, PrehElement.D_MIDRIASI);
    }

    private void initPreH_E() {
        initPreHCheckBox(R.id.preh_e_cb_1, PrehElement.E_PARA_TETRA_PARESI);
    }

    private void initPreH_Others(){
        RadioGroup territorialAreaSwitch = findViewById(R.id.preh_territorialArea_switch);

        territorialAreaSwitch.setOnCheckedChangeListener((radioGroup, i) -> {
            String value = ((RadioButton) findViewById(i)).getText().toString();
            ActiveTrauma.updatePrehElement(PrehElement.TERRITORIAL_AREA, value);
        });

        if(!ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA).isEmpty()){
            String value = ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA);

            if(value.equals(getString(R.string.place_ravenna))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_ra);
            } else if(value.equals(getString(R.string.place_forli))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_fo);
            } else if(value.equals(getString(R.string.place_cesena))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_ce);
            } else if(value.equals(getString(R.string.place_rimini))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_rn);
            }
        }

        initPreHCheckBox(R.id.preh_caraccident_checkbox, PrehElement.CAR_ACCIDENT);

        final TraumaTrackerEditText wrostBpEdit = findViewById(R.id.preh_wrost_pas_edit);
        wrostBpEdit.setOnClickListener(getContext(), getString(R.string.wrost_pas_label), "mmHg", false, false, new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                if(!input.isEmpty()){
                    double n = Double.parseDouble(input);
                    return n >= 0;
                }

                return true;
            }

            @Override
            public String errorMessage() {
                return getContext().getString(R.string.integer_error_message);
            }
        });

        wrostBpEdit.registerTextUpdateListener(text -> ActiveTrauma.updatePrehElement(PrehElement.WROST_BLOOD_PRESSURE, wrostBpEdit.getText().toString()));
        wrostBpEdit.setText(ActiveTrauma.getPrehElement(PrehElement.WROST_BLOOD_PRESSURE));

        final TraumaTrackerEditText wrostRespiratoryRateEdit = findViewById(R.id.preh_wrost_rr_edit);
        wrostRespiratoryRateEdit.setOnClickListener(getContext(), getString(R.string.wrost_resp_rate_label), "atti/min", false, false, new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                if(!input.isEmpty()){
                    double n = Double.parseDouble(input);
                    return n >= 0;
                }

                return true;
            }

            @Override
            public String errorMessage() {
                return getContext().getString(R.string.integer_error_message);
            }
        });

        wrostRespiratoryRateEdit.registerTextUpdateListener(text -> ActiveTrauma.updatePrehElement(PrehElement.WROST_RESPIRATORY_RATE, wrostRespiratoryRateEdit.getText().toString()));
        wrostRespiratoryRateEdit.setText(ActiveTrauma.getPrehElement(PrehElement.WROST_RESPIRATORY_RATE));
    }

    private void initPreHCheckBox(final int cbRes, final PrehElement element){
        final CheckBox cb = findViewById(cbRes);

        if(!ActiveTrauma.getPrehElement(element).isEmpty()) {
            String value = ActiveTrauma.getPrehElement(element);

            if(value.equals(getString(R.string.switch_yes_label).toLowerCase())){
                cb.setChecked(true);
            } else if (value.equals(getString(R.string.switch_no_label).toLowerCase())){
                cb.setChecked(false);
            }
        }

        cb.setOnClickListener(v -> ActiveTrauma.updatePrehElement(element, getString(cb.isChecked() ? R.string.switch_yes_label : R.string.switch_no_label).toLowerCase()));
    }

    private void initTabs(){
        final int[] tabs = new int[]{R.id.tab_patient_info_1, R.id.tab_patient_info_2, R.id.tab_patient_info_3};

        for(int i = 0; i < tabs.length; i++){
            final int tabId = i;
            findViewById(tabs[i]).setOnClickListener(v -> switchTab(tabs, tabId));
        }

        switchTab(tabs, 0);
    }

    private void switchTab(int[] tabs, int id){
        int[] containers = new int[]{R.id.tab_1_container, R.id.tab_2_container, R.id.tab_3_container};

        for(int i = 0; i < tabs.length; i++){
            TextView tab = findViewById(tabs[i]);
            tab.setBackgroundResource(R.drawable.traumatracker_tab);
            tab.setTypeface(null, Typeface.NORMAL);

            findViewById(containers[i]).setVisibility(View.GONE);
        }

        TextView selectedTab = findViewById(tabs[id]);
        selectedTab.setBackgroundResource(R.drawable.traumatracker_tab_selected);
        selectedTab.setTypeface(null, Typeface.BOLD);

        findViewById(containers[id]).setVisibility(View.VISIBLE);
    }

    private void savePatientInfo() {
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.CODE, infoMap.get(TraumaInfoElement.CODE));
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.SDO, infoMap.get(TraumaInfoElement.SDO));
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.ADMISSION_CODE, infoMap.get(TraumaInfoElement.ADMISSION_CODE));

        infoMap.put(TraumaInfoElement.NAME, ((EditText)findViewById(R.id.nameEditText)).getText().toString());
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.NAME, infoMap.get(TraumaInfoElement.NAME));

        infoMap.put(TraumaInfoElement.SURNAME, ((EditText)findViewById(R.id.surnameEditText)).getText().toString());
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.SURNAME, infoMap.get(TraumaInfoElement.SURNAME));

        ActiveTrauma.updatePatientInfo(TraumaInfoElement.GENDER, infoMap.get(TraumaInfoElement.GENDER));
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.AGE, ((TraumaTrackerEditText) findViewById(R.id.ageEditText)).getText().toString().trim());

        infoMap.put(TraumaInfoElement.DOB, ((EditText)findViewById(R.id.dobEditText)).getText().toString());
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.DOB, infoMap.get(TraumaInfoElement.DOB));

        ActiveTrauma.updatePatientInfo(TraumaInfoElement.ACCIDENT_DATE, !currentAccidentDateTime.isDateInit() ? "" : DateTimeUtils.formatDate(currentAccidentDateTime.year(), currentAccidentDateTime.month(), currentAccidentDateTime.day()));
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.ACCIDENT_TIME, !currentAccidentDateTime.isTimeInit() ? "" : DateTimeUtils.formatTime(currentAccidentDateTime.hour(), currentAccidentDateTime.minute(), currentAccidentDateTime.second()));
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.ACCIDENT_TYPE, infoMap.get(TraumaInfoElement.ACCIDENT_TYPE));
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.VEHICLE, infoMap.get(TraumaInfoElement.VEHICLE));
        ActiveTrauma.updatePatientInfo(TraumaInfoElement.FROM_OTHER_EMERGENCY, infoMap.get(TraumaInfoElement.FROM_OTHER_EMERGENCY));

        if(!((EditText)findViewById(R.id.other_emergency_3rd_level_text)).getText().toString().equals("")){
            infoMap.put(TraumaInfoElement.OTHER_EMERGENCY, ((EditText)findViewById(R.id.other_emergency_3rd_level_text)).getText().toString());
        }

        ActiveTrauma.updatePatientInfo(TraumaInfoElement.OTHER_EMERGENCY, infoMap.get(TraumaInfoElement.OTHER_EMERGENCY));

        for (int i = 0; i < mtcMap.keySet().toArray().length; i++) {
            MajorTraumaCriteriaElement mtce = (MajorTraumaCriteriaElement) mtcMap.keySet().toArray()[i];
            ActiveTrauma.setMajorTraumaCriteriaElement(mtce, mtcMap.get(mtce));
        }

        for (int i = 0; i < anamnesiMap.keySet().toArray().length; i++) {
            AnamnesiElement ae = (AnamnesiElement) anamnesiMap.keySet().toArray()[i];
            ActiveTrauma.setAnamnesiElement(ae, anamnesiMap.get(ae));
        }
    }

    public void updatePatientCode(String code) {
        if(code.equals("empty"))
            return;

        infoMap.put(TraumaInfoElement.CODE, code);
        ((EditText) findViewById(R.id.patient_code_text)).setText(code);
    }

    public void updatePatientSdo(String sdo) {
        if(sdo.equals("empty"))
            return;

        infoMap.put(TraumaInfoElement.SDO, sdo);
        ((EditText) findViewById(R.id.patient_sdo_text)).setText(sdo);
    }

    public void updatePreHInfo(JSONObject patient, JSONObject vs){
        final JSONObject anagraphic = patient.optJSONObject("anagraphic");
        final JSONObject status = patient.optJSONObject("status");

        if(anagraphic != null){
            ((EditText) findViewById(R.id.nameEditText)).setText(anagraphic.optString("firstname"));
            ((EditText) findViewById(R.id.surnameEditText)).setText(anagraphic.optString("lastname"));
            ((EditText) findViewById(R.id.dobEditText)).setText(anagraphic.optString("birthday"));

            if(anagraphic.optString("gender").equalsIgnoreCase("man")){
                ((RadioGroup)findViewById(R.id.gender_switch)).check(R.id.gender_switch_m);
            } else if(anagraphic.optString("gender").equalsIgnoreCase("woman")){
                ((RadioGroup)findViewById(R.id.gender_switch)).check(R.id.gender_switch_f);
            }

            ((CheckBox) findViewById(R.id.anamnesi_cb_1)).setChecked(anagraphic.optBoolean("anticoagulants"));
            ((CheckBox) findViewById(R.id.anamnesi_cb_2)).setChecked(anagraphic.optBoolean("antiplatelets"));
        }

        //TODO: complete...


    }

    public interface Listener {
        void newBarcodeRequest(String type);
        void retrievePrehMissions();
    }
}
