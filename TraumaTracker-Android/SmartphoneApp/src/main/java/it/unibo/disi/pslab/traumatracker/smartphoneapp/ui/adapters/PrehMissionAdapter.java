package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.PrehMission;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.PrehImportDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTime;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.DateTimeUtils;

public class PrehMissionAdapter extends ArrayAdapter<PrehMission> {

    private PrehImportDialog.Listener listener;

    private static class ViewHolder {
        TextView rescuer, vehicle, mission_code;
        TextView dt1, p1, dt2, p2, dt3, p3, dt4, p4, dt5, p5;

        TextView importButton;
    }

    public PrehMissionAdapter(Context context, List<PrehMission> data, PrehImportDialog.Listener listener) {
        super(context, R.layout.item_preh_data_list, data);

        this.listener = listener;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull final ViewGroup parent) {
        final PrehMission prehMission = getItem(position);

        final ViewHolder vh;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_preh_data_list, parent, false);

            vh = new ViewHolder();

            vh.rescuer = convertView.findViewById(R.id.rescuer);
            vh.vehicle = convertView.findViewById(R.id.vehicle);
            vh.mission_code = convertView.findViewById(R.id.mission_code);

            vh.dt1 = convertView.findViewById(R.id.dt1);
            vh.p1 = convertView.findViewById(R.id.p1);
            vh.dt2 = convertView.findViewById(R.id.dt2);
            vh.p2 = convertView.findViewById(R.id.p2);
            vh.dt3 = convertView.findViewById(R.id.dt3);
            vh.p3 = convertView.findViewById(R.id.p3);
            vh.dt4 = convertView.findViewById(R.id.dt4);
            vh.p4 = convertView.findViewById(R.id.p4);
            vh.dt5 = convertView.findViewById(R.id.dt5);
            vh.p5 = convertView.findViewById(R.id.p5);

            vh.importButton = convertView.findViewById(R.id.importButton);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        if(prehMission != null){
            vh.rescuer.setText(prehMission.rescuer().isEmpty() ? "--":prehMission.rescuer());
            vh.vehicle.setText(prehMission.vehicle().isEmpty() ? "--":prehMission.vehicle());
            vh.mission_code.setText(prehMission.missionCode().isEmpty() ? "--":prehMission.missionCode());

            final JSONObject tracking = prehMission.tracking();

            if(tracking != null){
                fillPrehStep(tracking, "crewDeparture", vh.dt1, vh.p1);
                fillPrehStep(tracking, "arrivalOnSite", vh.dt2, vh.p2);
                fillPrehStep(tracking, "departureFromSite", vh.dt3, vh.p3);
                fillPrehStep(tracking, "landingHelipad", vh.dt4, vh.p4);
                fillPrehStep(tracking, "arrivalEr", vh.dt5, vh.p5);
            }

            vh.importButton.setOnClickListener(v -> {
                listener.onImportMission(prehMission.missionId());
            });
        }

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    private void fillPrehStep(JSONObject tracking, String stepDescription, TextView datetime, TextView place){
        final JSONObject step = tracking.optJSONObject(stepDescription);

        if(step != null){
            final DateTime dt = DateTimeUtils.createDateTime(step.optString("datetime"), "T");

            datetime.setText(String.format("%s\n%s",
                    String.format("%s/%s/%s", dt.day(), dt.month(), dt.year()),
                    String.format("%s:%s", dt.hour(), dt.minute())));
            place.setText(step.optString("place"));
        }
    }
}
