package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.PrehElement;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.checkers.QuantityChecker;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.components.TraumaTrackerEditText;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class PrehDialog extends TTDialog {

    private Context ctx;

    public PrehDialog(final Context context) {
        super(context, R.layout.dialog_preh);

        this.ctx = context;

        intiUI();
    }

    private void intiUI() {
        findViewById(R.id.exitLabel).setOnClickListener(v -> {
            dismiss();
            ActiveTrauma.notifyPreH();
        });

        initPreH_A();
        initPreH_B();
        initPreH_C();
        initPreH_D();
        initPreH_E();

        initPreH_Others();
    }

    private void initPreH_A() {
        final RadioGroup aSwitch = findViewById(R.id.preh_a_switch);

        aSwitch.setOnCheckedChangeListener((radioGroup, i) -> {
            String value = ((RadioButton) findViewById(i)).getText().toString();
            ActiveTrauma.updatePrehElement(PrehElement.A, value);
        });

        if(!ActiveTrauma.getPrehElement(PrehElement.A).isEmpty()){
            String value = ActiveTrauma.getPrehElement(PrehElement.A);

            if(value.equals(getString(R.string.preh_a_tot_label))){
                aSwitch.check(R.id.preh_a_switch_tot);
            } else if(value.equals(getString(R.string.preh_a_sga_label))){
                aSwitch.check(R.id.preh_a_switch_sga);
            } else if(value.equals(getString(R.string.preh_a_rs_label))){
                aSwitch.check(R.id.preh_a_switch_rs);
            } else if(value.equals(getString(R.string.preh_a_ct_label))){
                aSwitch.check(R.id.preh_a_switch_ct);
            }
        }
    }

    private void initPreH_B() {
        initPreHCheckBox(R.id.preh_b_cb_1, PrehElement.B_PLEURAL_DECOMPRESSION);
    }

    private void initPreH_C() {
        initPreHCheckBox(R.id.preh_c_cb_1, PrehElement.C_BLOOD_PROTOCOL);
        initPreHCheckBox(R.id.preh_c_cb_2, PrehElement.C_TPOD);
    }

    private void initPreH_D() {

        final TraumaTrackerEditText gcsEdit = findViewById(R.id.preh_d_gcs_edit);
        gcsEdit.setOnClickListener(ctx, getString(R.string.vs_gcs_total_title), "", false, false, new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                if(!input.isEmpty()){
                    double n = Double.parseDouble(input);
                    return n >= 3 && n <= 15;
                }

                return true;
            }

            @Override
            public String errorMessage() {
                return ctx.getString(R.string.preh_gcs_error_message);
            }
        });

        gcsEdit.registerTextUpdateListener(text -> ActiveTrauma.updatePrehElement(PrehElement.D_GCS_TOTAL, gcsEdit.getText().toString()));
        gcsEdit.setText(ActiveTrauma.getPrehElement(PrehElement.D_GCS_TOTAL));

        initPreHCheckBox(R.id.preh_d_cb_1, PrehElement.D_ANISOCORIA);
        initPreHCheckBox(R.id.preh_d_cb_2, PrehElement.D_MIDRIASI);
    }

    private void initPreH_E() {
        initPreHCheckBox(R.id.preh_e_cb_1, PrehElement.E_PARA_TETRA_PARESI);
    }

    private void initPreH_Others(){
        RadioGroup territorialAreaSwitch = findViewById(R.id.preh_territorialArea_switch);

        territorialAreaSwitch.setOnCheckedChangeListener((radioGroup, i) -> {
            String value = ((RadioButton) findViewById(i)).getText().toString();
            ActiveTrauma.updatePrehElement(PrehElement.TERRITORIAL_AREA, value);
        });

        if(!ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA).isEmpty()){
            String value = ActiveTrauma.getPrehElement(PrehElement.TERRITORIAL_AREA);

            if(value.equals(getString(R.string.place_ravenna))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_ra);
            } else if(value.equals(getString(R.string.place_forli))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_fo);
            } else if(value.equals(getString(R.string.place_cesena))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_ce);
            } else if(value.equals(getString(R.string.place_rimini))){
                territorialAreaSwitch.check(R.id.preh_territorialArea_switch_rn);
            }
        }

        initPreHCheckBox(R.id.preh_caraccident_checkbox, PrehElement.CAR_ACCIDENT);

        final TraumaTrackerEditText wrostBpEdit = findViewById(R.id.preh_wrost_pas_edit);
        wrostBpEdit.setOnClickListener(ctx, getString(R.string.wrost_pas_label), "mmHg", false, false, new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                if(!input.isEmpty()){
                    double n = Double.parseDouble(input);
                    return n >= 0;
                }

                return true;
            }

            @Override
            public String errorMessage() {
                return ctx.getString(R.string.integer_error_message);
            }
        });

        wrostBpEdit.registerTextUpdateListener(text -> ActiveTrauma.updatePrehElement(PrehElement.WROST_BLOOD_PRESSURE, wrostBpEdit.getText().toString()));
        wrostBpEdit.setText(ActiveTrauma.getPrehElement(PrehElement.WROST_BLOOD_PRESSURE));

        final TraumaTrackerEditText wrostRespiratoryRateEdit = findViewById(R.id.preh_wrost_rr_edit);
        wrostRespiratoryRateEdit.setOnClickListener(ctx, getString(R.string.wrost_resp_rate_label), "atti/min", false, false, new QuantityChecker() {
            @Override
            public boolean checkInput(String input) {
                if(!input.isEmpty()){
                    double n = Double.parseDouble(input);
                    return n >= 0;
                }

                return true;
            }

            @Override
            public String errorMessage() {
                return ctx.getString(R.string.integer_error_message);
            }
        });

        wrostRespiratoryRateEdit.registerTextUpdateListener(text -> ActiveTrauma.updatePrehElement(PrehElement.WROST_RESPIRATORY_RATE, wrostRespiratoryRateEdit.getText().toString()));
        wrostRespiratoryRateEdit.setText(ActiveTrauma.getPrehElement(PrehElement.WROST_RESPIRATORY_RATE));
    }

    private void initPreHCheckBox(final int cbRes, final PrehElement element){
        final CheckBox cb = findViewById(cbRes);

        if(!ActiveTrauma.getPrehElement(element).isEmpty()) {
            String value = ActiveTrauma.getPrehElement(element);

            if(value.equals(getString(R.string.switch_yes_label).toLowerCase())){
                cb.setChecked(true);
            } else if (value.equals(getString(R.string.switch_no_label).toLowerCase())){
                cb.setChecked(false);
            }
        }

        cb.setOnClickListener(v -> ActiveTrauma.updatePrehElement(element, getString(cb.isChecked() ? R.string.switch_yes_label : R.string.switch_no_label).toLowerCase()));
    }
}
