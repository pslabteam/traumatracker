package it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.Note;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class LocationEventsCleaner {

    private static final LocationEventsCleaner instance = new LocationEventsCleaner();

    /**
     *
     * @return
     */
    public static LocationEventsCleaner instance() {
        return instance;
    }

    private LocationEventsCleaner() { }

    /**
     *
     * @param notes
     */
    public synchronized List<Note> applyRules(final List<Note> notes){

        final List<Note> notes1 = new TransportRule().apply(notes);
        final List<Note> notes2 = new RoomRule().apply(notes1);

        return notes2;
    }

    private List<Note> filterPlaceNotes(final List<Note> notes) throws JSONException {
        final List<Note> placeNotes = new LinkedList<>();

        for(final Note n : notes){
            if(n.getContent().getString(C.report.events.TYPE).equals(C.report.events.type.ROOM_IN)
                    || n.getContent().getString(C.report.events.TYPE).equals(C.report.events.type.ROOM_OUT)){
                placeNotes.add(n);
            }
        }

        return placeNotes;
    }

    interface PlaceRule {
        List<Note> apply(List<Note> notes);
    }

    /**
     * Elimina gli eventi relativi ai Trasporti non corretti (quando prima e dopo ci si ritrova nella
     * medesima stanza R1. Es.
     * ...
     * OUT ROOM R1
     * IN TRANSPORT
     * ...
     * OUT TRANSPORT
     * IN ROOM R1
     * ...
     */
    class TransportRule implements PlaceRule {

        @Override
        public List<Note> apply(List<Note> notes) {
            return null;
        }
    }

    /**
     *
     */
    class RoomRule implements PlaceRule {

        @Override
        public List<Note> apply(List<Note> notes) {
            return null;
        }
    }
}
