package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Drug;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Dosage;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class DrugConfirmationDialog extends TTDialog {

    private static final String ZERO = "0";
    private static final String COMMA = ",";
    private static final String CANCEL = "C";
    private static final String DOT = ".";

    private static final String EMPTY = "";

    private boolean firstDigit = true;

    private EditText number;

    public DrugConfirmationDialog(final Context context, final Drug drug, final Listener listener) {
        super(context, R.layout.dialog_drug_confirmation);

        initUI(drug, listener);

        switch (drug.dosageType()){
            case FIXED:
                hideKeyboard();
                break;

            case VARIABLE:
                showKeyboard();
                break;

            case NULL:
                hideKeyboard();
                hideDosage();
                break;
        }
    }

    private void initUI(final Drug drug, final Listener listener) {
        ((TextView) findViewById(R.id.title)).setText(drug.toString().toUpperCase());

        number = findViewById(R.id.number);

        if(!drug.dosageType().equals(Dosage.NULL)){
            number.setText(String.valueOf(new DecimalFormat("0.#").format(drug.defaultValue())).replace(DOT, COMMA));
            ((EditText) findViewById(R.id.unit)).setText(drug.unit().toString());
        }

        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.closeButton).setOnClickListener(v -> {
            if(drug.dosageType().equals(Dosage.NULL)){
                listener.onCloseButtonClicked();
            } else {
                double qty;

                try {
                    qty = Double.parseDouble(number.getText().toString().replace(COMMA, DOT));
                } catch (NullPointerException | NumberFormatException e) {
                    new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.value_error_message), null).show();
                    return;
                }

                if(qty == 0){
                    new MessageDialog(getContext(), getString(R.string.warning),
                            getString(R.string.value_error_message), null).show();
                    return;
                }

                listener.onCloseButtonClicked(qty);
            }

            dismiss();
        });
    }

    private void hideDosage(){
        number.setVisibility(View.GONE);
        findViewById(R.id.unit).setVisibility(View.GONE);
    }

    private void showKeyboard() {
        findViewById(R.id.pad).setVisibility(View.VISIBLE);

        int[] padButtons = new int[]{
                R.id.pad0, R.id.pad1, R.id.pad2, R.id.pad3, R.id.pad4, R.id.pad5,
                R.id.pad6, R.id.pad7, R.id.pad8, R.id.pad9, R.id.padC, R.id.padComma
        };

        for (int padButton : padButtons) {
            Button b = findViewById(padButton);
            b.setText(b.getTag().toString());
            b.setOnClickListener(v -> manageNumberText(v.getTag().toString()));
        }
    }

    private void hideKeyboard(){
        findViewById(R.id.pad).setVisibility(View.GONE);
    }

    private void manageNumberText(String value) {

        if (firstDigit) {
            number.setText(EMPTY);
            firstDigit = false;
        }

        if (value.equals(COMMA)) {
            if (!number.getText().toString().contains(COMMA) && !number.getText().toString().equals("")) {
                number.setText(String.format("%s,", number.getText()));
            }
            return;
        }

        if (value.equals(CANCEL)) {
            if (!number.getText().toString().equals(EMPTY)) {
                number.setText(number.getText().subSequence(0, number.getText().length() - 1));
            }
            return;
        }

        if (number.getText().toString().equals(ZERO)) {
            number.setText(EMPTY);
        }

        number.setText(String.format("%s%s", number.getText(), value));
    }

    public interface Listener {
        void onCloseButtonClicked();
        void onCloseButtonClicked(double value);
    }
}
