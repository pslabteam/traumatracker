package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.DiagnosticsFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.ContinuousInfusionsFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.DrugsFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.ProceduresFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.BloodProductsFragment;

public class FragmentTabsAdapter extends FragmentPagerAdapter {

    private static final int PAGES = 5;

    public FragmentTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        if(position >= PAGES)
            return null;

        switch (position) {
            case 0: return DrugsFragment.newInstance();
            case 1: return BloodProductsFragment.newInstance();
            case 2: return ContinuousInfusionsFragment.newInstance();
            case 3: return ProceduresFragment.newInstance();
            case 4: return DiagnosticsFragment.newInstance();

            default: return null;
        }
    }

    @Override
    public int getCount() {
        return PAGES;
    }
}
