package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.EditText;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class TextNoteDialog extends TTDialog {

    public TextNoteDialog(Context context, final Listener listener) {
        super(context, R.layout.dialog_text_note);

        intiUI(listener);
    }

    private void intiUI(final Listener listener){
        findViewById(R.id.saveButton).setOnClickListener(v -> {
            if(listener!=null){
                String note = ((EditText) findViewById(R.id.noteEdit)).getText().toString();
                listener.onNoteTaken(note);
            }

            dismiss();
        });

        findViewById(R.id.closeButton).setOnClickListener(v -> dismiss());
    }

    public interface Listener{
        void onNoteTaken(String note);
    }
}
