package it.unibo.disi.pslab.traumatracker.smartphoneapp;


import cartago.OPERATION;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class WarningDisplay extends JaCaArtifact {

    void init(){ }

    @OPERATION public void displayWarning(String message, String timestamp){
        System.out.println("["+ getId()+"] Warning @ " + timestamp + " -> " + message);
    }

    @OPERATION public void displayWarning(String message, int duration){
        System.out.println("["+ getId()+"] " + message);
    }
}
