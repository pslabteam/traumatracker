package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.util.Log;

import cartago.OPERATION;
import cartago.ObsProperty;
import cartago.OpFeedbackParam;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.logging.Logger;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools.LocationDetector;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools.PollingLocationDetector;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools.PushLocationDetector;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;

import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class LocationService extends JaCaArtifact implements Logger {

    private static final String POLLING_MODE = "polling";
    private static final String PUSH_NOTIFICATIONS_MODE = "push-notifications";

    private static final String PLACE_OBS_PROP = "place";

    private LocationDetector locationDetector;

    private volatile static LocationDetector.Status locationDetectionStatus = LocationDetector.Status.UNREACHABLE;

    void init(String mode) {
        defineObsProperty(PLACE_OBS_PROP, "");

        final String currentTagId = readSharedPreference(Preferences.ID, Preferences.BLE_TAG_ID, C.url.DEFAULT_BLE_TAG_ID);

        String url;

        switch (mode) {
            case POLLING_MODE:
                url = formatURL(POLLING_MODE, currentTagId);
                locationDetector = new PollingLocationDetector(url, 5000, this);
                log("Configured in POLLING mode.");
                break;

            case PUSH_NOTIFICATIONS_MODE:
                url = formatURL(PUSH_NOTIFICATIONS_MODE, currentTagId);
                locationDetector = new PushLocationDetector(url, currentTagId, 500, this);
                log("Configured to receive PUSH NOTIFICATIONS.");
                break;

            default:
                locationDetector = null;
                log("Error. The selected mode for location detection is unsupported.");
                break;
        }

        if (locationDetector != null) {
            locationDetector.setOnPlaceUpdateAvailableListener(this::updatePlace);
        }
    }

    @OPERATION public void changePlaceInfo(String room) {
        updatePlace(room);
    }

    @OPERATION public void restorePlaceInfo(String reportId, OpFeedbackParam<String> place) {
        Report report = Report.build(FileSystemUtils.loadFile(C.fs.TEMP_FOLDER, reportId + C.fs.REPORT_FILE_EXTENSION), false);

        Report.Event lastPlaceEvent = null;

        if (report != null) {
            for (Report.Event event : report.eventTrack().getEvents()) {
                if (event.type().equals(C.report.events.type.ROOM_IN)) {
                    lastPlaceEvent = event;
                }
            }
        }

        updatePlace(lastPlaceEvent != null ? lastPlaceEvent.place() : "");
        place.set(lastPlaceEvent != null ? lastPlaceEvent.place() : "");
    }

    @OPERATION public void checkLocationServiceReachability(OpFeedbackParam<String> opres) {
        //TODO: how to check reachability?
        locationDetectionStatus = LocationDetector.Status.IDLE;

        opres.set(locationDetectionStatus.value());
    }

    @OPERATION public void startPlaceDetection(OpFeedbackParam<String> opres) {
        locationDetector.startDetection();
        locationDetectionStatus = LocationDetector.Status.ACTIVE;
        opres.set(locationDetectionStatus.value());
    }

    @OPERATION public void stopPlaceDetection(OpFeedbackParam<String> opres) {
        locationDetector.stopDetection();
        locationDetectionStatus = LocationDetector.Status.IDLE;
        opres.set(locationDetectionStatus.value());
    }

    @Override
    public void writeOnLog(final String msg) {
        log(msg);
    }

    private void updatePlace(final String newPlace) {

        if(newPlace.isEmpty()){
            Log.d("-------->", "Called UPDATE PLACE with empty");
            beginExternalSession();
            getObsProperty(PLACE_OBS_PROP).updateValue("");
            endExternalSession(true);
            return;
        }

        try {
            beginExternalSession();

            final ObsProperty prop = getObsProperty(PLACE_OBS_PROP);

            final String currentPlace = prop.stringValue();
            //Log.d("-------->", "Called UPDATE PLACE with: " + newPlace + " and Current is: " + currentPlace);

            if (!currentPlace.equals(newPlace)) {
                Log.d("-------->", "room updated");
                prop.updateValue(newPlace);

                /*if(!(!currentPlace.matches(String.format("%s|%s|%s", "", C.place.PREH, C.place.TRANSPORT)) && !newPlace.matches(C.place.TRANSPORT))){
                    Log.d("-------->", "room updated");
                    prop.updateValue(newPlace);
                }*/
            }

            endExternalSession(true);
        } catch (Exception ex) {
            endExternalSession(false);
        }
    }

    public static boolean isLocationDetectionActive() {
        return locationDetectionStatus.equals(LocationDetector.Status.ACTIVE);
    }

    public static void updateLocationDetectionStatus(final LocationDetector.Status status) {
        locationDetectionStatus = status;
    }

    private String formatURL(final String mode, final String tagId) {
        final String address = readSharedPreference(Preferences.ID, Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP)
                + ":" + C.url.GT2_LOCATION_SERVICE_PORT
                + C.url.GET_PLACE_PREFIX
                + tagId;

        switch (mode) {
            case POLLING_MODE:
                return C.url.HTTP_PROTOCOL_PREFIX + address;

            case PUSH_NOTIFICATIONS_MODE:
                return C.url.WEB_SOCKET_PROTOCOL_PREFIX + address;

            default:
                return "";
        }
    }
}
