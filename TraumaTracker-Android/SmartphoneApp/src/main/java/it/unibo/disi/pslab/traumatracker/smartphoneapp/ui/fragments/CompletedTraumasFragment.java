package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments;

import android.database.DataSetObserver;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.StarterUI;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.Report;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.adapters.ReportsAdapter;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialogWithChoice;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.templates.TraumaTrackerFragment;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.FileSystemUtils;

public class CompletedTraumasFragment extends TraumaTrackerFragment {

    public static CompletedTraumasFragment newInstance() {
        return new CompletedTraumasFragment();
    }

    private ReportsAdapter adapter;

    private Map<ReportStatus, List<String>> reportsMap;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_completed_traumas, container, false);

        adapter = new ReportsAdapter(getContext(), getReportsList());
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                updateUI();
            }
        });

        reportsMap = new HashMap<>();

        initUI();

        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAdapterData();
    }

    protected void initUI() {
        ((ListView) mainView.findViewById(R.id.reports_list)).setAdapter(adapter);

        mainView.findViewById(R.id.buttonUploadAll).setOnClickListener(v -> {
            if(Objects.requireNonNull(reportsMap.get(ReportStatus.NOT_SYNC)).isEmpty()){
                new MessageDialog(getContext(), getString(R.string.warning), "Nessun report da sincronizzare!", () -> {
                    //nothing to do!
                }).show();
                return;
            }

            StarterUI.instance.manageReportsSyncRequest(reportsMap.get(ReportStatus.NOT_SYNC), this::refreshAdapterData);
        });

        mainView.findViewById(R.id.buttonDeleteAlreadySync).setOnClickListener(v -> {

            if(Objects.requireNonNull(reportsMap.get(ReportStatus.SYNC)).isEmpty()) {
                new MessageDialog(getContext(), getString(R.string.warning), "Non esistono report già sincronizzati che possano essere eliminati!", null).show();
                return;
            }

            new MessageDialogWithChoice(getContext(), getString(R.string.warning), "Attenzione. Confermando saranno eliminati da questo dispositivo tutti i report completati, già sincronizzati con il servizio TraumaTracker.", new MessageDialogWithChoice.Listener() {
                @Override
                public void onNoButtonClicked() {
                    //nothing to do!
                }

                @Override
                public void onYesButtonClicked() {
                    FileSystemUtils.emptyFolder(C.fs.REPORTS_SYNC_FOLDER);
                    new MessageDialog(getContext(), getString(R.string.success), "Tutti i report già sincronizzati sono stati cancellati!", null).show();
                    refreshAdapterData();
                }
            }).show();
        });

        updateUI();
    }

    private void refreshAdapterData(){
        final List<Report> reports = getReportsList();

        if(reports.size() != adapter.getCount()){
            adapter.clear();
            adapter.addAll(reports);
            adapter.notifyDataSetChanged();
        }
    }

    protected void updateUI(){
        reportsMap.put(ReportStatus.NOT_SYNC, new ArrayList<>());
        reportsMap.put(ReportStatus.SYNC, new ArrayList<>());

        if(adapter.getCount() == 0){
            //NO REPORTS
            mainView.findViewById(R.id.noUsersLabel).setVisibility(View.VISIBLE);
            mainView.findViewById(R.id.commands).setVisibility(View.GONE);
            return;
        }

        mainView.findViewById(R.id.noUsersLabel).setVisibility(View.GONE);
        mainView.findViewById(R.id.commands).setVisibility(View.VISIBLE);

        for (int i = 0; i < adapter.getCount(); i++) {
            final Report r = adapter.getItem(i);
            if (r != null) {
                Objects.requireNonNull(reportsMap.get(r.isSync() ? ReportStatus.SYNC : ReportStatus.NOT_SYNC)).add(r.id());
            }
        }
    }

    public interface Events {
        void onReportSyncCompleted();
    }

    private List<Report> getReportsList(){
        final List<Report> reports = new ArrayList<>();
        reports.addAll(getReportsFromFolder(C.fs.REPORTS_SYNC_FOLDER, true));
        reports.addAll(getReportsFromFolder(C.fs.REPORTS_TOSYNC_FOLDER, false));

        Collections.sort(reports, (r1, r2) -> (r1.startDate() + "-" + r1.startTime()).compareTo(r2.startDate() + "-" + r2.startTime()));
        Collections.reverse(reports);

        return reports;
    }

    private List<Report> getReportsFromFolder(String folder, boolean sync) {
        final List<Report> reports = new ArrayList<>();

        final File[] files = FileSystemUtils.getFilesFromDirectory(folder);

        if(files != null && files.length > 0){
            for (final File file : files) {
                final String fileContent = FileSystemUtils.loadFile(folder, file.getName());

                if(!fileContent.isEmpty()){
                    final Report currentReport = Report.build(fileContent, sync);
                    if(currentReport != null){
                        reports.add(currentReport);
                    }
                } else {
                    //nothing to do!
                }
            }
        }

        return reports;
    }

    private enum ReportStatus{
        SYNC, NOT_SYNC
    }
}
