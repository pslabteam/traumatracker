package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.util.Pair;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.HashMap;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.model.GCS;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.VitalSign;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class ClinicalPictureVariationDialog extends TTDialog {

    private static boolean firstFragmentLoad;

    private static Map<VitalSign, Pair<Integer, Integer[]>> radioGroupsMap = new HashMap<VitalSign, Pair<Integer, Integer[]>>() {{
        put(VitalSign.PUPILS, new Pair<>(R.id.vs_eye_selector, new Integer[]{R.id.vs_eye_normal, R.id.vs_eye_aniso, R.id.vs_eye_midriasi}));
    }};

    public ClinicalPictureVariationDialog(Context context) {
        super(context, R.layout.dialog_clicnical_picture_variation);

        intiUI();

        if (firstFragmentLoad) {
            for (int i = 0; i < radioGroupsMap.size(); i++) {
                VitalSign vs = (VitalSign) radioGroupsMap.keySet().toArray()[i];
                setCheckElementOnRadioGroup(radioGroupsMap.get(vs).first,
                        (VitalSign) radioGroupsMap.keySet().toArray()[i],
                        radioGroupsMap.get(vs).second);
            }

            ActiveTrauma.initializeVariationsMap();

            firstFragmentLoad = false;
        }
    }

    private void intiUI() {
        findViewById(R.id.exitLabel).setOnClickListener(v -> dismiss());

        for (int i = 0; i < radioGroupsMap.size(); i++) {
            VitalSign vs = (VitalSign)radioGroupsMap.keySet().toArray()[i];

            initVitalSignRadioGroup(radioGroupsMap.get(vs).first,
                    (VitalSign) radioGroupsMap.keySet().toArray()[i],
                    radioGroupsMap.get(vs).second);
        }

        findViewById(R.id.gcs_button).setOnClickListener(v1 -> {
            GCS currentGCS = new GCS(ActiveTrauma.getVariationVitalSign(VitalSign.GCS_MOTOR),
                    ActiveTrauma.getVariationVitalSign(VitalSign.GCS_VERBAL),
                    ActiveTrauma.getVariationVitalSign(VitalSign.GCS_EYES),
                    Boolean.parseBoolean(ActiveTrauma.getVariationVitalSign(VitalSign.SEDATED)));

            new GCSDialog(v1.getContext(), currentGCS, new GCSDialog.Listener() {
                @Override
                public void onBackButtonClicked() {
                    //nothing to do!
                }

                @Override
                public void onConfirmationButtonClicked(GCS newGCS) {
                    ActiveTrauma.registerVitalSignVariation(VitalSign.GCS_MOTOR, newGCS.motor());
                    ActiveTrauma.registerVitalSignVariation(VitalSign.GCS_VERBAL, newGCS.verbal());
                    ActiveTrauma.registerVitalSignVariation(VitalSign.GCS_EYES, newGCS.eyes());
                    ActiveTrauma.registerVitalSignVariation(VitalSign.GCS_TOTAL, String.valueOf(newGCS.total()));
                    ActiveTrauma.registerVitalSignVariation(VitalSign.SEDATED, String.valueOf(newGCS.sedated()));
                }
            }).show();
        });
    }

    private void initVitalSignRadioGroup(int rgRes, final VitalSign vs, Integer[] rbResList) {
        ((RadioGroup) findViewById(rgRes)).setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton rb = findViewById(i);
            if (!firstFragmentLoad) {
                ActiveTrauma.registerVitalSignVariation(vs, rb.getTag().toString());
            }
        });

        Enum<?>[] values = vs.getValues();

        for (int i = 0; i < values.length; i++) {
            RadioButton rb = findViewById(rbResList[i]);
            rb.setText(values[i].toString().toUpperCase());
            rb.setTag(values[i]);
        }
    }

    private void setCheckElementOnRadioGroup(int selector, VitalSign vs, Integer[] res) {
        String val = ActiveTrauma.getInitialVitalSign(vs);

        Enum<?>[] values = vs.getValues();

        for (int i = 0; i < values.length; i++) {
            if (values[i].toString().equals(val)) {
                ((RadioGroup) findViewById(selector)).check(res[i]);
                return;
            }
        }
    }
}
