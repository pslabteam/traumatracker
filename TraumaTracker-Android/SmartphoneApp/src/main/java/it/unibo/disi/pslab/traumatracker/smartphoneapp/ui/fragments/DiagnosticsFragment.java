package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.ActiveTrauma;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.DiagnosticsCategory;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.DiagnosticConfirmationDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.MessageDialog;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.templates.TraumaTrackerButtonsFragment;

public class DiagnosticsFragment extends TraumaTrackerButtonsFragment {
    public DiagnosticsFragment() {}

    public static DiagnosticsFragment newInstance() {
        return new DiagnosticsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_diagnostics, container, false);

        initUI(mainView);

        return mainView;
    }

    private void initUI(View mainView){
        Map<DiagnosticsCategory, List<Button>> diagnosticButtonsMap = new TreeMap<>(new DiagnosticsCategory.DiagnosticCategoryComparator());

        for (DiagnosticsCategory category : DiagnosticsCategory.values()) {
            List<Button> currentCategoryButtons = new ArrayList<>();

            for (int i = 0; i < Diagnostics.values().length; i++) {
                if (Diagnostics.values()[i].category().toString().equals(category.toString())) {
                    final Diagnostics d = Diagnostics.values()[i];

                    currentCategoryButtons.add(buildButton(getContext(), d.toString(), R.drawable.traumatracker_button, v -> {
                        if(!ActiveTrauma.isPatientAcceptedInER()){
                            new MessageDialog(getContext(), getString(R.string.warning), getString(R.string.patient_not_arrived_alert_message), null).show();
                            return;
                        }

                        new DiagnosticConfirmationDialog(getActivity(), d, options -> ActiveTrauma.registerNewDiagnostic(d, options)).show();
                    }));
                }
            }

            diagnosticButtonsMap.put(category, currentCategoryButtons);
        }

        buildButtonsGrid(mainView, diagnosticButtonsMap, 2);
    }
}
