package it.unibo.disi.pslab.traumatracker.smartphoneapp.tools;

public enum CommandFeedbackType {
	FEEDBACK_OK("ok"),
	FEEDBACK_ERROR("error");

	private String representation;

	CommandFeedbackType(String rep){
		this.representation = rep;
	}

	public String toString(){
		return representation;
	}
}
