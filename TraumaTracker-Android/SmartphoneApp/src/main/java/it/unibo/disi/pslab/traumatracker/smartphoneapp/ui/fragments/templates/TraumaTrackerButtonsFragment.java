package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.fragments.templates;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public class TraumaTrackerButtonsFragment extends Fragment {

    public TraumaTrackerButtonsFragment(){ }

    protected void buildButtonsList(final View container, final List<Button> buttonsList) {
        final LinearLayout list = container.findViewById(R.id.buttonsContainer);

        final LinearLayout categoryContainer = new LinearLayout(container.getContext());
        categoryContainer.setOrientation(LinearLayout.VERTICAL);
        categoryContainer.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        //categoryContainer.setTag(currentCategory);

        for (int j = 0; j <= buttonsList.size(); j += 1) {
            final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_VERTICAL;

            final LinearLayout layout = new LinearLayout(container.getContext());
            layout.setOrientation(LinearLayout.HORIZONTAL);
            layout.setLayoutParams(params);

            for(int k = 0; k < 1; k++){
                if ((j + k) < buttonsList.size()) {
                    layout.addView(buttonsList.get(j + k));
                }
            }

            categoryContainer.addView(layout);
        }

        list.addView(categoryContainer);
    }

    protected <C extends Enum<C>> void buildButtonsGrid(final View container, final Map<C, List<Button>> buttonsMap, final int gridSize){
        final LinearLayout grid = container.findViewById(R.id.buttonsContainer);

        final Iterator<C> iterator = buttonsMap.keySet().iterator();

        for (int i = 0; i < buttonsMap.size(); i++) {
            C currentCategory = iterator.next();

            final LinearLayout categoryContainer = new LinearLayout(container.getContext());
            categoryContainer.setOrientation(LinearLayout.VERTICAL);
            categoryContainer.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            categoryContainer.setTag(currentCategory);

            categoryContainer.addView(buildTitleLabel(container.getContext(), currentCategory));

            List<Button> tmp = buttonsMap.get(currentCategory);

            for (int j = 0; j <= tmp.size(); j += gridSize) {
                final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_VERTICAL;

                final LinearLayout layout = new LinearLayout(container.getContext());
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layout.setLayoutParams(params);

                for(int k = 0; k < gridSize; k++){
                    if ((j + k) < tmp.size()) {
                        layout.addView(tmp.get(j + k));
                    }
                }

                categoryContainer.addView(layout);
            }

            grid.addView(categoryContainer);
        }
    }

    protected Button buildButton(final Context ctx, final String text, final int bgRes, final int leftDrawableRes, final View.OnClickListener listener) {
        final Button btn = buildButton(ctx, text, bgRes, listener);

        if(leftDrawableRes != 0) {
            btn.setCompoundDrawablesWithIntrinsicBounds(leftDrawableRes, 0, 0, 0);
            btn.setPadding(dpi(10), dpi(5), dpi(5), dpi(5));
            btn.setCompoundDrawablePadding(-5);
        }

        return btn;
    }

    protected Button buildButton(final Context ctx, final String text, final int bgRes, final View.OnClickListener listener) {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight = 1.0f;
        params.setMargins(dpi(5), dpi(5), dpi(5), dpi(5));

        Button btn = new Button(ctx);
        btn.setLayoutParams(params);
        btn.setBackground(getResources().getDrawable(bgRes));
        btn.setText(text);
        btn.setTextSize(16);
        btn.setAllCaps(true);
        btn.setPadding(dpi(5), dpi(5), dpi(5), dpi(5));
        btn.setOnClickListener(listener);

        return btn;
    }

    private <C extends Enum<C>> TextView buildTitleLabel(Context ctx, C currentCategory) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1.0f;
        params.setMargins(0, dpi(5), 0, dpi(5));

        TextView tw = new TextView(ctx);
        tw.setLayoutParams(params);
        tw.setBackgroundColor(App.getAppResources().getColor(R.color.tt_red));
        tw.setText(currentCategory.toString());
        tw.setTextColor(App.getAppResources().getColor(R.color.tt_white));
        tw.setTextSize(20);
        tw.setPadding(dpi(5), dpi(2), dpi(5), dpi(2));
        return tw;
    }

    private int dpi(int value){
        return (int) (value * getResources().getDisplayMetrics().density);
    }
}
