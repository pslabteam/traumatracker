package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialog;

public class MessageDialog extends TTDialog {

    private Button btn;
    private TextView titleTextView, messageTextView;

    public MessageDialog(Context context, String title, String message, final Listener listener) {
        super(context, R.layout.dialog_message);

        intiUI(title, message, listener);
    }

    private void intiUI(String title, String message, final Listener listener) {
        titleTextView = findViewById(R.id.title);
        titleTextView.setText(title);

        messageTextView = findViewById(R.id.noUsersLabel);
        messageTextView.setText(message);

        btn = findViewById(R.id.closeButton);
        btn.setOnClickListener(v -> {
            if (listener != null) {
                listener.onCloseButtonClicked();
            }

            dismiss();
        });
    }

    public void toggleButtonVisibility(boolean visible) {
        if (visible) {
            btn.setVisibility(View.VISIBLE);
        } else {
            btn.setVisibility(View.GONE);
        }
    }

    public void changeTitle(String newTitle) {
        titleTextView.setText(newTitle);
    }

    public void changeMessage(String newMessage) {
        messageTextView.setText(newMessage);
    }

    public interface Listener {
        void onCloseButtonClicked();
    }
}
