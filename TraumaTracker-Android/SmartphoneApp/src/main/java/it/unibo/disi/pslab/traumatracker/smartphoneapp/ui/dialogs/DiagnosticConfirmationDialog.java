package it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs;

import android.content.Context;
import android.widget.TextView;

import java.util.Map;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology.Diagnostics;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.ui.dialogs.templates.TTDialogWithOptions;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public class DiagnosticConfirmationDialog extends TTDialogWithOptions<Diagnostics> {

    private boolean errors = false;

    public DiagnosticConfirmationDialog(Context context, Diagnostics diagnostics, final Listener listener) {
        super(context, R.layout.dialog_diagnostic_confirmation);

        initUI(diagnostics, listener);

        inflateOptions(diagnostics);
    }

    private void initUI(final Diagnostics diagnostics, final Listener listener){
        ((TextView) findViewById(R.id.title)).setText(diagnostics.toString().toUpperCase());

        findViewById(R.id.cancelButton).setOnClickListener(v -> dismiss());

        findViewById(R.id.closeButton).setOnClickListener(v -> {
            retrieveOptions(diagnostics);

            if(!errors) {
                listener.onCloseButtonClicked(getOptionsMap());
                dismiss();
            }
        });

    }

    @Override
    protected void inflateOptions(Diagnostics diagnostics) {
      switch (diagnostics) {
          case ABG:
              inflateOptionsStub(R.layout.options_diagnostics_abg);
              initializeOptionsMap(C.report.events.diagnostic.option.ABG_LACTATES,
                      C.report.events.diagnostic.option.ABG_BE,
                      C.report.events.diagnostic.option.ABG_PH,
                      C.report.events.diagnostic.option.ABG_HB);

              initFieldListener(R.id.lactates_number, getString(R.string.abg_option_lactates_title), getString(R.string.abg_option_lactates_unit));
              initFieldListener(R.id.be_number, getString(R.string.abg_option_be_title), "");
              initFieldListener(R.id.ph_number, getString(R.string.abg_option_ph_title),"");
              initFieldListener(R.id.hb_number, getString(R.string.abg_option_hb_title),getString(R.string.abg_option_hb_unit));

              findViewById(R.id.abgImportButton).setOnClickListener(btn -> {
                  new MessageDialog(getContext(), getString(R.string.warning),"Funzionalità non ancora disponibile!", null).show();
              });
              break;

          case ROTEM:
              inflateOptionsStub(R.layout.options_diagnostics_rotem);
              initializeOptionsMap(C.report.events.diagnostic.option.ROTEM_FIBTEM,
                      C.report.events.diagnostic.option.ROTEM_EXTEM,
                      C.report.events.diagnostic.option.ROTEM_HYPERFIBRINOLYSIS);

              initFieldListener(R.id.fibtem_a5_value, "FIBTEM - " + "A5", "mm");
              initFieldListener(R.id.extem_ct_value, "EXTEM - " + "CT", "sec");
              initFieldListener(R.id.extem_a10_value, "EXTEM - " + "A10", "mm");
              initFieldListener(R.id.extem_alpha_value, "EXTEM - " + "\u03b1", "°");
              break;

            default:
                break;
        }
    }

    @Override
    protected void retrieveOptions(Diagnostics diagnostics) {
        switch (diagnostics) {
            case ABG:
                updateOption(C.report.events.diagnostic.option.ABG_LACTATES,getFieldText(R.id.lactates_number));
                updateOption(C.report.events.diagnostic.option.ABG_BE, getFieldText(R.id.be_number));
                updateOption(C.report.events.diagnostic.option.ABG_PH, getFieldText(R.id.ph_number));
                updateOption(C.report.events.diagnostic.option.ABG_HB, getFieldText(R.id.hb_number));
                break;

            case ROTEM:
                String fibtemValue = (getFieldText(R.id.fibtem_a5_value).equals("") ? "" : "A5 = " + getFieldText(R.id.fibtem_a5_value) + "; ");

                if(fibtemValue.endsWith("; ")){
                    fibtemValue = fibtemValue.substring(0, fibtemValue.length() - 2);
                }

                String extemValue =  (getFieldText(R.id.extem_ct_value).equals("") ? "" : "CT = " + getFieldText(R.id.extem_ct_value) + "; ")
                        + (getFieldText(R.id.extem_a10_value).equals("") ? "" : "A10 = " + getFieldText(R.id.extem_a10_value) + "; ")
                        + (getFieldText(R.id.extem_alpha_value).equals("") ? "" : "alpha = " + getFieldText(R.id.extem_alpha_value) + "; ");

                if(extemValue.endsWith("; ")){
                    extemValue = extemValue.substring(0, extemValue.length() - 2);
                }

                updateOption(C.report.events.diagnostic.option.ROTEM_FIBTEM, fibtemValue);
                updateOption(C.report.events.diagnostic.option.ROTEM_EXTEM, extemValue);
                updateOption(C.report.events.diagnostic.option.ROTEM_HYPERFIBRINOLYSIS, getCheckBoxStatus(R.id.hyperfibrinolysis_cb));
                break;

            default:
                break;
        }
    }

    public interface Listener{
        void onCloseButtonClicked(Map<String, Object> options);
    }
}
