package it.unibo.disi.pslab.traumatracker.smartphoneapp.ontology;

import java.util.Comparator;

import it.unibo.disi.pslab.traumatracker.smartphoneapp.App;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.R;

public enum DrugCategory{
    INFUSIONS("infusions", R.string.ontology_drug_category_infusions, 1),
    GENERAL_DRUGS("general-drugs", R.string.ontology_drug_category_generaldrugs, 2),
    CONTINUOUS_INFUSION_DRUGS("continuous-infusion-drugs", R.string.ontology_drug_category_continuousinfusiondrugs, 3),
    ALS_DRUGS("als-drugs", R.string.ontology_drug_category_alsdrugs, 4),
    ANTIBIOTIC_PROPHYLAXIS("antibiotic-prophylaxis", R.string.ontology_drug_category_antibioticprophylaxis, 5);

    private final int labelRes;
    private final int position;

    DrugCategory(@SuppressWarnings("unused") final String id, final int labelRes, final int position){
        this.labelRes = labelRes;
        this.position = position;
    }

    public int position() {
        return position;
    }

    public String toString() {
        return App.getAppResources().getString(labelRes);
    }

    public static class DrugCategoryComparator implements Comparator<DrugCategory> {
        public int compare(DrugCategory c1, DrugCategory c2) {
            return Integer.compare(c1.position(), c2.position());
        }
    }
}
