package it.unibo.disi.pslab.traumatracker.smartphoneapp;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import cartago.OPERATION;
import cartago.OpFeedbackParam;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.logging.Logger;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.tools.NetworkManager;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.Preferences;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.vs_monitors.RealVitalSignsMonitor;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.vs_monitors.SimulatedVitalSignsMonitor;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.vs_monitors.VitalSignsMonitor;
import it.unibo.pslab.jaca_android.core.JaCaArtifact;

public class VitalSignsService extends JaCaArtifact implements Logger{

    private static final String SIMULATION_MODE = "simulation-mode";
    private static final String REAL_MONITOR_MODE = "real-monitor-mode";

    private static final String VS_REQUEST_TIMEOUT_OBS_PROP = "interval";

    private static VitalSignsService me;

    private VitalSignsMonitor monitor;

    private static Status status = Status.UNREACHABLE;
    private String currentSid = "";

    public void init(final String mode, final int timeout){
	    me = this;

	    switch(mode){
            case SIMULATION_MODE:
                monitor = new SimulatedVitalSignsMonitor(this);
                break;

            case REAL_MONITOR_MODE:
                monitor = new RealVitalSignsMonitor(formatURL(), this);
                break;

            default:
                monitor = null;
                break;
        }

        defineObsProperty(VS_REQUEST_TIMEOUT_OBS_PROP, timeout);
    }
	
	@OPERATION void getVitalSigns(OpFeedbackParam<Integer> opres, OpFeedbackParam<Integer> sys, OpFeedbackParam<Integer> dia, OpFeedbackParam<Integer> hr, OpFeedbackParam<Integer> etco2, OpFeedbackParam<Integer> spo2, OpFeedbackParam<Integer> temp){

	    final Map<String, Integer> values = monitor.getPatientVitalSigns();

	    if(values == null) {
            opres.set(HttpsURLConnection.HTTP_SERVER_ERROR);

            sys.set(null);
            dia.set(null);
            hr.set(null);
            etco2.set(null);
            spo2.set(null);
            temp.set(null);

            return;
        }

        opres.set(HttpsURLConnection.HTTP_OK);

        sys.set(values.get(C.vs.VS_SYS));
        dia.set(values.get(C.vs.VS_DIA));
        hr.set(values.get(C.vs.VS_HR));
        etco2.set(values.get(C.vs.VS_ETCO2));
        spo2.set(values.get(C.vs.VS_SPO2));
        temp.set(values.get(C.vs.VS_TEMP));
	}

    @OPERATION void checkVitalSignsServiceReachability(OpFeedbackParam<String> opres){
        final String url = C.url.HTTP_PROTOCOL_PREFIX
                + me.readSharedPreference(Preferences.ID, Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP)
                + ":"
                + C.url.GT2_GATEWAY_SERVICE_PORT
                + "/gt2/vsservice/info";

        try {
            if(NetworkManager.doGET(url).first.equals(HttpsURLConnection.HTTP_OK)){
                status = Status.IDLE;
            } else {
                status = Status.UNREACHABLE;
            }

            opres.set(status.value());
        } catch (IOException e) {
            opres.set(status.value());
        }
    }

	@OPERATION
    void createVitalSignsSession(final String sid, final int interval, final OpFeedbackParam<String> opRes){

        if(status.equals(Status.UNREACHABLE)){
            Log.d(C.LOG_TAG, "VitalSigns Service unreachable!");
            opRes.set(Status.UNREACHABLE.value());
            return;
        }

        final String url = C.url.HTTP_PROTOCOL_PREFIX
                + me.readSharedPreference(Preferences.ID, Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP)
                + ":"
                + C.url.GT2_GATEWAY_SERVICE_PORT
                + "/gt2/vsservice/api/sessions";

        currentSid = sid;

        try {
            JSONObject requestBody = new JSONObject()
                    .put("sid",sid)
                    .put("pid", ActiveTrauma.patientId())
                    .put("pname", ActiveTrauma.patientId())
                    .put("interval", interval);

            if(NetworkManager.doPOST(url, requestBody.toString()).equals(HttpsURLConnection.HTTP_OK)){
                Log.d(C.LOG_TAG, "Session " + sid  + " created on server!");
                status = Status.IDLE;
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    @OPERATION
    void startVitaSignsSession(OpFeedbackParam<String> opres){
        if(status.equals(Status.UNREACHABLE)){
            opres.set(status.value());
            Log.d(C.LOG_TAG, "VS Gateway unreachable!");
            return;
        }

        final String url = C.url.HTTP_PROTOCOL_PREFIX
                + me.readSharedPreference(Preferences.ID, Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP)
                + ":"
                + C.url.GT2_GATEWAY_SERVICE_PORT
                + "/gt2/vsservice/api/sessions/"
                + currentSid
                + "/start";

        try {
            if(NetworkManager.doGET(url).first.equals(HttpsURLConnection.HTTP_OK)){
                Log.d(C.LOG_TAG, "Session " + currentSid + " started on server!");
                status = Status.ACTIVE;
                opres.set(status.value());
            }

        } catch (IOException e) {
            e.printStackTrace();
            opres.set(status.value());
        }
    }

    @OPERATION
    void stopVitaSignsSession(OpFeedbackParam<String> opres){
        if(status.equals(Status.UNREACHABLE)){
            Log.d(C.LOG_TAG, "VS Gateway unreachable!");
            opres.set(status.value());
            return;
        }

        final String url = C.url.HTTP_PROTOCOL_PREFIX
                + me.readSharedPreference(Preferences.ID, Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP)
                + ":"
                + C.url.GT2_GATEWAY_SERVICE_PORT
                + "/gt2/vsservice/api/sessions/"
                + currentSid
                + "/stop";

        try {
            if(NetworkManager.doGET(url).first.equals(HttpsURLConnection.HTTP_OK)){
                Log.d(C.LOG_TAG, "Session " + currentSid  + " stopped on server!");
                status = Status.IDLE;
                opres.set(status.value());
            }

        } catch (IOException e) {
            e.printStackTrace();
            opres.set(status.value());
        }
    }

    public static void retryConnection(){
	    me.beginExternalSession();
	    me.signal("restart");
	    me.endExternalSession(true);
    }

    public static Status status(){
        return status;
    }

    private String formatURL() {
        return C.url.HTTP_PROTOCOL_PREFIX
                + readSharedPreference(Preferences.ID, Preferences.TT_SERVICE_IP, C.url.DEFAULT_TT_SERVICE_IP)
                + ":"
                + C.url.GT2_GATEWAY_SERVICE_PORT
                + C.url.GET_VITALSIGNS_PREFIX
                + File.separator
                + "TraumaT_SHOCKR"
                + C.url.GET_VITALSIGNS_SUFFIX;
    }

    @Override
    public void writeOnLog(String msg) {
        log(msg);
    }

    enum Status {
        ACTIVE("active"),
        IDLE("idle"),
        UNREACHABLE("unreachable");

        private String value;

        Status(String value){
            this.value = value;
        }

        public String value() {
            return value;
        }
    }
}
