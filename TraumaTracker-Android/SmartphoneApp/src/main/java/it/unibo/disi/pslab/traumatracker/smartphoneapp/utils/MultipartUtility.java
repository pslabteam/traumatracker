package it.unibo.disi.pslab.traumatracker.smartphoneapp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MultipartUtility {
    private static final String LINE_FEED = "\r\n";
    private static final String BOUNDARY = "MyBoundary";

    private HttpURLConnection httpConnection;
    private OutputStream outputStream;
    private PrintWriter writer;

    public MultipartUtility(String requestURL) throws IOException {
        httpConnection = (HttpURLConnection) new URL(requestURL).openConnection();
        httpConnection.setUseCaches(false);
        httpConnection.setDoOutput(true);
        httpConnection.setDoInput(true);
        httpConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        httpConnection.setRequestProperty("accept", "application/json");

        outputStream = httpConnection.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8), true);
    }

    public void addFilePart(String fieldName, File uploadFile) throws IOException {
        String contentDisposition = "Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + uploadFile.getName() + "\"";
        String contentType = "Content-Type: application/octet-stream";
        String contentTransferEncoding = "Content-Transfer-Encoding: binary";

        writer.append("--" + BOUNDARY).append(LINE_FEED);
        writer.append(contentDisposition).append(LINE_FEED);
        writer.append(contentType).append(LINE_FEED);
        writer.append(contentTransferEncoding).append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);

        byte[] buffer = new byte[4096];
        int bytesRead;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        outputStream.flush();
        inputStream.close();

        writer.append(LINE_FEED);
        writer.flush();
    }

    public List<String> finish() throws IOException {
        List<String> response = new ArrayList<>();

        writer.append(LINE_FEED).flush();
        writer.append("--" + BOUNDARY + "--").append(LINE_FEED);
        writer.close();

        int status = httpConnection.getResponseCode();

        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                response.add(line);
            }

            reader.close();
            httpConnection.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        return response;
    }
}
