package it.unibo.disi.pslab.traumatracker.smartphoneapp.place_tools;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;



import it.unibo.disi.pslab.traumatracker.smartphoneapp.LocationService;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.logging.Logger;
import it.unibo.disi.pslab.traumatracker.smartphoneapp.utils.C;

public final class PushLocationDetector implements LocationDetector {

    private String url;
    private final String currentTagId;
    private WebSocketClient wsClient;

    private final int reconnectionTimeout;

    private LocationDetector.Listener listener = null;

    private Logger logger;

    public PushLocationDetector(final String url, final String currentTagId, final int reconnectionTimeout){
        this(url, currentTagId, reconnectionTimeout, null);
    }

    public PushLocationDetector(final String url, final String currentTagId, final int reconnectionTimeout, final Logger logger){
        this.url = url;
        this.logger = logger;
        this.currentTagId = currentTagId;
        this.reconnectionTimeout = reconnectionTimeout;
    }

    @Override
    public void startDetection() {
        startWSClient();
    }

    @Override
    public void stopDetection() {
        if(wsClient != null && wsClient.isOpen()){
            wsClient.close();
        }
    }

    @Override
    public void setOnPlaceUpdateAvailableListener(LocationDetector.Listener listener) {
        this.listener = listener;
    }

    private String detectCurrentPlace(final JSONArray places) throws JSONException {
        if(places.length() == 0){
            return C.place.TRANSPORT;
        }

        final JSONObject bestPlace = places.getJSONObject(0);

        if(bestPlace != null){
            return bestPlace.getString("name");
        } else {
            return C.place.TRANSPORT;
        }
    }

    private void startWSClient(){
        URI uri;

        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        wsClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(final ServerHandshake serverHandshake) {
                log("Websocket connected to " + url);
                LocationService.updateLocationDetectionStatus(Status.ACTIVE);
            }

            @Override
            public void onMessage(final String msg) {
                log("Received Message from Service: " + msg.replace("\n","").replace(" ", ""));

                if(listener != null){
                    try {
                        final JSONObject msgObj = new JSONObject(msg);

                        if(msgObj.getString("tagId").equals(currentTagId)){
                            listener.placeUpdate(detectCurrentPlace(msgObj.getJSONArray("places")));
                        } else {
                            log("Ignored a received message for an update of another tag!");
                        }
                    } catch (final JSONException e) {
                        log("Error in parsing received message. " + e.getMessage());
                    }
                }
            }

            @Override
            public void onClose(final int i, final String message, final boolean remotlyClosed) {

                log("Websocket Closed "+ (remotlyClosed ? "(Rejected by Remote Host) " : "") + ": " + message);

                LocationService.updateLocationDetectionStatus(Status.IDLE);

                if(remotlyClosed){
                    new Thread(() -> {
                        do{
                            startWSClient();

                            log("Web socket not reachable, retry in " + reconnectionTimeout + "ms");
                            try {
                                Thread.sleep(reconnectionTimeout);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        } while(!wsClient.isOpen());
                    }).start();
                }
            }

            @Override
            public void onError(final Exception e) {
                log("Websocket Error : " + e.getMessage());
                LocationService.updateLocationDetectionStatus(Status.IDLE);
            }
        };

        wsClient.connect();
    }

    public void sendMessage(final JSONObject msg) {
        if(wsClient != null && wsClient.isOpen()){
            wsClient.send(msg.toString());
        }
    }

    private void log(final String msg){
        if(logger != null){
            logger.writeOnLog(msg);
        }
    }
}
