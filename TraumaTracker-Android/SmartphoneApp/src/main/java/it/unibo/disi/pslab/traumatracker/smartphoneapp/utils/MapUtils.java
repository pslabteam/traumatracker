package it.unibo.disi.pslab.traumatracker.smartphoneapp.utils;

import android.util.Log;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class MapUtils {

    public static String serializeMap(Map<String, Object> map) {
        return map.toString();
    }

    public static Map<String, Object> deserializeMap(String mapContent){
        Log.d("MAP-CONTENT", mapContent);

        Properties props = new Properties();

        Map<String, Object> map = new HashMap<>();

        try {
            props.load(new StringReader(mapContent.substring(1, mapContent.length() - 1).replace(", ", "\n")));

            for (Map.Entry<Object, Object> e : props.entrySet()) {
                boolean isANumber = false;

                try{
                    int intValue = Integer.parseInt((String) e.getValue());
                    map.put((String) e.getKey(), intValue);
                    isANumber = true;
                } catch(NumberFormatException ignored){ }

                try{
                    double doubleValue = Double.parseDouble((String) e.getValue());
                    map.put((String) e.getKey(), doubleValue);
                    isANumber = true;
                } catch(NumberFormatException ignored){ }

                if(!isANumber) {
                    final String value = (String) e.getValue();

                    if(value.equals(Boolean.TRUE.toString())){
                        map.put((String) e.getKey(), true);
                    } else if(value.equals(Boolean.FALSE.toString())){
                        map.put((String) e.getKey(), false);
                    } else {
                        map.put((String) e.getKey(), value);
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }
}
