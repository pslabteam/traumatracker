package it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions;


public class MissingJsonElementException extends Exception {
    private static final long serialVersionUID = 1L;
    private static final String MESSAGE = "Errore:manca uno dei tre parametri essenziali"+
            "[ TEXT, PRIORITY,UPDATE]";

    @Override
    public String getMessage() {
        return MissingJsonElementException.MESSAGE;
    }
}
