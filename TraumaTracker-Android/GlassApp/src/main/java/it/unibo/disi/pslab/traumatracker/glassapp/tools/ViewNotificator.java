package it.unibo.disi.pslab.traumatracker.glassapp.tools;

import it.unibo.disi.pslab.traumatracker.glassapp.C;
import it.unibo.disi.pslab.traumatracker.glassapp.R;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.MainActivity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ViewNotificator {

	private static final ViewNotificator instance = new ViewNotificator();

	private static final int CAMERA_TIMEOUT = 3000; // ms
	private static final int NOTIFICATION_TIMEOUT = 1500; // ms
    private static final int MONITOR_TIMEOUT = 5000; // ms

	private MainActivity context = null;

	private Integer cameraTicket = 0;
	private Integer notificationTicket = 0;
    private Integer monitorTicket = 0;

	protected ViewNotificator() { }

	public static ViewNotificator getInstance() {
		return instance;
	}

	public void setContext(Context ctx) {
		context = (MainActivity) ctx;
	}

    public void setTraumaTrackerStatus(final String status){
        if (context == null) {
            return;
        }

        dispatchToMainThread(new Runnable(){
            @Override
            public void run() {
                switch(status){
                    case "active":
                        ((TextView) context.findViewById(R.id.tt_status))
                                .setText(C.TRACKING_STATUS_ACTIVE_LABEL);

                        //TODO: remove this line with SR active
                        ((TextView) context.findViewById(R.id.statusbar_text))
                                .setText("");
                        break;

                    case "paused":
                        ((TextView) context.findViewById(R.id.tt_status))
                                .setText(C.TRACKING_STATUS_PAUSED_LABEL);
                        break;

                    case "terminated":
                        ((TextView) context.findViewById(R.id.tt_status))
                                .setText(C.TRACKING_STATUS_END_LABEL);
                        break;

                    default:
                        ((TextView) context.findViewById(R.id.tt_status))
                                .setText("");
                        break;
                }
            }
        });
    }

    public void setPlace(final String place){
        dispatchToMainThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) context.findViewById(R.id.place))
                        .setText(place);
            }
        });
    }

	public void notifyCommandConfirmation(final String message) {
		if (context == null) {
			return;
		}

		dispatchToMainThread(new Runnable(){
			@Override
			public void run() {
//				((ImageView) context.findViewById(R.id.message_icon_view))
//					.setImageResource(R.drawable.ic_ok);
				
				((TextView) context.findViewById(R.id.message_text))
					.setText(message);
			}
		});

		final int ticket;

		synchronized (notificationTicket) {
			notificationTicket++;
			ticket = notificationTicket;
		}

		new Thread(new NotificationTimeoutWaiter(ticket)).start();
	}

	public void notifyCommandError(final String message) {
		if (context == null){
			return;
		}

		dispatchToMainThread(new Runnable() {
			@Override
			public void run() {
				((ImageView) context.findViewById(R.id.message_icon_view))
						.setImageResource(R.drawable.ic_no);

				((TextView) context.findViewById(R.id.message_text))
					.setText(message);
			}
		});

		final int ticket;

		synchronized (notificationTicket) {
			notificationTicket++;
			ticket = notificationTicket;
		}

		new Thread(new NotificationTimeoutWaiter(ticket)).start();
	}

    public void showMonitor(final int sys, final int dia, final int hr, final int etco2, final int spo2, final int temp) {
        dispatchToMainThread(new Runnable() {
            @Override
            public void run() {
                context.findViewById(R.id.monitor).setVisibility(View.VISIBLE);

                ((TextView)context.findViewById(R.id.vs_sys_value)).setText(String.valueOf(sys));
                ((TextView)context.findViewById(R.id.vs_dia_value)).setText(String.valueOf(dia));
                ((TextView)context.findViewById(R.id.vs_hr_value)).setText(String.valueOf(hr));
                ((TextView)context.findViewById(R.id.vs_etco2_value)).setText(String.valueOf(etco2));
                ((TextView)context.findViewById(R.id.vs_spo2_value)).setText(String.valueOf(spo2));
                ((TextView)context.findViewById(R.id.vs_temp_value)).setText(String.valueOf(temp));
            }
        });


        final int ticket;

        synchronized (monitorTicket) {
            monitorTicket++;
            ticket = monitorTicket;
        }

        new Thread(new MonitorTimeoutWaiter(ticket)).start();
    }

	public void notifyPictureTaken() {
		if (context == null){
			return;
		}

		dispatchToMainThread(new Runnable() {
			@Override
			public void run() {
				((ImageView) context.findViewById(R.id.camera_icon_view))
						.setImageResource(R.drawable.ic_camera);
			}
		});

		final int ticket;

		synchronized (cameraTicket) {
			cameraTicket++;
			ticket = cameraTicket;
		}

		new Thread(new CameraTimeoutWaiter(ticket)).start();
	}

	public void setSpeechRecognizerStatus(final boolean available){
		dispatchToMainThread(new Runnable() {
			@Override
			public void run() {
				if(available) {
                    context.findViewById(R.id.statusbar)
                            .setBackgroundResource(R.color.green);

                    ((TextView) context.findViewById(R.id.statusbar_text))
                            .setText(R.string.listening_message);
				} else {
                    context.findViewById(R.id.statusbar)
                            .setBackgroundResource(R.color.red);

                    ((TextView) context.findViewById(R.id.statusbar_text))
                            .setText(R.string.not_listening_message);
				}
			}
		});
	}

	private void dispatchToMainThread(Runnable runable) {
		new Handler(Looper.getMainLooper()).post(runable);
	}

    class CameraTimeoutWaiter implements Runnable {

		private int ticket;

		public CameraTimeoutWaiter(int ticket) {
			this.ticket = ticket;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(CAMERA_TIMEOUT);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			synchronized (cameraTicket) {
				if (cameraTicket == ticket) {
					dispatchToMainThread(new Runnable() {
						@Override
						public void run() {
							((ImageView) context.findViewById(R.id.camera_icon_view))
									.setImageResource(0);
						}
					});
				}
			}
		}
	}

	class NotificationTimeoutWaiter implements Runnable {
		
		private int ticket;

		public NotificationTimeoutWaiter(int ticket) {
			this.ticket = ticket;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(NOTIFICATION_TIMEOUT);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			synchronized (notificationTicket) {
				if (notificationTicket == ticket) {
					dispatchToMainThread(new Runnable() {
						@Override
						public void run() {
							((ImageView) context.findViewById(R.id.message_icon_view))
									.setImageResource(0);

							((TextView) context.findViewById(R.id.message_text))
									.setText("");
						}
					});
				}
			}
		}
	}

    class MonitorTimeoutWaiter implements Runnable{
        private int ticket;

        public MonitorTimeoutWaiter(int ticket){
            this.ticket = ticket;
        }

        @Override
        public void run(){
            try{
                Thread.sleep(MONITOR_TIMEOUT);

            } catch (InterruptedException e){
                e.printStackTrace();
            }

            synchronized (monitorTicket) {
                if (monitorTicket == ticket) {
                    dispatchToMainThread(new Runnable() {
                        @Override
                        public void run() {
                            context.findViewById(R.id.monitor).setVisibility(View.GONE);
                        }
                    });
                }
            }
        }
    }
}