package it.unibo.disi.pslab.traumatracker.glassapp.gui.activities;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.SurfaceView;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import it.unibo.disi.pslab.bluetoothlib.BluetoothClient;
import it.unibo.disi.pslab.bluetoothlib.utils.BluetoothError;
import it.unibo.disi.pslab.traumatracker.glassapp.C;
import it.unibo.disi.pslab.traumatracker.glassapp.R;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.base.GlassActivity;
import it.unibo.disi.pslab.traumatracker.glassapp.tasks.CameraTask;
import it.unibo.disi.pslab.traumatracker.glassapp.tasks.PushMediaTask;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.CameraManager;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.ViewNotificator;

public class MainActivity extends GlassActivity {

    private UiHandler uiHandler = new UiHandler(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();

        BluetoothClient.getInstance().registerEventListener(new BluetoothClientEventListener());
        BluetoothClient.getInstance().connect("AC:22:0B:68:17:8D", C.bt.BLUETOOTH_UUID);
    }

    @Override
    public void onBackPressed() {
        shutdown();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    protected void onDestroy() {
        shutdown();
        super.onDestroy();
    }

    private void shutdown(){
        BluetoothClient btClient = BluetoothClient.getInstance();

        if(btClient.isConnected()){
            btClient.disconnect();
        }
    }

    private void initUI(){
		ViewNotificator.getInstance().setContext(this);
		
		((SurfaceView) findViewById(R.id.mainActivitySurfaceView))
			.getHolder()
			.addCallback(CameraManager.getInstance());
		
		((ImageView) findViewById(R.id.message_icon_view))
                .setImageDrawable(null);
//              .setImageResource(0);
		
		((ImageView) findViewById(R.id.camera_icon_view))
                .setImageDrawable(null);
//			    .setImageResource(0);
	
		((TextView) findViewById(R.id.message_text)).setText("");

		findViewById(R.id.statusbar).setBackgroundResource(0);

		((TextView) findViewById(R.id.statusbar_text)).setText("");
	}

    /**
     * BluetoothClient Event Listner
     */
    private class BluetoothClientEventListener implements BluetoothClient.Events {
        @Override
        public void onConnectionAccepted() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) findViewById(R.id.statusbar_text)).setText("Device Connesso!");
                }
            });
        }

        @Override
        public void onConnectionRefused(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) findViewById(R.id.statusbar_text)).setText("Avviare l'applicazione sul Tablet per iniziare!");
                }
            });
        }

        @Override
        public void deviceNotFound(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) findViewById(R.id.statusbar_text)).setText("TT-Tablet non trovato nella lista dei dispositivi accoppiati. Procedere al pairing e riavviare l'app!");
                }
            });
        }

        @Override
        public void onMessageReceived(String message) {
            Message m = new Message();
            m.obj = message;
            uiHandler.sendMessage(m);
        }

        @Override
        public void onClientError(BluetoothError error) {

        }
    }

    /**
     * Handler for the Main User Interface
     */
    private static class UiHandler extends Handler {
        private final WeakReference<MainActivity> context;

        private BluetoothSocket socket;

        UiHandler(MainActivity context){
            this.context = new WeakReference<>(context);
        }

        void setSocket(BluetoothSocket socket){
            this.socket = socket;
        }

        public void handleMessage(Message msg) {
            try {
                JSONObject msgObj = new JSONObject(msg.obj.toString());
                JSONObject content =  msgObj.getJSONObject("content");

                switch(msgObj.getString("request")){
                    case C.MESSAGE_TYPE_CAMERA:
                        manageCameraRequest(msgObj.getJSONObject("request")); //TODO
                        break;

                    case C.MESSAGE_TYPE_PUSH_MEDIA:
                        //managePushMediaRequest(msgObj.getJSONObject("request")); //TODO
                        break;

                    case "update_tracking_status":
                        ViewNotificator.getInstance().setTraumaTrackerStatus(content.getString("status"));
                        break;

                    case "update_place_info":
                        ViewNotificator.getInstance().setPlace(content.getString("place"));
                        break;

                    case "show_message":
                        String type = content.getString("type");

                        if(type.equals("ok")){
                            ViewNotificator.getInstance().notifyCommandConfirmation(content.getString("message"));
                        }

                        if(type.equals("error")){
                            ViewNotificator.getInstance().notifyCommandError(content.getString("message"));
                        }
                        break;

                    case "show_monitor":
                        ViewNotificator.getInstance().showMonitor(content.getInt("sys"),
                                content.getInt("dia"), content.getInt("hr"), content.getInt("etco2"),
                                content.getInt("spo2"),content.getInt("temp"));
                        break;

                    case "update_speechrecognizer_status":
                        ViewNotificator.getInstance().setSpeechRecognizerStatus(content.getBoolean("listening"));
                        break;

                    default:
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void manageCameraRequest(JSONObject request) throws JSONException {
            new Thread(new CameraTask(socket, request.getString("cmd"), request.getString("key")))
                    .start();
        }

        private void managePushMediaRequest(JSONObject request) throws JSONException{
            switch (request.getString("cmd")) {
                case C.MESSAGE_PUSH_MEDIA:
                    new Thread(new PushMediaTask(socket))
                            .start();
                    break;

                default:
                    break;
            }
        }
    }
}
