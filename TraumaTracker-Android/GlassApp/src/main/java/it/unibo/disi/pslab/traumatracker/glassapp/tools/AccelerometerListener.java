package it.unibo.disi.pslab.traumatracker.glassapp.tools;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Handler;

public class AccelerometerListener implements SensorEventListener {

    private final int axis;
    private final boolean positiveAxis;
    private final double sensorNoise;
    private final Runnable toBeRunOnMainThread;
    private volatile boolean initialized = false;
    private float lastAxisValue = 0;
    private final Context context;
    private volatile boolean managed = false;

    public AccelerometerListener(Context context, int axis, boolean positiveAxis, double sensorNoise, Runnable toBeRunOnMainThread) {
        this.axis = axis;
        this.positiveAxis = positiveAxis;
        this.sensorNoise = sensorNoise;
        this.toBeRunOnMainThread = toBeRunOnMainThread;
        this.context = context;
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER && !managed) {
            if (!initialized) {
                lastAxisValue = event.values[axis];
                initialized = true;
            } else {
                float axisValue = event.values[axis];
                float deltaValue = lastAxisValue - axisValue;
                if ((deltaValue < -sensorNoise)) {
                    managed = true;
                    if (positiveAxis) {
                        ((Activity) context).runOnUiThread(toBeRunOnMainThread);
                    }
                } else if ((deltaValue > sensorNoise)) {
                    managed = true;
                    if (!positiveAxis) {
                        ((Activity) context).runOnUiThread(toBeRunOnMainThread);
                    }
                }
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initialized = false;
                        managed = false;
                    }
                }, 1000);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

}
