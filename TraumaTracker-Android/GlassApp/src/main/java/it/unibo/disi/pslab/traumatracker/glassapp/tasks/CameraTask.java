package it.unibo.disi.pslab.traumatracker.glassapp.tasks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import it.unibo.disi.pslab.bluetoothlib.BluetoothClient;
import it.unibo.disi.pslab.bluetoothlib.exceptions.BluetoothMessageTooBigException;
import it.unibo.disi.pslab.traumatracker.glassapp.C;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.CameraManager;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.ViewNotificator;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.exceptions.CameraBusyException;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.exceptions.VideoRecordingInactiveException;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.os.Environment;

/**
 * 
 * @author Angelo Croatti (a.croatti@unibo.it)
 *
 */
public class CameraTask implements Runnable, CameraManager.Callback{

	private String cmd;
	private String key;
	
	private BluetoothSocket socket;
	
	public CameraTask(BluetoothSocket socket, String cmd, String key) {
		this.cmd = cmd;
		this.key = key;
		this.socket = socket;
	}

	@Override
	public void run() {
		
		switch(cmd){
			case C.MESSAGE_TAKE_PICTURE:
				manageTakePictureRequest();
				break;
	
			case C.MESSAGE_START_RECORDING:
				manageStartRecordingRequest();
				break;
	
			case C.MESSAGE_STOP_RECORDING:
				manageStopRecordingRequest();
				break;
		}
	}
	
	private void manageTakePictureRequest() {
		try {
			CameraManager.getInstance().takePicture(this);
		} catch (CameraBusyException e) {
			e.printStackTrace();
		}
	}

	private void manageStartRecordingRequest() {
		try {
			CameraManager.getInstance().startRecording(this, "");
		} catch (CameraBusyException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void manageStopRecordingRequest() {
		try {
			CameraManager.getInstance().stopRecording();
		} catch (VideoRecordingInactiveException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPictureTaken(Bitmap picture) {
		
		ViewNotificator.getInstance().notifyPictureTaken();
		
		try {
			JSONObject response = new JSONObject();
			response.put("message", C.RESPONSE_MESSAGE_PICTURE_STORED);
			response.put("name", savePictureTaken(picture, new Date()));
			response.put("key", key);

			BluetoothClient.getInstance().sendMessage(response.toString());
		} catch (IOException | BluetoothMessageTooBigException | JSONException e) {
			e.printStackTrace();
		}
	}
	
	private String savePictureTaken(Bitmap image, Date now) throws IOException {
		String name = now.getTime() + ".jpeg";

		File file = new File(Environment.getExternalStorageDirectory() + C.fs.PICTURES_FOLDER_PATH + "/" + name);

		FileOutputStream fos = new FileOutputStream(file);
		image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
		fos.close();

		return name;
	}

	@Override
	public void onVideoRecorded(String path) {
		// TODO Auto-generated method stub
	}
}
