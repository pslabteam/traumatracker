package it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.base;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

import it.unibo.disi.pslab.traumatracker.glassapp.R;
import it.unibo.disi.pslab.traumatracker.glassapp.SingletonController;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.EventActivity;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.C_GLASSES_APP;

public class MyActivity extends GlassActivity {
    protected SingletonController sing;
    private volatile boolean doubleBackToExitPressedOnce = false;
    private volatile boolean keyEventManaged = false;
    private Handler hnd = new Handler();
    private Runnable r;

    @Override
    protected void onCreate(Bundle save) {
        super.onCreate(save);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart(){
        sing = SingletonController.getInstance(this);
        super.onStart();
    }

    /**
     * set all vital signs
     * @param vitalSigns map<String,String>, keys are vitalsign's name(contained in{VS_SYS, VS_DIA, VS_HR, VS_EtC02, VS_Temp,
     *                   VS_Sp02}.
     */

    public void setVitalSigns(final Map<String,String> vitalSigns) {
        setSYS(vitalSigns.get(C_GLASSES_APP.jsonVSNames.SYS));
        setDIA(vitalSigns.get(C_GLASSES_APP.jsonVSNames.DIA));
        setEtC02(vitalSigns.get(C_GLASSES_APP.jsonVSNames.EtC02));
        setHR(vitalSigns.get(C_GLASSES_APP.jsonVSNames.HR));
        setSp02(vitalSigns.get(C_GLASSES_APP.jsonVSNames.Sp02));
        setTemp(vitalSigns.get(C_GLASSES_APP.jsonVSNames.Temp));
    }

    private void setSYS(String SYS) {
        ((TextView) this.findViewById(R.id.SYS)).setText(SYS);
    }

    private void setDIA(String DIA) {
        ((TextView) this.findViewById(R.id.DIA)).setText(DIA);
    }

    private void setHR(String hr) {
        ((TextView) this.findViewById(R.id.HR)).setText(hr);
    }

    private void setEtC02(String EtC02) { ((TextView) this.findViewById(R.id.EtCO2)).setText(EtC02);}

    private void setSp02(String Sp02) {
        ((TextView) this.findViewById(R.id.Sp02)).setText(Sp02);
    }

    private void setTemp(String Temp) {
        ((TextView) this.findViewById(R.id.Temp)).setText(Temp);
    }

    /**
     * Set current room
     * @param room current room
     */
    public void setRoom(String room) {
        ((TextView) findViewById(R.id.roomTextView)).setText(room);
    }

    protected void setDoubleTapTimeStamp(View v) {
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyEventManaged) {
                    keyEventManaged = false;
                    return true;
                } else {
                    if (event.getKeyCode() == KeyEvent.KEYCODE_ESCAPE) {
                        shutdown();
                        keyEventManaged = true;
                        return true;
                    } else {
                        if (doubleBackToExitPressedOnce) {
                            Toast toast = Toast.makeText(SingletonController.getInUseActivity(), "Marca temporale acquisita", Toast.LENGTH_SHORT);
                            toast.show();
                            Thread tr = new Thread(new Runnable() {
                                @Override
                                public void run() {


                                /*------>Codice di acquisizione e invio della marca temporale<-----*/
                                }
                            });
                            tr.start();
                            keyEventManaged = true;
                            doubleBackToExitPressedOnce = false;
                            hnd.removeCallbacks(r);
                        } else {
                            doubleBackToExitPressedOnce = true;
                            r = new Runnable() {
                                @Override
                                public void run() {
                                    doubleBackToExitPressedOnce = false;
                                    SingletonController.getInUseActivity();
                                    if (SingletonController.getInUseActivity() instanceof EventActivity && !keyEventManaged) {
                                        shutdown();
                                    }
                                    /*
                                    if(SingletonController.getInUseActivity() instanceof MainIDLEActivity && !keyEventManaged){
                                        shutdown();
                                    }
                                    */
                                }
                            };
                            keyEventManaged = true;
                            hnd.postDelayed(r, C_GLASSES_APP.params.DOUBLE_CLICK_DELAY_TIME);
                        }
                        return true;
                    }
                }
            }
        });
        v.requestFocus();
    }

    /**
     * Use this method to close activity, do not use Activity.finish()
     */
    public void shutdown(){
        sing.restoreOld();
        finish();
    }

    @Override
    public void onBackPressed(){
        shutdown();
    }

}
