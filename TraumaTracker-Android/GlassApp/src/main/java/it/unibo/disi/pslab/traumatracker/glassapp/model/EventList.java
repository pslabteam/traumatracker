package it.unibo.disi.pslab.traumatracker.glassapp.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions.NoMoreEventException;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.C_GLASSES_APP;

public class EventList extends LinkedList<Event> {

    public EventList() {
    }

    public EventList(Event base) {
        this.add(base);
    }

    /**
     * Add an element to the list, ordered by priority. If event has to be updated, then update
     * eventual occurrence.
     *
     * @param event, Event to be added
     * @return true if event has been successfully added, false otherwise
     */

    public boolean addEventOrderedByPriority(final Event event) {
        Log.d("NEW EVENT:",event.toString());
        for (Event t : this) {
            if (t.getType().equals(event.getType()) && (!event.getType().equals(C_GLASSES_APP.jsonDefaultTypes.NO_TYPE))) {
                if (t.isToBeUpdate()) {
                    this.set(this.indexOf(t), event);
                    return true;
                }
            }
        }
        int i = 0;
        Iterator<Event> it = this.iterator();
        while (it.hasNext()) {
            Event tmp = it.next();
            if (tmp.getPriority() < event.getPriority() || !it.hasNext()) {
                this.add(i, event);
                return true;
            } else {
                i++;
            }
        }
        return false;
    }

    /**
     * Add a collection of element to the list, ordered by priority. If one or more event have to be updated, then update all
     * eventually occurrences.
     *
     * @param eventCollection, Collection of Events to be added
     * @return true if all events have been successfully added, false otherwise
     */
    public boolean addEventsCollectionOrderedByPriority(Collection<Event> eventCollection) {
        boolean t;
        for (Event e : eventCollection) {
            t = addEventOrderedByPriority(e);
            if (t == false) {
                return t;
            }
        }
        return true;
    }

    /**
     * @return true if next Event is an alarm or a warning; false if no other event are available in the list:
     */
    public boolean checkEvent() {
        return !this.getFirst().getType().equals(C_GLASSES_APP.jsonDefaultTypes.BASE_EVENT_LIST);
    }

    /**
     * next available Event and remove it from the list, before using this method use checkEvent()
     *
     * @return next available Event
     */
    public Event getNextEvent() throws NoMoreEventException {
        if (!this.getFirst().getType().equals(C_GLASSES_APP.jsonDefaultTypes.BASE_EVENT_LIST)) {
            Event tmp = this.getFirst();
            this.remove();
            return tmp;
        } else {
            throw new NoMoreEventException();
        }
    }

    /**
     * remove from EventList all occurences of a specified event's type.
     *
     * @param obj a JSONObject of type C_GLASS_APP.jsonDefaultType.RESOLVE
     */
    public void removeEventMatchType(final JSONObject obj) {
        String type = null;
        try {
            type = obj.getString(C_GLASSES_APP.jsonParams.RESOLVED_EVENT_TYPE);
            for (int i = 0; i < this.size(); i++) {
                if (this.get(i).getType().equals(type)) {
                    this.remove(i);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
