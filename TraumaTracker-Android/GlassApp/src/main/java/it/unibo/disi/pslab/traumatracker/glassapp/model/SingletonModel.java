package it.unibo.disi.pslab.traumatracker.glassapp.model;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions.MissingJsonElementException;
import it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions.NoMoreEventException;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.C_GLASSES_APP;

public class SingletonModel {

    private static SingletonModel mInstance = new SingletonModel();
    private static EventList list;
    private static JSONObject jsonObjectFile = null;
    private static HashMap<String, String> vitalSigns;
    private static String room = "nessuna stanza";
    private static Context context;

    private SingletonModel() {
        vitalSigns = new HashMap<>();
        vitalSigns.put(C_GLASSES_APP.jsonVSNames.SYS, Integer.toString(0));
        vitalSigns.put(C_GLASSES_APP.jsonVSNames.DIA, Integer.toString(0));
        vitalSigns.put(C_GLASSES_APP.jsonVSNames.HR, Integer.toString(0));
        vitalSigns.put(C_GLASSES_APP.jsonVSNames.EtC02, Integer.toString(0));
        vitalSigns.put(C_GLASSES_APP.jsonVSNames.Sp02, Integer.toString(0));
        vitalSigns.put(C_GLASSES_APP.jsonVSNames.Temp, Integer.toString(0));
    }

    /**
     * initialize model providing a context
     *
     * @param c
     */
    public static void initializeModel(Context c) {
        context = c;
        if (list == null || list.isEmpty()) {
            list = new EventList((new Event.EventBuilder()).setEventType(C_GLASSES_APP.jsonDefaultTypes.BASE_EVENT_LIST)
                    .setPriority(1).setText("No Alarm").Build());
        }
    }

    /**
     * return singleton instance
     *
     * @return
     */
    public static SingletonModel getInstance() {
        return SingletonModel.mInstance;
    }

    /**
     * return current vital signs as a Map<VS_Name, values>
     *
     * @return curretn vital signs
     */
    public Map<String, String> getVsSign() {
        return vitalSigns;
    }

    /**
     * update vital signs providing a JSONObject. To easy build the JSONObject
     * use getWellKnowEvent().getJSONObject(C_GLASSES_APP.jsonDefaultTypes.UPDATE_VS).
     *
     * @param js JSONObject of type C_GLASS_APP.jsonDefaultTypes.UPDATE_VS
     * @throws JSONException
     */
    public void updateVsSign(JSONObject js) throws JSONException {
        Map<String, String> tmp = vitalSigns;
        for (String i : tmp.keySet()) {
            if (!js.isNull(i)) {
                vitalSigns.put(i, js.getString(i));
            }
        }
    }

    /**
     * update vital signs providing a Map<String,String> containing VS name and values
     *
     * @param vs Map containing VS you want to update. keys are VS's name, values are VS's values
     */
    public void updateVsSigns(Map<String, String> vs) {
        Map<String, String> tmp = vitalSigns;
        for (String i : tmp.keySet()) {
            if (vs.containsKey(i)) {
                vitalSigns.put(i, vs.get(i));
            }
        }
    }

    /**
     * set current room
     *
     * @param room
     */
    public void setRoom(String room) {
        SingletonModel.room = room;
    }

    /**
     * return current room
     *
     * @return
     */
    public String getRoom() {
        return room;
    }

    /**
     * return a JSONObject containing all "well know" Event
     *
     * @return
     */
    public JSONObject getWellKnowEvent() {
        if (jsonObjectFile == null) {
            try {
                String json = null;
                InputStream is = context.getAssets().open("wellKnowEvent.json");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, StandardCharsets.UTF_8);
                jsonObjectFile = new JSONObject(json);
                return jsonObjectFile;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonObjectFile;
    }

    /**
     * return current context
     *
     * @return
     */
    public Context getContext() {
        return context;
    }

    public EventList getEventList(){
        return list;
    }

}
