package it.unibo.disi.pslab.traumatracker.glassapp;

import android.content.Intent;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import it.unibo.disi.pslab.bluetoothlib.BluetoothClient;
import it.unibo.disi.pslab.bluetoothlib.utils.BluetoothError;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.EventActivity;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.MainIDLEActivity;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.base.MyActivity;
import it.unibo.disi.pslab.traumatracker.glassapp.model.SingletonModel;
import it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions.NoMoreEventException;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.C_GLASSES_APP;
import it.unibo.disi.pslab.traumatracker.glassapp.model.Event;
import it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions.MissingJsonElementException;

public class SingletonController {

    private static SingletonController mInstance = new SingletonController();//istanza unica del thread

    private static MyActivity inUseActivity = null;//inUseActivity in foreground
    private static MyActivity baseActivity = null;//inUseActivity che era prima in foreground
    private static Event inUseEvent = null;
    private static SingletonModel model;
    public static BluetoothClientEventListener ev;
    private static volatile boolean messageMenaged=true;
    private SingletonController() {
        ev = this.new BluetoothClientEventListener();
       /* BluetoothClient.getInstance().registerEventListener(ev);
        BluetoothClient.getInstance().connect(C.bt.BLUETOOTH_SERVER_NAME, C.bt.BLUETOOTH_UUID);
        */
        model = SingletonModel.getInstance();

    }

    /**
     * return  BluetoothClientEventListener instance
     * @return
     */
    public static BluetoothClientEventListener getListener() {
        return ev;
    }

    /**
     * return singleton instance. you must provide an activity istance.
     * if you call this method inside an Activity(onCreate() callback) use: getInstance(this),
     * otherwise use: getInstance(SingletonController.getInUseActivity()).
     * @param act
     * @return
     */
    public static SingletonController getInstance(MyActivity act) {
        if (act instanceof MainIDLEActivity) {
            inUseActivity = act;
            baseActivity = act;
            SingletonModel.initializeModel(baseActivity);
        } else {
            if (!(inUseActivity instanceof MainIDLEActivity)) {
                inUseActivity.shutdown();
            }
            inUseActivity = act;
        }
        inUseActivity.setVitalSigns(model.getVsSign());
        inUseActivity.setRoom(model.getRoom());
        return mInstance;
    }

    /**
     * return current foreground activity
     * @return
     */
    public static MyActivity getInUseActivity() {
        return inUseActivity;
    }

    private boolean checkAlarmAndShow() {
        inUseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (model.getEventList().checkEvent() &&
                        !(inUseActivity instanceof EventActivity)) {
                    try {
                        inUseEvent = model.getEventList().getNextEvent();
                    } catch (NoMoreEventException e) {
                        e.printStackTrace();
                    }
                    Intent k = new Intent(inUseActivity, EventActivity.class);
                    inUseActivity.startActivity(k);
                }
            }
        });
        return true;
    }

    private boolean handleAlarmMessage(final JSONObject js) throws MissingJsonElementException, JSONException {
        Event e = Event.buildEventFromJsonObj(js);
        if ((inUseActivity instanceof EventActivity) && e.getType().equals(inUseEvent.getType())
                && !e.getType().equals(C_GLASSES_APP.jsonDefaultTypes.NO_TYPE) && inUseEvent.isToBeUpdate()) {
            inUseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ((EventActivity) inUseActivity).setAlarm(Event.buildEventFromJsonObj(js));
                    } catch (MissingJsonElementException e) {
                        e.printStackTrace();
                    }
                }
            });
            inUseEvent = Event.buildEventFromJsonObj(js);
        } else {
            model.getEventList().addEventOrderedByPriority(Event.buildEventFromJsonObj(js));
        }
        return checkAlarmAndShow();
    }

    private boolean handleNoAlarmMessage(final JSONObject js) throws JSONException {
        if (!js.isNull(C_GLASSES_APP.jsonParams.TYPE)) {
            switch (js.getString(C_GLASSES_APP.jsonParams.TYPE)) {
                case (C_GLASSES_APP.jsonDefaultTypes.UPDATE_VS):
                    model.updateVsSign(js.getJSONObject(C_GLASSES_APP.jsonVSNames.VS));
                    inUseActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            inUseActivity.setVitalSigns(model.getVsSign());
                        }
                    });
                    return true;
                case (C_GLASSES_APP.jsonDefaultTypes.SET_ROOM):
                    model.setRoom(js.getString(C_GLASSES_APP.jsonParams.ROOM));
                    inUseActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            inUseActivity.setRoom(model.getRoom());
                        }
                    });
                    return true;
                case (C_GLASSES_APP.jsonDefaultTypes.RESOLVE):
                    model.getEventList().removeEventMatchType(js);
                    if ((inUseActivity instanceof EventActivity)
                            && inUseEvent.getType().equals(js.getString(C_GLASSES_APP.jsonParams.RESOLVED_EVENT_TYPE))) {
                        inUseActivity.shutdown();
                        restoreOld();
                        inUseActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(inUseActivity, "Allarme risolto", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    checkAlarmAndShow();
                    return true;
                default:
                    return true;
            }
        }
        return false;
    }

    /**
     * call this method into Activity closing procedure if that activity has previously called
     * SingletonController.getInstance(Activity act)
     */
    public void restoreOld() {
        if (!(inUseActivity instanceof MainIDLEActivity)) {
            inUseActivity = baseActivity;
        }
        checkAlarmAndShow();
    }

    /**
     * Stop handling message coming from bluetooth
     */
    public void stopBTHandlingMessage() {
        BluetoothClient btClient = BluetoothClient.getInstance();
        if (btClient.isConnected()) {
            btClient.disconnect();
        }
    }

    /**
     * return current displaying alarm, or last displayed alarm(if current activity is not an EvenActivity instance)
     * @return
     */
    public Event getInUseEvent() {
        return inUseEvent;
    }

    public class BluetoothClientEventListener implements BluetoothClient.Events {

        @Override
        public void onConnectionAccepted() {
            inUseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast t = Toast.makeText(inUseActivity, "Device Connesso", Toast.LENGTH_SHORT);
                    t.show();
                }
            });
        }

        @Override
        public void onConnectionRefused() {
            inUseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast t = Toast.makeText(inUseActivity, "Avviare l'applicazione sul Tablet per iniziare!", Toast.LENGTH_SHORT);
                    t.show();
                }
            });
        }

        @Override
        public void deviceNotFound() {
            inUseActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    Toast t = Toast.makeText(inUseActivity, "TT-Tablet non trovato nella lista dei dispositivi accoppiati. Procedere al pairing e riavviare l'app!", Toast.LENGTH_SHORT);
                    t.show();
                }
            });
        }

        @Override
        public void onMessageReceived(String message) {
            try {
                if(messageMenaged){
                    messageMenaged=false;
                    JSONObject js = new JSONObject(message);
                    if (js.isNull(C_GLASSES_APP.jsonParams.NOT_ALARM_MESSAGE)) {
                        messageMenaged= handleAlarmMessage(js);
                    } else {
                        messageMenaged=handleNoAlarmMessage(js);
                    }
                }
                else{
                    onMessageReceived(message);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (MissingJsonElementException missingJsonElement) {
                missingJsonElement.printStackTrace();
            }

        }

        @Override
        public void onClientError(BluetoothError error) {

        }
    }

}
