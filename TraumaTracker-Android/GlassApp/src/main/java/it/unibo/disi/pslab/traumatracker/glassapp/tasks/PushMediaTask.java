package it.unibo.disi.pslab.traumatracker.glassapp.tasks;

import android.bluetooth.BluetoothSocket;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.glassapp.C;

public class PushMediaTask implements Runnable {

    private BluetoothSocket socket;

    public PushMediaTask(BluetoothSocket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        pushAllMediaToServer();
    }

    private void pushAllMediaToServer() {
        final List<String> picturesPath = getAllPictureStored();

        Log.d(C.LOG_TAG,"N.FILE = " + picturesPath.size());

        for(String path : picturesPath){
            Log.d(C.LOG_TAG,"FILE = " + path);
            pushMedia(path);
        }
    }

    private void pushMedia(String path) {
        //TODO: push media in "path" to server
    }

    private List<String> getAllPictureStored(){
        List<String> list = new ArrayList<>();

        File pictureDir = new File(Environment.getExternalStorageDirectory() + C.fs.PICTURES_FOLDER_PATH);

        for (File f : pictureDir.listFiles()){
            list.add(f.getName());
        }

        return list;
    }
}
