package it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions;

/**
 * Created by giovanni on 25/05/17.
 */

public class NoMoreEventException extends Exception {
    private static final long serialVersionUID = 1L;
    private static final String msg = "Errore:manca uno dei tre parametri essenziali"+
            "[ TEXT, PRIORITY,UPDATE]";

    @Override
    public String getMessage() {
        return NoMoreEventException.msg;
    }
}
