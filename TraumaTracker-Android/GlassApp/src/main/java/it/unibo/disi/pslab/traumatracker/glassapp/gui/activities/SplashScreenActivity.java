package it.unibo.disi.pslab.traumatracker.glassapp.gui.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import it.unibo.disi.pslab.traumatracker.glassapp.R;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.base.GlassActivity;

public class SplashScreenActivity extends GlassActivity {

    private static final int SPLASH_SCREEN_TIMEOUT = 3000; //ms

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                startActivity(new Intent(SplashScreenActivity.this, MainIDLEActivity.class));
                finish();
            }
        }, SPLASH_SCREEN_TIMEOUT);
    }

    private void initUI() {
        try {
            ((TextView) findViewById(R.id.description)).setText(getString(R.string.app_name)
                    + " (ver. " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName + ")");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
