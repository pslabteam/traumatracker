package it.unibo.disi.pslab.traumatracker.glassapp.gui.activities;

import android.os.Bundle;
import android.view.View;

import it.unibo.disi.pslab.traumatracker.glassapp.R;
import it.unibo.disi.pslab.traumatracker.glassapp.ThreadDemo;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.base.MyActivity;

public class MainIDLEActivity extends MyActivity {

    private static boolean a = false;
    private static ThreadDemo t = new ThreadDemo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainidleactivity_layout);
        View v = findViewById(R.id.idle);
        super.setDoubleTapTimeStamp(v);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!a) {
            t.start();
            a = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onDestroy() {
        shutdown();
        super.onDestroy();
    }

    @Override
    public void shutdown() {
        sing.stopBTHandlingMessage();
        t.setStopped();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

}
