package it.unibo.disi.pslab.traumatracker.glassapp.tools.exceptions;

/**
 * 
 * @author Angelo Croatti (a.croatti@unibo.it)
 *
 */
public class CameraBusyException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CameraBusyException(){
		super();
	}
}
