 package it.unibo.disi.pslab.traumatracker.glassapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import it.unibo.disi.pslab.traumatracker.glassapp.model.SingletonModel;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.C_GLASSES_APP;


/**
 * Created by giovanni on 17/05/17.
 */



/*/


            DISMISS ME IN MainIDLEActivity !


 */

public class ThreadDemo extends Thread {
    SingletonController.BluetoothClientEventListener e= SingletonController.getListener();

    int i=0;
    private volatile boolean ciao;


    @Override public void run() {
        super.run();
        ciao = false;
    try {
        while (!ciao) {

            i++;
            Log.d("COUNTER", Integer.toString(i)+SingletonController.getInUseActivity());

            try {
                JSONObject j = SingletonModel.getInstance().getWellKnowEvent().getJSONObject(C_GLASSES_APP.jsonDefaultTypes.UPDATE_VS);
                JSONObject k = new JSONObject();
                k.put(C_GLASSES_APP.jsonVSNames.SYS, Integer.toString(i % 2));
                k.put(C_GLASSES_APP.jsonVSNames.DIA, Integer.toString(i % 3));
                k.put(C_GLASSES_APP.jsonVSNames.EtC02, Integer.toString(i % 4));
                k.put(C_GLASSES_APP.jsonVSNames.HR, Integer.toString(i % 5));
                k.put(C_GLASSES_APP.jsonVSNames.Sp02, Integer.toString(i % 6));
                k.put(C_GLASSES_APP.jsonVSNames.Temp, Integer.toString(i % 7));
                j.put(C_GLASSES_APP.jsonVSNames.VS, k);
                e.onMessageReceived(j.toString());
                // sleep(1000);//aspetto 1 secondo
                sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (i%10 == 0) {
                JSONObject alarm = new JSONObject();
                try {
                    alarm = SingletonModel.getInstance().getWellKnowEvent().getJSONObject("VS_SYS");
                    alarm.put(C_GLASSES_APP.jsonMandatoryParams.PRIORITY, 3);
                    alarm.put(C_GLASSES_APP.jsonMandatoryParams.TEXT, "allarme, con priorità: " +
                            alarm.get(C_GLASSES_APP.jsonMandatoryParams.PRIORITY) + "\n" +
                            "testo esteso?" + alarm.has(C_GLASSES_APP.jsonParams.EXTEND_TEXT));
                    List<String> tmp = Arrays.asList("Questo è un'allarme esteso", "", "notare che le stringhe vengono formatate" + "" +
                            "rispettando sia gli \"a capo\" che le righe vuote");
                    JSONArray jAR = new JSONArray();
                    for (Iterator<String> k = tmp.iterator(); k.hasNext(); ) {
                        jAR.put(k.next());
                    }
                    alarm.put(C_GLASSES_APP.jsonParams.EXTEND_TEXT, jAR);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    e.onMessageReceived(alarm.toString());
                } catch (Exception missingJsonElement) {
                    missingJsonElement.printStackTrace();
                }
            }
            if(i%20==0){
                JSONObject alarm;
                alarm=SingletonModel.getInstance().getWellKnowEvent().getJSONObject(C_GLASSES_APP.jsonDefaultTypes.RESOLVE);
                alarm.put(C_GLASSES_APP.jsonParams.RESOLVED_EVENT_TYPE, "VS_SYS");
                e.onMessageReceived(alarm.toString());

            }
            if(i%17==0){
                JSONObject alarm;
                alarm=SingletonModel.getInstance().getWellKnowEvent().getJSONObject(C_GLASSES_APP.jsonDefaultTypes.SET_ROOM);
                alarm.put(C_GLASSES_APP.jsonParams.ROOM, "Shock-Room");
                e.onMessageReceived(alarm.toString());
            }/*
            if(i%5==0){
                JSONObject al=new JSONObject();
                al.put(C_GLASSES_APP.jsonMandatoryParams.TEXT, "new Type event");
                al.put(C_GLASSES_APP.jsonMandatoryParams.PRIORITY, 10);
                al.put(C_GLASSES_APP.jsonMandatoryParams.UPDATE, true);
                al.put(C_GLASSES_APP.jsonParams.TYPE, "new_Type");
                Log.d("OOO", al.toString());
                e.onMessageReceived(al.toString());
            }*/

            //TEST 2:
            //-wellKnowEvent
            //-priorità non modificata rispetto al json già definito
            //-testo esteso non presente

            if (i %  11== 0) {
                JSONObject alarm = new JSONObject();
                try {
                    alarm = SingletonModel.getInstance().getWellKnowEvent().getJSONObject("VS_HR");
                    alarm.put(C_GLASSES_APP.jsonMandatoryParams.TEXT, "allarme, con priorità: " +
                            alarm.get(C_GLASSES_APP.jsonMandatoryParams.PRIORITY) + "\n" +
                            "testo esteso?" + (alarm.has(C_GLASSES_APP.jsonParams.EXTEND_TEXT)));
                } catch (JSONException e) {
                    e.printStackTrace();
                    alarm = new JSONObject();
                }
                try {
                   e.onMessageReceived(alarm.toString());
                } catch (Exception k) {
                    k.printStackTrace();
                }
            }


        }
    }catch(Exception e){
        e.printStackTrace();
    }
    }
    public void setStopped(){
        this.ciao=false;
    }
}


