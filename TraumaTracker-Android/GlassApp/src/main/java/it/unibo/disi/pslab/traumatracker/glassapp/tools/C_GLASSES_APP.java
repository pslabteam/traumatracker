package it.unibo.disi.pslab.traumatracker.glassapp.tools;

/**
 * Created by giovanni on 10/05/2017.
 */

public class C_GLASSES_APP {

    public class params {
        /*working params*/
        public static final long DOUBLE_CLICK_DELAY_TIME = 500;
        public static final double ACCELEROMETER_NOISE = 1.7;
    }

    /**
     * Event mandatory params
     */
    public class jsonMandatoryParams{
        public final static String TEXT = "text";
        public final static String PRIORITY = "priority";
        public final static String UPDATE = "update";
    }

    /**
     * Event non-mandatory params
     */
    public class jsonParams{
        public final static String TYPE = "type";
        public final static String DRAWABLE = "drawable";
        public final static String EXTEND_TEXT = "extendedText";
        public final static String NOT_ALARM_MESSAGE="not_alarm";
        public final static String ROOM="room";
        public final static String RESOLVED_EVENT_TYPE="resolved_event_type";
    }

    /**
     * Default Event
     */
    public class jsonDefaultTypes{
        public final static String SET_ROOM="setRoom";
        public final static String UPDATE_VS="update_vs";
        public final static String RESOLVE="resolve";
        public final static String NO_TYPE = "GENERIC";
        public final static String BASE_EVENT_LIST = "NO_AL";
        public final static String VS_SYS="VS_SYS";
        public final static String VS_DIA="VS_DIA";
        public final static String VS_HR="VS_HR";
        public final static String VS_EtC02="VS_EtC02";
        public final static String VS_Temp="VS_Temp";
        public final static String VS_Sp02="VS_Sp02";

    }

    /**
     * Vital signs names
     */
    public class jsonVSNames{
        public final static String VS="VS";
        public final static String SYS="SYS";
        public final static String DIA="DIA";
        public final static String HR="HR";
        public final static String EtC02="EtC02";
        public final static String Temp="Temp";
        public final static String Sp02="Sp02";
    }


}
