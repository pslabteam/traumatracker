package it.unibo.disi.pslab.traumatracker.glassapp;


import android.app.Application;
import android.os.Environment;

import java.io.File;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        checkMediaFolders();
    }

    private void checkMediaFolders() {
        String[] dirs = new String[] {
                C.fs.MEDIA_FOLDER_PATH,
                C.fs.PICTURES_FOLDER_PATH,
                C.fs.VIDEOS_FOLDER_PATH
        };

        for(String path : dirs) {
            File dir = new File(Environment.getExternalStorageDirectory() + path);
            if (!dir.exists()) {
                dir.mkdir();
            }
        }
    }
}
