package it.unibo.disi.pslab.traumatracker.glassapp.tools;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.unibo.disi.pslab.traumatracker.glassapp.C;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.exceptions.CameraBusyException;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.exceptions.VideoRecordingInactiveException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * 
 * @author Angelo Croatti (a.croatti@unibo.it)
 *
 */
public class CameraManager implements SurfaceHolder.Callback {
	
	private Camera camera;
	private MediaRecorder recorder;
	
	private SurfaceHolder holder;
	
	private static boolean busy;
	private static boolean recording;
	private String video_destination_path;
	
	private CameraManager.Callback callbackReceiver;
	
	private static CameraManager cameraManager;
	
	private CameraManager() {
		busy = false;
		recording = false;
	}
	
	public static CameraManager getInstance(){
		if(cameraManager == null)
			cameraManager = new CameraManager();
		
		return cameraManager;
	}

	public synchronized void takePicture(CameraManager.Callback receiver) throws CameraBusyException {
		if(busy)
			throw new CameraBusyException();
		
		busy = true;
		
		callbackReceiver = receiver;

		camera = Camera.open();

		try {
			camera.setPreviewDisplay(holder);
		} catch (IOException exception) {
			camera.release();
			camera = null;
		}

		Parameters parameters = camera.getParameters();
		camera.setParameters(parameters);

		camera.startPreview();

		camera.takePicture(null, null, new Camera.PictureCallback() {
			@Override
			public void onPictureTaken(byte[] data, Camera camera) {
				Log.d(C.LOG_TAG, "pictureTaken");
				Bitmap image = BitmapFactory.decodeByteArray(data, 0,
						data.length);

				camera.stopPreview();
				camera.release();
				camera = null;
				
				busy = false;
				
				callbackReceiver.onPictureTaken(image);
			}
		});
	}
	
	public synchronized void startRecording(CameraManager.Callback receiver, String videoFolderPath) throws CameraBusyException, IOException{
		if(busy)
			throw new CameraBusyException();
		
		File dir = new File(videoFolderPath);
		if(!(dir.exists() && dir.isDirectory()))
		  throw new IOException();
		
		busy = true;
		
		callbackReceiver = receiver;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);		
		video_destination_path = videoFolderPath + "/video_" + sdf.format(new Date())+ ".mp4";
		
		recorder = new MediaRecorder();
		
		recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        recorder.setOutputFile(video_destination_path);
        recorder.setMaxDuration(0); //no limits
        recorder.setMaxFileSize(5000000); //50MB
        
        recorder.setPreviewDisplay(holder.getSurface());

        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        recording = true;
        
        recorder.start();
	}
	
	public synchronized void stopRecording() throws VideoRecordingInactiveException {
		if(!recording)
			throw new VideoRecordingInactiveException();
		
		recorder.stop();
        
		recorder.release();
		
		recording = false;
        busy = false;
        
        callbackReceiver.onVideoRecorded(video_destination_path);
	}
	
	/*
	 * ------------------------------------------------------------------------
	 * ---- SURFACE HOLDER CALLBACKS ------------------------------------------
	 * ------------------------------------------------------------------------
	 */
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		this.holder = holder;
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {}
	
	public interface Callback {
		void onPictureTaken(Bitmap picture);
		void onVideoRecorded(String path);
	}
}
