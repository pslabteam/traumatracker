package it.unibo.disi.pslab.traumatracker.glassapp.model;

import android.graphics.drawable.Drawable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.unibo.disi.pslab.traumatracker.glassapp.model.exceptions.MissingJsonElementException;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.C_GLASSES_APP;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Event {

    private final String text;
    private final String type;
    private final int priority;
    private final List<String> extendedText;
    private final boolean isExtendedTextPresent;
    private final Drawable drawable;
    private final boolean isToBeUpdate;

    private Event(String text, String type, List<String> extendedText,
                  Drawable dr, int priority, boolean isToBeUpdate) {
        this.text = text;
        this.type = type;
        this.extendedText = extendedText;
        this.isExtendedTextPresent = extendedText.isEmpty();
        this.drawable = dr;
        this.priority = priority;
        this.isToBeUpdate = isToBeUpdate;
    }

    /**
     * Build an Event from a provided JSONObject, mandatory JSONElements(obj.has(element) must be true) are:
     * -C_GLASSES_APP.jsonMandatoryParams.TEXT
     * -C_GLASSES_APP.jsonMandatoryParams.PRIORITY
     * -C_GLASSES_APP.jsonMandatoryParams.UPDATE
     *
     * @param obj
     * @return
     * @throws MissingJsonElementException if at least one of mandatory JSONElement is missing(obj.has(element) is false)
     */
    public static Event buildEventFromJsonObj(JSONObject obj) throws MissingJsonElementException {
        EventBuilder builder = new Event.EventBuilder();
        try {
            if ((obj.isNull(C_GLASSES_APP.jsonMandatoryParams.TEXT)) || (obj.get(C_GLASSES_APP.jsonMandatoryParams.UPDATE) == null)
                    || (obj.get(C_GLASSES_APP.jsonMandatoryParams.PRIORITY) == null)) {
                throw new MissingJsonElementException();
            }
            if (!obj.isNull(C_GLASSES_APP.jsonParams.TYPE)) {
                Iterator pm = SingletonModel.getInstance().getWellKnowEvent().keys();
                while (pm.hasNext()) {
                    JSONObject k = (SingletonModel.getInstance().getWellKnowEvent().getJSONObject((String) pm.next()));
                    if (!k.isNull(C_GLASSES_APP.jsonParams.TYPE) &&
                            k.getString(C_GLASSES_APP.jsonParams.TYPE).equals(obj.getString(C_GLASSES_APP.jsonParams.TYPE))) {
                        //[OBBLIGATORI]
                        //type, obbligatorio solo perchè c'è una corrispondenza con una delle regole
                        builder.setEventType(obj.getString(C_GLASSES_APP.jsonParams.TYPE));
                        //Text
                        if (!obj.isNull(C_GLASSES_APP.jsonMandatoryParams.TEXT)) {
                            builder.setText(obj.getString(C_GLASSES_APP.jsonMandatoryParams.TEXT));
                        } else if (!k.isNull(C_GLASSES_APP.jsonMandatoryParams.TEXT)) {
                            builder.setText(k.getString(C_GLASSES_APP.jsonMandatoryParams.TEXT));
                        }
                        //Priority
                        if (!obj.isNull(C_GLASSES_APP.jsonMandatoryParams.PRIORITY)) {
                            builder.setPriority(obj.getInt(C_GLASSES_APP.jsonMandatoryParams.PRIORITY));
                        } else if (!k.isNull(C_GLASSES_APP.jsonMandatoryParams.PRIORITY)) {
                            builder.setPriority(k.getInt(C_GLASSES_APP.jsonMandatoryParams.PRIORITY));
                        }
                        //Update
                        if (!obj.isNull(C_GLASSES_APP.jsonMandatoryParams.UPDATE)) {
                            builder.setIsToBeUpdate(obj.getBoolean(C_GLASSES_APP.jsonMandatoryParams.UPDATE));
                        } else if (!k.isNull(C_GLASSES_APP.jsonMandatoryParams.UPDATE)) {
                            builder.setIsToBeUpdate(k.getBoolean(C_GLASSES_APP.jsonMandatoryParams.UPDATE));
                        }

                        //[FACOLTATIVI]
                        //drawable
                        if (!obj.isNull(C_GLASSES_APP.jsonParams.DRAWABLE)) {
                            Drawable d = SingletonModel.getInstance().getContext().getResources().getDrawable(SingletonModel.getInstance().getContext().getResources()
                                    .getIdentifier(obj.getString(C_GLASSES_APP.jsonParams.DRAWABLE), "drawable",
                                            SingletonModel.getInstance().getContext().getPackageName()));
                            builder.setDrawable(d);
                        } else if (!k.isNull(C_GLASSES_APP.jsonParams.DRAWABLE)) {
                            Drawable d = SingletonModel.getInstance().getContext().getResources().getDrawable(SingletonModel.getInstance().getContext().getResources()
                                    .getIdentifier(k.getString(C_GLASSES_APP.jsonParams.DRAWABLE), "drawable",
                                            SingletonModel.getInstance().getContext().getPackageName()));
                            builder.setDrawable(d);
                        }
                        //extendedText
                        if (!obj.isNull(C_GLASSES_APP.jsonParams.EXTEND_TEXT)) {
                            List<String> tmpList = new ArrayList<>();
                            JSONArray jsArray = obj.getJSONArray(C_GLASSES_APP.jsonParams.EXTEND_TEXT);
                            for (int l = 0; l < jsArray.length(); l++) {
                                tmpList.add(jsArray.getString(l));
                            }
                            builder.setExtendedText(tmpList);
                        } else if (!k.isNull(C_GLASSES_APP.jsonParams.EXTEND_TEXT)) {
                            List<String> tmpList = new ArrayList<>();
                            JSONArray jsArray = k.getJSONArray(C_GLASSES_APP.jsonParams.EXTEND_TEXT);
                            for (int l = 0; l < jsArray.length(); l++) {
                                tmpList.add(jsArray.getString(l));
                            }
                            builder.setExtendedText(tmpList);
                        }
                        return builder.Build();
                    }

                }
            }
               /*[Obbligatori]*/
                builder.setText(obj.getString(C_GLASSES_APP.jsonMandatoryParams.TEXT));
                builder.setPriority(obj.getInt(C_GLASSES_APP.jsonMandatoryParams.PRIORITY));
                builder.setIsToBeUpdate(obj.getBoolean(C_GLASSES_APP.jsonMandatoryParams.UPDATE));

                /*[Facoltativi]*/
                //type
                if (obj.isNull(C_GLASSES_APP.jsonParams.TYPE)) {
                    builder.setEventType(C_GLASSES_APP.jsonDefaultTypes.NO_TYPE);
                } else {
                    builder.setEventType(obj.getString(C_GLASSES_APP.jsonParams.TYPE));
                }
                //drawable
                if (!obj.isNull(C_GLASSES_APP.jsonParams.DRAWABLE)) {
                    Drawable d = SingletonModel.getInstance().getContext().getResources().getDrawable(SingletonModel.getInstance().getContext().getResources()
                            .getIdentifier(obj.getString(C_GLASSES_APP.jsonParams.DRAWABLE), "drawable",
                                    SingletonModel.getInstance().getContext().getPackageName()));
                    builder.setDrawable(d);
                }
                //extendedText
                if (!obj.isNull(C_GLASSES_APP.jsonParams.EXTEND_TEXT)) {
                    List<String> tmpList = new ArrayList<>();
                    JSONArray jsArray = obj.getJSONArray(C_GLASSES_APP.jsonParams.EXTEND_TEXT);
                    for (int l = 0; l < jsArray.length(); l++) {
                        tmpList.add(jsArray.getString(l));
                    }
                    builder.setExtendedText(tmpList);
                }
            return builder.Build();

        } catch (JSONException js) {
            js.printStackTrace();
            return null;
        }
    }

    /**
     * returns event's priority
     *
     * @return event's priority
     */
    public Integer getPriority() {
        return this.priority;
    }

    /**
     * returns event's text
     *
     * @return event's text
     */
    public String getText() {
        return text;
    }

    /**
     * returns event's type.
     *
     * @return event's type or C_GLASSES_APP.jsonDefaultType.NO_TYPE if event's type is missing.
     */
    public String getType() {
        return type;
    }

    /**
     * return event's extended text
     *
     * @return event's extended text or an empty list if event's extended text is missing
     */
    public List<String> getExtendedText() {
        return extendedText;
    }

    /**
     * @return true if event's extended text is present, false otherwise
     */
    public boolean isExtendedTextPresent() {
        return isExtendedTextPresent;
    }

    /**
     * return event's background drawable
     *
     * @return event's background drawable, , res/drawable/traumatracker_generic_event if event's
     * background is missing
     */
    public Drawable getDrawable() {
        return drawable;
    }

    /**
     * @return true if event has to be updated, false otherwise
     */
    @Override
    public String toString(){
        return "Type: "+this.getType()+"\nText: "+this.getText()+"\nPriority: "+this.getPriority()+"\nUpdate: "+this.isToBeUpdate();
    }
    public boolean isToBeUpdate() {
        return isToBeUpdate;
    }

    public static class EventBuilder {
        private String text = " ";
        private String type = " ";
        private List<String> extendedText = new ArrayList<>();
        private Drawable drawable = SingletonModel.getInstance().getContext().getResources().getDrawable(SingletonModel.getInstance().getContext().getResources()
                .getIdentifier("traumatracker_generics_event", "drawable",
                        SingletonModel.getInstance().getContext().getPackageName()));
        private int priority=1;
        private boolean isToBeUpdate=false;

        /**
         * Returns an Event constructed by the builder
         *
         * @return Event constructed by the builder
         */
        public Event Build() {
            return new Event(text, type, extendedText, drawable, priority, isToBeUpdate);
        }

        /**
         * set text to the Event that will be builded by the builder
         *
         * @param text
         * @return
         */
        public EventBuilder setText(String text) {
            this.text = text;
            return this;
        }

        /**
         * set type to the Event that will be builded by the builder
         *
         * @param type
         * @return
         */
        public EventBuilder setEventType(String type) {
            this.type = type;
            return this;
        }

        /**
         * set extended text to the Event that will be builded by the builder
         *
         * @param extendedText
         * @return
         */
        public EventBuilder setExtendedText(List<String> extendedText) {
            this.extendedText = extendedText;
            return this;
        }

        /**
         * set background drawable to the Event that will be builded by the builder
         *
         * @param dr
         * @return
         */
        public EventBuilder setDrawable(Drawable dr) {
            this.drawable = dr;
            return this;
        }

        /**
         * set priority to the Event that will be builded by the builder
         *
         * @param priority
         * @return
         */
        public EventBuilder setPriority(int priority) {
            this.priority = priority;
            return this;
        }

        /**
         * set if the Event that will be builded by the builder has to be updated or not
         *
         * @param isToBeUpdate
         * @return
         */
        public EventBuilder setIsToBeUpdate(boolean isToBeUpdate) {
            this.isToBeUpdate = isToBeUpdate;
            return this;
        }


    }

}
