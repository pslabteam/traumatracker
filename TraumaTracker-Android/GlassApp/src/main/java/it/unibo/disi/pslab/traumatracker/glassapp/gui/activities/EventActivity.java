package it.unibo.disi.pslab.traumatracker.glassapp.gui.activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Iterator;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.glassapp.R;
import it.unibo.disi.pslab.traumatracker.glassapp.SingletonController;
import it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.base.MyActivity;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.AccelerometerListener;
import it.unibo.disi.pslab.traumatracker.glassapp.tools.C_GLASSES_APP;
import it.unibo.disi.pslab.traumatracker.glassapp.model.Event;

public class EventActivity extends MyActivity {

    private boolean inExtendedTextMode = false;
    private Event ev;
    private SensorManager smg;
    private Sensor sensor;
    private AccelerometerListener ls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_layout);
        smg = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = smg.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
        super.setDoubleTapTimeStamp(findViewById(R.id.alarmShort));
        findViewById(R.id.alarmShort).requestFocus();
        ls = new AccelerometerListener(this, 0, true, C_GLASSES_APP.params.ACCELEROMETER_NOISE, new Runnable() {
            @Override
            public void run() {
                if ((SingletonController.getInUseActivity() instanceof EventActivity)
                        && ev.isExtendedTextPresent()) {
                    inExtendedTextMode = true;
                    Drawable dr = (SingletonController.getInUseActivity()
                            .findViewById(R.id.alarmText)).getBackground();
                    setContentView(R.layout.eventextendedtext_layout);
                    EventActivity.super.setDoubleTapTimeStamp(findViewById(R.id.alarmLong));
                    Iterator<String> k = ev.getExtendedText().iterator();
                    while (k.hasNext()) {
                        ((TextView) findViewById(R.id.alarmText)).append(k.next() + "\n");
                    }
                    if (Build.VERSION.SDK_INT < 16) {
                        findViewById(R.id.alarmText).setBackgroundDrawable(dr);
                    } else {
                        findViewById(R.id.alarmText).setBackground(dr);
                    }
                }
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        setAlarm(sing.getInUseEvent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        smg.registerListener(ls, sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ls != null) {
            smg.unregisterListener(ls);
        }
    }

    @Override
    public void setVitalSigns(final Map<String, String> vitalSigns) {
        /*
        L'override di questo metodo è necessario, in quanto se si è in extendedTextMode non è possibile
        visualizzare/settare i parametri vitali
         */
        if (!inExtendedTextMode) {
            super.setVitalSigns(vitalSigns);
        }
    }

    @Override
    public void setRoom(String room) {
         /*
        L'override di questo metodo è necessario, in quanto se si è in extendedTextMode non è possibile
        visualizzare/settare la stanza
         */
        if (!inExtendedTextMode) {
            super.setRoom(room);
        }
    }

    /**
     * Set Activity's displayed alarm
     * @param al
     */
    public void setAlarm(final Event al) {
        try {
            ev = al;
            ((TextView) this.findViewById(R.id.alarmText)).setText(ev.getText());
            if (al.getDrawable() != null) {
                if (Build.VERSION.SDK_INT < 16) {
                    findViewById(R.id.alarmText).setBackgroundDrawable(al.getDrawable());
                } else {
                    findViewById(R.id.alarmText).setBackground(al.getDrawable());
                }
            } else {
                findViewById(R.id.alarmText).setBackgroundResource(R.drawable.traumatracker_generics_event);
            }
        } catch (Exception e) {
            shutdown();
        }
    }

}


