package it.unibo.disi.pslab.traumatracker.glassapp.gui.activities.base;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

/**
 * The 'base' activity must be extended to implements activities able to run in fullscreen mode
 * on glass-like devices.
 */
public class GlassActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setActivityLayout();
    }

    /**
     * Optimizes the activity layout for glass devices
     */
    private void setActivityLayout(){
        /*
         * Request landscape orientation
         */
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        /*
         * Request fullscreen featur without status (notification) bar
         */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        /*
         * Request features to adapt the layout for the usage on smart glasses
         * (i.e. Epson Moverio BT-200, Vuzix m100, ...)
         */
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        winParams.flags |= 0x80000000;
        win.setAttributes(winParams);
    }
}
