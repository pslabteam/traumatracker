package it.unibo.disi.pslab.traumatracker.glassapp;

public class C {
	public static final String LOG_TAG = "traumatracker-glass";

	public class bt {
        public static final String BLUETOOTH_UUID = "c4daba7b-14c1-4cc1-b117-87ff24b9f21d";
        public static final String BLUETOOTH_SERVER_NAME = "tt-tablet";
    }

    public class fs {
        public static final String MEDIA_FOLDER_PATH = "/TraumaTrackerMedia";
        public static final String PICTURES_FOLDER_PATH = MEDIA_FOLDER_PATH + "/Pictures";
        public static final String VIDEOS_FOLDER_PATH = MEDIA_FOLDER_PATH + "/Videos";
    }

	public static final String MESSAGE_TYPE_CAMERA = "camera";
	public static final String MESSAGE_TYPE_NOTIFICATION = "notification";
	public static final String MESSAGE_TYPE_SPEECHRECOGNIZER_STATUS = "speech";
	public static final String MESSAGE_TYPE_PUSH_MEDIA = "media";
	
	public static final String MESSAGE_TAKE_PICTURE = "take-picture";
	public static final String MESSAGE_START_RECORDING = "start-recording";
	public static final String MESSAGE_STOP_RECORDING = "stop-recording";

	public static final String MESSAGE_PUSH_MEDIA = "push-media-to-server";
	
	public static final String MESSAGE_NOTIFICATION_OK = "notify_ok";
	public static final String MESSAGE_NOTIFICATION_ERROR = "notify_error";
	public static final String MESSAGE_NOTIFICATION_ACTIVE = "notify_active_status";
	public static final String MESSAGE_NOTIFICATION_PAUSED = "notify_pause_status";
    public static final String MESSAGE_NOTIFICATION_END = "notify_end_status";
	
	public static final String RESPONSE_MESSAGE_PICTURE_STORED = "picture_stored";

	public static final int TRACKING_STATUS_ACTIVE = 1;
	public static final int TRACKING_STATUS_PAUSED = 2;
	public static final int TRACKING_STATUS_END = 3;

    public static final String TRACKING_STATUS_ACTIVE_LABEL = "ACTIVE";
    public static final String TRACKING_STATUS_PAUSED_LABEL = "PAUSED";
    public static final String TRACKING_STATUS_END_LABEL = "END";
}
