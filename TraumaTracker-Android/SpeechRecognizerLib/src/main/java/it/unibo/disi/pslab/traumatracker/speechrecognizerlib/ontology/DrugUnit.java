package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology;

public enum DrugUnit {
    ML("ML"),
    MG("MG"),
    G("G"),
    UNITA("UNITA"),
    POOL("POOL");

    private String sRepresentation;

    DrugUnit(String sRepresentation){
        this.sRepresentation = sRepresentation;
    }

    public String toString(){
        return this.sRepresentation;
    }
}
