package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology;

import java.util.ArrayList;
import java.util.List;

public enum DrugAndInfusions {
    CRISTALLOIDI("cristalloidi", DrugUnit.ML),
    ZERO_NEGATIVO("zero negativo", DrugUnit.MG),
    SOLUZIONE_IPERTONICA("soluzione ipertonica", DrugUnit.ML),
    EMAZIE_CONCENTRATE("emazie concentrate", DrugUnit.MG),
    PLASMA_FRESCO_CONGELATO("plasma fresco congelato", DrugUnit.MG),
    FIBRINOGENO("fibrinogeno", DrugUnit.MG),
    TRANEX("tranex", DrugUnit.MG),
    ACIDO_TRANEXAMICO("acido tranexamico", DrugUnit.MG),
    POOLPIASTRINE("poolpiastrine", DrugUnit.MG),
    MANNITOLO("mannitolo", DrugUnit.MG),
    MIDAZOLAM("midazolam", DrugUnit.MG),
    PROPOFOL("propofol", DrugUnit.MG),
    FENTANIL("fentanil", DrugUnit.MG),
    KETAMINA("ketamina", DrugUnit.MG),
    ROCURONIO("rocuronio", DrugUnit.MG),
    SUCCINILCOLINA("succinilcolina", DrugUnit.MG),
    TIOPENTONE("tiopentone", DrugUnit.MG),
    MORFINA("morfina", DrugUnit.ML),
    CRIOPRECIPITATI("crioprecipitati", DrugUnit.MG);

    private String sRepresentation;
    private DrugUnit unit;
    private static MedicalTermsType type = MedicalTermsType.DRUG_AND_INFUSIONS;

    DrugAndInfusions(String sRepresentation, DrugUnit unit){
        this.sRepresentation = sRepresentation;
        this.unit = unit;
    }

    public String toString(){
        return this.sRepresentation;
    }

    public static List<String> getStringValues(){
        List<String> res = new ArrayList<String>();
        for (DrugAndInfusions elem: DrugAndInfusions.values()) {
            res.add(elem.toString());
        }
        return res;
    }

    public DrugUnit getUnit(){
    	return this.unit;
    }
    
    public static MedicalTermsType getType(){
        return type;
    }
    
    /**
     * Get the DrugAndInfusion value instance that corresponds to the string representation specified in sRepresentation
     * @param sRepresentation the string representation of the Drug or Infusion term
     * @return the DrugAndInfusion instance if sRepresentation match with one value of the enum, null otherwise
     */
    public static DrugAndInfusions valueOfByStringRepresentation(String sRepresentation){
    	for(DrugAndInfusions drug : DrugAndInfusions.values()){
    		if(drug.toString().equals(sRepresentation)){
    			return drug;
    		}
    	}
    	return null;    	
    }
}
