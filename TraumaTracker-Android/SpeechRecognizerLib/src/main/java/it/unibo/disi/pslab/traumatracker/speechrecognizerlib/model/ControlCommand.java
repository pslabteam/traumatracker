package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model;

public class ControlCommand {
	public static final String INIZIO_INTERVENTO = "inizio intervento";
	public static final String INIZIA_INTERVENTO = "inizia intervento";
	
	public static final String FINE_INTERVENTO = "fine intervento";
	
	public static final String SCATTA_FOTO = "scatta foto";
	public static final String REGISTRA_VIDEO = "registra video";
	public static final String FINE_VIDEO = "fine video";
	
	public static final String PAUSA_RICONOSCIMENTO = "trauma pausa";
	public static final String RIATTIVA_RICONOSCIMENTO = "trauma ascolta";
}
