package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology;

import java.util.ArrayList;
import java.util.List;

public enum ResuscitationManeuvers {
    INTUBAZIONE_OROTRACHEALE("intubazione orotracheale"),
    DRENAGGIO_TORACICO("drenaggio toracico"),
    PELVIC_BINDER("pelvic binder"),
    TPOD("tpod"),
    CATETERE_ALTO_FLUSSO("catetere alto flusso"),
    INFUSORE_A_PRESSIONE("infusore a pressione"),
    SUTURA_FERITA_EMORRAGICA("sutura ferita emorragica"),
    POSIZIONAMENTO_FISSATORE_ESTERNO("posizionamento fissatore esterno"),
    TORACOTOMIA_RESUSCITATIVA("toracotomia resuscitativa"),
    REBOA("reboa");

    private String sRepresentation;
    private static MedicalTermsType type = MedicalTermsType.RESUSCITATION_MANEUVERS;

    ResuscitationManeuvers(String sRepresentation){
        this.sRepresentation = sRepresentation;
    }

    public String toString(){
        return this.sRepresentation;
    }

    public static List<String> getStringValues(){
        List<String> res = new ArrayList<String>();
        for (ResuscitationManeuvers elem: ResuscitationManeuvers.values()) {
            res.add(elem.toString());
        }
        return res;
    }

    public static MedicalTermsType getType(){
        return type;
    }
}
