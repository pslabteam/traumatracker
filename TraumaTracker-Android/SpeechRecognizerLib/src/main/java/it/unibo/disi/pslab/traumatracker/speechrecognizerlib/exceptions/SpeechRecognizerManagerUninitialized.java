package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.exceptions;

public class SpeechRecognizerManagerUninitialized extends Exception {

	private static final long serialVersionUID = 1L;
	
	public SpeechRecognizerManagerUninitialized(){
		super();
	}

}
