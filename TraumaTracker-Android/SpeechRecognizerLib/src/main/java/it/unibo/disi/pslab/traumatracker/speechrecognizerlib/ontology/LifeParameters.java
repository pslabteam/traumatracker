package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology;

import java.util.ArrayList;
import java.util.List;

public enum LifeParameters {
    PARAMETRI_VITALI("parametri vitali"),

    VIE_AEREE("vie aeree"),
    DECOMPRESSIONE_PLEURICA("decompressione pleurica"),
    PRESSIONE_DIASTOLICA("pressione diastolica"),
    PRESSIONE_SISTOLICA("pressione sistolica"),
    FREQUENZA_CARDIACA("frequenza cardiaca"),
    SATURAZIONE("saturazione"),
    ETCO2("etCO2"),
    TEMPERATURA("temperatura"),
    PUPILLE("pupille"),
    OCCHI("occhi"),
    MOTORIO("motorio"),
    VERBALE("verbale");

    private String sRepresentation;
    private static MedicalTermsType type = MedicalTermsType.LIFE_PARAMETERS;

    LifeParameters(String sRepresentation){
        this.sRepresentation = sRepresentation;
    }

    public String toString(){
        return this.sRepresentation;
    }

    public static List<String> getStringValues(){
        List<String> res = new ArrayList<String>();
        for (LifeParameters elem: LifeParameters.values()) {
            res.add(elem.toString());
        }
        return res;
    }

    public static MedicalTermsType getType(){
        return type;
    }
}
