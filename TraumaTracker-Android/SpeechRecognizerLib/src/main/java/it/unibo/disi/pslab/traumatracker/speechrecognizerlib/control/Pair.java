package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.control;

/**
 *
 * An class that enclose two Object
 * @author Matteo Gabellini
 *
 * @param <X>
 *            type of the first {@link Object}
 * @param <Y>
 *            type of the second {@link Object}
 * */
public class Pair<X, Y> {
    private final X firstElement;
    private final Y secondElement;

    public Pair(X firstElement,Y secondElement){
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }

    public X getFirstElem(){
        return this.firstElement;
    }

    public Y getSecondElem(){
        return this.secondElement;
    }

    public String toString(){
        return "Pair [first=" + this.firstElement + ", second=" + this.secondElement + "]";
    }

    public int hashCode(){
        //Bynary XOR between the hash code of the two elements
        return (this.firstElement == null ? 0 : this.firstElement.hashCode()) ^ (this.secondElement == null ? 0:this.secondElement.hashCode());
    }

    public boolean equals(final Object obj){
        if (obj instanceof Pair) {
            final Pair<?, ?> p = (Pair<?, ?>) obj;
            if (this.firstElement == null && p.getFirstElem() != null) {
                return false;
            }
            if (this.secondElement == null && p.getSecondElem() != null) {
                return false;
            }
            return this.firstElement.equals(p.getFirstElem()) && this.secondElement.equals(p.getSecondElem());
        }
        return false;
    }
}
