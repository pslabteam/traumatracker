package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.control;
import java.util.ArrayList;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model.CommandCode;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model.ControlCommand;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model.MedicalTerm;

public class CommandManager {
	
	private boolean isOperationActive;
	private boolean isOnPause;
	private Handler uiHandler;
	private MedicalSpeechInterpreter interpreter;
	
	public CommandManager(Handler uiHandler){
		this.uiHandler = uiHandler;
		this.interpreter = new MedicalSpeechInterpreter();
		this.isOperationActive = false;
		this.isOnPause = false;
	}
	
	private void sendToUIHandler(int commandCode, Object message){
		Message msg = new Message();
		msg.what = commandCode;
		msg.obj = message;
		uiHandler.sendMessage(msg);
	}
	
	private String checkControlCommand(String sentence){
		if(sentence.compareTo(ControlCommand.INIZIO_INTERVENTO) == 0 || sentence.compareTo(ControlCommand.INIZIA_INTERVENTO) == 0){
			this.onStartOfMission();
			sendToUIHandler(CommandCode.START_MISSION, "Modalità intervento attivata");
			return sentence;
		} else if(this.isOperationActive){
            //Check the other control command only if the mission is active
            if(sentence.compareTo(ControlCommand.FINE_INTERVENTO) == 0){
                this.onEndOfMission();
                sendToUIHandler(CommandCode.END_MISSION, "Modalità intervento disattivata");
                return sentence;
            } else if(sentence.compareTo(ControlCommand.SCATTA_FOTO) == 0){
                sendToUIHandler(CommandCode.TAKE_PICTURE, "Scatta foto");
                return sentence;
            } else if(sentence.compareTo(ControlCommand.REGISTRA_VIDEO) == 0){
                sendToUIHandler(CommandCode.START_VIDEO_RECORDING, "Registra Video");
                return sentence;
            } else if(sentence.compareTo(ControlCommand.FINE_VIDEO) == 0){
                sendToUIHandler(CommandCode.STOP_VIDEO_RECORDING, "Ferma registrazione video");
                return sentence;
            } else if(sentence.compareTo(ControlCommand.PAUSA_RICONOSCIMENTO) == 0){
                sendToUIHandler(CommandCode.PAUSE_RECOGNITION, "Riconoscimento termini medici in pausa");
                this.onPauseMedicalTermRecognizer();
                return sentence;
            } else if(sentence.compareTo(ControlCommand.RIATTIVA_RICONOSCIMENTO) == 0){
                sendToUIHandler(CommandCode.RESUME_RECOGNITION, "Riconoscimento termini medici riattivata");
                this.onResumeMedicalTermRecognizer();
                return sentence;
            }
        }
		return null;
	}
	
	
	
	/* -------- Message to send to the trauma tracker Agent -------- */
	
	public void notifyCommand(ArrayList<String> sentences){
		boolean foundMatch = false;
		
		//for (int i = 0; i < sentences.size() && !foundMatch; i++) {
			Log.d("TRAUMA TRACKER SR LIB","Parola Rilevata -> " + sentences.get(0));
			
			String controlCmd = this.checkControlCommand(sentences.get(0));
			MedicalTerm medicalCmd = null;
			//until a mission isn't started or when the system is paused, doesn't control the medical terms 
			if(this.isOperationActive && !this.isOnPause){
				if(controlCmd == null){
					medicalCmd = this.interpreter.checkSentence(sentences.get(0));
				}
			}
			
			if(medicalCmd != null){
				foundMatch = true;
				
				sendToUIHandler(CommandCode.STRINGS_RECOGNIZED, medicalCmd);
				
				Log.d("TRAUMA TRACKER SR LIB","Ho interpretato -> " + medicalCmd);
			}
		//}
		
		if(!foundMatch){			
			sendToUIHandler(CommandCode.STRINGS_NOT_RECOGNIZED, "no command recognized");
		}
	}
	
	public void notifyError(String errorText){
		Message msg = new Message();
		msg.what = CommandCode.MESSAGE_ERROR;
		msg.obj = errorText;
		uiHandler.sendMessage(msg);
	}
	/* ------------------------------------------------------------- */


	/* -------- Collback from Speech Recognizer Manager -------- */

	public void onStartOfMission(){
		this.isOperationActive = true;
	}

	public void onEndOfMission(){
		this.isOperationActive = false;
		this.isOnPause = false;
	}

	public void onPauseMedicalTermRecognizer(){
        //The command has effect only if the mission is started
        if(this.isOperationActive) {
            this.isOnPause = true;
        }
	}

	public void onResumeMedicalTermRecognizer(){
		this.isOnPause = false;
	}

	/* ---------------------------------------------------------------- */

	
	/* -------- Message to send directly to the smart glasses -------- */
	
	public void notifyStartOfSpeech(){
		Message msg = new Message();
		msg.what = CommandCode.START_LISTENING;
		msg.obj = "Start Listening";
		uiHandler.sendMessage(msg);
	}
	
	public void notifyEndOfSpeech(){
		Message msg = new Message();
		msg.what = CommandCode.END_OF_SPEECH;
		msg.obj = "Stop Listening";
		uiHandler.sendMessage(msg);
	}
	
	/* ---------------------------------------------------------------- */
}
