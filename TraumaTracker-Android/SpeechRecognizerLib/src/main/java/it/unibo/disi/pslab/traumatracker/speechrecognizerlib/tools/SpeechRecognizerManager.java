package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.tools;

import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.control.CommandManager;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.control.SpeechRecognizerListener;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.exceptions.SpeechRecognizerManagerUninitialized;

public class SpeechRecognizerManager {

	private SpeechRecognizer sr;
	private RecognitionListener listener;
	private CommandManager cManager;

	private Context ctx;
	
	private volatile boolean initialized = false;
	
	private static SpeechRecognizerManager srm = new SpeechRecognizerManager();

	public static SpeechRecognizerManager getInstance(){
		return srm;
	}
	
	private SpeechRecognizerManager() { }

	public void initialize(Context context, Handler uiHandler){
		this.ctx = context;
		this.cManager = new CommandManager(uiHandler);
		this.listener = new SpeechRecognizerListener(this.cManager);

		this.initialize(context, this.listener);

	}

	private void initialize(Context context, RecognitionListener listener){
		sr = SpeechRecognizer.createSpeechRecognizer(context);
		sr.setRecognitionListener(listener);

		initialized = true;
	}

	/**
	 * 
	 * @throws SpeechRecognizerManagerUninitialized
	 */
	public void start() throws SpeechRecognizerManagerUninitialized {
		if(!initialized)
			throw new SpeechRecognizerManagerUninitialized();
		
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
		intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, false);
		//intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true); //Added in API level 23
		//intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, new Long(100));
		
		sr.startListening(intent);
	}
	
	public void notifyTimeExpired(){
		sr.stopListening();
	}
	
	/**
	 * 
	 * @throws SpeechRecognizerManagerUninitialized
	 */
	public void restart() throws SpeechRecognizerManagerUninitialized{
		if(!initialized)
			throw new SpeechRecognizerManagerUninitialized();
		
		sr.destroy();

		this.initialize(this.ctx, this.listener);
		
		start();
	}
	
	/**
	 * 
	 * @throws SpeechRecognizerManagerUninitialized
	 */
	public void stop() throws SpeechRecognizerManagerUninitialized{
		if(!initialized)
			throw new SpeechRecognizerManagerUninitialized();
		
		sr.cancel();
	}

	/**
	 * This method can be called by the class that uses the
	 * SpeechRecognizerManager (The SpeechAgent in trauma tracker)
	 * to manually start the mission
	 */
	public void onStartMission(){
        this.cManager.onStartOfMission();
	}

	/**
	 * This method can be called by the class that uses the
	 * SpeechRecognizerManager (The SpeechAgent in trauma tracker)
	 * to manually end the mission
	 */
	public void onEndOfMission(){
        this.cManager.onEndOfMission();
	}

	/**
	 * This method can be called by the class that uses the
	 * SpeechRecognizerManager (The SpeechAgent in trauma tracker)
	 * to manually set in pause the medical speech recognizer
	 * This command has effect only if the mission is started
	 */
	public void onPauseMedicalTermRecognizer(){
        this.cManager.onPauseMedicalTermRecognizer();
	}

	/**
	 * This method can be called by the class that uses the
	 * SpeechRecognizerManager (The SpeechAgent in trauma tracker)
	 */
	public void onResumeMedicalTermRecognizer(){
        this.cManager.onResumeMedicalTermRecognizer();
	}
}
