package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology;

public enum MedicalTermsType {
    RESUSCITATION_MANEUVERS,
    LIFE_PARAMETERS,
    DRUG_AND_INFUSIONS
}
