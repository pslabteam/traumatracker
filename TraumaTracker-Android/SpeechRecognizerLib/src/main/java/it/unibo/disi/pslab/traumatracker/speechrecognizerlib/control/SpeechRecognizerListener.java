package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.control;

import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;

import java.util.ArrayList;

import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.exceptions.SpeechRecognizerManagerUninitialized;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.tools.SpeechRecognizerManager;

public class SpeechRecognizerListener implements RecognitionListener {

	public static final int TIME_LISTENING_AFTER_START_OF_SPEECH = 3000; //milliseconds

	private CommandManager cManager;
	private volatile boolean recognitionStopped;
	
	public SpeechRecognizerListener(CommandManager cManager){
		this.cManager = cManager;
	}

	@Override
	public void onReadyForSpeech(Bundle params) {
		this.cManager.notifyStartOfSpeech();
	}

	@Override
	public void onBeginningOfSpeech() {
		/*this.recognitionStopped = false;
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(TIME_LISTENING_AFTER_START_OF_SPEECH);
					new Handler (Looper.getMainLooper ()).post (new Runnable () 
			        {
			            @Override
			            public void run () 
			            {			            	
			            	if(!recognitionStopped){
			            		SpeechRecognizerManager.getInstance().notifyTimeExpired();
			            	}
			            }
			        });					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			}
		}).start();*/
	}

	@Override
	public void onRmsChanged(float rmsdB) {
		
	}

	@Override
	public void onBufferReceived(byte[] buffer) {
		
	}

	@Override
	public void onEndOfSpeech() {
		this.recognitionStopped = true;
	}

	@Override
	public void onError(int error) {
		this.cManager.notifyEndOfSpeech();
		String errorText = getErrorText(error);		
		
		cManager.notifyError(errorText);
		
		try {
			SpeechRecognizerManager.getInstance().restart();
		} catch (SpeechRecognizerManagerUninitialized e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResults(Bundle results) {
		this.cManager.notifyEndOfSpeech();
		ArrayList<String> recognizedSentences = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		
		this.cManager.notifyCommand(recognizedSentences);	
		
		try {
			SpeechRecognizerManager.getInstance().restart();
		} catch (SpeechRecognizerManagerUninitialized e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPartialResults(Bundle partialResults) {
		
	}

	@Override
	public void onEvent(int eventType, Bundle params) {
		
	}
	
	private String getErrorText(int errorCode) {
		String message;
		
		switch (errorCode) {
		case SpeechRecognizer.ERROR_AUDIO:
			message = "Audio recording error";
			break;

		case SpeechRecognizer.ERROR_CLIENT:
			message = "Client side error";
			break;

		case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
			message = "Insufficient permissions";
			break;

		case SpeechRecognizer.ERROR_NETWORK:
			message = "Network error";
			break;

		case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
			message = "Network timeout";
			break;

		case SpeechRecognizer.ERROR_NO_MATCH:
			message = "No match";
			break;

		case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
			message = "RecognitionService busy";
			break;

		case SpeechRecognizer.ERROR_SERVER:
			message = "error from server";
			break;

		case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
			message = "No speech input";
			break;

		default:
			message = "Didn't understand, please try again.";
			break;
		}

		return message;
	}
	
}
