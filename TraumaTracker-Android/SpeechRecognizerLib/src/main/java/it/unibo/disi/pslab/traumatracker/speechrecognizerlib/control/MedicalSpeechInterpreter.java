package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model.MedicalTerm;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology.DrugAndInfusions;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology.LifeParameters;
import it.unibo.disi.pslab.traumatracker.speechrecognizerlib.ontology.ResuscitationManeuvers;


/**
 * This class provides the techniques to bind the results provided by Voice recognizer, 
 * to the Medical Terms defined in {@link ResuscitationManeuvers}, {@link DrugAndInfusions},
 * {@link LifeParameters}
 */

public class MedicalSpeechInterpreter {
	
	/*  
	 * 	This hashMap contains the words that that the recognizer gives when the user say a medical term
	 *  and that the algorithm fails to bind to a medical terms
	 *  
	 */	
	private final HashMap<String, String> ambiguousResMan = new HashMap<String, String>();
	private final HashMap<String, String> ambiguousLifeParam = new HashMap<String, String>();
	private final HashMap<String, String> ambiguousDrugAndInf = new HashMap<String, String>();
	
	public MedicalSpeechInterpreter(){
		this.initAmbiguousTermDictionary();		
	}
	
	
	private void initAmbiguousTermDictionary(){
		//Reboa
		this.ambiguousResMan.put("Rebel", "reboa");
		this.ambiguousResMan.put("Remo", "reboa");
		this.ambiguousResMan.put("Reebok", "reboa");
		this.ambiguousResMan.put("report", "reboa");
		this.ambiguousResMan.put("prego", "reboa");

		//Tpod
		this.ambiguousResMan.put("tipo te", "tpod");
		this.ambiguousResMan.put("ti porta", "tpod");
		this.ambiguousResMan.put("tipo", "tpod");
		this.ambiguousResMan.put("tipo da", "tpod");

		//etCO2
		this.ambiguousLifeParam.put("e ti CO2", "etCO2");
		this.ambiguousLifeParam.put("e di CO2", "etCO2");
		this.ambiguousLifeParam.put("e che CO2", "etCO2");
		this.ambiguousLifeParam.put("e Ticino 2", "etCO2");
		this.ambiguousLifeParam.put("che ti CO2", "etCO2");
		this.ambiguousLifeParam.put("e ti giro II", "etCO2");

		//frequenza cardiaca
		this.ambiguousLifeParam.put("cardiaca", "frequenza cardiaca");

		//Tranex
		this.ambiguousDrugAndInf.put("Trunks", "tranex");
		this.ambiguousDrugAndInf.put("tranne te", "tranex");
		this.ambiguousDrugAndInf.put("Alex", "tranex");

		//Propofol
		this.ambiguousDrugAndInf.put("troppo forte", "propofol");
		this.ambiguousDrugAndInf.put("troppo forti", "propofol");
		this.ambiguousDrugAndInf.put("troppa fall", "propofol");
		this.ambiguousDrugAndInf.put("troppa fuori", "propofol");
		this.ambiguousDrugAndInf.put("troppa fondi", "propofol");

		//Fibrinogeno
		this.ambiguousDrugAndInf.put("Sabrina Gino", "fibrinogeno");
		this.ambiguousDrugAndInf.put("si prenotano", "fibrinogeno");

		//Midazolam
		this.ambiguousDrugAndInf.put("Mirandola", "midazolam");

		//Succinilcolina
		this.ambiguousDrugAndInf.put("social in collina", "succinilcolina");
		this.ambiguousDrugAndInf.put("sotto nel collina", "succinilcolina");
		this.ambiguousDrugAndInf.put("oggi Nicolina", "succinilcolina");
		this.ambiguousDrugAndInf.put("sono in collina", "succinilcolina");
		this.ambiguousDrugAndInf.put("cerco Lina", "succinilcolina");
		this.ambiguousDrugAndInf.put("social Ercolino", "succinilcolina");
		this.ambiguousDrugAndInf.put("sotto Nicolina", "succinilcolina");
		this.ambiguousDrugAndInf.put("suggerisco Lina", "succinilcolina");
		this.ambiguousDrugAndInf.put("se c'è Nicolina", "succinilcolina");

		//cristalloidi
		this.ambiguousDrugAndInf.put("Crystal Lloyd", "cristalloidi");
        this.ambiguousDrugAndInf.put("Crystal ohi di", "cristalloidi");
        this.ambiguousDrugAndInf.put("Cristalli e di", "cristalloidi");
        this.ambiguousDrugAndInf.put("cristallo ohi di", "cristalloidi");

		//rocuronio 
		this.ambiguousDrugAndInf.put("Pro Cologno", "rocuronio");
		this.ambiguousDrugAndInf.put("ora Cologno", "rocuronio");
		this.ambiguousDrugAndInf.put("Pro colonia", "rocuronio");
		this.ambiguousDrugAndInf.put("caro colonia", "rocuronio");
		this.ambiguousDrugAndInf.put("era colonia", "rocuronio");
		this.ambiguousDrugAndInf.put("Rocco ragno", "rocuronio");
		this.ambiguousDrugAndInf.put("Rocco rogna", "rocuronio");
		this.ambiguousDrugAndInf.put("Rocco Rogno", "rocuronio");

	}
	
	public MedicalTerm checkSentence(String test){
		MedicalTerm res;
		ArrayList<String> dictionary = new ArrayList<String>();
		dictionary.addAll(ResuscitationManeuvers.getStringValues());

		String resCheck = this.checkAmbiguousTerms(this.ambiguousResMan,test);
		if(resCheck == null){
			resCheck = this.checkDictionary(test, dictionary);
		}
		
		if(resCheck != null){
			return new MedicalTerm(ResuscitationManeuvers.getType(), resCheck);
		}
		
		/* Check terms with quantity */
        dictionary.clear();
        dictionary.addAll(DrugAndInfusions.getStringValues());
        Pair<String, Double> resWithQ = this.checkSentenceWithQuantity(test, dictionary, this.ambiguousDrugAndInf);
        if(resWithQ != null && resWithQ.getSecondElem() != null){
            return new MedicalTerm(DrugAndInfusions.getType(), resWithQ.getFirstElem(), resWithQ.getSecondElem(), DrugAndInfusions.valueOfByStringRepresentation(resWithQ.getFirstElem()).getUnit());
        }
		
		dictionary.clear();
        dictionary.addAll(LifeParameters.getStringValues());
        resWithQ = this.checkSentenceWithQuantity(test, dictionary, this.ambiguousLifeParam);
        if(resWithQ != null){
            return new MedicalTerm(LifeParameters.getType(), resWithQ.getFirstElem(), resWithQ.getSecondElem());
        } else {
            return null;
        }
	}


    private Pair<String, Double> checkSentenceWithQuantity(String test, List<String> terms, Map<String, String> ambiguousTerm){
        String[] splittedTest = test.split(" ");
        String partialTest = "";
        Map<String, String> possibleNumberValue = new HashMap<String, String>();
        String res = null;
        for(String tmp : splittedTest){
            partialTest = partialTest.equals("") ? tmp : partialTest + " " + tmp;
            res = this.checkAmbiguousTerms(ambiguousTerm, partialTest);
            if(res == null) {
                res = this.checkDictionary(partialTest, terms);
            }
            if(res != null){
                // Add / Update the rest of the sentence that may be a number
                try {
                    String secondHalfString = test.split(partialTest + " ")[1];
                    if(secondHalfString != null){
                    	if(!possibleNumberValue.containsKey(res)){
                    		possibleNumberValue.put(res, secondHalfString);
                    	}
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
                }
            }
        }
        
        if(possibleNumberValue.keySet().isEmpty()){
        	if(res != null){
        		//if i found only one the term
        		return new Pair<String, Double>(res, null);
        	}
            return null;
        }

        Map<String, Double> translation = this.checkNumber(possibleNumberValue);
        
        /* 
         * in this moment this method return only the first translation but in the future 
         * it could be modified to return multiple possible traslations of the sentence said by the user
         */
        
        //If the second half of the string can be translated in to a number
        if(translation.size() != 0){
        	//retun the pair composed by the term and the quantity
            return new Pair<String,Double>(translation.keySet().toArray()[0].toString(), translation.get(translation.keySet().toArray()[0].toString()));
        } else {
        	//Otherwise return the pair composed by the term and null
            return new Pair<String, Double>(possibleNumberValue.keySet().toArray()[0].toString(), null);
        }
    }




    private String checkAmbiguousTerms(Map<String,String> ambiguousTerm, String test){
		if(ambiguousTerm.containsKey(test)){
			return ambiguousTerm.get(test);
		} 
		return null;
	}

	private String checkDictionary(String test, List<String> dictionary){
		double accuracy = 86;
		String max, min;
		for(; accuracy >= 54; accuracy -= 2) {
			for(String cmd : dictionary){
				
				if(cmd.length() >= test.length()){
					max = cmd;
					min = test;
				} else {
					max = test;
					min = cmd;
				}
	
				double ld = levenshteinDistance(min, max);
				double v = (max.length() - ld)/max.length()*100;
				
				if(v >= accuracy){
					return cmd;
				}
			}
		}		
		return null;
	}
	
	private Map<String, Double> checkNumber(Map<String, String> possibleNumber){
		Map<String, Double> res = new HashMap<String, Double>();		
		
		boolean isPresentNumericValue = false;
		
		/* Number that the speech recognizer gives in letters*/
		Map<String, Double> NUMBER = new HashMap<String, Double>();
		NUMBER.put("mille", 1000.0);
		NUMBER.put("sei", 6.0);
		NUMBER.put("uno", 1.0);
		NUMBER.put("due", 2.0);
		NUMBER.put("sette", 7.0);
		NUMBER.put("nove", 9.0);
		NUMBER.put("dieci", 10.0);
		NUMBER.put("centomila", 100000.0);
		NUMBER.put("mezzo", 0.50);
		
		for(String tmp : possibleNumber.keySet()){
			//for(String posNum : possibleNumber.get(tmp)){
			String posNum = possibleNumber.get(tmp);
					
			double converted = 0;
			posNum = posNum.replace(',', '.');
			//clean imperfection
			if(posNum.contains(":30")){
				posNum = posNum.replace(":30", ".50");
			}
			if(posNum.contains(":00")){
				posNum = posNum.replace(":00", "");
			}
			
			if(posNum.contains(" . ")){
				posNum = posNum.replace(" . ", ".");
			} else if(posNum.contains(". ")){
				posNum = posNum.replace(". ", ".");
			} else if(posNum.contains(" punto ")){
				posNum = posNum.replace(" punto ", ".");
			}
			
			
			String[] sSplitted = posNum.split(" ");  
			for(String s : sSplitted){
				s = s.toLowerCase(Locale.getDefault());
				try{
					converted += NUMBER.containsKey(s) ? NUMBER.get(s) : Double.parseDouble(s);
					isPresentNumericValue = true;
				} catch (NumberFormatException e){
					e.printStackTrace();
				}
			}
			
			if(isPresentNumericValue){				
				res.put(tmp, converted);
			} else {
				res.put(tmp, null);
			}
		}		
		return res;
	}
	
	/* This function provides a string metric for measuring 
	 * the difference between two sequences. The levenshtein distance between 
	 * two strings A and B is the minimum number of elementary modifies (deletions, insertions or substitutions) that allow
	 * to transform A in B */
	private int levenshteinDistance(String s, String t)	{
	    if (s.equals(t)){
            return 0;
        }
	    
	    if (s.length() == 0) {
            return t.length();
        }

	    if (t.length() == 0) {
            return s.length();
        }

	    int[] v0 = new int[t.length() + 1];
	    int[] v1 = new int[t.length() + 1];

	    for (int i = 0; i < v0.length; i++)
	        v0[i] = i;

	    for (int i = 0; i < s.length(); i++)
	    {
	        v1[0] = i + 1;

	        for (int j = 0; j < t.length(); j++)
	        {
	            int cost = (s.toCharArray()[i] == t.toCharArray()[j]) ? 0 : 1;
	            v1[j + 1] = Math.min(Math.min(v1[j] + 1, v0[j + 1] + 1), v0[j] + cost);
	        }

	        for (int j = 0; j < v0.length; j++) {
                v0[j] = v1[j];
            }
	    }

	    return v1[t.length()];
	}

}
