package it.unibo.disi.pslab.traumatracker.speechrecognizerlib.model;

public class CommandCode {
	public static final int STRINGS_RECOGNIZED = 1;
	public static final int STRINGS_NOT_RECOGNIZED = 2;
	public static final int MESSAGE_ERROR = 3;
	public static final int START_LISTENING = 4;
	public static final int END_OF_SPEECH = 5;
	public static final int TAKE_PICTURE = 6;
    public static final int START_VIDEO_RECORDING = 7;
	public static final int STOP_VIDEO_RECORDING = 8;
	
	public static final int START_MISSION = 10;
	public static final int END_MISSION = 11;
	
	public static final int PAUSE_RECOGNITION = 12;
	public static final int RESUME_RECOGNITION = 13;
	
	//public static final String APP_UUID = "recog-agent-12cd2-dfgh4-aqwfe";
}
