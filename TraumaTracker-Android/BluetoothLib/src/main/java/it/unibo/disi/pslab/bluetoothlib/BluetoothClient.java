package it.unibo.disi.pslab.bluetoothlib;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import it.unibo.disi.pslab.bluetoothlib.exceptions.BluetoothDeviceNotFound;
import it.unibo.disi.pslab.bluetoothlib.exceptions.BluetoothMessageTooBigException;
import it.unibo.disi.pslab.bluetoothlib.utils.BluetoothError;
import it.unibo.disi.pslab.bluetoothlib.utils.BluetoothUtils;
import it.unibo.disi.pslab.bluetoothlib.utils.C;

public class BluetoothClient {

    private static final int TRY_TO_CONNECT_WAIT_TIME = 1000;

    private volatile boolean active = false;

    private BluetoothSocket btSocket;

    private List<Events> listeners = new ArrayList<Events>();

    private static BluetoothClient instance = new BluetoothClient();

    public static BluetoothClient getInstance() {
        return instance;
    }

    private BluetoothClient() {}

    /**
     * Register an Event Listenr for this Bluetooth Client
     * @param el The event listener to be registered
     */
    public void registerEventListener(Events el) {
        listeners.add(el);
    }

    /**
     * Try-loop connect to the Server device
     * TODO: add attemps or timeout?
     * @param serverDeviceNameOrAddress The Bluetooth name of the Server Device
     * @param uuid The UUID to be used for the connection
     */
    public void connect(final String serverDeviceNameOrAddress, final String uuid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean tryToConnect = true;
                while (tryToConnect) {
                    try {
                        btSocket = BluetoothUtils.detectPairedDeviceByAddress(serverDeviceNameOrAddress).createRfcommSocketToServiceRecord(UUID.fromString(uuid));
                        btSocket.connect();

                        tryToConnect = false;
                    } catch (IOException e) {
                        for (Events ev : listeners) {
                            ev.onConnectionRefused();
                        }

                        try {
                            Thread.sleep(TRY_TO_CONNECT_WAIT_TIME);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                        for (Events e : listeners) {
                            e.deviceNotFound();
                        }

                        return;
                    }
                }

                for (Events e : listeners) {
                    e.onConnectionAccepted();
                }

                active = true;

                new ClientThread().start();
            }
        }).start();
    }

    /**
     * Send a message to the connected device
     * @param msg The message to be sent
     * @throws BluetoothMessageTooBigException
     * @throws IOException
     */
    public void sendMessage(String msg) throws BluetoothMessageTooBigException, IOException {
        if (msg.length() > C.BT_MESSAGE_MAX_LENGTH) {
            throw new BluetoothMessageTooBigException();
        }

        char[] array = msg.toCharArray();
        byte[] bytes = new byte[array.length + 1];
        bytes[0] = (byte) msg.length();

        for (int i = 0; i < array.length; i++) {
            bytes[i + 1] = (byte) array[i];
        }

        btSocket.getOutputStream().write(bytes);
        btSocket.getOutputStream().flush();
    }

    /**
     * Try to disconnect the Bluetooth Client
     * TODO: in realtà interrompe solo l'ascolto dei messaggi in arrivo
     */
    public void disconnect() {
        active = false;
    }

    /**
     * Check if the BluetoothClient is connected to the Server
     * @return True, if the connection is performed, Flase otherwise.
     */
    public boolean isConnected() {
        return active;
    }

    /**
     *
     */
    public interface Events {
        void onConnectionAccepted();
        void onConnectionRefused();
        void deviceNotFound();
        void onMessageReceived(String message);
        void onClientError(BluetoothError error);
    }

    /**
     *
     */
    private class ClientThread extends Thread {
        @Override
        public void run() {
            while (active) {
                try {
                    int bytesRead = 0, messageSize = btSocket.getInputStream().read();

                    StringBuffer buf = new StringBuffer();

                    while (bytesRead < messageSize) {
                        buf.append((char) btSocket.getInputStream().read());
                        bytesRead++;
                    }

                    for (Events e : listeners) {
                        e.onMessageReceived(buf.toString());
                    }

                } catch (IOException e) {
                    active = false;

                    for (Events ev : listeners) {
                        ev.onClientError(BluetoothError.GENERIC_EXCEPTION);
                    }
                }
            }
        }
    }
}
