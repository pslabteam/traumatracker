package it.unibo.disi.pslab.bluetoothlib.utils;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.util.Set;

import it.unibo.disi.pslab.bluetoothlib.exceptions.BluetoothDeviceNotFound;

public class BluetoothUtils {

    public static BluetoothDevice detectPairedDevice(String deviceName) throws BluetoothDeviceNotFound {
        Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals(deviceName)) {
                    return device;
                }
            }
        }

        throw new BluetoothDeviceNotFound();
    }

    public static BluetoothDevice detectPairedDeviceByAddress(String deviceAddress) throws BluetoothDeviceNotFound {
        Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getAddress().equals(deviceAddress)) {
                    return device;
                }
            }
        }

        throw new BluetoothDeviceNotFound();
    }
}
