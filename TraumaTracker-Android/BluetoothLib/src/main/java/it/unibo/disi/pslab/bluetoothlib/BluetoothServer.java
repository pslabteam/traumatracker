package it.unibo.disi.pslab.bluetoothlib;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import it.unibo.disi.pslab.bluetoothlib.exceptions.BluetoothMessageTooBigException;
import it.unibo.disi.pslab.bluetoothlib.exceptions.ServerRunningException;
import it.unibo.disi.pslab.bluetoothlib.utils.BluetoothError;
import it.unibo.disi.pslab.bluetoothlib.utils.C;

public class BluetoothServer {

    private volatile boolean running = false;

    private BluetoothServerSocket serverSocket;

    private List<Events> listeners = new ArrayList<Events>();

    private Map<BluetoothDevice, Connection> connectedDevices = new HashMap<BluetoothDevice, Connection>();

    private static BluetoothServer instance = new BluetoothServer();

    /**
     *
     * @return The singleton instance of this class
     */
    public static BluetoothServer getInstance() {
        return instance;
    }

    private BluetoothServer() {}

    /**
     * Call this method to register an Event Listener to obtain BluetoothServer events notification.
     * This method should be called before any other method on the singletone instance of this class.
     * @param el The class representing the Event Listener
     */
    public void registerEventListener(BluetoothServer.Events el) {
        listeners.add(el);
    }

    /**
     * Activates the Bluetooth Server, if not already running.
     * @param name The Name of this Bluetooth Server.
     * @param uuid The UUID to be used to instantiate the communication.
     * @throws ServerRunningException
     * @throws IOException
     */
    public void startServer(String name, String uuid) throws ServerRunningException, IOException {
        if (running) {
            throw new ServerRunningException();
        }

        running = true;

        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            for (Events e : listeners) {
                e.onServerError(BluetoothError.BLUETOOTH_NOT_ENABLED);
            }
        }

        serverSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord(name, UUID.fromString(uuid));

        new ServerThread().start();

        for (Events e : listeners) {
            e.onServerStarted();
        }
    }

    /**
     * Interrupts the Bluetooth Server
     */
    public void stopServer() {
        if (!running) {
            return;
        }

        running = false;

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Events e : listeners) {
            e.onServerStopped();
        }
    }

    /**
     * Check if the server is running
     * @return True, if the server is running, False otherwise.
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Sends a message to a specific connected device
     * @param device The device to send the message to.
     * @param message The message to send.
     */
    public void sendMessage(BluetoothDevice device, String message) {
        try {
            connectedDevices.get(device).send(message);
        } catch (BluetoothMessageTooBigException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public interface Events {
        void onServerStarted();
        void onServerStopped();
        void onServerError(BluetoothError error);
        void onDeviceConnected(BluetoothDevice device);
        void onDeviceDisconnected();
        void onNewMessageReceived(String message);
    }

    /**
     *
     */
    private class ServerThread extends Thread {

        @Override
        public void run() {
            while (running) {
                try {
                    BluetoothSocket socket = serverSocket.accept();
                    BluetoothDevice device = socket.getRemoteDevice();

                    Connection connection = new Connection(socket);
                    new Thread(connection).start();

                    connectedDevices.put(device, connection);

                    for (Events e : listeners) {
                        e.onDeviceConnected(device);
                    }

                } catch (IOException e) {
                    running = false;
                    for (Events ev : listeners) {
                        ev.onServerError(BluetoothError.GENERIC_EXCEPTION);
                    }
                }
            }
        }
    }

    /**
     *
     */
    private class Connection implements Runnable {

        private BluetoothSocket btSocket;
        private volatile boolean active = false;

        private Connection(BluetoothSocket socket) {
            btSocket = socket;
            active = true;
        }

        @Override
        public void run() {
            while (active) {
                try {
                    int bytesRead = 0, messageSize = btSocket.getInputStream().read();

                    StringBuffer buffer = new StringBuffer();

                    while (bytesRead < messageSize) {
                        buffer.append((char) btSocket.getInputStream().read());
                        bytesRead++;
                    }

                    for (Events e : listeners) {
                        e.onNewMessageReceived(buffer.toString());
                    }
                } catch (IOException e) {
                    active = false;

                    for (Events ev : listeners) {
                        ev.onDeviceDisconnected();
                    }
                }
            }
        }

        void send(String msg) throws BluetoothMessageTooBigException, IOException {
            if (msg.length() > C.BT_MESSAGE_MAX_LENGTH) {
                throw new BluetoothMessageTooBigException();
            }

            char[] array = msg.toCharArray();
            byte[] bytes = new byte[array.length + 1];
            bytes[0] = (byte) msg.length();

            for (int i = 0; i < array.length; i++) {
                bytes[i + 1] = (byte) array[i];
            }

            btSocket.getOutputStream().write(bytes);
            btSocket.getOutputStream().flush();
        }
    }
}
