package it.unibo.disi.pslab.bluetoothlib.utils;

public enum BluetoothError {
    BLUETOOTH_NOT_ENABLED,
    GENERIC_EXCEPTION
}
