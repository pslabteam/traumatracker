package app;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Main class of the application.
 */
public class App {

    private static final String CONFIG_TYPE = "file";
    private static final String CONFIG_PATH_KEY = "path";
    private static final String CONFIG_PATH = "res/config.txt";

    private static final String CONFIG_TAG_KEY = "tags";
    private static final String CONFIG_GATEWAY_KEY = "gateways";
    
    private static final String ID_KEY = "id";
    
    private static final int PORT = 8083;
    private static final String IP = "localhost";
    private static final String BASE_URL = "/gt2/locationservice/api";
    
    public static void main(final String[] args) {
        System.out.println("DEBUG: App started!");
        final Vertx v = Vertx.vertx();

        final ConfigStoreOptions storeOpt = new ConfigStoreOptions()
                                           .setType(CONFIG_TYPE)
                                           .setConfig(new JsonObject().put(CONFIG_PATH_KEY, CONFIG_PATH));

        final ConfigRetrieverOptions opt = new ConfigRetrieverOptions().addStore(storeOpt);

        ConfigRetriever.create(v, opt).getConfig(res -> {
              if (res.failed()) {
                  System.out.println("Failed to load system config!");
                  v.close();
              } else {
                  
                  /* Reading config */
                  final JsonObject config = res.result();
                  final JsonArray tagList = config.getJsonArray(CONFIG_TAG_KEY);
                  final JsonArray gwList = config.getJsonArray(CONFIG_GATEWAY_KEY);   

                  /* Send config msg(s) as REST request(s) */
                  final HttpClient client = v.createHttpClient();
                  final List<Future> endFuture = new ArrayList<>();
                  for (int i = 0; i < tagList.size() + gwList.size() + 2; i++) endFuture.add(Future.future());
                  final Iterator<Future> it = endFuture.iterator();
                  
                  CompositeFuture.all(endFuture).setHandler(r -> {
                      System.out.println("Config ended successfully!");
                      v.close();
                  });
                  
                  //Tags
                  client.delete(PORT, IP, BASE_URL + "/tags/any", r -> { it.next().complete(); }).end();
                  tagList.stream()
                          .map(JsonObject.class::cast)
                          .forEach(t -> client.post(PORT, IP, BASE_URL + "/tags/" + t.getString(ID_KEY), r -> { 
                                            it.next().complete();
                                        })
                                        .end(t.toBuffer()));

                  //Gateways               
                  client.delete(PORT, IP, BASE_URL + "/gateways/any", r -> { it.next().complete(); }).end();
                  gwList.stream()
                         .map(JsonObject.class::cast)
                         .forEach(g -> client.post(PORT, IP, BASE_URL + "/gateways/" + g.getString(ID_KEY), r -> { 
                                           it.next().complete();
                                       })
                                       .end(g.toBuffer()));  
              }     
          });     
    }
}
