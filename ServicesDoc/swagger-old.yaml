# this is an example of the Uber API
# as a demonstration of an API spec in YAML
swagger: '2.0'
info:
  title: TTService API
  description: Hospital 2.0
  version: "1.0.0"
# the domain of the service
host: localhost:8080
# array of all schemes that your API supports
schemes:
  - https
# will be prefixed to all paths
#basePath: 
produces:
  - application/json
paths:

  #GESTIONE REPORT#
  /gt2/traumatracker/api/reports:
    get:
      summary: Get all reports
      description: |
        All reports are returned
      tags:
        - TraumaTrackerService
      responses:
        200:
          description: Reports
          schema:
            type: array
            items:
              $ref: '#/definitions/Report'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    post:
      summary: Add a report.
      description: |
        Add a report after the mission is completed
      tags:
        - TraumaTrackerService
      parameters:
        - name: report
          description: Report of the mission.
          required: true
          in: body
          schema:
            $ref: '#/definitions/Report'
      responses:
        201:
          description: The report has been added.
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
            
  /gt2/traumatracker/api/reports/{id}:
    get:
      summary: Get a report.
      description: |
        The requested report is returned
      tags:
        - TraumaTrackerService
      parameters:
        - name: id
          in: path
          description: ID of the report to get
          required: true
          type: integer
          format: int64
      responses:
        200:
          description: Report to get
          schema:
              $ref: '#/definitions/Report'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    put:
      summary: Update a report
      description: |
        Update a report
      tags:
        - TraumaTrackerService
      parameters:
        - name: id
          in: path
          description: ID of the report to update
          required: true
          type: integer
          format: int64
        - name: report
          description: Report of the mission to update
          required: true
          in: body
          schema:
            $ref: '#/definitions/Report'
      responses:
        200:
          description: The report has been updated
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    delete:
      summary: Delete a report
      description: |
        Delete a report
      tags:
        - TraumaTrackerService
      parameters:
        - name: id
          in: path
          description: ID of the report to delete
          required: true
          type: integer
          format: int64
      responses:
        200:
          description: The report has been deleted
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    
            
  #GESTIONE MONITORI#    
  /gt2/gateway/api/monitors:
    get:
      summary: Get all monitors
      description: |
        All monitors are returned
      tags:
        - GatewayService
      responses:
        200:
          description: Monitors
          schema:
            type: array
            items:
              $ref: '#/definitions/Monitor'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    post:
      summary: Add a monitor
      description: |
        Add a new monitor
      tags:
        - GatewayService
      parameters:
        - name: monitor
          description: Monitor to create
          required: true
          in: body
          schema:
            $ref: '#/definitions/Monitor'
      responses:
        201:
          description: The monitor has been added
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
            
  /gt2/gateway/api/monitors/{id}:
    put:
      summary: Update a monitor
      description: |
        Update a monitor
      tags:
        - GatewayService
      parameters:
        - name: id
          in: path
          description: ID of the monitor to update
          required: true
          type: integer
          format: int64
        - name: monitor
          description: Monitor to update
          required: true
          in: body
          schema:
            $ref: '#/definitions/Monitor'
      responses:
        200:
          description: The monitor has been updated
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
    delete:
      summary: Delete a monitor
      description: |
        Delete a monitor
      tags:
        - GatewayService
      parameters:
        - name: id
          in: path
          description: ID of the monitor to delete
          required: true
          type: integer
          format: int64
      responses:
        200:
          description: The monitor has been deleted
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
      
  /gt2/gateway/api/monitors/{monitor_id}/{vitalsign}:
    get:
      summary: Get a vital sign.
      description: |
        Get one of the vital sign of the patient attached to the specified monitor
      tags:
        - GatewayService
      parameters:
        - name: monitor_id
          in: path
          description: ID of the monitor [in the form "careunitlabel_bedunitlabel"]
          required: true
          type: string
        - name: vitalsign
          in: path
          description: Vital sign to request
          required: true
          enum:
            - DIA
            - SYS
            - HR
            - SpO2
            - EtCO2
            - Temp
          type: string
      responses:
        200:
          description: Vital sign requested.
          schema:
              $ref: '#/definitions/VitalParameter'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
            
  /gt2/gateway/api/monitors/{monitor_id}/vitalsigns:
    get:
      summary: Get all relevant vital signs
      description: |
        Get all of the relevant vital signs of the patient attached to the specified monitor
      tags:
        - GatewayService
      parameters:
        - name: monitor_id
          in: path
          description: ID of the monitor [in the form "careunitlabel_bedunitlabel"]
          required: true
          type: string
      responses:
        200:
          description: Vital signs requested.
          schema:
            type: array
            items:
              $ref: '#/definitions/VitalParameter'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/Error'
          
            
            
            
            
definitions:
  Report:
    type: object
    properties:
      operatorID:
        type: string
        description: Unique identifier of the report
      date:
        type: string
        description: Date (ISO 8601) of the report
      time:
        type: string
        description: Time of the report
      notes:
        type: string
        description: Notes about the report
      events:
        type: array
        items:
          $ref: '#/definitions/Event'
        
  Event:
    type: object
    discriminator: type
    required:
    - type
    properties:
      eventID:
        type: string
        description: Unique identifier of event.
      when:
        type: string
        description: Date (ISO 8601) of the event
      where:
        type: string
        description: Location of the event
      type:
        type: string
        enum:
          - procedure
          - drug
          - vital-signs
          - photo
        description: Event type (procedure; drug; vital-signs; photo)
    
  EventProcedure:
    type: object
    allOf:
    - $ref: '#/definitions/Event'
    - type: object
      properties:
        what:
          $ref: '#/definitions/Procedure'
          
  EventDrug:
    type: object
    allOf:
    - $ref: '#/definitions/Event'
    - type: object
      properties:
        what:
          $ref: '#/definitions/Drug'
          
  EventVitalSigns:
    type: object
    allOf:
    - $ref: '#/definitions/Event'
    - type: object
      properties:
        what:
          $ref: '#/definitions/VitalSigns'
          
  EventPhoto:
    type: object
    allOf:
    - $ref: '#/definitions/Event'
    - type: object
      properties:
        what:
          $ref: '#/definitions/Photo'
    
  Procedure:
    type: object
    properties:
      procID:
        type: string
        enum:
          - Intubazione
          - Drenaggio toracico
          - Pelvic binder
          - Catetere alto flusso
          - Infusore a pressione
        description: Unique identifier of the procedure
        
  Drug:
    type: object
    properties:
      drugID:
        type: string
        description: Unique identifier of the drug
      drugQty:
        type: integer
        description: quantity of the drug
      drugUnit:
        type: string
        description: unit of measure of the drug
        
  VitalSigns:
    type: object
    properties:
      vitalSignsID:
        type: string
        enum:
          - Temp
          - HR
          - DIA
          - SYS
          - SpO2
          - EtCO2
        description: Unique identifier of the vital sign
        
  Photo:
    type: object
    properties:
      photoID:
        type: string
        description: Unique identifier of the photo
      photoImg:
        type: string
        description: Content of the photo
        
  VitalParameter:
    type: object
    properties:
      type:
        type: string
        description: Vital sign requested
      unitMeasure:
        type: string
        description: Unit measure of the vital sign
      measure:
        type: string
        description: Value of the vital sign
        
  Monitor:
    type: object
    properties:
      id:
        type: string
        description: ID of the monitor (CareUnitLabel_BedUnitLabel)
      ip_gateway:
        type: string
        description: IP address of the gateway the monitor is connected to
      port_gateway:
        type: string
        description: port of the gateway the monitor is connected to
        
  Error:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
      otherInfos:
        type: string
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
  