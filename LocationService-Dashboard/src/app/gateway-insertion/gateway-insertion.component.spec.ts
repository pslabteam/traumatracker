import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayInsertionComponent } from './gateway-insertion.component';

describe('GatewayInsertionComponent', () => {
  let component: GatewayInsertionComponent;
  let fixture: ComponentFixture<GatewayInsertionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatewayInsertionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayInsertionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
