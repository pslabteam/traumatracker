import { Component, OnInit, OnDestroy } from '@angular/core';
import { Gateway } from '../gateway';
import { gatewayBaseId } from '../gateway';
import { GatewayManagerService } from "../gateway-manager.service";
import { Region } from "../region";
import { RegionManagerService } from "../region-manager.service";

@Component({
  selector: 'app-gateway-insertion',
  templateUrl: './gateway-insertion.component.html',
  styleUrls: ['./gateway-insertion.component.css', '../common/button.css', '../common/title.css']
})
export class GatewayInsertionComponent implements OnInit {

  private gw: Gateway = { id: null, ipAddress: null, place: null, threshold: null, online: true };
  private baseId = gatewayBaseId;
  private rl: Region[];
  private selectedPlace: string;

  private subscription: any;

  constructor(private gatewayManager: GatewayManagerService, private regionManager: RegionManagerService) { }

  ngOnInit() {
    this.regionManager.getRegions().subscribe(list => this.rl = list);
    this.subscription = this.gatewayManager.change.subscribe(b => {
      if (b) {
        this.gw.id = null;
        this.gw.ipAddress = null;
        this.gw.place = null;
        this.gw.threshold = null;
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  addGateway(): void {
    this.gatewayManager.addGateway(this.gw);
  }
}
