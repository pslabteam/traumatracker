export class Tag {
  id: string;
  serialNumber: number;
  battery: number;
  timestamp: string;
}

export const tagBaseId = "tag";
