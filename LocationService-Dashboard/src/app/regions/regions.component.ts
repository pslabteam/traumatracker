import { Component, OnInit, OnDestroy } from '@angular/core';
import { Region } from '../region';
import { RegionManagerService } from '../region-manager.service';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css', '../common/button.css', '../common/title.css']
})

export class RegionsComponent implements OnInit {

  private regions: Region[];

  private subscription: any;

  constructor(private regionManager: RegionManagerService) { }

  ngOnInit() {
    this.updateRegions();
    this.subscription = this.regionManager.change.subscribe(any => this.updateRegions());
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  updateRegions(): void {
    this.regionManager.getRegions().subscribe(rs => this.regions = rs);
  }

}
