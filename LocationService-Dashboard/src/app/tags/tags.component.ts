import { Component, OnInit, OnDestroy } from '@angular/core';
import { TagManagerService } from '../tag-manager.service';
import { Tag } from '../tag';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css', '../common/button.css', '../common/title.css']
})
export class TagsComponent implements OnInit {

   tags: Tag[];
   private subscription: any;

  constructor(private tagManager: TagManagerService) { }

  ngOnInit() {
    this.updateTags();
    this.subscription = this.tagManager.change.subscribe(any => this.updateTags());
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  updateTags(): void {
    this.tagManager.getTags().subscribe(tags => this.tags = tags);
  }
}
