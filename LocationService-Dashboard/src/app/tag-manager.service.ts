import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from '@angular/router';

import { Tag } from "./tag";
import { tagBaseId } from "./tag";
import { ReplyMsg } from "./reply-msg";

import { serverAddress } from "./serverAddress";
import { serverBaseUrl } from "./serverAddress";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

class TagHttpBody {
  serialNumber : number;
}

@Injectable({
  providedIn: 'root'
})
export class TagManagerService {

  baseUrl: string;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient, private location: Location, private router: Router) {
    this.baseUrl = 'http://' + serverAddress + serverBaseUrl + 'tags/';
    this.router.onSameUrlNavigation = 'reload';
  }

  ngOnInit(): void { }

  createBody(t: Tag): TagHttpBody { //It creates the body used in POST and PUT requests
    return {
      "serialNumber" : +t.serialNumber
    };
  }

  getTags(): Observable<Tag[]> {
      return this.http.get<Tag[]>(this.baseUrl + 'any');
  }

  getTag(id: string): Observable<Tag> {
    return this.http.get<Tag>(this.baseUrl + `${id}`);
  }

  deleteTag(t: Tag): void {
    this.http.delete<ReplyMsg>(this.baseUrl + `${t.id}`, httpOptions)
    .subscribe(msg => {
      if (msg.code === 0) {
        this.router.navigateByUrl('tags');
        console.log(msg.msg);
      } else {
        console.log(msg.msg);
        alert(msg.msg);
      }
    });
  }

  updateSerial(t: Tag): void {
    this.http.put<ReplyMsg>(this.baseUrl + `${t.id}`, this.createBody(t), httpOptions)
    .subscribe(msg => {
      if (msg.code === 0) {
        console.log(msg.msg);
        alert(msg.msg);
        this.change.emit(false);
      } else {
        console.log(msg.msg);
        alert(msg.msg);
      }
    });
  }

  addTag(t: Tag): void {
      this.http.post<ReplyMsg>(this.baseUrl + tagBaseId + `${t.id}`, this.createBody(t), httpOptions)
      .subscribe(msg => {
          if (msg.code === 0) {
            console.log(msg.msg);
            alert(msg.msg);
            this.change.emit(true);
          } else {
            console.log(msg.msg);
            alert(msg.msg);
          }
      });
  }
}
