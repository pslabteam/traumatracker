import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './/app-routing.module';

import { AppComponent } from './app.component';
import { TagsComponent } from './tags/tags.component';
import { TagInsertionComponent } from './tag-insertion/tag-insertion.component';
import { GatewaysComponent } from './gateways/gateways.component';
import { GatewayInsertionComponent } from './gateway-insertion/gateway-insertion.component';
import { TagDetailComponent } from './tag-detail/tag-detail.component';
import { GatewayDetailComponent } from './gateway-detail/gateway-detail.component';
import { NotFoundResourceComponent } from './not-found-resource/not-found-resource.component';
import { RegionsComponent } from './regions/regions.component';
import { RegionInsertionComponent } from './region-insertion/region-insertion.component';
import { RegionDetailComponent } from './region-detail/region-detail.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    TagsComponent,
    TagInsertionComponent,
    GatewaysComponent,
    GatewayInsertionComponent,
    TagDetailComponent,
    GatewayDetailComponent,
    NotFoundResourceComponent,
    RegionsComponent,
    RegionInsertionComponent,
    RegionDetailComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
