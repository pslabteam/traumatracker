import { TestBed, inject } from '@angular/core/testing';

import { GatewayManagerService } from './gateway-manager.service';

describe('GatewayManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GatewayManagerService]
    });
  });

  it('should be created', inject([GatewayManagerService], (service: GatewayManagerService) => {
    expect(service).toBeTruthy();
  }));
});
