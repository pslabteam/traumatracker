import { Component, OnInit, OnDestroy } from '@angular/core';
import { Region } from '../region';
import { RegionWithTags } from '../region-with-tags';
import { Tag } from '../tag';
import { Gateway } from '../gateway';
import { TagManagerService } from '../tag-manager.service';
import { GatewayManagerService } from '../gateway-manager.service';
import { RegionManagerService } from '../region-manager.service';

class TagInRegion {
  id: string;
  region: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../common/title.css', '../common/button.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  private regionList: Region[] = [];
  private gatewayList: Gateway[] = [];
  private tagList: RegionWithTags[] = [];
  private tagMap: TagInRegion[] = [] = [];

  private baseImgStatusPath: string = "assets/img/";
  private onlineImg: string = this.baseImgStatusPath + "green-circle.png";
  private offlineImg: string = this.baseImgStatusPath +  "red-circle.png";

  private intervalId;
  private updatePeriod: number = 5000;

  constructor(private tagManager: TagManagerService, private gwManager: GatewayManagerService, private regionManager: RegionManagerService) { }

  ngOnInit() {
    this.updatePage();
    this.intervalId = setInterval(() => this.updatePage(), this.updatePeriod);
  }

  ngOnDestroy() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
  }

  updatePage(): void {
    this.regionList.length = 0;
    this.gatewayList.length = 0;
    this.tagList.length = 0;
    this.tagMap.length = 0;
    this.setRegion();
    this.setGw();
  }

  setGw(): void {
      this.gwManager.getGateways().subscribe(gl => this.gatewayList = gl);
  }

  setTag(): void {
      for (let r of this.regionList) {
        this.regionManager.getRegion(r.id).subscribe(tl =>  {
          console.log("Region: " + r.id + "; tags: " + JSON.parse(tl.tags));
          for (let t of JSON.parse(tl.tags)) {
            this.tagMap.push({id: t, region: r.id});
          }
        });
      }
  }

  setRegion(): void {
      this.regionManager.getRegions().subscribe(rl => {
        this.regionList = rl;
        this.setTag();
      });
  }
}
