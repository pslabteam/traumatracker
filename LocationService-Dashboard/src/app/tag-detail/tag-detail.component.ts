import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Tag } from '../tag';
import { TagManagerService } from '../tag-manager.service';

@Component({
  selector: 'app-tag-detail',
  templateUrl: './tag-detail.component.html',
  styleUrls: ['../common/button.css', './tag-detail.component.css', '../common/title.css']
})
export class TagDetailComponent implements OnInit {

  @Input() tag: Tag = {id: null, serialNumber: null, battery: null, timestamp: null};

  constructor(private tagManager: TagManagerService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.setTag();
  }

  setTag(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.tagManager.getTag(id).subscribe(t => this.tag = t);
  }

  updateSerial(): void {
    this.tagManager.updateSerial(this.tag);
  }

  deleteTag(): void {
    this.tagManager.deleteTag(this.tag);
  }
}
