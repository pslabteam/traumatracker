export class Gateway {
  id: string;
  ipAddress: string;
  place: string;
  threshold: number;
  online: boolean;
}

export const gatewayBaseId = "gw";
