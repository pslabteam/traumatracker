import { Component, OnInit, OnDestroy } from '@angular/core';
import { Tag } from '../tag';
import { tagBaseId } from '../tag';
import { TagManagerService } from "../tag-manager.service";

@Component({
  selector: 'app-tag-insertion',
  templateUrl: './tag-insertion.component.html',
  styleUrls: ['./tag-insertion.component.css', '../common/button.css', '../common/title.css']
})
export class TagInsertionComponent implements OnInit {

  private tag: Tag = {id: null, serialNumber: null, battery: null, timestamp: null};
  private baseId = tagBaseId;

  private subscription: any;

  constructor(private tagManager: TagManagerService) { }

  ngOnInit() {
    this.subscription = this.tagManager.change.subscribe(b => {
      if (b) {
        this.tag.id = null;
        this.tag.serialNumber = null;
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  addTag(): void {
      this.tagManager.addTag(this.tag);
  }
}
