import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagInsertionComponent } from './tag-insertion.component';

describe('TagInsertionComponent', () => {
  let component: TagInsertionComponent;
  let fixture: ComponentFixture<TagInsertionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagInsertionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagInsertionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
