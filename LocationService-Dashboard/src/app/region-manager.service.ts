import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Region } from './region';
import { regionBaseId } from './region';
import { ReplyMsg } from './reply-msg';
import { RegionWithTags } from './region-with-tags';

import { serverAddress } from "./serverAddress";
import { serverBaseUrl } from "./serverAddress";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

class RegionHttpBody {
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class RegionManagerService {

  baseUrl : string;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient, private router: Router) {
      this.baseUrl = 'http://' + serverAddress + serverBaseUrl + 'regions/';
  }

  createBody(r: Region): RegionHttpBody { //It creates the body used in POST and PUT requests
    return {
      "name" : r.name
    };
  }

  getRegions(): Observable<Region[]> {
    return this.http.get<Region[]>(this.baseUrl + "any");
  }

  getRegion(id: string): Observable<RegionWithTags> {
    return this.http.get<RegionWithTags>(this.baseUrl + `${id}`);
  }

  addRegion(r: Region): void {
      this.http.post<ReplyMsg>(this.baseUrl + regionBaseId + `${r.id}`, this.createBody(r), httpOptions)
      .subscribe(msg => {
          if (msg.code === 0) {
            console.log(msg.msg);
            alert(msg.msg);
            this.change.emit(true);
          } else {
            console.log(msg.msg);
            alert(msg.msg);
          }
      });
  }

  deleteRegion(r: Region): void {
    this.http.delete<ReplyMsg>(this.baseUrl + `${r.id}`, httpOptions)
    .subscribe(msg => {
      if (msg.code === 0) {
        this.router.navigateByUrl('regions');
        console.log(msg.msg);
      } else {
        console.log(msg.msg);
        alert(msg.msg);
      }
    });
  }

  updateRegion(r: Region): void {
    this.http.put<ReplyMsg>(this.baseUrl + `${r.id}`, this.createBody(r), httpOptions)
    .subscribe(msg => {
        if (msg.code === 0) {
          console.log(msg.msg);
          alert(msg.msg);
          this.change.emit(false);
        } else {
          console.log(msg.msg);
          alert(msg.msg);
        }
    });
  }
}
