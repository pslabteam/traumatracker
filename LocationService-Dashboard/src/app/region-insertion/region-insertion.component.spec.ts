import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionInsertionComponent } from './region-insertion.component';

describe('RegionInsertionComponent', () => {
  let component: RegionInsertionComponent;
  let fixture: ComponentFixture<RegionInsertionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionInsertionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionInsertionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
