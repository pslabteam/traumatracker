import { Component, OnInit, OnDestroy } from '@angular/core';
import { Region } from '../region';
import { regionBaseId} from '../region';
import { RegionManagerService } from "../region-manager.service";

@Component({
  selector: 'app-region-insertion',
  templateUrl: './region-insertion.component.html',
  styleUrls: ['./region-insertion.component.css', '../common/button.css', '../common/title.css']
})
export class RegionInsertionComponent implements OnInit {

  private region: Region = { id: null, name: null};
  private baseId = regionBaseId;
  constructor(private regionManager: RegionManagerService) { }

  private subscription: any;

  ngOnInit() {
    this.subscription = this.regionManager.change.subscribe(b => {
      if (b) {
        this.region.id = null;
        this.region.name = null;
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


  addRegion(): void {
    this.regionManager.addRegion(this.region);
  }
}
