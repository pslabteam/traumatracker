import { Injectable,  Output, EventEmitter  } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Gateway } from './gateway';
import { gatewayBaseId } from'./gateway';
import { ReplyMsg } from './reply-msg';

import { serverAddress } from "./serverAddress";
import { serverBaseUrl } from "./serverAddress";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

class GatewayHttpBody {
  ipAddress: string;
  place: string;
  threshold: number;
}

@Injectable({
  providedIn: 'root'
})
export class GatewayManagerService {

  baseUrl : string;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient, private router: Router) {
    this.baseUrl = 'http://' + serverAddress + serverBaseUrl + 'gateways/';
  }

  createBody(gw: Gateway): GatewayHttpBody { //It creates the body used in POST and PUT requests
    return {
      "ipAddress" : gw.ipAddress,
      "place" : gw.place,
      "threshold" : +gw.threshold
    };
  }

  getGateways(): Observable<Gateway[]> {
    return this.http.get<Gateway[]>(this.baseUrl + "any");
  }

  getGateway(id: string) : Observable<Gateway> {
    return this.http.get<Gateway>(this.baseUrl + `${id}`);
  }

  deleteGateway(gw: Gateway): void {
    this.http.delete<ReplyMsg>(this.baseUrl + `${gw.id}`, httpOptions)
    .subscribe(msg => {
      if (msg.code === 0) {
        this.router.navigateByUrl('gateways');
        console.log(msg.msg);
      } else {
        console.log(msg.msg);
        alert(msg.msg);
      }
    });
  }

  addGateway(gw: Gateway): void {
    this.http.post<ReplyMsg>(this.baseUrl + gatewayBaseId + `${gw.id}`, this.createBody(gw), httpOptions)
    .subscribe(msg => {
        if (msg.code === 0) {
          console.log(msg.msg)
          alert(msg.msg);
          this.change.emit(true);
        } else {
          console.log(msg.msg);
          alert(msg.msg);
        }
    });
  }

  updateGateway(gw: Gateway): void {
    this.http.put<ReplyMsg>(this.baseUrl + `${gw.id}`, this.createBody(gw), httpOptions)
    .subscribe(msg => {
        if (msg.code === 0) {
          console.log(msg.msg);
          alert(msg.msg);
          this.change.emit(false);
        } else {
          console.log(msg.msg);
          alert(msg.msg);
        }
    });
  }
}
