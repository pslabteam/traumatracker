import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Region } from '../region';
import { RegionManagerService } from '../region-manager.service';

@Component({
  selector: 'app-region-detail',
  templateUrl: './region-detail.component.html',
  styleUrls: ['./region-detail.component.css', './region-detail-tags.component.css', '../common/title.css', '../common/button.css']
})
export class RegionDetailComponent implements OnInit {

  @Input() region: Region = {id: null, name: null};
  private tags: string;
  private displayTags: boolean;
  private baseImgArrowPath: string = "assets/img/";
  private rightArrowImg: string = this.baseImgArrowPath + "right-arrow.png";
  private downArrowImg: string = this.baseImgArrowPath +  "down-arrow.png";

  constructor(private regionManager: RegionManagerService, private route: ActivatedRoute) {
    this.displayTags = true;
  }

  ngOnInit() {
    this.setRegion();
  }

  setRegion(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.regionManager.getRegion(id).subscribe(er => {
      this.region.id = er.id;
      this.region.name = er.name;
      this.tags = JSON.parse(er.tags);
    });
  }

  updateRegion(): void {
    this.regionManager.updateRegion(this.region);
  }

  deleteRegion(): void {
    this.regionManager.deleteRegion(this.region);
  }

  toggleTags() {
    this.displayTags = !this.displayTags;
  }
}
