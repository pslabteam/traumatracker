import { Component, OnInit, OnDestroy } from '@angular/core';
import { Gateway } from '../gateway';
import { GatewayManagerService } from "../gateway-manager.service";

@Component({
  selector: 'app-gateways',
  templateUrl: './gateways.component.html',
  styleUrls: ['./gateways.component.css', '../common/button.css', '../common/title.css']
})
export class GatewaysComponent implements OnInit {

  private gateways: Gateway[];
  private baseImgStatusPath: string = "assets/img/";
  private onlineImg: string = this.baseImgStatusPath + "green-circle.png";
  private offlineImg: string = this.baseImgStatusPath +  "red-circle.png";

  private subscription: any;

  constructor(private gwManager: GatewayManagerService) { }

  ngOnInit() {
    this.updateGws();
    this.subscription = this.gwManager.change.subscribe(any => this.updateGws());
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  updateGws(): void {
    this.gwManager.getGateways().subscribe(gws => this.gateways = gws);
  }

}
