import { TestBed, inject } from '@angular/core/testing';

import { RegionManagerService } from './region-manager.service';

describe('RegionManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegionManagerService]
    });
  });

  it('should be created', inject([RegionManagerService], (service: RegionManagerService) => {
    expect(service).toBeTruthy();
  }));
});
