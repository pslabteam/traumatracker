export class RegionWithTags {
  id: string;
  name: string;
  tags: string;
}
