import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Gateway } from '../gateway';
import { GatewayManagerService } from '../gateway-manager.service';
import { Region } from "../region";
import { RegionManagerService } from "../region-manager.service";

@Component({
  selector: 'app-gateway-detail',
  templateUrl: './gateway-detail.component.html',
  styleUrls: ['./gateway-detail.component.css', '../common/title.css', '../common/button.css']
})
export class GatewayDetailComponent implements OnInit {

  @Input() gw: Gateway = {id: null, ipAddress: null, place: null, threshold: null, online: true};
  private rl: Region[];

  constructor(private gwManager: GatewayManagerService, private route: ActivatedRoute, private regionManager: RegionManagerService) { }

  ngOnInit() {
    this.regionManager.getRegions().subscribe(list => this.rl = list);
    this.setGateway();
  }

  setGateway(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.gwManager.getGateway(id).subscribe(g => this.gw = g);
  }

  updateGateway(): void {
    this.gwManager.updateGateway(this.gw);
  }

  deleteGateway(): void {
    this.gwManager.deleteGateway(this.gw);
  }

}
