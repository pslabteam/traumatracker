import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TagsComponent } from './tags/tags.component';
import { GatewaysComponent } from './gateways/gateways.component';
import { RegionsComponent } from './regions/regions.component';
import { TagDetailComponent } from './tag-detail/tag-detail.component';
import { GatewayDetailComponent } from './gateway-detail/gateway-detail.component';
import { RegionDetailComponent } from './region-detail/region-detail.component';
import { NotFoundResourceComponent } from './not-found-resource/not-found-resource.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'tags', component: TagsComponent},
  {path: 'tags/:id', component: TagDetailComponent},
  {path: 'gateways', component: GatewaysComponent},
  {path: 'gateways/:id', component: GatewayDetailComponent},
  {path: 'regions', component: RegionsComponent },
  {path: 'regions/:id', component: RegionDetailComponent },
  {path: '**', component: NotFoundResourceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
